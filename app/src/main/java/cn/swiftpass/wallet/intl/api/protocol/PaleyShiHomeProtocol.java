package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/19
 * 派利是首页 数据
 */

public class PaleyShiHomeProtocol extends BaseProtocol {
    public static final String TAG = PaleyShiHomeProtocol.class.getSimpleName();

    public PaleyShiHomeProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/redpacket/redPacketHomePage";
    }
}
