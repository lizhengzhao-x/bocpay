package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/12/2.
 */
public class ApkVerifyWithSHAProtocol extends BaseProtocol {
    String sha;

    @Override
    public boolean isNeedLogin() {
        return false;
    }


    public ApkVerifyWithSHAProtocol(String shaIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        sha = shaIn;
        mUrl = "api/sysconfig/checkVersion";
    }

    @Override
    public void packData() {
        super.packData();
        //appVertion 1.0.18_98
        //shaInfo   xxxxxxx
        mBodyParams.put(RequestParams.APPVERTION, BuildConfig.VERSION_NAME + "_" + BuildConfig.VERSION_CODE);
        mBodyParams.put(RequestParams.SHAINFO, sha);
    }
}
