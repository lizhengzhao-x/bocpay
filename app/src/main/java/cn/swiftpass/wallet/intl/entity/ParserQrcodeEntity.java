package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class ParserQrcodeEntity extends BaseEntity {


    private String transferAmount;
    private String transferCurrency;

    //付款类别 0：个人，1：商户
    private String paymentCategory;

    //FPS扫码，如果有金额，判断editableTraAmountFalg是否为1，为1表示金额可重新编辑；为其他值不可编辑
    //如果没有金额，不需要判断editableTraAmountFalg，都需要输入金额
    public boolean getEditableTraAmountFalg() {
        if (TextUtils.isEmpty(transferAmount)) {
            return true;
        }

        if (TextUtils.isEmpty(editableTraAmountFalg)) {
            return false;
        }
        return editableTraAmountFalg.equals("1");
    }

    public void setEditableTraAmountFalg(String editableTraAmountFalg) {
        this.editableTraAmountFalg = editableTraAmountFalg;
    }

    private String editableTraAmountFalg;

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String getTransferAmount() {
        return transferAmount;
    }

    public void setTransferAmount(String transferAmount) {
        this.transferAmount = transferAmount;
    }

    public String getTransferCurrency() {
        return transferCurrency;
    }

    public void setTransferCurrency(String transferCurrency) {
        this.transferCurrency = transferCurrency;
    }

    public String getTransferBankCode() {
        return transferBankCode;
    }

    public void setTransferBankCode(String transferBankCode) {
        this.transferBankCode = transferBankCode;
    }

    public String getTransferAccNo() {
        return transferAccNo;
    }

    public void setTransferAccNo(String transferAccNo) {
        this.transferAccNo = transferAccNo;
    }

    public String getTransferAccTp() {
        return transferAccTp;
    }

    public void setTransferAccTp(String transferAccTp) {
        this.transferAccTp = transferAccTp;
    }

    private String transferBankCode;
    private String transferAccNo;
    private String transferAccTp;

    public String getDsplName() {
        return dsplName;
    }

    public void setDsplName(String dsplName) {
        this.dsplName = dsplName;
    }

    private String dsplName;
}
