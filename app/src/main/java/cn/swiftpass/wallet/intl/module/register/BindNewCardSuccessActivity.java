package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.CardListProtocol;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.register.adapter.BindNewCardResultAdapter;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 一键绑定信用卡绑卡成功
 */
public class BindNewCardSuccessActivity extends BaseCompatActivity {

    @BindView(R.id.tv_bind_result)
    TextView tvBindResult;
    @BindView(R.id.ry_confirm_bind_new_card)
    RecyclerView ryConfirmBindNewCard;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    private BindNewCardResultEntity bindCardResult;
    private int flowType;

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.KBCC2105_5_1);
        if (getIntent() != null) {
            flowType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
            Serializable serializableExtra = getIntent().getSerializableExtra(BindNewCardOtpActivity.DATA_BIND_NEW_CARD_RESULT);
            if (serializableExtra != null) {
                bindCardResult = (BindNewCardResultEntity) serializableExtra;
            }
        }


        ryConfirmBindNewCard.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        ArrayList<BindNewCardEntity> selectCardList = new ArrayList<>();
        if (bindCardResult != null) {
            if (bindCardResult.getPanSuccessList() != null) {
                for (BindNewCardEntity bindNewCardEntity : bindCardResult.getPanSuccessList()) {
                    bindNewCardEntity.setBindStatus(true);
                    selectCardList.add(bindNewCardEntity);
                }
            }
            if (bindCardResult.getPanFailList() != null) {
                for (BindNewCardEntity bindNewCardEntity : bindCardResult.getPanFailList()) {
                    bindNewCardEntity.setBindStatus(false);
                    selectCardList.add(bindNewCardEntity);
                }
            }
        }
        BindNewCardResultAdapter adapter = new BindNewCardResultAdapter(this, selectCardList);

        ryConfirmBindNewCard.setAdapter(adapter);


    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_bind_new_card_success;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    public void onBackPressed() {

    }

    @OnClick(R.id.tv_confirm)
    public void onClick() {
        if (ButtonUtils.isFastDoubleClick(R.id.tv_confirm)) {
            return;
        }
        refreshCardList();
    }

    private void nextPage() {
        if (bindCardResult != null
                && flowType == Constants.PAGE_FLOW_REGISTER
                && BindNewCardResultEntity.TYPE_SHOW_INVITE.equals(bindCardResult.getShowInvite())) {
            Intent intent = new Intent(this, RegisterInviteActivity.class);
            intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
            intent.putExtra(Constants.BIND_CARD_TYPE, true);
            startActivity(intent);
        } else {
            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
            MyActivityManager.removeAllTaskExcludeMainStack();
            ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
        }
    }

    /**
     * 如果一键绑定成功 直接到被扫界面 中间如果没有触发拉取卡列表流程 卡列表不会更新 目前先暂时做到点击按钮手动触发一次卡列表逻辑
     */
    public void refreshCardList() {
        showDialogNotCancel();
        new CardListProtocol(HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                nextPage();
            }

            @Override
            public void onSuccess(CardsEntity response) {
                dismissDialog();
                CacheManagerInstance.getInstance().setCardEntities(response.getRows());
                nextPage();
            }
        }).execute();
    }

}
