package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.jay.widget.StickyHeaders;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;

public class GetLiShiRedPacketHistoryAdapter extends RecyclerView.Adapter implements StickyHeaders {


    private GetLiShiHomeEntity homeEntity;
    private List<RedPacketBeanEntity> data;
    private Activity activity;
    private final int TYPE_UN_GET_LISHI_TITLE = 101;
    private final int TYPE_UN_GET_LISHI_CONTENT = 102;
    private int TYPE_GET_LISHI_TITLE_HISTORY = 103;
    private final int TYPE_GET_LISHI_HISTORY_ITEM = 104;
    private final int TYPE_GET_LISHI_HISTORY_EMPTY = 105;


    public GetLiShiRedPacketHistoryAdapter(Activity activity, GetLiShiHomeEntity homeEntity, @Nullable List<RedPacketBeanEntity> data) {
        this.activity = activity;
        this.homeEntity = homeEntity;
        this.data = data;
    }


    @Override
    public boolean isStickyHeader(int position) {
        return position == 0 || position == 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_UN_GET_LISHI_TITLE;
        } else if (position == 1) {
            return TYPE_UN_GET_LISHI_CONTENT;
        } else if (position == 2) {
            return TYPE_GET_LISHI_TITLE_HISTORY;
        } else {
            if (data != null && data.size() > 0) {
                return TYPE_GET_LISHI_HISTORY_ITEM;
            } else {
                return TYPE_GET_LISHI_HISTORY_EMPTY;
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_UN_GET_LISHI_TITLE) {
            View titleHolder = LayoutInflater.from(activity).inflate(R.layout.item_title_un_get_lishi, parent, false);
            return new UnGetLishiTitleHolder(activity, titleHolder);
        }
        if (viewType == TYPE_UN_GET_LISHI_CONTENT) {
            View contentHolder = LayoutInflater.from(activity).inflate(R.layout.item_content_un_get_lishi, parent, false);
            return new UnGetLishiContentHolder(activity, contentHolder);
        }
        if (viewType == TYPE_GET_LISHI_TITLE_HISTORY) {
            View titleHolder = LayoutInflater.from(activity).inflate(R.layout.item_title_history, parent, false);
            return new LishiHistoryTitleHolder(activity, titleHolder);
        }
        if (viewType == TYPE_GET_LISHI_HISTORY_ITEM) {
            View itemHolder = LayoutInflater.from(activity).inflate(R.layout.item_red_packet_history, parent, false);
            return new LishiHistoryItemHolder(activity, itemHolder);
        }
        if (viewType == TYPE_GET_LISHI_HISTORY_EMPTY) {
            View itemHolder = LayoutInflater.from(activity).inflate(R.layout.view_empty, parent, false);
            return new LishiHistoryEmptyHolder(activity, itemHolder);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UnGetLishiTitleHolder) {
            UnGetLishiTitleHolder unGetLishiTitleHolder = (UnGetLishiTitleHolder) holder;
            unGetLishiTitleHolder.setData(homeEntity);
        }
        if (holder instanceof UnGetLishiContentHolder) {
            UnGetLishiContentHolder unGetLishiContentHolder = (UnGetLishiContentHolder) holder;
            unGetLishiContentHolder.setData(homeEntity);
        }
        if (holder instanceof LishiHistoryTitleHolder) {
            LishiHistoryTitleHolder lishiHistoryTitleHolder = (LishiHistoryTitleHolder) holder;
            lishiHistoryTitleHolder.setData(homeEntity);
        }
        if (holder instanceof LishiHistoryItemHolder) {
            LishiHistoryItemHolder lishiHistoryItemHolder = (LishiHistoryItemHolder) holder;
            lishiHistoryItemHolder.setData(data.get(position - 3));
        }


    }

    @Override
    public int getItemCount() {
        return 3 + (data != null && data.size() > 0 ? data.size() : 1);
    }


    public void setDataList(List<RedPacketBeanEntity> redPacketBeanEntities) {
        this.data = redPacketBeanEntities;
    }

    public void setLiShiHome(GetLiShiHomeEntity getLiShiHomeEntity) {
        this.homeEntity = getLiShiHomeEntity;
    }

}
