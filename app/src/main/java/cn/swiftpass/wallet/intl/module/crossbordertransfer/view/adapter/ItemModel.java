package cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter;

import android.text.SpannableString;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class ItemModel extends BaseEntity {
    private int itemType;
    private String itemTitle = "";
    private String itemContent = "";
    private int textStatus;
    private SpannableString spannableString;

    public boolean isShowGradeReduceBtn() {
        return showGradeReduceBtn;
    }

    private boolean showGradeReduceBtn = false;
    public boolean isShowHeader() {
        return showHeader;
    }

    public void setShowHeader(boolean showHeader) {
        this.showHeader = showHeader;
    }

    private boolean showHeader = true;

    public int getContentTextSize() {
        return contentTextSize;
    }

    private int contentTextSize;

    public boolean isContentGone() {
        return mContentGone;
    }

    private boolean mContentGone = false;

    public int getItemContentColor() {
        return itemContentColor;
    }

    private int itemContentColor;

    public int getItemTitleColor() {
        return itemTitleColor;
    }

    public int getState() {
        return state;
    }

    private int itemTitleColor;

    public void setVisibleLine(boolean visibleLine) {
        isVisibleLine = visibleLine;
    }

    private boolean isVisibleLine = false;

    public int getResourceImg() {
        return resourceImg;
    }

    public void setResourceImg(int resourceImg) {
        this.resourceImg = resourceImg;
    }

    private int resourceImg;
    //跨境转账的结果
    private int state;

    public boolean ismItemRed() {
        return mItemRed;
    }

    private boolean mItemRed;

    public ItemModel(ItemModelBuilder itemModelBuilder) {
        this.itemType = itemModelBuilder.itemType;
        this.itemContent = itemModelBuilder.itemContent;
        this.itemTitle = itemModelBuilder.itemTitle;
        this.isVisibleLine = itemModelBuilder.isVisibleLine;
        this.state = itemModelBuilder.state;
        this.resourceImg = itemModelBuilder.resourceImg;
        this.textStatus = itemModelBuilder.textStatus;
        this.itemContentColor = itemModelBuilder.itemContentColor;
        this.itemTitleColor = itemModelBuilder.itemTitleColor;
        this.mContentGone = itemModelBuilder.mContentGone;
        this.contentTextSize = itemModelBuilder.contentTextSize;
        this.showHeader = itemModelBuilder.showHeader;
        this.spannableString = itemModelBuilder.spannableString;
        this.mItemRed = itemModelBuilder.mItemRed;
        this.showGradeReduceBtn = itemModelBuilder.showGradeReduceBtn;
    }

    public int getItemType() {
        return itemType;
    }

    public int getTextStatus() {
        return textStatus;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }


    public boolean isVisibleLine() {
        return isVisibleLine;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemContent() {
        return itemContent;
    }

    public SpannableString getItemContentSpan() {
        return spannableString;
    }

    public void setItemContent(String itemContent) {
        this.itemContent = itemContent;
    }

    public static class ItemModelBuilder {
        public static final int STYLE_BIG_TEXT = 1;
        public static final int STYLE_LINE = 2;

        private int itemType;
        private int itemContentColor;
        private int contentTextSize;
        private int itemTitleColor;
        private String itemTitle = "";
        private String itemContent = "";
        private boolean isVisibleLine = false;
        private boolean mContentGone = false;
        private boolean showHeader = true;
        private boolean mItemRed = false;

        public boolean isShowGradeReduceBtn() {
            return showGradeReduceBtn;
        }

        private boolean showGradeReduceBtn = false;

        public SpannableString getSpannableString() {
            return spannableString;
        }

        public void setSpannableString(SpannableString spannableString) {
            this.spannableString = spannableString;
        }

        private SpannableString spannableString;

        public int getState() {
            return state;
        }


        private int state;
        /**
         * 下划线模式 字体放大模式
         */
        private int textStatus;

        public int getResourceImg() {
            return resourceImg;
        }

        public void setResourceImg(boolean mContentGone) {
            this.mContentGone = mContentGone;
        }

        public void setResourceImg(int resourceImg) {
            this.resourceImg = resourceImg;
        }

        private int resourceImg;

        public ItemModelBuilder itemType(int itemType) {
            this.itemType = itemType;
            return this;
        }

        public ItemModelBuilder itemShowGradeReduceBtn(boolean mGradeReduce) {
            this.showGradeReduceBtn = mGradeReduce;
            return this;
        }

        public ItemModelBuilder itemContentGone(boolean mContentGone) {
            this.mContentGone = mContentGone;
            return this;
        }

        public ItemModelBuilder itemRedColor(boolean mItemRed) {
            this.mItemRed = mItemRed;
            return this;
        }

        public ItemModelBuilder itemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
            return this;
        }

        public ItemModelBuilder itemContentColor(int color) {
            this.itemContentColor = color;
            return this;
        }

        public ItemModelBuilder itemTitleColor(int color) {
            this.itemTitleColor = color;
            return this;
        }

        public ItemModelBuilder itemContent(String itemContent) {
            this.itemContent = itemContent;
            return this;
        }

        public ItemModelBuilder itemContent(SpannableString spannableString) {
            this.spannableString = spannableString;
            return this;
        }

        public ItemModelBuilder itemContentTextSize(int contentTextSize) {
            this.contentTextSize = contentTextSize;
            return this;
        }

        public ItemModelBuilder itemVisibleLine(boolean isVisibleLine) {
            this.isVisibleLine = isVisibleLine;
            return this;
        }

        public ItemModelBuilder itemImage(int imageResource) {
            this.resourceImg = imageResource;
            return this;
        }


        public ItemModelBuilder itemHeader(boolean showHeader) {
            this.showHeader = showHeader;
            return this;
        }

        public ItemModelBuilder itemState(int state) {
            this.state = state;
            return this;
        }

        public ItemModelBuilder itemTextState(int state) {
            this.textStatus = state;
            return this;
        }

        public ItemModel build() {
            return new ItemModel(this);
        }
    }


}
