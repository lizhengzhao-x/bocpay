package cn.swiftpass.wallet.intl.module.transfer.view;

import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.entity.TransferBillEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListTransferAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.TransferBillListAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;


public class TransferListActivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    private List<TransferBillEntity.TransferInfosBean> paymentEnquiryResults;
    private HashMap<String, List<TransferBillEntity.TransferInfosBean>> paymentResults;
    private static final String TYPE_TITLE = "TYPE_TITLE";
    private static final String TYPE_CONTENT = "TYPE_CONTENT";
    @BindView(R.id.id_empty_view)
    View id_empty_view;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_B0_2);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        idRecyclerview.setEmptyView(id_empty_view);
        paymentEnquiryResults = new ArrayList<>();
        ((IAdapter<TransferBillEntity.TransferInfosBean>) idRecyclerview.getAdapter()).setData(paymentEnquiryResults);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        paymentResults = new HashMap<>();
        getTransferList();
        id_empty_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTransferList();
            }
        });
    }

    private CommonRcvAdapter<TransferBillEntity.TransferInfosBean> getAdapter(List<TransferBillEntity.TransferInfosBean> data) {
        return new CommonRcvAdapter<TransferBillEntity.TransferInfosBean>(data) {

            @Override
            public Object getItemType(TransferBillEntity.TransferInfosBean demoModel) {
                return demoModel.getUiType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                //recycleView item样式分为两种 1：标题 2：内容
                if (type.equals(TYPE_TITLE)) {
                    return new BillListTransferAdapter(TransferListActivity.this);
                } else {
                    return new TransferBillListAdapter(TransferListActivity.this, new CardItemCallback() {

                        @Override
                        public void onItemCardClick(int position) {
                            super.onItemCardClick(position);
                            OrderQueryEntity orderQueryEntity = new OrderQueryEntity();

                            TransferBillEntity.TransferInfosBean transferInfosBean = paymentEnquiryResults.get(position);
                            orderQueryEntity.setTrxAmt(transferInfosBean.getAmount());
                            orderQueryEntity.setTxnId(transferInfosBean.getOrderNumber());
                            orderQueryEntity.setForwardingIIN("");
                            orderQueryEntity.setTransDate(transferInfosBean.getCreateTime());
                            orderQueryEntity.setPaymentStatus(transferInfosBean.getStatus());
                            orderQueryEntity.setCur(transferInfosBean.getCur());
                            orderQueryEntity.setMerchantName(transferInfosBean.getPayee());
                            orderQueryEntity.setTransactionNum(transferInfosBean.getOrderNumber());
                            orderQueryEntity.setStatusDesc(transferInfosBean.getStatusDesc());
                            orderQueryEntity.setPostscript(transferInfosBean.getPostscript());
                            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                            mHashMapsLogin.put(Constants.CARD_ENTITY, orderQueryEntity);
                            ActivitySkipUtil.startAnotherActivity(getActivity(), TransferDetailActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                }

            }
        };
    }

    private void getTransferList() {
        ApiProtocolImplManager.getInstance().transferSummary(mContext, "40", "", "", "", new NetWorkCallbackListener<TransferBillEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                }
                if (idRecyclerview.getAdapter() != null) {
                    idRecyclerview.getAdapter().notifyDataSetChanged();
                }
            }

            @Override
            public void onSuccess(TransferBillEntity response) {
                paymentEnquiryResults.clear();
                paymentResults.clear();
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                }
                TransferBillEntity transferBillEntity = response;
                if (transferBillEntity.getTransferInfos() == null) {
                    if (idRecyclerview.getAdapter() != null) {
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                    }
                    return;
                }
                int size = transferBillEntity.getTransferInfos().size();
                //所有交易列表客户端根据日期进行分组处理
                for (int i = 0; i < size; i++) {
                    TransferBillEntity.TransferInfosBean paymentEnquiryResult = transferBillEntity.getTransferInfos().get(i);
                    paymentEnquiryResult.setUiType(TYPE_CONTENT);
                    String transDate = paymentEnquiryResult.getCreateTime();
                    if (!TextUtils.isEmpty(transDate) && transDate.length() > 6) {
                        String monthTtitle = transDate.substring(4, 6) + "/" + transDate.substring(0, 4);
                        if (paymentResults.containsKey(monthTtitle)) {
                            List<TransferBillEntity.TransferInfosBean> paymentEnquiryResults = paymentResults.get(monthTtitle);
                            paymentEnquiryResults.add(paymentEnquiryResult);
                        } else {
                            TransferBillEntity.TransferInfosBean paymentEnquiryTitle = new TransferBillEntity.TransferInfosBean();
                            paymentEnquiryTitle.setUiTitle(monthTtitle);
                            paymentEnquiryTitle.setUiType(TYPE_TITLE);
                            paymentEnquiryResults.add(paymentEnquiryTitle);

                            List<TransferBillEntity.TransferInfosBean> paymentEnquiryResultItems = new ArrayList<>();
                            paymentEnquiryResultItems.add(paymentEnquiryResult);
                            paymentResults.put(monthTtitle, paymentEnquiryResultItems);
                        }
                        paymentEnquiryResults.add(paymentEnquiryResult);
                    } else {
                        paymentEnquiryResults.add(paymentEnquiryResult);
                    }
                }
                if (idRecyclerview.getAdapter() != null) {
                    idRecyclerview.getAdapter().notifyDataSetChanged();
                }
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_list;
    }


}
