package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Intent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderAccountListEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_SELECT_ACCOUNT;

public class TransferCrossBorderAccountListActivity extends BaseCompatActivity implements TransferAccountListAdapter.OnItemClickListener {


    @BindView(R.id.ry_purpose_list)
    RecyclerView ryPurposeList;
    private ArrayList<TransferCrossBorderAccountListEntity> accountList = new ArrayList<>();


    @Override
    public void init() {
        setToolBarTitle(R.string.CT1_3a_20);
        if (getIntent() != null) {
            TransferCrossBorderBaseEntity baseEntity = (TransferCrossBorderBaseEntity) getIntent().getSerializableExtra(TransferCrossBorderConst.DATA_TRANSFER_BASE);
            TransferCrossBorderAccountListEntity selectAccount = (TransferCrossBorderAccountListEntity) getIntent().getSerializableExtra(TransferCrossBorderConst.DATA_TRANSFER_SELECT_ACCOUNT);
            if (baseEntity != null) {
                accountList = baseEntity.accountList;
                ryPurposeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                TransferAccountListAdapter adapter = new TransferAccountListAdapter(this, accountList, this);
                if (selectAccount != null)
                    adapter.setSelect(selectAccount);
                ryPurposeList.setAdapter(adapter);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_cross_border_account_list;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    public void onItemClick(int position, TransferCrossBorderAccountListEntity select) {
        Intent data = new Intent();
        data.putExtra(DATA_TRANSFER_SELECT_ACCOUNT, select);
        setResult(RESULT_OK, data);
        finish();
    }
}
