package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class GetRegEddaInfoEntity extends BaseEntity {


    /**
     * bankList : [{"checked":false,"currency":"HKD","pageNumber":1,"pageSize":10,"participantCode":"013","participantName":"中过银行","total":0},{"checked":false,"currency":"HKD","pageNumber":1,"pageSize":10,"participantCode":"014","participantName":"中过银行","total":0},{"checked":false,"currency":"HKD","pageNumber":1,"pageSize":10,"participantCode":"111","participantName":"中过银行","total":0},{"checked":false,"currency":"HKD","pageNumber":1,"pageSize":10,"participantCode":"015","participantName":"中过银行","total":0},{"checked":false,"currency":"HKD","pageNumber":1,"pageSize":10,"participantCode":"345","participantName":"中过银行","total":0}]
     * idNum : S8933121
     * name : CHAN TAI MAN
     */

    private String idNum;
    private String name;
    private List<BankListBean> bankList;

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BankListBean> getBankList() {
        return bankList;
    }

    public void setBankList(List<BankListBean> bankList) {
        this.bankList = bankList;
    }

    public static class BankListBean extends BaseEntity {
        /**
         * checked : false
         * currency : HKD
         * pageNumber : 1
         * pageSize : 10
         * participantCode : 013
         * participantName : 中过银行
         * total : 0
         */

        private boolean checked;
        private String currency;
        private int pageNumber;
        private int pageSize;
        private String participantCode;
        private String participantName;
        private int total;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public String getParticipantCode() {
            return participantCode;
        }

        public void setParticipantCode(String participantCode) {
            this.participantCode = participantCode;
        }

        public String getParticipantName() {
            return participantName;
        }

        public void setParticipantName(String participantName) {
            this.participantName = participantName;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }
}
