package cn.swiftpass.wallet.intl.entity.event;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class BaseEventEntity extends BaseEntity {

    public BaseEventEntity(int eventType, String message) {

        this.eventType = eventType;
        this.eventMessage = message;
    }

    public BaseEventEntity(int eventType, String message, String errorCodeIn) {

        this.eventType = eventType;
        this.eventMessage = message;
        this.errorCode = errorCodeIn;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    private int eventType;
    private String eventMessage;

    public String getErrorCode() {
        return errorCode;
    }

    private String errorCode;

}
