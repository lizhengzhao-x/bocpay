package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.event.RegisterPopBannerDialogEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.RegisterVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardRegisterSuccessEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.RegisterVitualCardSendOtpPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardRegisterSendOtpActivity extends AbstractSendOTPActivity implements RegisterVitualCardSendOtpContract.View {
    private VirtualCardListEntity.VirtualCardListBean virtualCardListBean;
    private String mCardId;
    private String mWalletId;
    private RegisterVitualCardSendOtpContract.Presenter mPresenterDetail;

    @Override
    public void init() {
        if (mPresenter instanceof RegisterVitualCardSendOtpContract.Presenter) {
            mPresenterDetail = (RegisterVitualCardSendOtpContract.Presenter) mPresenter;
        }
        virtualCardListBean = (VirtualCardListEntity.VirtualCardListBean) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDLISTBEAN);
        phoneNo = getIntent().getExtras().getString(Constants.MOBILE_PHONE);
        mCardId = getIntent().getExtras().getString(Constants.CARD_ID);
        mWalletId = getIntent().getStringExtra(Constants.WALLET_ID);
        setToolBarTitle(getString(R.string.IDV_24_1));
        super.init();

        setBackBtnListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                AndroidUtils.showConfrimExitDialog(VirtualCardRegisterSendOtpActivity.this, new AndroidUtils.ConfirmClickListener() {
                    @Override
                    public void onConfirmBtnClick() {
                        ActivitySkipUtil.startAnotherActivity(VirtualCardRegisterSendOtpActivity.this, LoginActivity.class);
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    }
                });
            }
        });
    }

    @Override
    protected RegisterVitualCardSendOtpContract.Presenter createPresenter() {
        return new RegisterVitualCardSendOtpPresenter();
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.registerVitualCardVerifyOtp(virtualCardListBean, mWalletId, etwdOtpCode.getEditText().getText().toString().trim(), mCardId);
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {
        mPresenterDetail.registerVitualCardSenOtp(virtualCardListBean, isTryAgain);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void registerVitualCardSenOtpSuccess(VirtualCardSendOtpEntity response, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void registerVitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
        showRetrySendOtp();
    }

    @Override
    public void registerVitualCardVerifyOtpSuccess(VirtualCardRegisterSuccessEntity response) {
        CacheManagerInstance.getInstance().saveLoginStatus();
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        HttpCoreKeyManager.getInstance().setUserId(response.getWalletId());
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_VITUALCARD_REGISTER);
//        mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
//        mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
        if (!TextUtils.isEmpty(response.getNewUserPopUpBanner())) {
            RegisterPopBannerDialogEvent registerPopBannerDialogEvent = new RegisterPopBannerDialogEvent();
            registerPopBannerDialogEvent.updateEventParams(RegisterPopBannerDialogEvent.POPUPBANNERURL, response.getNewUserPopUpBanner());
            UserLoginEventManager.getInstance().addUserLoginEvent(registerPopBannerDialogEvent);
        }
        ActivitySkipUtil.startAnotherActivity(VirtualCardRegisterSendOtpActivity.this, VirtualCardBindSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void registerVitualCardVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(VirtualCardRegisterSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }
}
