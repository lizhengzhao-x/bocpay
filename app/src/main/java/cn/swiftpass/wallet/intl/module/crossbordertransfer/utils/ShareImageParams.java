package cn.swiftpass.wallet.intl.module.crossbordertransfer.utils;

public class ShareImageParams {
    public String currencyRMB;
    public String amountCNY;
    public String receiverName;
    public String receiverCardNo;
    public String receiverBankName;
    public String receiverCardType;
    public String panFour;
    public String payName;
    public String txTime;
    public String txnId;
    public String accountType;
    public String referenceNo;
    public String txnOptionStr;
    public String buttomShareTv;
    public String status;
    public String buttomShareImageUrl;
    public String fee;
}
