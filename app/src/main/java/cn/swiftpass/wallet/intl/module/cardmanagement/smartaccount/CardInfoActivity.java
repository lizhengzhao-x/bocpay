package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.DeleteCardPopWindow;
import cn.swiftpass.wallet.intl.dialog.OrderDetailPopWindow;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.UnlinkCardEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.BindCardSuccessActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;


public class CardInfoActivity extends BaseCompatActivity implements DeleteCardPopWindow.OnPopWindowOkClickListener, OrderDetailPopWindow.OnPopWindowOkClickListener {
    @BindView(R.id.tv_cardinfo_name)
    TextView tvCardinfoName;
    @BindView(R.id.tv_cardinfo_delete)
    TextView tvCardinfoDelete;
    @BindView(R.id.tv_cardinfo_cancel)
    TextView tvCardinfoCancel;
    @BindView(R.id.tv_left_image)
    ImageView mCardIconIV;
    @BindView(R.id.id_sub_line)
    View mSubLineView;
    @BindView(R.id.id_cancel_view)
    LinearLayout mDeleteAccountLinear;

    private DeleteCardPopWindow deleteCardPopWindow;
    private BankCardEntity cardEntity;
    private String defaultId;
    //卡列表
    private List<BankCardEntity> cardEntities;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        cardEntity = (BankCardEntity) getIntent().getSerializableExtra(Constants.CARD_ENTITY);

        setToolBarTitle(R.string.AC2101_4_1);
        defaultId = getIntent().getExtras().getString(Constants.DATA_NEW_DEFAULT_PANID);
        cardEntities = CacheManagerInstance.getInstance().getCardEntities();
        mDeleteAccountLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
                    initDeletePopWindow();
                }
            }
        });
        String cardName = AndroidUtils.getSubMasCardNumberTitleByLevel(this, cardEntity.getPan(), cardEntity.getCardType(), cardEntity.getSmartAccLevel());
        tvCardinfoDelete.setText(R.string.string_unlink);
        tvCardinfoName.setText(cardName);

        if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
            //智能账户 取消 解绑 其他 都是解绑
            mSubLineView.setVisibility(View.VISIBLE);
            tvCardinfoCancel.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            //支付账户 也是取消
            tvCardinfoCancel.setVisibility(View.VISIBLE);
            tvCardinfoDelete.setVisibility(View.GONE);
        }

        tvCardinfoCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
                    initDeletePopWindow();
                }
            }
        });

        GlideApp.with(CardInfoActivity.this).load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png").placeholder(R.mipmap.img_card_register_positive).into(mCardIconIV);

    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_cardinfo;
    }


    public void initDeletePopWindow() {
        deleteCardPopWindow = new DeleteCardPopWindow(this, this);
        deleteCardPopWindow.setMsg(R.string.AC2101_5_1);
        if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
            deleteCardPopWindow.showDeleteAccountView();
            deleteCardPopWindow.setMsg(R.string.AC2101_5_1);
        } else if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            //支付账户 也是取消
            deleteCardPopWindow.hideUnBindAccountView();
            deleteCardPopWindow.setMsg(R.string.AC2101_5_1);
        } else {
            deleteCardPopWindow.setMsg(R.string.AC2101_5_1);
        }
        if (TextUtils.equals(cardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            deleteCardPopWindow.setConfirmText(R.string.unlink_smart_account);
        }
        deleteCardPopWindow.show();
    }


    public void initPayPwdPopWindow() {

        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                unbindCardRequest();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    private void showPushSelDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(CardInfoActivity.this);

        //默认标题为解绑信用卡
        String title = getString(R.string.SMA2101_1_40);
        if (TextUtils.equals(cardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            //设置为解绑 我的账户
            title = getString(R.string.SMA2101_1_13);
        }

        String msg = getString(R.string.SMA2101_1_14);
        String str_ok = getString(R.string.SMA2101_1_16);
     /*   if (TextUtils.equals(cardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            title = getString(R.string.SMA2101_1_13);
        }*/
//        str_ok = getString(R.string.string_unlink);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton(str_ok, new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                initPayPwdPopWindow();
            }
        });
        CustomDialog mDealDialog = builder.create();
        mDealDialog.show();
    }

    @Override
    public void onPopWindowOkClickListener(boolean delete) {
        deleteCardPopWindow.dismiss();
        if (delete) {
            if (cardEntities.size() > 1) {
                initPayPwdPopWindow();
            } else {
                showPushSelDialog();
            }

        }
    }

    /**
     * 取消我的账户
     */
    @Override
    public void onPopWindowDeleteClickListener() {
        deleteCardPopWindow.dismiss();
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CARD_ENTITY, cardEntity);
        ActivitySkipUtil.startAnotherActivity(CardInfoActivity.this, DeleteSmartActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void onSelCardClickListener() {

    }

    @Override
    public void onDisMissClickListener() {

    }

    /**
     * 重新拉取一次卡列表
     */
    private void reloadBankcards() {
        CommonRequestUtils.getCardsList(this, HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(CardsEntity response) {
                ArrayList<BankCardEntity> rows = response.getRows();
                CacheManagerInstance.getInstance().setCardEntities(rows);
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_CARD_TYPE_UNBIND_CARD);
                ActivitySkipUtil.startAnotherActivity(CardInfoActivity.this, BindCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
            }
        });


    }

    private void unbindCardRequest() {
        final String cardType = cardEntity.getCardType();
        ApiProtocolImplManager.getInstance().getUnbindCard(this, cardEntity.getCardId() + "", defaultId, new NetWorkCallbackListener<UnlinkCardEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(CardInfoActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(UnlinkCardEntity response) {
                //1-表示退出应用
                if (TextUtils.equals(response.getLogout(), Constants.NO_CARD_EXIST)) {
                    String msg = getString(R.string.unlink_card_complete);
                    if (TextUtils.equals(cardType, SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
                        msg = getString(R.string.unlink_smart_account_complete);
                    }
                    showErrorMsgDialog(CardInfoActivity.this, msg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            //TODO fix zhangxinchao 用户登出应该要将MainActivity 干掉比较合理 需要优化
                            AndroidUtils.clearMemoryCache();
                            //为解决解绑 重新注册到首页停留在卡列表 这里需要保证停留在首页
//                            EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                            ActivitySkipUtil.startAnotherActivity(getActivity(), PreLoginActivity.class);
                        }
                    });
                } else {
                    reloadBankcards();
                }
            }
        });

    }

}
