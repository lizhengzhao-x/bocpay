package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class CreditCardBillEntity extends BaseEntity {


    private List<StmtTxnBean> stmtTxn;

    public List<StmtTxnBean> getStmtTxn() {
        return stmtTxn;
    }

    public void setStmtTxn(List<StmtTxnBean> stmtTxn) {
        this.stmtTxn = stmtTxn;
    }

    public static class StmtTxnBean {
        /**
         * chrgPymtLab : 100.00
         * subcardFg : 0
         * txnDt : 20370413
         * cur : HKD
         * conv : D
         * pstDt : 20370413
         * txnDesc : RETAIL TXN UAT FROM ASM
         */
        public String getUiType() {
            return uiType;
        }

        public void setUiType(String uiType) {
            this.uiType = uiType;
        }

        public String getUiTitle() {
            return uiTitle;
        }

        public void setUiTitle(String uiTitle) {
            this.uiTitle = uiTitle;
        }

        private String uiTitle;
        private String uiType;//ui界面使用区别是标题还是内容
        private String chrgPymtLab;
        private String subcardFg;
        private String txnDt;
        private String cur;
        private String conv;
        private String pstDt;
        private String txnDesc;

        public String getGpDetail() {
            return gpDetail;
        }

        public boolean isGpDetail() {
            if (TextUtils.isEmpty(gpDetail)) {
                return false;
            }
            return TextUtils.equals(gpDetail, "1");
        }

        public void setGpDetail(String gpDetail) {
            this.gpDetail = gpDetail;
        }

        private String gpDetail;
        public static final String UITYPE_MORE = "ADD_MORE";
        public static final String UITYPE_ITEM_MODE = "UITYPE_ITEM_MODE";

        public String getShowType() {
            return showType;
        }

        public void setShowType(String showType) {
            this.showType = showType;
        }

        private String showType;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        //-------------------------ui使用 信用卡分组-------------------------------
        private String cardId;
        private String cardName;

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getPanFour() {
            return panFour;
        }

        public void setPanFour(String panFour) {
            this.panFour = panFour;
        }

        public String getPanId() {
            return panId;
        }

        public void setPanId(String panId) {
            this.panId = panId;
        }

        private String panFour;
        private String panId;
        //-------------------------ui使用 信用卡分组-------------------------------

        public String getDiscount() {
            return discount;
        }

        public void setDiscount(String discount) {
            this.discount = discount;
        }

        private String discount;

        public String getChrgPymtLab() {
            return chrgPymtLab;
        }

        public void setChrgPymtLab(String chrgPymtLab) {
            this.chrgPymtLab = chrgPymtLab;
        }

        public String getSubcardFg() {
            return subcardFg;
        }

        public void setSubcardFg(String subcardFg) {
            this.subcardFg = subcardFg;
        }

        public String getTxnDt() {
            return txnDt;
        }

        public void setTxnDt(String txnDt) {
            this.txnDt = txnDt;
        }

        public String getCur() {
            return cur;
        }

        public void setCur(String cur) {
            this.cur = cur;
        }

        public String getConv() {
            return conv;
        }

        public void setConv(String conv) {
            this.conv = conv;
        }

        public String getPstDt() {
            return pstDt;
        }

        public void setPstDt(String pstDt) {
            this.pstDt = pstDt;
        }

        public String getTxnDesc() {
            return txnDesc;
        }

        public void setTxnDesc(String txnDesc) {
            this.txnDesc = txnDesc;
        }
    }
}
