package cn.swiftpass.wallet.intl.entity.event;


public class MainEventEntity extends BaseEventEntity{

    public static final int EVENT_KEY_HOME = 1001;
    public static final int EVENT_KEY_CLICK_HOME = 1008;
    public static final int EVENT_SHOW_HOMEPAGE = 1002;//显示首页
    public static final int EVENT_KEY_NOTIFIACTION_VIRTUALCARD_BIND = 1003;//通知推送-虚拟卡绑定
    public static final int EVENT_KEY_CLOSE_GPS_SERVICE = 1004;//关闭GPS 服务
    public static final int EVENT_KEY_CLOSE_LEFT_MENU = 1005;//关闭侧边栏
    public static final int EVENT_KEY_BIND_CARD_BACK_HOME = 1006;//绑卡并返回首页
    public static final int EVENT_KEY_START_TIMER = 1007;//开始资料区隐藏倒计时
    public static final int EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER = 1009;//home按键
    public static final int EVENT_KEY_REFRESH_ACCOUNT=1010;
    public static final int EVENT_KEY_END_TIMER = 1011;//资料区隐藏倒计时 结束
    public static final int EVENT_KEY_HIDE_ACCOUNT = 1012;//资料区隐藏

    public static final int EVENT_KEY_BIND_CARD = 1014;//绑卡

    public static final int EVENT_KEY_VIRTUAL_LIST_FAILED = 0x10210;// 绑卡确认页面 获取虚拟卡列表失败


    public MainEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public MainEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
