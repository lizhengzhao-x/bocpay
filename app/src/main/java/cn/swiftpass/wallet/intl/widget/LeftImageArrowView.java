package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * 左边文字 右边箭头布局封装
 */

public class LeftImageArrowView extends RelativeLayout {

    private Context mContext;
    private String leftText = "";
    /*   private int left_text_size =15;
       private int left_text_color = 0x24272B;
       private int left_text_margin_left = 5;*/
    private boolean showRightArrow;
    private boolean showLeftImage;
    private boolean showLine;

    public TextView getTvLeft() {
        return tvLeft;
    }

    public void setTvLeft(TextView tvLeft) {
        this.tvLeft = tvLeft;
    }

    private TextView tvLeft;
    private TextView mSubTextTV;

    public ImageView getImageRight() {
        return imageRight;
    }

    private ImageView imageRight;

    public ImageView getImageLeft() {
        return imageLeft;
    }

    private ImageView imageLeft;

    public View getLineView() {
        return lineView;
    }

    private View lineView;

    public LeftImageArrowView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public LeftImageArrowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public LeftImageArrowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_let_imageright, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ArrowRightView, defStyle, 0);

        leftText = a.getString(R.styleable.ArrowRightView_left_text);

        showRightArrow = a.getBoolean(R.styleable.ArrowRightView_show_right_arrow, true);
        showLeftImage = a.getBoolean(R.styleable.ArrowRightView_show_left_image, true);
        showLine = a.getBoolean(R.styleable.ArrowRightView_show_bottom_line, true);
        int d = a.getResourceId(R.styleable.ArrowRightView_right_image, R.mipmap.icon_button_nextxhdpi);

        tvLeft = rootView.findViewById(R.id.id_tv_left);
        lineView = rootView.findViewById(R.id.id_line);

        imageRight = rootView.findViewById(R.id.id_image_right);
        imageRight.setImageResource(d);

        imageLeft = rootView.findViewById(R.id.id_image_left);
        mSubTextTV = rootView.findViewById(R.id.tv_sub_txt);
        mSubTextTV.setVisibility(View.GONE);

        int leftImage = a.getResourceId(R.styleable.ArrowRightView_left_image, R.mipmap.icon_button_nextxhdpi);
        a.recycle();
        imageLeft.setImageResource(leftImage);
        tvLeft.setText(leftText);

        imageRight.setVisibility(showRightArrow ? VISIBLE : INVISIBLE);
        imageLeft.setVisibility(showLeftImage ? VISIBLE : GONE);
        lineView.setVisibility(showLine ? VISIBLE : INVISIBLE);
    }


    public void setLeftImageShow(boolean b) {
        imageLeft.setVisibility(b ? VISIBLE : INVISIBLE);
    }

    public void setLeftImageIsGone(boolean b) {
        imageLeft.setVisibility(b ? GONE : VISIBLE);
    }

    public void setRightImageShow(boolean b) {
        imageRight.setVisibility(b ? VISIBLE : INVISIBLE);
    }

    public void setSubText(String subText) {
        if (!TextUtils.isEmpty(subText)) {
            mSubTextTV.setVisibility(View.VISIBLE);
            mSubTextTV.setText(subText);
        }
    }
}
