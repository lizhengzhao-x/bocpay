package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.transfer.entity.GreetingTxtEntity;

/**
 * 派利是 转账金额
 */
public class TransferGreetingTxtAdapter implements AdapterItem<GreetingTxtEntity> {

    private TextView id_sel_text_money;
    private Context mContext;
    private int position;
    private GreetingTxtClickCallback greetingTxtClickCallback;

    public TransferGreetingTxtAdapter(Context contextIn, GreetingTxtClickCallback greetingTxtClickCallback) {
        mContext = contextIn;
        this.greetingTxtClickCallback = greetingTxtClickCallback;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.transfer_item_greeting_txt;
    }

    @Override
    public void bindViews(View root) {
        id_sel_text_money = root.findViewById(R.id.id_transfer_greeting_txt);

    }

    @Override
    public void setViews() {
        id_sel_text_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (greetingTxtClickCallback != null) {
                    greetingTxtClickCallback.onItemClick(position);
                }
            }
        });
    }

    @Override
    public void handleData(GreetingTxtEntity itemMoney, int positionIn) {
        position = positionIn;
        id_sel_text_money.setText(itemMoney.getTitleStr());
        id_sel_text_money.setBackgroundResource(itemMoney.isSel() ? R.drawable.bg_btn_transfer_type_sel : R.drawable.bg_btn_transfer_type);
        id_sel_text_money.setTextColor(itemMoney.isSel() ? Color.WHITE : mContext.getResources().getColor(R.color.app_red));
    }

    public static class GreetingTxtClickCallback {

        public void onItemClick(int position) {
            // do nothing
        }

    }
}
