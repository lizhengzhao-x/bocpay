package cn.swiftpass.wallet.intl.widget;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;


public class CustomNoScrollLinearLayoutManager extends LinearLayoutManager {
    public CustomNoScrollLinearLayoutManager(Context context) {
        super(context);
    }

    public CustomNoScrollLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }


    @Override
    public boolean canScrollVertically() {
        return false;
    }
}
