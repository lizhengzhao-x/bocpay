package cn.swiftpass.wallet.intl.module.register.model;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class BindNewCardListEntity extends BaseEntity {
    private ArrayList<BindNewCardEntity> cardInfoList = new ArrayList<>();

    public ArrayList<BindNewCardEntity> getCardInfoList() {
        return cardInfoList;
    }

    public void setCardInfoList(ArrayList<BindNewCardEntity> cardInfoList) {
        this.cardInfoList = cardInfoList;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;
}
