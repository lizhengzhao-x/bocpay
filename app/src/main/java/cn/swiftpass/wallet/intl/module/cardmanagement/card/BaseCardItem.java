package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import cn.swiftpass.httpcore.entity.BankCardEntity;

abstract class BaseCardItem {
    public final long id;
    public final BankCardEntity bankCardEntity;

    public BaseCardItem(long id, BankCardEntity bankCardEntity) {
        this.id = id;
        this.bankCardEntity = bankCardEntity;
    }
}
