package cn.swiftpass.wallet.intl.module.setting;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.gson.JsonSyntaxException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.setting.notify.view.NotificationSettingTransparentActivity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.CircleWebview;

/**
 * Created by zhangxinchao on 2018/2/27.
 */

public class WebViewActivity extends BaseWebViewActivity {

    public static final String NEW_MSG = "new_msg";
    public static final String SHARE_CONTENT = "share_content";
    public static final String METHOD_UPLAN = "CallUplan";
    public static final String SHOW_TOOLBAR_COLOSE_IMG = "CallUplan";
    @BindView(R.id.webView)
    CircleWebview webView;


    private ValueCallback<Uri[]> mValueCallback;
    /**
     * 是否为首页最新消息 如果是 展示分享按钮
     */
    private boolean isNewMsg;

    /**
     * 是否显示右上角关闭图标
     */
    private boolean isShowRightMoreImg = true;
    private String shareContent;

    private ImageView shareIv = null;

//    @Override
//    protected boolean notUsedToolbar() {
//        return true;
//    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            mOriginalUrl = intent.getStringExtra(Constants.DETAIL_URL);
            isNewMsg = intent.getBooleanExtra(NEW_MSG, false);
            shareContent = intent.getStringExtra(SHARE_CONTENT);
            isShowRightMoreImg = intent.getBooleanExtra(SHOW_TOOLBAR_COLOSE_IMG, true);
        }
        if (TextUtils.isEmpty(mOriginalUrl)) {
            return;
        }
        shareIv.setVisibility(View.INVISIBLE);
        if (isNewMsg) {
            if (!TextUtils.isEmpty(shareContent)) {
                //图片地址不为空，才需要显示分享按钮
                shareIv.setVisibility(View.VISIBLE);
                shareIv.setOnClickListener(new OnProhibitFastClickListener() {
                    @Override
                    public void onFilterClick(View v) {
                        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_NEWS_SHARE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                        sendAnalysisEvent(analysisButtonEntity);
                        shareImageUrl(shareContent);
                    }
                });
            }
        }
        getCloseView().setVisibility(isShowRightMoreImg ? View.VISIBLE : View.INVISIBLE);
        String content = intent.getExtras().getString(Constants.DETAIL_TITLE);
        boolean isVideoPlay = intent.getExtras().getBoolean(Constants.IS_VIDEO_PLAY, false);
        if (!isVideoPlay) {
            //视频播放 硬件加速问题
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        final boolean isOpenErrorHint = intent.getExtras().getBoolean(Constants.WEB_VIEW_ERROR_IS_HINT, false);
        if (!TextUtils.isEmpty(content)) {
            setToolBarTitle(content);
        }
        if (TextUtils.isEmpty(mOriginalUrl)) {
            return;
        }
        setWebViewClient(webView, isOpenErrorHint, true);
        initWebView(webView, mOriginalUrl);
    }

    @Override
    public void init() {
        super.init();
        EventBus.getDefault().register(this);
        if (getIntent() != null) {
            mOriginalUrl = getIntent().getStringExtra(Constants.DETAIL_URL);
            isNewMsg = getIntent().getBooleanExtra(NEW_MSG, false);
            shareContent = getIntent().getStringExtra(SHARE_CONTENT);
            isShowRightMoreImg = getIntent().getBooleanExtra(SHOW_TOOLBAR_COLOSE_IMG, true);
        }
        if (TextUtils.isEmpty(mOriginalUrl)) {
            finish();
            return;
        }
        android.webkit.WebView.enableSlowWholeDocumentDraw();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.rightMargin = AndroidUtils.dip2px(this, 20);
        shareIv = addToolBarRightViewToImage(R.mipmap.icon_share, layoutParams);
        shareIv.setVisibility(View.INVISIBLE);
        if (isNewMsg) {
            if (!TextUtils.isEmpty(shareContent)) {
                //图片地址不为空，才需要显示分享按钮
                shareIv.setVisibility(View.VISIBLE);
                shareIv.setOnClickListener(new OnProhibitFastClickListener() {
                    @Override
                    public void onFilterClick(View v) {
                        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_NEWS_SHARE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                        sendAnalysisEvent(analysisButtonEntity);
                        shareImageUrl(shareContent);
                    }
                });
            }
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            webView.addJavascriptInterface(new WebViewActivity.JsInterface(), METHOD_UPLAN);
        }
        getCloseView().setVisibility(isShowRightMoreImg ? View.VISIBLE : View.INVISIBLE);
        String content = getIntent().getExtras().getString(Constants.DETAIL_TITLE);
        boolean isVideoPlay = getIntent().getExtras().getBoolean(Constants.IS_VIDEO_PLAY, false);
        if (!isVideoPlay) {
            //视频播放 硬件加速问题
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        final boolean isOpenErrorHint = getIntent().getExtras().getBoolean(Constants.WEB_VIEW_ERROR_IS_HINT, false);
        if (!TextUtils.isEmpty(content)) {
            setToolBarTitle(content);
        }
        if (TextUtils.isEmpty(mOriginalUrl)) {
            finish();
        }
        setWebViewClient(webView, isOpenErrorHint, true);
        initWebView(webView, mOriginalUrl);
    }

    class JsInterface {

        @JavascriptInterface
        public void postMessage(String msg) {
            try {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    ApiProtocolImplManager.getInstance().getUplanInfo(msg, new NetWorkCallbackListener<UplanUrlEntity>() {
                        @Override
                        public void onFailed(String errorCode, String errorMsg) {
                            dismissDialog();
                            showErrorMsgDialog(mContext, msg);
                        }

                        @Override
                        public void onSuccess(UplanUrlEntity response) {
                            dismissDialog();
                            if (response != null) {
                                if (CacheManagerInstance.getInstance().isLogin()) {
                                    HashMap<String, Object> mHashMaps = new HashMap<>();
                                    mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
                                    mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
                                    mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
                                    mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
                                    ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }
                            }
                        }
                    });
                } else {
                    ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }

            } catch (JsonSyntaxException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }

        @JavascriptInterface
        public void CallDirectMarketingSetting() {
            ActivitySkipUtil.startAnotherActivity(getActivity(), NotificationSettingTransparentActivity.class);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_webview;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Uri[] results = new Uri[]{intent.getData()};
        if (mValueCallback != null) {
            mValueCallback.onReceiveValue(results);
            mValueCallback = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (webView != null) {
            webView.destroy();
            webView = null;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UplanEntity event) {
        if (event.getEventType() == UplanEntity.EVENT_UPLAN_BACK_SCAN) {
            finish();
        }
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {

        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }


}
