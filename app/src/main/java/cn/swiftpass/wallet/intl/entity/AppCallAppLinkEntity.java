package cn.swiftpass.wallet.intl.entity;


import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class AppCallAppLinkEntity extends BaseEntity {


    public static final String APP_LINK_TYPE = "APP_LINK_TYPE";//APP_LINK_TYPE
    public static final String APP_LINK_DATA = "APP_LINK_DATA";//APP_LINK_DATA
    public static final String APP_LINK_URL = "APP_LINK_URL";//APP_LINK_URL
    public static final String TYPE_CREATE_QRCODE = "1";//被扫
    public static final String TYPE_SCAN_QRCODE = "2";//主扫
    public static final String TYPE_ECOUPON = "3";//电子券
    public static final String TYPE_WEB_VIEW = "99";//跳转页面
    public static final String TYPE_APPCALLAPP = "100";//跳转页面

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    public String getJumpType() {
        return jumpType;
    }

    public void setJumpType(String jumpType) {
        this.jumpType = jumpType;
    }


    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    private String appLink;

    public String getMerchantLink() {
        return merchantLink;
    }

    public void setMerchantLink(String merchantLink) {
        this.merchantLink = merchantLink;
    }

    private String merchantLink;

    private String jumpType;

    public String getNetworkErrorLink() {
        return networkErrorLink;
    }

    public void setNetworkErrorLink(String networkErrorLink) {
        this.networkErrorLink = networkErrorLink;
    }

    private String networkErrorLink;
    /**
     * app call app 支持h5 H5 APP
     */
    private String fromType;

    public boolean isAppCallAppFromH5() {
        if (TextUtils.isEmpty(fromType)){
            return false;
        }
        return TextUtils.equals(fromType,"H5");
    }

    private String qrCode;

    public AppCallAppLinkEntity(String appLink) {
        this.appLink = appLink;
    }

    public AppCallAppLinkEntity() {
    }
}
