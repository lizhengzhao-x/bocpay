package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.module.register.contract.OtpAction;

public class SendOtpBaseProtocol extends BaseProtocol {
    public SendOtpBaseProtocol(int action, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        switch (action) {
            case OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT:
                mUrl = "api/smartBind/sentOtp";
                break;
            case OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER:
                mUrl = "api/smartReg/sentOtp";
                break;
            case OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD:
                mUrl = "api/smartForgot/sentOtp";
                break;
            default:
                break;
        }

    }
}
