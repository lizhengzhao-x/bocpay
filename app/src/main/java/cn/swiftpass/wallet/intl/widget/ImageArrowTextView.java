package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * 左边文字 右边箭头布局 箭头左边输入框封装
 */

public class ImageArrowTextView extends RelativeLayout {

    private Context mContext;
    private String leftText = "";
    private String rightText = "";
    private boolean showRightArrow;
    private boolean showLine;

    public TextView getTvLeft() {
        return tvLeft;
    }


    public String getRightText() {
        return mEditRight.getText().toString().trim();
    }


    private TextView tvLeft;

    public TextView getmEditRight() {
        return mEditRight;
    }

    private TextView mEditRight;

    public ImageView getImageRight() {
        return imageRight;
    }

    private ImageView imageRight;

    public ImageArrowTextView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public ImageArrowTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public ImageArrowTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_left_right_text, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ArrowRightTextView, defStyle, 0);

        leftText = a.getString(R.styleable.ArrowRightTextView_left_text);
        rightText = a.getString(R.styleable.ArrowRightTextView_right_text);
        showRightArrow = a.getBoolean(R.styleable.ArrowRightTextView_show_right_arrow, true);
        showLine = a.getBoolean(R.styleable.ArrowRightTextView_show_bottom_line, true);
        int d = a.getResourceId(R.styleable.ArrowRightTextView_right_image, R.mipmap.icon_button_nextxhdpi);
        boolean editAble = a.getBoolean(R.styleable.ArrowRightTextView_right_editable, true);
        a.recycle();

        tvLeft = rootView.findViewById(R.id.id_tv_left);
        View lineView = rootView.findViewById(R.id.id_line);

        imageRight = rootView.findViewById(R.id.id_image_right);
        imageRight.setImageResource(d);


        mEditRight = rootView.findViewById(R.id.id_tv_right);
        mEditRight.setEnabled(editAble);
        mEditRight.setText(rightText);

        tvLeft.setText(leftText);
        imageRight.setVisibility(showRightArrow ? VISIBLE : INVISIBLE);
        lineView.setVisibility(showLine ? VISIBLE : INVISIBLE);
    }


    public void setRightImageShow(boolean b) {
        imageRight.setVisibility(b ? VISIBLE : INVISIBLE);
    }

    public void setRightEditTextShow(boolean b) {
        mEditRight.setVisibility(b ? VISIBLE : INVISIBLE);
    }

    public void setRightEditTextEnabled(boolean b) {
        mEditRight.setEnabled(b);
    }

    public void setRightImage(int resourceId) {
        imageRight.setImageResource(resourceId);
    }

    public void setLeftTextTitle(String title) {
        tvLeft.setText(title);
    }

    public void setRightTextTitle(String title) {
        mEditRight.setText(title);
    }
}
