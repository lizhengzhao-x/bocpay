package cn.swiftpass.wallet.intl.sdk.sa;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * 埋点:点击事件
 */
public class AnalysisButtonEntity extends AnalysisBaseEntity {

    /**
     * 当前按钮id (新增字段，需要和大数据平台确认)
     */
    public String action_id;

    /**
     * 用户进入当前按钮所在页面时间，格式yyyy-MM-dd HH:mm:ss
     */
    public String date_time;

    /**
     * 按钮所在页面上一页面id，参考表1-1 中指定页面id(可能为空)
     */
    public String last_address;

    /**
     * 当前页面id
     */
    public String current_address;

    /**
     * 在該按鈕或頁面停留時間 (mm:ss)
     */
    public String time_duration;

    public AnalysisButtonEntity() {
        super();
    }

    public AnalysisButtonEntity(String action_id, long date_time, String last_address, String current_address, long time_duration) {
        super();
        this.action_id = action_id;
        this.date_time = longToStr(date_time, "yyyy-MM-dd HH:mm:ss");
        this.last_address = TextUtils.isEmpty(last_address) ? "" : last_address;
        this.current_address = current_address;
        this.time_duration = longToStr(time_duration, "mm:ss");
    }

    @Override
    public String getDataType() {
        return AnalysisDataType.ACTION_CLI_POINT;
    }

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = super.getJsonObject();
        jsonObject.put(AnalysisParams.ACTION_ID, action_id);
        jsonObject.put(AnalysisParams.DATE_TIME, date_time);
        jsonObject.put(AnalysisParams.LAST_ADDRESS, last_address);
        jsonObject.put(AnalysisParams.CURRENT_ADDRESS, current_address);
        jsonObject.put(AnalysisParams.TIME_DURATION, time_duration);
        return jsonObject;
    }
}
