package cn.swiftpass.wallet.intl.module.rewardregister.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class CheckRegistrationStatusProtocol extends BaseProtocol {

    String cardType;

    String campaignId;

    public CheckRegistrationStatusProtocol(String cardType, String campaignId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/creditCardReward/checkRegistrationStatus";
        this.cardType = cardType;
        this.campaignId = campaignId;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARDNO, cardType);
        mBodyParams.put(RequestParams.CAMPAIGNID, campaignId);
    }
}
