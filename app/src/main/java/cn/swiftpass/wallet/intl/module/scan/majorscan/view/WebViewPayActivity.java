package cn.swiftpass.wallet.intl.module.scan.majorscan.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.greenrobot.eventbus.EventBus;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.OrderDetailGradeDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentListDialogFragment;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.WebViewPayContract;
import cn.swiftpass.wallet.intl.module.scan.majorscan.presenter.WebViewPayPresenter;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.SettingCardDefaultAcitivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.utils.WalletConfig;
import cn.swiftpass.wallet.intl.widget.CircleWebview;
import cn.swiftpass.wallet.intl.widget.ProgressWebView;

/**
 * 扫微信商户二维码
 */
public class WebViewPayActivity extends BaseWebViewActivity implements WebViewPayContract.View {

    private final String TAG = "WebViewPayActivity";
    private String REDIRECTURL = "redirectUrl";
    private String PAYORDERINFO = "pay_order_info";
    private String PAY_URL = "https://qr.95516.com/qrcGtwWeb-web/api/pay?";
    @BindView(R.id.webView)
    CircleWebview webView;

    private String walletId, mCardId, mOriginalUrl, queryTaxId;
    private ActionTrxGpInfoEntity actionTrxGpInfoEntity;

    private Handler mQueryHandler;
    /**
     * 订单详情 收银台
     */
    private OrderDetailGradeDialogFragment orderDetailDialogFragment;
    /**
     * 强制停止订单查询
     */
    private boolean isForceStopQuery;
    /**
     * 订单支付完成之后 倒计时查询结果
     */
    private Timer queryOrderResultTimer;

    private int retainQueryTime;

    private WebViewPayContract.Presenter mPresenterDetail;



    /**
     * 轮训查询订单状态
     */
    private Runnable reQurtQrStatusRunnable = new Runnable() {
        @Override
        public void run() {
            if (!isForceStopQuery) {
                mPresenterDetail.getPayMentResult(mCardId, mOriginalUrl, queryTaxId);
            }
        }
    };
    /**
     * 支付里欸包
     */
    private PaymentListDialogFragment paymentListDialogFragment;

    /**
     * 轮询插曲订单结果
     */
    private TimerTask queryOrderResultTimerTask = new TimerTask() {
        @Override
        public void run() {
            retainQueryTime--;
            if (retainQueryTime < 0) {
                //查询倒计时结束 弹框提示退出
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isForceStopQuery = true;
                            queryOrderResultTimer.cancel();
                            showManualCheckDialog();
                        }
                    });
                }
            }
        }
    };

    @Override
    public void init() {
        super.init();
        getToolBarRightView().setVisibility(View.INVISIBLE);
        if (mPresenter instanceof WebViewPayContract.Presenter) {
            mPresenterDetail = (WebViewPayContract.Presenter) mPresenter;
        }
        if (getIntent() != null && getIntent().getExtras() != null) {
//            String lan = SpUtils.getInstance(mContext).getAppLanguage();
//
//            switchLanguage(lan);
            mOriginalUrl = getIntent().getExtras().getString(Constants.DETAIL_URL);
            String content = getIntent().getExtras().getString(Constants.DETAIL_TITLE);
            walletId = getIntent().getExtras().getString(Constants.WALLETID);
            mCardId = getIntent().getExtras().getString(Constants.CARDID);
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            queryOrderResultTimer = new Timer();
            retainQueryTime = WalletConfig.getMajorScanQueryQRCodeResultDelayTime();
            mQueryHandler = new Handler();
            if (!TextUtils.isEmpty(content)) {
                setToolBarTitle(content);
            }
            if (TextUtils.isEmpty(mOriginalUrl)) {
                finish();
            }

            initCusWebView(webView, mOriginalUrl);

            setWebViewClient(webView);
        } else {
            finish();
        }
    }

    private void initCusWebView(ProgressWebView webView, String url) {
        LogUtils.i(TAG, "original url :" + url);
        WebSettings settings = webView.getSettings();
        //User-Agent=UnionPay/<1.0> <WalletID>
        String ua = String.format("UnionPay/1.0 %s", walletId);
        settings.setUserAgentString(ua);
        initWebView(webView, url,true, null);
    }

    private void setWebViewClient(final WebView webView) {

        setWebViewClient(webView, true,false, new OnUrlVerifyCallBack() {
            @Override
            public void OnUrlVerifyFinish(String url, boolean isSuccess) {
                if (!TextUtils.isEmpty(url)) {
                    if (url.contains(REDIRECTURL)) {
                        //https://qr.95516.com/qrcGtwWebweb/api/userAuth?version=1.0.0&redirectUrl=xxxx
                        int subStr = url.indexOf(REDIRECTURL + "=");
                        String redirectUrlStr = url.substring(subStr + (REDIRECTURL + "=").length());
                        mPresenterDetail.generateAuthToken(mCardId, URLDecoder.decode(redirectUrlStr));
                    } else if (url.contains(PAYORDERINFO)) {
                        String redirectUrlStr = url.substring(url.indexOf(PAYORDERINFO + "="));
                        mPresenterDetail.actExtQrInfo(redirectUrlStr, mCardId);
                    } else if (url.contains(PAY_URL)) {
//                        url =  URLDecoder.decode(url);
                        String redirectUrlStr = url.substring(PAY_URL.length());
                        mPresenterDetail.actExtQrInfo(redirectUrlStr, mCardId);
                    } else {
                        webView.loadUrl(url);
                    }
                }
            }
        });
//        webView.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                LogUtils.i(TAG, "url:" + url + "\n " + webView.getUrl());
//                if (!TextUtils.isEmpty(url)) {
//                    if (url.contains(REDIRECTURL)) {
//                        //https://qr.95516.com/qrcGtwWebweb/api/userAuth?version=1.0.0&redirectUrl=xxxx
//                        int subStr = url.indexOf(REDIRECTURL + "=");
//                        String redirectUrlStr = url.substring(subStr + (REDIRECTURL + "=").length());
//                        mPresenterDetail.generateAuthToken(mCardId, URLDecoder.decode(redirectUrlStr));
//                    } else if (url.contains(PAYORDERINFO)) {
//                        String redirectUrlStr = url.substring(url.indexOf(PAYORDERINFO + "="));
//                        mPresenterDetail.actExtQrInfo(redirectUrlStr, mCardId);
//                    } else if (url.contains(PAY_URL)) {
////                        url =  URLDecoder.decode(url);
//                        String redirectUrlStr = url.substring(PAY_URL.length());
//                        mPresenterDetail.actExtQrInfo(redirectUrlStr, mCardId);
//                    } else {
//                        view.loadUrl(url);
//                    }
//                }
//                return true;
//            }
//        });
    }

    /**
     * 轮训结果出错
     */
    private void showManualCheckDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.string_failed_to_query));
        builder.setMessage(getString(R.string.check_in_transaction));
        builder.setPositiveButton(getString(R.string.string_btn_check), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE,""));
//                if (getActivity() != null) {
//                    getActivity().finish();
//                }
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = getActivity();
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    /**
     * 吊起收银台 后期要全部换成这种方式
     */
    private void showCheckStand(final ActionTrxGpInfoEntity actionTrxGpInfoEntity) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        this.mCardId = actionTrxGpInfoEntity.getCardId();
        if (orderDetailDialogFragment == null) {
            orderDetailDialogFragment = new OrderDetailGradeDialogFragment();
            OrderDetailGradeDialogFragment.OnDialogClickListener onDialogClickListener = new OrderDetailGradeDialogFragment.OnDialogClickListener() {
                @Override
                public void onOkListener(boolean isPoint) {
                    if (!ButtonUtils.isFastDoubleClick()) {
                        inoutPswPopWindow();
                    }
                }

                @Override
                public void onSelCardListener() {
                    mPresenterDetail.getPaymentTypeList(mOriginalUrl);
                }

                @Override
                public void onDisMissListener() {
                    orderDetailDialogFragment = null;
                    finish();
                }
            };
            orderDetailDialogFragment.initParams(actionTrxGpInfoEntity, actionTrxGpInfoEntity.canChangeCard(), onDialogClickListener);
            orderDetailDialogFragment.show(getSupportFragmentManager(), "OrderDetailGradeDialogFragment");
        } else {
            orderDetailDialogFragment.updateParams(actionTrxGpInfoEntity);
        }
    }

    /**
     * 严密逻辑
     */
    public void inoutPswPopWindow() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                submitOrderInfoRequest();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    /**
     * 提交订单信息
     */
    private void submitOrderInfoRequest() {
        MainScanOrderRequestEntity orderRequest = new MainScanOrderRequestEntity();
        orderRequest.setTrxAmt(actionTrxGpInfoEntity.getTranAmt());
        orderRequest.setQrcType(getIntent().getExtras().getString(Constants.QRCTYPE));
        orderRequest.setTrxFeeAmt("0");
        orderRequest.setMpQrCode(mOriginalUrl);
        orderRequest.setCardId(mCardId);
        orderRequest.setMerchantName(actionTrxGpInfoEntity.getName());
        orderRequest.setTxnCurr(actionTrxGpInfoEntity.getTranCur());
        orderRequest.setTxnId(actionTrxGpInfoEntity.getTranID());
        mPresenterDetail.getMainScanOrder(orderRequest);
    }

    @Override
    public void getMainScanOrderSuccess(ContentEntity response) {
        String tansId = response.getResult_msg();
        queryOrderStatus(tansId);
    }


    /**
     * 轮询订单状态
     *
     * @param tansId
     */
    private void queryOrderStatus(String tansId) {
        queryTaxId = tansId;
        //开始倒计时查询结果
        queryOrderResultTimer.schedule(queryOrderResultTimerTask, 0, 1000);
        showDialogNotCancel(getActivity());
        mPresenterDetail.getPayMentResult(mCardId, mOriginalUrl, tansId);
    }


    /**
     * 下单轮训接口成功
     *
     * @param response
     */
    @Override
    public void getPayMentResultSuccess(PaymentEnquiryResult response) {
        if (response == null) {
            mQueryHandler.postDelayed(reQurtQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
            return;
        }
        if (isForceStopQuery) {
            return;
        }
        if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_SUCCESS)) {
            mQueryHandler.removeCallbacks(reQurtQrStatusRunnable);
            queryOrderResultTimer.cancel();
            //支付成功
            dismissDialog();
            GradeReduceSuccessActivity.startActivityFromMajorScan(getActivity(), response);
            finish();
        } else if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_NOTPAY)) {
            mQueryHandler.postDelayed(reQurtQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
        }
    }

    @Override
    public void getPayMentResultError(String errorCode, String errorMsg) {
        dismissDialog();
        mQueryHandler.removeCallbacks(reQurtQrStatusRunnable);
        queryOrderResultTimer.cancel();
    }


    /**
     * 支付列表
     *
     * @param bankCardEntities
     */
    private void showPaymentListDialog(final ArrayList<BankCardEntity> bankCardEntities) {
        paymentListDialogFragment = new PaymentListDialogFragment();
        PaymentListDialogFragment.OnPaymentListClickListener onPaymentListClickListener = new PaymentListDialogFragment.OnPaymentListClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onMoreBtnClickListener() {
                ActivitySkipUtil.startAnotherActivity(getActivity(), SettingCardDefaultAcitivity.class);
            }

            @Override
            public void onCardSelListener(int positon) {
                //切换卡号
                mCardId = bankCardEntities.get(positon).getCardId();
                orderDetailDialogFragment.setDefaultCardId(bankCardEntities.get(positon));
            }
        };
        paymentListDialogFragment.initParams(onPaymentListClickListener, bankCardEntities, orderDetailDialogFragment.getCurrentSelCardId(), orderDetailDialogFragment.isFpsPaymentType());
        paymentListDialogFragment.show(getSupportFragmentManager(), "paymentListDialogFragment");
    }


    /**
     * 主扫下单报错
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getMainScanOrderError(final String errorCode, final String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_webview_pay;
    }

    @Override
    protected WebViewPayContract.Presenter createPresenter() {
        return new WebViewPayPresenter();
    }

    @Override
    public void generateAuthTokenError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });

    }

    @Override
    public void generateAuthTokenSuccess(String url) {
        webView.loadUrl(URLDecoder.decode(url));
    }

    @Override
    public void actExtQrInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    public void actExtQrInfoSuccess(ActionTrxGpInfoEntity content) {
        //吊起收银台
        showCheckStand(content);
    }


    @Override
    public void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards) {
        showPaymentListDialog(bankCards);
    }

    @Override
    public void getPaymentTypeListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }


}
