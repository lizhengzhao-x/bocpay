package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * RecycleView 公用配置
 */
public abstract class BaseTransferSelFragment<T,K extends IPresenter> extends BaseFragment<K> {

    /**
     * 是一般的转账 还是 派利是的转账
     */
    protected boolean isTransferNormalType;


    protected View emptyView;
    protected ImageView noDataImg;
    protected TextView noDataText;
    protected LinearLayoutManager layoutManager;

    protected void initRecycleView(RecyclerView recyclerView, final SwipeRefreshLayout swipeRefreshLayout, BaseRecyclerAdapter adapter) {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateItemList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.app_dark_red);

        emptyView = LayoutInflater.from(getContext()).inflate(R.layout.view_top_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(R.string.TR1_2b_1));
        emptyView.setVisibility(View.GONE);
        noDataImg.setImageResource(R.mipmap.icon_transaction_noresult);
        adapter.setEmptyView(emptyView);
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    protected View getEmptyView() {
        return emptyView;
    }

    protected void setEmptyText(int resId) {
        if (noDataText == null) {
            return;
        }
        noDataText.setText(resId);
    }

    protected void setEmptyImg(int resId) {
        if (noDataImg == null) {
            return;
        }
        noDataImg.setImageResource(resId);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment()!=null&&getParentFragment() instanceof GetTransferSelTypeListener){
            GetTransferSelTypeListener getTransferSelTypeListener = (GetTransferSelTypeListener) getParentFragment();
            isTransferNormalType = getTransferSelTypeListener.isNormalTransferType();
        }

    }

    protected void startAnotherActivity(final Activity activity, final boolean isTransferNormalType, final String phone, final String name, final String relName, final boolean isEmail) {
        ApiProtocolImplManager.getInstance().checkSmartAccountNewBind(getActivity(), true, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
                    if (isTransferNormalType) {
                        AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                verifyPwdToUpdateAccount();
                            }
                        });
                    } else {
                        AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_13), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                verifyPwdToUpdateAccount();
                            }
                        });
                    }
                } else {
                    showErrorMsgDialog(getActivity(), errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                TransferUtils.transferWithPhoneNumber(activity, isTransferNormalType, phone, name, relName, isEmail);
            }
        });
    }

    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false,new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (getActivity() == null) {
                    return;
                }
                 HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                    }
                },300);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {

                    }
                });
            }

            @Override
            public void onVerifyCanceled() {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE,""));
            }
        });
    }

    public abstract void updateItemList();

    protected abstract void updateContactList(ArrayList<ContactEntity> contactEntities, boolean obtain);
}
