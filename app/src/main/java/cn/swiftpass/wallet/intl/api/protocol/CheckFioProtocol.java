package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author create date on  on 2018/7/30 13:50
 * 设置FIO时，先获得账户信息，区分智账和信用卡
 */

public class CheckFioProtocol extends BaseProtocol {
    public static final String TAG = CheckFioProtocol.class.getSimpleName();

    public CheckFioProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/checkFIO";
    }
}
