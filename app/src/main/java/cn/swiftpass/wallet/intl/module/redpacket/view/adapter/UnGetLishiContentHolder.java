package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.RedPacketDialogUtils;

public class UnGetLishiContentHolder extends RecyclerView.ViewHolder {
    private final Activity activity;
    @BindView(R.id.lly_get_lishi_un_open_contain)
    LinearLayout llyGetLishiUnOpenContain;
    @BindView(R.id.lly_get_lishi_one_bg)
    LinearLayout llyGetLishiOneBg;
    @BindView(R.id.tv_get_lishi_title_one_name)
    TextView tvGetLishiTitleOneName;
    @BindView(R.id.tv_get_lishi_title_two_name)
    TextView tvGetLishiTitleTwoName;
    @BindView(R.id.tv_get_lishi_title_one_flag)
    TextView tvGetLishiTitleOneFlag;
    @BindView(R.id.tv_get_lishi_title_two_flag)
    TextView tvGetLishiTitleTwoFlag;

    @BindView(R.id.tv_get_lishi_open_all)
    TextView tvGetLishiOpenAll;
    @BindView(R.id.tv_get_lishi_title_one)
    TextView tvGetLishiTitleOne;
    @BindView(R.id.tv_get_lishi_open_one)
    TextView tvGetLishiOpenOne;
    @BindView(R.id.iv_getlishi_one_arrow)
    ImageView ivGetlishiOneArrow;
    @BindView(R.id.iv_getlishi_one_bottom_bg)
    ImageView ivGetlishiOneBottomBg;
    @BindView(R.id.tv_get_lishi_title_two)
    TextView tvGetLishiTitleTwo;
    @BindView(R.id.tv_get_lishi_open_two)
    TextView tvGetLishiOpenTwo;
    @BindView(R.id.lly_get_lishi_two_bg)
    LinearLayout llyGetLishiTwoBg;
    @BindView(R.id.iv_getlishi_two_arrow)
    ImageView ivGetlishiTwoArrow;
    @BindView(R.id.iv_getlishi_two_bottom_bg)
    ImageView ivGetlishiTwoBottomBg;
    @BindView(R.id.tv_get_lishi_un_open_empty)
    TextView TvGetLishiUnOpenEmpty;

    public UnGetLishiContentHolder(Activity activity, View contentHolder) {
        super(contentHolder);
        this.activity = activity;
        ButterKnife.bind(this, itemView);
   /*     int childTotalWidth = 0;
        int childTotalHeight = 0;
        int[] widthHeight = AndroidUtils.getRedWidthHeight(activity);
        if (null != widthHeight) {
            childTotalWidth = AndroidUtils.getRedWidthHeight(activity)[0];
            childTotalHeight = AndroidUtils.getRedWidthHeight(activity)[1];
        }

        //设置控件等比宽高
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) llyGetLishiOneBg.getLayoutParams();
        params.width = childTotalWidth;
        params.height = childTotalHeight;
        setLlyWidthHeight(llyGetLishiOneBg, childTotalWidth, childTotalHeight);
        setBottomWidthHeight(ivGetlishiOneBottomBg,childTotalWidth);
        setLlyWidthHeight(llyGetLishiTwoBg, childTotalWidth, childTotalHeight);
        setBottomWidthHeight(ivGetlishiTwoBottomBg,childTotalWidth);*/
        tvGetLishiOpenAll.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvGetLishiOpenAll.getPaint().setAntiAlias(true);//抗锯齿
    }

    /**
     * 设置红包bottomView的宽高
     */
    private void setBottomWidthHeight(ImageView imageView, int width) {
        //设置控件等比宽高
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        params.height = width * 111 / 318;
        params.width = width;
        imageView.setLayoutParams(params);
    }


    /**
     * 设置红包的宽高
     *
     * @param linearLayout
     * @param width
     * @param height
     */
    private void setLlyWidthHeight(LinearLayout linearLayout, int width, int height) {
        //设置控件等比宽高
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
        params.width = width;
        params.height = height;
        linearLayout.setLayoutParams(params);
    }


    public void setData(GetLiShiHomeEntity homeEntity) {

        llyGetLishiUnOpenContain.setVisibility(View.GONE);
        TvGetLishiUnOpenEmpty.setVisibility(View.GONE);
        if (homeEntity != null && null != homeEntity.getReceivedRecordsTurnOff()
                && homeEntity.getReceivedRecordsTurnOff().size() > 0) {
            llyGetLishiUnOpenContain.setVisibility(View.VISIBLE);
            tvGetLishiOpenAll.setVisibility(View.VISIBLE);
        } else {
            TvGetLishiUnOpenEmpty.setVisibility(View.VISIBLE);
            tvGetLishiOpenAll.setVisibility(View.GONE);
        }

        if (homeEntity != null && homeEntity.getReceivedRecordsTurnOff() != null && homeEntity.getReceivedRecordsTurnOff().size() > 0) {
            List<GetLiShiHomeEntity.ReceivedRecordsTurnOffEntity> receivedRecordsTrunOffEntities = homeEntity.getReceivedRecordsTurnOff();

            llyGetLishiOneBg.setVisibility(View.GONE);
            llyGetLishiTwoBg.setVisibility(View.GONE);
            if (receivedRecordsTrunOffEntities.size() == 1) {
                llyGetLishiOneBg.setVisibility(View.VISIBLE);
                setGetLishiUnOpen(receivedRecordsTrunOffEntities.get(0), llyGetLishiOneBg, ivGetlishiOneArrow, ivGetlishiOneBottomBg, tvGetLishiTitleOneName);
                setTextColor(tvGetLishiTitleOne, tvGetLishiTitleOneFlag, tvGetLishiOpenOne, receivedRecordsTrunOffEntities.get(0).getBgColor());
            } else {
                llyGetLishiOneBg.setVisibility(View.VISIBLE);
                llyGetLishiTwoBg.setVisibility(View.VISIBLE);
                setTextColor(tvGetLishiTitleOne, tvGetLishiTitleOneFlag, tvGetLishiOpenOne, receivedRecordsTrunOffEntities.get(0).getBgColor());
                setTextColor(tvGetLishiTitleTwo, tvGetLishiTitleTwoFlag, tvGetLishiOpenTwo, receivedRecordsTrunOffEntities.get(1).getBgColor());
                setGetLishiUnOpen(receivedRecordsTrunOffEntities.get(0), llyGetLishiOneBg, ivGetlishiOneArrow, ivGetlishiOneBottomBg, tvGetLishiTitleOneName);
                setGetLishiUnOpen(receivedRecordsTrunOffEntities.get(1), llyGetLishiTwoBg, ivGetlishiTwoArrow, ivGetlishiTwoBottomBg, tvGetLishiTitleTwoName);
            }
        }

        tvGetLishiOpenOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == homeEntity) {
                    return;
                }
                if (null == homeEntity.getReceivedRecordsTurnOff()) {
                    return;
                }
                if (homeEntity.getReceivedRecordsTurnOff().size() < 1) {
                    return;
                }
                if (null == activity) {
                    return;
                }
                RedPacketDialogUtils.openRedPacket(activity, homeEntity.getReceivedRecordsTurnOff().get(0).getSrcRefNo(), Constants.RED_PACKET_COMON_TYPE);

            }
        });


        tvGetLishiOpenTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == homeEntity) {
                    return;
                }
                if (null == homeEntity.getReceivedRecordsTurnOff()) {
                    return;
                }

                if (homeEntity.getReceivedRecordsTurnOff().size() < 2) {
                    return;
                }
                if (null == activity) {
                    return;
                }

                RedPacketDialogUtils.openRedPacket(activity, homeEntity.getReceivedRecordsTurnOff().get(1).getSrcRefNo(), Constants.RED_PACKET_COMON_TYPE);

            }
        });
        tvGetLishiOpenAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == activity) {
                    return;
                }
                RedPacketDialogUtils.openRedPacket(activity, "", Constants.RED_PACKET_ALL_TYPE);

            }
        });


    }


    /**
     * @param receivedRecordsTrunOffEntity
     * @param llyBg
     * @param centerView
     * @param bottomView
     * @param tvName
     */
    private void setGetLishiUnOpen(GetLiShiHomeEntity.ReceivedRecordsTurnOffEntity receivedRecordsTrunOffEntity, View llyBg, View centerView, ImageView bottomView, TextView tvName) {
        if (null == receivedRecordsTrunOffEntity) {
            return;
        }

        String releaName = receivedRecordsTrunOffEntity.getName();
        ContactCompareUtils.compareHisotryLocalRecord(activity, receivedRecordsTrunOffEntity.getPhone(), receivedRecordsTrunOffEntity.getEmail(), releaName, new ContactCompareUtils.CompareSingleResult() {
            @Override
            public void getStringName(String name) {
                tvName.setText(AndroidUtils.getLitterRedNameEllipStr(name));
            }
        });
        //llyBg.setVisibility(View.INVISIBLE);
        llyBg.setBackgroundResource(R.mipmap.bg_smallcny_styledefauly_bg);
        GlideApp.with(activity).load(receivedRecordsTrunOffEntity.getCoverUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        llyBg.setBackgroundDrawable(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        llyBg.setBackgroundResource(R.mipmap.bg_smallcny_styledefauly_bg);
                    }
                });

        GlideApp.with(activity).load(receivedRecordsTrunOffEntity.getBottomImageUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        //llyBg.setVisibility(View.VISIBLE);
                        bottomView.setImageDrawable(resource);
                    }
                });

        GlideApp.with(activity).load(receivedRecordsTrunOffEntity.getCenterImageUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        centerView.setBackground(resource);
                    }
                });

    }


    private void setTextColor(TextView textView1, TextView textView2, TextView textViewButton, String color) {
        textView1.setTextColor(Color.parseColor(color));
        textView2.setTextColor(Color.parseColor(color));


        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);//形状
        gradientDrawable.setCornerRadius(AndroidUtils.dip2px(activity, 30));//设置圆角Radius
        gradientDrawable.setColor(Color.parseColor(color));//颜色

        textViewButton.setBackground(gradientDrawable);//设置为background
    }

}
