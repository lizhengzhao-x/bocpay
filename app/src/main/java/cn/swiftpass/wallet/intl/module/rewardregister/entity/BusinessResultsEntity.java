package cn.swiftpass.wallet.intl.module.rewardregister.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author lizheng.zhao
 */
public class BusinessResultsEntity extends BaseEntity {

    private CreditCardRewardInfo creditCardRewardInfo;

    public void setCreditCardRewardInfo(CreditCardRewardInfo creditCardRewardInfo) {
        this.creditCardRewardInfo = creditCardRewardInfo;
    }

    public CreditCardRewardInfo getCreditCardRewardInfo() {
        return creditCardRewardInfo;
    }

    public class CreditCardRewardInfo extends BaseEntity {

        private String activityUrl;
        private String bankCardType;
        private String campaignId;
        private String explain;
        private String imagesUrl;
        private String termsAndConditionsUrl;
        private String description;

        public void setActivityUrl(String activityUrl) {
            this.activityUrl = activityUrl;
        }

        public String getActivityUrl() {
            return activityUrl;
        }

        public void setBankCardType(String bankCardType) {
            this.bankCardType = bankCardType;
        }

        public String getBankCardType() {
            return bankCardType;
        }

        public void setCampaignId(String campaignId) {
            this.campaignId = campaignId;
        }

        public String getCampaignId() {
            return campaignId;
        }

        public void setExplain(String explain) {
            this.explain = explain;
        }

        public String getExplain() {
            return explain;
        }

        public void setImagesUrl(String imagesUrl) {
            this.imagesUrl = imagesUrl;
        }

        public String getImagesUrl() {
            return imagesUrl;
        }

        public void setTermsAndConditionsUrl(String termsAndConditionsUrl) {
            this.termsAndConditionsUrl = termsAndConditionsUrl;
        }

        public String getTermsAndConditionsUrl() {
            return termsAndConditionsUrl;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

    }

}
