package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.base.password.VerifyPwdPresenter;
import cn.swiftpass.wallet.intl.base.password.VerifyPwdView;

/**
 * 电子券管理
 */
public class RedemPtionManagerContract {

    public interface View extends IView, VerifyPwdView<Presenter> {


    }


    public interface Presenter extends IPresenter<RedemPtionManagerContract.View>, VerifyPwdPresenter<RedemPtionManagerContract.View> {
    }

}
