package cn.swiftpass.wallet.intl.entity;


import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class RedPackageEntity extends BaseEntity {

    private String pushRrcRefNo;

    private String pushRedPacketType;

    //STAFF表示员工利是，COMMON表示普通利是
    public String getPushRedPacketType() {
        return pushRedPacketType;
    }

    public void setPushRedPacketType(String pushRedPacketType) {
        this.pushRedPacketType = pushRedPacketType;
    }

    public String getPushRrcRefNo() {
        return pushRrcRefNo;
    }

    public void setPushRrcRefNo(String pushRrcRefNo) {
        this.pushRrcRefNo = pushRrcRefNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    private String type;

    public String getTitle() {
        if (TextUtils.isEmpty(title)) {
            return "";
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    private String subTitle;
    private String sound;
    private String logoUrl;

    public MessageContent getContent() {
        return content;
    }

    public void setContent(MessageContent content) {
        this.content = content;
    }

    private MessageContent content;

    public static class MessageContent {

    }
}
