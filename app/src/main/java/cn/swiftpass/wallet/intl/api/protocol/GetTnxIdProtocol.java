package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 注册获取tnx id
 */
public class GetTnxIdProtocol extends BaseProtocol {

    public static final String TAG = GetTnxIdProtocol.class.getSimpleName();
    String phoneNumber;

    public GetTnxIdProtocol(String type, String phoneNumber, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.phoneNumber = phoneNumber;
        //不同流程 后台区分 ur不同
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/getTxnId";
        } else {
            mUrl = "api/smartReg/getTxnId";
        }
    }


    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(phoneNumber)) {
            mBodyParams.put(RequestParams.MOBILE, phoneNumber);
        }
    }

}
