package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;


public class CustomNotificationDialog extends Dialog {

    private static ImageView mLoadImageView;
    private static View mId_root_dialog_progress;

    public CustomNotificationDialog(Context context) {
        super(context);
    }

    public CustomNotificationDialog(Context context, int theme) {
        super(context, theme);
    }

    public void showProgressDialog() {
        if (mId_root_dialog_progress == null || mLoadImageView == null) return;
        mId_root_dialog_progress.setVisibility(View.VISIBLE);
        AnimationDrawable animationDrawable = (AnimationDrawable) mLoadImageView.getBackground();
        animationDrawable.start();
    }

    public void dismissProgressDialog() {
        if (mId_root_dialog_progress == null || mLoadImageView == null) return;
        AnimationDrawable animationDrawable = (AnimationDrawable) mLoadImageView.getBackground();
        animationDrawable.stop();
        mId_root_dialog_progress.setVisibility(View.GONE);
    }

    public static class Builder {
        private Context context;
        private OnClickListener positiveButtonClickListener;
        private OnClickListener negativeButtonClickListener;


        public Builder(Context context) {
            this.context = context;
        }


        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }


        /**
         * Set the positive button resource and it's listener
         *
         * @return
         */
        public Builder setPositiveButton(OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }


        public Builder setNegativeButton(OnClickListener listener) {
            this.negativeButtonClickListener = listener;
            return this;
        }


        public CustomNotificationDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final CustomNotificationDialog dialog = new CustomNotificationDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_notifacation_msg_layout, null);
            dialog.setContentView(layout);

            mLoadImageView = (ImageView) layout.findViewById(R.id.loadingImageView);
            mId_root_dialog_progress = (View) layout.findViewById(R.id.id_root_dialog_progress);

            WindowManager.LayoutParams dialogParams = dialog.getWindow().getAttributes();
            dialogParams.width = (int) (AndroidUtils.getScreenWidth(context) * 0.8);
            dialogParams.height = WindowManager.LayoutParams.WRAP_CONTENT;

            TextView openNotification = layout.findViewById(R.id.tv_allow_notification);
            TextView exitNotification = layout.findViewById(R.id.tv_exit);
            openNotification.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (positiveButtonClickListener != null) {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                    }
                }
            });
            exitNotification.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (negativeButtonClickListener != null) {
                        negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                    }
                }
            });
            dialog.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            return dialog;
        }


    }
}

