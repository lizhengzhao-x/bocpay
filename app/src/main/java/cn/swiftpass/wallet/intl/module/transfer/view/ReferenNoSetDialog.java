package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter;
import cn.swiftpass.wallet.intl.widget.ClearEditText;

import static cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter.CHARSEQUENCE;


public class ReferenNoSetDialog extends Dialog {

    private static String mReferenceNoStr;

    public String getReferenceNoStr() {
        return mReferenceNoStr;
    }


    public ReferenNoSetDialog(Context context) {
        super(context);
    }

    public ReferenNoSetDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private OnClickListener positiveButtonClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setPositiveListener(OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public ReferenNoSetDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final ReferenNoSetDialog dialog = new ReferenNoSetDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_set_referenceno_layout, null);
            final Button posBtn = layout.findViewById(R.id.positive_dl);
            Button negBtn = layout.findViewById(R.id.negative_dl);
            final ClearEditText amountET = layout.findViewById(R.id.et_amount);
            amountET.setInputType(InputType.TYPE_CLASS_TEXT);
            amountET.setFocusable(true);
            amountET.setFocusableInTouchMode(true);
            amountET.requestFocus();

            amountET.setFilters(new InputFilter[]{new InputFilter.LengthFilter(35)});
            negBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            //默认不可选中
            posBtn.setEnabled(false);
            amountET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    boolean isEnable = amountET.getText().toString().length() > 0;
                    posBtn.setEnabled(isEnable);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            posBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mReferenceNoStr = amountET.getText().toString();
                    if (positiveButtonClickListener != null) {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                    }
                    dialog.dismiss();
                }
            });
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

