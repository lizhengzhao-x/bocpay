package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.ActionQueryGpEcoupInfoProtocol;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.api.ActRedmIdProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.api.CheckCCardProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionDetailContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.ActRedmIdEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * 换领电子券P
 */
public class RedemptionDetailPresenter extends BasePresenter<RedemPtionDetailContract.View> implements RedemPtionDetailContract.Presenter {

    /**
     * 获取账户积分
     */
    @Override
    public void getUserTotalGrade() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ActionQueryGpEcoupInfoProtocol(null, new NetWorkCallbackListener<CreditCardGradeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().showTotalGradeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CreditCardGradeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().showTotalGradeSuccess(response);
                }
            }
        }).execute();

    }

    /**
     * 验证电子券库存
     */
    @Override
    public void redeemGiftCheckStock(String giftTp, List<RedeemResponseEntity.ResultRedmBean> redmBeansIn) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckCCardProtocol(giftTp, redmBeansIn, new NetWorkCallbackListener<CheckCcEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkCCardError("", errorMsg);
                }
            }

            @Override
            public void onSuccess(CheckCcEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkCCardSuccess(response);
                }
            }
        }).execute();
    }


    @Override
    public void actRedmId(EcouponConvertEntity ecouponConvertEntity, String giftTp) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ActRedmIdProtocol(giftTp, ecouponConvertEntity, new NetWorkCallbackListener<ActRedmIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().actRedmIdError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ActRedmIdEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().actRedmIdSuccess(response);
                }
            }
        }).execute();
    }
}
