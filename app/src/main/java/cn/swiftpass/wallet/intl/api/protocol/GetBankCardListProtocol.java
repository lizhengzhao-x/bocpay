package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardListEntity;

public class GetBankCardListProtocol extends BaseProtocol {


    public GetBankCardListProtocol(NetWorkCallbackListener<BindNewCardListEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/register/queryCreditAndVirtualCardList";

    }

    @Override
    public void packData() {
        super.packData();
    }
}
