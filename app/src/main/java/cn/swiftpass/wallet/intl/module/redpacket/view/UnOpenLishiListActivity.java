package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.redpacket.contract.UnOpenLiShiListContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.UnOpenLiShiListEntity;
import cn.swiftpass.wallet.intl.module.redpacket.presenter.UnOpenLiShiListPresenter;
import cn.swiftpass.wallet.intl.module.redpacket.view.adapter.UnOpenLishiListAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;
import cn.swiftpass.wallet.intl.utils.RedPacketDialogUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import me.jessyan.autosize.utils.AutoSizeUtils;

/**
 * 未拆的利是 记录
 */
public class UnOpenLishiListActivity extends BaseCompatActivity<UnOpenLiShiListContract.Presenter> implements UnOpenLiShiListContract.View {
    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;
    @BindView(R.id.ry_un_open_lisi_list)
    RecyclerView ryUnOpenLisiList;
    @BindView(R.id.tv_get_lishi_open_all)
    TextView tvGetLishiOpenAll;
    @BindView(R.id.ll_empty)
    LinearLayout llEmpty;

    private UnOpenLishiListAdapter lishiHistoryAdapter;
    private List<UnOpenLiShiListEntity.RowsBean> items;

    /**
     * 当前页面  默认为第一页
     */
    private int mCurrentPage = 1;
    /**
     * 每一页数据  默认为30条
     */
    private int mPageSize = 30;

    private String nextTimeStamp = "";

    UnOpenLiShiListEntity unOpenLiShiListEntity;

    @Override
    protected int getLayoutId() {
        return R.layout.act_un_open_lisi_list;
    }


    @Override
    protected void init() {
        EventBus.getDefault().register(this);
        setToolBarTitle(getString(R.string.RP2101_23_3));

        //swipeRefreshLayout.setRefreshing(true);

        initRecyCleView();

        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                nextTimeStamp = "";
                //清除通讯录
                ContactCompareUtils.clearAllContactList();
                mPresenter.getUnOpenRedPacket(mCurrentPage, mPageSize, nextTimeStamp);
            }
        });

        mainRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                if (null != items && items.size() > 0) {
                    nextTimeStamp = items.get(items.size() - 1).getSendTime() + "";
                }

                mPresenter.getUnOpenRedPacket(mCurrentPage, mPageSize, nextTimeStamp);
            }
        });
        mainRefreshLayout.setEnableAutoLoadMore(true);
        mPresenter.getUnOpenRedPacket(mCurrentPage, mPageSize, nextTimeStamp);

        lishiHistoryAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (baseRecyclerAdapter.getDataList().size() == 0) {
                    return;
                }
                final UnOpenLiShiListEntity.RowsBean rowsBean = (UnOpenLiShiListEntity.RowsBean) baseRecyclerAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.tv_get_lishi_open:
                        RedPacketDialogUtils.openRedPacket(getActivity(), rowsBean.getSrcRefNo(), Constants.RED_PACKET_COMON_TYPE);
                        break;

                    default:
                        break;
                }
            }
        });


        tvGetLishiOpenAll.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvGetLishiOpenAll.getPaint().setAntiAlias(true);//抗锯齿
    }

    private void initRecyCleView() {
        GridLayoutManager mLayoutManager = new GridLayoutManager(mContext, 3, RecyclerView.HORIZONTAL, false);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ryUnOpenLisiList.setLayoutManager(mLayoutManager);

        //item的个数
        int spanCount = 3;
        //每个item的间隔
        int itemSpacing = AutoSizeUtils.dp2px(this, 12);
        //最外层的边距
        int edgeMargin = AutoSizeUtils.dp2px(this, 17);
        int childTotalWidth = 0;
        int childTotalHeight = 0;
        int[] widthHeight = AndroidUtils.getRedWidthHeight(mContext);
        if (null != widthHeight) {
            childTotalWidth = AndroidUtils.getRedWidthHeight(mContext)[0];
            childTotalHeight = AndroidUtils.getRedWidthHeight(mContext)[1];
        }

        items = new ArrayList<>();
        lishiHistoryAdapter = new UnOpenLishiListAdapter(items, this, childTotalWidth, childTotalHeight);
        lishiHistoryAdapter.bindToRecyclerView(ryUnOpenLisiList);
        lishiHistoryAdapter.loadMoreEnd(true);
        ryUnOpenLisiList.addItemDecoration(new GridSpacingEqualDecoration(this, 3, AutoSizeUtils.dp2px(this, 12), AutoSizeUtils.dp2px(this, 17)));
    }

    @OnClick({R.id.tv_get_lishi_open_all})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) return;
        switch (view.getId()) {
            case R.id.tv_get_lishi_open_all:
                RedPacketDialogUtils.openRedPacket(getActivity(), "", Constants.RED_PACKET_ALL_TYPE);
                break;
        }
    }

    @Override
    public void getUnOpenRedPacketSuccess(UnOpenLiShiListEntity response) {

        unOpenLiShiListEntity = response;
        initData();
        if (mainRefreshLayout != null) {
            mainRefreshLayout.finishRefresh();
            mainRefreshLayout.finishLoadMore();
        }
    }

    @Override
    public void getUnOpenRedPacketError(String errorCode, String errorMsg) {
        if (mainRefreshLayout != null) {
            mainRefreshLayout.finishRefresh();
            mainRefreshLayout.finishLoadMore();
        }
    }

    @Override
    public void turnOnRedPacketSuccess(GetLiShiHomeEntity response) {

    }

    @Override
    public void turnOnRedPacketError(String errorCode, String errorMsg) {

    }

    private void initData() {
        if (null == unOpenLiShiListEntity) {
            return;
        }

        //没有nextTimeStamp 就是第一页
        if (TextUtils.isEmpty(nextTimeStamp)) {
            items.clear();
        }


        ContactCompareUtils.compareUnOpenListHisotryLocalRecord(this, unOpenLiShiListEntity.getRows(), new ContactCompareUtils.CompareRowsResult() {
            @Override
            public void getRedPackets(List<UnOpenLiShiListEntity.RowsBean> rowsBeans) {
                if (null != lishiHistoryAdapter) {
                    items.addAll(rowsBeans);
                    lishiHistoryAdapter.setDataList(items);
                }
            }
        });


        /**
         * 当前页数都是1 如果总页数比1小 就表示刷新到了最后一页
         */
        if (mCurrentPage >= unOpenLiShiListEntity.getPages()) {
            mainRefreshLayout.setEnableLoadMore(false);
        } else {
            mainRefreshLayout.setEnableLoadMore(true);
        }

        if (items.size() == 0) {
            llEmpty.setVisibility(View.VISIBLE);
            tvGetLishiOpenAll.setVisibility(View.GONE);
        } else {
            llEmpty.setVisibility(View.GONE);
            tvGetLishiOpenAll.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected UnOpenLiShiListContract.Presenter createPresenter() {
        return new UnOpenLiShiListPresenter();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == TransferEventEntity.EVENT_RED_PACKET_OPEN) {
            String refId = event.getEventMessage();

            /**
             * 表示打开全部
             */
            if (TextUtils.isEmpty(refId)) {
                items.clear();
            } else {
                int flagIndex = -1;
                for (int i = 0; i < items.size(); i++) {
                    if (TextUtils.equals(items.get(i).getSrcRefNo(), refId)) {
                        flagIndex = i;
                        break;
                    }
                }
                if (flagIndex >= 0) {
                    items.remove(flagIndex);
                }
            }
            lishiHistoryAdapter.setDataList(items);

            if (items.size() == 0) {
                llEmpty.setVisibility(View.VISIBLE);
                tvGetLishiOpenAll.setVisibility(View.GONE);
            } else {
                llEmpty.setVisibility(View.GONE);
                tvGetLishiOpenAll.setVisibility(View.VISIBLE);
            }
        }
    }


}
