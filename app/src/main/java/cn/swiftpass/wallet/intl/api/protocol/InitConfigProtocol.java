package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author create date on  on 2018/7/30 13:50
 * 初始化配置
 */

public class InitConfigProtocol extends BaseProtocol {

    @Override
    public boolean isNeedLogin() {
        return false;
    }

    public InitConfigProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/sysconfig/initConfig";
    }
}
