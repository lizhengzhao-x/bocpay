package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.os.Build;

import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * 权限获取单例类
 */
public class PermissionInstance {

    public boolean isHasFirstRejectContacts() {
        return hasFirstRejectContacts;
    }

    public void setHasFirstRejectContacts(boolean hasFirstRejectContacts) {
        this.hasFirstRejectContacts = hasFirstRejectContacts;
    }

    private boolean hasFirstRejectContacts;

    private PermissionInstance() {

    }

    private static class PermissonHolder {
        public static PermissionInstance permissionInstance = new PermissionInstance();
    }

    public static PermissionInstance getInstance() {
        return PermissonHolder.permissionInstance;
    }


    public void getStoreImagePermission(final Activity mActivity, final PermissionCallback permissionCallback, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            permissionCallback.acceptPermission();
        } else {
            new RxPermissions(mActivity).request(permissions).subscribe(new io.reactivex.functions.Consumer<Boolean>() {
                @Override
                public void accept(Boolean isAccept) throws Exception {
                    //是否获取系统权限
                    if (isAccept.booleanValue()) {
                        permissionCallback.acceptPermission();
                    } else {
                        //拒绝获取系统权限  会弹出app本地权限框
                        permissionCallback.rejectPermission();
                    }
                }
            });
        }
    }

    /**
     * 获取系统权限方法
     *
     * @param mActivity
     * @param permissions        多个权限string 例如 读取联系人权限 Manifest.permission.READ_CONTACTS
     * @param permissionCallback
     */
    public void getPermission(final Activity mActivity, final PermissionCallback permissionCallback, String... permissions) {
        new RxPermissions(mActivity).request(permissions).subscribe(new io.reactivex.functions.Consumer<Boolean>() {
            @Override
            public void accept(Boolean isAccept) throws Exception {
                //是否获取系统权限
                if (isAccept.booleanValue()) {
                    permissionCallback.acceptPermission();
                } else {
                    //拒绝获取系统权限  会弹出app本地权限框
                    permissionCallback.rejectPermission();
                }
            }
        });
    }

    public interface PermissionCallback {
        /**
         * 允许获取系统权限
         */
        void acceptPermission();

        /**
         * 拒绝获取系统权限
         */
        void rejectPermission();
    }
}
