package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class CheckHKCustEntity extends BaseEntity {

    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsHKCust() {
        return isHKCust;
    }

    public void setIsHKCust(String isHKCust) {
        this.isHKCust = isHKCust;
    }

    private String isHKCust;
}
