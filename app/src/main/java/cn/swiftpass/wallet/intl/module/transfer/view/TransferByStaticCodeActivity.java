package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.utils.QRCodeUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByStaticContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.TransferSetLimitEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferByStaticPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

/**
 * Created by aijingya on 2018/7/27.
 *
 * @Package cn.swiftpass.wallet.intl.activity
 * @Description: 收款码页面
 * @date 2018/7/27.14:24.
 */

public class TransferByStaticCodeActivity extends BaseCompatActivity<TransferByStaticContract.Presenter> implements TransferByStaticContract.View {

    private static final int REQ_SET_LIMIT = 1001;
    public static final String DATA_SET_LIMIT = "DATA_SET_LIMIT";
    public static final String DATA_SET_AMOUNT_LIMIT = "DATA_SET_AMOUNT_LIMIT";
    @BindView(R.id.iv_static_code)
    ImageView ivStaticCode;
    @BindView(R.id.ll_save_image)
    LinearLayout llSaveImage;
    @BindView(R.id.ll_msg)
    LinearLayout llMsg;
    @BindView(R.id.id_email)
    TextView idEmail;
    @BindView(R.id.id_phone)
    TextView idPhone;
    @BindView(R.id.tv_tag)
    TextView tagTv;
    @BindView(R.id.tv_msg)
    TextView msgTv;
    @BindView(R.id.tv_currency)
    TextView currencyTv;
    @BindView(R.id.tv_transfer_amount)
    TextView transferAmountTv;
    @BindView(R.id.line_msg)
    View msgLine;
    @BindView(R.id.tv_account_info)
    TextView tvAccountInfo;


    @BindView(R.id.tv_save_image)
    TextView tvSaveImage;

    @BindView(R.id.view_line)
    View lineView;


    @BindView(R.id.cl_line)
    ConstraintLayout lineLayout;


    private StaticCodeEntity staticCodeEntity;
    private boolean isShowEmail = true;
    private List<StaticCodeEntity.MobnQRBean> emalQRBeans;
    private List<StaticCodeEntity.MobnQRBean> mobnQRBeans;
    private String currentQrcode;
    private TransferSetLimitEntity dataSetLimit;
    private String remarks;
    private Bitmap bitmapEmpty;
    private CustomDialog mDealDialog;
    /**
     * 当前是在哪个界面设置金额 默认显示手机号
     */
    private boolean isStillPhonePage = true;
    private boolean isInLeft = true;


    @Override
    protected TransferByStaticContract.Presenter createPresenter() {
        return new TransferByStaticPresenter();
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_B0_10);
        bitmapEmpty = Bitmap.createBitmap(AndroidUtils.dip2px(getActivity(), 200), AndroidUtils.dip2px(getActivity(), 200), Bitmap.Config.RGB_565);
        bitmapEmpty.eraseColor(Color.parseColor("#f5f5f5"));
        //updateUI();
        idEmail.setTextColor(getResources().getColor(R.color.hint_color));
        idPhone.setTextColor(getResources().getColor(R.color.app_black));

        tvSaveImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                saveImgToLocal();
            }
        });
        getTransferFoundQrcode();
    }

    private void getTransferFoundQrcode() {
        String transferAmount = transferAmountTv.getText().toString();
        if (TextUtils.isEmpty(transferAmount)) {
            mPresenter.pullQRCode();
        } else {
            //离线断网问题
            String remarkTv = msgTv.getText().toString();
            mPresenter.updateQRCode(transferAmount, remarkTv);
        }
    }


    private void updateUI() {
        if (staticCodeEntity != null) {
            emalQRBeans = staticCodeEntity.getEmalQR();
            mobnQRBeans = staticCodeEntity.getMobnQR();
        } else {
            emalQRBeans = null;
            mobnQRBeans = null;
        }

        if (mobnQRBeans != null && mobnQRBeans.size() > 0 && isStillPhonePage) {
            isShowEmail = false;
            isStillPhonePage = true;
        } else if (emalQRBeans != null && emalQRBeans.size() > 0 && !isStillPhonePage) {
            isShowEmail = true;
        } else {
            isShowEmail = true;
        }

        //需要切换到显示手机号
        if (isStillPhonePage) {
            if (mobnQRBeans != null && mobnQRBeans.size() > 0) {
                showPhoneImage();
                showTab(false);
            } else if (emalQRBeans != null && emalQRBeans.size() > 0) {
                showEmailImage();
                showTab(true);
            } else {
                showMessageDialog(getString(R.string.CO1_2a_9), false);
            }
        } else {
            //需要切换到邮箱
            if (emalQRBeans != null && emalQRBeans.size() > 0) {
                showEmailImage();
                showTab(true);
            } else if (mobnQRBeans != null && mobnQRBeans.size() > 0) {
                showPhoneImage();
                showTab(false);
            } else {
                tvAccountInfo.setText("");
                showMessageDialog(getString(R.string.CO1_2a_8), true);
            }
        }

    }

    public void showTab(boolean isShowEmail) {
        if (isShowEmail) {
            animToRight();
        } else {
            animToLeft();
        }
        idEmail.setTextColor(isShowEmail ? getResources().getColor(R.color.app_black) : getResources().getColor(R.color.hint_color));
        idPhone.setTextColor(isShowEmail ? getResources().getColor(R.color.hint_color) : getResources().getColor(R.color.app_black));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        forbidScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
    }

    @Override
    protected void onPause() {
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmapEmpty != null && !bitmapEmpty.isRecycled()) {
            bitmapEmpty.recycle();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_by_static_code;
    }


    @OnClick({R.id.id_email, R.id.id_phone, R.id.tv_share, R.id.ll_set_limit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_email:
                if (isShowEmail) {
                    return;
                }
                isShowEmail = true;
                showTab(true);
                if (emalQRBeans == null && mobnQRBeans == null) {
                    getTransferFoundQrcode();
                    isStillPhonePage = false;
                    showEmailImage();
                    return;
                }
                if (emalQRBeans == null || emalQRBeans.size() == 0) {
                    showMessageDialog(getString(R.string.CO1_2a_8), true);
                    ivStaticCode.setImageBitmap(bitmapEmpty);
                    currentQrcode = "";
                    //如果两个码都为空的时候（比如上次超时了），再去拉取一次
                    if (mobnQRBeans == null || mobnQRBeans.isEmpty()) {
                        getTransferFoundQrcode();
                    }
                    return;
                }
                showEmailImage();
                break;
            case R.id.id_phone:
                if (!isShowEmail) {
                    return;
                }
                isShowEmail = false;
                showTab(false);
                if (emalQRBeans == null && mobnQRBeans == null) {
                    getTransferFoundQrcode();
                    isStillPhonePage = true;
                    showPhoneImage();
                    return;
                }
                if (mobnQRBeans == null || mobnQRBeans.isEmpty()) {
                    showMessageDialog(getString(R.string.CO1_2a_9), false);
                    ivStaticCode.setImageBitmap(bitmapEmpty);
                    currentQrcode = "";
                    if (emalQRBeans == null || emalQRBeans.isEmpty()) {
                        getTransferFoundQrcode();
                    } else {
                        showTab(true);
                        showEmailImage();
                    }
                    return;
                }
                showPhoneImage();
                break;
            case R.id.tv_share:
                shareImgToOther();
                break;
            case R.id.ll_set_limit:
                if (ButtonUtils.isFastDoubleClick()) return;
                if (staticCodeEntity != null) {
                    Intent intent = new Intent(TransferByStaticCodeActivity.this, TransferSetLimitActivity.class);
                    if (dataSetLimit != null) {
                        intent.putExtra(DATA_SET_LIMIT, dataSetLimit);
                    }
                    if (isShowEmail && staticCodeEntity != null && staticCodeEntity.getEmalQR() != null && staticCodeEntity.getEmalQR().size() > 0) {
                        intent.putExtra(DATA_SET_AMOUNT_LIMIT, staticCodeEntity.getEmalQR().get(0).getOutBalLmt());
                    } else if (staticCodeEntity != null && staticCodeEntity.getMobnQR() != null && staticCodeEntity.getMobnQR().size() > 0) {
                        intent.putExtra(DATA_SET_AMOUNT_LIMIT, staticCodeEntity.getMobnQR().get(0).getOutBalLmt());
                    }
                    startActivityForResult(intent, REQ_SET_LIMIT);
                } else {
                    getTransferFoundQrcode();
                }
                break;

//            case R.id.tv_save_image:
//                saveImgToLocal();
//                break;
            default:
                break;
        }
    }

    private void saveImgToLocal() {
        if (TextUtils.isEmpty(currentQrcode)) {
            return;
        }
        PermissionInstance.getInstance().getStoreImagePermission(TransferByStaticCodeActivity.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                saveImageToGalley(currentQrcode);
            }

            @Override
            public void rejectPermission() {
                showLackOfPermissionDialog();
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void shareImgToOther() {
        if (TextUtils.isEmpty(currentQrcode)) {
            return;
        }
        if (ButtonUtils.isFastDoubleClick()) return;
        PermissionInstance.getInstance().getStoreImagePermission(TransferByStaticCodeActivity.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                shareImage(currentQrcode);
            }

            @Override
            public void rejectPermission() {
                showLackOfPermissionDialog();
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    public void animToRight() {
        if (isInLeft) {
            isInLeft = false;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineLayout.getPaddingLeft(), lineView.getWidth());
            animator.setDuration(200);
            animator.start();
        }

    }

    public void animToLeft() {
        if (!isInLeft) {
            isInLeft = true;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineView.getWidth(), 0);
            animator.setDuration(200);
            animator.start();
        }

    }

    private void showPhoneImage() {
        isStillPhonePage = true;
        isShowEmail = false;
        if (mobnQRBeans != null) {
            currentQrcode = mobnQRBeans.get(0).getQrCode();
            Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrcode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_logo), this);
            ivStaticCode.setImageBitmap(bitmapTmp);
            String accountInfo = AndroidUtils.getPrimaryAccountDisplay(mobnQRBeans.get(0).getAcTp(), mobnQRBeans.get(0).getAcNo());
            tvAccountInfo.setText(accountInfo);
        } else {
            tvAccountInfo.setText("");
        }
        if (staticCodeEntity != null && !TextUtils.isEmpty(mobnQRBeans.get(0).getProxyId())) {
            tagTv.setText(mobnQRBeans.get(0).getProxyId());
        } else {
            tagTv.setText("");
        }
    }

    private void showEmailImage() {
        //if (TextUtils.isEmpty(currentQrcode)) return;
        isStillPhonePage = false;
        isShowEmail = true;
        if (emalQRBeans != null) {
            currentQrcode = emalQRBeans.get(0).getQrCode();
            Bitmap bitmap = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrcode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_logo), this);
            ivStaticCode.setImageBitmap(bitmap);
            String accountInfo = AndroidUtils.getPrimaryAccountDisplay(emalQRBeans.get(0).getAcTp(), emalQRBeans.get(0).getAcNo());
            tvAccountInfo.setText(accountInfo);
        } else {
            tvAccountInfo.setText("");
        }
        if (staticCodeEntity != null && !TextUtils.isEmpty(emalQRBeans.get(0).getProxyId())) {
            tagTv.setText(emalQRBeans.get(0).getProxyId());
        } else {
            tagTv.setText("");
        }
    }

    private void showMessageDialog(String content, final boolean isEmailTab) {
        String title = "   ";
        String msg = content;
        String left = getString(R.string.EC01_20);
        String right = getString(R.string.CO1_5_3);
        DialogInterface.OnClickListener rightListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isEmailTab) {
                    showTab(false);
                    showPhoneImage();
                } else {
                    showTab(true);
                    showEmailImage();
                }
                ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class);
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (isEmailTab) {
                    showTab(false);
                    showPhoneImage();
                } else {
                    showTab(true);
                    showEmailImage();
                }
                //TODO 是否要回到首页

            }
        };
        AndroidUtils.showTipDialog(TransferByStaticCodeActivity.this, title, msg, right, left, rightListener, cancelListener);
    }


    private void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(TransferByStaticCodeActivity.this);
        builder.setTitle(getString(R.string.SH2_4_1a));
        builder.setMessage(getString(R.string.SH2_4_1b));
        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(TransferByStaticCodeActivity.this);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    private void shareImage(String qrcode) {
        if (TextUtils.isEmpty(currentQrcode)) {
            return;
        }
        View saveQrInfoV = LayoutInflater.from(mContext).inflate(R.layout.include_save_qrcode, null);
        ImageView qrcodeImg = saveQrInfoV.findViewById(R.id.img_qrcode);
        TextView remarksTv = saveQrInfoV.findViewById(R.id.tv_remarks);
        TextView moneyTv = saveQrInfoV.findViewById(R.id.tv_money);
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(transferAmountTv.getText().toString())) {
            moneyTv.setText(getString(R.string.CO1_3a_2) + " " + transferAmountTv.getText().toString());
        }
        Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(qrcode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_logo_bocpay_collect_local), this);
        qrcodeImg.setImageBitmap(bitmapTmp);

        AndroidUtils.layoutView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext));
        saveQrInfoV.setDrawingCacheEnabled(true);
        saveQrInfoV.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        saveQrInfoV.setDrawingCacheBackgroundColor(Color.WHITE);
        // 把一个View转换成图片
        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext), mContext.getResources().getColor(R.color.color_blue_one));

        //YYYYMMDD_hhmmss

        Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, "request.jpg");
        saveQrInfoV.destroyDrawingCache();
        if (uri != null) {
            AndroidUtils.shareImage(this, "", uri);
        }
    }

    private void saveImageToGalley(String qrcode) {
        if (TextUtils.isEmpty(currentQrcode)) return;
        View saveQrInfoV = LayoutInflater.from(mContext).inflate(R.layout.include_save_qrcode, null);
        ImageView qrcodeImg = saveQrInfoV.findViewById(R.id.img_qrcode);
        TextView remarksTv = saveQrInfoV.findViewById(R.id.tv_remarks);
        TextView moneyTv = saveQrInfoV.findViewById(R.id.tv_money);
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(transferAmountTv.getText().toString())) {
            moneyTv.setText(getString(R.string.CO1_3a_2) + " " + transferAmountTv.getText().toString());
        }
        Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrcode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_logo_bocpay_collect_local), this);
        qrcodeImg.setImageBitmap(bitmapTmp);

        AndroidUtils.layoutView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext));
        saveQrInfoV.setDrawingCacheEnabled(true);
        saveQrInfoV.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        saveQrInfoV.setDrawingCacheBackgroundColor(Color.WHITE);
        // 把一个View转换成图片
        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext), mContext.getResources().getColor(R.color.color_blue_one));

        AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cachebmp, "Request_" + AndroidUtils.localImageStr(System.currentTimeMillis()));
        saveQrInfoV.destroyDrawingCache();
        Toast.makeText(mContext, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
        if (mHandler != null) {
            //用户提出重复点击 黑色toast 一直显示 这边做了一个简单的延迟
            tvSaveImage.setEnabled(false);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tvSaveImage.setEnabled(true);
                }
            }, 1500);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_SET_LIMIT && resultCode == RESULT_OK) {
            dataSetLimit = (TransferSetLimitEntity) data.getSerializableExtra(DATA_SET_LIMIT);
            if (dataSetLimit != null) {
                String amount = dataSetLimit.isAAChecked ? dataSetLimit.aaAmount : dataSetLimit.totalAmount;
                if (!TextUtils.isEmpty(dataSetLimit.msg)) {
                    remarks = dataSetLimit.msg;
                    msgTv.setText(dataSetLimit.msg);
                    llMsg.setVisibility(View.VISIBLE);
                    msgLine.setVisibility(View.VISIBLE);
                } else {
                    llMsg.setVisibility(View.GONE);
                    msgLine.setVisibility(View.GONE);
                }
                if (Double.parseDouble(amount) > 0) {
                    currencyTv.setVisibility(View.VISIBLE);
                    transferAmountTv.setText(AndroidUtils.subZeroAndDot(amount));
                    mPresenter.updateQRCode(AndroidUtils.subZeroAndDot(amount), dataSetLimit.msg);
                } else {
                    currencyTv.setVisibility(View.GONE);
                }

            }
        }

    }

    @Override
    public void updateQRCodeSuccess(StaticCodeEntity response) {
        staticCodeEntity = response;
        updateUI();
    }

    @Override
    public void updateQRCodeError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
        staticCodeEntity = null;
        emalQRBeans = null;
        mobnQRBeans = null;
        currentQrcode = null;

        showTab(!isStillPhonePage);
        ivStaticCode.setImageBitmap(bitmapEmpty);
    }

    @Override
    public void pullQRCodeSuccess(StaticCodeEntity response) {
        staticCodeEntity = response;
        updateUI();
    }

    @Override
    public void pullQRCodeError(String errorCode, String errorMsg) {
        if ("EWA3026".equals(errorCode)) {
            CustomDialog.Builder builder = new CustomDialog.Builder(this);
            builder.setTitle(getString(R.string.CO1_5_1));
            builder.setMessage("");
            builder.setTextBold(true);
            builder.setPositiveButton(getString(R.string.CO1_5_3), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class);
                    finish();
                }
            });
            builder.setNegativeButton(getString(R.string.CO1_5_2), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finish();
                }
            }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
            Activity mActivity = (Activity) mContext;
            if (mActivity != null && !mActivity.isFinishing()) {
                CustomDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.show();
            }
        } else {
            showErrorMsgDialog(mContext, errorMsg);
        }
        isShowEmail = false;
    }
}
