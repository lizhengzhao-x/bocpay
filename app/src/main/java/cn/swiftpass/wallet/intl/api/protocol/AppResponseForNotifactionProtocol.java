package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class AppResponseForNotifactionProtocol extends BaseProtocol {


    public AppResponseForNotifactionProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/messageCenter/pointResponse";
    }


}
