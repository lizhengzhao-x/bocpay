package cn.swiftpass.wallet.intl.module.cardmanagement.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountInfoProtocol;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountManagerContract;


public class SmartAccountManagerPresenter extends BasePresenter<SmartAccountManagerContract.View> implements SmartAccountManagerContract.Presenter {


    @Override
    public void getRegEddaInfo(int action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(action,errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(action,response);
                }
            }
        }).execute();
    }


    @Override
    public void checkSmartAccountBind(boolean isShowLoading) {
        if (getView() != null&&isShowLoading) {
            getView().showDialogNotCancel();
        }
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (isShowLoading){
                        getView().dismissDialog();
                    }
                    getView().checkSmartAccountBindError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    if (isShowLoading){
                        getView().dismissDialog();
                    }
                    getView().checkSmartAccountBindSuccess(response);
                }

            }
        }).execute();
    }


    @Override
    public void getSmartAccountInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();
    }

    @Override
    public void updateSmartAccountSuspend() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new UpdateSmartAccountInfoProtocol(SmartAccountConst.UPDATE_SMARTACCOUNT_SUSPEND,  null, null, null, null,new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateSmartAccountSuspendError( errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateSmartAccountSuspendSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void updateSmartAccountCancel() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new UpdateSmartAccountInfoProtocol(SmartAccountConst.UPDATE_SMARTACCOUNT_CANCEL, null, null, null, null, new NetWorkCallbackListener<SmartAccountUpdateEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateSmartAccountCancelError( errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(SmartAccountUpdateEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateSmartAccountCancelSuccess(response);
                }

            }
        }).execute();
    }


}
