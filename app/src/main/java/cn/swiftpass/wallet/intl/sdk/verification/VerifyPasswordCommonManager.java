package cn.swiftpass.wallet.intl.sdk.verification;

import android.app.Activity;
import android.content.Intent;

import androidx.fragment.app.FragmentActivity;

import cn.swiftpass.wallet.intl.utils.LogUtils;

public class VerifyPasswordCommonManager {


    private static final String TAG = "VerifyPasswordCommonManager";
    private Activity context;
    private OnPwdVerifyCallBack onPwdVerifyCallBack;
    private OnPwdVerifyWithMajorScanCallBack onPwdVerifyWithMajorScanCallBack;
    private boolean isSupportFingerPrint;

    private String mCurrentAction="";
    private boolean isDismissForForgetPwd = false;

    private VerifyPasswordCommonManager(Activity context) {
        this.context = context;
    }


    public static VerifyPasswordCommonManager with(FragmentActivity fragmentActivity) {
        return new VerifyPasswordCommonManager(fragmentActivity);
    }


    public VerifyPasswordCommonManager setSupportFingerPrint(boolean isSupportFingerPrint) {
        this.isSupportFingerPrint = isSupportFingerPrint;
        return this;
    }

    public VerifyPasswordCommonManager setAction(String action) {
        this.mCurrentAction = action;
        return this;
    }

    public VerifyPasswordCommonManager setDismissForgetPwd(boolean dismiss) {
        this.isDismissForForgetPwd = dismiss;
        return this;
    }

    public VerifyPasswordCommonManager setPwdVerifyCallBack(OnPwdVerifyCallBack callBack) {
        this.onPwdVerifyCallBack = callBack;
        return this;
    }

    public VerifyPasswordCommonManager setMajorScanCallBack(OnPwdVerifyWithMajorScanCallBack callBack) {
        this.onPwdVerifyWithMajorScanCallBack = callBack;
        return this;
    }

    public OnPwdVerifyCallBack getOnPwdVerifyCallBack() {
        return onPwdVerifyCallBack;
    }

    public OnPwdVerifyWithMajorScanCallBack getOnPwdVerifyWithMajorScanCallBack() {
        return onPwdVerifyWithMajorScanCallBack;
    }

    public boolean isSupportFingerPrint() {
        return isSupportFingerPrint;
    }

    public String getAction() {
        return mCurrentAction;
    }

    public boolean isDismissForForgetPwd() {
        return isDismissForForgetPwd;
    }

    public void show(){
        if (context==null){
            LogUtils.d(TAG,"验密接收的context 为null");
            return;
        }
        CommonVerifyPasswordActivity.builder=this;
        Intent intent = new Intent(context, CommonVerifyPasswordActivity.class);
        context.startActivity(intent);
        //去掉切换动画关键
        context.overridePendingTransition(0, 0);
    }


}
