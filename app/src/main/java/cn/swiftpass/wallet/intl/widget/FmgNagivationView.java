package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 左边文字 右边箭头布局封装
 */

public class FmgNagivationView extends RelativeLayout {

    private Context mContext;
    private String centerText = "";
    private String rightText = "";
    private ImageView idMenu;
    private boolean showRightArrow;
    private boolean showLine;
    private TextView tvRight;

    public ImageView getLeftMenu() {
        return idMenu;
    }


    public FmgNagivationView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public FmgNagivationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public FmgNagivationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    public TextView getTvRight() {
        return tvRight;
    }

    private void initViews(AttributeSet attrs, int defStyle) {


    }

}
