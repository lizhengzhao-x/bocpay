package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import cn.swiftpass.httpcore.entity.BankCardEntity;

public interface CardStackClickListener {


    void onClickTopUp(BankCardEntity bankCardEntity);
    void onClickWithdraw(BankCardEntity bankCardEntity);

    void onClickChangeLimitBalance(BankCardEntity bankCardEntity);
    void verifyPwdAction(String action, CardContentViewHolder holder, BankCardEntity bankCardEntity);

    void onClickUpdateAccount();

    void onClickVirtualCardManager(BankCardEntity bankCardEntity);

    void onClickAccountManager(BankCardEntity bankCardEntity);

    void onClickRegisterFps(BankCardEntity bankCardEntity);
}
