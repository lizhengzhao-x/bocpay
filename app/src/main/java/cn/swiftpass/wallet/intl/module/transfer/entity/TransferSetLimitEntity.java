package cn.swiftpass.wallet.intl.module.transfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/7 15:26
 * @change
 * @chang time
 * @class describe
 */
public class TransferSetLimitEntity extends BaseEntity {
    public boolean isAAChecked = false;
    public int aaCount = 2;
    public String totalAmount = "0";
    public String aaAmount = "0.00";
    public String msg = "";
}
