package cn.swiftpass.wallet.intl.entity;

public class SortEntity {

    private String name;
    private String letters;//显示拼音的首字母

    public String getRightHintText() {
        return rightHintText;
    }

    public void setRightHintText(String rightHintText) {
        this.rightHintText = rightHintText;
    }

    private String rightHintText;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLetters() {
        return letters;
    }

    public void setLetters(String letters) {
        this.letters = letters;
    }
}
