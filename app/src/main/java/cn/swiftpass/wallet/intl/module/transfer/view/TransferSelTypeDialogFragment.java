package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 转账 选择 fps/bocpay
 */
public class TransferSelTypeDialogFragment extends DialogFragment {

    private ImageView mId_back_image;
    private RecyclerView mRecycleView;

    public void setOnTransferSelTypeClickListener(OnTransferSelTypeClickListener onTransferSelTypeClickListener) {
        this.onTransferSelTypeClickListener = onTransferSelTypeClickListener;
    }

    private OnTransferSelTypeClickListener onTransferSelTypeClickListener;
    private ArrayList<BankCardEntity> cardEntities;
    private RelativeLayout mId_transfer_type_bocpay;
    private ImageView mId_transfer_type_bocpay_img;
    private ImageView mId_transfer_type_fps_sel;
    private TextView mId_transfer_type_fps_title;
    private TextView mId_transfer_type_fps_subtitle;
    private RelativeLayout mId_transfer_type_fps;
    private ImageView mId_transfer_type_fps_img;
    private ImageView mId_transfer_type_boc_sel;
    private TextView mId_transfer_type_boc_title;
    private TextView mId_transfer_type_boc_subtitle;
    private int currentSelPosition;
    private boolean isSupportBocPay, isSupportFps, isPaiLiShi, isOpenSmartAccount;
    //是否是信用卡
    private String bocpaySign;

    private void bindViews(View view) {
        mId_back_image = (ImageView) view.findViewById(R.id.id_back_image);
        mId_transfer_type_bocpay = (RelativeLayout) view.findViewById(R.id.id_transfer_type_bocpay);
        mId_transfer_type_bocpay_img = (ImageView) view.findViewById(R.id.id_transfer_type_bocpay_img);
        mId_transfer_type_fps_sel = (ImageView) view.findViewById(R.id.id_transfer_type_fps_sel);
        mId_transfer_type_fps_title = (TextView) view.findViewById(R.id.id_transfer_type_fps_title);
        mId_transfer_type_fps_subtitle = (TextView) view.findViewById(R.id.id_transfer_type_fps_subtitle);
        mId_transfer_type_fps = (RelativeLayout) view.findViewById(R.id.id_transfer_type_fps);
        mId_transfer_type_fps_img = (ImageView) view.findViewById(R.id.id_transfer_type_fps_img);
        mId_transfer_type_boc_sel = (ImageView) view.findViewById(R.id.id_transfer_type_boc_sel);
        mId_transfer_type_boc_title = (TextView) view.findViewById(R.id.id_transfer_type_boc_title);
        mId_transfer_type_boc_subtitle = (TextView) view.findViewById(R.id.id_transfer_type_boc_subtitle);
        View bacView = view.findViewById(R.id.id_bac_view);

        if (isPaiLiShi) {
            bacView.setBackgroundResource(R.drawable.bg_red_pocket);
        }
        mId_transfer_type_bocpay.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (onTransferSelTypeClickListener != null) {
                    currentSelPosition = 0;
                    updateSelUi();
                    dismiss();
                    onTransferSelTypeClickListener.onTransferTypeSelListener(0);
                }
            }
        });

        mId_transfer_type_fps.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (onTransferSelTypeClickListener != null) {
                    currentSelPosition = 1;
                    updateSelUi();
                    dismiss();
                    onTransferSelTypeClickListener.onTransferTypeSelListener(1);
                }
            }
        });

        if (isSupportBocPay) {
            mId_transfer_type_boc_subtitle.setText("");
            mId_transfer_type_boc_subtitle.setVisibility(View.GONE);
            mId_transfer_type_boc_title.setTextColor(getContext().getResources().getColor(R.color.app_black));
        } else {
            if(isOpenSmartAccount){
                if (!TextUtils.equals(bocpaySign, Constants.ACCOUNT_TYPE_SMART)) {
                    mId_transfer_type_boc_subtitle.setText(getString(R.string.TFX2101_1_1));
                } else {
                    mId_transfer_type_boc_subtitle.setText(getString(R.string.TFX2101_1_2));
                }
            }else {
                mId_transfer_type_boc_subtitle.setText(getString(R.string.TFX2101_1_2));
            }

            mId_transfer_type_boc_subtitle.setTextColor(getContext().getResources().getColor(R.color.red_title));
            mId_transfer_type_boc_title.setTextColor(getContext().getResources().getColor(R.color.app_black));
        }


        if (isSupportFps) {
            mId_transfer_type_fps_subtitle.setText("");
            mId_transfer_type_fps_subtitle.setVisibility(View.GONE);
            mId_transfer_type_fps_title.setTextColor(getContext().getResources().getColor(R.color.app_black));
        } else {
            mId_transfer_type_fps_subtitle.setText(getString(R.string.TR1_9_3_1));
            mId_transfer_type_fps_subtitle.setTextColor(getContext().getResources().getColor(R.color.red_title));
            mId_transfer_type_fps_title.setTextColor(getContext().getResources().getColor(R.color.app_black));
            mId_transfer_type_fps.setEnabled(false);
        }
        updateSelUi();
        mId_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private void updateSelUi() {
        selWithPosition();
    }

    private void selWithPosition() {
        mId_transfer_type_boc_sel.setImageResource(currentSelPosition == 0 ? R.mipmap.icon_transfer_checked : R.mipmap.icon_transfer_unchecked);
        mId_transfer_type_fps_sel.setImageResource(currentSelPosition == 1 ? R.mipmap.icon_transfer_checked : R.mipmap.icon_transfer_unchecked);
        if (!isSupportFps) {
            mId_transfer_type_fps_sel.setVisibility(View.GONE);
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sel_transfer_type, container, false);
        bindViews(view);
        updateUI();
        return view;
    }

    private void updateUI() {


    }

    public void setCurrentSelPosition(int lastSelTransferType, boolean isSupportBocPayIn, boolean isSupportFpsIn, boolean isOpenSmartAccountIn, boolean isPaiLiShi,String bocpaySign) {
        currentSelPosition = lastSelTransferType;
        this.isSupportBocPay = isSupportBocPayIn;
        this.isSupportFps = isSupportFpsIn;
        this.isPaiLiShi = isPaiLiShi;
        this.isOpenSmartAccount = isOpenSmartAccountIn;
        this.bocpaySign = bocpaySign;
    }


    public interface OnTransferSelTypeClickListener {
        void onBackBtnClickListener();

        void onTransferTypeSelListener(int positon);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

}
