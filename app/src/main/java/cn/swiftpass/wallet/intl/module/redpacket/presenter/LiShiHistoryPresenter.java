package cn.swiftpass.wallet.intl.module.redpacket.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.RedPacketRecordsProtocol;
import cn.swiftpass.wallet.intl.module.redpacket.contract.LiShiHistoryContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.LishiHistoryEntity;

/**
 * 派利是
 */
public class LiShiHistoryPresenter extends BasePresenter<LiShiHistoryContract.View> implements LiShiHistoryContract.Presenter {
    @Override
    public void getLiShiHistory(int pageNum, int pageSize, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new RedPacketRecordsProtocol(action, pageNum, pageSize, new NetWorkCallbackListener<LishiHistoryEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getLiShiHistoryError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(LishiHistoryEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getLiShiHistorySuccess(response);
                }

            }
        }).execute();
    }

}
