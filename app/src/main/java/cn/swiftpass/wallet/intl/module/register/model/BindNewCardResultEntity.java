package cn.swiftpass.wallet.intl.module.register.model;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class BindNewCardResultEntity extends BaseEntity {
    private ArrayList<BindNewCardEntity> panSuccessList; // 绑定成功列表
    private ArrayList<BindNewCardEntity> panFailList; //绑定失败列表
    private String showInvite;//是否展示邀请码页面 0 不显示 1显示
    private String errorMsg;//失败消息
    public static final String TYPE_SHOW_INVITE = "1";

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ArrayList<BindNewCardEntity> getPanSuccessList() {
        return panSuccessList;
    }

    public void setPanSuccessList(ArrayList<BindNewCardEntity> panSuccessList) {
        this.panSuccessList = panSuccessList;
    }

    public ArrayList<BindNewCardEntity> getPanFailList() {
        return panFailList;
    }

    public void setPanFailList(ArrayList<BindNewCardEntity> panFailList) {
        this.panFailList = panFailList;
    }

    public String getShowInvite() {
        return showInvite;
    }

    public void setShowInvite(String showInvite) {
        this.showInvite = showInvite;
    }
}
