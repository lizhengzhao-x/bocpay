package cn.swiftpass.wallet.intl.module.login;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.BaseRecycleAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SelCountyEntity;

public class SelCountyAdapter extends BaseRecycleAdapter<SelCountyEntity> {

    private List<SelCountyEntity> mList = new ArrayList<>();
    private Context mContext;
    private TextView tv_name;
    private TextView tv_code;

    public SelCountyAdapter(Context mContext, List<SelCountyEntity> datas) {
        super(datas);
        this.mContext = mContext;
        mList = datas;
    }

    @Override
    protected void bindData(BaseViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(position, mList.get(position));
                }
            }
        });
        tv_code = (TextView) holder.getView(R.id.tv_code);
        tv_name = (TextView) holder.getView(R.id.tv_name);
        Drawable drawable = mContext.getResources().getDrawable(R.mipmap.icon_check_choose_tick);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        if (mList.get(position).isSelection()) {
            tv_code.setCompoundDrawablePadding(20);
            tv_code.setCompoundDrawables(null, null, drawable, null);
        } else {
            tv_code.setCompoundDrawablePadding(0);
            tv_code.setCompoundDrawables(null, null, null, null);
        }
        tv_name.setText(mList.get(position).getCountyName());
        tv_code.setText(mList.get(position).getCountyCode());
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_selcounty;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, SelCountyEntity bean);
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }
}
