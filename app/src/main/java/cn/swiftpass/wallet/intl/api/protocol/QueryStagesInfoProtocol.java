package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 信用卡分期（一期
 */
public class QueryStagesInfoProtocol extends BaseProtocol {
    String key;

    public QueryStagesInfoProtocol(String key, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/creditCardInstallment/queryStagesInfo";
        this.key = key;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(key)) {
            mBodyParams.put(RequestParams.KEY, key);
        }
    }
}



