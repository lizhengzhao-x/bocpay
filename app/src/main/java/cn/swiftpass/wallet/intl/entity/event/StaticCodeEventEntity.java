package cn.swiftpass.wallet.intl.entity.event;

import cn.swiftpass.wallet.intl.module.transfer.entity.TransferSetLimitEntity;

public class StaticCodeEventEntity extends BaseEventEntity {

    public TransferSetLimitEntity getTransferSetLimitEntity() {
        return transferSetLimitEntity;
    }

    private TransferSetLimitEntity transferSetLimitEntity;

    public StaticCodeEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public StaticCodeEventEntity(TransferSetLimitEntity transferSetLimitEntity) {
        super(-1, null);
        this.transferSetLimitEntity = transferSetLimitEntity;
    }
}
