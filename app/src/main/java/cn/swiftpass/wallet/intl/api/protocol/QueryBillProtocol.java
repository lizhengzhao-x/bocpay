package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.BillListEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 10:39
 * 账单查询接口
 * 对应的返回对象为 BillListEntity
 */
public class QueryBillProtocol extends BaseProtocol {
    public static final String TAG = QueryBillProtocol.class.getSimpleName();
    /**
     * 查询方式 0全部 1信用卡 2电子账户
     */
    String mQueryType;
    /**
     * 页码
     */
    String mPageNo;
    /**
     * 页面大小（默认10）
     */
    String mPageSize;
    /**
     * 卡id
     */
    String mPanId;

    public QueryBillProtocol(String queryType, String pageNo, String pageSize, String panId, NetWorkCallbackListener<BillListEntity> dataCallback) {
        this.mQueryType = queryType;
        this.mPageNo = pageNo;
        this.mPageSize = pageSize;
        this.mPanId = panId;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actionTxnHistEnquiryBocPay";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.QUERYTYPE, mQueryType);
        mBodyParams.put(RequestParams.PAGENO, mPageNo);
        mBodyParams.put(RequestParams.PAGESIZE, mPageSize);
        mBodyParams.put(RequestParams.PANID, mPanId);
    }
}
