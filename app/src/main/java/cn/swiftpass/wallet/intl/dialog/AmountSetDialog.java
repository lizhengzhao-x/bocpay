package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;


public class AmountSetDialog extends Dialog {

    private int mAmount;
    private int mMaxAmount;


    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public AmountSetDialog(Context context) {
        super(context);
    }

    public AmountSetDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private OnClickListener positiveButtonClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setPositiveListener(OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public AmountSetDialog create(final int amount, final int maxAmount) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final AmountSetDialog dialog = new AmountSetDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_set_amount_layout, null);
            final Button posBtn = layout.findViewById(R.id.positive_dl);
            posBtn.setEnabled(false);
            Button negBtn = layout.findViewById(R.id.negative_dl);
            final EditText amountET = layout.findViewById(R.id.et_amount);
            if (amount != -1) {
                amountET.setText(String.valueOf(amount));
                amountET.setSelection(String.valueOf(amount).length());
            }
            negBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            posBtn.setEnabled(false);
            posBtn.setTextColor(context.getColor(R.color.app_gray));
            addListener(maxAmount, dialog, posBtn, amountET);
            dialog.setContentView(layout);
            return dialog;
        }

        private void addListener(final int maxAmount, final AmountSetDialog dialog, final Button posBtn, final EditText amountET) {
            amountET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable arg0) {
                    if (arg0.toString().length() == 1 && Constants.ZERA_STR.equals(arg0.toString())) {
                        arg0.clear();
                        posBtn.setEnabled(false);
                        posBtn.setTextColor(context.getColor(R.color.app_gray));
                        return;
                    }
                    String currentValueStr = amountET.getText().toString();
                    if (!TextUtils.isEmpty(currentValueStr)) {
                        int currentValue = 0;
                        try {
                            currentValue = Integer.valueOf(currentValueStr).intValue();
                        } catch (NumberFormatException e) {

                            currentValue = maxAmount + 1;
                        }
                        if (currentValue <= maxAmount) {
                            posBtn.setEnabled(true);
                            posBtn.setTextColor(context.getColor(R.color.color_green));
                        } else {
                            amountET.setText(String.valueOf(maxAmount));
                            amountET.setSelection(String.valueOf(maxAmount).length());
                        }
                    } else {
                        posBtn.setEnabled(false);
                        posBtn.setTextColor(context.getColor(R.color.app_gray));
                    }
                }
            });
            posBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(amountET.getText().toString())) {
                        dialog.setmAmount(Integer.valueOf(amountET.getText().toString()));
                        if (positiveButtonClickListener != null) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                        dialog.dismiss();
                    } else {
                        dialog.setmAmount(Integer.valueOf(amountET.getText().toString()));
                        if (positiveButtonClickListener != null) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                        dialog.dismiss();
                    }
                }
            });
            amountET.requestFocus();
        }
    }
}

