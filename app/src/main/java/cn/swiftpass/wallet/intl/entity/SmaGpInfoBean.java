package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name WalletBasicAppUIMergeVersion
 * @class name：cn.swiftpass.wallet.intl.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/8/26 20:43
 * @change
 * @chang time
 * @class describe
 */

public class SmaGpInfoBean extends BaseEntity {
    /**
     * bal : 2000
     * expiryDate : 2019年07月03日
     * name : 支付账户
     * pan : 012*******2323
     */

    private String bal;
    private String expiryDate;
    private String name;
    private String pan;

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }


    public String getRealPan() {
        return realPan;
    }

    public void setRealPan(String realPan) {
        this.realPan = realPan;
    }

    private String realPan;

    public String getType() {
        return type;
    }

    private boolean isSel;
    private String type;

    public String getBal() {
        return bal;
    }

    public void setBal(String bal) {
        this.bal = bal;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}

