package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class AddTransferInviteRecordProtocol extends BaseProtocol {
    String inCode;

    public AddTransferInviteRecordProtocol(String inCode, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.inCode = inCode;
        mUrl = "api/transfer/addTransferInviteRecord";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.INCODE, inCode);
    }
}
