package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class OCSLanguageEntity extends BaseEntity {

    private String language;

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getOldDeviceToken() {
        return oldDeviceToken;
    }

    public void setOldDeviceToken(String oldDeviceToken) {
        this.oldDeviceToken = oldDeviceToken;
    }

    public String getNewDeviceToken() {
        return newDeviceToken;
    }

    public void setNewDeviceToken(String newDeviceToken) {
        this.newDeviceToken = newDeviceToken;
    }

    private String appId;
    private String oldDeviceToken;
    private String newDeviceToken;


}
