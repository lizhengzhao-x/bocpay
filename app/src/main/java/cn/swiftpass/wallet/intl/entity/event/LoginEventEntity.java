package cn.swiftpass.wallet.intl.entity.event;


public class LoginEventEntity extends BaseEventEntity {

    /**
     * EVENT_KEY_DISMISS_PWD
     * EVENT_LOGIN_OVER_TIME_CODE 登录操作逾时
     */
    public static final int EVENT_KEY_DISMISS_PWD = 11001;
    public static final int EVENT_LOGIN_OVER_TIME_CODE = 11002;

    public LoginEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public LoginEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
