package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class RegisterIDEntity extends BaseEntity {
    public String getSmartMaxPay() {
        return smartMaxPay;
    }

    public void setSmartMaxPay(String smartMaxPay) {
        this.smartMaxPay = smartMaxPay;
    }

    private String smartMaxPay;

    public String getSmartMaxDefault() {
        return smartMaxDefault;
    }

    public void setSmartMaxDefault(String smartMaxDefault) {
        this.smartMaxDefault = smartMaxDefault;
    }

    private String smartMaxDefault;


}
