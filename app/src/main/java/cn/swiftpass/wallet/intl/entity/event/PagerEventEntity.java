package cn.swiftpass.wallet.intl.entity.event;


public class PagerEventEntity extends BaseEventEntity{

    /**
     * 页面跳转
     */
    public static final int EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT_NOTIFICATION = 999;//push通知
    public static final int EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT = 1001;//我的账户
    public static final int EVENT_KEY_SWITCH_PAGES_TO_ECOUPON = 1002;//电子券
    public static final int EVENT_KEY_SWITCH_PAGES_TO_INVITE_SHARE = 1003;//推荐亲友
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_FPS_ID = 1004;//FPS 转账
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_BANK = 1005;//银行账户转账
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_LISHI = 1006;//派利是
    public static final int EVENT_KEY_SWITCH_PAGES_TO_MAJOR_SCAN = 1007;//主扫
    public static final int EVENT_KEY_SWITCH_PAGES_TO_STATIC_QR_CODE = 1008;//收款
    public static final int EVENT_KEY_SWITCH_PAGES_TO_UPLAN = 1010;//UPLAN
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_CROSS_BORDER_PAYEE = 1009;//跨境转账
    public static final int EVENT_KEY_SWITCH_PAGES_TO_NEWS_PAGE = 1011;//最新消息
    public static final int EVENT_KEY_SWITCH_PAGES_TO_BACK_SCAN = 1012;
    public static final int EVENT_KEY_SWITCH_PAGES_TO_VIRTUAL_CARD_APPLY = 1013;
    public static final int EVENT_KEY_SWITCH_PAGES_TO_CREDIT_CARD_REWARD = 1014;//最新消息
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_SCAN = 0x1023;  //FPS缴费
    public static final int EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_LIST = 0x1024;  //交易记录
    public static final int EVENT_KEY_SWITCH_PAGES_TO_RED_PACKET_GET_FRAGMENT = 0x1025;  //利是 跳入拆利是页面



    public PagerEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public PagerEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
