package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.view.View;
import android.widget.ExpandableListView;

import androidx.annotation.NonNull;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.CreditCardBillEntity;
import cn.swiftpass.wallet.intl.entity.CreditCardEntity;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.CreditCardPaymentListExpandableAdapter;

/**
 * 信用卡交易记录 根据卡片分组
 */
public class TransacCreditCardFragment extends BaseFragment {
    private ArrayList<CreditCardEntity.CardListBean> groupTitles;
    private Map<String, List<CreditCardBillEntity.StmtTxnBean>> paymentListMap;
    @BindView(R.id.id_stickyHeaderView)
    ExpandableListView idStickyHeaderView;
    private CreditCardPaymentListExpandableAdapter creditCardPaymentListAdapter;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    //当前进行密码验证的卡
    private int currentGroupId;

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transac_credit;
    }

    @Override
    public void initTitle() {

    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }
    @Override
    protected void initView(View v) {
        groupTitles = new ArrayList<>();
        paymentListMap = new HashMap<>();
        creditCardPaymentListAdapter = new CreditCardPaymentListExpandableAdapter(groupTitles, getContext());
        idStickyHeaderView.setAdapter(creditCardPaymentListAdapter);

        idStickyHeaderView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                collapseGroup(groupPosition);
                creditCardPaymentListAdapter.notifyDataSetChanged();
            }
        });
        idStickyHeaderView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
                groupTitles.get(groupPosition).setExpand(false);
                creditCardPaymentListAdapter.notifyDataSetChanged();
            }
        });
        creditCardPaymentListAdapter.setOnGroupItemClickListener(new CreditCardPaymentListExpandableAdapter.OnGroupItemClickListener() {
            @Override
            public void onGroupItemClick(int groupId) {
                //密码校验
                TransactionManagementFragment transactionManagementFragment = (TransactionManagementFragment) getParentFragment();
                transactionManagementFragment.checkMoreEvent();
                currentGroupId = groupId;
            }
        });
        idStickyHeaderView.setGroupIndicator(null);
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getCreditCardList();
            }
        });
        getCreditCardList();
    }

    public void clearData() {
        if (paymentListMap != null && creditCardPaymentListAdapter != null) {
            paymentListMap.clear();
            creditCardPaymentListAdapter.notifyDataSetChanged();
        }
    }


    /**
     * 获取卡列表
     */
    private void getCreditCardList() {
        showDialogNotCancel();
        ApiProtocolImplManager.getInstance().getCreditCardList(getActivity(), new NetWorkCallbackListener<CreditCardEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
                showErrorMsgDialog(getContext(), errorMsg);
            }

            @Override
            public void onSuccess(CreditCardEntity response) {
                dismissDialog();
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
                updateParentView(response);
            }
        });
    }

    private void updateParentView(CreditCardEntity response) {
        groupTitles.clear();
        List<CreditCardEntity.CardListBean> cardListBeans = response.getCardList();
        if (cardListBeans != null) {
            for (int i = 0; i < cardListBeans.size(); i++) {
                CreditCardEntity.CardListBean cardListBean = cardListBeans.get(i);
                cardListBean.setUiTitle(cardListBean.getCardName() + "  (" + cardListBean.getPanFour() + ")");
                cardListBean.setStmtTxnBeans(new ArrayList<CreditCardBillEntity.StmtTxnBean>());
                groupTitles.add(cardListBean);
            }
        }
        creditCardPaymentListAdapter.notifyDataSetChanged();
        //如果当前只有一张信用卡 则直接展开显示
        if (groupTitles.size() == 1) {
            idStickyHeaderView.expandGroup(0);
            collapseGroup(0);
        }

    }

    /**
     * 收起其他组别
     */
    private void collapseGroup(int groupPosition) {
        groupTitles.get(groupPosition).setExpand(true);
        idStickyHeaderView.setSelectedGroup(groupPosition);
        ArrayList<CreditCardBillEntity.StmtTxnBean> stmtTxnBeans = groupTitles.get(groupPosition).getStmtTxnBeans();
        if (stmtTxnBeans.size() == 0) {
            getnTxnHistEnquiryCredit(groupTitles.get(groupPosition).getPanId(), groupPosition);
        }
        for (int i = 0; i < groupTitles.size(); i++) {
            if (groupPosition != i) {
                idStickyHeaderView.collapseGroup(i);
                groupTitles.get(i).setExpand(false);
            }
        }
    }


    /**
     * 根据卡列表中的pandId拉去交易列表
     *
     * @param pandId
     * @param groupPosition
     */
    private void getnTxnHistEnquiryCredit(final String pandId, final int groupPosition) {
        ApiProtocolImplManager.getInstance().getTnTxnHistEnquiryCredit(getActivity(), pandId, new NetWorkCallbackListener<CreditCardBillEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) return;
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onSuccess(CreditCardBillEntity response) {
                List<CreditCardBillEntity.StmtTxnBean> stmtTxnBeans = response.getStmtTxn();
                CreditCardEntity.CardListBean cardListBean = groupTitles.get(groupPosition);
                //所有记录存储到map中
                paymentListMap.put(pandId, stmtTxnBeans);
                List<CreditCardBillEntity.StmtTxnBean> stmtTxnBeansSub = new ArrayList<>();
                if (stmtTxnBeans.size() > Constants.BILL_ITEM_PAGE_SIZE) {
                    for (int i = 0; i < Constants.BILL_ITEM_PAGE_SIZE; i++) {
                        stmtTxnBeansSub.add(stmtTxnBeans.get(i));
                    }
                    //大于10条 最后一条要显示更多
                    CreditCardBillEntity.StmtTxnBean stmtTxnBean = new CreditCardBillEntity.StmtTxnBean();
                    stmtTxnBean.setUiType(CreditCardBillEntity.StmtTxnBean.UITYPE_MORE);
                    stmtTxnBeansSub.add(stmtTxnBean);
                } else {
                    stmtTxnBeansSub.addAll(stmtTxnBeans);
                }
                cardListBean.getStmtTxnBeans().clear();
                cardListBean.getStmtTxnBeans().addAll(stmtTxnBeansSub);
                creditCardPaymentListAdapter.notifyDataSetChanged();
                idStickyHeaderView.setSelectedGroup(groupPosition);
            }
        });
    }

    /**
     * 查看更多之后 更新显示ui
     */
    public void updateCheckMoreUI() {
        if (paymentListMap != null) {
            List<CreditCardBillEntity.StmtTxnBean> stmtTxnBeans = null;
            try {
                // 线上闪退日志分析
                stmtTxnBeans = paymentListMap.get(groupTitles.get(currentGroupId).getPanId());
                groupTitles.get(currentGroupId).getStmtTxnBeans().clear();
                groupTitles.get(currentGroupId).getStmtTxnBeans().addAll(stmtTxnBeans);
                creditCardPaymentListAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void updateList() {
        if (smartRefreshLayout != null && groupTitles != null && groupTitles.isEmpty()) {
            smartRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    smartRefreshLayout.autoRefresh();
                    getCreditCardList();
                }
            });
        }
    }
}
