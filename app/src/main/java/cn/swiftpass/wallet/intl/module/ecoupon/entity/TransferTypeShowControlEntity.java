package cn.swiftpass.wallet.intl.module.ecoupon.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/5.
 */
public class TransferTypeShowControlEntity extends BaseEntity {


    /**
     * fps_payment : 1
     * bank_account_payment : 1
     * cross_border_payment : 1
     * gift_activity : 0
     */

    private String fps_payment;
    private String bank_account_payment;
    private String cross_border_payment;
    private String gift_activity;

    public String getFps_payment() {
        return fps_payment;
    }

    public void setFps_payment(String fps_payment) {
        this.fps_payment = fps_payment;
    }

    public String getBank_account_payment() {
        return bank_account_payment;
    }

    public void setBank_account_payment(String bank_account_payment) {
        this.bank_account_payment = bank_account_payment;
    }

    public String getCross_border_payment() {
        return cross_border_payment;
    }

    public void setCross_border_payment(String cross_border_payment) {
        this.cross_border_payment = cross_border_payment;
    }

    public String getGift_activity() {
        return gift_activity;
    }

    public void setGift_activity(String gift_activity) {
        this.gift_activity = gift_activity;
    }
}
