package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.adapter;

import android.content.Context;
import android.view.View;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PrimaryAccountEntity;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;


public class PrimaryAccountAdapter implements AdapterItem<PrimaryAccountEntity> {
    private Context context;

    public PrimaryAccountAdapter(Context context, MessageItemCallback messageItemCallback) {
        this.context = context;
        this.mCallback = messageItemCallback;
    }

    ImageArrowView imageArrowView;

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_primaryaccount;
    }

    @Override
    public void bindViews(View root) {
        imageArrowView = root.findViewById(R.id.id_item_primary);

    }

    private MessageItemCallback mCallback;

    @Override
    public void setViews() {

        imageArrowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onMessageItemCallback(mPosition);
                }
            }
        });

    }

    @Override
    public void handleData(PrimaryAccountEntity text, int position) {
        mPosition = position;
        imageArrowView.setLeftTextTitle(text.getAccount());
        imageArrowView.setRightImageShow(text.isSel());
    }

    public static class MessageItemCallback {


        public void onMessageItemCallback(int position) {
            // do nothing
        }

    }
}
