package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.view.View;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;


public class TransacAdapter implements AdapterItem<PaymentEnquiryResult> {
    private TransacItemClickCallBack transacItemClickCallBack;
    private Context mcontext;

    public TransacAdapter(Context context, TransacItemClickCallBack transacItemClickCallBack) {

        this.mcontext = context;
        this.transacItemClickCallBack = transacItemClickCallBack;
    }

    @Override
    public int getLayoutResId() {
        return 0;
    }

    @Override
    public void bindViews(View root) {

    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(PaymentEnquiryResult paymentEnquiryResult, int position) {

    }

    public static class TransacItemClickCallBack {


        public void onTransacItemClick(int position) {
            // do nothing
        }

    }
}
