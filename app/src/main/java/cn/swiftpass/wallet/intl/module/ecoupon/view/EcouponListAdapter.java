package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;

public class EcouponListAdapter implements AdapterItem<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> {
    private Context context;

    public EcouponListAdapter(Context context) {
        this.context = context;
    }


    private TextView id_left_name;
    private TextView id_right_cnt;
    private TextView id_left_no;


    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoupon_item;
    }

    @Override
    public void bindViews(View root) {
        id_left_name = (TextView) root.findViewById(R.id.id_left_name);
        id_right_cnt = (TextView) root.findViewById(R.id.id_right_cnt);
        id_left_no = (TextView) root.findViewById(R.id.id_left_no);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(MyEVoucherEntity.EvoucherItem.EVoucherInfosBean homeMessageEntity, int position) {
        mPosition = position;
        id_left_name.setText(homeMessageEntity.getReferenceNo() + "");
        id_right_cnt.setText(homeMessageEntity.getExpireDate() + "");
        id_left_no.setText((position + 1) + "");
    }
}
