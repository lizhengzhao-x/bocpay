package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class PromotionNewEntity extends BaseEntity {


    /**
     * en_US : {"data":{"title":"Cross-border Bill Payment Platform","subTitle":"Flexible payment of various types of bills online in the Greater Bay Area","type":"BOC PAY","introduction":"You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>\"Bill Payment Steps\"<\/Flow> and \"Accepting Merchants\" for further details (Available in Chinese only)","tips":"To borrow or not to borrow? Borrow only if you can repay!","flowUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (\u201cBOCHK\u201d) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}}
     * zh_HK : {"data":{"title":"跨境繳費平台","subTitle":"輕鬆登入跨境繳費平台，支付大灣區各類賬單","type":"BOC PAY","introduction":"透過BoC Pay流動應用程式登入大灣區跨境繳費平台，讓您隨時隨地以中銀雙幣卡支付大灣區各類公共服務賬單，如水費、電費、燃氣費及電視費等，方便快捷。請按<Flow>「繳費平台流程」<\/Flow>及<Cooperation>「合作繳費商戶」<\/Cooperation>，了解更多詳情","tips":"借定唔借？還得到先好借！","flowUrl":"https://www.bochk.com/creditcard/chi/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/chi/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"大灣區跨境繳費平台由廣州銀聯網絡支付有限公司提供。中國銀行(香港)有限公司(下稱「中銀香港」)及中銀信用卡（國際）有限公司並不會對廣州銀聯網絡支付有限公司提供的產品及服務作出保證，或對於使用廣州銀聯網絡支付有限公司產品及服務時所構成的後果負責。產品及服務的使用條款須受廣州銀聯網絡支付有限公司的相關條款約束","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}}
     * zh_CN : {"data":{"title":"跨境缴费平台","subTitle":"轻松登入跨境缴费平台，支付大湾区各类账单","type":"BOC PAY","introduction":"透过BoC Pay流动应用程式登入大湾区跨境缴费平台，让您随时随地以中银双币卡支付大湾区各类公共服务账单，如水费、电费、燃气费及电视费等，方便快捷。请按<Flow>「缴费平台流程」<\/Flow>及<Cooperation>「合作缴费商户」<\/Cooperation>，了解更多详情。","tips":"借定唔借？还得到先好借！","flowUrl":"https://www.bochk.com/creditcard/smp/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/smp/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"大湾区跨境缴费平台由广州银联网络支付有限公司提供。中国银行(香港)有限公司(下称「中银香港」)及中银信用卡（国际）有限公司并不会对广州银联网络支付�","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}}
     */

    private EnUSBean en_US;
    private ZhHKBean zh_HK;
    private ZhCNBean zh_CN;

    public EnUSBean getEn_US() {
        return en_US;
    }

    public void setEn_US(EnUSBean en_US) {
        this.en_US = en_US;
    }

    public ZhHKBean getZh_HK() {
        return zh_HK;
    }

    public void setZh_HK(ZhHKBean zh_HK) {
        this.zh_HK = zh_HK;
    }

    public ZhCNBean getZh_CN() {
        return zh_CN;
    }

    public void setZh_CN(ZhCNBean zh_CN) {
        this.zh_CN = zh_CN;
    }

    public static class EnUSBean {
        /**
         * data : {"title":"Cross-border Bill Payment Platform","subTitle":"Flexible payment of various types of bills online in the Greater Bay Area","type":"BOC PAY","introduction":"You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>\"Bill Payment Steps\"<\/Flow> and \"Accepting Merchants\" for further details (Available in Chinese only)","tips":"To borrow or not to borrow? Borrow only if you can repay!","flowUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (\u201cBOCHK\u201d) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}
         */

        private DataBeanX data;

        public DataBeanX getData() {
            return data;
        }

        public void setData(DataBeanX data) {
            this.data = data;
        }


    }

    public static class ZhHKBean {
        /**
         * data : {"title":"跨境繳費平台","subTitle":"輕鬆登入跨境繳費平台，支付大灣區各類賬單","type":"BOC PAY","introduction":"透過BoC Pay流動應用程式登入大灣區跨境繳費平台，讓您隨時隨地以中銀雙幣卡支付大灣區各類公共服務賬單，如水費、電費、燃氣費及電視費等，方便快捷。請按<Flow>「繳費平台流程」<\/Flow>及<Cooperation>「合作繳費商戶」<\/Cooperation>，了解更多詳情","tips":"借定唔借？還得到先好借！","flowUrl":"https://www.bochk.com/creditcard/chi/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/chi/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"大灣區跨境繳費平台由廣州銀聯網絡支付有限公司提供。中國銀行(香港)有限公司(下稱「中銀香港」)及中銀信用卡（國際）有限公司並不會對廣州銀聯網絡支付有限公司提供的產品及服務作出保證，或對於使用廣州銀聯網絡支付有限公司產品及服務時所構成的後果負責。產品及服務的使用條款須受廣州銀聯網絡支付有限公司的相關條款約束","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}
         */

        private DataBeanX data;

        public DataBeanX getData() {
            return data;
        }

        public void setData(DataBeanX data) {
            this.data = data;
        }


    }

    public static class ZhCNBean {
        /**
         * data : {"title":"跨境缴费平台","subTitle":"轻松登入跨境缴费平台，支付大湾区各类账单","type":"BOC PAY","introduction":"透过BoC Pay流动应用程式登入大湾区跨境缴费平台，让您随时随地以中银双币卡支付大湾区各类公共服务账单，如水费、电费、燃气费及电视费等，方便快捷。请按<Flow>「缴费平台流程」<\/Flow>及<Cooperation>「合作缴费商户」<\/Cooperation>，了解更多详情。","tips":"借定唔借？还得到先好借！","flowUrl":"https://www.bochk.com/creditcard/smp/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/smp/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"大湾区跨境缴费平台由广州银联网络支付有限公司提供。中国银行(香港)有限公司(下称「中银香港」)及中银信用卡（国际）有限公司并不会对广州银联网络支付�","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}
         */

        private DataBeanX data;

        public DataBeanX getData() {
            return data;
        }

        public void setData(DataBeanX data) {
            this.data = data;
        }


    }

    public static class DataBeanX {
        /**
         * title : Cross-border Bill Payment Platform
         * subTitle : Flexible payment of various types of bills online in the Greater Bay Area
         * type : BOC PAY
         * introduction : You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>"Bill Payment Steps"</Flow> and "Accepting Merchants" for further details (Available in Chinese only)
         * tips : To borrow or not to borrow? Borrow only if you can repay!
         * flowUrl : https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html
         * cooperationUrl : https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html
         * tnc : Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (“BOCHK”) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.
         * crossPaymentUrl : https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT
         */

        private String title;
        private String subTitle;
        private String type;
        private String introduction;
        private String tips;
        private String flowUrl;
        private String cooperationUrl;
        private String tnc;
        private String crossPaymentUrl;
        private String tncurl;
        private String tnctitle;

        public String getBannerUrl() {
            return bannerUrl;
        }

        public void setBannerUrl(String bannerUrl) {
            this.bannerUrl = bannerUrl;
        }

        private String bannerUrl;

        public String getTncurl() {
            return tncurl;
        }

        public void setTncurl(String tncurl) {
            this.tncurl = tncurl;
        }

        public String getTnctitle() {
            return tnctitle;
        }

        public void setTnctitle(String tnctitle) {
            this.tnctitle = tnctitle;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getIntroduction() {
            return introduction;
        }

        public void setIntroduction(String introduction) {
            this.introduction = introduction;
        }

        public String getTips() {
            return tips;
        }

        public void setTips(String tips) {
            this.tips = tips;
        }

        public String getFlowUrl() {
            return flowUrl;
        }

        public void setFlowUrl(String flowUrl) {
            this.flowUrl = flowUrl;
        }

        public String getCooperationUrl() {
            return cooperationUrl;
        }

        public void setCooperationUrl(String cooperationUrl) {
            this.cooperationUrl = cooperationUrl;
        }

        public String getTnc() {
            return tnc;
        }

        public void setTnc(String tnc) {
            this.tnc = tnc;
        }

        public String getCrossPaymentUrl() {
            return crossPaymentUrl;
        }

        public void setCrossPaymentUrl(String crossPaymentUrl) {
            this.crossPaymentUrl = crossPaymentUrl;
        }
    }
}
