package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.AddVouchersView;
import cn.swiftpass.wallet.intl.module.ecoupon.view.EcouponSelChangedListener;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;


public class EcouponsDetailVerticalAdapter implements AdapterItem<RedeemableGiftListEntity.EVoucherListBean> {

    private ImageView mId_right_message;
    private TextView mId_msg_name;
    private TextView mId_msg_date;
    private TextView mId_msg_error;
    private Context mContext;
    private int mPosition;
    private AddVouchersView addVouchersView;
    private EcouponSelChangedListener onItemEcouponCountChangeListener;
    private boolean isDiscount;

    public EcouponsDetailVerticalAdapter(Context mContextIn, boolean isDiscountIn, EcouponSelChangedListener onItemEcouponCountChangeListenerIn) {
        this.mContext = mContextIn;
        this.onItemEcouponCountChangeListener = onItemEcouponCountChangeListenerIn;
        this.isDiscount = isDiscountIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoupons_detail;
    }

    @Override
    public void bindViews(View root) {

        mId_right_message = (ImageView) root.findViewById(R.id.id_right_message);
        mId_msg_name = (TextView) root.findViewById(R.id.id_msg_name);
        mId_msg_date = (TextView) root.findViewById(R.id.id_msg_date);
        mId_msg_error = (TextView) root.findViewById(R.id.id_msg_error);
        addVouchersView = (AddVouchersView) root.findViewById(R.id.id_switch_view);
        addVouchersView.setOnItemEcouponCountChangeListener(new AddVouchersView.OnItemEcouponCountChangeListener() {
            @Override
            public void onItemEcouponCountChanged(int count, int positon) {
                if (onItemEcouponCountChangeListener != null) {
                    onItemEcouponCountChangeListener.onItemEcouponCountChanged(count, mPosition);
                }
            }

            @Override
            public void onItemCheckMore(int position) {

            }

            @Override
            public void onReachMaxValue() {
                onItemEcouponCountChangeListener.onReachMaxValue();
            }

            @Override
            public void onEditTextValue() {
                if (onItemEcouponCountChangeListener != null) {
                    onItemEcouponCountChangeListener.onEditTextValue(mPosition);
                }
            }
        });
    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(RedeemableGiftListEntity.EVoucherListBean ecouponsItemEntity, int position) {
        mPosition = position;
        mId_msg_name.setText(ecouponsItemEntity.getEVoucherDesc());

        if (!TextUtils.isEmpty(ecouponsItemEntity.getEVoucherOriginalPrice())) {
            if (isDiscount) {
                mId_msg_date.setText(mContext.getString(R.string.EC15_5_a) + AndroidUtils.formatPrice(Double.valueOf(ecouponsItemEntity.getEVoucherUnitPrice()), false) + mContext.getString(R.string.EC05_3b_a));
            } else {
                mId_msg_date.setText(mContext.getString(R.string.EC15_5_a) + AndroidUtils.formatPrice(Double.valueOf(ecouponsItemEntity.getEVoucherOriginalPrice()), false) + mContext.getString(R.string.EC05_3b_a));
            }
        }
        addVouchersView.setMaxCount(50 + "");
        if (Integer.valueOf(ecouponsItemEntity.geteVoucherStock()) == 0) {
            mId_msg_error.setVisibility(View.VISIBLE);
            mId_msg_error.setText(mContext.getString(R.string.EC05_4_1));
            addVouchersView.setCurrentCnt(Integer.valueOf(ecouponsItemEntity.geteVoucherStock()));
            addVouchersView.setEnabledStatusWithZero(false);
            ecouponsItemEntity.setCurrentSelCnt(Integer.valueOf(ecouponsItemEntity.geteVoucherStock()));
            mId_msg_error.setVisibility(View.VISIBLE);
        } else {
            if (ecouponsItemEntity.getCurrentSelCnt() >= Integer.valueOf(ecouponsItemEntity.geteVoucherStock())) {
                addVouchersView.setEnabledEditAddStatus(false);
                addVouchersView.setCurrentCnt(Integer.valueOf(ecouponsItemEntity.geteVoucherStock()));
                ecouponsItemEntity.setCurrentSelCnt(Integer.valueOf(ecouponsItemEntity.geteVoucherStock()));
                mId_msg_error.setVisibility(View.VISIBLE);
                String errorMsg = mContext.getString(R.string.EC05_8).replace("xx", ecouponsItemEntity.geteVoucherStock());
                mId_msg_error.setText(errorMsg);
            } else {
                addVouchersView.setCurrentCnt(ecouponsItemEntity.getCurrentSelCnt());
                addVouchersView.setEnabledStatus(true);
                mId_msg_error.setVisibility(View.INVISIBLE);
            }
        }

        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + ecouponsItemEntity.getMerchantIconUrl()).into(mId_right_message);
    }
}
