package cn.swiftpass.wallet.intl.base.otp;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;

public interface OTPVerifyPresenter<V extends IView> extends IPresenter<V> {
    void verifyOTP(String walletId, String phone, String otp, String action);
}
