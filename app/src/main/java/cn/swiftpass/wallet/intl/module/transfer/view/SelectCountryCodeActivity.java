package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SortEntity;
import cn.swiftpass.wallet.intl.entity.TransferListEntity;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.SortAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.PinyinComparator;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.SideBar;

import static cn.swiftpass.wallet.intl.entity.Constants.TOP_HEADER_CHAR_CUSTOM;


public class SelectCountryCodeActivity extends BaseCompatActivity {

    @BindView(R.id.id_edit_search)
    ClearEditText idEditSearch;
    @BindView(R.id.id_recyclerView)
    RecyclerView idRecyclerView;
    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    private SortAdapter adapter;
    LinearLayoutManager manager;
    private List<SortEntity> mSourceDateList;
    private PinyinComparator pinyinComparator;
    private TransferListEntity transferListEntity;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        pinyinComparator = new PinyinComparator();
        idSideBar.setTextView(idDialog);
        setToolBarTitle(R.string.SH2_1_1);
        getBankList();
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contentEquals("\n")) {
                    return "";
                } else {
                    return null;
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter};
        idEditSearch.setFilters(filters);
        //根据输入框输入值的改变来过滤搜索
        idEditSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }


    private void getBankList() {
        ApiProtocolImplManager.getInstance().getBankList(mContext, "", "", "", new NetWorkCallbackListener<TransferListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);

            }

            @Override
            public void onSuccess(TransferListEntity response) {
                transferListEntity = response;
                List<TransferListEntity.BankListBean> rowsBeans = transferListEntity.getBankList();
                List<TransferListEntity.BankListBean> topRowsBeans = transferListEntity.getTopBankList();
                if (topRowsBeans != null && topRowsBeans.size() > 0) {
                    //其中四个银行要单独放在最前边--》特殊需求 用特殊字符来区分
                    for (int i = 0; i < topRowsBeans.size(); i++) {
                        topRowsBeans.get(i).setTempNames(Constants.TOP_HEADER_CHAR + Constants.TOP_HEADER_CHAR + Constants.TOP_HEADER_CHAR);
                    }
                    rowsBeans.addAll(topRowsBeans);
                }
                List<String> arrays = new ArrayList<>();
                boolean needContain = false;
                if (getIntent() != null && getIntent().getExtras() != null) {
                    needContain = getIntent().getExtras().getBoolean(Constants.HAS_EXTRA_SEL);
                }
                if (needContain) {
                    //默认server 返回的列表前边要追加一个本地的选项transfer_to_other
                    arrays.add(Constants.TOP_HEADER_CHAR_CUSTOM + getString(R.string.SH2_1_4));
                }

                String lan = SpUtils.getInstance(mContext).getAppLanguage();
                for (int i = 0; i < rowsBeans.size(); i++) {
                    if (!TextUtils.isEmpty(rowsBeans.get(i).getTempNames())) {
                        if (AndroidUtils.isZHLanguage(lan)) {
                            arrays.add(rowsBeans.get(i).getTempNames() + rowsBeans.get(i).getParticipantNameSc() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        } else if (AndroidUtils.isHKLanguage(lan)) {
                            arrays.add(rowsBeans.get(i).getTempNames() + rowsBeans.get(i).getParticipantNameTc() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        } else {
                            arrays.add(rowsBeans.get(i).getTempNames() + rowsBeans.get(i).getParticipantName() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        }
                    } else {
                        if (AndroidUtils.isZHLanguage(lan)) {
                            arrays.add(rowsBeans.get(i).getParticipantNameSc() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        } else if (AndroidUtils.isHKLanguage(lan)) {
                            arrays.add(rowsBeans.get(i).getParticipantNameTc() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        } else {
                            arrays.add(rowsBeans.get(i).getParticipantName() + Constants.SPECIAL_LETTER_BANK_TEXT + rowsBeans.get(i).getParticipantCode());
                        }
                    }
                }
                updateUi(arrays);
            }
        });

    }


    private void updateUi(List<String> arrays) {
        String[] dataStrs = arrays.toArray(new String[arrays.size()]);
        mSourceDateList = filledData(dataStrs);
        // 根据a-z进行排序源数据
        Collections.sort(mSourceDateList, pinyinComparator);
        //RecyclerView社置manager
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        idRecyclerView.setLayoutManager(manager);
        adapter = new SortAdapter(this, mSourceDateList);
        idRecyclerView.setAdapter(adapter);
        //item点击事件
        adapter.setOnItemClickListener(new SortAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                SortEntity sortEntity = (SortEntity) adapter.getItem(position);
                String name = sortEntity.getName();
                Intent intent = new Intent();
                if (name.contains(TOP_HEADER_CHAR_CUSTOM)) {
                    //选择的是本地加进去的选项 不是其中一个银行
                    intent.putExtra(Constants.BANK_NAME, getString(R.string.SH2_1_4));
                    intent.putExtra(Constants.BANK_CODE, "");
                } else {
                    String code = name.substring(name.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT) + Constants.SPECIAL_LETTER_BANK_TEXT.length(), name.length());
                    TransferListEntity.BankListBean bankListBean = (infilterWithBankCode(code));
                    String lan = SpUtils.getInstance(mContext).getAppLanguage();
                    if (AndroidUtils.isZHLanguage(lan)) {
                        intent.putExtra(Constants.BANK_NAME, bankListBean.getParticipantNameSc());
                    } else if (AndroidUtils.isHKLanguage(lan)) {
                        intent.putExtra(Constants.BANK_NAME, bankListBean.getParticipantNameTc());
                    } else {
                        intent.putExtra(Constants.BANK_NAME, bankListBean.getParticipantName());
                    }
                    intent.putExtra(Constants.BANK_CODE, bankListBean.getParticipantCode());
                }
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });
    }


    private TransferListEntity.BankListBean infilterWithBankCode(String bankCode) {
        List<TransferListEntity.BankListBean> bankListBeans = transferListEntity.getBankList();
        for (int i = 0; i < bankListBeans.size(); i++) {
            if (bankListBeans.get(i).getParticipantCode().equals(bankCode)) {
                return bankListBeans.get(i);
            }
        }
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_select_countrycode;
    }


    /**
     * 为RecyclerView填充数据
     *
     * @param date
     * @return
     */
    private List<SortEntity> filledData(String[] date) {
        List<SortEntity> mSortList = new ArrayList<>();
        for (int i = 0; i < date.length; i++) {
            SortEntity sortModel = new SortEntity();
            sortModel.setName(date[i]);
            //汉字转换成拼音
            String pinyin = Pinyin.toPinyin(date[i], "");
            String sortString = pinyin.substring(0, 1).toUpperCase();
            // 正则表达式，判断首字母是否是英文字母
            if (sortString.matches("[A-Z]")) {
                sortModel.setLetters(sortString.toUpperCase());
            } else if (sortString.equals(Constants.TOP_HEADER_CHAR) || sortString.equals(Constants.TOP_HEADER_CHAR_CUSTOM)) {
                sortModel.setLetters(Constants.TOP_HEADER_CHAR);
            } else {
                sortModel.setLetters("#");
            }
            mSortList.add(sortModel);
        }
        return mSortList;

    }

    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        if (adapter == null) return;
        List<SortEntity> filterDateList = new ArrayList<>();
        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = mSourceDateList;
        } else {
            if (mSourceDateList != null && mSourceDateList.size() > 0) {
                filterDateList.clear();
                for (SortEntity sortModel : mSourceDateList) {
                    String name = sortModel.getName();
//                    String firstSpell = PinyinUtils.getFirstSpell(name);
                    String firstSpell = Pinyin.toPinyin(name, "");
                    if (name.indexOf(filterStr.toString()) != -1 ||
                            firstSpell.startsWith(filterStr.toString()) ||
                            //不区分大小写||
                            firstSpell.toLowerCase().startsWith(filterStr.toString()) ||
                            firstSpell.toUpperCase().startsWith(filterStr.toString()) ||
                            firstSpell.contains(filterStr.toString().toLowerCase()) ||
                            firstSpell.contains(filterStr.toString().toUpperCase())) {
                        filterDateList.add(sortModel);
                    }
                }
            }
        }
        if (filterDateList.size() > 0) {
            // 根据a-z进行排序
            Collections.sort(filterDateList, pinyinComparator);
        }
        adapter.updateList(filterDateList);
    }

}
