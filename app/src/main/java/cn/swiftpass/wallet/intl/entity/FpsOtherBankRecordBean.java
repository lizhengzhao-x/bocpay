package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/10/9.
 * FPS他行绑定记录
 */

public class FpsOtherBankRecordBean extends BaseEntity {

    //-- 银行代码 012-中银
    String bankCode;
    // -- 银行名称;
    String bankName;
    //    // -- 收款账户
//    String accountNo;
    // -- 是否默认收款银行 Y:是 N:否
    String defaultBank;
    //-- 显示的英文名称
    String displayEngName;
    // --- 显示的中文名称
    String displayChinName;
    //-- 创建时间
    String createDate;
    //--- 最后一次修改时间
    String lastUpdateDate;

    public String getIsBankChina() {
        return isBankChina;
    }

    public void setIsBankChina(String isBankChina) {
        this.isBankChina = isBankChina;
    }

    // Y是 N否  判断是否中银
    String isBankChina;

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankcode) {
        this.bankCode = bankcode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDefaultBank() {
        return defaultBank;
    }

    public void setDefaultBank(String defaultBank) {
        this.defaultBank = defaultBank;
    }

    public String getDisplayEngName() {
        return displayEngName;
    }

    public void setDisplayEngName(String displayEngName) {
        this.displayEngName = displayEngName;
    }

    public String getDisplayChinName() {
        return displayChinName;
    }

    public void setDisplayChinName(String displayChinName) {
        this.displayChinName = displayChinName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
