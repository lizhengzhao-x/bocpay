package cn.swiftpass.wallet.intl.module.register;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.contract.BankRegisterVerifyCodeContract;
import cn.swiftpass.wallet.intl.module.register.presenter.BankRegisterVerifyCodePresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardRegisterCardInfoActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.CardIOExtraHelper;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;
import io.card.payment.CardIOActivity;
import io.card.payment.CardType;
import io.card.payment.CreditCard;

import static android.view.View.IMPORTANT_FOR_AUTOFILL_NO;


public class InputBankCardNumberFragment extends BaseFragment<BankRegisterVerifyCodeContract.Presenter> implements BankRegisterVerifyCodeContract.View {
    @BindView(R.id.tv_nextPage)
    TextView tvNextPage;
    @BindView(R.id.id_buttom_msg)
    TextView tvButtomMsg;
    @BindView(R.id.id_support_title)
    TextView tvSupportTitle;
    @BindView(R.id.edtwd_card_num)
    EditTextWithDel edtwdCardNum;
    private static final int REQUEST_SCAN = 100;
    private static final String TAG = "RegisterInputBankCardNumberActivity";
    @BindView(R.id.id_sub_title)
    TextView idSubTitle;
    @BindView(R.id.id_addcard_title)
    TextView idAddcardTitle;

    /**
     * mIv_verify_code 验证码显示框
     * mIv_refresh_code 验证码刷新按钮
     * mCet_verify_code 验证码输入框
     * mLl_verify_code 验证码布局
     */
    @BindView(R.id.iv_verify_code)
    ImageView mIv_verify_code;
    @BindView(R.id.iv_refresh_code)
    ImageView mIv_refresh_code;
    @BindView(R.id.cet_verify_code)
    CustomTvEditText mCetVerifyCode;
    @BindView(R.id.id_verifycode_view)
    LinearLayout mLl_verify_code;


    /**
     * 跳转type 1.注册 2.绑卡
     */
    private int mPageFlow;
    public String repalceSpaceStr = "  ";
    public String lastString = null;
    public int deleteSelect;

    /**
     * 忘记密码手机号
     */
    private String mForgetPwdPhone;

    /**
     * onResume的时候是否需要刷新验证码
     */
    private boolean isNeedRefreshCode = false;


    public static InputBankCardNumberFragment newInstance() {
        return new InputBankCardNumberFragment();
    }

    public void setPageFlow(int pageFlow) {
        this.mPageFlow = pageFlow;
    }

    /**
     * 初始化验证码控件
     */
    private void initVerifyCodeView() {
        mLl_verify_code.setVisibility(View.VISIBLE);
        mCetVerifyCode.getEditText().addTextChangedListener(textWatcher);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mCetVerifyCode.getEditText().setImportantForAutofill(IMPORTANT_FOR_AUTOFILL_NO);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        mCetVerifyCode.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mIv_refresh_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVerifyCode();
            }
        });
        getVerifyCode();
    }


    /**
     * 获取验证码
     */
    private void getVerifyCode() {
        mIv_refresh_code.setEnabled(false);
        if (mPresenter != null) {
            mPresenter.getBankRegisterVerifyCode(getContext());
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER && isNeedRefreshCode) {
            getVerifyCode();
        }
        showKeyBoard();
    }


    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    protected void checkPermission(final Activity mActivity) {
        if (mActivity == null) {
            return;
        }
        //检查是否有通讯录权限
        if (!isGranted_(Manifest.permission.CAMERA)) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, Manifest.permission.CAMERA)) {
                //拒绝并勾选不再询问
                PermissionInstance.getInstance().setHasFirstRejectContacts(false);
            } else {
                PermissionInstance.getInstance().setHasFirstRejectContacts(true);
            }
        }

        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                startScanCardActivity();
            }

            @Override
            public void rejectPermission() {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                    if (!PermissionInstance.getInstance().isHasFirstRejectContacts()) {
                        showLackOfPermissionDialog();
                    } else {
                        PermissionInstance.getInstance().setHasFirstRejectContacts(false);
                    }
                }
            }
        }, Manifest.permission.CAMERA);
    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.d("测试","onPause");

    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.d("测试",hidden?"show":"hidden");
    }

    private CustomDialog mDealDialog;


    private void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(getContext());
        builder.setTitle(getString(R.string.string_camera_use));
        builder.setMessage(getString(R.string.string_turn_on_camera));
        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(getContext());
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    /**
     * 弹出软键盘
     */
    public void showKeyBoard() {
        mFmgHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (edtwdCardNum == null)
                    return;
                edtwdCardNum.getEditText().setFocusable(true);
                edtwdCardNum.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(edtwdCardNum.getEditText());
            }
        }, 500);
    }


    private void startScanCardActivity() {

        //解决部分机型 语言不一致bug
        String language = SpUtils.getInstance(mContext).getAppLanguage();
        if (!TextUtils.isEmpty(language) && (language.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_TW) || language.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_HK_NEW))) // 繁体
        {
            language = "zh-Hant_TW";
        } else if (!TextUtils.isEmpty(language) && language.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US)) {

        } else if (!TextUtils.isEmpty(language) && language.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_CN)) {
            language = "zh-Hans";
        }

        Intent intent = new Intent(getContext(), CardIOActivity.class).
                putExtra(CardIOActivity.EXTRA_NO_CAMERA, false).
                putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true).
                putExtra(CardIOActivity.EXTRA_SCAN_EXPIRY, true).
                putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true).
                putExtra(CardIOActivity.EXTRA_USE_CARDIO_LOGO, false).//隐藏logo
                putExtra(CardIOActivity.EXTRA_KEEP_APPLICATION_THEME, false).//保持系统的主题
                putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.GREEN).
                putExtra(CardIOActivity.EXTRA_LANGUAGE_OR_LOCALE, language).
                putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true).//隐藏键盘
                putExtra(CardIOActivity.EXTRA_REQUIRE_CARDHOLDER_NAME, false).
                putExtra(CardIOActivity.EXTRA_SUPPRESS_CONFIRMATION, true).
                putExtra(CardIOActivity.EXTRA_GUIDE_COLOR, Color.WHITE).//扫描框颜色
                putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true).//隐藏logo
                putExtra(CardIOActivity.EXTRA_SCAN_INSTRUCTIONS, getString(R.string.scan_layout_guide_text)).
                putExtra(CardIOActivity.EXTRA_RETURN_CARD_IMAGE, false);//截图回来

        String cardNumberInputStr = getString(R.string.title_input_number);
        String toolBarTitleStr = "";
        int type = TempSaveHelper.getBankCardNumberType();
        if (type == Constants.PAGE_FLOW_REGISTER) {
            toolBarTitleStr = getString(R.string.IDV_2_1);
        } else if (type == Constants.PAGE_FLOW_BIND_CARD) {
            toolBarTitleStr = getString(R.string.bind_title);
        } else if (type == Constants.PAGE_FLOW_FORGETPASSWORD) {
            toolBarTitleStr = getString(R.string.setting_payment_short);
        }
        CardIOExtraHelper.addBackIconToCardIOActivity(mActivity.getApplication(), toolBarTitleStr, cardNumberInputStr);
        startActivityForResult(intent, REQUEST_SCAN);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            //因为重新排序之后setText的存在，从而会重新进行一次输入的过程
            //会导致输入框的内容从0开始输入，这里是为了避免这种情况产生一系列问题
            //此方法会替换从start开始的count个字符替换EditText旧的长度为before个字符即旧文本
            //这种情况下不会改变光标的位置，所以直接return掉
            if (start == 0 && count > 0) {
                return;
            }
            //如果是第一次设置值，如果当前的没有内容也直接return掉，默认为不改变光标位置
            String editTextContent = edtwdCardNum.getText();
            if (TextUtils.isEmpty(editTextContent) || TextUtils.isEmpty(lastString)) {
                return;
            }
            //然后根据输入的内容来得到加完空格之后要显示的内容
            editTextContent = AndroidUtils.addSpaceByInputContent(editTextContent, 22, repalceSpaceStr);

            //如果最新的长度 < 上次的长度，代表进行了删除, =表示进行了空格删除，则不改变光标的位置
            if (editTextContent.length() <= lastString.length()) {
                deleteSelect = start;
            } else {//如果是新增字符，这个时候光标的判断比较麻烦
                //先判断这时候新增的位置
                if (start + repalceSpaceStr.length() + count == editTextContent.length()) {//如果是在后面新增，则光标就在最后
                    deleteSelect = editTextContent.length();
                } else {//否则就要根据当前的某个位置来计算光标
                    deleteSelect = start + count;
                }

            }

        }

        @Override
        public void afterTextChanged(Editable editable) {
            //对输入的数字，每四位加一个空格
            //获取输入框中的内容,不可以去空格
            String etContent = edtwdCardNum.getEditText().getText().toString();
            if (TextUtils.isEmpty(etContent)) {
                updateOkBackground(false);
                return;
            }
            //重新加入空格拼接字符串
            String newContent = AndroidUtils.addSpaceByInputContent(etContent, 22, repalceSpaceStr);
            //保存本次字符串数据
            lastString = newContent;
            //如果有改变，则重新填充
            //防止EditText无限setText()产生死循环
            if (!etContent.equals(newContent)) {
                edtwdCardNum.setContentText(newContent);
                //保证光标的位置
                edtwdCardNum.getEditText().setSelection(deleteSelect > newContent.length() ? newContent.length() : deleteSelect);
            }

            //信用卡注册界面有验证码，需单独处理
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER && mCetVerifyCode.getInputText().length() != 4) {
                updateOkBackground(false);
                return;
            }

            updateOkBackground(lastString.length() >= 22);
        }
    };


    private void updateOkBackground(boolean isSel) {
        tvNextPage.setEnabled(isSel);
        tvNextPage.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        LogUtils.i(TAG, "onActivityResult(" + requestCode + ", " + resultCode + ", " + data + ")");
        String outStr = new String();
        if ((requestCode == REQUEST_SCAN) && data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
            CreditCard result = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
            if (result != null) {
                //需要对输入的卡号加入空格
                String content = AndroidUtils.addSpaceByInputContent(result.cardNumber, 22, repalceSpaceStr);
                edtwdCardNum.setContentText(content);
                outStr += "Card number: " + result.getRedactedCardNumber() + "\n";
                CardType cardType = result.getCardType();
                outStr += "Card mPageFlow: " + cardType.name() + " cardType.getDisplayName(null)=" + cardType.getDisplayName(null) + "\n";
                outStr += "Expiry: " + result.expiryMonth + "/" + result.expiryYear + "\n";
                outStr += "CVV: " + result.cvv + "\n";
                outStr += "Postal Code: " + result.postalCode + "\n";
                outStr += "Cardholder Name: " + result.cardholderName + "\n";
            }
        }
        LogUtils.i(TAG, "outStr:" + outStr);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_register_input_bankcard_number;
    }

    @Override
    protected void initView(View v) {

    }


    @OnClick(R.id.tv_nextPage)
    public void onViewClicked() {
        //信用卡注册界面有验证码，需单独处理
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
            if (mPresenter != null) {
                mPresenter.checkBankRegisterVerifyCode(getContext(), mCetVerifyCode.getInputText());
            }
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            //需要对内容进行去空格的处理
            String content = edtwdCardNum.getEditText().getText().toString().trim();
            //去掉所有空格
            content = content.replaceAll(" +", "");
            if (content.length() < 16) {
                showErrorMsgDialog(getContext(), getString(R.string.input_card_num_notice_1));
                return;
            }
            mHashMaps.put(Constants.DATA_CARD_NUMBER, content);
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
            if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER || mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                //虚拟卡注册
                mHashMaps.put(Constants.VIRTUALCARDREGISTERVERIFINVO, getIntent().getSerializableExtra(Constants.VIRTUALCARDREGISTERVERIFINVO));
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardRegisterCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                mHashMaps.put(Constants.MOBILE_PHONE, mForgetPwdPhone);
                ActivitySkipUtil.startAnotherActivity(getActivity(), RegisterInputBankCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }
        }
    }

    @Override
    public void initTitle() {
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
            //注册流程
            idAddcardTitle.setText(getString(R.string.title_register_bank_num));
            //初始化验证码控件
            initVerifyCodeView();
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            //绑卡流程
            idSubTitle.setText(getString(R.string.bind_title));
            idAddcardTitle.setText(getString(R.string.title_register_bank_num));
            mLl_verify_code.setVisibility(View.GONE);
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
            //忘记支付密码
            idSubTitle.setText(getString(R.string.string_find_pwd));
            mLl_verify_code.setVisibility(View.GONE);
            mForgetPwdPhone = getIntent().getExtras().getString(Constants.MOBILE_PHONE);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册
            tvSupportTitle.setVisibility(View.INVISIBLE);
            idSubTitle.setText(getString(R.string.VC04_04_2));
            idAddcardTitle.setText(getString(R.string.VC04_04_1));
            tvButtomMsg.setVisibility(View.GONE);
            mLl_verify_code.setVisibility(View.GONE);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡忘记密码
            tvSupportTitle.setVisibility(View.INVISIBLE);
            idSubTitle.setText(getString(R.string.VC04_04_2));
            idAddcardTitle.setText(getString(R.string.VC04_04_1));
            tvButtomMsg.setVisibility(View.GONE);
            mLl_verify_code.setVisibility(View.GONE);
        }
        TempSaveHelper.setBankCardNumberType(mPageFlow);

        edtwdCardNum.addRightImageView(R.mipmap.camera_bttn, false, 0, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //检测是否有相机权限
                if (!ButtonUtils.isFastDoubleClick(v.getId())) {
                    checkPermission(mActivity);
                }
            }
        });

        //默认按钮设为不可用状态
        updateOkBackground(false);
        edtwdCardNum.hideDelView();
        edtwdCardNum.hideErrorView();
        edtwdCardNum.addTextChangedListener(textWatcher);
    }

    @Override
    protected BankRegisterVerifyCodeContract.Presenter loadPresenter() {
        return new BankRegisterVerifyCodePresenter();
    }


    /**
     * 获取验证码成功
     *
     * @param response 返回的验证码
     */
    @Override
    public void getBankRegisterVerifyCodeSuccess(BankRegisterVerifyCodeEntity response) {
        mIv_refresh_code.setEnabled(true);
        int codeLen = 8;
        try {
            codeLen = Integer.valueOf(response.getVerifyCodeLength());
        } catch (Exception e) {
        }
        try {
            String bmpCode = response.getImageCode();
            Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
            mIv_verify_code.setImageBitmap(bmp);
            mCetVerifyCode.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
            mCetVerifyCode.setContentText("");
        } catch (Exception e) {
        }
        //dialog 隐藏之后 软键盘需要弹出
    }

    /**
     * 获取验证码失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getBankRegisterVerifyCodeFailed(String errorCode, String errorMsg) {
        mIv_refresh_code.setEnabled(true);
        showErrorMsgDialog(getContext(), errorMsg);
    }

    /**
     * 检查验证码成功
     *
     * @param response
     */
    @Override
    public void checkBankRegisterVerifyCodeSuccess(BankRegisterVerifyCodeCheckEntity response) {
        //需要对内容进行去空格的处理
        String content = edtwdCardNum.getEditText().getText().toString().trim();
        //去掉所有空格
        content = content.replaceAll(" +", "");
        if (content.length() < 16) {
            showErrorMsgDialog(getContext(), getString(R.string.input_card_num_notice_1));
            return;
        }
        final String contentFinal = content;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DATA_CARD_NUMBER, contentFinal);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER || mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡注册
            mHashMaps.put(Constants.VIRTUALCARDREGISTERVERIFINVO, getIntent().getSerializableExtra(Constants.VIRTUALCARDREGISTERVERIFINVO));
            ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardRegisterCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            isNeedRefreshCode = true;
            ActivitySkipUtil.startAnotherActivity(getActivity(), RegisterInputBankCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    /**
     * 检查验证码失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void checkBankRegisterVerifyCodeFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(getContext(), errorMsg);
        //重新获取验证码
        if (NetworkUtil.isNetworkAvailable()) {
            getVerifyCode();
        }
    }

    public void hideKeyboard() {
        if (edtwdCardNum != null) {
            AndroidUtils.hideKeyboard(edtwdCardNum);
        }
        if (mCetVerifyCode != null) {
            AndroidUtils.hideKeyboard(mCetVerifyCode);
        }
    }
}
