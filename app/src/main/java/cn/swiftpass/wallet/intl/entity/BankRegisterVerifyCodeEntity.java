package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */
public class BankRegisterVerifyCodeEntity extends BaseEntity {

    /**
     * imageCode 验证码base64
     * verifyCodeLengt 验证码长度
     */
    String imageCode;
    String verifyCodeLength;

    public String getVerifyCodeLength() {
        return verifyCodeLength;
    }

    public void setVerifyCodeLength(String verifyCodeLength) {
        this.verifyCodeLength = verifyCodeLength;
    }


    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }
}
