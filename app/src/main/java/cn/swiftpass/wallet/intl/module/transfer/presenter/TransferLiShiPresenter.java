package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferQueryBalanceProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferResourcesProtocol;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferLiShiFillInfoContract;

/**
 * 转账
 */
public class TransferLiShiPresenter extends TransferBasePresenter<TransferLiShiFillInfoContract.View> implements TransferLiShiFillInfoContract.Presenter {


    @Override
    public void getRegEddaInfo() {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                 //   getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                  //  getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartAccountInfo() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                  //  getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                   // getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();

    }

    @Override
    public void getAccountBalanceInfo() {
        new TransferQueryBalanceProtocol(new NetWorkCallbackListener<AccountBalanceEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                 //   getView().dismissDialog();
                    getView().getAccountBalanceInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(AccountBalanceEntity response) {
                if (getView() != null) {
                  //  getView().dismissDialog();
                    getView().getAccountBalanceInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getTransferResourceInfo() {
        new TransferResourcesProtocol(new NetWorkCallbackListener<TransferResourcesEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                  //  getView().dismissDialog();
                    getView().getTransferResourceInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(TransferResourcesEntity response) {
                if (getView() != null) {
                  //  getView().dismissDialog();
                    getView().getTransferResourceInfoSuccess(response);
                }
            }
        }).execute();
    }
}
