package cn.swiftpass.wallet.intl.module.topup;

import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 智s2用户充值 需要绑定一个卡号
 */
public class TopUpActivity extends BaseCompatActivity {

    private TextView mId_topup_name;
    private TextView mId_topup_idnumber;
    private TextView mId_topup_title;
    private RelativeLayout mId_sel_bank;
    private TextView mTv_bank;
    private LinearLayout mLl_line_common;
    private cn.swiftpass.wallet.intl.widget.EditTextWithDel mTv_cardnumber;
    private LinearLayout mId_conditions;
    private ImageView mIv_confirm_mark, id_right_arrow;
    private TextView mId_confirm_conditions;
    private TextView mTv_card_info_nextPage;
    private String mCurrentBankCode, mCurrency;
    private GetRegEddaInfoEntity mGetRegInfo;
    private int eventType;
    private String bankName, bankCode;



    private void bindViews() {
        mId_topup_name = (TextView) findViewById(R.id.id_topup_name);
        mId_topup_idnumber = (TextView) findViewById(R.id.id_topup_idnumber);
        mId_sel_bank = (RelativeLayout) findViewById(R.id.id_sel_bank);
        mTv_bank = (TextView) findViewById(R.id.tv_bank);
        mLl_line_common = (LinearLayout) findViewById(R.id.ll_line_common);
        mTv_cardnumber = (cn.swiftpass.wallet.intl.widget.EditTextWithDel) findViewById(R.id.tv_cardnumber);
        mId_conditions = (LinearLayout) findViewById(R.id.id_conditions);
        mIv_confirm_mark = (ImageView) findViewById(R.id.iv_confirm_mark);
        id_right_arrow = (ImageView) findViewById(R.id.id_right_arrow);
        mId_confirm_conditions = (TextView) findViewById(R.id.id_confirm_conditions);
        mTv_card_info_nextPage = (TextView) findViewById(R.id.tv_card_info_nextPage);
        mId_topup_title = (TextView) findViewById(R.id.id_topup_title);
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_D1_2_1_1);
        bindViews();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        eventType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        mGetRegInfo = (GetRegEddaInfoEntity) getIntent().getExtras().getSerializable(Constants.REG_EDDA_INFO);
        if (eventType == Constants.ADDEDDA) {
            mId_topup_title.setText(getString(R.string.P3_ewa_01_045));
            mTv_cardnumber.getEditText().addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    updateNextStatus(mIv_confirm_mark.isSelected() && mTv_cardnumber.getEditText().getText().toString().length() > 0);
                }
            });
            mId_sel_bank.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickSelBank();
                }
            });
        } else {
            //删除edda 直接显示
            mId_conditions.setVisibility(View.GONE);
            mId_topup_title.setText(getString(R.string.P3_ewa_01_046));
            try {
                MySmartAccountEntity MySmartAccountEntity = (MySmartAccountEntity) getIntent().getExtras().getSerializable(Constants.SMART_ACCOUNT);
                List<BankCardEntity> bankCardsBeans = MySmartAccountEntity.getBankCards();
                if (bankCardsBeans.get(0).getEddaStatus().equals("A")) {
                    //不可删除
                    setToolBarRightViewToText(R.string.P3_B1_1_b_1);
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            mTv_cardnumber.getEditText().setFocusable(false);
            mTv_card_info_nextPage.setVisibility(View.GONE);
            bankCode = getIntent().getExtras().getString(Constants.BANK_CODE);
            bankName = getIntent().getExtras().getString(Constants.BANK_NAME);
            mTv_cardnumber.setContentText(bankCode);
            mTv_cardnumber.getEditText().setEnabled(false);
            mTv_bank.setText(bankName);
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mTv_cardnumber.hideDelView();
                    AndroidUtils.hideKeyboard(mTv_cardnumber);
                }
            }, 200);
            mLl_line_common.setVisibility(View.INVISIBLE);
            mTv_cardnumber.setLineVisible(false);
            id_right_arrow.setVisibility(View.INVISIBLE);

            getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    sendEddaRequestInfo();
                }
            });
        }
        mTv_cardnumber.hideErrorView();
        mTv_cardnumber.hideDelView();
        mId_topup_name.setText(mGetRegInfo.getName());
        String idNumber = mGetRegInfo.getIdNum();
        mId_topup_idnumber.setText(AndroidUtils.formatIdNumber(idNumber));
        initAgreement();
        mTv_card_info_nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEddaRequestInfo();

            }
        });

        mIv_confirm_mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIv_confirm_mark.setSelected(!mIv_confirm_mark.isSelected());
                mIv_confirm_mark.setImageResource(mIv_confirm_mark.isSelected() ? R.mipmap.icon_check_choose_circle : R.mipmap.icon_check_choose_circle_default);

                updateNextStatus(mIv_confirm_mark.isSelected() && mTv_cardnumber.getEditText().getText().toString().length() > 0 && !TextUtils.isEmpty(mCurrentBankCode));
            }
        });


        updateNextStatus(false);
        //流程结束后 页面管理
        ProjectApp.getTempTaskStack().add(this);
    }

    private void updateNextStatus(boolean enable) {
        mTv_card_info_nextPage.setEnabled(enable);
        mTv_card_info_nextPage.setBackgroundResource(enable ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }


    private void sendEddaRequestInfo() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        if (eventType == Constants.ADDEDDA) {
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_PA);
            mHashMaps.put(Constants.DATA_SMART_ACTION, "RE");
        } else {
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_S2_BIND_DELETE_ACCOUNT);
            mHashMaps.put(Constants.DATA_SMART_ACTION, "DE");
        }
        mHashMaps.put(Constants.BANK_CODE, mCurrentBankCode);
        mHashMaps.put(Constants.BANK_NUMBER, mTv_cardnumber.getText().toString());
        mHashMaps.put(Constants.CURRENCY, mCurrency);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
        mHashMaps.put(Constants.PAGE_FROM_REGISTER, getIntent().getExtras().getBoolean(Constants.PAGE_FROM_REGISTER));
        ActivitySkipUtil.startAnotherActivity(TopUpActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void initAgreement() {
        String totalStr = getResources().getString(R.string.P3_D1_2_7_a);
        int startIndex = totalStr.indexOf("%");
        SpannableString spannableString = new SpannableString(totalStr.replace("%", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(TopUpActivity.this, ServerAgreementActivity.class);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        mId_confirm_conditions.setText(spannableString);
        mId_confirm_conditions.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * 选择区域 银行
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SEL_BANK) {
                mCurrentBankCode = data.getExtras().getString(Constants.BANK_CODE);
                String bankName = data.getExtras().getString(Constants.BANK_NAME);
                mCurrency = data.getExtras().getString(Constants.CURRENCY);
                mTv_bank.setText(bankName);
                updateNextStatus(mIv_confirm_mark.isSelected() && mTv_cardnumber.getEditText().getText().toString().length() > 0);
            }
        }
    }


    /**
     * 选择银行
     */
    public void onClickSelBank() {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.BANK_LIST, mGetRegInfo.getBankList());
        ActivitySkipUtil.startAnotherActivityForResult(TopUpActivity.this, SelectBankCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_BANK);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_topup;
    }
}
