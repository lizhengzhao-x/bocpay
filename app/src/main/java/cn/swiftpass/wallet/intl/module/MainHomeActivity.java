package cn.swiftpass.wallet.intl.module;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.LOCATION_TIP_ALREADY_SHOW;
import static cn.swiftpass.wallet.intl.entity.Constants.ACTION_BASE_ERROR;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;
import static cn.swiftpass.wallet.intl.module.ecoupon.view.ECouponsManagerFragment.CURRENTPAGE;
import static cn.swiftpass.wallet.intl.module.ecoupon.view.ECouponsManagerFragment.PAGE_MY;
import static cn.swiftpass.wallet.intl.module.home.MenuAction.ACTION_BIND_CARD;
import static cn.swiftpass.wallet.intl.module.home.MenuAction.ACTION_BIND_CARD_IN_HOME;
import static cn.swiftpass.wallet.intl.module.home.MenuAction.ACTION_BIND_SMART_ACCOUNT;
import static cn.swiftpass.wallet.intl.module.scan.backscan.BackScanFragment.LOCATION_IN_HK;
import static cn.swiftpass.wallet.intl.module.setting.location.LocationSettingActivity.LOCATION_SP;
import static cn.swiftpass.wallet.intl.module.transactionrecords.view.TransactionManagementFragment.JUMP_PAGE_POSITION;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferByStaticCodeFragment.STATICCODE_LIMIT_ENTITY;
import static cn.swiftpass.wallet.intl.utils.statusbar.StatusBarUtil.createTranslucentStatusBarView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bocpay.analysis.AnalysisManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;
import com.egoo.chat.ChatSDKManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.event.AutoLoginEventEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.HomeWatcherReceiver;
import cn.swiftpass.wallet.intl.app.HttpInitUtils;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.CustomNotificationDialog;
import cn.swiftpass.wallet.intl.dialog.ToastDialog;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.entity.RedPackageEntity;
import cn.swiftpass.wallet.intl.entity.RedpacketPopResourceEntity;
import cn.swiftpass.wallet.intl.entity.TurnOnRedPackEtntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkEvent;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkSkipEventEntity;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PWidEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.entity.event.RedPushEventEntity;
import cn.swiftpass.wallet.intl.entity.event.StaticCodeEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.EditPwdDialogEvent;
import cn.swiftpass.wallet.intl.event.JumpCenterNotificationEvent;
import cn.swiftpass.wallet.intl.event.PushNotificationEvent;
import cn.swiftpass.wallet.intl.event.RegisterPopBannerDialogEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.manage.OtpCountDownManager;
import cn.swiftpass.wallet.intl.module.activitys.NewMsgActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.NewCardManagerFragment;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpgradeAccountActivity;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.CreditCardRewardFragment;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.TransferCrossBorderPayeeFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.ECouponsManagerFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.home.HomeMainFragment;
import cn.swiftpass.wallet.intl.module.home.MenuAction;
import cn.swiftpass.wallet.intl.module.home.MenuItemAdapter;
import cn.swiftpass.wallet.intl.module.home.OnMenuItemClickListener;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.home.constants.MenuItemEnum;
import cn.swiftpass.wallet.intl.module.home.constants.MenuKey;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.redpacket.view.RedPacketMainFragment;
import cn.swiftpass.wallet.intl.module.register.RegisterSmartAccountDescActivity;
import cn.swiftpass.wallet.intl.module.scan.backscan.BackScanFragment;
import cn.swiftpass.wallet.intl.module.scan.majorscan.view.MajorScanFragment;
import cn.swiftpass.wallet.intl.module.setting.SettingFragment;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.help.HelpActivity;
import cn.swiftpass.wallet.intl.module.setting.location.LocationUIUtils;
import cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteShareActivity;
import cn.swiftpass.wallet.intl.module.staging.view.StagingItemCashFragment;
import cn.swiftpass.wallet.intl.module.staging.view.StagingItemMonthFragment;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.TransactionManagementFragment;
import cn.swiftpass.wallet.intl.module.transfer.entity.ApkVerifyEntity;
import cn.swiftpass.wallet.intl.module.transfer.view.FPSPaymentScanFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferByStaticCodeFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferSelTypeFragment;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardApplyActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardBindTermsFragment;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardConfirmFragment;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BocLaunchSDK;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;
import cn.swiftpass.wallet.intl.utils.GlideImageDownloadManager;
import cn.swiftpass.wallet.intl.utils.LocationUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MediaPlayerUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.RedPacketDialogUtils;
import cn.swiftpass.wallet.intl.utils.RedPacketResourceUtil;
import cn.swiftpass.wallet.intl.utils.SPFuncUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.BottomButtonView;


public class MainHomeActivity extends BaseCompatActivity<MainContract.Presenter> implements MainContract.View, BottomButtonView.OnBottomButtonCheckListener, OnMenuItemClickListener, GlideImageDownLoadListener, JumpCenterNotificationEvent.JumpCenterNotificationDialogDealOwner, EditPwdDialogEvent.EditPwdDialogDealOwner, RegisterPopBannerDialogEvent.RegisterPopBannerDialogDealOwner, AppCallAppEvent.AppCallAppDealOwner, PushNotificationEvent.PushNotificationDialogDealOwner {

    private static final String TAG = "MainHomeActivity";
    public static final String IS_AUTO_LOGIN = "IS_AUTO_LOGIN";
    public static final String IS_RESET_LANGUAGE = "IS_RESET_LANGUAGE";


    private static final int HIDE_MENU = 10000;
    private static final long TIME_VERIFY_PWD = 60 * 1000 * 10;
    @BindView(R.id.ry_menu)
    RecyclerView menuRy;
    @BindView(R.id.tv_version)
    TextView versionTv;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.drawer_layout_home)
    LinearLayout mDrawerLayoutLeft;
    @BindView(R.id.ll_main_body)
    LinearLayout mainBodyLayout;
    @BindView(R.id.toolbar_main)
    Toolbar mToolBarMain;
    @BindView(R.id.toolbar_title)
    TextView mToolBarTitleMain;
    @BindView(R.id.toolbar_title_icon)
    ImageView mToolBarIconMain;
    @BindView(R.id.toolbar_right_fl)
    LinearLayout mToolBarRightFlMain;
    @BindView(R.id.ll_setting)
    LinearLayout llSetting;
    @BindView(R.id.ll_help)
    LinearLayout llHelp;

    @BindView(R.id.fl_main)
    FrameLayout flMainView;

    private FragmentManager mFragmentManager;
    private BottomButtonView mBottomView;
    public BaseFragment mCurrentFragment;
    private HomeMainFragment homeMainFragment;
    private HomeWatcherReceiver mHomeKeyReceiver = null;


    private MenuItemAdapter menuItemAdapter;


    private ArrayList<MenuItemEntity> menuItemList;
    private String checkPwdAction;
//    private CustomNotificationDialog mNotificationDialog;


    //退出时的时间
    private long mExitTime;

    /**
     * 是否手动跳转到设定里头打开GPS开关
     */
    private boolean isManuallyOpen = false;

    /**
     * 进入被扫默认定位是不是香港
     */
    private boolean currentLocationIsHk;
    /**
     * 每次点击被扫切换定位是否成功
     */
    private boolean perLocationSuccess;


    private int currentPosition;
    private HashMap<String, Boolean> saveFragment = new HashMap<>();
    private int currentAction;
    private String menuEncryptionData = "";
    private String virtualCardUrl = "";
    private String virtualCardShareUrl = "";
    /**
     * 是否需要自动登录 1.有可能用户登录态 然后applink过来
     * 2.登录态 杀死进程
     * 3.登录态 断网进来
     */
    private boolean isNeedAutoLogin;
    private CountDownTimer timerHideAccount;
    //现有倒计时属于多少秒
    private long nowTime;
    /**
     * 加载图片的次数
     */
    private int mRetryLoadBitMapCnt = 0;
    private String bgVersion = "";
    private ObjectKey backgroudkey;
    private boolean useDefaultBackGround;

    /**
     * 涉及到onNewIntent调用 intent会覆盖
     */
    private Intent currentIntent;

    //红包重复拆 弹框
    private static CustomDialog redRedRePeatDailog;
    //默认为不需要，因为会有主动发起的主动登录
    public boolean needShowLoading;
    /**
     * 事件拦截 防止重复点击 主要是解决appcallapp/deepLink
     */
    private boolean isShouldInterceptTouchEvent = false;

    /**
     * 登出态过来的 deepLink 标志
     */
    private boolean isFromLogoutDeepLink = false;

    @Override
    protected MainContract.Presenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }


    @Override
    public void init() {
        //设置全屏 并且取消了状态栏的占位
        setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS);
        BocLaunchSDK.saveAppVersion(MainHomeActivity.this);
        //是否需要主动触发自动登录
        if (getIntent() != null) {
            isNeedAutoLogin = getIntent().getBooleanExtra(IS_AUTO_LOGIN, false);
        }
        //是否是登出态过来的deepLink
        if (getIntent() != null) {
            isFromLogoutDeepLink = getIntent().getBooleanExtra(Constants.IS_FROM_LOGOUT_DEEP_LINK, false);
        }
        initMainToolbar();
        //设置首页的背景图片
        initMainHomeBg();

        //整个HTTP请求初始化 必须加载
        HttpInitUtils.init(this);
//        checkHasPermission();
        mBottomView = (BottomButtonView) findViewById(R.id.id_bottom_view);
        mBottomView.setOnBottomButtonCheckListener(this);
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        currentIntent = getIntent();
        homeMainFragment = new HomeMainFragment();
        if (isNeedAutoLogin) {
            initLoginStatus();
            //开始触发自动登录的时候，防止OnStart触发接口请求
            homeMainFragment.setIgnoreLoadAccount(true);
        } else {
            Boolean isDealWithSuccess = UserLoginEventManager.getInstance().dealWithEvent();
            InitBannerEntity initBannerUrlParams = TempSaveHelper.getInitBannerUrlParams();
            if (initBannerUrlParams == null) {
                mPresenter.getAppLoginConfigParams("");
            }
            //用户第一次注册成功 显示推广通知弹框 切换语言会导致这里重新触发 不应该弹lbs弹框 IS_RESET_LANGUAGE作为判断标记
            if (!isDealWithSuccess && !currentIntent.getBooleanExtra(IS_RESET_LANGUAGE, false)) {
                showAcceptLocationPermission();
//                mPresenter.getAppParameters();
            }
        }
        initMenuView();
        mCurrentFragment = homeMainFragment;
        //每个fragment添加后需要保存到saveFragment中
        saveFragment.put(homeMainFragment.getClass().getSimpleName(), true);
        fragmentTrasaction.add(R.id.fl_main, mCurrentFragment);
        fragmentTrasaction.commitAllowingStateLoss();
        EventBus.getDefault().register(this);
        //校验APP 的SHA
        mPresenter.apkVerifyWithSHA(AndroidUtils.apkVerifyWithInfo(this));


        //判断自动隐藏资料区的时间 使用单例的目的，防止语言切换时，重新从10分钟开始倒计时
        if (SystemInitManager.getInstance().getNowHideTime() > 0) {
            initTimer(SystemInitManager.getInstance().getNowHideTime());
            startTimerHideAccountData();
        }

        //拉取证书 并且判断是否需要强制升级
        if (!Constants.BUILD_FLAVOR_SIT.equals(BuildConfig.FLAVOR)) {
            if (BuildConfig.DOWNLOAD_CER_SERVER) {
                if (TempSaveHelper.getCertificates() != null) {
                    //如果内存中已经保存 不需要再次拉取
                    LogUtils.i(TAG, "Certificates EXIT");
                } else {
                    AndroidUtils.loadIdvCerts(this);
                }
            }
        }

        flMainView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                flMainView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                TempSaveHelper.setMainFrameHeight(flMainView.getHeight());
                LogUtils.d("flMainView 高度", "" + flMainView.getHeight());
            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();
        initMainHomeBg();
        refreshBackground();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initMainHomeBg();
        registerHomeKeyReceiver();
        //主页从GPS打开返回 要定位
        boolean isServerOpen = TempSaveHelper.getLbsAppParameters() != null && TempSaveHelper.getLbsAppParameters().getMultipleSwitch() != null && !TempSaveHelper.getLbsAppParameters().getMultipleSwitch().isLbsClose();
        if (isServerOpen && isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) && isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (isManuallyOpen && LocationUtils.isLocationEnabled(this)) {
                isManuallyOpen = false;
                //app启动 用户手动打开GPS服务开关 返回app中更新开关状态
                HttpCoreSpUtils.put(LOCATION_SP, true);
            }
        }
    }


    public void refreshBackground() {
        if (mPresenter != null) {
            mPresenter.getBackgroundUrl(this);
        }
    }

    public void initMainHomeBg() {

        try {
            String mainBackground = SpUtils.getInstance().getMainBackground();
            String mainBgVersion = SpUtils.getInstance().getMainBgVersion();

            if (TextUtils.isEmpty(bgVersion)) {
                //bgVersion 这个值不能为空
                bgVersion = "0.0.1";
            }
            if (!bgVersion.equals(mainBgVersion)) {
                if (!TextUtils.isEmpty(mainBgVersion)) {
                    bgVersion = mainBgVersion;
                    backgroudkey = new ObjectKey(mainBgVersion);
                } else {
                    backgroudkey = new ObjectKey(bgVersion);
                }
            } else {
                if (backgroudkey == null) {
                    backgroudkey = new ObjectKey(bgVersion);
                }
            }

            if (!TextUtils.isEmpty(mainBackground)) {
                Uri uri = Uri.parse(mainBackground);
                loadBacImage(uri);
            } else {
                setDefaultBackground();
            }
        } catch (Exception e) {
            setDefaultBackground();
        }

    }


    public void setDefaultBackground() {
        useDefaultBackGround = true;
        RequestOptions options = new RequestOptions()
                .override(AndroidUtils.getScreenWidth(this), Target.SIZE_ORIGINAL);
        if (mContext != null && !isFinishing()) {
            GlideApp.with(this)
                    .asBitmap()
                    .skipMemoryCache(true)
                    .apply(options)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .load(R.mipmap.bg_post_login)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            SpUtils.getInstance().setMainBackground("");
                            SpUtils.getInstance().setMainBgVersion("");
                            getWindow().getDecorView().setBackground(new BitmapDrawable(resource));
                        }
                    });
        }

    }

    private void loadBacImage(Uri uri) {
        if (mContext == null || isFinishing()) {
            return;
        }
        useDefaultBackGround = false;
        Glide.with(this)
                .asBitmap()
                .load(uri)
                .signature(backgroudkey)
                .skipMemoryCache(true)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        //这里需要设置到getWindow().getDecorView() 中，不能是contentView中
                        getWindow().getDecorView().setBackground(new BitmapDrawable(resource));
                        mRetryLoadBitMapCnt = 0;
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        if (mRetryLoadBitMapCnt >= 1) return;
                        mRetryLoadBitMapCnt += 1;
                        //本地图片有可能会被清除掉 重新加载本地的图片
                        bgVersion = "0.0.1";
                        //1.清除掉本地的图片资源路径
                        //2.本地保存图片版本清除掉
                        SpUtils.getInstance().setMainBackground("");
                        SpUtils.getInstance().setMainBgVersion("");
                        setDefaultBackground();
                    }
                });
    }


    @Override
    public void resetToolbar() {

        if (getSupportActionBar() != null && mToolBarTitleMain != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
        }

        if (mToolBarRightFlMain != null) {
            mToolBarRightFlMain.removeAllViews();
        }
        if (mToolBarTitleMain != null) {
            mToolBarTitleMain.setText("");
        }
        if (mToolBarIconMain != null) {
            mToolBarIconMain.setImageDrawable(null);
        }

    }


    /**
     * 初始化Main 界面的toolbar
     * main界面的toolbar使用的是自定义的toolbar
     * 因为main 界面的 toolbar 需要根据fragment 不停的切换不同的样式
     */
    private void initMainToolbar() {
        //设置状态栏的字体颜色为白色
        AndroidUtils.setStatusBar(this, true);
        //添加一个和状态高度一样的透明View， 防止Toolbar 浸入 状态栏中
        mainBodyLayout.addView(createTranslucentStatusBarView(this, 0), 0);
        mToolBarMain.setNavigationIcon(R.drawable.ic_menu_white);
        setSupportActionBar(mToolBarMain);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white);
        }
        initToolBarRightView(mToolBarRightFlMain);
        initToolbarTitleView(mToolBarTitleMain);
    }

    /**
     * toolbar 中间的 imageView
     *
     * @return
     */
    public ImageView getToolBarIconMain() {
        return mToolBarIconMain;
    }

    /**
     *
     */
    private void showAcceptLocationPermission() {
        boolean isLocationDialogShow = (Boolean) HttpCoreSpUtils.get(LOCATION_TIP_ALREADY_SHOW, false);
        //本地有没有开启lbs开关 有一种情况是 用户第一次登陆没有等到lbs开关弹框弹出 自己手动到设定里头开启这样就不用再打开lbs开关
        boolean isLocationServiceOpen = (Boolean) SPFuncUtils.get(LOCATION_SP, false);
        if (!isLocationDialogShow && CacheManagerInstance.getInstance().isLogin() && !isLocationServiceOpen) {
            showSelLocationDialog();
            HttpCoreSpUtils.put(LOCATION_TIP_ALREADY_SHOW, true);
        }
    }

    /**
     * 检测是否是登录态 如果是 自动登录操作
     * 1.自动登录 不能并发 会导致登出问题 所以是在登录态所有接口的前置接口
     * 2.ServerPublicKeyProtocol 是秘钥交换 涉及到加解密的也依赖这个接口 所以是所有涉及到加解密的接口前置
     * <p>
     * 这里的设想是 app启动第一个接口是设备登录 如果是登录态 直接设备登录 设备登录成功才后 《并发就没有问题》去掉其他依赖秘钥交换的接口
     * 如果是登录态 设备登录失败 其他业务也肯定失败 无需在调用
     * 如果不是登录态 非登录态调用其他需要加密的接口 会进行秘钥交换
     */
    private void initLoginStatus() {
        if (CacheManagerInstance.getInstance().isLogin() && !CacheManagerInstance.getInstance().isHasSessionId()) {
            //登录态 延迟请求接口，确保主页页面先完成绘制
            if (mHandler != null) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        needShowLoading = true;
                        if (homeMainFragment != null) {
                            homeMainFragment.showLoading();
                        }
                        mPresenter.doAutoLogin(true);
                    }
                }, 200);
            }
        } else {
            //有一种异常是 登录otp界面 验证otp过程中loading 退到后台 startActivity流程 导致app重启，但是Application没有重启 单例保存的内存变量没有清除 bug是生活场景区域不显示
            //TODO这里只是异常处理 切换语言也会类似问题
            if (mPresenter != null && TempSaveHelper.getInitBannerUrlParams() == null) {
                mPresenter.getAppLoginConfigParams("");
            }
        }
    }

    /**
     * 初始化菜单栏，并绑定初始化数据
     * 初始化数据为本地数据
     */
    @SuppressLint("SetTextI18n")
    private void initMenuView() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolBarMain, R.string.LBS209_7_3, R.string.LBS209_4_3) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AndroidUtils.setStatusBar(MainHomeActivity.this, false);
                if (mPresenter != null) {
                    mPresenter.getMenuList();
                }
                notifyDrawerOpened();
            }


            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                AndroidUtils.setStatusBar(MainHomeActivity.this, true);
                if (menuItemAdapter != null) {
                    menuItemAdapter.clearSelect();
                    menuItemAdapter.notifyDataSetChanged();
                }
                notifyDrawerClosed();
            }
        };
        //设取消滑动动画
        mToggle.setDrawerSlideAnimationEnabled(false);
        mDrawerLayout.addDrawerListener(mToggle);
        //同步DrawerLayout的状态
        mToggle.syncState();
        //setNavigationIcon  必须是mToggle.syncState()之后设定
        mToolBarMain.setNavigationIcon(R.drawable.ic_menu_white);
        versionTv.setText("v" + BuildConfig.VERSION_NAME);
        menuRy.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        menuRy.setItemAnimator(null);
        menuItemAdapter = new MenuItemAdapter(this, this);
        menuItemList = MenuItemEnum.toList(this);
        menuItemAdapter.setList(MenuItemEnum.toDefaultList(this));
        menuRy.setAdapter(menuItemAdapter);

        new HashMap<>();
    }

    /**
     * 菜单栏打开 需求让停止时扫码
     */
    private void notifyDrawerOpened() {

        if (mCurrentFragment instanceof MajorScanFragment) {
            MajorScanFragment majorScanFragment = (MajorScanFragment) mCurrentFragment;
            majorScanFragment.stopPreview();
        }

    }

    /**
     * 菜单栏关闭 需求让重新开始扫码
     */
    private void notifyDrawerClosed() {
        if (mCurrentFragment instanceof MajorScanFragment) {
            MajorScanFragment majorScanFragment = (MajorScanFragment) mCurrentFragment;
            majorScanFragment.restartPreviewAfterDelay(200);
        }

    }


    /**
     * 当主界面存在时，applink 会调用这个方法
     *
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        currentIntent = intent;
        if (intent.getIntExtra(Constants.ACTION_TYPE, -1) == ACTION_BASE_ERROR) {
            //底层不管是验证密码错误/otp错误到达首页 判断是否是appLink流程 然后清除
            //APPCALLAPP 很多错误码 app call app流程中 /登出
            if (!TextUtils.isEmpty(TempSaveHelper.getMerchantLink())) {
                TempSaveHelper.clearLink();
                UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
            }
        } else {
            //比如用户在主页 点击通知栏消息重新进入主页会触发这里逻辑 app call app一样
            isNeedAutoLogin = intent.getBooleanExtra(IS_AUTO_LOGIN, false);
            if (isNeedAutoLogin) {
                initLoginStatus();
                //开始触发自动登录的时候，防止OnStart触发接口请求
                homeMainFragment.setIgnoreLoadAccount(true);
            } else {
                if (mHandler != null) {
                    //TODO 这里有一个问题 SplashActivity->DeepLinkActivity->MainHomeActivity(onNewIntent )
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //需要隐藏之前可能存在的其他非首页的fragment接口触发的loading
                            dismissDialog();
                            ApiProtocolImplManager.getInstance().dismissDialog();
                            UserLoginEventManager.getInstance().dealWithEvent();
                        }
                    }, 0);
                }
            }
        }
    }

    @Override
    public void getNotificationStatusSuccess(CustomNotificationDialog mNotificationDialog, NotificationStatusEntity notificationStatusEntity) {
        if (mNotificationDialog == null) return;
        ToastDialog toastDialog = ToastDialog.createToastDialog(this);
        toastDialog.show();
        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toastDialog.dismiss();
                }
            }, 2000);
        }
        mNotificationDialog.dismissProgressDialog();
        mNotificationDialog.dismiss();
        mNotificationDialog = null;
    }

    @Override
    public void getNotificationStatusFailed(CustomNotificationDialog mNotificationDialog, String errorCode, String errorMsg) {
        if (mNotificationDialog != null) {
            mNotificationDialog.dismissProgressDialog();
        }
        showRetryDialog(mNotificationDialog, errorMsg);
    }

    private void showRetryDialog(CustomNotificationDialog mNotificationDialog, String errorMsg) {
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(errorMsg);
        builder.setMessage("");
        builder.setTextBold(true);
        builder.setPositiveButton(getString(R.string.NT2101_1_11), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getString(R.string.NT2101_1_10), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (mNotificationDialog != null) {
                    mNotificationDialog.dismiss();
//                    mNotificationDialog = null;
                }
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }


    /**
     * 选择是否记录位置
     */
    private void showSelLocationDialog() {
        LocationUIUtils.showSelLocationDialog(this, new LocationUIUtils.DialogSelListener() {
            @Override
            public void onSelOkListener() {
                checkLocationService();
            }

            @Override
            public void onSelCancelListener() {

            }
        });
    }

    private void checkLocationService() {
        LocationUIUtils.checkLocationService(MainHomeActivity.this, new LocationUIUtils.DialogSelListener() {
            @Override
            public void onSelOkListener() {
                updateLocationStatus();
            }

            @Override
            public void onSelCancelListener() {

            }
        });
    }

    public void updateLocationStatus() {
        if (LocationUtils.isLocationEnabled(this)) {
            //保存app设定里头开关打开状态
            HttpCoreSpUtils.put(LOCATION_SP, true);
        } else {
            showOpenLocationServiceDialog();
        }
    }

    public void getLocation(LocationUIUtils.LocationLoadListener locationLoadListener) {
        LocationUtils.registerLocationWifi(MainHomeActivity.this, 0, 0, new LocationUtils.OnLocationChangeListener() {
            @Override
            public void getLastKnownLocation(Location location) {
                LogUtils.i(TAG, "getLastKnownLocation->");
                locationChanged(location, locationLoadListener);
            }

            @Override
            public void onLocationChanged(Location location) {
                LogUtils.i(TAG, "onLocationChanged->");
                locationChanged(location, locationLoadListener);
            }

            @Override
            public void onLocationClose() {

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                LogUtils.i(TAG, "onStatusChanged->");
            }
        });
    }


    /**
     * 定位服务
     */
    private void showOpenLocationServiceDialog() {
        LocationUIUtils.showOpenLocationServiceDialog(MainHomeActivity.this, new LocationUIUtils.DialogSelListener() {
            @Override
            public void onSelOkListener() {
                isManuallyOpen = true;
                LocationUtils.openGpsSettings(MainHomeActivity.this);
            }

            @Override
            public void onSelCancelListener() {

            }
        });
    }


    private void locationChanged(Location location, LocationUIUtils.LocationLoadListener locationLoadListener) {
        LocationUIUtils.onLocationChangedEvent(this, location, locationLoadListener);
    }


    /**
     * 点击通知推送时，弹出的虚拟卡绑定 弹出窗口
     */
    private void showBindVirtualCardDialog() {
        AndroidUtils.showTipDialog(MainHomeActivity.this, getString(R.string.VC01_03_5), getString(R.string.VC01_03_6),
                getString(R.string.VC01_03_7), getString(R.string.VC01_03_8), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!CacheManagerInstance.getInstance().isLogin()) {
                            ActivitySkipUtil.startAnotherActivity(MainHomeActivity.this, LoginActivity.class);
                            finish();
                        } else {
                            dismissDialog();
                            inputPswPopWindow(MenuAction.ACTION_OPEN_VIRTUAL_CARD);
                        }
                    }
                });
    }

    /**
     * 点击通知推送时，打开普通红包
     */
    private void showRedCommonPacketOpenDialog(RedPackageEntity packageEntity, Activity activity) {
        if (null == packageEntity) {
            return;
        }

        RedPacketDialogUtils.openRedPacket(activity, packageEntity.getPushRrcRefNo(), Constants.RED_PACKET_PUSH_TYPE);
    }


    /**
     * 很多 例如绑卡/解绑完成之后要退到首页 刷新卡列表等
     *
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_HOME || event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME) {
            LogUtils.i(TAG, "onEvent home");
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
            //用户按home或者锁屏按键的时候 需要关闭被扫界面
            //有可能从菜单栏进去 到首页之后需要关闭菜单到首页之后 如果菜单栏是打开的 需要关闭
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawers();
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_SHOW_HOMEPAGE) {
            //显示首页
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_NOTIFIACTION_VIRTUALCARD_BIND) {
            showBindVirtualCardDialog();
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_CLOSE_GPS_SERVICE) {
            LocationUtils.unregister();
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_CLOSE_LEFT_MENU) {
            mDrawerLayout.closeDrawers();
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_START_TIMER) {
            startTimerHideAccountData();
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER) {
            if (mCurrentFragment != null && (mCurrentFragment instanceof MajorScanFragment || mCurrentFragment instanceof BackScanFragment)) {
                //点击home按键  主扫被扫都返回
                EventBus.getDefault().postSticky(new PasswordEventEntity(PasswordEventEntity.EVENT_CLOSE_VERIFY_PASSWORD, ""));//关闭键盘，如果键盘打开的话
                OnBottomButtonCheck(0);
                mBottomView.selectWithPosition(0);
                //并且如果存在验证密码弹框 需要关闭
            }

            //锁屏需要关闭红包音乐
            MediaPlayerUtils.stopPlayMusic();
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_BIND_CARD_BACK_HOME) {
            inputPswPopWindow(ACTION_BIND_CARD_IN_HOME);
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_BIND_CARD) {
            inputPswPopWindow(ACTION_BIND_CARD);
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            mPresenter.getSmartAccountInfo();
            //因为home 界面每次可视的时候都会拉取数据，所以不需要在重新触发刷新
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_VIRTUAL_LIST_FAILED) {
            NewCardManagerFragment cardManagerFragment = new NewCardManagerFragment();
            switchDiffFragmentContent(cardManagerFragment, R.id.fl_main, -1, false);
        }

    }


    /**
     * 接收页面跳转的 event
     * APP 没有启动 或者在登录页面 在parseIntent里面处理
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RedPushEventEntity event) {
        if (event.getEventType() == RedPushEventEntity.EVENT_KEY_NOTIFIACTION_COMMON_RED_PACKET_OPEN) {
            showRedCommonPacketOpenDialog(event.getRedPackageEntity(), MyActivityManager.getInstance().getCurrentActivity());
        } else if (event.getEventType() == RedPushEventEntity.EVENT_KEY_NOTIFIACTION_STAFF_RED_PACKET_OPEN) {
            mPresenter.getStaffRedPacket(true, Constants.IS_STAFF_PUSH);
        }
    }

    /**
     * 接收页面跳转的 event
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PagerEventEntity event) {
        if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT) {
            NewCardManagerFragment cardManagerFragment = new NewCardManagerFragment();
            switchDiffFragmentContent(cardManagerFragment, R.id.fl_main, -1, false);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ECOUPON) {
            ECouponsManagerFragment eCouponsManagerFragment = new ECouponsManagerFragment();
            switchDiffFragmentContent(eCouponsManagerFragment, R.id.fl_main, -1, false);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_INVITE_SHARE) {
//            InviteShareFragment shareFragment = new InviteShareFragment();
//            switchDiffFragmentContent(shareFragment, R.id.fl_main, -1, false);
            //推荐亲友二级界面
            ActivitySkipUtil.startAnotherActivity(getActivity(), InviteShareActivity.class);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_FPS_ID) {
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.TRANSFER_BY_FPS);
            mPresenter.checkSmartAccountBind(menuItmByEnum);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_BANK) {
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.TRANSFER_BY_BANK);
            mPresenter.checkSmartAccountBind(menuItmByEnum);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_LISHI) {
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.RED_POCKET);
            mPresenter.checkSmartAccountBind(menuItmByEnum);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_UPLAN) {
            AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_HOME_PAGE_UPLAN, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
            sendAnalysisEvent(analysisButtonEntity);
            mPresenter.getUplanUrl();
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_NEWS_PAGE) {
//            mBottomView.clearSelect();
//            NewMsgFragment newMsgFragment = new NewMsgFragment();
//            switchDiffFragmentContent(newMsgFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_BACK_SCAN, false);
            //最新消息二级界面
            ActivitySkipUtil.startAnotherActivity(getActivity(), NewMsgActivity.class);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_CROSS_BORDER_PAYEE) {
            mPresenter.getTransferBaseData();
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_CREDIT_CARD_REWARD) {
            CreditCardRewardFragment newMsgFragment = new CreditCardRewardFragment();
            switchDiffFragmentContent(newMsgFragment, R.id.fl_main, -1, false);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_VIRTUAL_CARD_APPLY) {
            if (!TextUtils.isEmpty(virtualCardUrl)) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                mHashMaps.put(WebViewActivity.SHARE_CONTENT, virtualCardShareUrl);
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                mPresenter.getAppLoginConfigParams(MenuItemEnum.VIRTUAL_CARD_APPLY.action);
            }
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_RED_PACKET_GET_FRAGMENT) {
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.RED_POCKET_GET_LISHI);
            if (TextUtils.equals(event.getEventMessage(), Constants.IS_SHOW_DAILOG)) {
                mPresenter.checkSmartAccountBind(menuItmByEnum);
            } else {
                mPresenter.checkSmartAccountBind(menuItmByEnum, false);
            }
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_SCAN) {
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.PAYMENT_BY_FPS);
            mPresenter.checkSmartAccountBind(menuItmByEnum);
        } else if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_LIST) {
            TransactionManagementFragment transactionManagementFragment = new TransactionManagementFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(JUMP_PAGE_POSITION, 1);
            transactionManagementFragment.setArguments(bundle);
            switchDiffFragmentContent(transactionManagementFragment, R.id.fl_main, -1, false);
        }


    }


    /**
     * 由登出态的deepLink进入时
     * homeMainFragment的toolBar绘制完成后，进行deepLink一级界面fragment的切换
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(DeepLinkEvent event) {
        if (event.getEventType() == DeepLinkEvent.DEEP_LINK && isFromLogoutDeepLink) {
            String actionUrl = currentIntent.getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
            DeepLinkUtils.skipByDeepLinkType(actionUrl, null, 0, DeepLinkUtils.SKIP_TYPE_MAIN_HOME);
            isFromLogoutDeepLink = false;
        }
    }


    /**
     * 接收解卡或者绑卡，以及刷新卡列表的event
     * 这些event 因为卡列表的改变，会导致当前用户的类型的改变
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_CLOSE_VIRTUALCADR_PAGE) {
            //显示首页
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
        } else if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD
                || event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            if (!(mCurrentFragment instanceof NewCardManagerFragment)) {
                mPresenter.refreshCardList();
            }
            if (!TextUtils.isEmpty(TempSaveHelper.getAppLink())) {
                //4.为了解决 appLink过来，如果绑卡成功成功 重新触发流程 需要在这里处理
                AppCallAppPaymentActivity.startAppCallApp(this, TempSaveHelper.getAppLink(), TempSaveHelper.getMerchantLink(), TempSaveHelper.getNetworkErrorLink(), TempSaveHelper.isIsH5Pay());
            }
        }
    }


    /**
     * 处理Uplan webview Js调用
     * Uplan页面 通过webview的Js 调用到付款码页面的时候，需要event 到主页后，把数据传递给付款码页面
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UplanEntity event) {
        if (event.getEventType() == UplanEntity.EVENT_UPLAN_BACK_SCAN) {
            //电子券跳转到被扫
            BackScanFragment backScanFragment = new BackScanFragment();
            if (!TextUtils.isEmpty(event.getAction()) && !TextUtils.isEmpty(event.getCouponInfo())) {
                backScanFragment.setData(event.getAction(), event.getCouponInfo());
            }
            switchDiffFragmentContent(backScanFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_BACK_SCAN, false);
            currentPosition = 2;
            mBottomView.selectWithPosition(2);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AutoLoginEventEntity event) {
        if (event.getEventType() == AutoLoginEventEntity.EVENT_AUTO_LOGIN_SUCCESS) {
            queryDataAfterAutoLogin(event.getAutoLoginSucEntity());
        }
    }


    /**
     * 电子券 业务逻辑
     * 完成电子券兑换
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EcoupEventEntity event) {
        if (event.getEventType() == EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP) {
            //显示首页
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
        }
    }


    /**
     * PWid 拒纳信息
     * 弹框后返回主页
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PWidEventEntity event) {
        if (event.getEventType() == PWidEventEntity.EVENT_HOME_PAGE) {
            //显示首页
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
        }
    }


    /**
     * AA收款设置信息跳转到二维码界面
     *
     * @param
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StaticCodeEventEntity staticCodeEventEntity) {
        TransferByStaticCodeFragment staticCodeFragment = new TransferByStaticCodeFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(STATICCODE_LIMIT_ENTITY, staticCodeEventEntity.getTransferSetLimitEntity());
        staticCodeFragment.setArguments(bundle);
        switchDiffFragmentContent(staticCodeFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC, false);
        mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC);
    }


    /**
     * 转账结果 页面跳转逻辑
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            TransactionManagementFragment managementFragment = new TransactionManagementFragment();
            switchDiffFragmentContent(managementFragment, R.id.fl_main, -1, false);
        } else if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS) {
            OnBottomButtonCheck(0);
            mBottomView.selectWithPosition(0);
        } else if (event.getEventType() == TransferEventEntity.EVENT_RED_PACKET_OPEN) {
            if (null != homeMainFragment) {
                homeMainFragment.queryLifeSceneAreaCount();
            }
        }
    }


    /**
     * 处理 deepLink页面跳转
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(DeepLinkSkipEventEntity event) {
        int type = event.getEventType();
        switch (type) {
            case DeepLinkUtils.DEEP_LINK_TYPE_U_PLAN_STORE://UPlan指定门店 二级界面
                DeepLinkUtils.goToUPlanStore(event.getEventMessage(), getActivity());
                break;
            case DeepLinkUtils.DEEP_LINK_TYPE_MGM://MGM二级界面
                startActivity(new Intent(MainHomeActivity.this, InviteShareActivity.class));
                break;
            case DeepLinkUtils.DEEP_LINK_TYPE_U_PLAN://U_PLAN二级界面
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_HOME_PAGE_UPLAN, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                mPresenter.getUplanUrl();
                break;
            case DeepLinkUtils.DEEP_LINK_TYPE_E_COUPON:
                ECouponsManagerFragment eCouponsManagerFragment = new ECouponsManagerFragment();
                switchDiffFragmentContent(eCouponsManagerFragment, R.id.fl_main, -1, false);
                break;
            case DeepLinkUtils.DEEP_LINK_TYPE_CROSS_BORDER:
                if (event.getTransferCrossBorderBaseEntity() != null) {
                    //webView过来的
                    TransferCrossBorderPayeeFragment transferCrossBorderPayeeFragment = new TransferCrossBorderPayeeFragment();
                    transferCrossBorderPayeeFragment.setBaseEntity(event.getTransferCrossBorderBaseEntity());
                    switchDiffFragmentContent(transferCrossBorderPayeeFragment, R.id.fl_main, -1, false);
                } else {
                    //deepLink过来的
                    mPresenter.getTransferBaseData();
                }
                break;
            case DeepLinkUtils.DEEP_LINK_TYPE_REWARD_REGISTER:
                DeepLinkUtils.goToAppointRewardRegisterActivity(getActivity(), event.getEventMessage());
                break;
            case -1:
                //报错
                break;
        }
    }


    /**
     * 监听home按键 主要是解决被扫页面关闭问题
     */
    private void registerHomeKeyReceiver() {
        LogUtils.i(TAG, "registerHomeKeyReceiver");
        mHomeKeyReceiver = new HomeWatcherReceiver();
        final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        getApplicationContext().registerReceiver(mHomeKeyReceiver, homeFilter);
    }

    /**
     * 取消home按键
     */
    private void unregisterHomeKeyReceiver() {
        LogUtils.i(TAG, "unregisterHomeKeyReceiver");
        if (null != mHomeKeyReceiver) {
            getApplicationContext().unregisterReceiver(mHomeKeyReceiver);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        LogUtils.i(TAG, "onDestroy----------->");
        unregisterHomeKeyReceiver();
        EventBus.getDefault().unregister(this);

        if (timerHideAccount != null) {
            SystemInitManager.getInstance().setNowHideTime(nowTime);
            timerHideAccount.cancel();
        }
//        AnalysisManager.getInstance().stopAnalysisService();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            ChatSDKManager.reset();
        }
    }


    /**
     * fragment页面动态切换
     *
     * @param toFragment
     * @param resId
     * @param position
     */
    protected void switchDiffFragmentContent(BaseFragment toFragment, int resId, int position, boolean isSave) {

        //切换fragment时校验背景图是否需要刷新
      /*  String nowMainBgVersion = SpUtils.getInstance().getMainBgVersion();
        if (!bgVersion.equals(nowMainBgVersion)){
            initMainHomeBg();
        }*/

        if (null == mCurrentFragment || null == toFragment) {
            return;
        }

        //同一个页面切换 不用重新加载 底部 tab栏位切换 菜单栏条目切换 fragment不用每次创建
        if (toFragment.getClass().getSimpleName().equals(mCurrentFragment.getClass().getSimpleName())) {
            if (toFragment instanceof BackScanFragment) {
            } else {
                return;
            }
        }

        if (toFragment instanceof MajorScanFragment) {
            ((RelativeLayout.LayoutParams) flMainView.getLayoutParams()).removeRule(RelativeLayout.ABOVE);
        } else {
            ((RelativeLayout.LayoutParams) flMainView.getLayoutParams()).addRule(RelativeLayout.ABOVE, R.id.id_bottom_view);
        }

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        //切换到主界面中的，每个fragment都需要被记录到saveFragment中
        saveFragment.put(toFragment.getClass().getSimpleName(), isSave);
        if (!toFragment.isAdded()) {
            fragmentTransaction.hide(mCurrentFragment);
            fragmentTransaction.add(resId, toFragment, toFragment.getClass().getSimpleName());
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            //切换时需要重新设置activity的toolbar
            resetToolbar();
            toFragment.initTitle();
            fragmentTransaction.hide(mCurrentFragment);
            fragmentTransaction.show(toFragment);
            fragmentTransaction.commitAllowingStateLoss();
        }

        if (null != homeMainFragment && toFragment == homeMainFragment && !TempSaveHelper.isInitBannerUrlSuccess()) {
            mPresenter.getAppLoginConfigParams("");
        }


        mCurrentFragment = toFragment;
        //切换页面的时候，如果该页面不属于底部toolbar中的任何一个，那么需要把下标设置为-1，并且清除底部radiogroup的选中
        if (position < 0) {
            mBottomView.clearSelect();
            currentPosition = -1;
        } else {
            currentPosition = position;
        }
        //获取当前在容器中的fragment
        List<Fragment> fragments = mFragmentManager.getFragments();
        if (fragments != null && fragments.size() > 0) {
            for (Fragment childFragment : fragments) {
                //如果这个fragment 不是当前展示的fragment 并且 在savefragment的队列中的时候
                if (mCurrentFragment != childFragment && saveFragment.containsKey(childFragment.getClass().getSimpleName())) {
                    //判断这个fragment是否需要保存
                    Boolean isSaveFrg = saveFragment.get(childFragment.getClass().getSimpleName());
                    if (isSaveFrg == null || !isSaveFrg) {
                        fragmentTransaction.remove(childFragment);
                    }
                }
            }
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_main;
    }


    /**
     * 底部Navigation Bar
     *
     * @param position
     */
    @Override
    public void OnBottomButtonCheck(final int position) {
        if (isShouldInterceptTouchEvent) {
            mBottomView.selLastPosition();
            return;
        }
        AnalysisButtonEntity analysisButtonEntity;
        switch (position) {
            case BottomButtonView.TAB_INDEX_HOME:
                if (mCurrentFragment != homeMainFragment) {
                    analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TAB_HOME, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                    sendAnalysisEvent(analysisButtonEntity);
                }
                switchDiffFragmentContent(homeMainFragment, R.id.fl_main, position, true);
                break;
            case BottomButtonView.TAB_INDEX_MAJOR_SCAN:
                if (CacheManagerInstance.getInstance().isLogin()) {
                    analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TAB_SCAN, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                    sendAnalysisEvent(analysisButtonEntity);
                }
                checkLoginStatusWithPosition(position);
                break;
            case BottomButtonView.TAB_INDEX_BACK_SCAN:
                if (mCurrentFragment instanceof BackScanFragment) {

                } else {
                    analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TAB_PAYMENTCODE, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                    sendAnalysisEvent(analysisButtonEntity);
                    checkLoginStatusWithPosition(position);
                }
                break;
            case BottomButtonView.TAB_INDEX_TRANSFER_SELECT_LIST:
                //因为涉及到验证密码 所以页面重新load需要体验处理
                if (mCurrentFragment instanceof TransferSelTypeFragment) return;
                analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TAB_TRANSFER, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                checkLoginStatusWithPosition(position);
                break;
            case BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC:
                //因为涉及到验证密码 所以页面重新load需要体验处理
                if (mCurrentFragment instanceof TransferByStaticCodeFragment) return;
                analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TAB_COLLECT, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                checkLoginStatusWithPosition(position);
                break;
            default:
                break;
        }
    }

    /**
     * 检查登录态下的底部导航栏页面切换 可优化
     *
     * @param position
     * @return
     */
    private boolean checkLoginStatusWithPosition(final int position) {
        changeFragmentWithPosition(position);
        return true;
    }

    /**
     * 根据底部导航栏的position 切换fragment
     *
     * @param position
     */
    private void changeFragmentWithPosition(int position) {
        if (position == BottomButtonView.TAB_INDEX_MAJOR_SCAN) {
            //只有主扫需要隐藏actionBar 自定义
            MajorScanFragment majorScanFragment = new MajorScanFragment();
            switchDiffFragmentContent(majorScanFragment, R.id.fl_main, position, false);
        } else if (position == BottomButtonView.TAB_INDEX_TRANSFER_SELECT_LIST) {
            /**
             * 转账先判断是否可以转账 才能跳转  先选择上一次选中位置
             */
            mBottomView.selLastPosition();
            MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.TRANSFER_SELECT_LIST);
            mPresenter.checkSmartAccountBind(menuItmByEnum);
        } else if (position == BottomButtonView.TAB_INDEX_BACK_SCAN) {
            switchPagesToBackScan(false);
        } else if (position == BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC) {
            switchPagesToTransferStaticQrCode(false);
        }
    }

    /**
     * 切换到收款页面
     * 如果在菜单栏进入收款页面的时候，因为需要先验密
     * 场景一：
     * 点击底部bar 切换，选中收款bar->验密->取消验密->回退到上一次选中的底部bar
     * 场景二：
     * 点击菜单栏item 验密->取消验密
     *
     * @param openMenu 判断是否是在菜单栏点击进入
     */
    public void switchPagesToTransferStaticQrCode(boolean openMenu) {
        currentAction = MainSwitchAction.ACTION_SWITCH_PAGES_TO_STATIC_QR_CODE;
        verifyPwdAction(openMenu);
    }

    /**
     * 切换到付款码页面
     * 如果在菜单栏进入付款码页面的时候，因为需要先验密
     * 场景一：
     * 点击底部bar 切换，选中付款码bar->验密->取消验密->回退到上一次选中的底部bar
     * 场景二：
     * 点击菜单栏item 验密->取消验密
     *
     * @param openByMenu 判断是否是在菜单栏点击进入
     */
    public void switchPagesToBackScan(boolean openByMenu) {
        perLocationSuccess = false;
        boolean isLocationServiceOpen = (Boolean) HttpCoreSpUtils.get(LOCATION_SP, false);
        //定位开关在app里头打开 定位权限/定位服务都可用的情况下进行定位
        if (isLocationServiceOpen &&
                isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) &&
                isGranted(Manifest.permission.ACCESS_FINE_LOCATION) &&
                LocationUtils.isLocationEnabled(this)) {
            if (!isFinishing()) {
                getLocation(new LocationUIUtils.LocationLoadListener() {
                    @Override
                    public void onLocationLoadSuccess(boolean isHk) {
                        LogUtils.i(TAG, "onLocationLoadSuccess:" + isHk);
                        currentLocationIsHk = isHk;
                        perLocationSuccess = true;
                    }
                });
            }
        } else {
            //做到关的时候跟系统同步，app使用过程中 用户在系统里头将权限关闭 app里头开关也要关闭
            HttpCoreSpUtils.put(LOCATION_SP, false);
        }
        currentAction = MainSwitchAction.ACTION_SWITCH_PAGES_TO_BACK_SCAN;
        verifyPwdAction(openByMenu);
    }


    /**
     * 验密
     * 具体逻辑 可参考 {@link #switchPagesToBackScan(boolean)} {@link #switchPagesToTransferStaticQrCode(boolean)}
     *
     * @param openByMenu 判断是否是在菜单栏点击进入
     */
    public void verifyPwdAction(boolean openByMenu) {
//        LogUtils.i("TESTCLICK","parserLocalQrcodeSuccess2---->");
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                //如果这里添加类型，需要针对是否是侧边栏打开的，进行处理，侧边栏打开的不应该重置为上次打开的位置
                if (currentAction == MainSwitchAction.ACTION_SWITCH_PAGES_TO_BACK_SCAN) {
                    BackScanFragment backScanFragment = new BackScanFragment();
                    if (perLocationSuccess) {
                        Bundle bundle = new Bundle();
                        bundle.putBoolean(LOCATION_IN_HK, currentLocationIsHk);
                        backScanFragment.setArguments(bundle);
                    }
                    switchDiffFragmentContent(backScanFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_BACK_SCAN, false);
                    mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_BACK_SCAN);

                } else if (currentAction == MainSwitchAction.ACTION_SWITCH_PAGES_TO_STATIC_QR_CODE) {
                    MenuItemEntity menuItmByEnum = MenuItemEnum.getMenuItmByEnum(MainHomeActivity.this, MenuItemEnum.TRANSFER_BY_STATIC_CODE);
                    mPresenter.checkSmartAccountBind(menuItmByEnum);
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(MainHomeActivity.this, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                //重置
                //
                if (!openByMenu) {
                    mBottomView.selLastPosition();
                    currentPosition = mBottomView.getSelectPosition();
                }

            }
        });
    }


    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            mExitTime = System.currentTimeMillis();
        } else {
            //只有杀死app的时候才需要关闭服务 设置语言重启等等不需要
            AnalysisManager.getInstance().stopAnalysisService();
            UserLoginEventManager.getInstance().clearAllEvent();
            finish();
            System.exit(0);
        }
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {

    }

    /**
     * 密码弹出框
     */
    public void inputPswPopWindow(String action) {
        LogUtils.i(TAG, "ACTION:" + action);
        checkPwdAction = action;
        boolean isSupportFinger = MenuAction.ACTION_OPEN_VIRTUAL_CARD.equals(checkPwdAction) || MenuAction.ACTION_BIND_CARD_IN_HOME.equals(checkPwdAction) || MenuAction.ACTION_BIND_CARD.equals(checkPwdAction);
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), !isSupportFinger, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (MenuAction.ACTION_OPEN_VIRTUAL_CARD.equals(checkPwdAction)) {
                    //打开虚拟卡需要验证支付密码
                    mPresenter.getVirtualCardList();
                } else if (ACTION_BIND_CARD_IN_HOME.equals(checkPwdAction)) {
                    binderCard(true);
                } else if (ACTION_BIND_CARD.equals(checkPwdAction)) {
                    binderCard(false);
                } else if (ACTION_BIND_SMART_ACCOUNT.equals(checkPwdAction)) {
                    HashMap<String, Object> maps = new HashMap<>();
                    maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                    RegisterSmartAccountDescActivity.startActivity(MainHomeActivity.this, maps);
                } else if (MenuAction.ACTION_UPDATE_ACCOUNT.equals(checkPwdAction)) {
                    HashMap<String, Object> maps = new HashMap<>();
                    maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
                    ActivitySkipUtil.startAnotherActivity(MainHomeActivity.this, UpgradeAccountActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (MenuAction.ACTION_TRANSFER_BY_FPS.equals(checkPwdAction)) {
                    //登记转数快 需要判断是否能跳转到转数快
                    mPresenter.queryBankRecordsByFps();
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    public boolean onMenuItemClick(MenuItemEntity item) {
        //去掉侧边栏收起动画  防止出现侧边栏不关闭的问题
        mDrawerLayout.closeDrawer(GravityCompat.START, false);
        checkPwdAction = "";
        if (MenuItemEnum.HOME.action.equals(item.getMenuKey())) {
            switchDiffFragmentContent(homeMainFragment, R.id.fl_main, 0, false);
            mBottomView.selectWithPosition(0);
        } else if (MenuItemEnum.MAJOR_SCAN_QR_CODE.action.equals(item.getMenuKey())) {
            if (mBottomView.getSelectPosition() == BottomButtonView.TAB_INDEX_MAJOR_SCAN) {
                return false;
            }
            MajorScanFragment majorScanFragment = new MajorScanFragment();
            switchDiffFragmentContent(majorScanFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_MAJOR_SCAN, false);
            mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_MAJOR_SCAN);
        } else if (MenuItemEnum.BACK_SCAN_QR_CODE.action.equals(item.getMenuKey())) {
            if (mBottomView.getSelectPosition() == BottomButtonView.TAB_INDEX_BACK_SCAN) {
                return false;
            }
            switchPagesToBackScan(true);
        } else if (MenuItemEnum.TRANSFER_BY_STATIC_CODE.action.equals(item.getMenuKey())) {
            if (mBottomView.getSelectPosition() == BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC) {
                return false;
            }
            switchPagesToTransferStaticQrCode(true);
        } else if (MenuItemEnum.TRANSFER_SELECT_LIST.action.equals(item.getMenuKey())) {
            if (mBottomView.getSelectPosition() == BottomButtonView.TAB_INDEX_TRANSFER_SELECT_LIST) {
                return false;
            }
            mPresenter.checkSmartAccountBind(item);
        } else if (item.getMenuKey().equals(MenuItemEnum.VIRTUAL_CARD_APPLY.action)) {
            //虚拟卡注册 需要 一个url 从公共数据接口获得
            if (!TextUtils.isEmpty(virtualCardUrl)) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                mHashMaps.put(WebViewActivity.SHARE_CONTENT, virtualCardShareUrl);
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                mPresenter.getAppLoginConfigParams(MenuItemEnum.VIRTUAL_CARD_APPLY.action);
            }
        } else if (item.getMenuKey().equals(MenuItemEnum.VIRTUAL_CARD_CONFIRM.action)) {
            inputPswPopWindow(MenuAction.ACTION_OPEN_VIRTUAL_CARD);
//            if (!TextUtils.isEmpty(virtualCardUrl)) {
//                VirtualCardConfirmFragment virtualCardConfirmFragment = VirtualCardConfirmFragment.newInstance(virtualCardUrl, virtualCardShareUrl);
//                switchDiffFragmentContent(virtualCardConfirmFragment, R.id.fl_main, -1, false);
//            } else {
//                mPresenter.getAppLoginConfigParams(MenuItemEnum.VIRTUAL_CARD_CONFIRM.action);
//            }
        } else if (MenuItemEnum.TRANSFER_CROSS_BORDER.action.equals(item.getMenuKey()) || MenuKey.CROSS_TRANSFER.equals(item.getMenuKey())) {
            if (mCurrentFragment.getClass() == item.getCls()) {
                return false;
            }
            mPresenter.getTransferBaseData();
        } else if (MenuItemEnum.MONTH_STAGING.action.equals(item.getMenuKey())) {
            StagingItemMonthFragment stagingItemMonthFragment = new StagingItemMonthFragment();
            switchDiffFragmentContent(stagingItemMonthFragment, R.id.fl_main, -1, false);
        } else if (MenuItemEnum.CASH_STAGING.action.equals(item.getMenuKey())) {
            StagingItemCashFragment stagingItemCashFragment = new StagingItemCashFragment();
            switchDiffFragmentContent(stagingItemCashFragment, R.id.fl_main, -1, false);
        } else if (MenuItemEnum.MY_ACCOUNT.action.equals(item.getMenuKey())) {
            NewCardManagerFragment cardManagerFragment = new NewCardManagerFragment();
            switchDiffFragmentContent(cardManagerFragment, R.id.fl_main, -1, false);
        } else if (MenuItemEnum.UPLAN.action.equals(item.getMenuKey())) {
            mPresenter.getUplanUrl();
        } else if (MenuItemEnum.TRANSFER_BY_FPS.action.equals(item.getMenuKey())
                || MenuItemEnum.TRANSFER_BY_BANK.action.equals(item.getMenuKey())
                || MenuItemEnum.RED_POCKET.action.equals(item.getMenuKey())
                || MenuItemEnum.NEW_YEAR_RED_POCKET.action.equals(item.getMenuKey())) {
            if (mCurrentFragment.getClass() == item.getCls()) {
                return false;
            }
            mPresenter.checkSmartAccountBind(item);
        } else {
            //暂时只统计交易记录点击事件和信用卡奖赏
            if (MenuItemEnum.TRANSACTION_MANAGEMENT.action.equals(item.getMenuKey())) {
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_MENU_RECORD, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
            }
            if (MenuItemEnum.CREDIT_CARD_REWARD.action.equals(item.getMenuKey())) {
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_MENU_CREDIT_REWARD, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
            }

            if (item.isLevelFirst()) {
                if (BaseFragment.class.isAssignableFrom(item.getCls())) {
                    Class<? extends BaseFragment> cls = (Class<? extends BaseFragment>) item.getCls();
                    try {
                        BaseFragment fragment = cls.newInstance();
                        switchDiffFragmentContent(fragment, R.id.fl_main, -1, false);
                        currentPosition = -1;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            } else {
                if (Activity.class.isAssignableFrom(item.getCls())) {
                    ActivitySkipUtil.startAnotherActivity(getActivity(), (Class<? extends Activity>) item.getCls(), ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }

        }
        return false;
    }


    /**
     * 初始化资料区倒计时
     *
     * @param time 单位 秒
     */
    private void initTimer(long time) {
        timerHideAccount = new CountDownTimer(time * 1000L, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                nowTime = millisUntilFinished / 1000L;
            }

            @Override
            public void onFinish() {//结束后的操作
                nowTime = 0L;

                //发送眼睛 倒计时结束事件
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_END_TIMER, ""));
                //眼睛倒计时结束 需要验密
                SystemInitManager.getInstance().setValidShowAccount(false);
            }
        };
    }

    /**
     * 开始资料区数据隐藏的倒计时
     */
    public void startTimerHideAccountData() {
        if (timerHideAccount != null) {
            timerHideAccount.start();
        } else {
            //默认十分钟
            initTimer(10 * 60L);
            timerHideAccount.start();
        }
    }

    /**
     * 绑定卡/绑定账号
     * 跳转到SelRegisterTypeActivity
     * 可选 CC/SA/PA 三种不同的 绑定类型
     */
    private void binderCard(boolean backHomePage) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        if (backHomePage && mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            }, 300);
        }
    }

    /**
     * 绑定智能账户
     * 跳转到RegisterSmartAccountDescActivity
     * 绑定SA账户流程
     */
    private void binderSmartAccount() {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        RegisterSmartAccountDescActivity.startActivity(MainHomeActivity.this, maps);
        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            }, 300);
        }
    }


    @Override
    public void getVirtualCardListSuccess(VirtualCardListEntity response) {


        Bundle putBundle = new Bundle();
        putBundle.putInt(Constants.CURRENT_PAGE_FLOW, Constants.VITUALCARD_BIND);
        putBundle.putSerializable(Constants.VIRTUALCARDLIST, response);

        VirtualCardBindTermsFragment virtualCardBindTermsFragment = new VirtualCardBindTermsFragment();
        virtualCardBindTermsFragment.setArguments(putBundle);
        switchDiffFragmentContent(virtualCardBindTermsFragment, R.id.fl_main, -1, false);
    }

    @Override
    public void getVirtualCardListError(final String errorCode, String errorMsg) {
        stopPreviewWithScanView();
        if (TextUtils.equals(errorCode, ErrorCode.VIRTUAL_LIST_FAILED.code)) {
            NewCardManagerFragment cardManagerFragment = new NewCardManagerFragment();
            switchDiffFragmentContent(cardManagerFragment, R.id.fl_main, -1, false);
        } else {
            showErrorMsgDialog(this, errorMsg);
        }
    }

//    @Override
//    public void getAppParametersError(String errorCode, String errorMsg) {
//
//        showErrorMsgDialog(this, errorMsg);
//    }
//
//    @Override
//    public void getAppParametersSuccess(AppParameters appParameters) {
//        //防止更改密码 请求地址 两个弹框重复弹出
//        TempSaveHelper.setLbsSwitch(appParameters);
//        if (!appParameters.getMultipleSwitch().isLbsClose()) {
//            showAcceptLocationPermission();
//        }
//    }


    @Override
    public void apkVerifyWithSHAError(String errorCode, String errorMsg) {

    }

    @Override
    public void apkVerifyWithSHASuccess(ApkVerifyEntity response) {
        if (response != null && response.isVerifySuccess()) {

        } else {
            //杀死当前进程
            Process.killProcess(Process.myPid());
        }
    }

    @Override
    public void doAutoLoginError(String errorCode, String errorMsg) {
        if (ErrorCode.FPS_SESSION_INVALID.code.equalsIgnoreCase(errorCode)) {
            MyActivityManager.removeAllTaskExcludeMainStack();
            ActivitySkipUtil.startAnotherActivity(MainHomeActivity.this, PreLoginActivity.class);
            finish();
        }
        needShowLoading = false;
        if (homeMainFragment != null) {
            homeMainFragment.setIgnoreLoadAccount(false);
            homeMainFragment.showNetworkError();
        }
    }

    @Override
    public void doAutoLoginSuccess(AutoLoginSucEntity response) {
        needShowLoading = false;
        queryDataAfterAutoLogin(response);
    }

    /**
     * 自动登录成功之后需要做的事情
     * <p>
     * 1.appcalapp 如果是appcallapp吊起的bocpay 以下所有的事件都不处理都会等到下一次killapp重启才会触发
     * 2.推送通知点进来
     * 3.强制升级 消息开关
     * 4.修改密码
     * 5.自动登录通知弹框消息消息开关
     * 6.员工利是的接口
     * 7.lbs
     *
     * @param response
     */
    public void queryDataAfterAutoLogin(AutoLoginSucEntity response) {
        //只有当自动登录完成后，才能发起查询智能账户信息
        if (homeMainFragment != null) {
            homeMainFragment.hideLoading();
            homeMainFragment.showAccountTypeUI();
            if (response != null) {
//                homeMainFragment.queryAccountData(false);
                homeMainFragment.queryLifeSceneAreaCount();
                homeMainFragment.setIgnoreLoadAccount(false);
            }
        }
        if (response != null) {
            mPresenter.getAppLoginConfigParams("");
        }

        if (response != null && !TextUtils.isEmpty(response.getNewUserPopUpBanner())) {
            RegisterPopBannerDialogEvent registerPopBannerDialogEvent = new RegisterPopBannerDialogEvent();
            registerPopBannerDialogEvent.updateEventParams(RegisterPopBannerDialogEvent.POPUPBANNERURL, response.getNewUserPopUpBanner());
            UserLoginEventManager.getInstance().addUserLoginEvent(registerPopBannerDialogEvent);
        }
        if (response != null && response.isNeedEditPwd()) {
            EditPwdDialogEvent editPwdDialogEvent = new EditPwdDialogEvent();
            UserLoginEventManager.getInstance().addUserLoginEvent(editPwdDialogEvent);
        }

        //推广通知弹框逻辑
        boolean jumpMessageCenter = false;
        if (!SpUtils.getInstance().hasShowNotificationDialog()) {
            //首次安装，升级
            jumpMessageCenter = response != null && response.isJumpToMessageCenterWhenFirstInstallOrUpdate();
        } else {
            jumpMessageCenter = response != null && response.isJumpToMessageCenter();
        }
        if (jumpMessageCenter) {
            JumpCenterNotificationEvent jumpCenterNotificationEvent = new JumpCenterNotificationEvent();
            UserLoginEventManager.getInstance().addUserLoginEvent(jumpCenterNotificationEvent);
        }

        boolean hasDealSuccess = UserLoginEventManager.getInstance().dealWithEvent();
        if (!hasDealSuccess) {
            if (mPresenter != null) {
                //员工利是 lbs弹框有可能同时存在
//                mPresenter.getAppParameters();
                showAcceptLocationPermission();
                initStaffRedPacketEvent();
            }
        }
    }


    /**
     * 首页每次进入都需要打开红包  通知进来红包的方法是不一样的
     */
    private void initStaffRedPacketEvent() {
        //如果有红包参数 说明是从推送消息点击进来 需要提示拆重复
        //登录成功是否需要修弹出红包
        if (currentIntent.getExtras() != null && currentIntent.getExtras().get(RED_PACKET_RED_PACKET_PARAMS) != null) {
            RedPackageEntity redPackageEntity = (RedPackageEntity) currentIntent.getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS);
            if (TextUtils.equals(redPackageEntity.getType(), Constants.PUSH_REDPACKET_STAFF_TYPE)) {
                mPresenter.getStaffRedPacket(true, Constants.IS_STAFF_PUSH);
            } else {
                mPresenter.getStaffRedPacket(true, Constants.IS_NOT_STAFF_PUSH);
            }
        } else {
            mPresenter.getStaffRedPacket(true, Constants.IS_NOT_STAFF_PUSH);
        }
    }


    @Override
    public void queryBankRecordsByFpsError(String errorCode, String errorMsg) {
        stopPreviewWithScanView();
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    HashMap<String, Object> maps = new HashMap<>();
                    maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                    RegisterSmartAccountDescActivity.startActivity(MainHomeActivity.this, maps);
                }
            });
        } else {
            showErrorMsgDialog(MainHomeActivity.this, errorMsg);
        }
    }

    @Override
    public void queryBankRecordsByFpsSuccess(FpsBankRecordsEntity entity) {
        ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class);
    }


    @Override
    public void getMenuListSuccess(MenuListEntity data) {
        ArrayList<MenuItemEntity> cache = new ArrayList<>();
        if (data != null) {
            //如果2次拉取菜单栏得到的结果相同 不需要刷新侧边栏
            if (menuEncryptionData.equals(data.getMenuEncryptionData())) {
                return;
            }
            menuEncryptionData = data.getMenuEncryptionData();
            ArrayList<MenuItemEntity> menuList = data.getMenuData();
            if (menuList != null && menuList.size() > 0) {
                MenuItemEnum.mapMenuToList(cache, menuList, menuItemList);
                TempSaveHelper.setMenuList(cache);
                menuItemAdapter.setList(cache);
                menuItemAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void getMenuListFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getTransferBaseDataSuccess(TransferCrossBorderBaseEntity entity) {
        if (entity != null) {
            TransferCrossBorderPayeeFragment transferCrossBorderPayeeFragment = new TransferCrossBorderPayeeFragment();
            transferCrossBorderPayeeFragment.setBaseEntity(entity);
            switchDiffFragmentContent(transferCrossBorderPayeeFragment, R.id.fl_main, -1, false);
        }
    }

    /**
     * 停止主扫页面中的扫描器
     */
    private void stopPreviewWithScanView() {
        if (mCurrentFragment instanceof MajorScanFragment) {
            MajorScanFragment majorScanFragment = (MajorScanFragment) mCurrentFragment;
            majorScanFragment.stopPreview();
        }
    }

    @Override
    public void getTransferBaseDataFail(final String errorCode, final String errorMsg) {
        stopPreviewWithScanView();
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS)) {
            //支付账户升级
            String message = getString(R.string.CT1_1c_4) + " (" + errorCode + ")";
            AndroidUtils.showBindSmartAccountAndCancelDialogTwo(getActivity(), message, getString(R.string.upgrade_account), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    inputPswPopWindow(MenuAction.ACTION_UPDATE_ACCOUNT);
                }
            });
        } else if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT_TWO) || TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS_TWO)) {
            //信用卡绑定智能账户
            AndroidUtils.showBindSmartAccountAndCancelDialogTwo(getActivity(), getString(R.string.CT1_1c_1) + " (" + errorCode + ")", getString(R.string.PU2107_1_3), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    verifyPwdToUpdateAccount();
                }
            });
        } else {
            showErrorMsgDialog(this, errorMsg);
        }
    }

    @Override
    public void appCallAppLinkSuccess(AppCallAppLinkEntity response, String url) {
        if (response == null)
            return;

        if (!TextUtils.isEmpty(response.getJumpType())) {
            //如果侧边栏打开的时候，先关闭侧边栏
            if (mDrawerLayout != null) {
                mDrawerLayout.closeDrawers();
            }
            EventBus.getDefault().postSticky(new PasswordEventEntity(PasswordEventEntity.EVENT_CLOSE_VERIFY_PASSWORD, ""));//关闭键盘，如果键盘打开的话
            if (AppCallAppLinkEntity.TYPE_CREATE_QRCODE.equals(response.getJumpType())) {
                switchPagesToBackScan(false);
            } else if (AppCallAppLinkEntity.TYPE_SCAN_QRCODE.equals(response.getJumpType())) {
                String qrCode = AndroidUtils.splitQrCode(url, response.getQrCode());
                MajorScanFragment majorScanFragment = new MajorScanFragment();
                majorScanFragment.setQrCodeInfo(qrCode);
                switchDiffFragmentContent(majorScanFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_MAJOR_SCAN, false);
                mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_MAJOR_SCAN);
            } else if (AppCallAppLinkEntity.TYPE_ECOUPON.equals(response.getJumpType())) {
                //如果是登录态 检测sessionId获取成功
                EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ECOUPON, ""));
            } else if (AppCallAppLinkEntity.TYPE_APPCALLAPP.equals(response.getJumpType())) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismissDialog();
                        AppCallAppPaymentActivity.startAppCallApp(MainHomeActivity.this, url, response.getMerchantLink(), response.getNetworkErrorLink(), response.isAppCallAppFromH5());
                    }
                }, 800);

            }
        } else {
            if (!TextUtils.isEmpty(response.getAppLink())) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, response.getAppLink());
                ActivitySkipUtil.startAnotherActivity(this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void appCallAppLinkError(String errorCode, String errorMsg) {

        LogUtils.d("appLink", "appCallAppLinkError");
//        ActivitySkipUtil.startAnotherActivity(MainHomeActivity.this, AppCallAppPaymentActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getAppLoginConfigSuccess(InitBannerEntity response, String action) {
        //TODO 数据成功待处理 剩下otp时间等时间
        if (response == null)
            return;
        //init banner 配置信息查询成功之后保存
        if (TempSaveHelper.getInitBannerUrlParams() == null) {
            TempSaveHelper.setInitBannerUrlParams(response, true);
            if (homeMainFragment != null) {
                homeMainFragment.refreshLifeMenu(response);
            }
        } else {
            TempSaveHelper.setInitBannerUrlParams(response, true);
            if (homeMainFragment != null) {
                homeMainFragment.refreshLifeMenu(response);
            }
        }
        OtpCountDownManager.getInstance().setmOtpCountDownInfo(response);

        //下载通用图片时，不刷新通用图片，下个界面打开时，会发生改变
        if (!SpUtils.getInstance().getMainBgVersion().equals(response.getPostLoginBgImageVer())) {
            GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_MAIN_BG, response.getPostLoginBgImageUrl(), response.getPostLoginBgImageVer());
        }
        if (response.getHideTime() > 0) {
            SystemInitManager.getInstance().setServerHideTime(response.getHideTime());
            initTimer(response.getHideTime());
        }

        if (!TextUtils.isEmpty(response.getVirtualCardUrl(MainHomeActivity.this))) {
            virtualCardUrl = response.getVirtualCardUrl(MainHomeActivity.this);
            virtualCardShareUrl = response.getVirtualCardShareUrl(MainHomeActivity.this);
        }
        if (MenuItemEnum.VIRTUAL_CARD_APPLY.action.equals(action)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
            mHashMaps.put(WebViewActivity.NEW_MSG, true);
            mHashMaps.put(WebViewActivity.SHARE_CONTENT, virtualCardShareUrl);
            ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

        if (MenuItemEnum.VIRTUAL_CARD_CONFIRM.action.equals(action)) {
            VirtualCardConfirmFragment virtualCardConfirmFragment = VirtualCardConfirmFragment.newInstance(virtualCardUrl, virtualCardShareUrl);
            switchDiffFragmentContent(virtualCardConfirmFragment, R.id.fl_main, -1, false);
        }

        downRedResource(response.getRedpacketPopResource().getZh_CN(), Constants.RED_SAVE_CN);
        downRedResource(response.getRedpacketPopResource().getZh_HK(), Constants.RED_SAVE_HK);
        downRedResource(response.getRedpacketPopResource().getEn_US(), Constants.RED_SAVE_EN);

    }

    private void downRedResource(RedpacketPopResourceEntity redpacketPopResourceEntity, String languageType) {
        if (null == redpacketPopResourceEntity) {
            return;
        }

        new RedPacketResourceUtil(mContext, redpacketPopResourceEntity.getBocpayRPImageUrl()
                , redpacketPopResourceEntity.getBocpayRPMusicUrl(), redpacketPopResourceEntity.getStaffRPImageUrl()
                , redpacketPopResourceEntity.getStaffRPMusicUrl(), languageType);
    }

    @Override
    public void getAppLoginConfigError(String errorCode, String errorMsg, String action) {
        if (MenuItemEnum.VIRTUAL_CARD_APPLY.action.equals(action)) {
            showErrorMsgDialog(MainHomeActivity.this, errorMsg);
        }
    }

    @Override
    public void checkSmartAccountBindSuccess(ContentEntity response, MenuItemEntity menuItemEntity) {


        if (BaseFragment.class.isAssignableFrom(menuItemEntity.getCls())) {
            Class<? extends BaseFragment> cls = (Class<? extends BaseFragment>) menuItemEntity.getCls();
            try {
                BaseFragment fragment = cls.newInstance();
                if (MenuItemEnum.TRANSFER_BY_STATIC_CODE.action.equals(menuItemEntity.getMenuKey())) {
                    TransferByStaticCodeFragment staticCodeFragment = new TransferByStaticCodeFragment();
                    switchDiffFragmentContent(staticCodeFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC, false);
                    mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_QR_CODE_BY_STATIC);
                } else if (MenuItemEnum.RED_POCKET.action.equals(menuItemEntity.getMenuKey())
                        || MenuItemEnum.NEW_YEAR_RED_POCKET.action.equals(menuItemEntity.getMenuKey())) {
                    if (fragment instanceof RedPacketMainFragment) {
                        RedPacketMainFragment redPacketMainFragment = RedPacketMainFragment.newInstance(RedPacketMainFragment.PALEY_SHI_TAB);
                        switchDiffFragmentContent(redPacketMainFragment, R.id.fl_main, -1, false);
                    }
                } else if (MenuItemEnum.RED_POCKET_GET_LISHI.action.equals(menuItemEntity.getMenuKey())) {
                    if (fragment instanceof RedPacketMainFragment) {
                        RedPacketMainFragment redPacketMainFragment = RedPacketMainFragment.newInstance(RedPacketMainFragment.GET_LI_SHI_TAB);
                        switchDiffFragmentContent(redPacketMainFragment, R.id.fl_main, -1, false);
                        //如果当前页面是派利是页面 那么需要手动切换
                        if (fragment.getClass().getSimpleName().equals(mCurrentFragment.getClass().getSimpleName())) {
                            RedPacketMainFragment flagFragment = (RedPacketMainFragment) mCurrentFragment;
                            if (null != flagFragment) {
                                flagFragment.changeTab(RedPacketMainFragment.GET_LI_SHI_TAB);
                            }
                        }
                    }
                } else if (MenuItemEnum.TRANSFER_SELECT_LIST.action.equals(menuItemEntity.getMenuKey())) {
                    TransferSelTypeFragment transferSelTypeFragment = new TransferSelTypeFragment();
                    switchDiffFragmentContent(transferSelTypeFragment, R.id.fl_main, BottomButtonView.TAB_INDEX_TRANSFER_SELECT_LIST, false);
                    mBottomView.selectWithPosition(BottomButtonView.TAB_INDEX_TRANSFER_SELECT_LIST);
                } else if (MenuItemEnum.PAYMENT_BY_FPS.action.equals(menuItemEntity.getMenuKey())) {
                    FPSPaymentScanFragment mScanFragment = new FPSPaymentScanFragment();
                    switchDiffFragmentContent(mScanFragment, R.id.fl_main, -1, false);
                } else {
                    switchDiffFragmentContent(fragment, R.id.fl_main, -1, false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
    }


    @Override
    public void checkSmartAccountBindError(String errorCode, String errorMsg, MenuItemEntity menuItemEntity) {
        stopPreviewWithScanView();
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            if (MenuItemEnum.TRANSFER_BY_STATIC_CODE.action.equals(menuItemEntity.getMenuKey())) {
                AndroidUtils.showBindSmartAccountAndCancelDialog(MainHomeActivity.this, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        binderCard(true);
                    }
                });

            } else if (MenuItemEnum.TRANSFER_CROSS_BORDER.action.equals(menuItemEntity.getMenuKey()) || MenuKey.CROSS_TRANSFER.equals(menuItemEntity.getMenuKey())) {
                AndroidUtils.showBindSmartAccountAndCancelDialog(MainHomeActivity.this, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        inputPswPopWindow(ACTION_BIND_SMART_ACCOUNT);
                    }
                });
            } else {
                AndroidUtils.showBindSmartAccountAndCancelDialog(MainHomeActivity.this, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        inputPswPopWindow(ACTION_BIND_CARD_IN_HOME);
                    }
                });
            }
        } else {
            showErrorMsgDialog(getActivity(), errorMsg);
        }
    }


    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (getActivity() == null) {
                    return;
                }
                binderSmartAccount();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(MainHomeActivity.this, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
            }
        });
    }

    @OnClick({R.id.ll_setting, R.id.ll_help})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_setting:
                SettingFragment settingFragment = new SettingFragment();
                switchDiffFragmentContent(settingFragment, R.id.fl_main, -1, false);
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
                break;
            case R.id.ll_help:
                ActivitySkipUtil.startAnotherActivity(this, HelpActivity.class);
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void getUplanUrlSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getUplanUrlError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }

    @Override
    public void refreshCardListError(String errorCode, String errorMsg) {

    }

    @Override
    public void refreshCardListSuccess(CardsEntity response) {
        if (response != null) {
            ArrayList<BankCardEntity> rows = response.getRows();
            if (rows != null && rows.size() > 0) {
                CacheManagerInstance.getInstance().setCardEntities(rows);
                if (homeMainFragment != null) {
                    homeMainFragment.hideAccountData(false);
                    homeMainFragment.queryAccountData(true);
                }
            }
        }

    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {

    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        //数据更新已经在底层接口中完成
    }

    @Override
    public void getStaffRedPacketError(String errorCode, String errorMsg) {
        if (ErrorCode.RED_OPEN_REPEAT.code.equals(errorCode)) {
            showRedRepeatDialog(errorMsg);
        } else {
            AndroidUtils.showTipDialog(this, errorMsg);
        }
    }

    @Override
    public void getStaffRedPacketSuccess(TurnOnRedPackEtntity.ReceivedRedPacketDetailBean response) {
        if (null == response) {
            //后台约定 如果员工利是弹框弹过，下次会response=null
        } else {
            RedPacketDialogUtils.showStaffRedPacketOpenDialog(this, response);
        }

//        String value = "{\n" +
//                "\t\"bgColor\": \"#F2618D\",\n" +
//                "\t\"blessingWords\": \"牛年行大运\",\n" +
//                "\t\"bocPayRedPacket\": false,\n" +
//                "\t\"bottomImageUrl\": \"http://mbasit1.ftcwifi.com/images/en_US/1.3.1/layout3/bottom.png\",\n" +
//                "\t\"centerImageUrl\": \"http://mbasit1.ftcwifi.com/images/en_US/1.3.1/layout3/center.png\",\n" +
//                "\t\"coverId\": \"3\",\n" +
//                "\t\"coverUrl\": \"http://mbasit1.ftcwifi.com/images/en_US/1.3.1/layout3/redPacketCoverImage.png\",\n" +
//                "\t\"currency\": \"HKD\",\n" +
//                "\t\"fPSRedPacket\": false,\n" +
//                "\t\"name\": \"中银香港\",\n" +
//                "\t\"redPacketImageUrl\": \"https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/images/red_packet/smp/background_staff.gif?version=202101072003\",\n" +
//                "\t\"redPacketMusicUrl\": \"https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/sounds/red_packet/smp/background_music_staff.mp3?version=202101121639\",\n" +
//                "\t\"sendAmt\": \"20000\",\n" +
//                "\t\"sendTime\": 1610442389000,\n" +
//                "\t\"sendTimeStr\": \"2021-01-12 17:06:29\",\n" +
//                "\t\"srcRefNo\": \"00000001269\",\n" +
//                "\t\"staffRedPacket\": true,\n" +
//                "\t\"transferType\": \"9\",\n" +
//                "\t\"turnOn\": \"1\"\n" +
//                "}";
//
//        TurnOnRedPackEtntity.ReceivedRedPacketDetailBean receivedRedPacketDetailBean = new Gson().fromJson(value,TurnOnRedPackEtntity.ReceivedRedPacketDetailBean.class);
//        RedPacketDialogUtils.showStaffRedPacketOpenDialog(this, receivedRedPacketDetailBean);
    }

    private void showRedRepeatDialog(String errorMsg) {
        if (redRedRePeatDailog != null && redRedRePeatDailog.isShowing()) {
            redRedRePeatDailog.dismiss();
            redRedRePeatDailog = null;
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(this);

        builder.setMessage(errorMsg);

        builder.setPositiveButton(this.getString(R.string.dialog_right_btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != redRedRePeatDailog) {
                    redRedRePeatDailog.dismiss();
                }
                redRedRePeatDailog = null;
                //跳转到拆利是页面
                EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_RED_PACKET_GET_FRAGMENT, Constants.IS_SHOW_DAILOG));
            }
        });

        redRedRePeatDailog = builder.create();
        redRedRePeatDailog.setCancelable(false);
        redRedRePeatDailog.show();
    }


    /**
     * 下载背景图成功，刷新，在子线程返回，需要切换到主线程
     */
    @Override
    public void onSuccessImageDownLoad() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BaseAbstractActivity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
                if (currentActivity instanceof MainHomeActivity) {
                    initMainHomeBg();
                }
            }
        });
    }

    /**
     * 在子线程返回，需要切换到主线程
     */
    @Override
    public void onFailImageDownLoad() {

    }

    @Override
    public Handler getHandler() {
        return mHandler;
    }

    @Override
    public MainContract.Presenter getCurrentPresenter() {
        return mPresenter;
    }

    @Override
    public void initStaffRedPacket() {
        initStaffRedPacketEvent();
    }

    @Override
    public void AppCallAppPaymentChangeHomeFragment() {
        OnBottomButtonCheck(0);
        mBottomView.selectWithPosition(0);
    }

    @Override
    public Context getCurrentContext() {
        return mContext;
    }

    @Override
    public Activity getCurrentActivity() {
        return MainHomeActivity.this;
    }


    @Override
    public void getNewMessageUrlSuccess(NotificationJumpEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, response.getTarget());
            mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.what_is_new));
            mHashMaps.put(WebViewActivity.NEW_MSG, true);
            mHashMaps.put(WebViewActivity.SHARE_CONTENT, response.getShareTarget());
            ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getNewMessageUrlError(String errorCode, String errorMsg) {
        showErrorMsgDialog(MainHomeActivity.this, errorMsg);
    }


    @Override
    public void getMyDiscountUrlSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getMyDiscountUrlError(String errorCode, String errorMsg) {
        showErrorMsgDialog(MainHomeActivity.this, errorMsg);
    }

    @Override
    public void changeFragment() {
        ECouponsManagerFragment eCouponsManagerFragment = new ECouponsManagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(CURRENTPAGE, PAGE_MY);
        eCouponsManagerFragment.setArguments(bundle);
        switchDiffFragmentContent(eCouponsManagerFragment, R.id.fl_main, -1, false);
    }

    /**
     * 查询账户总积分
     */
    @Override
    public void goToNewCardFragment() {
        NewCardManagerFragment cardManagerFragment = new NewCardManagerFragment();
        switchDiffFragmentContent(cardManagerFragment, R.id.fl_main, -1, false);
        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    EventBus.getDefault().post(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT_NOTIFICATION, ""));
                }
            }, 200);
        }
    }

    /**
     * 优计划首页
     */
    @Override
    public void goToUPlanHomePage() {
        if (mPresenter != null) {
            mPresenter.getUplanUrl();
        }
    }

    /**
     * 查询信用卡奖赏记录
     */
    @Override
    public void goToCreditCardRewardFragment() {
        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_NEWS_CREDIT_REWARD, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
        sendAnalysisEvent(analysisButtonEntity);
        if (mCurrentFragment != null && mCurrentFragment instanceof CreditCardRewardFragment) {
            //当前正处于信用卡奖赏界面，切换至“我的任务”
            Message msg = ((CreditCardRewardFragment) mCurrentFragment).creditCardRewardHandler.obtainMessage();
            msg.what = CreditCardRewardFragment.MSG_ID_CREDIT_CARD_REWARD;
            ((CreditCardRewardFragment) mCurrentFragment).creditCardRewardHandler.sendMessage(msg);
        } else {
            CreditCardRewardFragment creditCardRewardFragment = new CreditCardRewardFragment();
            switchDiffFragmentContent(creditCardRewardFragment, R.id.fl_main, -1, false);
        }
    }

    /**
     * 最新消息
     */
    @Override
    public void goToNewMessagePage() {
        if (mPresenter != null) {
            mPresenter.getNewMessageUrl();
        }
    }

    /**
     * 我的优惠券
     */
    @Override
    public void goToMyDiscountPage() {
        if (mPresenter != null) {
            mPresenter.getMyDiscountUrl();
        }
    }


}
