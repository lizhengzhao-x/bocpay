package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * 我的账户注册的时候左边选择图标  右边 Direct TopUp 底部 Debit directly from the primary account view封装
 */

public class ImageTopUpView extends RelativeLayout {
    private Context mContext;

    private boolean mCheck = false;

    public ImageView getmLeftImage() {
        return mLeftImage;
    }

    private ImageView mLeftImage;
    private TextView tvTitle;

    int mIconDefaultID = R.mipmap.icon_check_choose_register_default;
    int mIconCheckID = R.mipmap.icon_check_choose_circle;

    public boolean isCheck() {
        return mCheck;
    }

    public void setCheck(boolean check) {
        this.mCheck = check;
        setLeftImage();
    }

    public void switchCheck() {
        if (!mCheck) {
            this.mCheck = !mCheck;
            setLeftImage();
        }
    }

    public void setLeftImage() {
        if (!mCheck) {
            mLeftImage.setImageResource(mIconDefaultID);
        } else {
            mLeftImage.setImageResource(mIconCheckID);
        }
    }

    public void setLeftImage(int resId) {
        mLeftImage.setImageResource(resId);
    }

    public ImageTopUpView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public ImageTopUpView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public ImageTopUpView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {

        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_image_topup_view, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ArrowTopUpView, defStyle, 0);

        String mainTitle = a.getString(R.styleable.ArrowTopUpView_main_text);
        String subTitle = a.getString(R.styleable.ArrowTopUpView_sub_text);
        boolean subVisiable = a.getBoolean(R.styleable.ArrowTopUpView_sub_visiable, true);
        boolean showLine = a.getBoolean(R.styleable.ArrowTopUpView_line_visiable, true);

        tvTitle = rootView.findViewById(R.id.id_main_title);

        TextView tvsubTitle = rootView.findViewById(R.id.id_sub_title);

        View lineView = rootView.findViewById(R.id.v_line);

        tvTitle.setText(mainTitle);
        tvsubTitle.setText(subTitle);
        tvsubTitle.setVisibility(subVisiable ? View.VISIBLE : View.GONE);
        lineView.setVisibility(showLine ? View.VISIBLE : View.GONE);
        a.recycle();
        mLeftImage = rootView.findViewById(R.id.id_left_image);

    }

    public void setLeftText(String title) {
        tvTitle.setText(title);
    }

    public void setIconCheck(int resId) {
        mIconCheckID = resId;
    }

    public void setIconDefault(int resId) {
        mIconDefaultID = resId;
    }
}
