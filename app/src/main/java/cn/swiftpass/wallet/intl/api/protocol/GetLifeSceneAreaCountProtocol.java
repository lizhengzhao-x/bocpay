package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.HomeLifeSceneAreaCountEntity;


public class GetLifeSceneAreaCountProtocol extends BaseProtocol {


    public GetLifeSceneAreaCountProtocol(NetWorkCallbackListener<HomeLifeSceneAreaCountEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/postLogin/queryLifeSceneAreaCount";
    }

    @Override
    public void packData() {
        super.packData();
    }
}
