package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;

public class BocPayTransUtils {


    public static int getImageResourceForSmart(SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult) {

        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "2")) {
            //收款 收利是 也是收款
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                //收利是
                if (TextUtils.equals(paymentEnquiryResult.getOrderType(), "NRP")) {
                    //员工利是
                    return R.mipmap.icon_transfer_redpacket_successful;
                } else {
                    return R.mipmap.icon_transactions_redpacket;
                }
            } else {
                return R.mipmap.icon_transactions_topup;
            }


        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "3")) {
            //转账
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                //派利是
                //派利是图标 区分
                return R.mipmap.icon_transactions_redpacket;
            } else {
                return R.mipmap.icon_transactions_transfer;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "4")) {
            //积分抵消欠账
            return R.mipmap.icon_transactions_points;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "10")) {
            //增值
            return R.mipmap.icon_transactions_topup;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "5")) {
            //转回余额
            return R.mipmap.icon_transactions_cashback;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "7")) {
            //跨境汇款
            if (paymentEnquiryResult.isCrossBorderTransfer()) {
                return R.mipmap.icon_transactions_crossborder_remittance;
            } else {
                return R.mipmap.icon_transactions_merchant;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "8")) {
            //首榜奖赏
            return R.mipmap.icon_transactions_reward;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "9")) {
            //银行账户转账
            return R.mipmap.icon_transactions_transfer;
        } else {


            //原来的
            return R.mipmap.icon_transactions_merchant;
        }
    }


    public static int getImageResource(OrderQueryEntity paymentEnquiryResult) {
        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "1")) {
            //银联消费
            if (paymentEnquiryResult.isWeChatPay()) {
                return R.mipmap.icon_transaction_wechat_pay;
            } else if (paymentEnquiryResult.isUplanInfo()) {
                //uplan只可能在bocpay这一列出现
                return R.mipmap.icon_transactions_uplan;
            } else if (paymentEnquiryResult.isAppCallApp()) {
                return R.mipmap.icon_transactions_appcallapp;
            } else {
                return R.mipmap.icon_transactions_merchant;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "2")) {
            //收款 收利是 也是收款
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                //收利是
                if (TextUtils.equals(paymentEnquiryResult.getOrderType(), "NRP")) {
                    //员工利是
                    return R.mipmap.icon_transfer_redpacket_successful;
                }else{
                    return R.mipmap.icon_transactions_redpacket;
                }

            } else {
                return R.mipmap.icon_transactions_topup;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "3")) {
            //转账
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                //派利是
                //派利是图标 区分
                return R.mipmap.icon_transactions_redpacket;
            } else {
                return R.mipmap.icon_transactions_transfer;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "4")) {
            if (paymentEnquiryResult.isAppCallApp()) {
                return R.mipmap.icon_transactions_appcallapp;
            } else {
                //积分抵消欠账
                return R.mipmap.icon_transactions_points;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6")) {
            //增值
            return R.mipmap.icon_transactions_topup;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "5")) {
            //转回余额
            return R.mipmap.icon_transactions_cashback;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "7")) {
            //跨境汇款
            if (paymentEnquiryResult.isCrossBorderTransfer()) {
                return R.mipmap.icon_transactions_crossborder_remittance;
            } else {
                return R.mipmap.icon_transactions_merchant;
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "8")) {
            //首榜奖赏
            return R.mipmap.icon_transactions_reward;
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "9")) {
            //银行账户转账
            return R.mipmap.icon_transactions_transfer;
        } else {
            //原来的
            return R.mipmap.icon_transactions_merchant;
        }
    }

    /**
     * @param mContext
     * @param showType
     * @param localView 是否是保存到本地
     * @return
     */
    public static int getColor(Context mContext, String showType, boolean localView) {
        if (localView) {
            return Color.parseColor("#24272B");
        }
        if (!TextUtils.isEmpty(showType) && TextUtils.equals(showType, "+")) {
            return mContext.getResources().getColor(R.color.color_green_one);
        } else if (!TextUtils.isEmpty(showType) && TextUtils.equals(showType, "-")) {
            return mContext.getResources().getColor(R.color.color_red_trans);
        } else {
            return mContext.getResources().getColor(R.color.app_black);
        }
    }
}
