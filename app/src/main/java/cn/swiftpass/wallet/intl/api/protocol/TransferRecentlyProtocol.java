package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 转账用户列表
 */
public class TransferRecentlyProtocol extends BaseProtocol {
    String transferType;

    public TransferRecentlyProtocol(String type, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.transferType = type;
        mUrl = "api/transfer/transferRecently";
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(transferType)) {
            mBodyParams.put(RequestParams.TRANSTYPE, transferType);
        }
    }

}
