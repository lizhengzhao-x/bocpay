package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.utils.QRCodeUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByStaticContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.TransferSetLimitEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferByStaticPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

import static android.app.Activity.RESULT_OK;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEFROM;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGE_STATICCODE;

/**
 * Created by aijingya on 2018/7/27.
 *
 * @Package cn.swiftpass.wallet.intl.activity
 * @Description: 收款码页面
 * @date 2018/7/27.14:24.
 */

public class TransferByStaticCodeFragment extends BaseFragment<TransferByStaticContract.Presenter> implements TransferByStaticContract.View {

    private static final int REQ_SET_LIMIT = 1001;
    public static final String DATA_SET_LIMIT = "DATA_SET_LIMIT";
    public static final String DATA_SET_AMOUNT_LIMIT = "DATA_SET_AMOUNT_LIMIT";
    @BindView(R.id.iv_static_code)
    ImageView ivStaticCode;
    @BindView(R.id.ll_save_image)
    LinearLayout llSaveImage;
    @BindView(R.id.ll_msg)
    LinearLayout llMsg;
    @BindView(R.id.id_email)
    TextView idEmail;
    @BindView(R.id.id_phone)
    TextView idPhone;
    @BindView(R.id.tv_tag)
    TextView tagTv;
    @BindView(R.id.tv_msg)
    TextView msgTv;
    @BindView(R.id.tv_currency)
    TextView currencyTv;
    @BindView(R.id.tv_transfer_amount)
    TextView transferAmountTv;
    @BindView(R.id.line_msg)
    View msgLine;
    @BindView(R.id.tv_account_info)
    TextView tvAccountInfo;


    @BindView(R.id.tv_save_image)
    TextView tvSaveImage;

    @BindView(R.id.view_line)
    View lineView;


    @BindView(R.id.cl_line)
    ConstraintLayout lineLayout;


    private StaticCodeEntity staticCodeEntity;
    private boolean isShowEmail = true;
    private List<StaticCodeEntity.MobnQRBean> emailQRBeans;
    private List<StaticCodeEntity.MobnQRBean> mobileQRBeans;
    private String currentQrCode;
    private TransferSetLimitEntity dataSetLimit;
    private String remarks;
    private Bitmap bitmapEmpty;
    private CustomDialog mDealDialog;
    /**
     * 当前是在哪个界面设置金额 默认显示手机号
     */
    private boolean isStillPhonePage = true;
    private boolean isInLeft = true;

    public static final String STATICCODE_LIMIT_ENTITY = "STATICCODE_LIMIT_ENTITY";

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.P3_B0_10);
            bitmapEmpty = Bitmap.createBitmap(AndroidUtils.dip2px(mActivity, 200), AndroidUtils.dip2px(mActivity, 200), Bitmap.Config.RGB_565);
            bitmapEmpty.eraseColor(Color.parseColor("#f5f5f5"));
            getTransferFoundQrCode();
        }
    }

    @Override
    protected void initView(View v) {
        //updateUI();
        idEmail.setTextColor(getResources().getColor(R.color.hint_color));
        idPhone.setTextColor(getResources().getColor(R.color.app_black));

        tvSaveImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                saveImgToLocal();
            }
        });

    }

    @Override
    protected TransferByStaticContract.Presenter loadPresenter() {
        return new TransferByStaticPresenter();
    }


    private void getTransferFoundQrCode() {
        if (getArguments() != null && getArguments().getSerializable(STATICCODE_LIMIT_ENTITY) != null) {
            //从aa收款金额带过来
            TransferSetLimitEntity transferSetLimitEntity = (TransferSetLimitEntity) getArguments().getSerializable(STATICCODE_LIMIT_ENTITY);
            dataSetLimit = transferSetLimitEntity;
            if (transferSetLimitEntity != null) {
                String amountStr = dataSetLimit.isAAChecked ? transferSetLimitEntity.aaAmount : transferSetLimitEntity.totalAmount;
                mPresenter.updateQRCode(amountStr, transferSetLimitEntity.msg);
                updateMsgView(transferSetLimitEntity.msg);
                if (Double.parseDouble(transferSetLimitEntity.aaAmount) > 0) {
                    currencyTv.setVisibility(View.VISIBLE);
                    transferAmountTv.setText(AndroidUtils.subZeroAndDot(amountStr));
                } else {
                    currencyTv.setVisibility(View.GONE);
                }
                return;
            }
        }
        String transferAmount = transferAmountTv.getText().toString();
        if (TextUtils.isEmpty(transferAmount)) {
            mPresenter.pullQRCode();
        } else {
            //离线断网问题 AndroidUtils.subZeroAndDot(transferAmount)
            String remarkTv = msgTv.getText().toString();
            mPresenter.updateQRCode(transferAmount.replace(",",""), remarkTv);
        }
    }


    private void updateUI() {
        if (staticCodeEntity != null) {
            emailQRBeans = staticCodeEntity.getEmalQR();
            mobileQRBeans = staticCodeEntity.getMobnQR();
        } else {
            emailQRBeans = null;
            mobileQRBeans = null;
        }

        if (mobileQRBeans != null && mobileQRBeans.size() > 0 && isStillPhonePage) {
            isShowEmail = false;
            isStillPhonePage = true;
        } else if (emailQRBeans != null && emailQRBeans.size() > 0 && !isStillPhonePage) {
            isShowEmail = true;
        } else {
            isShowEmail = true;
        }

        //需要切换到显示手机号
        if (isStillPhonePage) {
            if (mobileQRBeans != null && mobileQRBeans.size() > 0) {
                showPhoneImage();
                showTab(false);
            } else if (emailQRBeans != null && emailQRBeans.size() > 0) {
                showEmailImage();
                showTab(true);
            } else {
                showMessageDialog(getString(R.string.CO1_2a_9), false);
            }
        } else {
            //需要切换到邮箱
            if (emailQRBeans != null && emailQRBeans.size() > 0) {
                showEmailImage();
                showTab(true);
            } else if (mobileQRBeans != null && mobileQRBeans.size() > 0) {
                showPhoneImage();
                showTab(false);
            } else {
                tvAccountInfo.setText("");
                showMessageDialog(getString(R.string.CO1_2a_8), true);
            }
        }

    }

    public void showTab(boolean isShowEmail) {
        if (isShowEmail) {
            animToRight();
        } else {
            animToLeft();
        }
        idEmail.setTextColor(isShowEmail ? getResources().getColor(R.color.app_black) : getResources().getColor(R.color.hint_color));
        idPhone.setTextColor(isShowEmail ? getResources().getColor(R.color.hint_color) : getResources().getColor(R.color.app_black));
    }


    @Override
    public void onResume() {
        super.onResume();
        if (mActivity != null) {
            mActivity.setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
        }
    }

    @Override
    public void onPause() {
        if (mActivity != null) {
            mActivity.setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
        }
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (bitmapEmpty != null && !bitmapEmpty.isRecycled()) {
            bitmapEmpty.recycle();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_by_static_code;
    }


    @OnClick({R.id.id_email, R.id.id_phone, R.id.tv_share, R.id.ll_set_limit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_email:
                if (isShowEmail) {
                    return;
                }
                isShowEmail = true;
                showTab(true);
                if (emailQRBeans == null && mobileQRBeans == null) {
                    getTransferFoundQrCode();
                    isStillPhonePage = false;
                    showEmailImage();
                    return;
                }
                if (emailQRBeans == null || emailQRBeans.size() == 0) {
                    showMessageDialog(getString(R.string.CO1_2a_8), true);
                    ivStaticCode.setImageBitmap(bitmapEmpty);
                    currentQrCode = "";
                    //如果两个码都为空的时候（比如上次超时了），再去拉取一次
                    if (mobileQRBeans == null || mobileQRBeans.isEmpty()) {
                        getTransferFoundQrCode();
                    }
                    return;
                }
                showEmailImage();
                break;
            case R.id.id_phone:
                if (!isShowEmail) {
                    return;
                }
                isShowEmail = false;
                showTab(false);
                if (emailQRBeans == null && mobileQRBeans == null) {
                    getTransferFoundQrCode();
                    isStillPhonePage = true;
                    showPhoneImage();
                    return;
                }
                if (mobileQRBeans == null || mobileQRBeans.isEmpty()) {
                    showMessageDialog(getString(R.string.CO1_2a_9), false);
                    ivStaticCode.setImageBitmap(bitmapEmpty);
                    currentQrCode = "";
                    if (emailQRBeans == null || emailQRBeans.isEmpty()) {
                        getTransferFoundQrCode();
                    } else {
                        showTab(true);
                        showEmailImage();
                    }
                    return;
                }
                showPhoneImage();
                break;
            case R.id.tv_share:
                shareImgToOther();
                break;
            case R.id.ll_set_limit:
                if (ButtonUtils.isFastDoubleClick()) return;
                if (staticCodeEntity != null) {
                    Intent intent = new Intent(getContext(), TransferSetLimitActivity.class);
                    intent.putExtra(PAGEFROM, PAGE_STATICCODE);
                    if (dataSetLimit != null) {
                        intent.putExtra(DATA_SET_LIMIT, dataSetLimit);
                    }
                    if (isShowEmail && staticCodeEntity != null && staticCodeEntity.getEmalQR() != null && staticCodeEntity.getEmalQR().size() > 0) {
                        intent.putExtra(DATA_SET_AMOUNT_LIMIT, staticCodeEntity.getEmalQR().get(0).getOutBalLmt());
                    } else if (staticCodeEntity != null && staticCodeEntity.getMobnQR() != null && staticCodeEntity.getMobnQR().size() > 0) {
                        intent.putExtra(DATA_SET_AMOUNT_LIMIT, staticCodeEntity.getMobnQR().get(0).getOutBalLmt());
                    }
                    this.startActivityForResult(intent, REQ_SET_LIMIT);
                } else {
                    getTransferFoundQrCode();
                }
                break;

            default:
                break;
        }
    }

    private void saveImgToLocal() {
        if (TextUtils.isEmpty(currentQrCode)) {
            return;
        }
        PermissionInstance.getInstance().getStoreImagePermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                saveImageToGalley(currentQrCode);
            }

            @Override
            public void rejectPermission() {
                showLackOfPermissionDialog();
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void shareImgToOther() {
        if (TextUtils.isEmpty(currentQrCode)) {
            return;
        }
        if (ButtonUtils.isFastDoubleClick()) return;
        shareImage(currentQrCode);
//        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
//            @Override
//            public void acceptPermission() {
//                shareImage(currentQrCode);
//            }
//
//            @Override
//            public void rejectPermission() {
//                showLackOfPermissionDialog();
//            }
//        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    public void animToRight() {
        if (isInLeft) {
            isInLeft = false;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineLayout.getPaddingLeft(), lineView.getWidth());
            animator.setDuration(200);
            animator.start();
        }

    }

    public void animToLeft() {
        if (!isInLeft) {
            isInLeft = true;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineView.getWidth(), 0);
            animator.setDuration(200);
            animator.start();
        }

    }

    private void showPhoneImage() {
        isStillPhonePage = true;
        isShowEmail = false;
        if (mobileQRBeans != null) {
            currentQrCode = mobileQRBeans.get(0).getQrCode();
            Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrCode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_logo), mActivity);
            ivStaticCode.setImageBitmap(bitmapTmp);
            String accountInfo = AndroidUtils.getPrimaryAccountDisplay(mobileQRBeans.get(0).getAcTp(), mobileQRBeans.get(0).getAcNo());
            tvAccountInfo.setText(accountInfo);

        } else {
            tvAccountInfo.setText("");
        }
        if (staticCodeEntity != null && !TextUtils.isEmpty(mobileQRBeans.get(0).getProxyId())) {
            tagTv.setText(mobileQRBeans.get(0).getProxyId());
        } else {
            tagTv.setText("");
        }
    }

    private void showEmailImage() {
        //if (TextUtils.isEmpty(currentQrcode)) return;
        isStillPhonePage = false;
        isShowEmail = true;
        if (emailQRBeans != null) {
            currentQrCode = emailQRBeans.get(0).getQrCode();
            Bitmap bitmap = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrCode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_logo), mActivity);
            ivStaticCode.setImageBitmap(bitmap);
            String accountInfo = AndroidUtils.getPrimaryAccountDisplay(emailQRBeans.get(0).getAcTp(), emailQRBeans.get(0).getAcNo());
            tvAccountInfo.setText(accountInfo);
        } else {
            tvAccountInfo.setText("");
        }
        if (staticCodeEntity != null && emailQRBeans != null && !TextUtils.isEmpty(emailQRBeans.get(0).getProxyId())) {
            tagTv.setText(emailQRBeans.get(0).getProxyId());
        } else {
            tagTv.setText("");
        }
    }

    private void showMessageDialog(String content, final boolean isEmailTab) {
        String title = "   ";
        String msg = content;
        String left = getString(R.string.EC01_20);
        String right = getString(R.string.CO1_5_3);
        DialogInterface.OnClickListener rightListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (isEmailTab) {
                    showTab(false);
                    showPhoneImage();
                } else {
                    showTab(true);
                    showEmailImage();
                }
                HashMap<String, Object> params = new HashMap<String, Object>();
                params.put(FpsManageActivity.ACTION_CALL_BACK, FpsManageActivity.ACTION_BACK_TO_HOME);
                ActivitySkipUtil.startAnotherActivity(mActivity, FpsManageActivity.class, params, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        };
        DialogInterface.OnClickListener cancelListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                if (isEmailTab) {
                    showTab(false);
                    showPhoneImage();
                } else {
                    showTab(true);
                    showEmailImage();
                }
            }
        };

        AndroidUtils.showTipDialog(mActivity, title, msg, right, left, rightListener, cancelListener);
    }


    private void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.SH2_4_1a));
        builder.setMessage(getString(R.string.SH2_4_1b));
        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mActivity);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    private void shareImage(String qrcode) {
        if (TextUtils.isEmpty(currentQrCode)) {
            return;
        }
        View saveQrInfoV = LayoutInflater.from(mContext).inflate(R.layout.include_save_qrcode, null);
        ImageView qrcodeImg = saveQrInfoV.findViewById(R.id.img_qrcode);
        TextView remarksTv = saveQrInfoV.findViewById(R.id.tv_remarks);
        TextView moneyTv = saveQrInfoV.findViewById(R.id.tv_money);
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(transferAmountTv.getText().toString())) {
            moneyTv.setText(getString(R.string.CO1_3a_2) + " " + transferAmountTv.getText().toString());
        }
        Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(qrcode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_logo_bocpay_collect_local), mActivity);
        qrcodeImg.setImageBitmap(bitmapTmp);

        AndroidUtils.layoutView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext));
        saveQrInfoV.setDrawingCacheEnabled(true);
        saveQrInfoV.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        saveQrInfoV.setDrawingCacheBackgroundColor(Color.WHITE);
        // 把一个View转换成图片
        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext), mContext.getResources().getColor(R.color.color_blue_one));

        //YYYYMMDD_hhmmss
        Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, "request.jpg");
        saveQrInfoV.destroyDrawingCache();
        if (uri != null) {
            AndroidUtils.shareImage(mActivity, "", uri);
        }
    }

    private void saveImageToGalley(String qrcode) {
        if (TextUtils.isEmpty(currentQrCode)) return;
        View saveQrInfoV = LayoutInflater.from(mContext).inflate(R.layout.include_save_qrcode, null);
        ImageView qrcodeImg = saveQrInfoV.findViewById(R.id.img_qrcode);
        TextView remarksTv = saveQrInfoV.findViewById(R.id.tv_remarks);
        TextView moneyTv = saveQrInfoV.findViewById(R.id.tv_money);
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(remarks)) {
            remarksTv.setText(remarks);
        }
        if (!TextUtils.isEmpty(transferAmountTv.getText().toString())) {
            moneyTv.setText(getString(R.string.CO1_3a_2) + " " + transferAmountTv.getText().toString());
//            moneyTv.setText(getString(R.string.CO1_3a_2) + " " + AndroidUtils.formatPriceWithPoint(Double.parseDouble(transferAmountTv.getText().toString()), true));
        }
        Bitmap bitmapTmp = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrCode, AndroidUtils.dip2px(getActivity(), 200)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_logo_bocpay_collect_local), mActivity);
        qrcodeImg.setImageBitmap(bitmapTmp);

        AndroidUtils.layoutView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext));
        saveQrInfoV.setDrawingCacheEnabled(true);
        saveQrInfoV.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        saveQrInfoV.setDrawingCacheBackgroundColor(Color.WHITE);
        // 把一个View转换成图片
        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(saveQrInfoV, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext), mContext.getResources().getColor(R.color.color_blue_one));

        AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cachebmp, "Request_" + AndroidUtils.localImageStr(System.currentTimeMillis()));
        saveQrInfoV.destroyDrawingCache();
        Toast.makeText(mContext, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
        if (mFmgHandler != null) {
            //用户提出重复点击 黑色toast 一直显示 这边做了一个简单的延迟
            tvSaveImage.setEnabled(false);
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    tvSaveImage.setEnabled(true);
                }
            }, 1500);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_SET_LIMIT && resultCode == RESULT_OK) {
            dataSetLimit = (TransferSetLimitEntity) data.getSerializableExtra(DATA_SET_LIMIT);
            if (dataSetLimit != null) {
                String amount = dataSetLimit.isAAChecked ? dataSetLimit.aaAmount : dataSetLimit.totalAmount;
                updateMsgView(dataSetLimit.msg);
                if (Double.parseDouble(amount) > 0) {
                    currencyTv.setVisibility(View.VISIBLE);
//                    transferAmountTv.setText(AndroidUtils.subZeroAndDot(amount));
                    transferAmountTv.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(AndroidUtils.subZeroAndDot(amount)), true));
                    mPresenter.updateQRCode(AndroidUtils.subZeroAndDot(amount), dataSetLimit.msg);
                } else {
                    currencyTv.setVisibility(View.GONE);
                }

            }
        }

    }

    private void updateMsgView(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            remarks = msg;
            msgTv.setText(msg);
            llMsg.setVisibility(View.VISIBLE);
            msgLine.setVisibility(View.VISIBLE);
        } else {
            llMsg.setVisibility(View.GONE);
            msgLine.setVisibility(View.GONE);
        }
    }

    @Override
    public void updateQRCodeSuccess(StaticCodeEntity response) {
        staticCodeEntity = response;
        updateUI();
    }

    @Override
    public void updateQRCodeError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
        staticCodeEntity = null;
        emailQRBeans = null;
        mobileQRBeans = null;
        currentQrCode = null;

        showTab(!isStillPhonePage);
        ivStaticCode.setImageBitmap(bitmapEmpty);
    }

    @Override
    public void pullQRCodeSuccess(StaticCodeEntity response) {
        staticCodeEntity = response;
        updateUI();
    }

    @Override
    public void pullQRCodeError(String errorCode, String errorMsg) {
        if ("EWA3026".equals(errorCode)) {
            CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
            builder.setTitle(getString(R.string.CO1_5_1));
            builder.setMessage("");
            builder.setPositiveButton(getString(R.string.CO1_5_3), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class);
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            });
            builder.setNegativeButton(getString(R.string.CO1_5_2), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
            Activity mActivity = (Activity) mContext;
            if (mActivity != null && !mActivity.isFinishing()) {
                CustomDialog dialog = builder.create();
                dialog.setCancelable(false);
                dialog.show();
            }
        } else {
            showErrorMsgDialog(mContext, errorMsg);
        }
        isShowEmail = false;
    }
}
