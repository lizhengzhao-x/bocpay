package cn.swiftpass.wallet.intl.entity;

import android.content.Context;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.GreetingEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * @author zf
 * <p>
 * 页面初始化数据的加载
 */

public class SystemPagerDataEntity extends BaseEntity {
    ArrayList<PreLoginBannerEntity> bottomBannerEntities;
    private String preLoginBgImageUrl;
    private String preLoginBgImageVer;
    private String postLoginBgImageUrl;
    private String postLoginBgImageVer;
    private String virtualCardUrl;
    private String virtualCardShareUrl;
    private ArrayList<GreetingEntity> greetings;
    private VirtualUrlEntity virtualCardUrl_zh_HK=new VirtualUrlEntity();
    private VirtualUrlEntity virtualCardUrl_zh_CN=new VirtualUrlEntity();
    private VirtualUrlEntity virtualCardUrl_en_US=new VirtualUrlEntity();


    public VirtualUrlEntity getVirtualCardUrl_zh_HK() {
        return virtualCardUrl_zh_HK;
    }

    public void setVirtualCardUrl_zh_HK(VirtualUrlEntity virtualCardUrl_zh_HK) {
        this.virtualCardUrl_zh_HK = virtualCardUrl_zh_HK;
    }

    public VirtualUrlEntity getVirtualCardUrl_zh_CN() {
        return virtualCardUrl_zh_CN;
    }

    public void setVirtualCardUrl_zh_CN(VirtualUrlEntity virtualCardUrl_zh_CN) {
        this.virtualCardUrl_zh_CN = virtualCardUrl_zh_CN;
    }

    public VirtualUrlEntity getVirtualCardUrl_en_US() {
        return virtualCardUrl_en_US;
    }

    public void setVirtualCardUrl_en_US(VirtualUrlEntity virtualCardUrl_en_US) {
        this.virtualCardUrl_en_US = virtualCardUrl_en_US;
    }
    /**
     * 为了保证图片能在版本或者地址变化时更新
     * @return
     */
    public String getPreLoginBgImageVer() {
        return preLoginBgImageUrl+preLoginBgImageVer;
    }

    public void setPreLoginBgImageVer(String preLoginBgImageVer) {
        this.preLoginBgImageVer = preLoginBgImageVer;
    }

    /**
     * 为了保证图片能在版本或者地址变化时更新
     * @return
     */
    public String getPostLoginBgImageVer() {
        return postLoginBgImageUrl+postLoginBgImageVer;
    }

    public void setPostLoginBgImageVer(String postLoginBgImageVer) {
        this.postLoginBgImageVer = postLoginBgImageVer;
    }

    public String getVirtualCardShareUrl(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getVirtualCardUrl_zh_CN().getVirtualCardShareUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getVirtualCardUrl_zh_HK().getVirtualCardShareUrl();
        } else {
            return getVirtualCardUrl_en_US().getVirtualCardShareUrl();
        }
    }

    public void setVirtualCardShareUrl(String virtualCardShareUrl) {
        this.virtualCardShareUrl = virtualCardShareUrl;
    }

    public ArrayList<PreLoginBannerEntity> getBottomBanner() {
        return bottomBannerEntities;
    }

    public void setBottomBanner(ArrayList<PreLoginBannerEntity> bottomBanner) {
        this.bottomBannerEntities = bottomBanner;
    }

    public String getPreLoginBgImageUrl() {
        return preLoginBgImageUrl;
    }

    public void setPreLoginBgImageUrl(String preLoginBgImageUrl) {
        this.preLoginBgImageUrl = preLoginBgImageUrl;
    }

    public String getPostLoginBgImageUrl() {
        return postLoginBgImageUrl;
    }

    public void setPostLoginBgImageUrl(String postLoginBgImageUrl) {
        this.postLoginBgImageUrl = postLoginBgImageUrl;
    }

    public String getVirtualCardUrl(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getVirtualCardUrl_zh_CN().getVirtualCardUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getVirtualCardUrl_zh_HK().getVirtualCardUrl();
        } else {
            return getVirtualCardUrl_en_US().getVirtualCardUrl();
        }

    }

    public void setVirtualCardUrl(String virtualCardUrl) {
        this.virtualCardUrl = virtualCardUrl;
    }

    public ArrayList<GreetingEntity> getGreetings() {
        return greetings;
    }

    public void setGreetings(ArrayList<GreetingEntity> greetings) {
        this.greetings = greetings;
    }
}
