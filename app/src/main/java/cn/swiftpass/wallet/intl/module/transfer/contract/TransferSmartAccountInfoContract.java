package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;

public class TransferSmartAccountInfoContract {

    public interface View extends IView {
        void getSmartAccountInfoSuccess(MySmartAccountEntity response, String action, boolean isOnResume);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);
    }

    public interface Presenter<P extends TransferSmartAccountInfoContract.View> extends IPresenter<P> {
        void getSmartAccountInfo(String action, boolean iShowLoading, boolean isOnResume);

        void getRegEddaInfo();
    }
}
