package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 信用卡确认注册接口，返回对象为
 */

public class SmartForgotPwdProtocol extends E2eeProtocol {
    public static final String TAG = SmartForgotPwdProtocol.class.getSimpleName();
    /**
     *支付密码
     */
    //String mPasscode;
    /**
     * 标识取【V】：B：绑定检查；V：仅验证；F：忘记密码；
     */
    String mAction;

    public SmartForgotPwdProtocol(String passcode, String action, NetWorkCallbackListener dataCallback) {
        this.mPasscode = passcode;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/settingPwd";
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAY_PASS_CODE, mPasscode);
        mBodyParams.put(RequestParams.ACTION, mAction);
    }
}
