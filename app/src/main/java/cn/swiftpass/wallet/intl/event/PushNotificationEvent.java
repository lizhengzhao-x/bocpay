package cn.swiftpass.wallet.intl.event;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RedPackageEntity;
import cn.swiftpass.wallet.intl.entity.SevenVersionNotificationEntity;
import cn.swiftpass.wallet.intl.module.MainContract;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.RedPacketDialogUtils;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_PUSHNOTIFICATION;

public class PushNotificationEvent extends BaseEventType {
    public static final String PUSH_NOTIFICATION_EVENT = "PushNotificationEvent";
    public static final String RED_PACKET_RED_PACKET_PARAMS = "red_packet_red_packet_params";
    public static final String SEVEN_VERSION_NOTIFICATION_PARAMS = "seven_version_notification_params";
    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";

    /**
     * EWA_TO_TAL_GP_PARAM     查询账户总积分
     * EWA_U_PLAN_HOME_PARAM   优计划首页
     * EWA_NEW_MESSAGE         最新消息
     * EWA_MY_DISCOUNT_PARAM   我的优惠券
     * EWA_CREDIT_AWARD_PARAM  查询信用卡奖赏记录
     */
    public static final String EWA_TO_TAL_GP_PARAM = "EWATotalGp";
    public static final String EWA_U_PLAN_HOME_PARAM = "EWAUplanHome";
    public static final String EWA_NEW_MESSAGE = "EWANewMessage";
    public static final String EWA_MY_DISCOUNT_PARAM = "EWAMyDiscount";
    public static final String EWA_CREDIT_AWARD_PARAM = "EWACreditAward";

    public PushNotificationEvent() {
        super(EVENT_PUSHNOTIFICATION.eventPriority, EVENT_PUSHNOTIFICATION.eventName);
    }

    @Override
    protected String getCurrentEventType() {
        return PUSH_NOTIFICATION_EVENT;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof PushNotificationDialogDealOwner) {
            PushNotificationDialogDealOwner pushNotificationDialogDealOwner = (PushNotificationDialogDealOwner) currentActivity;
            showPushNotifacationDialog(pushNotificationDialogDealOwner);
        }
        return true;
    }

    private void showPushNotifacationDialog(PushNotificationDialogDealOwner pushNotificationDialogDealOwner) {
        String messageType = (String) eventParams.get(MESSAGE_TYPE);
        RedPackageEntity redPackageEntity = null;
        SevenVersionNotificationEntity sevenVersionNotificationEntity = null;
        if (eventParams.get(RED_PACKET_RED_PACKET_PARAMS) != null) {
            redPackageEntity = (RedPackageEntity) eventParams.get(RED_PACKET_RED_PACKET_PARAMS);
        }
        if (eventParams.get(SEVEN_VERSION_NOTIFICATION_PARAMS) != null) {
            sevenVersionNotificationEntity = (SevenVersionNotificationEntity) eventParams.get(SEVEN_VERSION_NOTIFICATION_PARAMS);
        }
        if (TextUtils.equals(messageType, Constants.MSGTYPE_VCNTF)) {
            //点击虚拟卡推送
            showBindVirtualCardDialog(pushNotificationDialogDealOwner);
        } else if (redPackageEntity != null) {
            //登录成功是否需要修弹出红包
            //STAFF表示员工利是，COMMON表示普通利是  如果不是在APP使用中 普通利是需要 “拆成功”：背景页面为拆利是页面；
            //利是类型 8 bocpay利是 9 员工利是
            if (TextUtils.equals(Constants.PUSH_REDPACKET_COMMON_TYPE, redPackageEntity.getPushRedPacketType())
                    || TextUtils.equals(Constants.RED_PACKET_TYPE_COMMON, redPackageEntity.getPushRedPacketType())) {
                if (pushNotificationDialogDealOwner.getHandler() != null) {
                    RedPackageEntity finalRedPackageEntity = redPackageEntity;
                    pushNotificationDialogDealOwner.getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (null == finalRedPackageEntity) {
                                return;
                            }
                            RedPacketDialogUtils.openRedPacket(pushNotificationDialogDealOwner.getCurrentActivity(), finalRedPackageEntity.getPushRrcRefNo(), Constants.RED_PACKET_PUSH_TYPE, true);
                        }
                    }, 500);
                }
            }
            pushNotificationDialogDealOwner.initStaffRedPacket();
        } else if (TextUtils.equals(messageType, Constants.MSGTYPE_CONRE)) {
            //点击消费奖赏推送
            if (pushNotificationDialogDealOwner.getHandler() != null) {
                pushNotificationDialogDealOwner.getHandler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pushNotificationDialogDealOwner.changeFragment();
//                        ECouponsManagerFragment eCouponsManagerFragment = new ECouponsManagerFragment();
//                        Bundle bundle = new Bundle();
//                        bundle.putInt(CURRENTPAGE, PAGE_MY);
//                        eCouponsManagerFragment.setArguments(bundle);
//                        switchDiffFragmentContent(eCouponsManagerFragment, R.id.fl_main, -1, false);
                    }
                }, 500);
            }
        } else if (sevenVersionNotificationEntity != null) {
            if (TextUtils.equals(messageType, EWA_TO_TAL_GP_PARAM)) {
                //查询账户总积分
                if (pushNotificationDialogDealOwner.getHandler() != null) {
                    pushNotificationDialogDealOwner.getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pushNotificationDialogDealOwner.goToNewCardFragment();
                        }
                    }, 600);
                }
            } else if (TextUtils.equals(messageType, EWA_U_PLAN_HOME_PARAM)) {
                //UPlan主界面
                pushNotificationDialogDealOwner.goToUPlanHomePage();
            } else if (TextUtils.equals(messageType, EWA_NEW_MESSAGE)) {
                //最新消息
                pushNotificationDialogDealOwner.goToNewMessagePage();
            } else if (TextUtils.equals(messageType, EWA_MY_DISCOUNT_PARAM)) {
                //我的优惠券
                pushNotificationDialogDealOwner.goToMyDiscountPage();
            } else if (TextUtils.equals(messageType, EWA_CREDIT_AWARD_PARAM)) {
                //查询信用卡奖赏记录
                if (pushNotificationDialogDealOwner.getHandler() != null) {
                    pushNotificationDialogDealOwner.getHandler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            pushNotificationDialogDealOwner.goToCreditCardRewardFragment();
                        }
                    }, 600);
                }
            }

        }
    }

    /**
     * 点击通知推送时，弹出的虚拟卡绑定 弹出窗口
     */
    private void showBindVirtualCardDialog(PushNotificationDialogDealOwner pushNotificationDialogDealOwner) {
        MainContract.Presenter mPresenter = pushNotificationDialogDealOwner.getCurrentPresenter();
        Activity activity = pushNotificationDialogDealOwner.getCurrentActivity();
//        Context context = pushNotificationDialogDealOwner.getCurrentContext();
        AndroidUtils.showTipDialog(activity, activity.getString(R.string.VC01_03_5), activity.getString(R.string.VC01_03_6),
                activity.getString(R.string.VC01_03_7), activity.getString(R.string.VC01_03_8), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!CacheManagerInstance.getInstance().isLogin()) {
                            ActivitySkipUtil.startAnotherActivity(activity, LoginActivity.class);
                        } else {
                            inputPswPopWindow(activity, mPresenter);
                        }
                    }
                });
    }

    private void inputPswPopWindow(Activity activity, MainContract.Presenter mPresenter) {
        VerifyPasswordCommonActivity.startActivityForResult(activity, false, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                //打开虚拟卡需要验证支付密码
                mPresenter.getVirtualCardList();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    public interface PushNotificationDialogDealOwner extends EventDealOwner {
        MainContract.Presenter getCurrentPresenter();

        void initStaffRedPacket();

        void changeFragment();

        /**
         * 查询账户总积分
         */
        void goToNewCardFragment();

        /**
         * 优计划首页
         */
        void goToUPlanHomePage();

        /**
         * 查询信用卡奖赏记录
         */
        void goToCreditCardRewardFragment();

        /**
         * 最新消息
         */
        void goToNewMessagePage();

        /**
         * 我的优惠券
         */
        void goToMyDiscountPage();
    }

}
