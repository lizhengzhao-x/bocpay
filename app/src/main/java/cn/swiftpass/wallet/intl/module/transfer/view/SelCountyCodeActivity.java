package cn.swiftpass.wallet.intl.module.transfer.view;


import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.widget.LeftImageArrowView;


public class SelCountyCodeActivity extends BaseCompatActivity {

    @BindView(R.id.id_country_china)
    LeftImageArrowView idCountryChina;
    @BindView(R.id.id_country_hk)
    LeftImageArrowView idCountryHk;
    @BindView(R.id.id_country_mc)
    LeftImageArrowView idCountryMc;
    @BindView(R.id.tv_msg)
    TextView tv_msg;

    private ArrayList<LeftImageArrowView> leftImageArrowViews;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        idCountryChina.setRightImageShow(true);
        idCountryMc.setRightImageShow(false);
        idCountryHk.setRightImageShow(false);
        idCountryChina.setLeftImageIsGone(true);
        idCountryMc.setLeftImageIsGone(true);
        idCountryHk.setLeftImageIsGone(true);
        leftImageArrowViews = new ArrayList<>();
        leftImageArrowViews.add(idCountryChina);
        leftImageArrowViews.add(idCountryHk);
        leftImageArrowViews.add(idCountryMc);
        String countryCode = getIntent().getExtras().getString(Constants.DATA_COUNTRY_CODE);
        //用来区分登入进入显示字段 1注册
        int loginType = getIntent().getIntExtra(Constants.LOGIN_TYPE, 0);
        if (loginType == 1) {
            //注册逻辑 显示底部提示
            tv_msg.setVisibility(View.VISIBLE);
            setToolBarTitle(R.string.IDV_2_1);
        } else {
            tv_msg.setVisibility(View.GONE);
            setToolBarTitle(R.string.title_region);
        }

        for (int i = 0; i < Constants.DATA_COUNTRY_CODES.length; i++) {
            if (Constants.DATA_COUNTRY_CODES[i].equals(countryCode)) {
                selPosition(i);
                break;
            }
        }
    }

    private void selPosition(int position) {
        for (int i = 0; i < leftImageArrowViews.size(); i++) {
            if (i == position) {
                leftImageArrowViews.get(i).setRightImageShow(true);
            } else {
                leftImageArrowViews.get(i).setRightImageShow(false);
            }
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_sel_country;
    }


    private void onResult(String code, int resourid) {
        Intent in = new Intent();
        in.putExtra(Constants.DATA_COUNTRY_CODE, code);
        in.putExtra(Constants.DATA_COUNTRY_IMAGE_URL, resourid);
        setResult(RESULT_OK, in);
        finish();
    }


    @OnClick({R.id.id_country_china, R.id.id_country_hk, R.id.id_country_mc})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_country_china:
                selPosition(0);
                onResult(Constants.DATA_COUNTRY_CODES[0], Constants.DATA_COUNTRY_CODES_RESOURCE[0]);
                break;
            case R.id.id_country_hk:
                selPosition(1);
                onResult(Constants.DATA_COUNTRY_CODES[1], Constants.DATA_COUNTRY_CODES_RESOURCE[1]);
                break;
            case R.id.id_country_mc:
                selPosition(2);
                onResult(Constants.DATA_COUNTRY_CODES[2], Constants.DATA_COUNTRY_CODES_RESOURCE[2]);
                break;
        }
    }
}
