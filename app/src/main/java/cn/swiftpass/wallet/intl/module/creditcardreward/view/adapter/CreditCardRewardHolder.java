package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.boc.commonui.base.widget.CircleProgressGroup;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardEntity;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class CreditCardRewardHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_credit_card_banner)
    ImageView ivCreditCardBanner;
    @BindView(R.id.tv_credit_card_msg_title_tip)
    TextView tvCreditCardMsgTitleTip;
    @BindView(R.id.tv_credit_card_msg_tip)
    TextView tvCreditCardMsgTip;
    @BindView(R.id.tv_un_finish_amount)
    TextView tvUnFinishAmount;
    @BindView(R.id.tv_reward_detail)
    TextView tvRewardDetail;
    @BindView(R.id.tv_account)
    TextView tvAccount;
    @BindView(R.id.tv_finish_total_amount)
    TextView tvFinishTotalAmount;
    @BindView(R.id.tv_can_get_total_max_tip)
    TextView tvCanGetTotalMaxTip;
    @BindView(R.id.tv_finish_reward_tip)
    TextView tvFinishRewardTip;
    @BindView(R.id.tv_can_get_total_max_limit)
    TextView tvCanGetTotalMaxLimit;
    @BindView(R.id.tv_reward_end_date_tip)
    TextView tvRewardEndDateTip;
    @BindView(R.id.tv_finish_reward_next_date)
    TextView tvFinishRewardNextDate;
    @BindView(R.id.tv_reward_end_date)
    TextView tvRewardEndDate;
    @BindView(R.id.tv_update_end_date)
    TextView tvUpdateEndDate;
    @BindView(R.id.donut_progress)
    CircleProgressGroup donutProgress;

    public CreditCardRewardHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        tvRewardDetail.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
    }

    public void bindData(Context context, CreditCardRewardEntity creditCardRewardEntity, OnRewadDetailListener listener) {
        tvUnFinishAmount.setVisibility(View.GONE);
        if (creditCardRewardEntity != null) {
            //设置进度条
            try {
                float progress = Float.parseFloat(creditCardRewardEntity.getCompletionRate());
                donutProgress.setProgress(progress);
            } catch (NumberFormatException e) {

            }
            //设置详情跳转
            if (!TextUtils.isEmpty(creditCardRewardEntity.getDetails())) {
                tvRewardDetail.setVisibility(View.VISIBLE);
                tvRewardDetail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.OnJumpDetail(creditCardRewardEntity.getDetails());

                    }
                });
            } else {
                tvRewardDetail.setVisibility(View.INVISIBLE);
            }


            //设置头部banner图片
            RequestOptions options = new RequestOptions()
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
            GlideApp.with(context)
                    .load(creditCardRewardEntity.getBannerUrl())
                    .placeholder(R.mipmap.img_placeholder_reward)
                    .apply(options)
                    .into(ivCreditCardBanner);
            //设置头部banner中的文字说明
            tvCreditCardMsgTitleTip.setText(creditCardRewardEntity.getTitle());
            tvCreditCardMsgTip.setText(creditCardRewardEntity.getContent());
            //尚未完成的账户金额
            if (!TextUtils.isEmpty(creditCardRewardEntity.getOweArrivalsSpendingAmt())) {
                tvUnFinishAmount.setText(context.getText(R.string.CRQ2107_1_9) + creditCardRewardEntity.getOweArrivalsSpendingAmt());
                tvUnFinishAmount.setVisibility(View.VISIBLE);
            }
            //账户
            tvAccount.setText(creditCardRewardEntity.getAccount());
            //合资格累计签账额
            //现金奖赏
            if (CreditRewardType.REWARD_TYPE_CASH.equals(creditCardRewardEntity.getRewardsType())) {
                tvFinishTotalAmount.setTextColor(context.getColor(R.color.color_FF149B1A));
            } else {
                //积分奖赏
                tvFinishTotalAmount.setTextColor(context.getColor(R.color.color_green));
            }

            tvFinishTotalAmount.setText(creditCardRewardEntity.getAccumulatedSpendingAmt());
            //(预计)可得  现金/积分  回赠(上限) 文本说明
            String totalMaxTip = CreditRewardType.REWARD_TYPE_CASH.equals(creditCardRewardEntity.getRewardsType()) ? context.getString(R.string.CRQ2107_1_13) : context.getString(R.string.CRQ2107_1_14);
            tvCanGetTotalMaxTip.setText(totalMaxTip);
            //累计签账达标后可获得的积分或者金额
            tvFinishRewardTip.setText(creditCardRewardEntity.getEarnPointsForReachingTarget());
            tvCanGetTotalMaxLimit.setText(creditCardRewardEntity.getObtainReward());
            tvRewardEndDate.setText(creditCardRewardEntity.getDeadline());
            tvUpdateEndDate.setText(creditCardRewardEntity.getLastUpdateDate());
        }
    }
}
