package cn.swiftpass.wallet.intl.event;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import java.util.HashMap;


public abstract class BaseEventType {

    protected final String eventType;
    protected final int eventPriority;
    protected final String eventDesc;
    protected HashMap eventParams;

    public BaseEventType(int eventPriority, String eventDesc) {
        this(eventPriority, eventDesc, null);
    }

    public BaseEventType(int eventPriority, String eventDesc, HashMap<String, Object> eventParams) {
        this.eventType = getCurrentEventType();
        this.eventPriority = eventPriority;
        this.eventDesc = eventDesc;
        this.eventParams = eventParams;
    }


    /**
     * 获取当前的事件类型
     *
     * @return
     */
    protected abstract String getCurrentEventType();

    /**
     * 事件处理
     */
    protected abstract boolean dealWithEvent();


    /**
     * 获取当前事件的优先级
     *
     * @return
     */
    public int getCurrentEventPriority() {

        return eventPriority;
    }

    /**
     * 更新事件涉及到的参数类型
     *
     * @param hashMap
     */
    public void updateEventParams(HashMap hashMap) {
        if (eventParams == null) {
            eventParams = new HashMap();
        }
        eventParams.putAll(hashMap);
    }


    public void updateEventParams(String key, Object value) {
        if (eventParams == null) {
            eventParams = new HashMap();
        }
        eventParams.put(key, value);
    }

    /**
     * 获取当前事件涉及到的参数
     *
     * @return
     */
    public HashMap getCurrentEventHashParams() {

        return eventParams;
    }


    @Override
    public String toString() {
        return "UserLoginEventType{" +
                "eventType='" + eventType + '\'' +
                ", eventPriority=" + eventPriority +
                ", eventDesc='" + eventDesc + '\'' +
                ", eventParams=" + eventParams +
                '}';
    }


    /**
     * 该事件需不需要被处理
     */
    public boolean isNeedDealWithEvent() {
        return eventParams != null;
    }

    public interface EventDealOwner {
        Context getCurrentContext();

        Activity getCurrentActivity();

        Handler getHandler();


    }
}
