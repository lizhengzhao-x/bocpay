package cn.swiftpass.wallet.intl.entity.event;


public class GradeReduceEntity extends BaseEventEntity {

    public static final int EVENT_GRADE_REDUCE_SUCCESS = 1001;

    public GradeReduceEntity(int eventType, String message) {
        super(eventType, message);
    }

}
