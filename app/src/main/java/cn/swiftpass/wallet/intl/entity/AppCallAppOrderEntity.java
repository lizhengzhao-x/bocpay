package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class AppCallAppOrderEntity extends BaseEntity {


    /**
     * 更新了订单状态的商户link
     */
    private String appLink;
    private String resultCode;
    /**
     * 报错的message
     */
    private String message;
    private String fromType;
    /**
     * 交易ID resultCode = 0 才会有
     *         如果不是0 代表失败 需要取appLink覆盖掉之前未支付的appLink更新订单状态
     *
     */
    private String txnId;

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public boolean isSuccess() {
        if (TextUtils.isEmpty(resultCode)) {
            return false;
        }
        return TextUtils.equals(resultCode, "0");
    }

    public boolean isAppCallAppFromH5() {
        if (TextUtils.isEmpty(fromType)){
            return false;
        }
        return TextUtils.equals(fromType,"H5");
    }
}
