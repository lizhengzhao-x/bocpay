package cn.swiftpass.wallet.intl.module.virtualcard.utils;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Pattern;


public class NormalInputFilter implements InputFilter {
    public static final int CHARSEQUENCE = 901;
    public static final int CHINESE = 902;
    public static final int CHARSEQUENCE_CHINESE = 903;
    public static final int CHARSEQUENCE_CHINESE_SPECIAL = 904;
    public static final int CHARSEQUENCE_SPECIAL = 905;
    public static final int CHINESE_SPECIAL = 906;
    public static final int CHARSEQUENCE_SPACE = 907;
    public static final int CHINESE_SPACE = 908;
    public static final int CHARSEQUENCE_CHINESE_SPACE = 909;
    public static final int CHARSEQUENCE_CHINESE_SPECIAL_SPACE = 910;
    public static final int CHARSEQUENCE_SPECIAL_SPACE = 911;
    public static final int CHINESE_SPECIAL_SPACE = 912;
    public static final int CHINESE_SPECIAL_BANK_NAME = 913;
    public static final int CONTINUATION_STR_OR_NUMBER = 914;
    public static final int BOTH_OF_CHAR_OR_SPECIAL = 915;
    public static final int CHARSEQUENCE_BANK_SPECIAL = 916;
    public static final int BOTH_OF_CHAR_OR_SPECIAL_PASS = 917;
    public static final int LESS_THAN_ONE_CHAR = 918;
    public static final int NUMBER = 920;
    private final String STR_CHAR_CAPITAL = "A-Z";
    private final String STR_NUMBER = "0-9";
    private final String STR_SPACE = " ";
    private final String STR_CHINESE = "\\u4E00-\\u9FFF";
    private final String STR_CHAR_LOWERCASE = "a-z";
    private final String STR_SPECIAL_BANK_SPECIAL = ".\\-_";
    private final String STR_SPECIAL = "`~!\\-_@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？";
    private boolean reverse = false;


    private Pattern mPattern = Pattern.compile("[A-Za-z0-9\\u4E00-\\u9FFF`~!@#$%^&*()\\-_+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]*");

    public NormalInputFilter(int type, boolean reverse) {
        this(type);
        this.reverse = reverse;
    }

    public Pattern getmPattern() {
        return mPattern;
    }

    public NormalInputFilter(int type) {
        switch (type) {
            case CHARSEQUENCE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + "]*");
                break;
            case NUMBER:
                mPattern = Pattern.compile("["  + STR_NUMBER + "]*");
                break;
            case CHINESE:
                mPattern = Pattern.compile("[" + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_CHINESE + "]*");
                break;
            case CHARSEQUENCE_CHINESE_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_CHINESE + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_SPECIAL + "]*");
                break;
            case CHINESE_SPECIAL:
                mPattern = Pattern.compile("[" + STR_SPECIAL + "]*");
                break;
            case CHARSEQUENCE_SPACE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_SPACE + "]*");
                break;
            case CHINESE_SPACE:
                mPattern = Pattern.compile("[" + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE_SPACE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_CHINESE + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_CHINESE_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_CHINESE + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHARSEQUENCE_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHINESE_SPECIAL_SPACE:
                mPattern = Pattern.compile("[" + STR_SPECIAL + STR_SPACE + "]*");
                break;
            case CHINESE_SPECIAL_BANK_NAME:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_SPACE + "]*");
                break;
            case CONTINUATION_STR_OR_NUMBER:
                mPattern = Pattern.compile("([" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_SPECIAL + "])\\1\\1");
                break;
            case BOTH_OF_CHAR_OR_SPECIAL:
//            "^(?![a-z0-9]+$)(?![A-Z0-9]+$)(?![.-_0-9]+$)[\da-zA-Z.-_]*$"
                mPattern = Pattern.compile("^(?![" + STR_NUMBER + STR_CHAR_LOWERCASE + "]+$)(?![" + STR_NUMBER + STR_CHAR_CAPITAL + "]" +
                        "+$)(?![" + STR_NUMBER + STR_SPECIAL_BANK_SPECIAL + "]+$)[" + STR_NUMBER + STR_CHAR_LOWERCASE + STR_CHAR_CAPITAL + STR_SPECIAL_BANK_SPECIAL + "]*$");
                break;
            case CHARSEQUENCE_BANK_SPECIAL:
                mPattern = Pattern.compile("[" + STR_CHAR_CAPITAL + STR_CHAR_LOWERCASE + STR_NUMBER + STR_SPECIAL_BANK_SPECIAL + "]*");
                break;
            case BOTH_OF_CHAR_OR_SPECIAL_PASS:
                mPattern = Pattern.compile("^(?![" + STR_NUMBER + STR_CHAR_LOWERCASE + "]+$)(?![" + STR_NUMBER + STR_CHAR_CAPITAL + "]" +
                        "+$)(?![" + STR_NUMBER + STR_SPECIAL + "]+$)[" + STR_NUMBER + STR_CHAR_LOWERCASE + STR_CHAR_CAPITAL + STR_SPECIAL + "]*$");
                break;
            case LESS_THAN_ONE_CHAR:
                //^(?![0-9]+$)(?![.-_0-9]+$)[\da-zA-Z.-_]*$
                mPattern = Pattern.compile("^(?![" + STR_NUMBER + "]+$)(?![" + STR_NUMBER + STR_SPECIAL_BANK_SPECIAL + "]+$)[" + STR_NUMBER + STR_CHAR_LOWERCASE + STR_CHAR_CAPITAL + STR_SPECIAL_BANK_SPECIAL + "]*$");
                break;


            default:
                break;

        }
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        String temp = dest.toString();
        temp = temp.substring(0, dstart) + source.subSequence(start, end) + temp.substring(dend);
        if (reverse) {
            if (!mPattern.matcher(temp).matches()) {
                return source;
            }
        } else {
            if (mPattern.matcher(temp).matches()) {
                return source;
            }
        }

        return "";
    }
}
