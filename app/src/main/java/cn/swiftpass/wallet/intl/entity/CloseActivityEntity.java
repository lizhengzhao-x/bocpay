package cn.swiftpass.wallet.intl.entity;


public class CloseActivityEntity {


    public CloseActivityEntity(int type) {
        this.eventType = type;
    }

    /*关闭密码框*/
    public static final int CLOSE_VERIFY_PAGE = 0;
    public static final int CLOSE_CLEAR_PAGE = 2;
    public static final int CLOSE_ORDER_PAGE = 4;
    public static final int CLOSE_SHOW_ERROR_MESSAGE = 6;
    public static final int SHOW_MESSAGE = 8;

    public CloseActivityEntity(int eventType, String message) {
        this.eventType = eventType;
        this.message = message;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    private int eventType;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
