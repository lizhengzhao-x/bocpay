package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 检查信用卡OTP返回对象
 */

public class VerifyOTPEntity extends BaseEntity {
    String unqKey;
    String srvrNm;
    String reqTS;
    String cDCRevTS;
    String cDCRpyTS;
    String respCode;
    String respMsg;
    String pwdVerifyState;
    String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public String getPwdVerifyState() {
        return pwdVerifyState;
    }

    public void setPwdVerifyState(String pwdVerifyState) {
        this.pwdVerifyState = pwdVerifyState;
    }

    public void setcDCRevTS(String cDCRevTS) {
        this.cDCRevTS = cDCRevTS;
    }

    public void setcDCRpyTS(String cDCRpyTS) {
        this.cDCRpyTS = cDCRpyTS;
    }

    public String getcDCRevTS() {
        return cDCRevTS;
    }

    public String getcDCRpyTS() {
        return cDCRpyTS;
    }

    public void setUnqKey(String unqKey) {
        this.unqKey = unqKey;
    }

    public void setSrvrNm(String srvrNm) {
        this.srvrNm = srvrNm;
    }

    public void setReqTS(String reqTS) {
        this.reqTS = reqTS;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public String getUnqKey() {
        return unqKey;
    }

    public String getSrvrNm() {
        return srvrNm;
    }

    public String getReqTS() {
        return reqTS;
    }


    public String getRespCode() {
        return respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }
}
