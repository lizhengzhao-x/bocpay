package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import cn.swiftpass.wallet.intl.R;
import io.card.payment.CardIOActivity;

/**
 * cardio 扫描身份证界面 添加back按键
 */
public class CardIOExtraHelper {

    private static final int FRAME_ID = 1;

    public static void addBackIconToCardIOActivity(Application application, String toolBarTitleStr, String cardNumberInputStr) {
        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                resetContentView(activity, toolBarTitleStr, cardNumberInputStr);
                activity.getApplication().unregisterActivityLifecycleCallbacks(this);

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    /**
     * @param activity CardIOActivity
     * @return void
     * @author shilei.mo
     * @description 重新设置CardIOActivity中的View
     * @date 2020/4/10 14:21
     */
    private static void resetContentView(Activity activity, String toolBarTitleStr, String cardNumberInputStr) {
        if (activity instanceof CardIOActivity) {
            CardIOActivity cardIOActivity = (CardIOActivity) activity;
            FrameLayout decorView = (FrameLayout) cardIOActivity.getWindow().getDecorView();
            View extraView = LayoutInflater.from(cardIOActivity).inflate(R.layout.extra_card_io, decorView, false);
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    if (context instanceof Activity) {
                        ((Activity) context).finish();
                    }
                }
            };
            TextView id_title = extraView.findViewById(R.id.id_title);
            id_title.setText(toolBarTitleStr);
//            int type = TempSaveHelper.getBankCardNumberType();
//            if (type == Constants.PAGE_FLOW_REGISTER) {
//                id_title.setText(R.string.IDV_2_1);
//            } else if (type == Constants.PAGE_FLOW_BIND_CARD) {
//                id_title.setText(R.string.bind_title);
//            } else if (type == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                id_title.setText(R.string.setting_payment_short);
//            }
            Button btn = extraView.findViewById(R.id.ExtraCardIO_mBtnBack);
//            btn.setText(R.string.title_input_number);
            btn.setText(cardNumberInputStr);
            btn.setOnClickListener(onClickListener);
            extraView.findViewById(R.id.ExtraCardIO_mIvBack).setOnClickListener(onClickListener);

            FrameLayout mainLayout = findMainLayout(decorView);
            if (mainLayout != null) {
                ((ViewGroup) mainLayout.getParent()).removeView(mainLayout);
                FrameLayout spaceLayout = extraView.findViewById(R.id.ExtraCardIO_mSpace);
                spaceLayout.setOutlineProvider(new ViewOutlineProvider() {
                    @Override
                    public void getOutline(View view, Outline outline) {
                        Rect rect = new Rect();
                        outline.setRoundRect(rect, ScreenUtils.dip2px(20f));
                        int radius = ScreenUtils.dip2px(20);

                        //进行边缘圆角切割时，因为底部2个角不需要切割圆角，所以让底部的切割区域高于整体的区域大小，实现底部不切割圆角
                        outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + radius, radius);
                    }
                });
                spaceLayout.setClipToOutline(true);
//                adjustPreViewWH(mainLayout);
                spaceLayout.addView(mainLayout);
            }
            String mainBackground = SpUtils.getInstance().getBaseBackground();
            if (!TextUtils.isEmpty(mainBackground)) {
                Uri parse = Uri.parse(mainBackground);
                GlideApp.with(activity)
                        .asBitmap()
                        .load(parse)
                        .into(new SimpleTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                decorView.setBackground(new BitmapDrawable(resource));
                            }
                        });

            } else {
                decorView.setBackgroundResource(R.mipmap.bg_base_act);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE);//API19
            } else {
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                );
            }
            decorView.addView(extraView);
        }
    }


    /**
     * @param view 闯入decorView为宜
     * @return android.widget.FrameLayout
     * @author shilei.mo
     * @description 查找CardIOActivity中的MainLayout
     * @date 2020/4/10 14:20
     */
    private static FrameLayout findMainLayout(View view) {
        View frameLayout = view.findViewById(FRAME_ID);
        if (frameLayout != null && frameLayout.getParent() instanceof FrameLayout) {
            return (FrameLayout) frameLayout.getParent();
        }

        return null;
    }

    /**
     * @param view 传入MainLayout为宜
     * @return android.view.View
     * @author shilei.mo
     * @description 查找PreView  因为PreView未开放  所以根据CardIOActivity源码直接取第0个View
     * @date 2020/4/10 13:39
     */
    private static View findSurfaceView(View view) {
        View frameLayout = view.findViewById(FRAME_ID);
        if (frameLayout != null && frameLayout.getParent() instanceof FrameLayout) {
            FrameLayout layout = (FrameLayout) frameLayout;
            return layout.getChildAt(0);
        }

        return null;
    }

    /**
     * @param mainLayout 传入MainLayout为宜
     * @return void
     * @author shilei.mo
     * @description 调整PreView布局  (requestLayout不生效 待解决)
     * @date 2020/4/10 14:35
     */
    private static void adjustPreViewWH(final View mainLayout) {
        final View view = findSurfaceView(mainLayout);

        mainLayout.post(new Runnable() {
            @Override
            public void run() {
                if (view != null) {
                    ReflectUtils.setFieldValue(view, "mPreviewWidth", view.getMeasuredWidth());
                    ReflectUtils.setFieldValue(view, "mPreviewHeight", view.getMeasuredHeight());
                    mainLayout.forceLayout();
                }

            }
        });
//
//        ReflectUtils.setFieldValue(view, "mPreviewWidth", 1080);
//        ReflectUtils.setFieldValue(view, "mPreviewHeight", 1783);

    }
}
