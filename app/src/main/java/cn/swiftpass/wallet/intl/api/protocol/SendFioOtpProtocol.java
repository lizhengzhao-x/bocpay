package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 发送信用卡OTP接口，返回对象为 SendOTPEntity
 */

public class SendFioOtpProtocol extends BaseProtocol {
    public static final String TAG = SendFioOtpProtocol.class.getSimpleName();


    public SendFioOtpProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/triggerOtpFIO";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, "FIO");
    }
}
