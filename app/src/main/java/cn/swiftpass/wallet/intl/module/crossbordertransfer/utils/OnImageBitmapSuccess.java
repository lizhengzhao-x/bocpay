package cn.swiftpass.wallet.intl.module.crossbordertransfer.utils;

import android.net.Uri;

interface OnImageBitmapSuccess {
    void onImageBitmapCallBack(Uri uri);
}
