package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.content.Context;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by  on 2018/03/17.
 */
public class GridSpacingEqualDecoration extends RecyclerView.ItemDecoration {

    //item的个数
    private int spanCount;
    //每个item的间隔
    private int itemSpacing;
    //最外层的边距
    private int edgeMargin;


    private boolean paramsSetting = false;

    public GridSpacingEqualDecoration(Context mContext, int spanCount, int itemSpacing, int edgeMargin) {
        this.spanCount = spanCount;
        this.itemSpacing = itemSpacing;
        this.edgeMargin = edgeMargin;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        int column = position % spanCount; // item column
        View child = parent.getChildAt(position);

        //默认recycleView无边距 等距排列item   recycleView边距通过margin padding来设置
        int totalSpace = itemSpacing * (spanCount - 1);
        int itemSpace = totalSpace / spanCount;
        int the = itemSpacing - itemSpace;
        outRect.left = column * the;
        outRect.right = itemSpace - column * the;
        outRect.top = itemSpacing;
    }
}