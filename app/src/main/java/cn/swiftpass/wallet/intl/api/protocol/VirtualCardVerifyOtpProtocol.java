package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;

/**
 * 虚拟卡绑定发送otp
 */
public class VirtualCardVerifyOtpProtocol extends BaseProtocol {
    VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiy;
    String verifyCode;
    String mCardId;
    String action;

    public VirtualCardVerifyOtpProtocol(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String actionIn, String verifyCodeIn, String cardId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        vitualCardSendOtpEntiy = vitualCardSendOtpEntiyIn;
        this.verifyCode = verifyCodeIn;
        this.mCardId = cardId;
        this.action = actionIn;
        if (action.equals(ApiConstant.VIRTUALCARD_BIND)) {
            mUrl = "api/virtual/bind/checkOtp";
        } else {
            mUrl = "api/virtual/query/checkOtp";
        }

    }

    @Override
    public void packData() {
        super.packData();
//        action	String	M	“VB”
//        virtualCardInd	String	M	“Y”
//        pan	String	M
//        expDate	String	M
//        eaiAction	String	M	“B”
//        verifyCode	String	M
//        cardId	String	M
//        otpAct	String 	M	V:check B:bind  “B”
//        type	String	M	L:list  D:detail  查列表L  查详情：D
        mBodyParams.put(RequestParams.ACTION, action);
        mBodyParams.put(RequestParams.CARDID, vitualCardSendOtpEntiy.getCardId());
        if (action.equals(ApiConstant.VIRTUALCARD_BIND)) {
            mBodyParams.put(RequestParams.FACEID, vitualCardSendOtpEntiy.getCrdArtId());
            //虚拟卡绑定
            mBodyParams.put(RequestParams.VIRTUALCARDIND, "Y");
            mBodyParams.put(RequestParams.PAN_ID, vitualCardSendOtpEntiy.getPan());
            mBodyParams.put(RequestParams.EXPDATE, vitualCardSendOtpEntiy.getExpDate());
            mBodyParams.put(RequestParams.EAIACTION, "B");
            mBodyParams.put(RequestParams.OTPACT, "B");
            mBodyParams.put(RequestParams.VERIFYCODE, verifyCode);
            mBodyParams.put(RequestParams.CARDID, mCardId);
            mBodyParams.put(RequestParams.TYPE, "L");
        } else {
            mBodyParams.put(RequestParams.VERIFYCODE, verifyCode);
        }
    }

}
