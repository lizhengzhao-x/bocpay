package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.SendTxnIdOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VerifyTxnIdOtpProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderOTPContract;

;


public class TransferCrossBorderOTPPresenter extends BasePresenter<TransferCrossBorderOTPContract.View> implements TransferCrossBorderOTPContract.Presenter {


    @Override
    public void sendOTP(String walletId, String phone, String action) {
        if (getView() != null)
            getView().showDialogNotCancel();
        new SendTxnIdOtpProtocol(walletId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendOTPFailed(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendOTPSuccess(response);
                }

            }
        }).execute();

    }

    @Override
    public void tryOTP(String txnId, String phone, String action) {
        if (getView() != null)
            getView().showDialogNotCancel();
        new SendTxnIdOtpProtocol(txnId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().tryOTPFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().tryOTPSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void verifyOTP(String txnId, String phone, String otp, String action) {
        if (getView() != null)
            getView().showDialogNotCancel();
        new VerifyTxnIdOtpProtocol(txnId, otp, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOTPFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOTPSuccess(response);
                }
            }
        }).execute();

    }


}
