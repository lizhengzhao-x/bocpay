package cn.swiftpass.wallet.intl.utils;


import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.dialog.BaseRedPacketOpenDialog;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.RedCommonPacketOpenDialog;
import cn.swiftpass.wallet.intl.dialog.RedStaffPacketOpenDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.TurnOnRedPackEtntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;

/**
 * 红包打开弹窗
 */
public class RedPacketDialogUtils {
    public static RedCommonPacketOpenDialog redCommonPacketOpenDialog;
    public static RedCommonPacketOpenDialog.Builder commonbuilder;


    private static RedStaffPacketOpenDialog redStaffPacketOpenDialog;
    private static RedStaffPacketOpenDialog.Builder staffbuilder;
    private static String openType;
    private static String srcRefNo;
    public static Activity mActivity;

    //需要跳转到拆利是页面 默认不需要
    private static boolean needJumpGetLishi = false;

    //保存推送红包消息Id
    public static String pushRrcRefNo;
    //STAFF表示员工利是，COMMON表示普通利是
    public static String pushRedPacketType;
    public static String ALL_SRCREFNO = "all_srcrefno";

    //红包重复拆 弹框
    private static CustomDialog redRedRePeatDailog;

    public static void openRedPacket(Activity mActivity, String srcRefNo, String openType) {
        openRedPacket(mActivity, srcRefNo, openType, false);
    }

    public static void openRedPacket(Activity mActivity, String srcRefNo, String openType, boolean needJumpGetLishi) {
        //前一个mActivity已经关闭 需要清空dialog
        if (null != RedPacketDialogUtils.mActivity && RedPacketDialogUtils.mActivity.isFinishing()) {
            redCommonPacketOpenDialog = null;
            commonbuilder = null;
            redStaffPacketOpenDialog = null;
            staffbuilder = null;
        }

        RedPacketDialogUtils.openType = openType;
        RedPacketDialogUtils.mActivity = mActivity;
        RedPacketDialogUtils.srcRefNo = srcRefNo;
        RedPacketDialogUtils.needJumpGetLishi = needJumpGetLishi;

        if (null == mActivity) {
            return;
        }
        boolean isShowDialog = false;
        //第一次打开红包 需要显示网络dialog  后面再次打开显示redCommonPacketOpenDialog本事的dialog
        if (null == commonbuilder) {
            isShowDialog = true;
        } else {
            commonbuilder.showMyDialog();
        }

        /**
         * 拆第一个列表的
         */
        ApiProtocolImplManager.getInstance().openRedPacket(mActivity, srcRefNo, new NetWorkCallbackListener<TurnOnRedPackEtntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (null != commonbuilder) {
                    commonbuilder.dimissMyDialog();
                }

                if (ErrorCode.RED_OPEN_REPEAT.code.equals(errorCode)) {
                    showRedRepeatDialog(errorMsg);
                } else {
                    AndroidUtils.showTipDialog(mActivity, errorMsg);
                }
            }

            @Override
            public void onSuccess(TurnOnRedPackEtntity response) {
                if (null != commonbuilder) {
                    commonbuilder.dimissMyDialog();
                }

                EventBus.getDefault().post(new TransferEventEntity(TransferEventEntity.EVENT_RED_PACKET_OPEN, srcRefNo));

                //这里从首页进入 需要打开红包成功后才进入拆利是页面
                if (needJumpGetLishi) {
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_RED_PACKET_GET_FRAGMENT, ""));
                }
                showCommonRedPacketOpenDialog(response);
            }
        }, isShowDialog);
    }


    private static void showRedRepeatDialog(String errroMessage) {
        if (redRedRePeatDailog != null && redRedRePeatDailog.isShowing()) {
            redRedRePeatDailog.dismiss();
            redRedRePeatDailog = null;
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);

        builder.setMessage(errroMessage);

        builder.setPositiveButton(mActivity.getString(R.string.dialog_right_btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                redRedRePeatDailog = null;
                closeRedCommonPacketOpenDialog();

                MyActivityManager.removeAllTaskWithMainActivity();
                //跳转到拆利是页面
                EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_RED_PACKET_GET_FRAGMENT, ""));
            }
        });

        if (mActivity != null && !mActivity.isFinishing()) {
            redRedRePeatDailog = builder.create();
            redRedRePeatDailog.setCancelable(false);
            redRedRePeatDailog.show();
        }
    }

    /**
     * 点击通知推送时，打开员工红包
     */
    public static void showStaffRedPacketOpenDialog(Activity activity, TurnOnRedPackEtntity.ReceivedRedPacketDetailBean receivedRedPacketDetailBean) {
        //前一个mActivity已经关闭 需要清空dialog
        if (null != RedPacketDialogUtils.mActivity && RedPacketDialogUtils.mActivity.isFinishing()) {
            redCommonPacketOpenDialog = null;
            commonbuilder = null;
            redStaffPacketOpenDialog = null;
            staffbuilder = null;
        }

        if (null == receivedRedPacketDetailBean) {
            return;
        }
        mActivity = activity;

        String gifType = Constants.STAFF_GIF;
        String mp3Type = Constants.STAFF_MP3;
        if (null != staffbuilder) {
            closeRedStaffPacketOpenDialog();
        }


        staffbuilder = new RedStaffPacketOpenDialog.
                Builder(MyActivityManager.getInstance().getCurrentActivity()).
                setCloseClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        closeRedStaffPacketOpenDialog();
                    }
                })
                .setGifType(gifType)
                .setGifUri(RedPacketResourceUtil.getResource(mActivity, gifType, receivedRedPacketDetailBean.getRedPacketImageUrl(), FileDownloadUtil.getLanguageType()))
                .setMp3Uri(RedPacketResourceUtil.getResource(mActivity, mp3Type, receivedRedPacketDetailBean.getRedPacketMusicUrl(), FileDownloadUtil.getLanguageType()))
                .setPositiveListener(new BaseRedPacketOpenDialog.ConFrimClick() {
                    @Override
                    public void onClick(TextView v, String nextId, String openType) {
                        closeRedStaffPacketOpenDialog();
                        MyActivityManager.removeAllTaskWithMainActivity();
                        EventBus.getDefault().post(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_LIST, srcRefNo));
                    }
                });

        redStaffPacketOpenDialog = staffbuilder.create();


        redStaffPacketOpenDialog.showDailog();


        redStaffPacketOpenDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //关闭了就置空 redPacketOpenDialog
                if (null != redStaffPacketOpenDialog && !redStaffPacketOpenDialog.isShowing()) {
                    MediaPlayerUtils.stopPlayMusic();
                    redStaffPacketOpenDialog = null;
                    staffbuilder = null;
                    RedPacketDialogUtils.mActivity = null;
                    RedPacketDialogUtils.openType = null;
                    RedPacketDialogUtils.srcRefNo = null;
                }
            }
        });
    }


    /**
     * 点击通知推送时，打开普通红包
     */
    private static void showCommonRedPacketOpenDialog(TurnOnRedPackEtntity response) {
        if (null == response) {
            return;
        }

        if (null == response.getReceivedRedPacketDetail()) {
            return;
        }
        TurnOnRedPackEtntity.ReceivedRedPacketDetailBean receivedRedPacketDetailBean = response.getReceivedRedPacketDetail();
        String releaName = receivedRedPacketDetailBean.getName();
        ContactCompareUtils.compareHisotryLocalRecord(mActivity, receivedRedPacketDetailBean.getPhone(), receivedRedPacketDetailBean.getEmail(), releaName, new ContactCompareUtils.CompareSingleResult() {
            @Override
            public void getStringName(String name) {
                openDialog(response, name);
            }
        });

    }

    public static void openDialog(TurnOnRedPackEtntity response, String releaName) {
        String gifType = Constants.COMON_GIF;
        String mp3Type = Constants.COMON_MP3;

        String title = AndroidUtils.getBigRedNameEllipStr(releaName) + " " + mActivity.getString(R.string.RP2101_24_2);
        //如果是打开全部
        if (TextUtils.equals(openType, Constants.RED_PACKET_ALL_TYPE)) {
            title = response.getTurnRedPacketCount() + "";
        }
        TurnOnRedPackEtntity.ReceivedRedPacketDetailBean receivedRedPacketDetailBean = response.getReceivedRedPacketDetail();

        String bless = receivedRedPacketDetailBean.getBlessingWords();

        String lan = SpUtils.getInstance(mActivity).getAppLanguage();
        String titleFlag = mActivity.getString(R.string.RP2101_24_4) + " " + AndroidUtils.getBigRedNameEllipStr(releaName)
                + mActivity.getString(R.string.RP2101_24_5) + mActivity.getString(R.string.RP2101_24_6);
        if (AndroidUtils.isEGLanguage(lan)) {
            //英文's需要添加空格
            titleFlag = mActivity.getString(R.string.RP2101_24_4) + " " + AndroidUtils.getBigRedNameEllipStr(releaName)
                    + mActivity.getString(R.string.RP2101_24_5) + " " + mActivity.getString(R.string.RP2101_24_6);
        }
        //没有祝福语，就不需要显示这句话
        if (TextUtils.isEmpty(bless)) {
            titleFlag = "";
        }

        String redOpenNum = "0";
        //如果是打开全部
        if (TextUtils.equals(openType, Constants.RED_PACKET_ALL_TYPE)) {
            redOpenNum = response.getTurnRedPacketCount() + "";
        }
        if (null == commonbuilder) {
            commonbuilder = new RedCommonPacketOpenDialog.
                    Builder(MyActivityManager.getInstance().getCurrentActivity())
                    .setTitle(title)
                    .setTitleFlag(titleFlag)
                    .setRedOpenNum(redOpenNum)
                    .setAmount(receivedRedPacketDetailBean.getSendAmt())
                    .setNextRedPacketRefNo(response.getNextRedPacketRefNo())
                    .setOpenType(RedPacketDialogUtils.openType)
                    .setBlessingWords(bless)
                    .setGifType(gifType)
                    .setTransferType(receivedRedPacketDetailBean.getTransferType())
                    .setGifUri(RedPacketResourceUtil.getResource(mActivity, gifType, receivedRedPacketDetailBean.getRedPacketImageUrl(), FileDownloadUtil.getLanguageType()))
                    .setMp3Uri(RedPacketResourceUtil.getResource(mActivity, mp3Type, receivedRedPacketDetailBean.getRedPacketMusicUrl(), FileDownloadUtil.getLanguageType()))
                    .setPositiveListener(new RedCommonPacketOpenDialog.ConFrimClick() {
                        @Override
                        public void onClick(TextView v, String nextId, String openType) {
                            if (null == mActivity) {
                                closeRedCommonPacketOpenDialog();
                                return;
                            }
                            String textStr = v.getText().toString();
                            if (TextUtils.equals(textStr, mActivity.getString(R.string.RP2101_25_1))) {
                                closeRedCommonPacketOpenDialog();
                            } else if (TextUtils.equals(textStr, mActivity.getString(R.string.RP2101_24_1))) {
                                openRedPacket(mActivity, nextId, openType);
                            } else {
                                closeRedCommonPacketOpenDialog();
                            }
                        }
                    })
                    .setCloseClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            closeRedCommonPacketOpenDialog();
                        }
                    });
            redCommonPacketOpenDialog = commonbuilder.create();
            redCommonPacketOpenDialog.showDailog();
        } else {
            commonbuilder.updateContain(title,
                    bless, receivedRedPacketDetailBean.getSendAmt()
                    , RedPacketResourceUtil.getResource(mActivity, gifType, receivedRedPacketDetailBean.getRedPacketImageUrl(), FileDownloadUtil.getLanguageType())
                    , RedPacketResourceUtil.getResource(mActivity, mp3Type, receivedRedPacketDetailBean.getRedPacketMusicUrl(), FileDownloadUtil.getLanguageType())
                    , response.getNextRedPacketRefNo(), RedPacketDialogUtils.openType, receivedRedPacketDetailBean.getTransferType(), gifType, titleFlag);
        }

        redCommonPacketOpenDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //关闭了就置空 redPacketOpenDialog
                if (null != redCommonPacketOpenDialog && !redCommonPacketOpenDialog.isShowing()) {
                    redCommonPacketOpenDialog = null;
                    commonbuilder = null;
                    RedPacketDialogUtils.mActivity = null;
                    RedPacketDialogUtils.openType = null;
                    RedPacketDialogUtils.srcRefNo = null;
                }
            }
        });
    }

    private static void closeRedStaffPacketOpenDialog() {
        if (null != redStaffPacketOpenDialog) {
            redStaffPacketOpenDialog.dismiss();
        }
    }

    private static void closeRedCommonPacketOpenDialog() {
        if (null != redCommonPacketOpenDialog) {
            redCommonPacketOpenDialog.dismiss();
        }
    }


}
