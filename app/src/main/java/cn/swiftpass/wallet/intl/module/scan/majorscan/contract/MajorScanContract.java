package cn.swiftpass.wallet.intl.module.scan.majorscan.contract;

import android.app.Activity;
import android.net.Uri;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.QrcodeTypeEntity;

/**
 * 主扫二维码
 */
public class MajorScanContract {

    public interface View extends IView {


        /**
         * 解析二维码失败
         *
         * @param errorCode
         * @param errorMsg
         */
        void parserUrlCodeError(String errorCode, String errorMsg);

        /**
         * 解析二维码成功
         *
         * @param entity
         */
        void parserUrlCodeSuccess(MpQrUrlInfoResult entity);

        /**
         * 转账二维码解析失败
         *
         * @param errorCode
         * @param errorMsg
         */
        void parseTransferCodeError(String errorCode, String errorMsg);

        void parseTransferCodeSuccess(ParserQrcodeEntity parserQrcodeEntity);

        void dealWithEmvCodeSuccess(MpQrUrlInfoResult mpQrUrlInfoResult);

        void dealWithEmvCodeError();


        void getActionTrxGpInfoError(String errorCode, String errorMsg);

        /**
         * 查新积分信息
         *
         * @param parserQrcodeEntity
         */
        void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity parserQrcodeEntity);

        /**
         * 获取银行卡列表
         *
         * @param bankCards
         */
        void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards, final boolean isUrlMode);

        void getPaymentTypeListError(String errorCode, String errorMsg);

        void getPayMentResultSuccess(PaymentEnquiryResult response);

        void getPayMentResultError(String errorCode, String errorMsg);

        /**
         * 主扫成功
         *
         * @param response
         */
        void getMainScanOrderSuccess(ContentEntity response);

        /**
         * 主扫下单报错
         *
         * @param errorCode
         * @param errorMsg
         */
        void getMainScanOrderError(String errorCode, String errorMsg);

        /**
         * 本地二维码解析成功
         *
         * @param content
         */
        void parserLocalQrcodeSuccess(String content);

        void scanQrCodeTypeSuccess(QrcodeTypeEntity qrcodeTypeEntity);

        void scanQrCodeTypeFailed(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<MajorScanContract.View> {

        /**
         * url码 需要解析二维码信息
         *
         * @param content
         */
        void getUpMerchantURLQRInfo(String content);

        /**
         * emv二维码的解析
         *
         * @param content
         */
        void dealWithEmvCode(String content);

        /**
         * 解析转账的二维码
         *
         * @param content
         */
        void parseTransferCode(String content, boolean showDialog);


        /**
         * 主扫是转账二维码 检测是否是智能账户
         */
        void checkSmartAccountNewBind();

        /**
         * 查询二维码积分信息
         *
         * @param mpQrUrlInfoResult
         */
        void getActionTrxGpInfo(MpQrUrlInfoResult mpQrUrlInfoResult);

        /**
         * 拉取当前二维码支付的卡列表
         *
         * @param qrcode
         */
        void getPaymentTypeList(String qrcode, final boolean isUrlMode);


        /**
         * 主扫下单轮训接口
         *
         * @param cardId
         * @param cpQrCode
         * @param txnId
         */
        void getPayMentResult(String cardId, String cpQrCode, String txnId);

        /**
         * 主扫下单
         *
         * @param orderRequest
         */
        void getMainScanOrder(MainScanOrderRequestEntity orderRequest);

        /**
         * 解析本地的二维码
         *
         * @param content
         */
        void parserLocalQrcode(Activity context, Uri content);

        /**
         * 二维码解析
         *
         * @param mpQrCode
         * @param cardId
         */
        void scanQrCodeType(String mpQrCode);

    }

}
