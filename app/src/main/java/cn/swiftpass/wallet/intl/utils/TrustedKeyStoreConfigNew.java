package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.content.res.AssetManager;

import com.bochklaunchflow.base.AppSecuityConfig;
import com.bochklaunchflow.utils.BOCLFLogUtil;
import com.bochklaunchflow.utils.BOCLFUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStore.TrustedCertificateEntry;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class TrustedKeyStoreConfigNew {
    private static final String TAG = TrustedKeyStoreConfigNew.class.getSimpleName();

    private static KeyStore trustedKeyStore = null;

    private static Set<String> localCertCNSet = null;

    private static HashMap<String, HashMap<String, Certificate>> localCerts = null;

    public static void setup(Context mContext) {
        trustedKeyStore = getKeyStoreFromLocalCerts(mContext);
    }

    private static KeyStore getKeyStoreFromLocalCerts(Context mContext) {
//		if (BOCHKLFConfig.isSitEnv()||BOCHKLFConfig.isUatEnv() ||BOCHKLFConfig.isProdEnv()) {
        if (mContext != null) {
            // generate keystore at runtime
            KeyStore trustedKeyStore = null;
            try {
                BOCLFLogUtil.i("CertificateScan", "Reminder: please clear the project after adding any cert in cert folder");
                final AssetManager assetManager = mContext.getResources().getAssets();
                final String CERT_PATH = "certs";
                final String[] certNameList = assetManager.list(CERT_PATH);
                trustedKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
                // To create an empty keystore using the load method,
                // pass null as the InputStream argument
                // * as requested, no password is needed
                trustedKeyStore.load(null, null);
                if (certNameList != null && certNameList.length > 0) {
                    InputStream in = null;
                    TrustedCertificateEntry trustedCertEntry = null;
                    String certAlias = null;
                    localCertCNSet = new HashSet<String>();
                    localCerts = new HashMap<>();
                    BOCLFLogUtil.i(TAG, "=======================start scan cert=======================");
                    try {
                        for (int i = 0; i < certNameList.length; i++) {
                            in = assetManager.open(CERT_PATH + "/" + certNameList[i]);
                            try {
                                boolean isCertValidToBeAdded = true;
                                X509Certificate certificate = null;
                                String cn = "";
                                try {
                                    certificate = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(in);
                                    final String fullSN = certificate.getSubjectDN().getName();
                                    cn = fullSN.substring(fullSN.indexOf("CN=") + 3, fullSN.indexOf(",", fullSN.indexOf("CN=") + 3));
                                    localCertCNSet.add(cn);
                                    // 19 Feb 2016: ignore checking the validity of Client certs
                                    //			    		    		certificate.checkValidity();
                                    //			    		    	} catch (CertificateNotYetValidException e) {
                                    //			    		    		isCertValidToBeAdded = false;
                                    //			    		    		Log.e(TAG, "certificate:"+certNameList[i]+" has not yet valid");
                                    //			    		    	} catch (CertificateExpiredException e) {
                                    //			    		    		isCertValidToBeAdded = false;
                                    //			    		    		Log.e(TAG, "certificate:"+certNameList[i]+" is expired");
                                } catch (CertificateException e) {
                                    isCertValidToBeAdded = false;
                                    BOCLFLogUtil.e(TAG, "certificate:" + certNameList[i] + " cannot parse into X509 certificate");
                                }
                                if (certificate != null && isCertValidToBeAdded) {
                                    trustedCertEntry = new TrustedCertificateEntry(certificate);
                                    certAlias = certNameList[i].substring(0, certNameList[i].indexOf("."));
                                    HashMap<String, Certificate> certs = new HashMap<>();
                                    certs.put(certAlias, certificate);
                                    localCerts.put(cn, certs);
                                    trustedKeyStore.setEntry(certAlias, trustedCertEntry, null);
                                    BOCLFLogUtil.i(TAG, "added CERT:[" + certNameList[i] + "] with ALIAS:[" + certAlias + "] CN:[" + cn + "] successfully");
                                }
                            } catch (NullPointerException e) {
                                BOCLFLogUtil.e(TAG, "certificate:" + certNameList[i] + " is null");
                            }
                        }
                    } catch (Exception e) {
                        if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
                    } finally {
                        BOCLFUtils.safeClose(in);
                    }
                    BOCLFLogUtil.i(TAG, "=======================end scan cert=======================");
                    return trustedKeyStore;
                } else {
                    BOCLFLogUtil.e(TAG, "certNameList is null or empty");
                }
            } catch (KeyStoreException e) {
                if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
            } catch (FileNotFoundException e) {
                if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
            } catch (IOException e) {
                if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
            } catch (CertificateException e) {
                if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
            }
            BOCLFLogUtil.e(TAG, "got exception; use the default KeyStore");
            localCertCNSet = new HashSet<String>();
            return getDefaultKeyStore();
        } else {
            BOCLFLogUtil.e(TAG, "Context of " + TAG + " is null; use the default KeyStore");
            localCertCNSet = new HashSet<String>();
            return getDefaultKeyStore();
        }
//		} else {
//			BOCLFLogUtil.e(TAG, "is not UAT | PROD; use the default KeyStore");
//			localCertCNSet = new HashSet<String>();
//			return getDefaultKeyStore();
//		}
    }

    public final static Set<String> getCNFromLocalCerts() {
        return localCertCNSet;
    }

    public final static boolean insert(Context mContext, Map<String, X509Certificate> certNameToCertMap) {
        if (certNameToCertMap == null || certNameToCertMap.size() == 0) {
            BOCLFLogUtil.w(TAG, "===[insert: certNameToList is empty; the original TrustedKeyStore remains unchanged]===");
            return true;
        } else {
            try {
                // copy the current keystore into a temp keystore
                KeyStore tempTrustedKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
                tempTrustedKeyStore.load(null, null);
                Enumeration<String> enumeration = trustedKeyStore.aliases();
                while (enumeration.hasMoreElements()) {
                    String alias = enumeration.nextElement();
                    X509Certificate certificate = (X509Certificate) trustedKeyStore.getCertificate(alias);
                    TrustedCertificateEntry trustedCertEntry = new TrustedCertificateEntry(certificate);
                    tempTrustedKeyStore.setEntry(alias, trustedCertEntry, null);

                    final String fullSN = certificate.getSubjectDN().getName();
                    String cn = fullSN.substring(fullSN.indexOf("CN=") + 3, fullSN.indexOf(",", fullSN.indexOf("CN=") + 3));
                    BOCLFLogUtil.i(TAG, "insert: copying cert with ALIAS:[" + alias + "] CN:[" + cn + "] successfully");
                }
                BOCLFLogUtil.i(TAG, "insert: copying cert end -------------------------------------");
                //--/ copy the current keystore into a temp keystore

                for (String certName : certNameToCertMap.keySet()) {
                    if (certName != null && certNameToCertMap.get(certName) != null) {
                        TrustedCertificateEntry trustedCertEntry = new TrustedCertificateEntry(certNameToCertMap.get(certName));
                        tempTrustedKeyStore.setEntry(certName, trustedCertEntry, null);

                        String cn = "";
                        try {
                            final String fullSN = certNameToCertMap.get(certName).getSubjectDN().getName();
                            cn = fullSN.substring(fullSN.indexOf("CN=") + 3, fullSN.indexOf(",", fullSN.indexOf("CN=") + 3));
                        } catch (Exception e) {
                            BOCLFLogUtil.w(TAG, "insert: cannot get cert with ALIAS:[" + certName + "]'s CN name");
                        }

                        BOCLFLogUtil.i(TAG, "inserted cert with ALIAS:[" + certName + "] CN:[" + cn + "] successfully");
                    }
                }

                trustedKeyStore = tempTrustedKeyStore;
                BOCLFLogUtil.i(TAG, "===[inserted " + certNameToCertMap.size() + " certs into TrustedKeyStore successfully]===");
                BOCLFLogUtil.i(TAG, "===[They are " + trustedKeyStore.size() + " certs in total at present.]===");
                return true;
            } catch (KeyStoreException e) {
                BOCLFLogUtil.e(TAG, "insert process terminated; ; #1 got exception: " + e.getMessage());
            } catch (NoSuchAlgorithmException e) {
                BOCLFLogUtil.e(TAG, "insert process terminated; ; #2 got exception: " + e.getMessage());
            } catch (CertificateException e) {
                BOCLFLogUtil.e(TAG, "insert process terminated; ; #3 got exception: " + e.getMessage());
            } catch (IOException e) {
                BOCLFLogUtil.e(TAG, "insert process terminated; ; #4 got exception: " + e.getMessage());
            }
        }
        return false;
    }

    private final static KeyStore getDefaultKeyStore() {
        try {
            KeyStore trustedKeyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustedKeyStore.load(null, null);
            return trustedKeyStore;
        } catch (KeyStoreException e) {
            if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
        } catch (CertificateException e) {
            if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
        } catch (IOException e) {
            if (AppSecuityConfig.SHOW_PRINT_STACK) e.printStackTrace();
        }
        BOCLFLogUtil.e(TAG, "Cannot generate default KeyStore");
        return null;
    }

    public final static KeyStore getTrustedKeyStore() {
        return trustedKeyStore == null ? getDefaultKeyStore() : trustedKeyStore;
    }

    public static HashMap<String, HashMap<String, Certificate>> getLocalCerts() {
        if (localCerts != null) {
            return localCerts;
        } else {
            return null;
        }
    }
}
