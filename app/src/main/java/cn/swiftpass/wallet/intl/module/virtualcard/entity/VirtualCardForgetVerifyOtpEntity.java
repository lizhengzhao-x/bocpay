package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/22.
 */
public class VirtualCardForgetVerifyOtpEntity extends BaseEntity {

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    private String sId;
}
