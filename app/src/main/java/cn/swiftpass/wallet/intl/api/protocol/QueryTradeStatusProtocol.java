package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 查询交易状态，返回对象为 TradeStatusEntity
 */

public class QueryTradeStatusProtocol extends BaseProtocol {
    public static final String TAG = QueryTradeStatusProtocol.class.getSimpleName();
    /**
     * 交易银行卡id，只能是数字，长度不能超过32位
     */
    String mCardId;
    /**
     * 二维码信息
     */
    String mCpQrCode;
    /**
     * 交易单号
     */
    String mTxnId;

    String action;

    public QueryTradeStatusProtocol(String cardId, String cpQrCode, String txnId, NetWorkCallbackListener dataCallback) {
        this.mCardId = cardId;
        this.mCpQrCode = cpQrCode;
        this.mTxnId = txnId;
        this.mDataCallback = dataCallback;
        //mUrl = "api/transaction/paymentResultEnquery";
        mUrl = "api/transaction/paymentResultEnquery1";
    }

    public QueryTradeStatusProtocol(String txnId, String actionIn, NetWorkCallbackListener dataCallback) {
        this.action = actionIn;
        this.mTxnId = txnId;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/paymentResultEnquery1";
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(mCardId)) {
            mBodyParams.put(RequestParams.CARD_ID, mCardId);
        }
        if (!TextUtils.isEmpty(mCpQrCode)) {
            mBodyParams.put(RequestParams.CP_QRCODE, mCpQrCode);
        }
        if (!TextUtils.isEmpty(mTxnId)) {
            mBodyParams.put(RequestParams.TXN_ID, mTxnId);
        }
        if (!TextUtils.isEmpty(action)) {
            mBodyParams.put(RequestParams.ACTION, action);
        }
    }
}
