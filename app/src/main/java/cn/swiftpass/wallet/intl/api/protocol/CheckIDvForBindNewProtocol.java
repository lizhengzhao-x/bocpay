package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class CheckIDvForBindNewProtocol extends BaseProtocol {

    public CheckIDvForBindNewProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        //不同流程 后台区分 ur不同
        mUrl = "api/smartBind/checkIdvRetry";

    }

}
