package cn.swiftpass.wallet.intl.manage;


import cn.swiftpass.wallet.intl.BuildConfig;

public class AppTestManager {

    private static AppTestManager sInstance;
    private static final Object INSTANCE_LOCK = new Object();

    private String baseUrl = BuildConfig.BASE_URL;

    private boolean e2ee = BuildConfig.E2EE;

    private boolean skipIdvFrp = BuildConfig.SKIP_IDV;

    private AppTestManager() {

    }

    public static AppTestManager getInstance() {
        if (sInstance == null) {
            synchronized (INSTANCE_LOCK) {
                if (sInstance == null) {
                    sInstance = new AppTestManager();
                }
            }
        }
        return sInstance;
    }

    public String getBaseUrl() {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            return baseUrl;
        } else {
            return BuildConfig.BASE_URL;
        }
    }

    public void setBaseUrl(String baseUrl) {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            this.baseUrl = baseUrl;
        }

    }

    public boolean isE2ee() {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            return e2ee;
        } else {
            return BuildConfig.E2EE;
        }
    }

    public void setE2ee(boolean e2ee) {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            this.e2ee = e2ee;
        }
    }

    public boolean isSkipIdvFrp() {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            return skipIdvFrp;
        } else {
            return BuildConfig.SKIP_IDV;
        }
    }

    public void setSkipIdvFrp(boolean skipIdvFrp) {
        if (!BuildConfig.FLAVOR.equals("prd")) {
            this.skipIdvFrp = skipIdvFrp;
        }
    }
}
