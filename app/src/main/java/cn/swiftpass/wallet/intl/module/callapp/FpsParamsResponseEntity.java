package cn.swiftpass.wallet.intl.module.callapp;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class FpsParamsResponseEntity extends BaseEntity {


    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    private String payload;


}
