package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.AreaListEntity;


public class GetCustomTelRegionProtocol extends BaseProtocol {
    public static final String TAG = GetCustomTelRegionProtocol.class.getSimpleName();
    String version;

    public GetCustomTelRegionProtocol(String version, NetWorkCallbackListener<AreaListEntity> dataCallback) {
        mUrl = "api/telephoneCode/telephoneCodeList";
        this.version = version;
        this.mDataCallback = dataCallback;
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERSION, version);
    }


}
