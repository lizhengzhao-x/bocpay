package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


/**
 * Created by admin on 2017/12/19.
 *
 * @Package cn.swiftpass.wallet.intl.widget
 * @Description:(可以自定义默认页面的default_view)
 * @date 2017/12/19.20:47.
 */

public class CustomView extends LinearLayout {

    private static final String TAG = "Custom_View";

    private ImageView defaultImageView;
    private TextView defaultText;
    private Button defaultButton;

    private ButtonCallBack mButtonListener;

    public CustomView(Context context) {
        super(context);
        init(context);
    }

    public CustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public CustomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public void init(Context context) {
        Log.i(TAG, "construct");

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.act_default_view, this);

        defaultImageView = findViewById(R.id.MyImageView);

        defaultText = findViewById(R.id.MyTextView);

        defaultButton = findViewById(R.id.MyButton);

        defaultButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view.getId() == R.id.MyButton) {
                    if (mButtonListener == null) {

                        Log.i(TAG, "mButtonListener == null");
                    } else {

                        Log.i(TAG, "mButtonListener.onClickButtonCallBack()");
                        mButtonListener.onClickButtonCallBack();
                    }
                }
            }
        });
    }

    /**
     * 定义接口用来响应button的监听
     */
    public interface ButtonCallBack {

        void onClickButtonCallBack();
    }

    public void setCurrentListener(ButtonCallBack listener) {

        mButtonListener = listener;
    }


    /**
     * 设置图片资源
     */
    public ImageView setImageResource(int resId) {
        defaultImageView.setImageResource(resId);
        return defaultImageView;
    }

    /**
     * 设置显示的文字
     */
    public void setTextViewText(int resid) {
        defaultText.setText(resid);
    }

    public void setTextViewText(CharSequence text) {
        defaultText.setText(text);
    }

    /**
     * 设置button显示的文字
     */
    public void setButtonText(int resid) {

        defaultButton.setText(resid);
    }

    /**
     * 设置button显示的文字
     */
    public void setButtonBackground(int resid) {

        defaultButton.setBackgroundColor(resid);
    }

    /**
     * 设置ImageView 显示不显示
     */
    public void setImageVisible(boolean visible) {

        if (visible == true) {
            defaultImageView.setVisibility(VISIBLE);
        } else if (visible == false) {
            defaultImageView.setVisibility(INVISIBLE);
        }
    }

    /**
     * 设置TextView 显示不显示
     */
    public void setTextViewVisible(boolean visible) {

        if (visible == true) {
            defaultText.setVisibility(VISIBLE);
        } else if (visible == false) {
            defaultText.setVisibility(INVISIBLE);
        }
    }

    /**
     * 设置Button 显示不显示
     */
    public void setButtonVisible(boolean visible) {

        if (visible == true) {
            defaultButton.setVisibility(VISIBLE);
        } else if (visible == false) {
            defaultButton.setVisibility(INVISIBLE);
        }
    }
}
