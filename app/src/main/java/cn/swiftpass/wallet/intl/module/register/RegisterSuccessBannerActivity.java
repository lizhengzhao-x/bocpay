package cn.swiftpass.wallet.intl.module.register;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.RelativeLayout;

import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdatePopUpControlProtocol;
import cn.swiftpass.wallet.intl.module.activitys.NewMsgActivity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.widget.ProgressWebView;

public class RegisterSuccessBannerActivity extends BaseWebViewActivity {

    @BindView(R.id.webView)
    ProgressWebView webView;
    @BindView(R.id.id_relative_layout_register_success_banner)
    RelativeLayout relativeLayout;

    public static final String EXTRA_URL = "EXTRA_URL";

    /**
     * CallNewMsgUI  迎新banner
     * CallNewUserPopUpUplanBannerUI  銀聯Uplan優惠券場景 – Welcome Package
     */
    public static final String METHOD_NEW_MSG_PARAM = "CallNewMsgUI";
    public static final String METHOD_U_PLAN_PARAM = "CallNewUserPopUpUplanBannerUI";

    @Override
    public void init() {
        if (getIntent() == null) {
            finish();
            return;
        }
        initApi();
        initView();
        String testUtl = getIntent().getExtras().getString(EXTRA_URL);
        initWebView(webView, false, testUtl);
        supportZoom(webView, false);
        //启用javaScript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new NewMsgJsInterface(), METHOD_NEW_MSG_PARAM);
        webView.addJavascriptInterface(new UPlanJsInterface(), METHOD_U_PLAN_PARAM);
    }

    private void initApi() {
        new UpdatePopUpControlProtocol(new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(Object response) {

            }
        }).execute();
    }

    private void initView() {
        //右侧x号按钮
        findViewById(R.id.id_img_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //设置高宽比
        float screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        double width = screenWidth * 0.8;
        double height = 485.0 / 300.0 * width;
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) relativeLayout.getLayoutParams();
        params.width = (int) width;
        params.height = (int) height;
        relativeLayout.setLayoutParams(params);
    }


    /**
     * 迎新banner
     */
    class NewMsgJsInterface {
        @JavascriptInterface
        public void postMessage(String msg) {
            startActivity(new Intent(RegisterSuccessBannerActivity.this, NewMsgActivity.class));
            finish();
        }
    }

    /**
     * 銀聯Uplan優惠券場景 – Welcome Package
     */
    class UPlanJsInterface {
        @JavascriptInterface
        public void postMessage(String msg) {
            goToUPlanMyDiscount();
        }
    }

    /**
     * 跳转至Uplan
     */
    private void goToUPlanMyDiscount() {
        showDialogNotCancel();
        new GetUplanurlProtocol("{\"action\":\"WELCOME_UPLAN_POP_UP\"}", new NetWorkCallbackListener<UplanUrlEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        finish();
                    }
                });
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                dismissDialog();
                if (response != null) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
                    mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
                    mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
                    mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
                    ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
                finish();
            }
        }).execute();
    }

    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_register_success_banner;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    /**
     * 启动 Activity
     *
     * @param fromActivity
     */
    public static void startActivity(Activity fromActivity, String url) {
        if (fromActivity == null) return;
        Intent intent = new Intent(fromActivity, RegisterSuccessBannerActivity.class);
        intent.putExtra(EXTRA_URL, url);
        fromActivity.startActivity(intent);
        fromActivity.overridePendingTransition(0, 0);
    }


    /**
     * 设置结束动画
     */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }


    /**
     * 不使用底层 Toolbar 及 背景图
     *
     * @return
     */
    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    /**
     * 吸收 back 事件
     */
    @Override
    public void onBackPressed() {

    }
}