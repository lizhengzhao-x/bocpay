package cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter;


import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@IntDef({
        ItemType.HEAD,
        ItemType.BODY,
        ItemType.TAIL,
        ItemType.BARCODE,
        ItemType.MONEY,
        ItemType.IMAGE,
        ItemType.SPLITLINE,
})
@Retention(RetentionPolicy.SOURCE)
public @interface ItemType {
    int HEAD = 0;
    int BODY = 1;
    int TAIL = 2;
    int BARCODE = 3;
    int MONEY = 4;
    int IMAGE = 5;
    int SPLITLINE = 6;
}
