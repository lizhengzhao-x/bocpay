package cn.swiftpass.wallet.intl.module.scan.backscan;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.utils.CreateOneDiCodeUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class ScaleBarCodeActivity extends BaseCompatActivity {
    @BindView(R.id.tv_code)
    TextView tvCode;
    @BindView(R.id.iv_code)
    ImageView ivCode;
    private String orderNoMch;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        AndroidUtils.setStatusBar(this, false);
        orderNoMch = getIntent().getStringExtra(Constants.DATA_ORDER_NUMBER);
        //设置屏幕亮度最大
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
        initView();
    }




    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_landsapce_barcode;
    }

    private void initView() {
        WindowManager wm = this.getWindowManager();
        int width = wm.getDefaultDisplay().getWidth();
        int w = (int) (width * 0.35);
        int height = wm.getDefaultDisplay().getHeight();
        int h = (int) (height * 0.75);
        Bitmap tempBitmap = CreateOneDiCodeUtil.createCode(orderNoMch, h, w);
        Bitmap codeBitmap = CreateOneDiCodeUtil.adjustPhotoRotation(tempBitmap, 90);
        if (!tempBitmap.isRecycled()) {
            tempBitmap.recycle();
        }
        ivCode.setImageBitmap(codeBitmap);
        tvCode.setText(orderNoMch);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) tvCode.getLayoutParams();
                lp.width = (int) (AndroidUtils.getScreenWidth(mContext) * 0.8);
                lp.height = AndroidUtils.dip2px(mContext, 310);
                tvCode.setLayoutParams(lp);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        AndroidUtils.setStatusBar(this, true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

}
