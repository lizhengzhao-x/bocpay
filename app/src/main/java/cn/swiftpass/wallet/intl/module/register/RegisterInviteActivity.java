package cn.swiftpass.wallet.intl.module.register;


import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InviteStatusEntity;
import cn.swiftpass.wallet.intl.entity.RegisterInviteInfo;
import cn.swiftpass.wallet.intl.entity.RegisterInviteStatusEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.VerifyEditView;

/**
 * 注册邀请
 */
public class RegisterInviteActivity extends BaseCompatActivity {


    private TextView mId_invite_share_title;
    private TextView mId_invite_share_sub_title;
    private TextView mId_invite_share_error_msg;
    private TextView mId_invite_share_rule;
    private cn.swiftpass.wallet.intl.widget.VerifyEditView mId_verifyEdit;
    private TextView mId_tv_skip;
    private TextView mId_tv_send;
    private View id_margin_view;
    //标识输入邀请码是从注册流程 还是在设置流程
    public static int INVITE_TYPE_REGISTER = 100;
    public static int INVITE_TYPE_SETTING = 200;
    private int eventType;
    private long currentPageCreateTime;
    private RegisterInviteInfo registerInviteInfo;


    private void bindViews() {
        currentPageCreateTime = System.currentTimeMillis();
        mId_invite_share_title = (TextView) findViewById(R.id.id_invite_share_title);
        mId_invite_share_sub_title = (TextView) findViewById(R.id.id_invite_share_sub_title);
        mId_invite_share_rule = (TextView) findViewById(R.id.id_invite_share_rule_btn);
        mId_verifyEdit = (cn.swiftpass.wallet.intl.widget.VerifyEditView) findViewById(R.id.id_verifyEdit);
        mId_tv_skip = (TextView) findViewById(R.id.id_tv_skip);
        mId_tv_send = (TextView) findViewById(R.id.id_tv_send);
        id_margin_view = findViewById(R.id.id_margin_view);
        mId_invite_share_error_msg = (TextView) findViewById(R.id.id_invite_share_error_msg);
        mId_tv_send.setEnabled(false);
        mId_verifyEdit.setOnInputListener(new VerifyEditView.OnInputListener() {
            @Override
            public void onInputComplete(String content) {
                mId_tv_send.setBackgroundResource(R.drawable.bg_btn_next_page);
                mId_tv_send.setEnabled(true);
                AndroidUtils.hideKeyboard(mId_verifyEdit);
            }

            @Override
            public void onInputChange(String content) {
                if (content.length() == 8) {
                    mId_tv_send.setEnabled(true);
                    mId_tv_send.setBackgroundResource(R.drawable.bg_btn_next_page);
                } else {
                    mId_invite_share_error_msg.setText("");
                    mId_tv_send.setEnabled(false);
                    mId_tv_send.setBackgroundResource(R.drawable.bg_btn_send);
                }
            }

        });

        mId_tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                if (CacheManagerInstance.getInstance().isLogin()) {
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_REGISTRATION_CNF, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_REFER_FRIENDS_PAGE, PagerConstant.ADDRESS_PAGE_FRONT_REGISTRATION_PAGE, System.currentTimeMillis() - currentPageCreateTime);
                    sendAnalysisEvent(analysisButtonEntity);
                }
                addTransferInviteRecord();
            }
        });
        mId_tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                skipInputInviteCodePage();
            }
        });

        //服务条款 更换词条的时候 需要添加% 为了适配文字
        String totalStr = getResources().getString(R.string.KBCC2105_7_4);
        String[] arrays = totalStr.split("##");
        final int startIndex = arrays[0].length();
        final int endIndex = arrays[0].length() + arrays[1].length();
        final String title = totalStr.replace("##", "");
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ForegroundColorSpan(Color.parseColor("#136EF1")), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                if (registerInviteInfo != null) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.DETAIL_URL, registerInviteInfo.getActivityTncUrl());
                    mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                    ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        mId_invite_share_rule.setText(spannableString);
        mId_invite_share_rule.setMovementMethod(LinkMovementMethod.getInstance());

    }

    /**
     * 跳过输入邀请码页面 直接到达首页
     */
    private void skipInputInviteCodePage() {
        if (CacheManagerInstance.getInstance().isLogin()) {
            MyActivityManager.removeAllTaskExcludeMainStack();
            Intent intent = new Intent(RegisterInviteActivity.this, MainHomeActivity.class);
            intent.putExtra(Constants.ISREGESTER, true);
//            boolean isNeedJumpNotification = false;
//            if (getIntent() != null) {
//                isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//                intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//            }
//            intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
            startActivity(intent);
            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
        }
    }

    /**
     * 邀请成功之后 页面关闭
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(InviteStatusEntity event) {
        finish();
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        bindViews();
        if (null == getIntent() || null == getIntent().getExtras()) {
            finish();
            return;
        }
        eventType = getIntent().getIntExtra(Constants.INVITE_TYPE, 0);
        if (eventType == INVITE_TYPE_REGISTER) {
            //注册流程 隐藏back按键
            hideBackIcon();
            setToolBarTitle(R.string.IDV_2_1);
            if (getIntent().getBooleanExtra(Constants.BIND_CARD_TYPE, false)) {
                //一键绑定流程过来的，设置toolBar为“绑定新卡”
                setToolBarTitle(R.string.KBCC2105_2_1);
            }
        } else {
            setToolBarTitle(R.string.MGM1_2_1);
            id_margin_view.setVisibility(View.GONE);
            mId_tv_skip.setVisibility(View.GONE);
        }

        //显示软键盘
        if (mId_verifyEdit.getFirstEdit() != null) {
            AndroidUtils.showKeyboard(this, mId_verifyEdit.getFirstEdit());
        }
        getTransferInviteInfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getTransferInviteInfo() {
        ApiProtocolImplManager.getInstance().getTransferInviteInfo(this, new NetWorkCallbackListener<RegisterInviteInfo>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                mId_invite_share_error_msg.setText(errorMsg);
            }

            @Override
            public void onSuccess(RegisterInviteInfo response) {
                updateTitleStr(response);

            }
        });
    }

    private void updateTitleStr(RegisterInviteInfo response) {
        this.registerInviteInfo = response;
        //恭喜您成功注册BoC Pay，现在输入邀请码，您的亲朋好友亦可得到<money>$10元奖赏</money> 红色处理
        mId_invite_share_title.setText(response.getPageTitle());
        //恭喜您成功注册BoC Pay，现在输入邀请码，您的亲朋好友亦可得到<money>$10元奖赏</money>
        String reqardRule = response.getRewardRule();
        int firstSubStart = 0;
        int firstSubEnd = 0;
        if (reqardRule.contains("<money>")) {
            firstSubStart = reqardRule.indexOf("<money>");
            reqardRule = reqardRule.replace("<money>", "");
            firstSubEnd = reqardRule.indexOf("</money>");
            reqardRule = reqardRule.replace("</money>", "");
        }
        SpannableString spannableStringSubTitle = new SpannableString(reqardRule);
        spannableStringSubTitle.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#B00931"));
                ds.setUnderlineText(false);
            }
        }, firstSubStart, firstSubStart + (firstSubEnd - firstSubStart), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        mId_invite_share_sub_title.setText(spannableStringSubTitle);

    }

    private void addTransferInviteRecord() {
        String incode = mId_verifyEdit.getCurrentInputContent();
        ApiProtocolImplManager.getInstance().addTransferInviteRecord(this, incode, new NetWorkCallbackListener<RegisterInviteStatusEntity>() {
            @Override
            public void onFailed(String errorCode, final String errorMsg) {
                showErrorMsgDialog(RegisterInviteActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(RegisterInviteStatusEntity response) {
                Intent intent = new Intent(RegisterInviteActivity.this, RegisterInviteStatusActivity.class);
                intent.putExtra(Constants.INVITE_TYPE, eventType);
                intent.putExtra(Constants.STATUS_MESSAGE, response.getAwardTip());
//                intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//                intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_invite;
    }

    @Override
    public void onBackPressed() {

        if (eventType == INVITE_TYPE_REGISTER) {

        } else {
            super.onBackPressed();
        }
    }
}
