package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;

/**
 * Created by ramon on 2018/8/20.
 * 转账试算返回结果
 */

public class TransferConfirmReq extends BaseEntity {
    //内部订单号
    public String scrRefNo;

    //债务方账号，电话或者邮箱
    public String tansferToAccount;
    public String collectionAccount;

    //转账金额
    public String transferAmount;

    //银行标识,行内转账，行外转账标识0：FPS-Transfer to other banks   1: Bank of China (HK)
    public String bankType;

    //债务方账户类型 0：电话，1：邮箱
    public String transferType;
    public String currency;
    public String tansferAccount;
    public String billReference;

    //红包tips
    public String bocpayRedPackeNotifyTip;



    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String paymentCategory;

    public String postscript;
    public String mSmartLevel;

    public String transferDate;

    public String transferBank;
    public String dbtrNm;
    public String txnId;

    public String getExtCmt() {
        return extCmt;
    }

    public void setExtCmt(String extCmt) {
        this.extCmt = extCmt;
    }

    public String extCmt;
    public String preBank;
    public boolean isLiShi;

    public String getSmartAcLevel() {
        return smartAcLevel;
    }

    public void setSmartAcLevel(String smartAcLevel) {
        this.smartAcLevel = smartAcLevel;
    }

    /**
     * 我的账户级别 1：S1  2:S2  3:S3
     */
    public String smartAcLevel;

    /**
     * 区分是bocpay转账还是fps转账
     */
    public boolean isBocPayTransfer;
    public TransferResourcesEntity.DefBean.BckImgsBean bckImgsBean;
}
