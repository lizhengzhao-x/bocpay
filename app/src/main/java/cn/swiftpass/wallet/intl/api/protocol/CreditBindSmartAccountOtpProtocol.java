package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class CreditBindSmartAccountOtpProtocol extends BaseProtocol {
    public static final String TAG = CreditBindSmartAccountOtpProtocol.class.getSimpleName();


    public CreditBindSmartAccountOtpProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartBind/sentOtp";
    }
}
