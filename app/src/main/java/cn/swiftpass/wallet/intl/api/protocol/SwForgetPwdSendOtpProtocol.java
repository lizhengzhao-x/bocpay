package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class SwForgetPwdSendOtpProtocol extends BaseProtocol {

    public SwForgetPwdSendOtpProtocol(NetWorkCallbackListener dataCallback) {
        mUrl = "api/smartForgot/sentOtp";
        mDataCallback = dataCallback;
    }

}
