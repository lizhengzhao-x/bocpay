package cn.swiftpass.wallet.intl.api.protocol;


import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/19
 * 未拆利是翻页查询
 */

public class ReceivedRedPacketTurnOffProtocol extends BaseProtocol {
    public static final String TAG = ReceivedRedPacketTurnOffProtocol.class.getSimpleName();
    private int pageNum;
    private int pageSize;
    private String nextTimeStamp;

    public ReceivedRedPacketTurnOffProtocol(int pageNum,int pageSize,String nextTimeStamp,NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.nextTimeStamp = nextTimeStamp;
        mUrl = "api/redpacket/receivedRedPacketTurnOff";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAGENUM, pageNum);
        mBodyParams.put(RequestParams.PAGE_SIZE, pageSize);
        if(!TextUtils.isEmpty(nextTimeStamp)){
            mBodyParams.put(RequestParams.NEXTTIME_STAMP, nextTimeStamp);
        }

    }
}
