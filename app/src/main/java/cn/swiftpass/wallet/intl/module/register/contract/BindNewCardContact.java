package cn.swiftpass.wallet.intl.module.register.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardListEntity;

public class BindNewCardContact {
    public interface View extends IView {
        void getBankCardListSuccess(BindNewCardListEntity response);

        void getBankCardListFailed(String errorCode, String errorMsg);
    }

    public interface Presenter extends IPresenter<View> {
        //获取card 列表
        void getBankCardList();

    }
}
