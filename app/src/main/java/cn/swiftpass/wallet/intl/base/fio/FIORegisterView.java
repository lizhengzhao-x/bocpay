package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.FioRegRequestEntity;


public interface FIORegisterView<V extends IPresenter> {
    void onFioRegSuccess(FioRegRequestEntity response);

    void onFioRegFail(String errorCode, String errorMsg);
}
