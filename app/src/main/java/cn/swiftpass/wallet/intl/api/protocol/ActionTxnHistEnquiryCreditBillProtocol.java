package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/26 10:39
 * 账单查询接口
 * 对应的返回对象为 BillListEntity
 */
public class ActionTxnHistEnquiryCreditBillProtocol extends BaseProtocol {
    String mPanId;

    public ActionTxnHistEnquiryCreditBillProtocol(String mPanId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.mPanId = mPanId;
        mUrl = "api/transaction/actionTxnHistEnquiryCredit";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PANID, mPanId);
    }

}
