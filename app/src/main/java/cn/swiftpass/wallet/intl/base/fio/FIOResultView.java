package cn.swiftpass.wallet.intl.base.fio;


import android.content.Intent;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;


public interface FIOResultView<V extends IPresenter> {
    void onFioSuccess(int requestCode, int resultCode, Intent data);

    void onFioFail(int requestCode, int resultCode, Intent data);
}
