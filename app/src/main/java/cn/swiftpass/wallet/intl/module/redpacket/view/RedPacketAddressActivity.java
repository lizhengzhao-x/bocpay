package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.redpacket.contract.RedPacketAddressContract;
import cn.swiftpass.wallet.intl.module.redpacket.presenter.RedPacketAddressPresenter;
import cn.swiftpass.wallet.intl.module.redpacket.view.adapter.RedPacketContactSortAdapter;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;
import cn.swiftpass.wallet.intl.utils.ContactLocalCacheUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.SideBar;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * 通讯录
 */
public class RedPacketAddressActivity extends BaseCompatActivity<RedPacketAddressContract.Presenter> implements RedPacketAddressContract.View {
    private static final String TAG = "RedPacketAddressFragment";
    @BindView(R.id.ry_contact)
    RecyclerView ryContact;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.id_transfer_search_et)
    ClearEditText idTransferSearchEt;
    @BindView(R.id.lly_red_packet_address_contain)
    LinearLayout llyRedPacketAddressContain;


    @BindView(R.id.id_sideBar)
    SideBar idSideBar;

    /**
     * 派利是的转账
     */
    protected boolean isTransferNormalType = false;


    protected View emptyView;
    protected ImageView noDataImg;
    protected TextView noDataText;
    protected LinearLayoutManager layoutManager;

    private RedPacketContactSortAdapter contactSortAdapter;

    public List<ContactEntity> getItemCommonCollectionList() {
        return itemCommonCollectionList;
    }

    private List<ContactEntity> itemCommonCollectionList;

    public static RedPacketAddressActivity newInstance() {
        return new RedPacketAddressActivity();
    }


    @Override
    protected void init() {
        setToolBarTitle(getString(R.string.RP2101_11_1));

        itemCommonCollectionList = new ArrayList<>();
        EventBus.getDefault().register(this);

        contactSortAdapter = new RedPacketContactSortAdapter(itemCommonCollectionList);
        contactSortAdapter.bindToRecyclerView(ryContact);
        initRecycleView(ryContact, swipeRefreshLayout, contactSortAdapter);

        contactSortAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter adapter, View view, int position) {

                switch (view.getId()) {
                    case R.id.id_contain_view:
                        if (ButtonUtils.isFastDoubleClick(view.getId())) {
                            return;
                        }
                        startAnotherActivity(getActivity(), isTransferNormalType, itemCommonCollectionList.get(position).getNumber(), itemCommonCollectionList.get(position).getUserName(), null, itemCommonCollectionList.get(position).isEmail());
                        break;
                    default:
                        break;
                }
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = contactSortAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (getLayoutManager() != null) {
                        getLayoutManager().scrollToPositionWithOffset(position, 0);
                    }
                }

            }
        });

        idTransferSearchEt.setCursorVisible(false);
        idTransferSearchEt.setFocusable(false);
        idTransferSearchEt.setFocusableInTouchMode(false);
        idTransferSearchEt.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                List<ContractListEntity.RecentlyBean> recentlyBeans = mPresenter.dealWithAllList(getItemCommonCollectionList());
                RedPacketItemSearchActivity.setRecentlyInfo(recentlyBeans);
                HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
                mHashMapTransferInfo.put(Constants.TRANSFER_IS_LISHI, true);
                ActivitySkipUtil.startAnotherActivity(RedPacketAddressActivity.this, RedPacketItemSearchActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        checkPermission(getActivity());

        llyRedPacketAddressContain.setBackgroundResource(R.drawable.bg_red_pocket);
    }

    @Override
    protected RedPacketAddressContract.Presenter createPresenter() {
        return new RedPacketAddressPresenter();
    }

    private void updateContactList(ArrayList<ContactEntity> contactEntities) {
        if (contactEntities == null) {
            return;
        }
        //清除默认选中状态
        for (int j = 0; j < contactEntities.size(); j++) {
            ContactEntity contactEntitItem = contactEntities.get(j);
            contactEntitItem.setCollect(false);
        }
        //通讯录与本地缓存的列表对比 标记已经收藏的 ui显示不同
        List<String> contactEntitiesLocal = ContactLocalCacheUtils.getLocalCacheWithUserId(mContext, HttpCoreKeyManager.getInstance().getUserId());
        if (contactEntitiesLocal == null || contactEntitiesLocal.size() == 0) {

        } else {
            if (!contactEntities.isEmpty()) {
                for (int i = 0; i < contactEntitiesLocal.size(); i++) {
                    String contactID = contactEntitiesLocal.get(i);
                    for (int j = 0; j < contactEntities.size(); j++) {
                        ContactEntity contactEntitItem = contactEntities.get(j);
                        if (contactEntitItem.getContactID().equals(contactID)) {
                            contactEntitItem.setCollect(true);
                            break;
                        }
                    }
                }
            }
        }
        if (itemCommonCollectionList != null) {
            itemCommonCollectionList.clear();
            itemCommonCollectionList.addAll(contactEntities);
            updateRecycleView();
        }
    }


    private void updateRecycleView() {
        if (itemCommonCollectionList.size() > 0) {
            idSideBar.setVisibility(View.VISIBLE);
        } else {
            setEmptyText(R.string.TR1_2b_1);
            idSideBar.setVisibility(View.GONE);
        }
        contactSortAdapter.setDataList(itemCommonCollectionList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //清除通讯录
                ContactCompareUtils.clearAllContactList();
                updateItemList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.app_dark_red);
    }

    protected void updateContactList(ArrayList<ContactEntity> contactEntities, boolean obtain) {
        if (!obtain) {
            itemCommonCollectionList = new ArrayList<>();
            updateRecycleView();
            setEmptyText(R.string.STF2101_1_5);
        } else {
            updateContactList(contactEntities);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_red_packet_contact_view;
    }


    @Override
    public void checkSmartAccountNewBindSuccess(ContentEntity response, String phone, String name, String relName, boolean isEmail) {
        TransferUtils.transferWithPhoneNumber(getActivity(), isTransferNormalType, phone, name, relName, isEmail);
    }

    @Override
    public void checkSmartAccountNewBindError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            if (isTransferNormalType) {
                AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifyPwdToUpdateAccount();
                    }
                });
            } else {
                AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_13), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifyPwdToUpdateAccount();
                    }
                });
            }
        } else {
            showErrorMsgDialog(getActivity(), errorMsg);
        }
    }

    protected void checkPermission(final Activity mActivity) {
        if (mActivity == null) {
            return;
        }
        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                //拒绝并勾选不再询问
                PermissionInstance.getInstance().setHasFirstRejectContacts(false);
            } else {
                PermissionInstance.getInstance().setHasFirstRejectContacts(true);
            }
        }

        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                //清除通讯录 每次进入这个页面都是最新的通讯录
                final ArrayList<ContactEntity> contactEntitiesTmp = new ArrayList<>();
                if (ContactUtils.getInstance().getContacts() != null && ContactUtils.getInstance().getContacts().size() > 0) {
                    contactEntitiesTmp.addAll(ContactUtils.getInstance().getContacts());
                    updateContactList(contactEntitiesTmp, true);
                } else {
                    ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                        @Override
                        public void OnReadContactsSuccess(ArrayList<ContactEntity> contactEntities) {
                            contactEntitiesTmp.addAll(contactEntities);
                            updateContactList(contactEntitiesTmp, true);
                        }
                    });
                }
            }

            @Override
            public void rejectPermission() {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                    if (!PermissionInstance.getInstance().isHasFirstRejectContacts()) {
                        showLackOfPermissionDialog();
                    } else {
                        PermissionInstance.getInstance().setHasFirstRejectContacts(false);
                    }
                }
                ArrayList<ContactEntity> itemCommonCollectionList = new ArrayList<>();
                updateContactList(itemCommonCollectionList, false);
            }
        }, Manifest.permission.READ_CONTACTS);
    }

    public void showLackOfPermissionDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(getString(R.string.STF2101_1_1));
        builder.setMessage(getString(R.string.STF2101_1_2));
        builder.setPositiveButton(getString(R.string.M10_1_25), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AndroidUtils.startAppSettingForResult(mContext, RedPacketAddressActivity.this, Constants.REQUEST_ADDRESS_SET_CODE);

            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        CustomDialog mDealDialog = builder.create();
        mDealDialog.setCancelable(false);
        mDealDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_ADDRESS_SET_CODE) {
            checkPermission(getActivity());
            return;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_UPDATE_ADDRESS_COLLECTION_STATUS) {
            updateContactList(ContactUtils.getInstance().getAllContacts(mContext, false));
        }
    }

    public void updateItemList() {
        if (swipeRefreshLayout == null) {
            return;
        }
        //更新通讯录数据 只有手动刷新的操作才会重新激发从本地通讯录读取通讯录
        if (isGranted(Manifest.permission.READ_CONTACTS)) {
            //单独线程来处理 防止阻塞 华为手机上5000条通讯录会有明显卡顿
            new Thread(new Runnable() {

                @Override
                public void run() {
                    final ArrayList<ContactEntity> contactEntities = ContactUtils.getInstance().getAllContacts(mContext, true);
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateContactList(contactEntities);
                            }
                        });
                    }
                }
            }).run();

        }
        swipeRefreshLayout.setRefreshing(false);
    }

    protected void initRecycleView(RecyclerView recyclerView, final SwipeRefreshLayout swipeRefreshLayout, BaseRecyclerAdapter adapter) {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateItemList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.app_dark_red);

        emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(R.string.TR1_2b_1));
        noDataImg.setImageResource(R.mipmap.icon_transaction_noresult);
        adapter.setEmptyView(emptyView);
    }

    public LinearLayoutManager getLayoutManager() {
        return layoutManager;
    }

    protected View getEmptyView() {
        return emptyView;
    }

    protected void setEmptyText(int resId) {
        if (noDataText == null) {
            return;
        }
        noDataText.setText(resId);
    }

    protected void setEmptyImg(int resId) {
        if (noDataImg == null) {
            return;
        }
        noDataImg.setImageResource(resId);
    }

    protected void startAnotherActivity(final Activity activity, final boolean isTransferNormalType, final String phone, final String name, final String relName, final boolean isEmail) {
        mPresenter.checkSmartAccountNewBind(phone, name, relName, isEmail);
    }

    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false,new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (getActivity() == null) {
                    return;
                }
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                    }
                }, 300);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {

                    }
                });
            }

            @Override
            public void onVerifyCanceled() {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
            }
        });
    }

}
