package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/19
 * P2M交易结果页增加AA收款快速入口-FPS登记检查接口  说明：未开通智能账户抛出EWA3019  未登记FPS抛出EWA3026
 */

public class CheckStaticCodeProtocol extends BaseProtocol {
    public static final String TAG = CheckStaticCodeProtocol.class.getSimpleName();

    public CheckStaticCodeProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/checkAddressingFps";
    }
}
