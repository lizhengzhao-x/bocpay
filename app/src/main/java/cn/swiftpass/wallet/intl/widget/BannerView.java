package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * 无限轮播banner控件 实现无限轮播  在viewList前后各加一个view
 * 1.android:clipChildren="false",它的意思是系统不会把超过当前view的页面的切掉，默认是“true”，
 * 2.pager.setPadding(60, 0, 60, 0); pager.setPageMargin(20);
 * 3.给viewpager设置 android:clipToPadding="false"属性。
 *
 * @author shican
 */
public class BannerView extends FrameLayout implements ViewPager.OnPageChangeListener {
    public String tag = "BannerView";
    private int mIndicatorMargin = 5;
    private int mIndicatorWidth = 8;
    private int mIndicatorHeight = 8;
    /**
     * 改变这个值更改itemview 之间的间距
     */
    public static final int LEFT_MARGIN = 15;
    /**
     * 改变这个参数 控制viewpager item width
     */
    public static final int LEFT_PADING = 35;
    public static final int INDICATORHEIGHT = 6;
    private int mIndicatorSelectedResId = R.mipmap.img_guide_circle_now;
    private int mIndicatorUnselectedResId = R.mipmap.img_guide_circle_default;
    private int count;
    /**
     * banner当前位置
     */
    private int currentItem;
    /**
     * 自动轮播时间间隔
     */
    private int delayTime = 0000;
    private int gravity = -1;
    /**
     * 是否开启自动轮播  默认开启
     */
    private boolean isAutoPlay = false;
    private List<View> containViews;
    private List<ImageView> indicatorImages;
    private Context context;
    private ViewPager viewPager;
    private LinearLayout indicatorView;
    private Handler handler = new Handler();
    private OnBannerClickListener listener;
    private OnLoadImageListener imageListener;
    private int lastPosition = 1;

    private List<String> currentImageUrls;
    /**
     * 是否是用户手动切换ViewPager
     */
    private boolean manualDraging = false;

    public BannerView(Context context) {
        this(context, null);
    }

    public BannerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        indicatorImages = new ArrayList<ImageView>();
        containViews = new ArrayList<View>();
        initView(context, attrs);
    }

    private void handleTypedArray(Context context, AttributeSet attrs) {
        if (attrs == null) {
            return;
        }
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.BannerView);
        mIndicatorHeight = AndroidUtils.dip2px(context, INDICATORHEIGHT);
        mIndicatorWidth = mIndicatorHeight;
        mIndicatorMargin = typedArray.getDimensionPixelSize(R.styleable.BannerView_indicator_margin, 8);
        mIndicatorSelectedResId = typedArray.getResourceId(R.styleable.BannerView_indicator_drawable_selected, R.mipmap.icon_carousel_filled);
        mIndicatorUnselectedResId = typedArray.getResourceId(R.styleable.BannerView_indicator_drawable_unselected, R.mipmap.icon_carousel_empty);
        typedArray.recycle();
    }

    private void initView(Context context, AttributeSet attrs) {
        containViews.clear();
        currentImageUrls = new ArrayList<>();
        View view = LayoutInflater.from(context).inflate(R.layout.view_banner, this, true);
        viewPager = view.findViewById(R.id.viewpager);
        indicatorView = view.findViewById(R.id.indicator);
        handleTypedArray(context, attrs);
    }

    public void setDelayTime(int delayTime) {
        this.delayTime = delayTime * 1000;
        if (this.isAutoPlay && this.delayTime > 0) {
            startAutoPlay();
        }
    }


    public void setImageList(List<String> imagesUrl) {
        if (imagesUrl == null || imagesUrl.size() <= 0) {
            return;
        }
        currentImageUrls.clear();
        currentImageUrls.addAll(imagesUrl);
        count = currentImageUrls.size();
        containViews.clear();
        for (int i = 0; i < currentImageUrls.size() + 2; i++) {
            ImageView view = new ImageView(getContext());
            view.setScaleType(ImageView.ScaleType.FIT_XY);
            containViews.add(view);
        }
        createIndicator();
        setData();
    }

    private void createIndicator() {
        indicatorImages.clear();
        indicatorView.removeAllViews();
        for (int i = 0; i < currentImageUrls.size(); i++) {
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mIndicatorWidth, mIndicatorHeight);
            params.leftMargin = mIndicatorMargin;
            params.rightMargin = mIndicatorMargin;
            if (i == 0) {
                imageView.setImageResource(mIndicatorSelectedResId);
            } else {
                imageView.setImageResource(mIndicatorUnselectedResId);
            }
            indicatorView.addView(imageView, params);
            indicatorImages.add(imageView);
        }
    }


    private void setData() {
        viewPager.setOffscreenPageLimit(3);
        viewPager.setAdapter(new BannerPagerAdapter());
        viewPager.setFocusable(true);
        viewPager.setCurrentItem(1);

        int paddingWidth = AndroidUtils.dip2px(getContext(), LEFT_PADING);
        //Padding会改变itemview的宽度
        viewPager.setPadding(paddingWidth, 0, paddingWidth, 0);
        currentItem = 1;
        viewPager.addOnPageChangeListener(this);
        //margin item 与 item 之间的距离
        int pageMargin = AndroidUtils.dip2px(getContext(), LEFT_MARGIN);
        viewPager.setPageMargin(pageMargin);
        if (gravity != -1) {
            indicatorView.setGravity(gravity);
        }
        if (isAutoPlay && delayTime > 0) {
            startAutoPlay();
        }
    }

    public void isAutoPlay(boolean isAutoPlay) {
        this.isAutoPlay = isAutoPlay;
    }

    private void startAutoPlay() {
        isAutoPlay = true;
        handler.removeCallbacks(task);
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(task, delayTime);
    }

    public void stopPlayImage() {
        isAutoPlay = false;
        handler.removeCallbacks(task);
        handler.removeCallbacksAndMessages(null);

    }

    public void restartPlayImage() {
        isAutoPlay = true;
        currentItem = currentItem + 1;
        //用户需求离开页面再进来 要从下个banner
        if (isAutoPlay && delayTime > 0) {
            currentItem = currentItem % (count + 1);
            if (currentItem == 0) {
                currentItem = 1;
                viewPager.setCurrentItem(1);
            } else {
                viewPager.setCurrentItem(currentItem);
            }
            if (delayTime > 0) {
                //时间后台配置 大于0 才需要自动轮播
                handler.removeCallbacks(task);
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(task, delayTime);
            }
        }
    }


    private final Runnable task = new Runnable() {

        @Override
        public void run() {
            if (isAutoPlay) {
                currentItem = currentItem % (count + 1) + 1;
                if (currentItem == 1) {
                    viewPager.setCurrentItem(currentItem, false);
                    handler.post(task);
                } else {
                    viewPager.setCurrentItem(currentItem);
                    handler.postDelayed(task, delayTime);
                }
            } else {
                handler.postDelayed(task, delayTime);
            }
        }
    };

    class BannerPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return containViews.size();
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            ImageView imageItem = (ImageView) containViews.get(position);
            // 图片地址位置  0 1 2 3 4 5  size 是 6
            //底部view预加载 前边加一个 后边加一个（漏出来两部分） 5 0 1 2 3 4 5 0  size 8
            int positionRel = 0;
            LogUtils.i("TAG", "position:" + position);
            if (position == 0) {
                positionRel = currentImageUrls.size() - 1;
            } else {
                positionRel = position - 1;
            }
            positionRel = positionRel % containViews.size();
            if (positionRel >= currentImageUrls.size()) {
                if (positionRel == containViews.size() - 1) {
                    positionRel = 1;
                } else if (positionRel == containViews.size() - 2) {
                    positionRel = 0;
                }
            }
            GlideApp.with(context).load(currentImageUrls.get(positionRel)).apply(RequestOptions.bitmapTransform(new RoundedCorners(10))).placeholder(R.mipmap.banner_bg_preview).into(imageItem);
            container.addView(imageItem);
            int finalPositionRel = positionRel;
            imageItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.OnBannerClick(v, finalPositionRel + 1);
                    }
                }
            });
            return imageItem;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ImageView view = (ImageView) containViews.get(position % containViews.size());
            container.removeView(view);
            view.setImageBitmap(null);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        switch (state) {
            case ViewPager.SCROLL_STATE_DRAGGING:
                manualDraging = true;
                isAutoPlay = false;
                break;
            case ViewPager.SCROLL_STATE_SETTLING:
                isAutoPlay = true;
                break;
            case ViewPager.SCROLL_STATE_IDLE:
                manualDraging = false;
                //如果滑到当前位置为0,则设置为imgList最后一个位置
                if (viewPager.getCurrentItem() == 0) {
                    viewPager.setCurrentItem(count, false);
                } else if (viewPager.getCurrentItem() == count + 1) {
                    ////如果滑到当前位置为添加前后图片的最后一个位置,则设置为imgList第一个位置
                    viewPager.setCurrentItem(1, false);
                }
                currentItem = viewPager.getCurrentItem();
                isAutoPlay = true;
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        LogUtils.i(tag, "onPageSelected->position " + position + " manualDraging " + manualDraging);
        indicatorImages.get((lastPosition - 1 + count) % count).setImageResource(mIndicatorUnselectedResId);
        indicatorImages.get((position - 1 + count) % count).setImageResource(mIndicatorSelectedResId);
        lastPosition = position;
        if (manualDraging && isAutoPlay && delayTime > 0) {
            //手动滑动 要重新开始计时
            stopPlayImage();
            startAutoPlay();
        }
    }


    public void setOnBannerClickListener(OnBannerClickListener listener) {
        this.listener = listener;
    }

    public void setOnBannerImageListener(OnLoadImageListener imageListener) {
        this.imageListener = imageListener;
    }

    public interface OnBannerClickListener {
        void OnBannerClick(View view, int position);
    }

    public interface OnLoadImageListener {
        void OnLoadImage(ImageView view, Object url);
    }
}

