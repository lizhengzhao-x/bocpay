package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardRewardHistoryInfo extends BaseEntity {
    private String account;
    private String obtainReward;
    private String obtainRewardDate;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getObtainReward() {
        return obtainReward;
    }

    public void setObtainReward(String obtainReward) {
        this.obtainReward = obtainReward;
    }

    public String getObtainRewardDate() {
        return obtainRewardDate;
    }

    public void setObtainRewardDate(String obtainRewardDate) {
        this.obtainRewardDate = obtainRewardDate;
    }
}
