package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.FioCheckEntity;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOCheckView
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 17:21
 * @change
 * @chang time
 * @class
 */
public interface FIOCheckView<V extends IPresenter> {
    void onFioCheckSuccess(FioCheckEntity response);

    void onFioCheckFail(String errorCode, String errorMsg);
}
