package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.view.LiShiHistoryActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

public class LishiHistoryTitleHolder extends RecyclerView.ViewHolder {
    private final Activity activity;
    @BindView(R.id.tv_lishi_more)
    LinearLayout tvLishiMore;
    @BindView(R.id.tv_title_history)
    TextView tvTitleHistory;


    public LishiHistoryTitleHolder(Activity activity, View titleHolder) {
        super(titleHolder);
        ButterKnife.bind(this, itemView);
        this.activity = activity;
        tvTitleHistory.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
        tvTitleHistory.getPaint().setStrokeWidth(0.7f);
    }

    public void setData(GetLiShiHomeEntity homeEntity) {
        tvLishiMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == activity) {
                    return;
                }
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.TRANSACTIONTYPE, Constants.RECEVIED);
                ActivitySkipUtil.startAnotherActivity(activity, LiShiHistoryActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        //大于0显示更多
        if (homeEntity != null && homeEntity.getReceivedRecordsTurnOnHasMore() != 0) {
            tvLishiMore.setVisibility(View.VISIBLE);
        } else {
            tvLishiMore.setVisibility(View.GONE);
        }

    }
}
