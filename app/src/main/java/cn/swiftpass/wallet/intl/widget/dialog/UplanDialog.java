package cn.swiftpass.wallet.intl.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class UplanDialog extends Dialog {

    private Context context;

    public UplanDialog(Context context) {
        this(context, R.style.CustomDialog);
        this.context = context;
    }

    public UplanDialog(Context context, int themeResId) {
        super(context, themeResId);
        initStyle(themeResId);
    }

    private void initStyle(int themeResId) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().getAttributes().windowAnimations = themeResId;
            getWindow().setBackgroundDrawableResource(android.R.color.background_light);
            getWindow().setType(WindowManager.LayoutParams.FIRST_APPLICATION_WINDOW);
        }
        setCanceledOnTouchOutside(true);
        setCancelable(true);
    }

    @Override
    public void dismiss() {
        if (this.isShowing()) {
            super.dismiss();
        }
    }


    public void showMatchParentWidthAndGravityBottom() {
        if (context == null || getWindow() == null) {
            return;
        }
        getWindow().setGravity(Gravity.CENTER);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        float padding = (AndroidUtils.getScreenWidth(context) * 1.0f / AndroidUtils.getScreenHeight(context) / 0.5f - 1) * 400 + 60;

        lp.width = AndroidUtils.getScreenWidth(getContext()) - AndroidUtils.dip2px(getContext(), padding);
        getWindow().setAttributes(lp);

        show();
    }

    /**
     * 宽度显示在中间，从底部动画冒出
     */
    public void showWithBottomAnim() {
        if (getWindow() == null) {
            return;
        }
        showMatchParentWidthAndGravityBottom();
    }

}
