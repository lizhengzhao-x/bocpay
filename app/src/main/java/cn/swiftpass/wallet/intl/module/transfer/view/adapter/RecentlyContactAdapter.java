package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

/**
 * 最近联系人adapter
 */
public class RecentlyContactAdapter extends BaseRecyclerAdapter<ContractListEntity.RecentlyBean> {

    public RecentlyContactAdapter(@Nullable List<ContractListEntity.RecentlyBean> data) {
        super(R.layout.item_recent_contact, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, ContractListEntity.RecentlyBean recentContactModel, int position) {
        holder.setText(R.id.id_contact_user_name, recentContactModel.getDisCustName());
        if (recentContactModel.isEmail()) {
            holder.setText(R.id.id_contact_user_number, recentContactModel.getDisEmail());
        } else {
            holder.setText(R.id.id_contact_user_number, recentContactModel.getDisPhoneNo());
        }
        holder.setGone(R.id.id_contact_collect_imag, true);
        holder.setImageResource(R.id.id_contact_head_img, recentContactModel.isEmail() ? R.mipmap.icon_transfer_profile_email : R.mipmap.icon_transfer_profile_mobile);
    }
}
