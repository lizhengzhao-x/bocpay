package cn.swiftpass.wallet.intl.module.transfer.view;

/**
 * Created by ZhangXinchao on 2019/11/6.
 */
public interface GetTransferSelTypeListener {

    boolean isNormalTransferType();
}
