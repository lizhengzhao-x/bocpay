package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderCardEntity;


public class TransferCrossBorderPayeeContract {

    public interface View extends IView {


        void checkCardIdSuccess(TransferCrossBorderCardEntity response);

        void checkCardIdError(String errorCode, String errorMsg);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(MySmartAccountEntity response, String action);

        void getRegEddaInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);
    }


    public interface Presenter extends IPresenter<View> {


        void checkCardId(String cardText);

        void getSmartAccountInfo(String action, boolean iShowLoading);

        void getRegEddaInfo();
    }

}
