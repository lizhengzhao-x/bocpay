package cn.swiftpass.wallet.intl.utils;

import java.io.UnsupportedEncodingException;

/**
 * 从一期lib中独立出来 误删除 无改动
 */
public class BaseUtils {

    private static final byte[] ALPHABET;
    private static final byte[] _NATIVE_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] DECODABET;


    static {
        byte[] __bytes;
        try {
            __bytes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".getBytes("UTF-8");
        } catch (UnsupportedEncodingException use) {
            __bytes = _NATIVE_ALPHABET;
        }
        ALPHABET = __bytes;

        DECODABET = new byte[]{-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9};
    }


    private static byte[] encode3to4(byte[] source, int srcOffset, int numSigBytes, byte[] destination, int destOffset) {
        int inBuff = (numSigBytes > 0 ? source[srcOffset] << 24 >>> 8 : 0) | (numSigBytes > 1 ? source[(srcOffset + 1)] << 24 >>> 16 : 0) | (numSigBytes > 2 ? source[(srcOffset + 2)] << 24 >>> 24 : 0);

        switch (numSigBytes) {
            case 3:
                destination[destOffset] = ALPHABET[(inBuff >>> 18)];
                destination[(destOffset + 1)] = ALPHABET[(inBuff >>> 12 & 0x3F)];
                destination[(destOffset + 2)] = ALPHABET[(inBuff >>> 6 & 0x3F)];
                destination[(destOffset + 3)] = ALPHABET[(inBuff & 0x3F)];
                return destination;
            case 2:
                destination[destOffset] = ALPHABET[(inBuff >>> 18)];
                destination[(destOffset + 1)] = ALPHABET[(inBuff >>> 12 & 0x3F)];
                destination[(destOffset + 2)] = ALPHABET[(inBuff >>> 6 & 0x3F)];
                destination[(destOffset + 3)] = 61;
                return destination;
            case 1:
                destination[destOffset] = ALPHABET[(inBuff >>> 18)];
                destination[(destOffset + 1)] = ALPHABET[(inBuff >>> 12 & 0x3F)];
                destination[(destOffset + 2)] = 61;
                destination[(destOffset + 3)] = 61;
                return destination;
        }

        return destination;
    }


    public static String encodeBytes(byte[] source) {
        return encodeBytes(source, 0, source.length, 0);
    }

    public static String encodeBytes(byte[] source, int off, int len, int options) {
        int dontBreakLines = options & 0x8;
        int gzip = options & 0x2;

        if (gzip == 2) {
            return new String("");
        }

        boolean breakLines = dontBreakLines == 0;

        int len43 = len * 4 / 3;
        byte[] outBuff = new byte[len43 + (len % 3 > 0 ? 4 : 0) + (breakLines ? len43 / 76 : 0)];
        int d = 0;
        int e = 0;
        int len2 = len - 2;
        int lineLength = 0;
        for (; d < len2; e += 4) {
            encode3to4(source, d + off, 3, outBuff, e);

            lineLength += 4;
            if ((breakLines) && (lineLength == 76)) {
                outBuff[(e + 4)] = 10;
                e++;
                lineLength = 0;
            }
            d += 3;
        }

        if (d < len) {
            encode3to4(source, d + off, len - d, outBuff, e);
            e += 4;
        }

        try {
            return new String(outBuff, 0, e, "UTF-8");
        } catch (UnsupportedEncodingException uue) {
        }
        return new String(outBuff, 0, e);
    }


}
