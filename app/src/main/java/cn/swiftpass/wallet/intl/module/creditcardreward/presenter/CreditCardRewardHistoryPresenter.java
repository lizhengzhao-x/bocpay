package cn.swiftpass.wallet.intl.module.creditcardreward.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetHistoryCreditCardRewardListProtocol;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryList;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditCardRewardListContact;

public class CreditCardRewardHistoryPresenter extends BasePresenter<CreditCardRewardListContact.View> implements CreditCardRewardListContact.Presenter {

    @Override
    public void getRewardHistoryList(String dateKey, String rewardsType, int pageNum, boolean isRefresh) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetHistoryCreditCardRewardListProtocol(dateKey, rewardsType, String.valueOf(pageNum), new NetWorkCallbackListener<CreditCardRewardHistoryList>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getHistoryCreditCardRewardListError(errorCode, errorMsg, isRefresh);
                }
            }

            @Override
            public void onSuccess(CreditCardRewardHistoryList response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getHistoryCreditCardRewardListSuccess(response, isRefresh);
                }
            }
        }).execute();
    }
}
