package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.FioInitEntity;


public interface FIOAsKeyView<V extends IPresenter> {
    void onFioAsKeySuccess(FioInitEntity response);

    void onFioAsKeyFail(String errorCode, String errorMsg);
}
