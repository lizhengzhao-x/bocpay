package cn.swiftpass.wallet.intl.sdk.fcm;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;

public class MyFirebaseInstanceIDService extends FirebaseMessagingService {

    private static final String TAG = "Firebase";

    @Override
    public void onNewToken(@NonNull String refreshedToken) {
        super.onNewToken(refreshedToken);
        HttpCoreKeyManager.getInstance().setmGcmDeviceToken(refreshedToken);
        LogUtils.i(TAG, "Refreshed token: " + refreshedToken);
    }
}

