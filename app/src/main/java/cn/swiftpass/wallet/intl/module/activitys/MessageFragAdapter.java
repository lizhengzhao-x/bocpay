package cn.swiftpass.wallet.intl.module.activitys;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;


public class MessageFragAdapter extends FragmentPagerAdapter {

    private ArrayList<String> titleList;
    private ArrayList<Fragment> fragmentList;

    public MessageFragAdapter(FragmentManager supportFragmentManager, ArrayList<Fragment> fragments) {
        super(supportFragmentManager);
        this.fragmentList = fragments;
    }


    @Override
    public Fragment getItem(int position) {
//        if (position==0){
//            return new MajorScanFragment();
//        }else if (position==1){
//            return new BackScanFragment();
//        }
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

}
