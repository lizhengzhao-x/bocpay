package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class AppParameters extends BaseEntity {

    public OpenSwitch getMultipleSwitch() {
        if (multipleSwitch == null) return new OpenSwitch();
        return multipleSwitch;
    }

    public void setMultipleSwitch(OpenSwitch multipleSwitch) {
        this.multipleSwitch = multipleSwitch;
    }

    /**
     * 一些开关操作
     */
    private OpenSwitch multipleSwitch;

    public static class OpenSwitch extends BaseEntity {

        public String getLbs() {
            return lbs;
        }

        public void setLbs(String lbs) {
            this.lbs = lbs;
        }

        /**
         * 开关 1是开
         */
        private String lbs;

        public boolean isLbsClose() {
            if (TextUtils.isEmpty(lbs)) {
                return true;
            } else {
                return !TextUtils.equals(lbs, "1");
            }
        }
    }
}
