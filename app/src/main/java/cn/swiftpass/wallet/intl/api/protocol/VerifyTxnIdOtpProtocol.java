package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 校验信用卡OTP接口，返回对象为 SendOTPEntity
 */

public class VerifyTxnIdOtpProtocol extends BaseProtocol {
    public static final String TAG = VerifyTxnIdOtpProtocol.class.getSimpleName();

    /**
     * 用户id
     */
    String txnId;


    /**
     * 短信验证码
     */
    String mOtp;


    public VerifyTxnIdOtpProtocol(String txnId, String otp, NetWorkCallbackListener dataCallback) {
        this.txnId = txnId;

        this.mOtp = otp;
        this.mDataCallback = dataCallback;
        mUrl = "api/crossTransfer/verifyOTP";

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);
        mBodyParams.put(RequestParams.VCODE, mOtp);

    }
}
