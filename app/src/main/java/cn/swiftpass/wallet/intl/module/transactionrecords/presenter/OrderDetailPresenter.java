package cn.swiftpass.wallet.intl.module.transactionrecords.presenter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.GetSweptCodePointsProtocol;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemType;
import cn.swiftpass.wallet.intl.module.transactionrecords.api.QueryTxnDetailProtocol;
import cn.swiftpass.wallet.intl.module.transactionrecords.contract.OrderDetailContract;
import cn.swiftpass.wallet.intl.module.transactionrecords.entity.OrderDetailEntity;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.BocPayTransUtils;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;

import static cn.swiftpass.wallet.intl.api.protocol.GetSweptCodePointsProtocol.REDEEMTYPE_TRANSACTION;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel.ItemModelBuilder.STYLE_LINE;
import static cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListAdapter.SUCCESS_INFO_;

/**
 * 详情页很复杂，前边列表展示数据拼接成详情接口返回数据*(根据不同的交易类型取不同的参数) {"crAcNo":"86-18675597115","crAcTp":"0","creditName":"ONE T*****","debitName":"TEAM B*******","outgroup":[]}
 */
public class OrderDetailPresenter extends BasePresenter<OrderDetailContract.View> implements OrderDetailContract.Presenter {

    public static final String SPECIAL_CHAR = "&&";
    public static final String SPECIAL_SPACE = " ";
    private Context mContext;

    private OrderDetailEntity orderDetailEntity;

    /**
     * 是不是派利是的交易记录
     */
    private boolean transactionIsLiShi;

    public OrderDetailPresenter(Context mContextIn) {

        this.mContext = mContextIn;
    }

    @Override
    public void getOrderDetailBySmartAccount(SmartAccountBillListEntity.OutgroupBean smartEntity, String listType, String srcRefNo, String orderType) {

        transactionIsLiShi = false;

        if (!TextUtils.isEmpty(srcRefNo) && !TextUtils.isEmpty(orderType)) {
            if (getView() != null) {
                getView().showDialogNotCancel();
            }
            new QueryTxnDetailProtocol(srcRefNo, orderType, listType, new NetWorkCallbackListener<OrderDetailEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().getOrderDetailError(errorCode, errorMsg);
                        getView().dismissDialog();
                    }
                }

                @Override
                public void onSuccess(OrderDetailEntity response) {
                    if (getView() != null) {
                        orderDetailEntity = response;
                        getView().getOrderDetailSuccess(dealDateForSmartAccount(smartEntity, response, false));
                        getView().dismissDialog();

                    }
                }
            }).execute();
        } else {
            getView().getOrderDetailSuccess(dealDateForSmartAccount(smartEntity, null, false));
        }
    }


    @Override
    public void getOrderDetailByBocPay(OrderQueryEntity orderQueryEntity, String listType, String srcRefNo, String orderType) {
        transactionIsLiShi = false;
        if (!TextUtils.isEmpty(srcRefNo) && !TextUtils.isEmpty(orderType)) {
            if (getView() != null) {
                getView().showDialogNotCancel();
            }
            new QueryTxnDetailProtocol(srcRefNo, orderType, listType, new NetWorkCallbackListener<OrderDetailEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().getOrderDetailError(errorCode, errorMsg);
                        getView().dismissDialog();
                    }
                }

                @Override
                public void onSuccess(OrderDetailEntity response) {
                    if (getView() != null) {
                        orderDetailEntity = response;
                        getView().getOrderDetailSuccess(dealDateForBocPay(orderQueryEntity, response, false));
                        getView().dismissDialog();
                    }
                }
            }).execute();
        } else {
            getView().getOrderDetailSuccess(dealDateForBocPay(orderQueryEntity, null, false));
        }
    }

    @Override
    public void queryGradeInfo(String tnxId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetSweptCodePointsProtocol(tnxId, REDEEMTYPE_TRANSACTION, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryGradeInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryGradeInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public ArrayList<ItemModel> dealDateWithBocPay(OrderQueryEntity bocPayTransDetailEntity, boolean isShareView) {
        return dealDateForBocPay(bocPayTransDetailEntity, orderDetailEntity, isShareView);
    }

    @Override
    public ArrayList<ItemModel> dealDateWithSmartAccount(SmartAccountBillListEntity.OutgroupBean smartEntity, boolean isShareView) {
        return dealDateForSmartAccount(smartEntity, orderDetailEntity, isShareView);
    }

    @Override
    public boolean transferIsLiShi() {
        return transactionIsLiShi;
    }


    //-------------------------------------BocPay交易记录跳转过来数据封装处理----------------------------------------------------//
    private void generateBarCodeModel(OrderQueryEntity paymentEnquiryResult, ArrayList<ItemModel> itemModels) {
        if (!TextUtils.isEmpty(paymentEnquiryResult.getBarcode())) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BARCODE).itemContent(paymentEnquiryResult.getBarcode()).build());
        }
    }

    /**
     * 判断是否需要添加原价栏位
     * 对于银联消费 理论上排列顺序应该是 金额 --> 原价 --> 留言
     *
     * @param paymentEnquiryResult
     * @param itemModels
     * @return
     */
    private boolean generateOriginalMoneyModel(OrderQueryEntity paymentEnquiryResult, ArrayList<ItemModel> itemModels, boolean isSaveView) {
        if (!TextUtils.isEmpty(paymentEnquiryResult.getDiscount())) {
            //优惠信息
            try {
                if (Double.valueOf(paymentEnquiryResult.getDiscount()) > 0) {
                    String discountStr = AndroidUtils.getTrxCurrency(paymentEnquiryResult.getCur()) + SPECIAL_SPACE + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getOriginalAmt(), 2);
                    if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
                        //没有留言 直接是 金额--->原价
                        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CP1_6c_2)).itemContent(discountStr).itemVisibleLine(true).itemTextState(STYLE_LINE).build());
                    } else {
                        //有留言 应该是 金额--->原价--> 这里因为之前理解不到位 留言在第一个栏位已经增加进去(留言已经增加进去数组中)
                        itemModels.add(itemModels.size() - 1, new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(mContext.getString(R.string.CP1_6c_2)).itemContent(discountStr).itemTextState(STYLE_LINE).build());
                    }

                    return true;
                }
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    /**
     * bocPay 标题取参数
     *
     * @param paymentEnquiryResult
     * @param itemModels
     * @param orderDetailEntity
     */
    private void generateHeaderModel(OrderQueryEntity paymentEnquiryResult, ArrayList<ItemModel> itemModels, OrderDetailEntity orderDetailEntity) {
        //标题
        String title = paymentEnquiryResult.getMerchantName();
        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "8") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6")) {
            //需求是 增值/奖赏标题 内容要特殊处理
            title = paymentEnquiryResult.getTxTitle();
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "9")) {
            //银行账户转账 对于bocpay 标题要改
            if (!TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
                title = orderDetailEntity.getCreditName();
            } else {
                title = "";
            }
        }
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(BocPayTransUtils.getImageResource(paymentEnquiryResult)).itemTitle(title).build());
    }

    private boolean generateRemarkModel(OrderQueryEntity paymentEnquiryResult, ArrayList<ItemModel> itemModels) {
        boolean isHasRemark = false;
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            isHasRemark = true;
            //留言
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                //派利是
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_11_2)).itemVisibleLine(true).itemContent(paymentEnquiryResult.getPostscript()).build());
            } else {
                //转账这些都是留言，不是备注，只有uplan的是备注
                String title = "";
                if (paymentEnquiryResult.isUplanInfo()) {
                    title = mContext.getString(R.string.P209_12_6);
                } else {
                    title = mContext.getString(R.string.UI209_1_2);
                }
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemVisibleLine(true).itemContent(paymentEnquiryResult.getPostscript()).build());
            }
        }
        return isHasRemark;
    }


    /**
     * 分享 保存到本地的图标 除了标题/没有退款条码 金额大小问题 其他一样
     *
     * @param paymentEnquiryResult
     * @param isShareView
     * @return
     */
    public ArrayList<ItemModel> dealDateForBocPay(OrderQueryEntity paymentEnquiryResult, OrderDetailEntity orderDetailEntity, boolean isShareView) {
        transactionIsLiShi = false;
        ArrayList<ItemModel> itemModels = new ArrayList<>();
        if (!isShareView) {
            generateHeaderModel(paymentEnquiryResult, itemModels, orderDetailEntity);
        } else {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
        }

        if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
            transactionIsLiShi = true;
        }

        String trxCurrency = paymentEnquiryResult.getCur();
        String currencyShow = "";
        String showType = "";
        if (TextUtils.isEmpty(paymentEnquiryResult.getOutamtsign())) {
            showType = paymentEnquiryResult.getShowType();
            currencyShow = paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency);
        } else {
            showType = paymentEnquiryResult.getOutamtsign();
            currencyShow = paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency);
        }
        //金额
        if (isShareView) {
            String trxAmt = AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_SPACE + AndroidUtils.formatPriceWithPoint(Double.parseDouble(paymentEnquiryResult.getTrxAmt()), true);
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(mContext.getString(R.string.string_amount)).
                    itemContent(trxAmt).itemContentColor(BocPayTransUtils.getColor(mContext, showType, isShareView)).build());
        } else {
//            String trxAmt = currencyShow + SPECIAL_CHAR + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getTrxAmt(), 2);
            String trxAmt = currencyShow + SPECIAL_CHAR + AndroidUtils.formatPriceWithPoint(Double.parseDouble(paymentEnquiryResult.getTrxAmt()), true);
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(mContext.getString(R.string.string_amount)).itemContent(trxAmt).
                    itemContentColor(BocPayTransUtils.getColor(mContext, showType, isShareView)).build());
        }
        //备注
        generateRemarkModel(paymentEnquiryResult, itemModels);
        String accountStr = getAccountName(orderDetailEntity);
        if (itemModels.size() == 2) {
            itemModels.get(1).setVisibleLine(true);
        }
        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "2")) {
            //收款类型
            //server这边特殊处理 如果列表附言为空 附言从详情中取
            if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript()) && !TextUtils.isEmpty(orderDetailEntity.getRemark())) {
                //前一个条目肯定是金额 下划线要去掉
                itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.P209_12_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getRemark()).build());
            }

            if (isShareView) {
                //保存到本地的要增加一个收款人
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_3)).itemVisibleLine(true).itemContent(orderDetailEntity.getCreditName()).build());
                }
            }
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "1")) {
            //消费类型 商户 账户
            //原价
            if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
                //如果没有备注 但是有原价
                boolean isHas = generateOriginalMoneyModel(paymentEnquiryResult, itemModels, isShareView);
                if (!isHas) {
                    itemModels.get(itemModels.size() - 1).setVisibleLine(true);
                } else {
                    itemModels.get(itemModels.size() - 2).setVisibleLine(false);
                }
            } else {
                //有附言
                boolean isHas = generateOriginalMoneyModel(paymentEnquiryResult, itemModels, isShareView);
                if (!isHas) {
                    //没有原价 需要增加横线
                    itemModels.get(itemModels.size() - 1).setVisibleLine(true);
                }
            }
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(paymentEnquiryResult.getMerchantName()).build());
            if (isShareView) {
                //保存到本地显示 付款人
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                }
            } else {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_7)).itemVisibleLine(true).itemContent(accountStr).build());
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "3")) {
            //转账
            //server这边特殊处理 如果列表附言为空 附言从详情中取
            if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript()) && !TextUtils.isEmpty(orderDetailEntity.getRemark())) {
                //前一个条目肯定是金额 下划线要去掉
                itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.P209_12_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getRemark()).build());
            }

            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_3)).itemContent(orderDetailEntity.getCreditName()).build());
            }
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCrAcNo())) {
                String title = "";
                //0：手机号 1：邮箱
                if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "0")) {
                    title = mContext.getString(R.string.UI209_1_4);
                } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "1")) {
                    title = mContext.getString(R.string.UI209_1_5);
                } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "2")) {
                    title = mContext.getString(R.string.TR1_7_11);
                } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "4")) {
                    title = mContext.getString(R.string.TR1_7_9);
                }
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemContent(orderDetailEntity.getCrAcNo()).build());
            }
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getBankName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_5)).itemContent(orderDetailEntity.getBankName()).build());
            }
            if (isShareView) {
                //保存到本地的应该是付款人
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                }
            } else {
                if (!TextUtils.isEmpty(accountStr)) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_7)).itemVisibleLine(true).itemContent(accountStr).build());
                }
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "8")) {
            //首绑奖赏
            if (isShareView) {
                itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_14_2)).itemVisibleLine(true).itemContent(paymentEnquiryResult.getOuttxtype()).build());
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "5")) {
            //增值
            String accountTitle = mContext.getString(R.string.UI209_15_4);
            if (!TextUtils.isEmpty(paymentEnquiryResult.getSmartAcLevel()) && TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE, paymentEnquiryResult.getSmartAcLevel())) {
                accountTitle = mContext.getString(R.string.UI209_15_3);
            }
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(accountTitle).itemVisibleLine(true).itemContent(paymentEnquiryResult.getAcMainNo()).build());
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "4")) {
            //积分抵消欠账 不显示金额栏位，先移除所有的item 重新组建
            ItemModel headerItemModel = itemModels.get(0);
            itemModels.clear();
            itemModels.add(headerItemModel);
            //积分抵消欠账 抵扣金额，
            String moneyStr = "";
            //积分抵扣 低分抵扣的详情页 要动态显示 积分抵扣信息 和积分抵扣失败的message
            if (TextUtils.isEmpty(paymentEnquiryResult.getOutamtsign())) {
                if (isShareView) {
//                    moneyStr = AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_SPACE + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemAmt(), 2);
                    moneyStr = AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_SPACE + AndroidUtils.formatPriceWithPoint(Double.parseDouble(paymentEnquiryResult.getRedeemAmt()), true);
                } else {
//                    moneyStr = paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_CHAR + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemAmt(), 2);
                    moneyStr = paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_CHAR + AndroidUtils.formatPriceWithPoint(Double.parseDouble(paymentEnquiryResult.getRedeemAmt()), true);
                }

            } else {
                String title = isShareView ? "" : SPECIAL_CHAR;
//                moneyStr = paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency) + (isShareView ? SPECIAL_SPACE : "") + title + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemAmt(), 2);
                moneyStr = paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency) + (isShareView ? SPECIAL_SPACE : "") + title + AndroidUtils.formatPriceWithPoint(Double.parseDouble(paymentEnquiryResult.getRedeemAmt()), true);
            }
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(mContext.getString(R.string.CP1_4a_8)).itemContent(moneyStr).itemContentColor(BocPayTransUtils.getColor(mContext, showType, isShareView)).build());
            //抵扣积分
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_17_3)).itemContent(getGradeInfo(orderDetailEntity.getReduceGp())).build());
            //抵扣积分失败的原因
            if (!TextUtils.isEmpty(paymentEnquiryResult.getRedeemStatus()) && !paymentEnquiryResult.getRedeemStatus().equals("1")) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(paymentEnquiryResult.getRedeemStatusMsg()).itemContentGone(true).itemContentColor(Color.parseColor("#DE0138")).build());
            }
            // 原价
            boolean isHas = generateOriginalMoneyModel(paymentEnquiryResult, itemModels, isShareView);
            if (isHas) {

            } else {
                itemModels.get(itemModels.size() - 1).setVisibleLine(true);
            }
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getMerchantName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(orderDetailEntity.getMerchantName()).build());
            }
            if (isShareView) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
            } else {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_7)).itemVisibleLine(true).itemContent(accountStr).build());
            }

        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "9")) {
            //银行账户转账
            dealLishi(orderDetailEntity, isShareView, itemModels);
        }
        //交易状态
        itemModels.add(new ItemModel.ItemModelBuilder().
                itemType(ItemType.BODY).
                itemTitle(mContext.getString(R.string.UI209_1_8)).
                itemContent(paymentEnquiryResult.getPaymentStatusDesc()).
                build());
        //交易类型
        itemModels.add(new ItemModel.ItemModelBuilder().
                itemType(ItemType.BODY).
                itemTitle(mContext.getString(R.string.UI209_1_9)).
                itemContent(paymentEnquiryResult.getOuttxtype()).
                build());
        //交易时间
        itemModels.add(new ItemModel.ItemModelBuilder().
                itemType(ItemType.BODY).
                itemTitle(mContext.getString(R.string.UI209_1_10)).
                itemContent(AndroidUtils.formatTimeString(paymentEnquiryResult.getTransDate())).
                build());
        //交易编号
        itemModels.add(new ItemModel.ItemModelBuilder().
                itemType(ItemType.BODY).
                itemTitle(mContext.getString(R.string.UI209_1_11)).
                itemVisibleLine(true).
                itemContent(paymentEnquiryResult.getTransactionNum()).
                build());

        if (!paymentEnquiryResult.getPaymentStatus().equals(SUCCESS_INFO_)) {
            //失败类型 server 端 要求 失败类型先从详情里头去取 取不到 再从列表中取
            String outrejType = "";
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getOutrejtype())) {
                outrejType = orderDetailEntity.getOutrejtype();
            } else {
                outrejType = paymentEnquiryResult.getOutrejtype();
            }
            if (!TextUtils.isEmpty(outrejType)) {
                itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_3_9)).itemVisibleLine(true).itemContent(outrejType).build());
            }
        }

        if (!isShareView) {
            //退款的barcode
            generateBarCodeModel(paymentEnquiryResult, itemModels);
            boolean isShow = paymentEnquiryResult.canGradeReduce() && !paymentEnquiryResult.alreadyGradeReduce();
            itemModels.add(new ItemModel.ItemModelBuilder().itemShowGradeReduceBtn(isShow).itemType(ItemType.TAIL).build());
        }
        return itemModels;
    }

    private String getGradeInfo(String gradeInfo) {
        if (TextUtils.isEmpty(gradeInfo)) {
            return "";
        }
        try {
            return AndroidUtils.formatPrice(Double.valueOf(gradeInfo), false);
        } catch (NumberFormatException e) {
        }
        return gradeInfo;
    }

    private String getAccountNameFromList(SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult) {
        if (paymentEnquiryResult == null) return "";
        String accountStr = "";
        //主要是 区分 信用卡支付 还是智能账户 还是支付账户
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPanFour())) {
            if (!TextUtils.isEmpty(paymentEnquiryResult.getCardType())) {
                if (paymentEnquiryResult.getCardType().equals("2")) {
                    if (!TextUtils.isEmpty(paymentEnquiryResult.getSmartAcLevel())) {
                        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE, paymentEnquiryResult.getSmartAcLevel())) {
                            accountStr = AndroidUtils.getSubSmartCardNumberTitleNew(paymentEnquiryResult.getPanFour(), mContext);
                        } else {
                            accountStr = AndroidUtils.getPaymentTitleNew(paymentEnquiryResult.getPanFour(), mContext);
                        }
                    }
                } else {
                    accountStr = AndroidUtils.getMasCardNumberTitle(paymentEnquiryResult.getPanFour(), mContext);
                }
            } else {

            }
        }
        return accountStr;
    }


    private String getAccountName(OrderDetailEntity paymentEnquiryResult) {
        if (paymentEnquiryResult == null) return "";
        String accountStr = "";
        //主要是 区分 信用卡支付 还是智能账户 还是支付账户
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPanFour())) {
            if (!TextUtils.isEmpty(paymentEnquiryResult.getCardType())) {
                if (paymentEnquiryResult.getCardType().equals("2")) {
                    if (!TextUtils.isEmpty(paymentEnquiryResult.getSmartLevel())) {
                        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE, paymentEnquiryResult.getSmartLevel())) {
                            accountStr = AndroidUtils.getSubSmartCardNumberTitleNew(paymentEnquiryResult.getPanFour(), mContext);
                        } else {
                            accountStr = AndroidUtils.getPaymentTitleNew(paymentEnquiryResult.getPanFour(), mContext);
                        }
                    }
                } else {
                    accountStr = AndroidUtils.getMasCardNumberTitle(paymentEnquiryResult.getPanFour(), mContext);
                }
            } else {

            }
        }
        return accountStr;
    }


    //-------------------------------------BocPay交易记录跳转过来数据封装处理----------------------------------------------------//


    /**
     * 特殊处理
     * 增值 转回余额 银行账户转账 防止
     * 1.增值/首绑奖赏  不需要请求后台数据 标题特殊处理 不管是bocpay 还是智能账户 都取列表中txTitle
     * 2.收款 转账 银联消费 标题要动态显示 收款-->付款人 转账-->收款人  银联消费-->商户 其他类型取交易类型字段
     * 3.FPS转账 收款 失败不显示附言-----> getPaymentType 2/3 的时候 判断列表中是否有附言 如果没有从详情中取remark
     * 4.积分抵消签章 适配旧的数据 本来应该都要访问后台，server端反馈旧的记录无法从详情中获取，都从列表中获取
     *
     * @param paymentEnquiryResult
     * @param orderDetailEntity
     * @param isShareView
     * @return
     */
    private ArrayList<ItemModel> dealDateForSmartAccount(SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult, OrderDetailEntity orderDetailEntity, boolean isShareView) {
        transactionIsLiShi = false;
        ArrayList<ItemModel> itemModels = new ArrayList<>();
        String trxCurrency = paymentEnquiryResult.getOuttxccy();
        String trxAmt = "";
        if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
            transactionIsLiShi = true;
        }
        if (isShareView) {
            trxAmt = AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_SPACE + AndroidUtils.formatPriceWithPoint(Double.valueOf(paymentEnquiryResult.getOuttxamt()), true);
        } else {
            trxAmt = paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency) + SPECIAL_CHAR + AndroidUtils.formatPriceWithPoint(Double.valueOf(paymentEnquiryResult.getOuttxamt()), true);
        }

        //金额
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            //备注不为空
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(getTitle(paymentEnquiryResult)).
                    itemContent(trxAmt).itemContentColor(BocPayTransUtils.getColor(mContext, paymentEnquiryResult.getShowType(), isShareView)).build());
        } else {
            //增加下划线
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.MONEY).itemTitle(getTitle(paymentEnquiryResult)).itemVisibleLine(true).
                    itemContent(trxAmt).itemContentColor(BocPayTransUtils.getColor(mContext, paymentEnquiryResult.getShowType(), isShareView)).build());
        }

        //备注
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            String remarkTitle = mContext.getString(R.string.UI209_1_2);
            if (paymentEnquiryResult.isUplanInfo()) {
                remarkTitle = mContext.getString(R.string.P209_12_6);
            }
            if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                remarkTitle = mContext.getString(R.string.UI209_11_2);
            }

            if (!TextUtils.isEmpty(paymentEnquiryResult.getOrderType()) && TextUtils.equals(paymentEnquiryResult.getOrderType(), "NRP")) {
                remarkTitle = mContext.getString(R.string.UI209_11_2);
            }
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(remarkTitle).itemVisibleLine(true).itemContent(paymentEnquiryResult.getPostscript()).build());
        }

        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "2")) {
            //收款类型
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
                //server这边特殊处理 如果列表附言为空 附言从详情中取
                if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript()) && !TextUtils.isEmpty(orderDetailEntity.getRemark())) {
                    //前一个条目肯定是金额 下划线要去掉
                    itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.P209_12_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getRemark()).build());
                }
                //保存到本地的要增加一个收款人
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_3)).itemContent(orderDetailEntity.getCreditName()).build());
                }
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                }
            } else {
                if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity()) && paymentEnquiryResult.getGiftActivity().equals("1")) {
                    //收利是
                    if (TextUtils.equals(paymentEnquiryResult.getOrderType(), "NRP")) {
                        //员工利是
                        itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getOuttxtype()).itemImage(R.mipmap.icon_transfer_redpacket_successful).build());
                    } else {
                        itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getDebitName()).itemImage(R.mipmap.icon_transactions_redpacket).build());
                    }
                    if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                    }
                } else {
                    if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                        itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getDebitName()).itemImage(R.mipmap.icon_transactions_topup).build());
                        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                    }
                }
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "1")) {
            //消费类型
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
            } else {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getMerchantName()).itemImage(R.mipmap.icon_transactions_merchant).build());
            }
            if (isShareView) {
                if (paymentEnquiryResult != null && !TextUtils.isEmpty(paymentEnquiryResult.getMerchantName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(paymentEnquiryResult.getMerchantName()).build());
                }
                //保存到本地显示 付款人
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
                }
            } else {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(paymentEnquiryResult.getMerchantName()).build());
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getPanFour()) && !TextUtils.isEmpty(orderDetailEntity.getCardType())) {
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_7)).itemVisibleLine(true).itemContent(getAccountName(orderDetailEntity)).build());
                }
            }

        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "3")) {
            //转账
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
            } else {
                //server这边特殊处理 如果列表附言为空 附言从详情中取
                if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript()) && !TextUtils.isEmpty(orderDetailEntity.getRemark())) {
                    //前一个条目肯定是金额 下划线要去掉
                    itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.P209_12_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getRemark()).build());
                }

                if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity())) {
                    //派利是
                    if (paymentEnquiryResult.getGiftActivity().equals("1")) {
                        //派利是图标 区分
                        itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getCreditName()).itemImage(R.mipmap.icon_transactions_redpacket).build());
                    } else {
                        itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getCreditName()).itemImage(R.mipmap.icon_transactions_transfer).build());
                    }
                } else {
                    itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getCreditName()).itemImage(R.mipmap.icon_transactions_transfer).build());
                }
            }
            dealLishi(orderDetailEntity, isShareView, itemModels);
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "8")) {
            //首绑奖赏
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
                itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_14_2)).itemVisibleLine(true).itemContent(paymentEnquiryResult.getTxTitle()).build());
            } else {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getTxTitle()).itemImage(R.mipmap.icon_transactions_reward).build());
            }

        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "5") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "10")) {
            //增值
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
            } else {
                if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "6") || TextUtils.equals(paymentEnquiryResult.getPaymentType(), "10")) {
                    itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getTxTitle()).itemImage(R.mipmap.icon_transactions_topup).build());
                } else {
                    itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getOuttxtype()).itemImage(R.mipmap.icon_transactions_cashback).build());
                }
            }
            String accountTitle = mContext.getString(R.string.UI209_15_4);
            if (!TextUtils.isEmpty(paymentEnquiryResult.getSmartAcLevel()) && TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE, paymentEnquiryResult.getSmartAcLevel())) {
                accountTitle = mContext.getString(R.string.UI209_15_3);
            }
            if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "10")) {
                //PA增值
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(accountTitle).itemVisibleLine(true).itemContent(orderDetailEntity.getAcMainNo()).build());
            } else {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(accountTitle).itemVisibleLine(true).itemContent(paymentEnquiryResult.getAcMainNo()).build());
            }
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "4")) {
            //积分抵消欠账
            dealWithGradeInfo(paymentEnquiryResult, orderDetailEntity, isShareView, itemModels);
        } else if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "9")) {
            //银行账户转账
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
            } else {
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
                    itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(orderDetailEntity.getCreditName()).itemImage(R.mipmap.icon_transactions_transfer).build());
                }
            }
            dealLishi(orderDetailEntity, isShareView, itemModels);
        } else {
            //原来的
            if (isShareView) {
                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
            } else {

                itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getOuttxtype()).itemImage(R.mipmap.icon_transactions_merchant).build());
            }
        }

        //交易状态
        if (paymentEnquiryResult.getOuttxsts().equals(SUCCESS_INFO_) || paymentEnquiryResult.getOuttxsts().equals("2") || paymentEnquiryResult.getOuttxsts().equals("S")) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_8)).itemContent(mContext.getString(R.string.UI209_1_14)).build());
        } else {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_8)).itemContent(mContext.getString(R.string.UI209_1_15)).build());
        }

        //交易类型
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_9)).itemContent(paymentEnquiryResult.getOuttxtype()).build());
        //交易时间
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_10)).itemContent(AndroidUtils.formatTimeString(paymentEnquiryResult.getOuttxdate())).build());
        //交易编号
        if (!TextUtils.equals("-1", paymentEnquiryResult.getOuttxrefno())) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_11)).itemVisibleLine(true).itemContent(paymentEnquiryResult.getOuttxrefno()).build());
        }
        //失败类型
        if (paymentEnquiryResult.getOuttxsts().equals(SUCCESS_INFO_) || paymentEnquiryResult.getOuttxsts().equals("2") || paymentEnquiryResult.getOuttxsts().equals("S")) {
        } else {
            //失败类型
            if (paymentEnquiryResult.getOuttxsts().equals(SUCCESS_INFO_) || paymentEnquiryResult.getOuttxsts().equals("2") || paymentEnquiryResult.getOuttxsts().equals("S")) {
            } else {
                //失败类型 server 端 要求 失败类型先从详情里头去取 娶不到 再从列表中取
                String outrejType = "";
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getOutrejtype())) {
                    outrejType = orderDetailEntity.getOutrejtype();
                } else {
                    outrejType = paymentEnquiryResult.getOutrejtype();
                }
                //失败类型   失败类型先从详情里头去取 取不到 再从列表中取
                if (!TextUtils.isEmpty(outrejType)) {
                    itemModels.get(itemModels.size() - 1).setVisibleLine(false);
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_3_9)).itemVisibleLine(true).itemContent(outrejType).build());
                } else {
                }
            }
        }
        if (!isShareView) {
            //分享
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.TAIL).build());
        }

        return itemModels;
    }

    /**
     * 积分抵消
     *
     * @param paymentEnquiryResult
     * @param orderDetailEntity
     * @param isShareView
     * @param itemModels
     */
    private void dealWithGradeInfo(SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult, OrderDetailEntity orderDetailEntity, boolean isShareView, ArrayList<ItemModel> itemModels) {
        if (isShareView) {
            itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemImage(R.mipmap.shareimg).build());
        } else {
            itemModels.add(0, new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(paymentEnquiryResult.getOuttxtype()).itemImage(R.mipmap.icon_transactions_points).build());
        }
        itemModels.get(1).setVisibleLine(false);

        if (orderDetailEntity == null) {
            //适配旧的交易记录 都要从列表中获取
//            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_17_3)).itemContent(getGradeInfo(paymentEnquiryResult.getReduceGp())).itemVisibleLine(true).build());
            if (!TextUtils.isEmpty(paymentEnquiryResult.getMerchantName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(paymentEnquiryResult.getMerchantName()).build());
            }

            if (isShareView) {
//                String title = mContext.getString(R.string.UI209_2_6);
//                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemVisibleLine(true).itemContent(paymentEnquiryResult.getDebitName()).build());
            } else {
                String title = mContext.getString(R.string.UI209_1_7);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemVisibleLine(true).itemContent(getAccountNameFromList(paymentEnquiryResult)).build());
            }
        } else {
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getReduceGp())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_17_3)).itemContent(getGradeInfo(orderDetailEntity.getReduceGp())).itemVisibleLine(true).build());
            }
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getMerchantName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_9_3)).itemContent(orderDetailEntity.getMerchantName()).build());
            }
            if (isShareView) {
                String title = mContext.getString(R.string.UI209_2_6);
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemVisibleLine(true).
                        itemContent(orderDetailEntity.getDebitName()).build());
            } else {
                if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getPanFour()) && !TextUtils.isEmpty(orderDetailEntity.getCardType())) {
                    String title = mContext.getString(R.string.UI209_1_7);
                    itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemVisibleLine(true).
                            itemContent(getAccountName(orderDetailEntity)).build());
                }
            }
        }
    }

    private String getTitle(SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult) {
        if (TextUtils.equals(paymentEnquiryResult.getPaymentType(), "4")) {
            return mContext.getString(R.string.CP1_4a_8);
        } else {
            return mContext.getString(R.string.UI209_1_1);
        }
    }

    private void dealLishi(OrderDetailEntity orderDetailEntity, boolean isShareView, ArrayList<ItemModel> itemModels) {
        if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCreditName())) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_3)).itemContent(orderDetailEntity.getCreditName()).build());
        }
        if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getCrAcNo())) {
            String title = "";
            //0：手机号 1：邮箱
            if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "0")) {
                title = mContext.getString(R.string.UI209_1_4);
            } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "1")) {
                title = mContext.getString(R.string.UI209_1_5);
            } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "2")) {
                title = mContext.getString(R.string.UI209_5_4);
            } else if (TextUtils.equals(orderDetailEntity.getCrAcTp(), "4")) {
                title = mContext.getString(R.string.TR1_7_9);
            }
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(title).itemContent(orderDetailEntity.getCrAcNo()).build());
        }
        if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getBankName())) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_5)).itemContent(orderDetailEntity.getBankName()).build());
        }
        if (isShareView) {
            //保存到本地的应该是付款人
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getDebitName())) {
                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_2_6)).itemVisibleLine(true).itemContent(orderDetailEntity.getDebitName()).build());
            }
        } else {
            //详情页应该是收款人
            if (orderDetailEntity != null && !TextUtils.isEmpty(orderDetailEntity.getPanFour()) && !TextUtils.isEmpty(orderDetailEntity.getCardType())) {
                String accountStr = getAccountName(orderDetailEntity);

                itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.UI209_1_7)).itemVisibleLine(true).
                        itemContent(accountStr).build());
            }
        }
    }


}
