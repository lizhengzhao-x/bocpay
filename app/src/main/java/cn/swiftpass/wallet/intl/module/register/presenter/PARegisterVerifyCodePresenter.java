package cn.swiftpass.wallet.intl.module.register.presenter;

import android.content.Context;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.ErrorEntity;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeEntity;
import cn.swiftpass.wallet.intl.module.register.contract.PARegisterVerifyCodeContract;
import cn.swiftpass.wallet.intl.module.register.model.PARegisterVerifyCodeModel;

public class PARegisterVerifyCodePresenter extends BasePresenter<PARegisterVerifyCodeContract.View> implements PARegisterVerifyCodeContract.Presenter {

    /**
     * 获取PA用户注册界面验证码
     *
     * @param context
     */
    @Override
    public void getPARegisterVerifyCode(final Context context) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        } else {
            return;
        }

        PARegisterVerifyCodeModel.getPARegisterVerifyCode("PAREG", new NetWorkCallbackListener<PARegisterVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPARegisterVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(PARegisterVerifyCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPARegisterVerifyCodeSuccess(response);
                }
            }
        });

    }

    /**
     * 检查PA用户注册界面验证码
     *
     * @param context
     * @param mobile
     * @param verifyCode
     */
    @Override
    public void checkPARegisterVerifyCode(final Context context, String mobile, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        } else {
            return;
        }

        PARegisterVerifyCodeModel.checkPARegisterVerifyCode(mobile, verifyCode, "PAREG", new NetWorkCallbackListener<PARegisterVerifyCodeCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkPARegisterVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(PARegisterVerifyCodeCheckEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkPARegisterVerifyCodeSuccess(response);
                }
            }
        });
    }
}
