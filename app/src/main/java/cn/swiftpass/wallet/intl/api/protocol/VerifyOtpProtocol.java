package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

import static cn.swiftpass.wallet.intl.entity.ApiConstant.FLOW_TRANSFER_CROSS_BORDER;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 校验信用卡OTP接口，返回对象为 SendOTPEntity
 */

public class VerifyOtpProtocol extends BaseProtocol {
    public static final String TAG = VerifyOtpProtocol.class.getSimpleName();

    /**
     * 用户id
     */
    String walletId;

    String mobile;
    /**
     * 标识取【L】：R：注册；V：仅验证；F：忘记密码，L：登录；
     */
    String mAction;
    /**
     * 短信验证码
     */
    String mOtp;


    public VerifyOtpProtocol(String walletId, String mobile, String action, String otp, NetWorkCallbackListener dataCallback) {
        this.walletId = walletId;
        this.mAction = action;
        this.mobile = mobile;
        this.mOtp = otp;
        this.mDataCallback = dataCallback;

        if (FLOW_TRANSFER_CROSS_BORDER.equals(mAction)) {
            mUrl = "api/crossTransfer/verifyOTP";
        } else {
            mUrl = "api/otp/check";
        }
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, walletId);
        mBodyParams.put(RequestParams.MOBILE, mobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mOtp);

    }
}
