package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;

public class SmartAccountManagerContract {
    public interface View extends IView {


        void getRegEddaInfoError(int action, String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(int action, GetRegEddaInfoEntity response);


        void checkSmartAccountBindError(String errorCode, String errorMsg);

        void checkSmartAccountBindSuccess(ContentEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(MySmartAccountEntity response);

        void updateSmartAccountSuspendError(String errorCode, String errorMsg);

        void updateSmartAccountSuspendSuccess(BaseEntity response);

        void updateSmartAccountCancelSuccess(SmartAccountUpdateEntity response);

        void updateSmartAccountCancelError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<SmartAccountManagerContract.View> {


        void getRegEddaInfo(int action);

        void checkSmartAccountBind(boolean isShowLoading);

        void getSmartAccountInfo();

        /**
         * 暂停 激活我的账户更新ui
         */
        void updateSmartAccountSuspend();

        /**
         * 取消我的账户
         */
        void updateSmartAccountCancel();
    }
}
