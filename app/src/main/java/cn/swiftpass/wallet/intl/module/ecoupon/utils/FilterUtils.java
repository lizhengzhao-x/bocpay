package cn.swiftpass.wallet.intl.module.ecoupon.utils;

import android.app.Activity;
import android.view.View;

import java.util.List;

import cn.swiftpass.wallet.intl.dialog.FilterTypePopWindow;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;

/**
 * Created by ZhangXinchao on 2019/8/13.
 */
public class FilterUtils {

    public static void initFilterPopView(Activity mActivity, View dropDownView, List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntitiesIn, final OnFilterConfimListener onFilterConfimListener) {
        FilterTypePopWindow filterTypePopWindow = new FilterTypePopWindow(mActivity, dropDownView.getWidth(), menuFilterEntitiesIn, new FilterTypePopWindow.OnFilterSelListener() {
            @Override
            public void onFilterSelConfirm(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities) {
                onFilterConfimListener.onFilterConfrim(menuFilterEntities);
            }
        });
        filterTypePopWindow.showAsDropDown(dropDownView);
    }

    public interface OnFilterConfimListener {
        void onFilterConfrim(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities);
    }
}
