package cn.swiftpass.wallet.intl.utils;

import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;


/**
 * emv二维码解析
 */
public class ParseQRUtils {

    private static final String TAG = "ParseQRUtils";

    public static HashMap<String, String> parseEMV(String content) {
        HashMap<String, String> maps = new HashMap<>();
//        content = "000201010211153125000344000203441000000000006665204074353037025802CN5908leungbun6002SZ62600120000000000000000000000520000000000000000000000708000800086304E14A";
        while (!TextUtils.isEmpty(content)) {
            try {
                if (content.length() < 2) {
                    return null;
                }
                String key = content.substring(0, 2);
                if (content.length() < 4) {
                    return null;
                }
                String lengthStr = content.substring(2, 4);
                int length = Integer.valueOf(lengthStr);
                if (content.length() < 4 + length) {
                    return null;
                }
                String value = content.substring(4, 4 + length);
                maps.put(key, value);
                content = content.substring(4 + length);
            } catch (NumberFormatException e) {
                LogUtils.i(TAG, Log.getStackTraceString(e));
                content = null;
            }
        }
        if (content == null) {
            return null;
        }
        return maps;
    }

}
