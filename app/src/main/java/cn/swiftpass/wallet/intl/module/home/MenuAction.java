package cn.swiftpass.wallet.intl.module.home;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.home
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/12 17:27
 * @change
 * @chang time
 * @class describe
 */
public interface MenuAction {
    String ACTION_SEE_DATA = "ACTION_SEE_DATA";
    String ACTION_SEE_POINT = "ACTION_SEE_POINT";
    String ACTION_OPEN_VIRTUAL_CARD = "ACTION_OPEN_VIRTUAL_CARD";
    String ACTION_BIND_CARD_IN_HOME = "ACTION_BIND_CARD_IN_HOME";
    String ACTION_BIND_CARD = "ACTION_BIND_CARD";
    String ACTION_BIND_SMART_ACCOUNT = "ACTION_BIND_SMART_ACCOUNT";

    String ACTION_UPDATE_ACCOUNT = "ACTION_UPDATE_ACCOUNT";
    String ACTION_TRANSFER_BY_FPS = "ACTION_TRANSFER_BY_FPS";
    String TRANSFER_BY_STATIC_CODE = "TRANSFER_BY_STATIC_CODE";
}
