package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;

public class EveryYearCreditCardRewardFragment extends MyCreditCardRewardBaseFragment {

    public EveryYearCreditCardRewardFragment() {
        super(CreditRewardType.REWARD_DATE_TYPE_EVERY_YEAR);
    }
}