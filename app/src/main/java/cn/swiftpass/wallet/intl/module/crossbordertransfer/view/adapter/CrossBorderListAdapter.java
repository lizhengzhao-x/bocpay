package cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.CustomeLineText;

public class CrossBorderListAdapter extends RecyclerView.Adapter {
    private int shareResourceId[] = {R.mipmap.icon_app_whatsapp, R.mipmap.icon_app_wechat, R.mipmap.icon_app_line, R.mipmap.icon_app_other};
    private Context mContext;
    private List<ItemModel> mList;
    private LayoutInflater mLayoutInflater;
    private OnItemShareListener onItemShareListener;

    public CrossBorderListAdapter(Context mContext, OnItemShareListener onItemShareListenerIn) {
        this.mContext = mContext;
        mList = new ArrayList<>();
        mLayoutInflater = LayoutInflater.from(mContext);
        this.onItemShareListener = onItemShareListenerIn;
    }


    @Override
    public int getItemViewType(int position) {
        ItemModel itemModel = mList.get(position);
        int itemType = itemModel.getItemType();
        return itemType;
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ItemType.HEAD) {
            View view = View.inflate(mContext, R.layout.cross_list_item1, null);
            return new HeaderViewHolder(view);
        } else if (viewType == ItemType.TAIL) {
            View view = View.inflate(mContext, R.layout.cross_list_item4, null);
            return new TailViewHolder(view);
        } else {
            View view = View.inflate(mContext, R.layout.cross_list_item2, null);
            return new BodyViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        ItemModel itemModel = mList.get(position);
        boolean visibleLine = itemModel.isVisibleLine();

        switch (itemViewType) {
            case ItemType.HEAD:
                HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
                headerHolder.leftImage.setImageResource(itemModel.getResourceImg());
                headerHolder.content.setText(itemModel.getItemTitle());
                break;
            case ItemType.BODY:
                BodyViewHolder bodyViewHolder = (BodyViewHolder) holder;
                bodyViewHolder.clt.setContentText(itemModel.getItemContent());
                bodyViewHolder.clt.setTitleText(itemModel.getItemTitle());
                bodyViewHolder.mBodyLine.setVisibility(visibleLine ? View.VISIBLE : View.GONE);
                break;
            case ItemType.TAIL:
                break;
        }
    }


    public void refresh(List<ItemModel> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public ImageView leftImage;
        public TextView content;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            leftImage = itemView.findViewById(R.id.id_cross_border_header_iv);
            content = itemView.findViewById(R.id.id_cross_border_header_txt);

        }
    }

    private class TailViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout shareView;
        public TextView mSaveToLocal;

        public TailViewHolder(@NonNull View itemView) {
            super(itemView);
            shareView = itemView.findViewById(R.id.ll_share_item);
            mSaveToLocal = itemView.findViewById(R.id.save_detail_tv);
            initShareButtonGroup(shareView);
            mSaveToLocal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemShareListener != null) {
                        onItemShareListener.OnItemShare(-1);
                    }
                }
            });
        }
    }

    private class BodyViewHolder extends RecyclerView.ViewHolder {
        public CustomeLineText clt;
        public View mBodyLine;

        public BodyViewHolder(@NonNull View itemView) {
            super(itemView);
            clt = itemView.findViewById(R.id.clt);
            mBodyLine = itemView.findViewById(R.id.body_line);
        }
    }

    private void initShareButtonGroup(LinearLayout shareView) {
        int imageWidth = AndroidUtils.dip2px(mContext, 45);
        int itemMargin = (AndroidUtils.getScreenWidth(mContext) - imageWidth * 4) / 5;
        for (int i = 0; i < shareResourceId.length; i++) {
            ImageView imageView = new ImageView(mContext);
            imageView.setImageResource(shareResourceId[i]);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageWidth, imageWidth);
            layoutParams.leftMargin = itemMargin;
            imageView.setLayoutParams(layoutParams);
            final int finalI = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareImageWithPosition(finalI);
                }
            });
            shareView.addView(imageView);
        }
    }


    /**
     * 具体采用哪种方式分享
     *
     * @param positon
     */
    private void shareImageWithPosition(final int positon) {

        if (onItemShareListener != null) {
            onItemShareListener.OnItemShare(positon);
        }

    }

    public interface OnItemShareListener {

        void OnItemShare(int position);
    }


}
