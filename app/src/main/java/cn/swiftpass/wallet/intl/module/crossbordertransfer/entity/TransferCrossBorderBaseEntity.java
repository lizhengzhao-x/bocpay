package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.PayeeEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 20:01
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderBaseEntity extends BaseEntity {


    /**
     * txFirst : 0
     * exchangeRate : 0.6
     * lastUpdatedRate : 2018/11/11 11:11
     * availableLimit : 1000
     * currency : HKD
     * customerNotice : http://abc.123.com
     * forUsed : ["购物","旅行","医疗"]
     * accountList : [{"cardId":"1","pan":"13545","panFour":"3545","cardType":"2"},{"cardId":"2","pan":"325253545","panFour":"3545","cardType":"1"}]
     * self : {"enName":"lisi","cardNo":"123124","bankName":"招商银行"}
     * recent : [{"enName":"lisi","enHideName":"li**","cardNo":"123124","bankName":"招商银行","txTime":"2018-11-11 11:11:11","txAmount":"1500"},{"enName":"zhangs**","cardNo":"335123124","bankName":"工商银行","txTime":"2018-11-11 11:11:11","txAmount":"1500"}]
     */

    public String CNY2HKDRate = "0";//1港币等于多少人民币
    public String HKD2CNYRate = "0";//1人民币等于多少港币

    public String refNo;
    public String dailyCrosstransferLimit;
    public String lastUpdatedRate;

    public String availableLimitCur;
    public String dailyCrosstransferLimitCur;
    public String currency;
    public String tncUrl;
    public String currencyRMB;
    public String customerNotice;//客户需知url
    public String feeDescription;//手续费说明url
    public PayeeEntity self;
    public ArrayList<TransferCrossBorderUsedEntity> forUsed = new ArrayList<>();
    public ArrayList<TransferCrossBorderAccountListEntity> accountList = new ArrayList<>();
    public List<TransferCrossBorderRecordEntity> recent = new ArrayList<>();


}
