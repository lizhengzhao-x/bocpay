package cn.swiftpass.wallet.intl.module.scan;

import android.app.Activity;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.ecoupon.view.CheckEcouponsDetailPop;
import cn.swiftpass.wallet.intl.module.transfer.StaticCodeUtils;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;

import static cn.swiftpass.wallet.intl.module.transfer.view.TransferConfirmMoneyActivity.TRANSFER_APPCALLAPP_PAYMENT;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEFROM;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEMONEY;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGE_COLLECTION;

/**
 * Created by ZhangXinchao on 2019/10/29.
 */
public class GradeReduceSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.ll_line_message)
    LinearLayout mIl_line_message;
    @BindView(R.id.id_rel_money_reduce)
    RelativeLayout idRelMoneyReduce;
    @BindView(R.id.id_rel_grade_reduce)
    RelativeLayout idRelGradeReduce;
    @BindView(R.id.id_linear_grade_retain)
    LinearLayout idLinearGradeRetain;
    @BindView(R.id.id_payment_status)
    TextView idPaymentStatus;
    @BindView(R.id.id_tv_ok)
    TextView mNextBtn;
    @BindView(R.id.tv_grade_retain)
    TextView idTvGradeRetain;
    @BindView(R.id.id_grade_reduce)
    TextView idTvGradeReduce;
    @BindView(R.id.id_grade_reduct)
    TextView idGradeRedect;
    @BindView(R.id.id_jifen_status_error)
    TextView idGradeStatusError;
    @BindView(R.id.id_status_image)
    ImageView idStatusImage;
    @BindView(R.id.id_jifen_status_success)
    Button idGradeStatusSuccess;
    @BindView(R.id.id_reduce_smart_account)
    RelativeLayout idReduceSmartAccount;
    @BindView(R.id.ll_line_oringinal)
    LinearLayout ll_line_oringinal;
    @BindView(R.id.ll_line_grade)
    LinearLayout ll_line_grade;

    @BindView(R.id.id_rel_discount)
    RelativeLayout relOriginalView;
    @BindView(R.id.id_grade_reduct_credit)
    TextView idGradeReductCredit;
    @BindView(R.id.ll_postscript)
    LinearLayout ll_postscript;
    @BindView(R.id.tv_postscript)
    TextView tvPostscript;
    @BindView(R.id.id_send_msg_img)
    ImageView idSendMsgImg;

    //原价
    @BindView(R.id.tv_discount)
    TextView tvOriginalMoney;
    @BindView(R.id.id_smart_type)
    TextView idSmartAccountType;
    @BindView(R.id.id_grade_reduct_smart)
    TextView idGradeReductSmart;
    @BindView(R.id.id_reduce_credit)
    RelativeLayout idReduceCredit;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_content)
    TextView tvMoney;
    @BindView(R.id.tv_card)
    TextView tvCard;

    @BindView(R.id.tv_staticCode)
    TextView tvStaticCode;

    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_send_msg_view)
    View idSendMsgView;
    @BindView(R.id.id_jifen_send_message)
    TextView idJiFenSendMessage;
    @BindView(R.id.id_bannber_detail)
    ImageView idBannerDetail;

    /**
     * 付款金额下划线
     */
    @BindView(R.id.ll_line_discount)
    LinearLayout lineTopView;


    //支付成功 结果详情
    private PaymentEnquiryResult paymentEnquiryResult;
    private int currentPaymentType;

    public static void startActivityFromBackScan(Activity activity, SweptCodePointEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.URLQRINFO_ENTITY, response.convertBySweptCodePointEntity());
        mHashMaps.put(Constants.IS_MAJOR_SCAN, false);
        ActivitySkipUtil.startAnotherActivity(activity, GradeReduceSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startActivityFromOrderDetail(Activity activity, SweptCodePointEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.URLQRINFO_ENTITY, response.convertBySweptCodePointEntity());
        mHashMaps.put(Constants.IS_MAJOR_SCAN, false);
        mHashMaps.put(Constants.IS_FROM_ORDERDETAIL, true);
        ActivitySkipUtil.startAnotherActivity(activity, GradeReduceSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    /**
     * 主扫成功 直接跳转 积分抵扣页面
     *
     * @param activity
     * @param paymentEnquiryResult
     */
    public static void startActivityFromMajorScan(Activity activity, PaymentEnquiryResult paymentEnquiryResult) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.URLQRINFO_ENTITY, paymentEnquiryResult);
        mHashMaps.put(Constants.IS_MAJOR_SCAN, true);
        ActivitySkipUtil.startAnotherActivity(activity, GradeReduceSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startActivityFromAppCallApp(Activity activity, PaymentEnquiryResult paymentEnquiryResult) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.URLQRINFO_ENTITY, paymentEnquiryResult);
        mHashMaps.put(Constants.IS_MAJOR_SCAN, true);
        mHashMaps.put(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
        mHashMaps.put(Constants.MERCHANT_LINK, paymentEnquiryResult.getAppLink());
        mHashMaps.put(Constants.APPCALLAPP_IS_H5, paymentEnquiryResult.isAppCallAppFromH5());
        ActivitySkipUtil.startAnotherActivity(activity, GradeReduceSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    @Override
    public void init() {
        setToolBarTitle(R.string.CCP2105_3_1);
        hideBackIcon();
        if (null == getIntent() || getIntent().getExtras() == null) {
            finish();
            return;
        }
        try {
            paymentEnquiryResult = (PaymentEnquiryResult) getIntent().getExtras().getSerializable(Constants.URLQRINFO_ENTITY);
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (paymentEnquiryResult == null) {
            finish();
            return;
        }
        //商户名称
        tvName.setText(paymentEnquiryResult.getMerchantName());
        currentPaymentType = getIntent().getExtras().getInt(Constants.TYPE);
        //支付的卡号后四位
        tvCard.setText(AndroidUtils.getSubMasCardNumberTitle(mContext, paymentEnquiryResult.getPanFour(), paymentEnquiryResult.getCardType()));
        //支付的币种符号
        idPaySymbol.setText(AndroidUtils.getTrxCurrency(paymentEnquiryResult.getTrxCurrency()));
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            ll_postscript.setVisibility(View.VISIBLE);
            tvPostscript.setText(paymentEnquiryResult.getPostscript());
        } else {
            ll_postscript.setVisibility(View.GONE);
        }
        updateDiscountInfo(paymentEnquiryResult.getDiscount(), paymentEnquiryResult.getTrxCurrency(), paymentEnquiryResult.getOriginalAmt());
        boolean isMajorScan = getIntent().getExtras().getBoolean(Constants.IS_MAJOR_SCAN);
        tvMoney.setText(BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getTrxAmt(), 2));
        if (isMajorScan) {
            idSendMsgView.setVisibility(View.VISIBLE);
            idSendMsgImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = "";
                    if (paymentEnquiryResult == null) return;
                    if (!TextUtils.isEmpty(paymentEnquiryResult.getCardType()) && paymentEnquiryResult.getCardType().equals("1")) {
                        message = getString(R.string.EPTNC_CC_01);
                    } else {
                        message = getString(R.string.EPTNC_AC_01);
                    }
                    showEcouponCountDetail(message);
                }
            });
        }
        updateReduceGradeInfo(paymentEnquiryResult, isMajorScan);
        initListener();
        showDetailBanners();
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            mNextBtn.setText(getResources().getString(R.string.ACA2101_0_4));
        }
        tvStaticCode.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                checkBindSmartAccount();
            }
        });
        //从交易记录过来的页面 应该是返回
        boolean isFromOrderDetail = getIntent().getBooleanExtra(Constants.IS_FROM_ORDERDETAIL, false);
        if (isFromOrderDetail) {
            tvStaticCode.setVisibility(View.GONE);
            mNextBtn.setText(getResources().getString(R.string.CCP2105_5_13));
        } else {
            tvStaticCode.setVisibility(View.VISIBLE);
        }
    }

    private void checkBindSmartAccount() {
        StaticCodeUtils.checkBindCard(this, new StaticCodeUtils.BindSmartAccountCallBack() {
            @Override
            public void onBindSmartAccountSuccess() {

                //先通知关闭其他转账流程的页面
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(PAGEFROM, PAGE_COLLECTION);
                mHashMaps.put(PAGEMONEY, paymentEnquiryResult.getTrxAmt());
                ActivitySkipUtil.startAnotherActivity(GradeReduceSuccessActivity.this, TransferSetLimitActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    /**
     * 被扫成功之后 显示积分获取详情
     *
     * @param titleDetail
     */
    private void showEcouponCountDetail(String titleDetail) {
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), "", titleDetail);
        checkEcouponsDetailPop.show();
    }


    private void showDetailBanners() {
        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {
                updateBannerView();
            }
        });
    }


    private void updateBannerView() {
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
        if (initBannerEntity != null) {
            BannerImageEntity pTwoPBean = initBannerEntity.getPTwoM(this);
            // 0 是不显示 1 利利是 2 邀请
            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                idBannerDetail.setVisibility(View.VISIBLE);
                String imageUrl = pTwoPBean.getImagesUrl();
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idBannerDetail.getLayoutParams();
                int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(this, 15) * 2;
                // 375 85
                int height = width * 85 / 375;
                lp.width = width;
                lp.height = height;
                idBannerDetail.setLayoutParams(lp);
                if (imageUrl.endsWith(".gif")) {
                    GlideApp.with(this).asGif().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).load(imageUrl).into(idBannerDetail);
                } else {
                    GlideApp.with(this).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(idBannerDetail);
                }
            }
        }
    }

    private void initListener() {
        mNextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
                    String merchantLink = getIntent().getStringExtra(Constants.MERCHANT_LINK);
                    boolean isFromH5 = getIntent().getBooleanExtra(Constants.APPCALLAPP_IS_H5, false);
                    ActivitySkipUtil.startAnotherActivity(GradeReduceSuccessActivity.this, MainHomeActivity.class);
                    AppCallAppUtils.goToMerchantApp(GradeReduceSuccessActivity.this, merchantLink, isFromH5);
                } else {
                    EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
                finish();
            }
        });
    }


    private void updateReduceGradeInfo(PaymentEnquiryResult paymentEnquiryResult, boolean isMajorScan) {
        mNextBtn.setText(getResources().getString(R.string.CP1_6c_10));
        //被扫成功界面显示“付款成功”，再点击 积分抵销 ，不涉及付款，应该为“抵销成功”
        //主扫成功无论是否用积分，都显示“付款成功”
        if (isMajorScan) {
            idPaymentStatus.setText(getString(R.string.CP1_7_2));
        } else {
            idPaymentStatus.setText(getString(R.string.CP1_6c_3));
        }
        if (paymentEnquiryResult.isPayWithPoint()) {
            //TODO 这里下划线问题要处理
            //如果有积分，需要显示付款金额下边的下划线
            lineTopView.setVisibility(View.VISIBLE);
            //使用了积分抵扣 这些ui布局需要展示
            mIl_line_message.setVisibility(View.VISIBLE);
            idRelMoneyReduce.setVisibility(View.VISIBLE);
            idRelGradeReduce.setVisibility(View.VISIBLE);
            idLinearGradeRetain.setVisibility(View.VISIBLE);
            //剩余积分
            idTvGradeRetain.setText(AndroidUtils.formatPrice(Double.valueOf(paymentEnquiryResult.getGpBalance()), false));
            //积分抵扣了多少金额
            idTvGradeReduce.setText(AndroidUtils.getTrxCurrency(paymentEnquiryResult.getTrxCurrency()) + " " + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemGpTnxAmt(), 2));
            //抵扣了多少积分
            idGradeRedect.setText(AndroidUtils.formatPrice(Double.valueOf(paymentEnquiryResult.getRedeemCount()), false));
            if (paymentEnquiryResult.isPayWithPointSuccess()) {
                //积分抵扣失败
                if (!isMajorScan) {
                    //被扫需要更换标题 更换图标
                    idPaymentStatus.setText(getString(R.string.CP2_6d_1));
                    idStatusImage.setBackgroundResource(R.mipmap.icon_result_unsuccessful);
                }
                //主扫被扫都需要显示图片
                idGradeStatusError.setVisibility(View.VISIBLE);
                idGradeStatusError.setText(paymentEnquiryResult.getRedeemStatusMsg());
            }


            //需求更改 只有信用卡界面才显示这个字段
            if (paymentEnquiryResult.getCardType().equals("1")) {
                idGradeStatusSuccess.setVisibility(View.VISIBLE);
                idGradeStatusSuccess.setTextColor(getColor(R.color.order_detail_gray_color));
            }
            idTvGradeReduce.setTextColor(paymentEnquiryResult.getColorSign());
            showGradeReduceUiDetail(paymentEnquiryResult.getSmaUsedGp(), paymentEnquiryResult.getCcUsedGP(), paymentEnquiryResult.getSmaType());
        } else {
            setToolBarTitle(R.string.CP1_4a_1);
            if (!TextUtils.isEmpty(paymentEnquiryResult.getDiscount()) && !paymentEnquiryResult.getDiscount().equals("0")) {
                ll_line_grade.setVisibility(View.GONE);
            }
            //没有进行积分抵扣
            if (isMajorScan) {
            } else {
                if (ll_line_oringinal.isShown()) {
                    //原价的横线如果显示
                    //显示积分 收款人之间的横线
                    ll_line_grade.setVisibility(View.VISIBLE);
                }

            }
        }
        if (relOriginalView.getVisibility() == View.VISIBLE) {
            lineTopView.setVisibility(View.GONE);
        }
    }


    /**
     * 判断这笔订单是否有优惠信息 如果有 需要显示折扣信息
     *
     * @param discount
     * @param currency
     * @param originalAmt
     */
    private void updateDiscountInfo(String discount, String currency, String originalAmt) {
        if (!TextUtils.isEmpty(discount)) {
            //优惠信息
            try {
                if (Double.valueOf(discount) > 0) {
                    relOriginalView.setVisibility(View.VISIBLE);
                    tvOriginalMoney.setText(AndroidUtils.getTrxCurrency(currency) + " " + BigDecimalFormatUtils.forMatWithDigs(originalAmt, 2));
                    tvOriginalMoney.getPaint().setAntiAlias(true);
                    tvOriginalMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
//                    //显示 原价与金额之间的分割线
                    ll_line_oringinal.setVisibility(View.VISIBLE);
                }
            } catch (NumberFormatException e) {
            }
        }
    }

    /**
     * 扣取积分明细(信用卡  智能账户)
     *
     * @param smartUsedGp
     * @param ccUsedGp
     * @param smartType
     */
    private void showGradeReduceUiDetail(String smartUsedGp, String ccUsedGp, String smartType) {

        if (!TextUtils.isEmpty(smartUsedGp) && !TextUtils.equals("0", smartUsedGp)) {
            //智能账户 积分抵扣
            idReduceSmartAccount.setVisibility(View.VISIBLE);
            idSmartAccountType.setText(smartType.equals("2") ? getString(R.string.payment_account) : getString(R.string.smart_account_old));
            idGradeReductSmart.setText(AndroidUtils.formatPrice(Double.valueOf(smartUsedGp), false));
        }
        if (!TextUtils.isEmpty(ccUsedGp) && !TextUtils.equals("0", ccUsedGp)) {
            //信用卡 积分抵扣
            idReduceCredit.setVisibility(View.VISIBLE);
            idGradeReductCredit.setText(AndroidUtils.formatPrice(Double.valueOf(ccUsedGp), false));
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_grade_reduce_success;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            TempSaveHelper.clearLink();
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
