package cn.swiftpass.wallet.intl.app;

import android.app.Activity;
import android.os.Handler;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.AndroidKeyStoreUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem;
import cn.swiftpass.wallet.intl.entity.event.LoginEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisImplManager;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.entity.Constants.ACTION_BASE_ERROR;

public class HttpInitUtils {
    public static void init(Activity mContext) {
        HttpCoreKeyManager.getInstance().initHttpCallBack(mContext,
                R.string.connect_time_out,
                R.string.string_network_available,
                R.string.fio_reg_fail,
                R.string.fio_auth_fail,
                R.string.fio_dereg_fail,
                new HttpCoreKeyManager.HttpRequestCallBack() {
                    @Override
                    public boolean onRequestCallBack(String errorCode, String errorMsg) {
                        if (checkErrorPasscode(errorCode)) {
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            goMainPage(errorMsg);
//                            if (CacheManagerInstance.getInstance().checkLoginStatus()) {
//                                goMainPage(errorMsg);
//                            } else {
//                                goLoginPage(errorMsg);
//                            }
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.CARD_INFO_HAD_REG.code)) {
                            //重复绑卡回到登录页面
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            if (CacheManagerInstance.getInstance().checkLoginStatus()) {
                                goMainPage(errorMsg);
                            } else {
                                goLoginPage(errorMsg);
                            }
                            return true;
                        } else if (checkErrorCodeLogin(errorCode)) {
                            //处理设备登录session有效期失败 清除session 点击退出到登录页面
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            try {
                                BaseAbstractActivity baseActivity = (BaseAbstractActivity) MyActivityManager.getInstance().getCurrentActivity();
                                if (baseActivity != null) {
                                    baseActivity.dismissDialog();
                                }
                            } catch (Exception e) {
                                if (BuildConfig.isLogDebug) {
                                    e.printStackTrace();
                                }
                            }
                            goLoginPage(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.RELOGIN.code)) {
                            //长时间未操作
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            startMainPage(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.SERVER_VERUFT_PWD_AGAIN.code) || TextUtils.equals(errorCode, ErrorCode.SERVER_DEVICE_OTHER_LOGIN.code)) {
                            //校验密码错误5次 账号被锁 强制退出
                            if (MyActivityManager.getInstance().getCurrentActivity() instanceof LoginActivity) {
                                MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                                DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                                    @Override
                                    public void onBtnClickListener() {
                                        EventBus.getDefault().postSticky(new LoginEventEntity(LoginEventEntity.EVENT_KEY_DISMISS_PWD, ""));
                                    }
                                });
                                return true;
                            } else {
                                MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                                goLoginPage(errorMsg);
                                return true;
                            }
                        } else if ((TextUtils.equals(errorCode, ErrorCode.SERVER_PUBLIC_KEY_INVALID.code)) || (TextUtils.equals(errorCode, ErrorCode.SERVER_PUBLIC_KEY_AGAIN.code))) {
                            //key失效，重新获取
                            AndroidKeyStoreUtils.clearLocalKey();
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            goLoginPage(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.SERVER_PUBKEY_ERROR.code)) {
                            //key失效，重新获取
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            goLoginPage(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.OTP_FAILED_FIVE.code)) {
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            showStartMainPageForLogin(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.SERVER_STWO_REGISTER.code) || TextUtils.equals(errorCode, ErrorCode.SERVER_PUBKEY_ERROR_DATA.code) || TextUtils.equals(errorCode, ErrorCode.BOUNCE_BACK.code)) {
                            MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                            AnalysisImplManager.sendErrorEvent(MyActivityManager.getInstance().getCurrentActivity(), System.currentTimeMillis(), errorCode, errorMsg);
                            goLoginPage(errorMsg);
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.REGISTER_TIME_OUT.code)) {
                            //pa注册 需求 pa注册 绑卡 中如果存在流程控制的超时错误 退出界面也有要求 埋点也要上传
                            AnalysisImplManager.sendErrorEvent(MyActivityManager.getInstance().getCurrentActivity(), System.currentTimeMillis(), errorCode, errorMsg);
                            DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                                @Override
                                public void onBtnClickListener() {
                                    MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                                    MyActivityManager.removeAllTaskWithRegister();
                                }
                            });
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.PA_BIND_TIME_OUT.code)) {
                            //pa绑卡
                            AnalysisImplManager.sendErrorEvent(MyActivityManager.getInstance().getCurrentActivity(), System.currentTimeMillis(), errorCode, errorMsg);
                            DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                                @Override
                                public void onBtnClickListener() {
                                    MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                                    MyActivityManager.removeAllTaskForBindCardStack();
                                }
                            });
                            return true;
                        } else if (TextUtils.equals(errorCode, ErrorCode.PA_FORGETPWD_TIME_OUT.code)) {
                            //pa忘记密码
                            DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                                @Override
                                public void onBtnClickListener() {
                                    MyActivityManager.getInstance().getCurrentActivity().dismissDialog();
                                    MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                                }
                            });

                            return true;
                        }

                        return false;
                    }

                    @Override
                    public void startLoginPage(String errorMsg) {
                        DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                            @Override
                            public void onBtnClickListener() {
                                AndroidUtils.goPreLoginPage();
                                AndroidUtils.clearMemoryCacheOnly();
                                MyActivityManager.removeAllTaskExcludePreLoginStack();
                            }
                        });
                    }

                    @Override
                    public void goMainPage(String errorMsg) {
                        Handler mainHandler = new Handler(ProjectApp.getContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                showStartMainPage(errorMsg);
                            }
                        });
                    }

                    @Override
                    public void startOnlyMainPage(String errorMsg) {
                        DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
                            @Override
                            public void onBtnClickListener() {
//                                MyActivityManager.removeAllTaskExcludeMainStack();
                                HashMap<String, Object> mHashMaps = new HashMap<>();
                                mHashMaps.put(Constants.ACTION_TYPE, ACTION_BASE_ERROR);
                                ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            }
                        });
                    }

                    @Override
                    public void startMainPage(String errorMsg) {
                        Handler mainHandler = new Handler(ProjectApp.getContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                showStartMainPage(errorMsg);
                            }
                        });
                    }

                    @Override
                    public void goLoginPage(String errorMsg) {
                        Handler mainHandler = new Handler(ProjectApp.getContext().getMainLooper());
                        mainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                showStartLoginPage(errorMsg);
                            }
                        });
                    }
                });
    }

    //检测三次OTP错误
    private static boolean checkErrorPasscode(String errorCode) {
        return (TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_LOGIN_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_LOGIN_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_REG_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_REG_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_BIND_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_BIND_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_SMART_ACCOUNT_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_SMART_ACCOUNT_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_FIO_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_FIO_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_FORGET_ONE.code) ||
                TextUtils.equals(errorCode, ErrorCode.OTP_FAIL_FORGET_TWO.code) ||
                TextUtils.equals(errorCode, ErrorCode.BANK_VERIFY_ONLINE_INFO_FAIL.code) ||
                TextUtils.equals(errorCode, ErrorCode.BANK_VERIFY_OFFLINE_INFO_FAIL.code) ||
                TextUtils.equals(errorCode, ErrorCode.FPS_EMAIL_OTP_FAIL_SEND.code) ||
                TextUtils.equals(errorCode, ErrorCode.FPS_OTP_FAIL_SEND.code) ||
                TextUtils.equals(errorCode, ErrorCode.FPS_OTP_FAIL_VERIFY.code));
    }

    public static void showStartMainPageForLogin(String errorMsg) {
        DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                //登录态 跳转到首页
                if (CacheManagerInstance.getInstance().isLogin()) {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.ACTION_TYPE, ACTION_BASE_ERROR);
                    ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
                } else {
                    //非登录态 跳转到登录界面
                    if (MyActivityManager.getInstance().getCurrentActivity() instanceof PreLoginActivity || MyActivityManager.getInstance().getCurrentActivity() instanceof LoginActivity) {

                    } else {
                        if (MyActivityManager.containLoginPage()) {
                            MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                            //从登录界面发起的流程 返回到登录界面
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(LoginActivity.JEMP_ERRORMSG, errorMsg);
                            ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), LoginActivity.class,mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        } else {
                            MyActivityManager.removeAllTaskExcludePreLoginStack();
                            //从PreLoign发起的流程 返回到Prelogin页面
                            ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), PreLoginActivity.class);
                        }
                    }
                }
            }
        });
    }


    public static void showStartMainPage(String errorMsg) {
        DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                //登录态 跳转到首页
                if (CacheManagerInstance.getInstance().isLogin()) {
//                    MyActivityManager.removeAllTaskExcludeMainStack();
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.ACTION_TYPE, ACTION_BASE_ERROR);
                    ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
                } else {
                    //非登录态 跳转到登录界面
                    if (MyActivityManager.getInstance().getCurrentActivity() instanceof PreLoginActivity || MyActivityManager.getInstance().getCurrentActivity() instanceof LoginActivity) {

                    } else {
                        MyActivityManager.removeAllTaskExcludePreLoginStack();
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(LoginActivity.JEMP_ERRORMSG, errorMsg);
                        ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), LoginActivity.class,mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });
    }

    private static boolean checkErrorCodeLogin(String errorCode) {
        return (TextUtils.equals(errorCode, ErrorCode.UNLOGIN.code) ||
                TextUtils.equals(errorCode, ErrorCode.LOGIN_OLD_DEVICE.code) ||
                TextUtils.equals(errorCode, ResponseCode.RE_LOGIN) ||
                TextUtils.equals(errorCode, ResponseCode.SESSION_LOSE_RE_LOGIN) ||
                TextUtils.equals(errorCode, ErrorCode.SERVER_DEVICE_OTHER_LOGIN.code) ||
                TextUtils.equals(errorCode, ErrorCode.PHONE_CHANGE_UNREG_ACCOUNT.code) ||
                TextUtils.equals(errorCode, ErrorCode.REG_FAIL_TO_BIND.code) ||
                TextUtils.equals(errorCode, ErrorCode.SMARTACCOUNT_CHANGE_TO_REG.code)
        );
    }


    public static void showStartLoginPage(String errorMsg) {
        DialogUtils.showErrorMsgDialog(MyActivityManager.getInstance().getCurrentActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                Activity context = MyActivityManager.getInstance().getCurrentActivity();
                if (context instanceof LoginActivity) {

                } else {
                    if (context == null) {
                        return;
                    }
                    AndroidUtils.clearMemoryCacheOnly();
                    MyActivityManager.removeAllTaskExcludePreLoginStack();
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(LoginActivity.JEMP_ERRORMSG, errorMsg);
                    ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), LoginActivity.class,mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        //这里有个单独场景需要处理，如果app call appAppCallAppPaymentActivity 透明存在 需要关闭
        if (MyActivityManager.getInstance().getCurrentActivity() instanceof AppCallAppPaymentActivity) {
            EventBus.getDefault().postSticky(new AppCallAppEventItem(AppCallAppEventItem.EVENT_APP_CALLAPP_SUCCESS, ""));
        }
    }

}
