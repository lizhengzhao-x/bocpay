package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class UpdatePopUpControlProtocol extends BaseProtocol {

    public UpdatePopUpControlProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/messageCenter/pointResponse";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, "NEWUSERPOPUP");
    }
}
