package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/24 14:41
 * 查询单个币种接口,返回对象为SingleCurrencyEntity
 */

public class QueryCurrencyProtocol extends BaseProtocol {
    String mNumberCode;

    public QueryCurrencyProtocol(String numberCode, NetWorkCallbackListener dataCallback) {
        this.mNumberCode = numberCode;
        this.mDataCallback = dataCallback;
        mUrl = "http://bocpay.dev.swiftpass.cn/api/transaction/actionTxnHistEnquiry";
        mEncryptFlag = false;
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.NUMBERCODE, mNumberCode);
    }
}
