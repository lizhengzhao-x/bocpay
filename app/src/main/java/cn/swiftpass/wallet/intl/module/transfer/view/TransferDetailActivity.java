package cn.swiftpass.wallet.intl.module.transfer.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;

public class TransferDetailActivity extends BaseCompatActivity {
    @BindView(R.id.id_merchant_head)
    ImageView idMerchantHead;
    @BindView(R.id.id_merchant_name)
    TextView idMerchantName;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.id_merchant_money)
    TextView idMerchantMoney;
    @BindView(R.id.id_payment_status)
    TextView idPaymentStatus;
    @BindView(R.id.id_payto)
    TextView idPayto;
    @BindView(R.id.id_ordernumber)
    TextView idOrdernumber;
    @BindView(R.id.id_createtime)
    TextView idCreatetime;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_rel_postscript)
    RelativeLayout idRelPostscript;
    @BindView(R.id.id_left_remark)
    TextView idLeftRemark;
    @BindView(R.id.id_remark)
    TextView idRemark;
//    @BindView(R.id.id_fail_type) TextView idFailType;
//    @BindView(R.id.id_rel_failure_type) RelativeLayout idRelFailureType;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.order_deatail_title);
        OrderQueryEntity paymentEnquiryResult = (OrderQueryEntity) getIntent().getExtras().getSerializable(Constants.CARD_ENTITY);
        String trxAmt = paymentEnquiryResult.getTrxAmt();
        idMerchantMoney.setText(BigDecimalFormatUtils.forMatWithDigs(trxAmt, 2));
        idOrdernumber.setText(paymentEnquiryResult.getTransactionNum());

        idCreatetime.setText(AndroidUtils.formatTimeString(paymentEnquiryResult.getTransDate()));
        idPaymentStatus.setText(paymentEnquiryResult.getStatusDesc());
        String trxCurrency = paymentEnquiryResult.getCur();
        if (TextUtils.isEmpty(paymentEnquiryResult.getOutamtsign())) {
            idPaySymbol.setText(AndroidUtils.getTrxCurrency(trxCurrency));
        } else {
            idPaySymbol.setText(paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency));
        }
        //控制附言
        if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            idRelPostscript.setVisibility(View.GONE);
        } else {
            idRemark.setText(paymentEnquiryResult.getPostscript());
        }
        String merchantName = "";
        merchantName = paymentEnquiryResult.getMerchantName();
        idMerchantName.setText(merchantName);
        idPayto.setText(merchantName);

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_detail;
    }


}
