package cn.swiftpass.wallet.intl.module.transactionrecords.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;

public class OrderDetailContract {

    public interface View extends IView {

        void getOrderDetailSuccess(ArrayList<ItemModel> response);

        void getOrderDetailError(String errorCode, String errorMsg);


        void queryGradeInfoSuccess(SweptCodePointEntity sweptCodePointEntity);

        void queryGradeInfoError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<OrderDetailContract.View> {

        void getOrderDetailByBocPay(OrderQueryEntity orderQueryEntity, String listType, String srcRefNo, String orderType);

        void queryGradeInfo(String tnxId);

        void getOrderDetailBySmartAccount(SmartAccountBillListEntity.OutgroupBean smartEntity, String listType, String srcRefNo, String orderType);

        ArrayList<ItemModel> dealDateWithBocPay(OrderQueryEntity bocPayTransDetailEntity, boolean isShareView);

        ArrayList<ItemModel> dealDateWithSmartAccount(SmartAccountBillListEntity.OutgroupBean smartEntity, boolean isShareView);


        boolean transferIsLiShi();
    }

}
