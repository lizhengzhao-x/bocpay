package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.api.QueryVoucherRecordsProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertRecordEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcouponRecordAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

/**
 * 电子券兑换记录
 */
public class EcouponsConvertRecordFragment extends BaseFragment {
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;

    private List<EcouponConvertRecordEntity.RowsBean> mEcouponsConvertList;

    private EcouponRecordAdapter ecouponRecordAdapter;

    private View emptyView;

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }
    @Override
    public void initTitle() {

    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_ecoupons_record;
    }

    @Override
    protected void initView(View v) {
        if (getContext() == null) {
            return;
        }
        mEcouponsConvertList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(getContext(), 0.5f);
        recyclerview.addItemDecoration(MyItemDecoration.createVertical(getContext().getColor(R.color.line_common), ryLineSpace));
        recyclerview.setLayoutManager(layoutManager);
        ecouponRecordAdapter = new EcouponRecordAdapter(mEcouponsConvertList);
        ecouponRecordAdapter.bindToRecyclerView(recyclerview);
        emptyView = getEmptyView(getContext(), recyclerview, R.string.EC15_9, R.mipmap.norecord_icon);
        ecouponRecordAdapter.setEmptyView(emptyView);
        emptyView.setVisibility(View.GONE);
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getRecordsList();
            }
        });

        smartRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                getRecordsList();
            }
        });

    }

    private void getRecordsList() {
        showDialogNotCancel();
        new QueryVoucherRecordsProtocol("exchange", new NetWorkCallbackListener<EcouponConvertRecordEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (smartRefreshLayout == null) {
                    return;
                }
                //停止刷新
                smartRefreshLayout.finishRefresh();
                showErrorMsgDialog(getContext(), errorMsg);
            }

            @Override
            public void onSuccess(EcouponConvertRecordEntity response) {
                dismissDialog();
                if (smartRefreshLayout == null) {
                    return;
                }
                //停止刷新
                smartRefreshLayout.finishRefresh();
                mEcouponsConvertList.clear();
                mEcouponsConvertList.addAll(response.getRows());
                if (mEcouponsConvertList.size() == 0) {
                    emptyView.setVisibility(View.VISIBLE);
                }
                ecouponRecordAdapter.setDataList(mEcouponsConvertList);
            }
        }).execute();
    }

    protected View getEmptyView(Context mContext, RecyclerView recyclerView, int textId, int imgId) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(textId));
        noDataImg.setImageResource(imgId);
        return emptyView;
    }

}
