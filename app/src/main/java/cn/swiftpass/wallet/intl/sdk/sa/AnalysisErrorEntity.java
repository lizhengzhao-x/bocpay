package cn.swiftpass.wallet.intl.sdk.sa;

import com.alibaba.fastjson.JSONObject;

/**
 * 埋点:错误事件
 */
public class AnalysisErrorEntity extends AnalysisBaseEntity {

    /**
     * 用户进入当前页面时间，格式yyyy-MM-dd HH:mm:ss
     */
    public String date_time;

    /**
     * 错误码
     */
    public String error_code;

    /**
     * 错误描述
     */
    public String error_msg;

    /**
     * 当前页面id
     */
    public String current_address;

    public AnalysisErrorEntity() {
        super();
    }

    public AnalysisErrorEntity(long date_time, String error_code, String error_msg, String current_address) {
        super();
        this.date_time = longToStr(date_time, "yyyy-MM-dd HH:mm:ss");
        this.error_code = error_code;
        this.error_msg = error_msg;
        this.current_address = current_address;
    }

    @Override
    public String getDataType() {
        return AnalysisDataType.PA_REG_ERR_POINT;
    }

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = super.getJsonObject();
        jsonObject.put(AnalysisParams.DATE_TIME, date_time);
        jsonObject.put(AnalysisParams.ERROR_MSG, error_msg);
        jsonObject.put(AnalysisParams.ERROR_CODE, error_code);
        jsonObject.put(AnalysisParams.CURRENT_ADDRESS, current_address);
        return jsonObject;
    }
}
