package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.ConfirmTransferCrossBorderDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryRateProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderAmountContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.RateInfoEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;


public class TransferCrossBorderAmountPresenter extends BasePresenter<TransferCrossBorderAmountContract.View> implements TransferCrossBorderAmountContract.Presenter {


    @Override
    public void commitTransferUseData(String receiverName, String cardNo, String receiverbankName, String amountHKD, String amountCNY, String forUsed, String sendCardId, String cardType, String refNo, String lastUpdatedRate, String HKD2CNYRate, String isTransferToSelf) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ConfirmTransferCrossBorderDataProtocol(receiverName, cardNo, receiverbankName, amountHKD, amountCNY, forUsed, sendCardId, cardType, refNo, lastUpdatedRate, HKD2CNYRate, isTransferToSelf, new NetWorkCallbackListener<TransferCrossBorderDetailEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().commitTransferUseDataError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderDetailEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().commitTransferUseDataSuccess(response);
                }
            }
        }

        ).execute();

    }

    @Override
    public void getSmartAccountInfo(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(action,response);
                }
            }
        })).execute();
    }

    @Override
    public void queryRateInfo() {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new QueryRateProtocol(new NetWorkCallbackListener<RateInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().queryRateInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RateInfoEntity response) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().queryRateInfoSuccess(response);
                }
            }
        }).execute();
    }

}
