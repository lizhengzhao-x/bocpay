package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.wallet.intl.entity.PromotionEntity;

/**
 * @author create by ramon date on  on 2018/8/11 15:07
 * 获取首页活动列表文件地址
 * 对应的返回对象为 PromotionEntity
 */

public class ActivityPromotionProtocol extends BaseProtocol {
    private NetWorkCallbackListener mPromotionCallback;

    public ActivityPromotionProtocol(String url, NetWorkCallbackListener dataCallback) {
        mPromotionCallback = dataCallback;
        this.mDataCallback = new PromotionCallback();
        mUrl = url;
        mEncryptFlag = false;
        mPostFlag = false;
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }


    @Override
    public String getUrl() {
        return mUrl;
    }

    class PromotionCallback extends NetWorkCallbackListener<String> {
        @Override
        public void onFailed(String errorCode, String errorMsg) {
            if (null != mPromotionCallback) {
                mPromotionCallback.onFailed(errorCode, errorMsg);
            }
        }

        @Override
        public void onSuccess(String response) {
            try {
                PromotionEntity entity = mGson.fromJson(response, PromotionEntity.class);
                if (null != mPromotionCallback) {
                    mPromotionCallback.onSuccess(entity);
                }
            } catch (Exception e) {
                mPromotionCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
            }

        }
    }
}
