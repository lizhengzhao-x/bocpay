package cn.swiftpass.wallet.intl.entity;

import android.content.Context;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class StagingItemEntity extends BaseEntity {


    private LanguageItem en_US;
    private LanguageItem zh_CN;
    private LanguageItem zh_HK;


    public String getLoadUrl(Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getApplyUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getApplyUrl();
        } else {
            return getEn_US().getApplyUrl();
        }
    }

    public String getTopImgUrl(Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getBannerUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getBannerUrl();
        } else {
            return getEn_US().getBannerUrl();
        }
    }

    public String getTitle(Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getTitle();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getTitle();
        } else {
            return getEn_US().getTitle();
        }
    }

    public String getContentA(Context mContext) {
//        return  "這是內容這是內容<a href=\"https://baidu.com/agreement.html\">高亮标题1</a >這是內容這是內容<a href=\"https://baidu.com/privacy.html\">高亮标题2</a >這是內容這是內容<a href=\"tel:+852 3988 2388\">(852) 3988 2388</a>";
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getContent();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getContent();
        } else {
            return getEn_US().getContent();
        }
    }

    public String getContentB(Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getContentB();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getContentB();
        } else {
            return getEn_US().getContentB();
        }
    }

    public String getContentC(Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getZh_CN().getContentC();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getZh_HK().getContentC();
        } else {
            return getEn_US().getContentC();
        }
    }

    public LanguageItem getEn_US() {
        return en_US;
    }

    public void setEn_US(LanguageItem en_US) {
        this.en_US = en_US;
    }

    public LanguageItem getZh_CN() {
        return zh_CN;
    }

    public void setZh_CN(LanguageItem zh_CN) {
        this.zh_CN = zh_CN;
    }

    public LanguageItem getZh_HK() {
        return zh_HK;
    }

    public void setZh_HK(LanguageItem zh_HK) {
        this.zh_HK = zh_HK;
    }

    private static class LanguageItem {


        private String applyUrl;
        private String bannerUrl;

        public String getApplyUrl() {
            return applyUrl;
        }

        public void setApplyUrl(String applyUrl) {
            this.applyUrl = applyUrl;
        }

        public String getBannerUrl() {
            return bannerUrl;
        }

        public void setBannerUrl(String bannerUrl) {
            this.bannerUrl = bannerUrl;
        }

        public String getContent() {
            return contentA;
        }

        public void setContent(String content) {
            this.contentA = content;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        private String contentA;

        public String getContentA() {
            return contentA;
        }

        public void setContentA(String contentA) {
            this.contentA = contentA;
        }

        public String getContentB() {
            return contentB;
        }

        public void setContentB(String contentB) {
            this.contentB = contentB;
        }

        public String getContentC() {
            return contentC;
        }

        public void setContentC(String contentC) {
            this.contentC = contentC;
        }

        private String contentB;
        private String contentC;
        private String title;

    }

}
