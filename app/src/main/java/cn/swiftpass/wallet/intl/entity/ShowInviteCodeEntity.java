package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class ShowInviteCodeEntity extends BaseEntity {


    /**
     * isShowInviteUI : 0
     */

    private String isShowInviteUI;

    public String getIsShowInviteUI() {
        return isShowInviteUI;
    }

    public void setIsShowInviteUI(String isShowInviteUI) {
        this.isShowInviteUI = isShowInviteUI;
    }

    public boolean showInviteUIStatus() {
        if (TextUtils.isEmpty(isShowInviteUI)) {
            return false;
        }
        return isShowInviteUI.equals("1");
    }
}
