package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import android.view.View;

import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;

public abstract class CardBaseViewHolder extends AbstractExpandableItemViewHolder {

    public CardBaseViewHolder(View itemView) {
        super(itemView);

    }
}
