package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOCheckPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 17:30
 * @change
 * @chang time
 * @class 验证FIO是否正确
 */
public interface FIOCheckPresenter<V extends IView> {
    void onFIOCheck();
}
