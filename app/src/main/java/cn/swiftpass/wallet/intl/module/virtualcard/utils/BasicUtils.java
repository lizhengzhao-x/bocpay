package cn.swiftpass.wallet.intl.module.virtualcard.utils;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

/**
 * Created by ZhangXinchao on 2019/11/23.
 */
public class BasicUtils {



    public static void initSpannableStrWithTvByColor(Context context,String totalStr, String filter, TextView idTv, int color, final View.OnClickListener onClickListener) {
        String[] arrays = totalStr.split(filter);
        final int startIndex = arrays[0].length();
        final int endIndex = arrays[0].length() + arrays[1].length();
        final String title = totalStr.replace(filter, "");
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (onClickListener != null) {
                    onClickListener.onClick(view);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor( ContextCompat.getColor(context, color));
                ds.setUnderlineText(false);
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        idTv.setText(spannableString);
        idTv.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /**
     * 富文本处理
     *
     * @param totalStr
     * @param filter
     * @param idTv
     */
    public static void initSpannableStrWithTv(String totalStr, String filter, TextView idTv, final View.OnClickListener onClickListener) {
        String[] arrays = totalStr.split(filter);
        final int startIndex = arrays[0].length();
        final int endIndex = arrays[0].length() + arrays[1].length();
        final String title = totalStr.replace(filter, "");
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (onClickListener != null) {
                    onClickListener.onClick(view);
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        idTv.setText(spannableString);
        idTv.setMovementMethod(LinkMovementMethod.getInstance());
    }

    //根据当前输入的内容每两位添加一个斜杠，返回值为拼接之后的字符串
    public static String addLineByInputContent(String content, int Totalcount, String spaceString) {
        if (TextUtils.isEmpty(content)) {
            return "";
        }
        //去空格,//去掉所有斜杠
        content = content.replaceAll("/+", "");
        if (TextUtils.isEmpty(content)) {
            return "";
        }
        //过期时间限制为5位的Totalcount=5
        if (content.length() > Totalcount) {
            content = content.substring(0, Totalcount);
        }
        StringBuilder newString = new StringBuilder();
        for (int i = 1; i <= content.length(); i++) {
            //当为第2位时，并且不是最后一个第2位时
            //拼接字符的同时，拼接一个空格spaceString
            //如果在最后一个第2位也拼接，会产生空格无法删除的问题
            //因为一删除，马上触发输入框改变监听，又重新生成了空格
            if (i % 2 == 0 && i != content.length()) {
                newString.append(content.charAt(i - 1) + spaceString);
            } else {
                //如果不是2位的倍数，则直接拼接字符即可
                newString.append(content.charAt(i - 1));
            }
        }
        return newString.toString();
    }
}
