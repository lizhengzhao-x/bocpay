package cn.swiftpass.wallet.intl.module.register.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckFRPRetryProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckHKCustProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetIdCardImageProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetOCRInfoForForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetOCRInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.S2ForgetPwdCustomerInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.S2RegisterInfoProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.CheckHKCustEntity;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.IdvImageEntity;
import cn.swiftpass.wallet.intl.entity.OcrInfoEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.module.register.contract.IdAuthInfoFillContract;

public class IdAuthInfoFillPresenter extends BasePresenter<IdAuthInfoFillContract.View> implements IdAuthInfoFillContract.Presenter {

    @Override
    public void submitRegisterData(String action, S2RegisterInfoEntity registerInfoEntity) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new S2RegisterInfoProtocol(action, registerInfoEntity, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().submitDataFail(action, errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().submitDataSuccess(response, action);
                }
            }
        }).execute();
    }


    @Override
    public void downloadCardImage(String mIdvSequnumber, String mIdvFid) {
        new GetIdCardImageProtocol(mIdvSequnumber, mIdvFid, new NetWorkCallbackListener<IdvImageEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getCardImageFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(IdvImageEntity response) {
                if (getView() != null) {
                    getView().getCardImageSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void checkHKCust(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckHKCustProtocol(action, new NetWorkCallbackListener<CheckHKCustEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkHKCustFail(action, errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(CheckHKCustEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkHKCustSuccess(action, response);
                }
            }
        }).execute();
    }

    @Override
    public void forgetPwdCustomerInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new S2ForgetPwdCustomerInfoProtocol(new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().forgetPwdCustomerInfoFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().forgetPwdCustomerInfoSuccess(response);
                }
            }
        }).execute();
    }


    @Override
    public void checkFPRUsedCount(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckFRPRetryProtocol(action, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkFPRUsedFailed(action, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkFPRUsedSuccess(action, response);
                }
            }
        }).execute();

    }

    @Override
    public void getOcrInfo(String action, String seqNumber, String fileId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        if (RequestParams.FORGET_PWD.equals(action)) {
            new GetOCRInfoForForgetPwdProtocol(seqNumber, fileId, new NetWorkCallbackListener<OcrInfoEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().getORCFailed(errorCode, errorMsg);
                    }
                }

                @Override
                public void onSuccess(OcrInfoEntity response) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().getORCSuccess(response);
                    }
                }
            }).execute();
        } else {
            new GetOCRInfoProtocol(action, seqNumber, fileId, new NetWorkCallbackListener<OcrInfoEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().getORCFailed(errorCode, errorMsg);
                    }

                }

                @Override
                public void onSuccess(OcrInfoEntity response) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().getORCSuccess(response);
                    }

                }
            }).execute();
        }

    }
}
