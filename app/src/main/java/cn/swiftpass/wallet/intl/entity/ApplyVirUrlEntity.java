package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/12/4.
 */
public class ApplyVirUrlEntity extends BaseEntity {


    /**
     * refUrl : https://mobileuat.boccc.com.hk/oca/NOCAWEB/index.html?pcode=214&SCode=624&rtk=
     */

    private String refUrl;

    public String getRefUrl() {
        return refUrl;
    }

    public void setRefUrl(String refUrl) {
        this.refUrl = refUrl;
    }
}
