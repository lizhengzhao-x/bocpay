package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 电子券使用记录 左边兑换记录 右边使用记录
 */
public class EcoupRecordsActivity extends BaseCompatActivity {

    @BindView(R.id.id_ecoupon_convert)
    TextView tabEcouponConvert;
    @BindView(R.id.id_ecoupon_my)
    TextView tabEcouponUse;
    @BindView(R.id.fl_transaction)
    FrameLayout flTransaction;
    private FragmentManager mFragmentManager;
    private EcouponsConvertRecordFragment mEcouponsConvertRecordFragment;
    private EcouponsUseRecordFragment eEcouponsUseRecordFragment;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC15_1));
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        mEcouponsConvertRecordFragment = new EcouponsConvertRecordFragment();
        eEcouponsUseRecordFragment = new EcouponsUseRecordFragment();
        fragmentTrasaction.add(R.id.fl_transaction, mEcouponsConvertRecordFragment);
        fragmentTrasaction.commitAllowingStateLoss();
        tabEcouponConvert.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                setDefaultSelect();
                tabEcouponConvert.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
                tabEcouponConvert.setTextColor(getResources().getColor(R.color.white));
                tabEcouponUse.setTextColor(getResources().getColor(R.color.font_gray_three));
                switchDiffFragmentContent(mFragmentManager, mEcouponsConvertRecordFragment, eEcouponsUseRecordFragment, R.id.fl_transaction, 0);
            }
        });

        tabEcouponUse.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                setDefaultSelect();
                tabEcouponUse.setBackgroundResource(R.drawable.right_china_btn_bac);
                tabEcouponUse.setTextColor(getResources().getColor(R.color.app_white));
                tabEcouponConvert.setTextColor(getResources().getColor(R.color.font_gray_three));
                switchDiffFragmentContent(mFragmentManager, eEcouponsUseRecordFragment, mEcouponsConvertRecordFragment, R.id.fl_transaction, 1);
            }
        });
    }

    private void setDefaultSelect() {
        tabEcouponConvert.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        tabEcouponConvert.setTextColor(getResources().getColor(R.color.white));
        tabEcouponUse.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        tabEcouponUse.setTextColor(getResources().getColor(R.color.white));
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoup_record;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


}
