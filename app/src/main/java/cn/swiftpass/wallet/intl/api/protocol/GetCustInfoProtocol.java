package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class GetCustInfoProtocol extends BaseProtocol {

    public static final String TAG = GetCustInfoProtocol.class.getSimpleName();

    public GetCustInfoProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartReg/getCustInfo";
    }

}
