package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountTopUpActivity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferLiShiFillInfoContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.FpsCheckEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.GreetingTxtEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferLiShiPresenter;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.TransferItemBacAdapter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;
import cn.swiftpass.wallet.intl.widget.MyLinearLyout;

/**
 * Created by ZhangXinchao on 2019/11/4.
 * 派利是 通过转账
 */
public class TransferWithLiShiActivity extends BaseTransferActivity<TransferLiShiFillInfoContract.Presenter> implements TransferLiShiFillInfoContract.View {
    private static final String SPLITFILTER = ", ";
    @BindView(R.id.id_transfer_user_name)
    TextView idTransferUserName;
    @BindView(R.id.tv_region_code_str)
    TextView tvRegionCodeStr;
    @BindView(R.id.etwd_transfer_phone_num)
    TextView etwdTransferPhoneNum;
    @BindView(R.id.id_remark_size)
    TextView remarkSizeTv;
    @BindView(R.id.id_transfer_type_bocpay_img)
    ImageView idTransferTypeBocpayImg;
    @BindView(R.id.id_transfer_type_bocpay)
    RelativeLayout idTransferTypeBocpay;
    @BindView(R.id.id_transfer_bank_name)
    TextView idTransferBankName;
    @BindView(R.id.id_transfer_money)
    EditTextWithDel idTransferMoney;
    @BindView(R.id.mlly_background_contain)
    MyLinearLyout mllyBackgroundContain;
    @BindView(R.id.id_transfer_remark)
    EditTextWithDel idTransferRemark;
    @BindView(R.id.tv_transfer_remark_line)
    TextView tvTransferRemarkLine;
    @BindView(R.id.id_transfer_type_sel_remark)
    RelativeLayout idTransferTypeSelRemark;
    @BindView(R.id.id_recycleview_remark)
    RecyclerView idRecycleviewRemark;
    @BindView(R.id.id_transfer_btn_next)
    TextView idTransferBtnNext;
    @BindView(R.id.id_transfer_type)
    TextView idTransferType;
    @BindView(R.id.id_transfer_type_not_support_tv)
    TextView idTransferTypeNotSupportTv;
    @BindView(R.id.id_transfer_type_not_support_view)
    LinearLayout idTransferTypeNotSupportView;
    @BindView(R.id.id_sel_transfer_type_arrow)
    ImageView isTransferTypeArrow;
    @BindView(R.id.id_sel_bank_view)
    LinearLayout idSelBankView;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.nestedScrollView)
    NestedScrollView nestedScrollView;
    @BindView(R.id.id_transfer_image)
    ImageView idTransferImage;
    @BindView(R.id.id_transfer_fill_info_view)
    LinearLayout idTransferFillInfoView;
    @BindView(R.id.iv_transfer_bank_edit)
    LinearLayout ivTransferBankEdit;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.tv_toggle_see)
    TextView tvToggleSee;
    @BindView(R.id.rly_transfer_eye)
    RelativeLayout rlyTransferEye;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.tv_available_amount_flag)
    TextView tvAvailableAmountFlag;
    @BindView(R.id.tv_transfer_info_title)
    TextView tvTransferInfoTitle;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;
    @BindView(R.id.lly_transfer_contain_name)
    LinearLayout llyTransferContainName;
    @BindView(R.id.tv_transfer_phone_title)
    TextView tvTransferPhoneTitle;


    @BindView(R.id.id_lin_billno)
    LinearLayout llBillNoView;
    @BindView(R.id.id_transfer_billno)
    EditTextWithDel etBillNo;

    private List<String> transferSelItemMoneys;
    private List<TransferResourcesEntity.DefBean.BckImgsBean> transferSelBacImages;
    private List<GreetingTxtEntity> mGreetingTxts;
    private int lastSelBackPosition;
    private int CURRENTPAGEDEPENDREQUESTCALLBACKSIZE = 4;

    //需不要隐藏眼睛
    private boolean isHideEye = true;

    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;

    List<View> views = new ArrayList<>();

    /**
     * 进入页面 需要三个请求都成功 dialog才可以消失 防止dialog消失 隐藏之间事件相应
     */
    private int currentRequestCallBackSize;

    //1.如為BoC Pay轉賬，字數限制應為 50
    //2.如為FPS轉賬，字數限制應為 40
    private int bocPayLimitNum = 50;
    private int fpsLimitNum = 40;

    /**
     * 选择的红包背景
     */
    TransferResourcesEntity.DefBean.BckImgsBean transferSelectBckImg;

    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName, String relUserName) {
        startActivityWithPhoneNumber(activity, area, phone, userName, relUserName, false);
    }

    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName, String relUserName, boolean isEmail) {
        HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
        mHashMapTransferInfo.put(Constants.DATA_PHONE_NUMBER, phone);
        mHashMapTransferInfo.put(Constants.CHOICE_AREA, area);
        mHashMapTransferInfo.put(Constants.USER_NAME, userName);
        mHashMapTransferInfo.put(Constants.IS_EMAIL, isEmail);
        ActivitySkipUtil.startAnotherActivity(activity, TransferWithLiShiActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.RP2101_11_1));
        isPaiLiShi = true;
        idTransferMoney.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(9)});
        idTransferMoney.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        initOriginalView(llyTransferContainName, tvTransferPhoneTitle, idTransferUserName, etwdTransferPhoneNum, null, tvRegionCodeStr, null, isTransferTypeArrow);
        //防止软键盘换车换行
        idTransferRemark.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

        initEditTextView(idTransferMoney);
        transferSelItemMoneys = new ArrayList<>();
        transferSelBacImages = new ArrayList<>();
        mGreetingTxts = new ArrayList<>();
        initSelBankLayout(ivTransferBankEdit, idTransferTypeBocpay, idTransferBtnNext, tvRegionCodeStr, ivSetDailyLimitBalance);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        LinearLayoutManager linearLayoutManagerWithBac = new LinearLayoutManager(this);
        linearLayoutManagerWithBac.setOrientation(LinearLayoutManager.HORIZONTAL);
        idRecycleviewRemark.setLayoutManager(linearLayoutManager);

        idRecycleviewRemark.setAdapter(getAdapterWithTransferBac(transferSelBacImages));
        idRecycleviewRemark.getAdapter().notifyDataSetChanged();

        idTransferImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                showSelGreetingsDialog();
            }
        });
        // 当前页面涉及到两个api调用 为了防止dialog dismiss->show 之间点击事件连续相应 暂时通过参数控制
        showDialogNotCancel(this);
        getTransferResourcesInfo();
        checkRetainBalanceInfo(false);
        updateNextStatus();
        initTextWatcher();

        /**
         * 样式原因  需要手动设置祝福语的下划线
         */
        idTransferRemark.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (hasFocus) {
                    tvTransferRemarkLine.setBackgroundColor(getResources().getColor(R.color.app_black, null));
                } else {
                    tvTransferRemarkLine.setBackgroundColor(getResources().getColor(R.color.line_main, null));
                }
            }
        });

        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });


        cbToggleSee.setChecked(false);
        hideAccount();
        getSmartAccountInfo();

        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));


    }

    private void iniLlyContent() {
        if (null == transferSelItemMoneys) {
            return;
        }
        mllyBackgroundContain.removeAllViews();
        views.clear();
        View view;
        for (int i = 0; i < transferSelItemMoneys.size(); i++) {
            String str = transferSelItemMoneys.get(i);
            final int position = i;
            view = LayoutInflater.from(this).inflate(R.layout.transfer_item_sel_money, mllyBackgroundContain, false);
            TextView idSelTexMoney = view.findViewById(R.id.id_sel_text_money);

            idSelTexMoney.setText("$" + str);

            int itemMargin = AndroidUtils.dip2px(mContext, 20);
            int width = (AndroidUtils.getScreenWidth(mContext) - itemMargin * 2 - itemMargin * 3) / 4;
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.findViewById(R.id.id_sel_text_money).getLayoutParams();
            lp.leftMargin = itemMargin;
            lp.width = width;
            lp.height = width * 43 / 64;
            view.findViewById(R.id.id_sel_text_money).setLayoutParams(lp);
            idSelTexMoney.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    idTransferMoney.setContentText(transferSelItemMoneys.get(position));
                    idTransferMoney.getEditText().setSelection(transferSelItemMoneys.get(position).length());
                }
            });
            views.add(view);
        }
        mllyBackgroundContain.addViews(views);
    }

    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }

        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);

            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");

        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }

    @OnClick({R.id.tv_transfer_top_up, R.id.tv_balance_title,
            R.id.tv_amount,
            R.id.tv_balance_tody_title,
            R.id.tv_amount_usable, R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
            return;
        }
        switch (view.getId()) {

            case R.id.tv_transfer_top_up:
                onClickTopUp();
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_REDPACKET_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    String str = new Gson().toJson(mSmartAccountInfo);
                    try {
                        SmartAccountAdjustDailyLimitActivity.startActivity(this, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                    } catch (Exception e) {

                    }
                }
                break;
        }
    }

    //充值
    private void onClickTopUp() {
        if (mSmartAccountInfo == null) return;
        if (TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel())) return;
        if (!TextUtils.isEmpty(mSmartAccountInfo.getRelevanceAccNo()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
            mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
            mHashMaps.put(Constants.ACCTYPE, mSmartAccountInfo.getAccType());
            mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            topUpAccountEvent();
        }

    }

    private void topUpAccountEvent() {
        List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
        if (bankCardsBeans != null && bankCardsBeans.size() > 0 && !TextUtils.isEmpty(bankCardsBeans.get(0).getStatus())) {
            //Edda状态，N：没有登记过，A：登记成功 R：登记失败 1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
            // E:删除失败 C:删除成功 4:删除申请失败 9删除申请处理中 3提交删除申请成功
            if (bankCardsBeans.get(0).getEddaStatus().equals("A") || bankCardsBeans.get(0).getEddaStatus().equals("E") || bankCardsBeans.get(0).getEddaStatus().equals("3")) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
                mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
                mHashMaps.put(Constants.ACCTYPE, bankCardsBeans.get(0).getBankName());
                mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
                ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("N")) {
                onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("R")) {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("0") || bankCardsBeans.get(0).getEddaStatus().equals("1")) {
                showErrorMsgDialog(getActivity(), getString(R.string.string_edda_loading));
            } else {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            }
        } else {
            onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
        }

    }

    /**
     * s2用户绑定银行卡
     */
    private void onWainningTopUpWithBindAccount(String message) {
        String str_title = getString(R.string.P3_D1_1_1);
        String str_msg = message;
        String str_pos = getString(R.string.P3_D1_1_3);
        String str_neg = getString(R.string.string_cancel);
        AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_pos, str_neg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getRegEddaInfo(mSmartAccountInfo);
            }
        });
    }

    private void getRegEddaInfo(final MySmartAccountEntity mSmartAccountInfo) {
        mPresenter.getRegEddaInfo();
    }


    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    public void getSmartAccountInfo() {
        llContent.setVisibility(View.GONE);
        mPresenter.getSmartAccountInfo();
    }


    private void initTextWatcher() {
        idTransferMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (llBillNoView.getVisibility() == View.VISIBLE) {
                    //商户转账需要输入交易参考编号
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
                } else {
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etwdTransferPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateNextStatus();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etBillNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

    }

    /**
     * 查询账户余额
     */
    protected void checkRetainBalanceInfo(boolean showDialog) {
        mPresenter.getAccountBalanceInfo();
    }

    /**
     * 获取转账背景图片资源
     */
    private void getTransferResourcesInfo() {
        mPresenter.getTransferResourceInfo();
    }

    /**
     * 请求成功之后判断是否显示dialog
     */
    private void judgeDismissDialog() {
        currentRequestCallBackSize++;
        if (currentRequestCallBackSize >= CURRENTPAGEDEPENDREQUESTCALLBACKSIZE) {
            dismissDialog();
        }
    }

    private void updateResourceInfo(TransferResourcesEntity response) {
        List<TransferResourcesEntity.DefBean.BckImgsBean> bacImgs = response.getDef().getBck_imgs();
        for (int i = 0; i < bacImgs.size(); i++) {
            TransferResourcesEntity.DefBean.BckImgsBean imgsBean = bacImgs.get(i);
            imgsBean.setSel(i == 0);
        }
        transferSelBacImages.addAll(bacImgs);

        //初始化红包选择背景 默认第一个
        if (transferSelBacImages.size() > 0) {
            transferSelectBckImg = transferSelBacImages.get(0);
        }

        idRecycleviewRemark.getAdapter().notifyDataSetChanged();
        List<String> defAmounts = response.getDef().getDef_amt();
        transferSelItemMoneys.clear();
        transferSelItemMoneys.addAll(defAmounts);
        //idRecycleviewBackground.getAdapter().notifyDataSetChanged();
        iniLlyContent();

        List<String> mGreetingTxtStrs = new ArrayList<>();
        //派利是 祝福语
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            if (response.getDef().getDef_postscript() != null && response.getDef().getDef_postscript().getPst_cn() != null) {
                mGreetingTxtStrs.addAll(response.getDef().getDef_postscript().getPst_cn());
            }
        } else if (AndroidUtils.isHKLanguage(lan)) {
            if (response.getDef().getDef_postscript() != null && response.getDef().getDef_postscript().getPst_hk() != null) {
                mGreetingTxtStrs.addAll(response.getDef().getDef_postscript().getPst_hk());
            }
        } else {
            if (response.getDef().getDef_postscript() != null && response.getDef().getDef_postscript().getPst_en() != null) {
                mGreetingTxtStrs.addAll(response.getDef().getDef_postscript().getPst_en());
            }
        }
        for (int i = 0; i < mGreetingTxtStrs.size(); i++) {
            GreetingTxtEntity greetingTxtEntity = new GreetingTxtEntity();
            greetingTxtEntity.setSel(false);
            greetingTxtEntity.setTitleStr(mGreetingTxtStrs.get(i));
            mGreetingTxts.add(greetingTxtEntity);
        }
    }

    private void showSelGreetingsDialog() {
        String contentTxt = idTransferRemark.getText().toString().trim();
        String[] items = contentTxt.split(SPLITFILTER);
        //先恢复未选中状态
        for (int i = 0; i < mGreetingTxts.size(); i++) {
            mGreetingTxts.get(i).setSel(false);
        }
        for (int i = 0; i < mGreetingTxts.size(); i++) {
            for (int j = 0; j < items.length; j++) {
                if (items[j].equals(mGreetingTxts.get(i).getTitleStr())) {
                    mGreetingTxts.get(i).setSel(true);
                    break;
                }
            }
        }
        final TransferSelGreetingsDialogFragment transferSelGreetingsDialogFragment = new TransferSelGreetingsDialogFragment();
        transferSelGreetingsDialogFragment.setOnTransferGreetingTxtSelClickListener(new TransferSelGreetingsDialogFragment.OnTransferGreetingTxtSelClickListener() {
            @Override
            public void onBackBtnClickListener() {
                transferSelGreetingsDialogFragment.dismiss();
            }

            @Override
            public void onTransferGreetingTxtSeflListener(int positon) {
                lastSelBackPosition = positon;
                String contentTxt = idTransferRemark.getText().toString().trim();
                if (TextUtils.isEmpty(contentTxt)) {
                    //之前附言为空 直接追加词条
                    contentTxt = mGreetingTxts.get(positon).getTitleStr();
                } else {
                    //这里要判断是否已经存在这个词条 如果已经存在 要剔除
                    String tempStr = mGreetingTxts.get(positon).getTitleStr();
                    if (contentTxt.contains(tempStr)) {
                        int position = contentTxt.indexOf(tempStr);
                        if (position == 0) {
                            if (tempStr.length() == contentTxt.length()) {
                                contentTxt = contentTxt.replace(tempStr, "");
                            } else {
                                contentTxt = contentTxt.replace(tempStr + SPLITFILTER, "");
                            }
                        } else {
                            contentTxt = contentTxt.replace(SPLITFILTER + tempStr, "");
                        }
                    } else {
                        contentTxt = contentTxt + SPLITFILTER + mGreetingTxts.get(positon).getTitleStr();
                    }
                }
                if (contentTxt.startsWith(SPLITFILTER)) {
                    contentTxt = contentTxt.replace(SPLITFILTER, "");
                }
                int maxSize = lastSelTransferType == 0 ? 50 : 40;
                if (contentTxt.length() > maxSize) {
                    showErrorMsgDialog(mContext, getString(R.string.RP1_7a_11a));
                } else {
                    idTransferRemark.setContentText(contentTxt);
                }
                try {

                    idTransferRemark.getEditText().setSelection(idTransferRemark.getText().length());
                } catch (Exception e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
                transferSelGreetingsDialogFragment.dismiss();
            }
        });
        transferSelGreetingsDialogFragment.setmGreetingTxts(mGreetingTxts);
        transferSelGreetingsDialogFragment.show(getSupportFragmentManager(), TransferSelGreetingsDialogFragment.class.getName() + "");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_lishi;
    }

    @Override
    protected TransferLiShiFillInfoContract.Presenter createPresenter() {
        return new TransferLiShiPresenter();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        if (response != null) {
            llContent.setVisibility(View.VISIBLE);
            mSmartAccountInfo = response;
            CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);
            judgeDismissDialog();
            //这里也重新设置AccountBalanceEntity
            accountBalanceEntity = new AccountBalanceEntity();
            accountBalanceEntity.setBalance(mSmartAccountInfo.getOutavailbal());
            accountBalanceEntity.setCurrency(mSmartAccountInfo.getCurrency());
            if (null != cbToggleSee && cbToggleSee.isChecked()) {
                openAccount();
            } else {
                hideAccount();
            }
            if (isEyeDateNull) {
                cbToggleSee.setChecked(true);
                isEyeDateNull = false;
            }
            if (!idTransferTypeNotSupportView.isShown()) {
                if (llBillNoView.getVisibility() == View.VISIBLE) {
                    //商户转账需要输入交易参考编号
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
                } else {
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, null);
                }
            }
        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        getRetainBalanceInfo(null);
        dismissDialog();
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                TransferWithLiShiActivity.this.finish();
            }
        });
    }

    @Override
    public void getAccountBalanceInfoSuccess(AccountBalanceEntity response) {
        if (response != null) {
            getRetainBalanceInfo(response);
            judgeDismissDialog();
        }
    }

    @Override
    public void getAccountBalanceInfoError(String errorCode, String errorMsg) {
        TransferWithLiShiActivity.this.finish();
    }

    @Override
    public void getTransferResourceInfoSuccess(TransferResourcesEntity response) {
        updateResourceInfo(response);
        judgeDismissDialog();
    }

    @Override
    public void getTransferResourceInfoError(String errorCode, String errorMsg) {
        dismissDialog();
        showErrorMsgDialog(TransferWithLiShiActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                TransferWithLiShiActivity.this.finish();
            }
        });
    }

    @Override
    protected void updateNextStatus() {
        if (isInviteStatus) {
            updateOkBackground(idTransferBtnNext, true);
            return;
        }
        if (!TextUtils.isEmpty(idTransferMoney.getText()) && !TextUtils.isEmpty(etwdTransferPhoneNum.getText())) {
            if (llBillNoView.getVisibility() == View.VISIBLE) {
                //商户转账需要输入交易参考编号
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), null, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
            } else {
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), null, idTransferBtnNext, idSelBankView, null);
            }
            return;
        }
        idTransferBtnNext.setEnabled(false);
        idTransferBtnNext.setBackgroundResource(R.drawable.bg_btn_next_page_disable);
    }

    void getRetainBalanceInfo(AccountBalanceEntity response) {

        accountBalanceEntity = response;

        String str = getString(R.string.TF2101_4_8);
        str = str.replace("(HKD)", "");
        if (null != accountBalanceEntity) {
            tvTransferInfoTitle.setText(str + "(" + accountBalanceEntity.getCurrency() + ")");
        }

        updateNextStatus();
    }

    @Override
    void getBankNameResult(String mCurrentBankCodeIn, String mCurrentBankNameIn) {
        mCurrentBankName = mCurrentBankNameIn;
        mCurrentBankCode = mCurrentBankCodeIn;
        idTransferBankName.setText(mCurrentBankName);
        updateNextStatus();
    }

    @Override
    void checkEditMoney(boolean isRed) {
        if (null == idTransferMoney) {
            return;
        }

        if (isRed) {
            idTransferMoney.setLineRedVisible(true);
            idTransferMoney.setLineVisible(false);
        } else {
            idTransferMoney.setLineRedVisible(false);
            idTransferMoney.setLineVisible(true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //防止按钮多次点击
        if (idTransferBtnNext == null) return;
        idTransferBtnNext.setEnabled(true);

        if (null != cbToggleSee) {
            if (isHideEye) {
                cbToggleSee.setChecked(false);
            } else {
                isHideEye = true;
            }

        }
    }

    @Override
    void transferPreCheck() {
        if (null != transferSelectBckImg) {

            if (llBillNoView.getVisibility() == View.VISIBLE) {
                //商户转账需要输入交易参考编号
                preTransferBocPayAction(tvRegionCodeStr.getText().toString(), etwdTransferPhoneNum.getText().toString(), idTransferRemark.getText().toString(), idTransferMoney.getText(), true, idSelBankView, idTransferBtnNext, transferSelectBckImg.getCoverId(), etBillNo.getEditText().getText().toString());
            } else {
                preTransferBocPayAction(tvRegionCodeStr.getText().toString(), etwdTransferPhoneNum.getText().toString(), idTransferRemark.getText().toString(), idTransferMoney.getText(), true, idSelBankView, idTransferBtnNext, transferSelectBckImg.getCoverId(), null);
            }

        }
    }

    private CommonRcvAdapter<TransferResourcesEntity.DefBean.BckImgsBean> getAdapterWithTransferBac(final List<TransferResourcesEntity.DefBean.BckImgsBean> data) {
        return new CommonRcvAdapter<TransferResourcesEntity.DefBean.BckImgsBean>(data) {

            @Override
            public Object getItemType(TransferResourcesEntity.DefBean.BckImgsBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new TransferItemBacAdapter(TransferWithLiShiActivity.this, new TransferItemBacAdapter.OnIemBacClickCallback() {

                    @Override
                    public void onItemClick(int position) {
                        super.onItemClick(position);
                        if (transferSelBacImages.get(position).isSel()) return;
                        for (int i = 0; i < transferSelBacImages.size(); i++) {
                            if (i == position) {
                                transferSelBacImages.get(i).setSel(true);
                            } else {
                                transferSelBacImages.get(i).setSel(false);
                            }
                        }
                        transferSelectBckImg = transferSelBacImages.get(position);
                        idRecycleviewRemark.getAdapter().notifyDataSetChanged();
                    }
                });
            }
        };
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }

    @Override
    void updateAreaCode(String code) {
        tvRegionCodeStr.setText(code);
        currentAreaStr = code;
        String phoneNumberEnd = currentAreaStr.replace("+", "") + "-" + currentPhoneStr;
        fpsCheckAction(Constants.TRANSFER_SEL_ACCOUNT_PHONE, phoneNumberEnd, true);
    }

    @Override
    void selCurrentTransferType(int position) {
        updateSelTransferType(position, idTransferType, idTransferTypeBocpayImg, idTransferTypeNotSupportTv, idTransferTypeNotSupportView, idSelBankView, idTransferBtnNext);
    }

    @Override
    protected void onFpsCheckResult(FpsCheckEntity response) {
        judgeDismissDialog();
        setDefaultTransferType(response, idTransferType, idTransferTypeBocpayImg, idSelBankView);
        updateSelTransferType(lastSelTransferType, idTransferType, idTransferTypeBocpayImg, idTransferTypeNotSupportTv, idTransferTypeNotSupportView, idSelBankView, idTransferBtnNext);

        //红包转账需要重新写 转账方法的显示
        upDatePayWay(response);
        if (response.isMerchantType()) {
            //如果是商户 需要填写交易参考号
            llBillNoView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * i.  如对方同时有下载BoC Pay并绑定智能/支付账户，及有登记FPS，
     * 付款方式只显示及允许使用「BoC Pay转账」，不需要提供下拉框切换付款方式派利是。
     * <p>
     * ii. 如对方只有BoC Pay，效果与第 i 点一致。
     * <p>
     * iii.         如对方没有BoC Pay，只有FPS，付款方式只显示及允许使用「转数快」，
     * 不需要提供下拉框切换付款方式派利是。
     *
     * @param response
     */
    private void upDatePayWay(FpsCheckEntity response) {
        isSupportBocPay = response.getBocpaySign();
        isSupportFps = response.getFpsSign();

        //先不能点击转账方法
        idTransferTypeBocpay.setEnabled(false);
        isTransferTypeArrow.setVisibility(View.INVISIBLE);
        idSelBankView.setVisibility(View.GONE);
        //  i.  如对方同时有下载BoC Pay并绑定智能/支付账户，及有登记FPS，
        //  付款方式只显示及允许使用「BoC Pay转账」，不需要提供下拉框切换付款方式派利是。
        //  ii. 如对方只有BoC Pay，效果与第 i 点一致。
        //  iii.         如对方没有BoC Pay，只有FPS，付款方式只显示及允许使用「转数快」，
        //  不需要提供下拉框切换付款方式派利是。
        //  iv. 如对方只绑定信用卡、没有注册BoC Pay，及/或没有登记FPS，流程与现有一致。
        if (isSupportBocPay && isSupportFps) {
            //两种支付方式都支持 默认选中BoC Pay 并且不支持换
            lastSelTransferType = 0;
            idTransferType.setText(getString(R.string.TF2101_4_5));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_bocpay);
            AndroidUtils.setLimitWithPailiShi(idTransferRemark.getEditText(), bocPayLimitNum, remarkSizeTv);
        } else if (!isSupportBocPay && !isSupportFps) {
            if (isOpenSmartAccount) {
                if (!TextUtils.equals(response.bocpaySign, Constants.ACCOUNT_TYPE_SMART)) {
                    idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_1));
                } else {
                    idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_2));
                }
            } else {
                idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_2));
            }

            //两种方式都不支持 默认bocpay
            lastSelTransferType = 0;
            idTransferType.setText(getString(R.string.TF2101_4_5));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_bocpay);
            AndroidUtils.setLimitWithPailiShi(idTransferRemark.getEditText(), bocPayLimitNum, remarkSizeTv);
            idSelBankView.setVisibility(View.GONE);
            hideButtomFillInfoView(true);
        } else if (isSupportBocPay && !isSupportFps) {
            //选中bocpay
            lastSelTransferType = 0;
            idTransferType.setText(getString(R.string.TF2101_4_5));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_bocpay);
            AndroidUtils.setLimitWithPailiShi(idTransferRemark.getEditText(), bocPayLimitNum, remarkSizeTv);
        } else if (!isSupportBocPay && isSupportFps) {
            //选中fps
            lastSelTransferType = 1;
            idTransferType.setText(getString(R.string.TR1_9_5));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_fps);
            idSelBankView.setVisibility(View.VISIBLE);
            AndroidUtils.setLimitWithPailiShi(idTransferRemark.getEditText(), fpsLimitNum, remarkSizeTv);
        }
    }

    @Override
    protected void onFpsCheckErrorResult(String errorCode, String errorMsg) {
        dismissDialog();
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
        //修改昵称后需要更新昵称
        else if (event.getEventType() == TransferEventEntity.UPDATE_TRANSFER_LIST) {
            currentUserName = event.getEventMessage();
            if (!TextUtils.isEmpty(currentUserName)) {
                idTransferUserName.setText(currentUserName);
            } else {
                idTransferUserName.setText(currentPhoneStr);
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            checkRetainBalanceInfo(true);
        }
    }

    /**
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }
        }

    }


    @Override
    protected TransferResourcesEntity.DefBean.BckImgsBean getBackImageBean() {
        TransferResourcesEntity.DefBean.BckImgsBean bckImgsBean = null;
        for (int i = 0; i < transferSelBacImages.size(); i++) {
            if (transferSelBacImages.get(i).isSel()) {
                bckImgsBean = transferSelBacImages.get(i);
            }
        }
        return bckImgsBean;
    }

    @Override
    void hideButtomFillInfoView(boolean isHide) {
        llContent.setVisibility(View.VISIBLE);
        idTransferFillInfoView.setVisibility(isHide ? View.GONE : View.VISIBLE);
    }

}
