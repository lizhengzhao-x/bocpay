package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetMyeVoucherListProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.api.GeteVoucherDetailsProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.api.RedeemeVouchersStatusProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.EcouponsUseContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EVoucherStatusEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponsDetailEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;

/**
 * Created by ZhangXinchao on 2019/8/11.
 */
public class UseEcouponsPresenter extends BasePresenter<EcouponsUseContract.View> implements EcouponsUseContract.Presenter {
    @Override
    public void geteVoucherDetails(final String referenceNo, final boolean showLoading) {
        if (getView() != null && showLoading) {
            getView().showDialogNotCancel();
        }
        new GeteVoucherDetailsProtocol(referenceNo, new NetWorkCallbackListener<EcouponsDetailEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().geteVoucherDetailsError(errorCode, errorMsg, referenceNo);
                }
            }

            @Override
            public void onSuccess(EcouponsDetailEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().geteVoucherDetailsSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void redeemeVouchersStatus(List<String> referenceNos) {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new RedeemeVouchersStatusProtocol(referenceNos, new NetWorkCallbackListener<EVoucherStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    //getView().dismissDialog();
                    getView().redeemeVouchersStatusError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(EVoucherStatusEntity response) {
                if (getView() != null) {
                    // getView().dismissDialog();
                    getView().redeemeVouchersStatusSuccess(response);
                }
            }
        }).execute();
    }


    @Override
    public void getMyEcouponsList(String bu, String reload) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetMyeVoucherListProtocol(bu, reload, new NetWorkCallbackListener<MyEVoucherEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyEcouponsListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MyEVoucherEntity response) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().getMyEcouponsListSuccess(response);
                }
            }
        }).execute();
    }
}
