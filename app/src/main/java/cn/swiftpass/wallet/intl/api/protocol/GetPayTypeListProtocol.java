package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 获取支付方式列表
 */
public class GetPayTypeListProtocol extends BaseProtocol {


    private String qrcode;

    public GetPayTypeListProtocol(String qrcode, NetWorkCallbackListener dataCallback) {
        this.qrcode = qrcode;
        this.mDataCallback = dataCallback;
        mUrl = "api/querycard/getPayTypeList";
    }


    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(qrcode)) {
            mBodyParams.put(RequestParams.QRCODE, qrcode);
        }
    }
}
