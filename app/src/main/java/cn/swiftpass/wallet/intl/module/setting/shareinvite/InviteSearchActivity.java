package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteListEntity;
import cn.swiftpass.wallet.intl.entity.InviteShareEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.InviteContactContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.InviteContactPresenter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

/**
 * @author shican
 */
public class InviteSearchActivity extends BaseCompatActivity<InviteContactContract.Presenter> implements InviteContactContract.View {

    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.et_search)
    ClearEditText etSearch;
    @BindView(R.id.con_search)
    ConstraintLayout conSearch;
    @BindView(R.id.ry_invite)
    RecyclerView ryInvite;

    private InviteSearchAdapter inviteSearchAdapter;
    private List<InviteContactEntity> filterContactList = new ArrayList<>();

    /**
     * 获取本地通讯录 ondestroy销毁
     */
    public static List<InviteContactEntity> allContactList = new ArrayList<>();

    private String lastFilterStr;

    private boolean hasData;

    private InviteShareEntity mInviteShareEntity;
    /**
     * 分享的内容
     */
    private String shareContent;

    private View emptyView;
    private LinearLayout llEmpty;

    public static void setAllContactList(List<InviteContactEntity> allContactList) {
        InviteSearchActivity.allContactList = allContactList;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.MGM4_2_1);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mInviteShareEntity = (InviteShareEntity) bundle.getSerializable(InviteShareFragment.SHARE_INFO);
            hasData = bundle.getBoolean(InviteShareFragment.HAS_DATA);
            if (mInviteShareEntity != null) {
                shareContent = mInviteShareEntity.getShareInfo();
            }
        }

        initRecycleView();
        initEditTextListener();
        getFocusView();
    }

    private void initRecycleView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 0.5f);
        ryInvite.addItemDecoration(MyItemDecoration.createVertical(getColor(R.color.line_common), ryLineSpace));
        ryInvite.setLayoutManager(mLayoutManager);
        inviteSearchAdapter = new InviteSearchAdapter(filterContactList);
        inviteSearchAdapter.setHeaderAndEmpty(true);
        inviteSearchAdapter.bindToRecyclerView(ryInvite);

        emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) ryInvite.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        llEmpty = emptyView.findViewById(R.id.ll_empty);
        llEmpty.setVisibility(View.GONE);
        noDataText.setText(getString(R.string.TR1_2b_1));
        noDataImg.setImageResource(R.mipmap.icon_transaction_noresult);
        inviteSearchAdapter.setEmptyView(emptyView);

        inviteSearchAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (!inviteSearchAdapter.isGetStatus()) {
                    InviteContactEntity inviteContactEntity = filterContactList.get(position);
                    List<InviteContactEntity> inviteList = new ArrayList<>();
                    inviteList.add(inviteContactEntity);
                    mPresenter.getInviteContact(inviteList);
                }
            }
        });

        inviteSearchAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (baseRecyclerAdapter.getDataList().size() == 0) {
                    return;
                }
                final InviteContactEntity inviteContactEntity = (InviteContactEntity) baseRecyclerAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.iv_sms:
                        if (inviteContactEntity != null) {
                            SdkShareUtil.sendSMS(mContext, shareContent, inviteContactEntity.getAccount());
                        }
                        break;
                    case R.id.iv_whatsapp:
                        //判断是否安装了whatsApp
                        boolean isSuccess = SdkShareUtil.isAppInstalled(mContext, SdkShareUtil.WHATS_APP);
                        if (!isSuccess) {
                            showErrorMsgDialog(mContext, getResources().getString(R.string.SH2_3_1));
                        } else {
//                            AndroidUtils.showTipDialog(InviteSearchActivity.this, "", getString(R.string.BBS1_1_1),
//                                    getString(R.string.dialog_right_btn_ok), getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
//                                        @Override
//                                        public void onClick(DialogInterface dialog, int which) {
                            if (inviteContactEntity != null) {
                                String phoneNumber = inviteContactEntity.getAccount();
                                if (phoneNumber != null && phoneNumber.length() == 11) {
                                    if (phoneNumber.startsWith("853") || phoneNumber.startsWith("852")) {

                                    } else {
                                        phoneNumber = "86" + phoneNumber;
                                    }
                                } else if (phoneNumber != null && phoneNumber.length() == 8) {
                                    phoneNumber = "852" + phoneNumber;
                                }
                                SdkShareUtil.openWhatsApp(mContext, shareContent, phoneNumber);
                            }
//                                        }
//                                    });
                        }
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        allContactList = null;
    }

    private void getFocusView() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                etSearch.setFocusable(true);
                etSearch.requestFocus();
                AndroidUtils.showKeyboardView(etSearch);
            }
        }, 200);
    }

    private void initEditTextListener() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contentEquals("\n")) {
                    return "";
                } else {
                    return null;
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter};
        etSearch.setFilters(filters);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                lastFilterStr = s.toString();
                if (hasData) {
                    searchFilterWithAllList(lastFilterStr);
                } else {
                    llEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void searchFilterWithAllList(String filterStr) {
        filterContactList.clear();
        inviteSearchAdapter.setGetStatus(false);
        //去除空格的匹配规则
        filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
        if (TextUtils.isEmpty(filterStr)) {
            llEmpty.setVisibility(View.GONE);
            filterContactList.clear();
            inviteSearchAdapter.replaceData(filterContactList);
            return;
        }
        filterContactList.clear();
        if (allContactList != null && allContactList.size() > 0) {
            int size = allContactList.size();
            for (int i = 0; i < size; i++) {
                InviteContactEntity inviteContactEntity = allContactList.get(i);
                String name = inviteContactEntity.getNameNoMapping() + inviteContactEntity.getPhoneNoMapping();
                String pinyinName = Pinyin.toPinyin(name, "");
                if (pinyinName.toLowerCase().startsWith(filterStr.toLowerCase()) || name.toLowerCase().contains(filterStr.toLowerCase())) {
                    filterContactList.add(inviteContactEntity);
                }
            }

            if (filterContactList.size() == 0) {
                llEmpty.setVisibility(View.VISIBLE);
            } else {
                inviteSearchAdapter.replaceData(filterContactList);
            }
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_invite_search;
    }

    @Override
    protected InviteContactPresenter createPresenter() {
        return new InviteContactPresenter();
    }

    @Override
    public void getInviteContactFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, R.string.CO1_3a_6, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
            }
        });
    }

    @Override
    public void getInviteContactSuccess(InviteListEntity inviteListEntity) {
        if (inviteListEntity != null) {
            etSearch.setCursorVisible(false);
            etSearch.setText("");

            inviteSearchAdapter.setGetStatus(true);
            List<InviteContactEntity> contactList = inviteListEntity.result;
            inviteSearchAdapter.replaceData(contactList);
        }
    }

    @Override
    public void getDataSuccess(List<InviteContactEntity> dataList, int totalNum) {

    }

}
