package cn.swiftpass.wallet.intl.base;


import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.UnsafeOkHttpClient;
import okhttp3.OkHttpClient;


public class OkHttpUtils {
    private static final String TAG = "OkHttpUtils";
    private static OkHttpUtils mOkHttpUtilsInstance;
    private OkHttpClient mOkHttpClient;

    public static OkHttpUtils getInstance() {
        if (mOkHttpUtilsInstance == null) {
            synchronized (OkHttpUtils.class) {
                if (mOkHttpUtilsInstance == null) {
                    mOkHttpUtilsInstance = new OkHttpUtils();
                }
            }
        }
        return mOkHttpUtilsInstance;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    private OkHttpUtils() {
        if (BuildConfig.FLAVOR.equals("prd")) {
            //生产环境下
            mOkHttpClient = new OkHttpClient();
        } else {
            mOkHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
        }
        OkHttpClient.Builder builder = mOkHttpClient.newBuilder();
        setBuilder(builder);
        builder.connectTimeout(OkHttpManager.CONNECT_TIME_OUT, TimeUnit.SECONDS).readTimeout(OkHttpManager.READ_TIME_OUT, TimeUnit.SECONDS).writeTimeout(OkHttpManager.WRITE_TIME_OUT, TimeUnit.SECONDS);
    }

    public OkHttpClient.Builder setBuilder(OkHttpClient.Builder builder) {
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(HttpsUtils.readKeyStore(BuildConfig.HTTPS_CER));
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            builder.sslSocketFactory(sslContext.getSocketFactory());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
        } catch (Exception e) {
            LogUtils.d(TAG, e.getMessage());
        }
        return builder;
    }

}
