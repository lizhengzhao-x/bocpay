package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.TransInformation;
import cn.swiftpass.wallet.intl.module.login.SelectLoginCodeActivity;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterBankActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.AreaSelectionForVirtualCardActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.RegisterVitualCardVerifyIdCardContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VitualCardIsBocEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.VirtualCardRegisterPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter.CHARSEQUENCE;

/**
 * Created by ZhangXinchao on 2019/11/18.
 * 绑定虚拟卡 身份证校验
 */
public class VirtualCardIdCardVerificationActivity extends BaseCompatActivity<RegisterVitualCardVerifyIdCardContract.Presenter> implements RegisterVitualCardVerifyIdCardContract.View {
    @BindView(R.id.id_virtualcard_idcard_number)
    EditTextWithDel idVirtualcardIdcardNumber;
    @BindView(R.id.id_virtualcard_phonenumber)
    EditTextWithDel idVirtualcardPhonenumber;
    @BindView(R.id.id_virtualcard_confirm)
    TextView idVirtualcardConfirm;
    @BindView(R.id.et_idcard_type)
    TextView et_idcard_type;
    @BindView(R.id.id_vc_ver_doc_type)
    RelativeLayout idVcVerDocType;
    @BindView(R.id.et_vc_country_name)
    TextView etVcCountryName;
    @BindView(R.id.id_vc_ver_doc_country)
    RelativeLayout idVcVerDocCountry;
    private VirtualcardRegisterVerifyEntity mVirtualcardRegisterVerifyEntityIn;
    @BindView(R.id.id_sel_arrow_code)
    ImageView idSelArrowCode;
    @BindView(R.id.tv_region_code_str)
    TextView idTvCodeStr;
    private final String TAG_ID_NUMBER = "TAG_ID_NUMBER";
    private final String TAG_HPONE_NUMBER = "TAG_HPONE_NUMBER";
    private int mActionFlow;
    public static int CHOICE_AREA_CODE = 1002;
    public static int CHOICE_COUNTRY_CODE = 1003;
    private int choice_area_position = -1;
    private int choice_id_position = -1;
    private String idCardType, countryType;

    @Override
    public void init() {
        mActionFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mActionFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //忘记密码
            setToolBarTitle(R.string.setting_payment_short);
        } else if (mActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            setToolBarTitle(R.string.IDV_2_1);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        idTvCodeStr.setText(Constants.DATA_COUNTRY_CODES[1]);
        idSelArrowCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAreaCode();
            }
        });
        idTvCodeStr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAreaCode();
            }
        });
        idVirtualcardIdcardNumber.setTag(TAG_ID_NUMBER);
        idVirtualcardPhonenumber.setTag(TAG_HPONE_NUMBER);
        idVirtualcardIdcardNumber.addTextChangedListener(onTextChangeeListener);
        idVirtualcardPhonenumber.addTextChangedListener(onTextChangeeListener);
        idVirtualcardIdcardNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10), new NormalInputFilter(CHARSEQUENCE),});
        idVirtualcardPhonenumber.getEditText().setFilters(AndroidUtils.getPhoneInputFilter(Constants.DATA_COUNTRY_CODES[1]));
        idVirtualcardIdcardNumber.getEditText().setTransformationMethod(new TransInformation());

        updateNextBtnStatus();
        //默认选择香港身份证
        et_idcard_type.setText(getString(R.string.VC04_03_4a));
        et_idcard_type.setTextColor(Color.BLACK);

        //默认选择中国香港
        etVcCountryName.setText(getString(R.string.VC04_03_5a));
        etVcCountryName.setTextColor(Color.BLACK);

        idCardType = Constants.DATA_ID_CARD_TYPE[0];
        countryType = Constants.DATA_ID_COUNRTY_TYPE[0];

        idVcVerDocType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceIDCard();
            }
        });

        idVcVerDocCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceArea();
            }
        });
        //默认选中中国香港
        choice_area_position = 0;
        //默认选择香港身份证
        choice_id_position = 0;
    }

    /**
     * 选择身份证类型
     */
    private void choiceIDCard() {
        Intent intent = new Intent(VirtualCardIdCardVerificationActivity.this, IDCardSelectionActivity.class);
        intent.putExtra(Constants.CHOICE_AREA_POSITION, choice_id_position);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mActionFlow);
        startActivityForResult(intent, CHOICE_AREA_CODE);
    }

    /**
     * 选择签发国家
     */
    private void choiceArea() {
        Intent intent = new Intent(VirtualCardIdCardVerificationActivity.this, AreaSelectionForVirtualCardActivity.class);
        intent.putExtra(Constants.CHOICE_AREA_POSITION, choice_area_position);
        startActivityForResult(intent, CHOICE_COUNTRY_CODE);
    }


    private final TextWatcher onTextChangeeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            updateNextBtnStatus();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void updateNextBtnStatus() {
        String idCardStr = idVirtualcardIdcardNumber.getText().toString();
        String idPhoneStr = idVirtualcardPhonenumber.getText().toString();
        String idvCode = idTvCodeStr.getText().toString();
        boolean isPhoneEnabled = false;
        if (TextUtils.equals(idvCode, Constants.DATA_COUNTRY_CODES[0])) {
            isPhoneEnabled = idPhoneStr.length() >= 11;
        } else if (TextUtils.equals(idvCode, Constants.DATA_COUNTRY_CODES[1])) {
            isPhoneEnabled = idPhoneStr.length() >= 8;
        } else if (TextUtils.equals(idvCode, Constants.DATA_COUNTRY_CODES[2])) {
            isPhoneEnabled = idPhoneStr.length() >= 8;
        } else {
            isPhoneEnabled = idPhoneStr.length() > 0;
        }
        idVirtualcardConfirm.setEnabled(!TextUtils.isEmpty(idCardStr) &&
                isPhoneEnabled &&
                !TextUtils.isEmpty(et_idcard_type.getText().toString()) &&
                !TextUtils.isEmpty(etVcCountryName.getText().toString()));
    }


    private void changeAreaCode() {
        if (ButtonUtils.isFastDoubleClick()) return;
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.LAST_SELECT_COUNTRY, idTvCodeStr.getText().toString());
        ActivitySkipUtil.startAnotherActivityForResult(VirtualCardIdCardVerificationActivity.this, SelectLoginCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_COUNTRY);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_verification;
    }

    @Override
    protected RegisterVitualCardVerifyIdCardContract.Presenter createPresenter() {
        return new VirtualCardRegisterPresenter();
    }


    @OnClick(R.id.id_virtualcard_confirm)
    public void onViewClicked() {
        mVirtualcardRegisterVerifyEntityIn = new VirtualcardRegisterVerifyEntity();
        String cardNumberStr = idVirtualcardIdcardNumber.getText().toString().trim().toUpperCase();
        String phoneNumberStr = idVirtualcardPhonenumber.getText().toString().trim();
        phoneNumberStr = idTvCodeStr.getText().toString().replace("+", "") + "-" + phoneNumberStr;
        mVirtualcardRegisterVerifyEntityIn.setPhoneNo(phoneNumberStr);
        mVirtualcardRegisterVerifyEntityIn.setNid(cardNumberStr);
        mVirtualcardRegisterVerifyEntityIn.setNidType(idCardType);
        mVirtualcardRegisterVerifyEntityIn.setAction(mActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER ? ApiConstant.VIRTUALCARD_REGISTER : ApiConstant.VIRTUALCARD_FORGETPWD);
        mVirtualcardRegisterVerifyEntityIn.setNidRgn(countryType);
        mPresenter.virtualCardJudgeIsBocCustomer(mVirtualcardRegisterVerifyEntityIn);
    }

    @Override
    public void virtualCardJudgeIsBocCustomerSuccess(VitualCardIsBocEntity response) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, mActionFlow);
        maps.put(Constants.VIRTUALCARDREGISTERVERIFINVO, mVirtualcardRegisterVerifyEntityIn);
        if (response.getBoccus().equals("1")) {
            ActivitySkipUtil.startAnotherActivity(this, RegisterBankActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(this, InputBankCardNumberActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void virtualCardJudgeIsBocCustomerError(String errorCode, String errorMsg) {

        showErrorMsgDialog(VirtualCardIdCardVerificationActivity.this, errorMsg);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_SEL_COUNTRY && resultCode == RESULT_OK) {
            String countryCode = data.getExtras().getString(Constants.DATA_COUNTRY_CODE);
            idTvCodeStr.setText(countryCode);
            idVirtualcardPhonenumber.getEditText().setText("");
            idVirtualcardPhonenumber.getEditText().setFilters(AndroidUtils.getPhoneInputFilter(countryCode));
        } else if (requestCode == CHOICE_AREA_CODE && resultCode == 1001) {
            //TOTO 临时写 优化 fix shouhu
            String result = data.getStringExtra(Constants.CHOICE_AREA);
            if (!TextUtils.isEmpty(result)) {
                et_idcard_type.setText(result);
                et_idcard_type.setTextColor(Color.BLACK);
                idVirtualcardIdcardNumber.getEditText().setText("");
                if (result.equals(getString(R.string.VC04_03_4a))) {
                    idCardType = Constants.DATA_ID_CARD_TYPE[0];
                    idVirtualcardIdcardNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10), new NormalInputFilter(CHARSEQUENCE),});
                } else if (result.equals(getString(R.string.VC04_03_4b))) {
                    idCardType = Constants.DATA_ID_CARD_TYPE[1];
                    idVirtualcardIdcardNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10), new NormalInputFilter(CHARSEQUENCE),});
                } else {
                    idCardType = Constants.DATA_ID_CARD_TYPE[2];
                    idVirtualcardIdcardNumber.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(18), new NormalInputFilter(CHARSEQUENCE),});
                }
            }
            choice_id_position = data.getIntExtra(Constants.CHOICE_AREA_POSITION, 0);
        } else if (requestCode == CHOICE_COUNTRY_CODE && resultCode == 1001) {
            //TOTO 临时写 优化 fix shouhu
            String result = data.getStringExtra(Constants.CHOICE_AREA);
            if (!TextUtils.isEmpty(result)) {
                etVcCountryName.setText(result);
                etVcCountryName.setTextColor(Color.BLACK);
                if (result.equals(getString(R.string.VC04_03_5a))) {
                    countryType = Constants.DATA_ID_COUNRTY_TYPE[0];
                } else if (result.equals(getString(R.string.VC04_03_5b))) {
                    countryType = Constants.DATA_ID_COUNRTY_TYPE[1];
                } else {
                    countryType = Constants.DATA_ID_COUNRTY_TYPE[2];
                }
            }
            choice_area_position = data.getIntExtra(Constants.CHOICE_AREA_POSITION, 0);
        }
    }
}
