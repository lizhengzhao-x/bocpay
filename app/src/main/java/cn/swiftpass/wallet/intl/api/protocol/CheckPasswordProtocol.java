package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 检验支付密码，返回对象为 CheckPasswordEntity
 * e2ee
 */

public class CheckPasswordProtocol extends E2eeProtocol {
    public static final String TAG = CheckPasswordProtocol.class.getSimpleName();
    /**
     * 钱包登陆的原密码
     */
    //String mPayPwd;
    String mAccount;
    String mAction;

    public CheckPasswordProtocol(String payPwd, NetWorkCallbackListener dataCallback) {
        mPasscode = payPwd;
        this.mDataCallback = dataCallback;
        mUrl = "api/security/decrypt";
    }

    //没登录的需要传account
    public CheckPasswordProtocol(String payPwd, String account, NetWorkCallbackListener dataCallback) {
        mPasscode = payPwd;
        mAccount = account;
        this.mDataCallback = dataCallback;
        mUrl = "api/security/decrypt";
    }

    public CheckPasswordProtocol(String payPwd, String action, String type, NetWorkCallbackListener dataCallback) {
        mAction = action;
        mPasscode = payPwd;
        this.mDataCallback = dataCallback;
        mUrl = "api/security/decrypt";
    }

    public CheckPasswordProtocol(String payPwd, String action, int value, NetWorkCallbackListener dataCallback) {
        mAction = action;
        mPasscode = payPwd;
        this.mDataCallback = dataCallback;
        mUrl = "api/security/decrypt";
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(mAction)) {
            mBodyParams.put(RequestParams.ACTION, mAction);
        }
        mBodyParams.put(RequestParams.PASSCODE, mPasscode);
        mBodyParams.put(RequestParams.ACCOUNT, TextUtils.isEmpty(mAccount) ? "" : mAccount);
    }
}
