package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;

/**
 * 发送验证码
 */
public class TransferFirstSendOtpContract {

    public interface View extends IView {

        void transferFirstSenOtpSuccess(ContentEntity response, boolean isTryAgain);

        void transferFirstSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void transferFirstVerifyOtpSuccess(ContentEntity response);

        void transferFirstVerifyOtpError(String errorCode, String errorMsg);

        void transferConfrimSuccess(TransferCompleteEntity response);

        void transferConfirmError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<TransferFirstSendOtpContract.View> {

        void transferFirstSenOtp(String txnId, boolean isTryAgain);

        void transferFirstVerifyOtp(String txnId, String verifyCode, TransferPreCheckEntity mPreCheckEntity, boolean isLiShi, boolean isBocPay);

        void transferConfirm(TransferPreCheckEntity mPreCheckEntity, boolean isLiShi, boolean isBocPay);

    }

}
