package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.RegisterAccountEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;


/**
 * s2用户注册成功
 */
public class STwoRegisterSuccessActiviy extends BaseCompatActivity {


    private TextView mId_register_number;
    private TextView mId_register_mailbox;
    private TextView mTv_start_now, id_register_message;
    private ImageView id_loading_icon;
    private TextView idInviteIntro;
    private RegisterAccountEntity registerAccountEntity;
    private int currentType;
    private TextView tv_fast, tv_pay;
    private MySmartAccountEntity mSmartAccountInfo;
    //注册成功的描述
    private String rewardtip;
    //是否需要跳转到输入邀请码的界面
    private Boolean showInvitePage;

    private void bindViews() {
        tv_pay = (TextView) findViewById(R.id.tv_pay);
        tv_fast = (TextView) findViewById(R.id.tv_fast);
        mId_register_number = (TextView) findViewById(R.id.id_register_number);
        idInviteIntro = (TextView) findViewById(R.id.id_invite_intro);
        mId_register_mailbox = (TextView) findViewById(R.id.id_register_mailbox);
        mTv_start_now = (TextView) findViewById(R.id.tv_start_now);
        id_loading_icon = (ImageView) findViewById(R.id.id_loading_icon);
        id_register_message = (TextView) findViewById(R.id.id_register_message);
        mTv_start_now.setOnClickListener(onClickListener);


        tv_pay.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //edda
                getSmartAccountInfo();
            }
        });

        tv_fast.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //fps
                refreshRecords();

            }
        });

    }

    public boolean isBindCardEvent() {

        return currentType == Constants.PAGE_FLOW_BIND_PA;
    }

    @Override
    public boolean isAnalysisPage() {
        if (currentType == Constants.PAGE_FLOW_REGISTER_PA || currentType == Constants.PAGE_FLOW_BIND_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.ONE_TIME_PASSCODE;
        analysisPageEntity.current_address = PagerConstant.REGISTRATION_DONE;
        return analysisPageEntity;
    }


    private void refreshRecords() {
        ApiProtocolImplManager.getInstance().fpsQueryBankRecords(this, new NetWorkCallbackListener<FpsBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(STwoRegisterSuccessActiviy.this, errorMsg);
            }

            @Override
            public void onSuccess(FpsBankRecordsEntity response) {
                ActivitySkipUtil.startAnotherActivity(STwoRegisterSuccessActiviy.this, FpsManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    /**
     * 绑定edda
     */
    private void getRegEddaInfo() {
        ApiProtocolImplManager.getInstance().getRegEddaInfo(this, new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(STwoRegisterSuccessActiviy.this, errorMsg);
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
                mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
                mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
                mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
                mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, true);
                ActivitySkipUtil.startAnotherActivity(STwoRegisterSuccessActiviy.this, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    /**
     * 获取我的账户信息
     */
    private void getSmartAccountInfo() {
        NetWorkCallbackListener<MySmartAccountEntity> callback = new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(STwoRegisterSuccessActiviy.this, errorMsg);
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                mSmartAccountInfo = response;
                getRegEddaInfo();
            }
        };
        ApiProtocolImplManager.getInstance().getSmartAccountInfo(this, callback);
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        if (null == getIntent() || null == getIntent().getExtras()) {
            finish();
            return;
        }
        hideBackIcon();
        bindViews();
        mTv_start_now.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        mTv_start_now.getPaint().setAntiAlias(true);
        DataCollectManager.getInstance().sendCompleteRegister();
        currentType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (currentType == Constants.PAGE_FLOW_BIND_PA) {
            setToolBarTitle(R.string.IDV_3_1a);
        } else {
            setToolBarTitle(R.string.IDV_2_1);
        }
        registerAccountEntity = (RegisterAccountEntity) getIntent().getExtras().getSerializable(Constants.REGISTER_SUCCESS_DATA);
        mId_register_number.setText(registerAccountEntity.getMobile());
        mId_register_mailbox.setText(registerAccountEntity.getEmail());
        rewardtip = getIntent().getExtras().getString(Constants.REWARDTIP);
        showInvitePage = getIntent().getExtras().getBoolean(Constants.SHOWINVITEPAGE);
        if (!TextUtils.isEmpty(rewardtip)) {
            idInviteIntro.setText(rewardtip);
            idInviteIntro.setVisibility(View.VISIBLE);
        } else {
            idInviteIntro.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(registerAccountEntity.getIsAudit())) {
//            isAudit 是1的话，就显示审核中
            if (registerAccountEntity.getIsAudit().equals("1")) {
                id_register_message.setText(getString(R.string.register_loading));
                id_loading_icon.setImageResource(R.mipmap.register_loading);
            }
        }
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ButtonUtils.isFastDoubleClick()) {
                if (showInvitePage) {
                    //首次注册
                    Intent intent = new Intent(STwoRegisterSuccessActiviy.this, RegisterInviteActivity.class);
//                    intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//                    intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
                    intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
                    startActivity(intent);
                } else {
                    if (currentType == Constants.PAGE_FLOW_BIND_PA) {
                        //信用卡用户绑定我的账户
                        MyActivityManager.removeAllTaskExcludeMainStack();
                        ActivitySkipUtil.startAnotherActivity(STwoRegisterSuccessActiviy.this, MainHomeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
                        TempSaveHelper.setIsCheckIdSuccess(false);
                        finish();
                    } else {
                        //注册
                        //点击完成 需要结束整个注册流程中涉及到的activity
                        TempSaveHelper.setIsCheckIdSuccess(false);
                        MyActivityManager.removeAllTaskExcludeMainStack();
                        HttpCoreKeyManager.getInstance().setUserId(registerAccountEntity.getCards().getRows().get(0).getUserId() + "");
                        Intent intent = new Intent(STwoRegisterSuccessActiviy.this, MainHomeActivity.class);
//                        intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//                        intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
                        intent.putExtra(Constants.ISREGESTER, true);
                        startActivity(intent);
                    }
                }
            }
        }
    };

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_s2_register_success;
    }
}
