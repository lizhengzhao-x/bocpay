package cn.swiftpass.wallet.intl.module.transfer.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/12/2.
 */
public class ApkVerifyEntity extends BaseEntity {

    @Override
    public String toString() {
        return "ApkVerifyEntity{" +
                "code='" + code + '\'' +
                '}';
    }

    public boolean isVerifySuccess() {
        if (TextUtils.isEmpty(code)) {
            return false;
        }
        return code.equals("1");
    }

    /**
     * code : 1
     */

    /**
     * -- 1表示pass   0表示拒绝
     */
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}