package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


public class TransactionManagementFragment extends BaseFragment {

    @BindView(R.id.id_boc_pay)
    TextView idBocPay;
    @BindView(R.id.id_smart_account)
    TextView idSmartAccount;
    @BindView(R.id.id_credit_card)
    TextView idCreditCard;
    private FragmentManager mFragmentManager;
    private Fragment mCurrentFragment;
    private TransacSmartAccountFragment transacSmartAccountFragment;
    private TransacCreditCardFragment transacCreditCardFragment;
    private BocPayTransacFragment bocPayTransacFragment;
    private int lastSelPosition;
    public static String JUMP_PAGE_POSITION = "JUMP_PAGE_POSITION";


    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.card_manager_transation);
        }
    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transaction;
    }

    @Override
    protected void initView(View v) {

        bocPayTransacFragment = new BocPayTransacFragment();
        transacSmartAccountFragment = new TransacSmartAccountFragment();
        transacCreditCardFragment = new TransacCreditCardFragment();
        mFragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        if (getArguments() != null && getArguments().getInt(JUMP_PAGE_POSITION, -1) != -1) {
            //临时加上 派利是弹框 点击跳转到智能账户记录
            mCurrentFragment = transacSmartAccountFragment;
            setDefaultSelect();
            idSmartAccount.setBackgroundResource(R.drawable.center_smart_btn_bac);
            idSmartAccount.setTextColor(getResources().getColor(R.color.app_white));
        } else {
            mCurrentFragment = bocPayTransacFragment;
        }
        fragmentTrasaction.add(R.id.fl_transaction, mCurrentFragment);
        fragmentTrasaction.commitAllowingStateLoss();

    }


    public void checkMoreEvent() {
        verifyPwdAction();
    }


    private void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                updateCheckMoreData();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


//    /**
//     * 其他页面切换到当前页面的时候 判断当前页面显示是否为空 然后自动加载
//     */
//    public void retryUpdateView() {
//        if (lastSelPosition == 1 && transacSmartAccountFragment != null) {
//            transacSmartAccountFragment.updateList();
//        }
//
//        if (lastSelPosition == 2 && transacCreditCardFragment != null) {
//            transacCreditCardFragment.updateList();
//        }
//        if (lastSelPosition == 0 && bocPayTransacFragment != null) {
//            bocPayTransacFragment.updateList();
//        }
//    }

    /**
     * 显示更多
     */
    public void updateCheckMoreData() {
        transacSmartAccountFragment.updateCheckMoreUI();
        transacCreditCardFragment.updateCheckMoreUI();
        bocPayTransacFragment.updateCheckMoreUI();
    }

    @OnClick({R.id.id_boc_pay, R.id.id_smart_account, R.id.id_credit_card})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) return;
        setDefaultSelect();
        switch (view.getId()) {
            case R.id.id_boc_pay:
                idBocPay.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
                idBocPay.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(bocPayTransacFragment, R.id.fl_transaction, 0);
                break;
            case R.id.id_smart_account:
                idSmartAccount.setBackgroundResource(R.drawable.center_smart_btn_bac);
                idSmartAccount.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(transacSmartAccountFragment, R.id.fl_transaction, 1);
                break;
            case R.id.id_credit_card:
                idCreditCard.setBackgroundResource(R.drawable.right_china_btn_bac);
                idCreditCard.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(transacCreditCardFragment, R.id.fl_transaction, 2);
                break;
            default:
                break;
        }
    }

    private void setDefaultSelect() {
        idSmartAccount.setBackgroundResource(R.drawable.center_smart_btn_nosel_bac);
        idSmartAccount.setTextColor(getResources().getColor(R.color.font_gray_three));
        idBocPay.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        idBocPay.setTextColor(getResources().getColor(R.color.font_gray_three));
        idCreditCard.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        idCreditCard.setTextColor(getResources().getColor(R.color.font_gray_three));
    }

    protected void switchDiffFragmentContent(Fragment toFragment, int resId, int index) {
        if (null == mCurrentFragment || null == toFragment) {
            return;
        }
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        if (!toFragment.isAdded()) {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.add(resId, toFragment, String.valueOf(index));
            fragmentTrasaction.commitAllowingStateLoss();
        } else {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.show(toFragment);
            fragmentTrasaction.commitAllowingStateLoss();
        }
        mCurrentFragment = toFragment;
        lastSelPosition = index;
        if (toFragment instanceof TransacCreditCardFragment) {
            transacCreditCardFragment.updateList();
        }
    }


    public void clearDatalist() {

        if (bocPayTransacFragment != null) {
            bocPayTransacFragment.clearData();
            transacSmartAccountFragment.clearData();
            transacCreditCardFragment.clearData();
        }
    }
}
