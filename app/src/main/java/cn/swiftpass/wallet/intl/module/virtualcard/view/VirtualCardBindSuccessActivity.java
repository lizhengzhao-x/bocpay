package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.content.Intent;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.event.JumpCenterNotificationEvent;
import cn.swiftpass.wallet.intl.event.RegisterPopBannerDialogEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardVeryOtpEntiy;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * Created by ZhangXinchao on 2019/11/23.
 * 虚拟卡绑卡成功
 */
public class VirtualCardBindSuccessActivity extends BaseCompatActivity {
    private int currentActionType;
    private String mCardId;

    @Override
    public void init() {
        if (null == getIntent() || null == getIntent().getExtras()) {
            finish();
            return;
        }
        hideBackIcon();
        currentActionType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        mCardId = getIntent().getStringExtra(Constants.VIRTUALCARDID);
        if (currentActionType == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            setToolBarTitle(getString(R.string.IDV_24_1));
        } else {
            setToolBarTitle(R.string.VC02_01a_1);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_bindsuccess;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @OnClick(R.id.tv_card_info_nextPage)
    public void onViewClicked() {
        if (currentActionType == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册成功
            MyActivityManager.removeAllTaskExcludeMainStack();
            Intent intent = new Intent(VirtualCardBindSuccessActivity.this, MainHomeActivity.class);
//            intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
//            intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
            addRegisterSuccessEvent(getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER), getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
            intent.putExtra(Constants.ISREGESTER, true);
            startActivity(intent);
        } else {
            //虚拟卡绑定成功界面
            VirtualCardVeryOtpEntiy response = (VirtualCardVeryOtpEntiy) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDLISTBEAN);
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, response);
            mHashMaps.put(Constants.VIRTUALCARDID, mCardId);
            ActivitySkipUtil.startAnotherActivity(VirtualCardBindSuccessActivity.this, VirtualCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
            this.finish();
        }
    }

    private void addRegisterSuccessEvent(String popUpBanner, boolean jumpMessageCenter) {
        if (!TextUtils.isEmpty(popUpBanner)) {
            RegisterPopBannerDialogEvent registerPopBannerDialogEvent = new RegisterPopBannerDialogEvent();
            registerPopBannerDialogEvent.updateEventParams(RegisterPopBannerDialogEvent.POPUPBANNERURL, popUpBanner);
            UserLoginEventManager.getInstance().addUserLoginEvent(registerPopBannerDialogEvent);
        }
        if (jumpMessageCenter) {
            JumpCenterNotificationEvent jumpCenterNotificationEvent = new JumpCenterNotificationEvent();
            UserLoginEventManager.getInstance().addUserLoginEvent(jumpCenterNotificationEvent);
        }
    }

    @Override
    public void onBackPressed() {

    }
}
