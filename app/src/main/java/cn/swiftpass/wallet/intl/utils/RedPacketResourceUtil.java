package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import java.io.File;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;

/**
 * 红包资源文件下载
 */
public class RedPacketResourceUtil {
    //普通利是的背景
    private String redBgGifComon;
    //普通利是的Mp3
    private String redBgMp3Comon;
    //员工利是的背景
    private String redBgGifStaff;
    //员工利是的Mp3
    private String redBgMp3Staff;
    private Context mContext;
    private String languageType;

    public RedPacketResourceUtil(Context mContext, String redBgGifComon, String redBgMp3Comon, String redBgGifStaff, String redBgMp3Staff, String languageType) {
        this.redBgGifComon = redBgGifComon;
        this.redBgMp3Comon = redBgMp3Comon;
        this.redBgGifStaff = redBgGifStaff;
        this.redBgMp3Staff = redBgMp3Staff;
        this.mContext = mContext;
        this.languageType = languageType;
        downResource();
    }

    /**
     * 根据背景和url来得到资源 如果没有下载就下载，返回本地地址
     *
     * @param type
     * @param url
     * @return
     */
    public static Uri getResource(Context mContext, String type, String url, String languageType) {
        //如果是没有返回 音乐的url 就表示不需要播放音乐
        if (!type.contains("gif")) {
            if (TextUtils.isEmpty(url)) {
                return null;
            }
        }
        String localUrl = getLocalPath(mContext, type, url, languageType);
        String defaultPath;
        Uri defalutUri = null;
        if (type.contains("gif")) {
            defaultPath = getDefaultPath(mContext, type);
        } else {
            //如果是没有返回 音乐的url 就表示不需要播放音乐
            if (TextUtils.isEmpty(url)) {
                return null;
            }
            defaultPath = getDefaultPath(mContext, type);

        }
        defalutUri = Uri.parse(defaultPath);

        /**
         * 如果没有保存url就重新下载
         */
        if (TextUtils.isEmpty(localUrl)) {
            downFile(mContext, type, url, languageType);
            return defalutUri;
        }

        /**
         * 如果文件存在就返回 文件的uri 否则就下载文件 返回本地uri
         */
        File file = new File(localUrl);
        if (file.exists() && file.length() > 0) {
            return Uri.parse(localUrl);
        }
        downFile(mContext, type, url, languageType);
        return defalutUri;

    }

    /**
     * 得到存在本地图片
     *
     * @param mContext
     * @param type
     * @param url
     * @return
     */
    private static String getLocalPath(Context mContext, String type, String url, String languageType) {
        if (null == mContext) {
            return "";
        }
        if (TextUtils.isEmpty(url)) {
            return "";
        }

        String localPath = FileDownloadUtil.getExistDir(mContext, type, languageType);
        File dir = new File(localPath);
        if (dir == null || !dir.exists() || !dir.isDirectory()) {
            return "";
        }
        for (File file : dir.listFiles()) {
            //文件存在就保存
            LogUtils.i("FILEXXX", "'URL: filename " + file.getName());
            String fileName = file.getName();
//            if (TextUtils.equals(type, Constants.STAFF_MP3)) {
//                fileName = AndroidUtils.md5(fileName.replace(".mp3", "")) + ".mp3";
//            }
            if (TextUtils.equals(FileDownloadUtil.getNameFromUrl(url, type), fileName)) {
                if (file.length() > 0) {
                    return file.getAbsolutePath();
                } else {
                    //如果文件不对 就删除文件
                    try {
                        file.delete();
                    } catch (Exception e) {

                    }
                }

            }
        }
        return "";
    }


    //删除文件夹和文件夹里面的文件
    private static void deleteDirWihtFile(File dir) {
        if (dir == null || !dir.exists() || !dir.isDirectory())
            return;
        for (File file : dir.listFiles()) {
            if (file.isFile()) {
                file.delete(); // 删除所有文件
            } else if (file.isDirectory()) {
                deleteDirWihtFile(file); // 递规的方式删除文件夹
            }
        }
        dir.delete();// 删除目录本身
    }

    /**
     * 下载文件
     */
    private void downResource() {
        downFile(mContext, Constants.COMON_GIF, redBgGifComon, languageType);
        downFile(mContext, Constants.COMON_MP3, redBgMp3Comon, languageType);
        downFile(mContext, Constants.STAFF_GIF, redBgGifStaff, languageType);
        downFile(mContext, Constants.STAFF_MP3, redBgMp3Staff, languageType);
    }


    /**
     * 下载文件
     *
     * @param mContext
     * @param type
     * @param url
     */
    private static void downFile(Context mContext, String type, String url, String languageType) {
        //url 为null 不需要下载
        if (TextUtils.isEmpty(url)) {
            return;
        }
        //如果找不到本地的文件path  就下载文件
        if (TextUtils.isEmpty(getLocalPath(mContext, type, url, languageType))) {
            FileDownloadUtil.get(mContext).download(url, type, languageType);
        }

    }

    public static String getDefaultPath(Context mContext, String type) {
        //员工利是和普通利是的默认图不一样
        if (type.contains("mp3")) {
            return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_secs;
        } else {
            //员工利是和普通利是的默认图不一样
            return getDefaultUrl(mContext, type);
        }
    }

    /**
     * 根据国际化来显示默认图
     *
     * @param mContext
     * @param type
     * @return
     */
    private static String getDefaultUrl(Context mContext, String type) {
        if (null == mContext) {
            return "";
        }

        String lan = SpUtils.getInstance().getAppLanguage();
        if (TextUtils.equals(type, Constants.STAFF_GIF)) {
            if (AndroidUtils.isZHLanguage(lan)) {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_staff_cn;
            } else if (AndroidUtils.isHKLanguage(lan)) {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_staff_hk;
            } else {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_staff_en;
            }
        } else {
            if (AndroidUtils.isZHLanguage(lan)) {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_common_cn;
            } else if (AndroidUtils.isHKLanguage(lan)) {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_common_hk;
            } else {
                return "android.resource://" + mContext.getPackageName() + "/" + R.raw.lishi_common_en;
            }
        }
    }

    /**
     * 加载失败
     * 根据国际化来显示默认图
     *
     * @param mContext
     * @param type
     * @return
     */
    public static int getFailDefaultUrl(String type) {
        String lan = SpUtils.getInstance().getAppLanguage();
        if (TextUtils.equals(type, Constants.STAFF_GIF)) {
            if (AndroidUtils.isZHLanguage(lan)) {
                return R.raw.lishi_staff_cn;
            } else if (AndroidUtils.isHKLanguage(lan)) {
                return R.raw.lishi_staff_hk;
            } else {
                return R.raw.lishi_staff_en;
            }
        } else {
            if (AndroidUtils.isZHLanguage(lan)) {
                return R.raw.lishi_common_cn;
            } else if (AndroidUtils.isHKLanguage(lan)) {
                return R.raw.lishi_common_hk;
            } else {
                return R.raw.lishi_common_en;
            }
        }
    }

}
