package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */

public class CheckPAAcountVerifyCodeProtocol extends BaseProtocol {

    String mMobile;
    String mVerifyCode;
    String mAction;

    public CheckPAAcountVerifyCodeProtocol(String mobile, String verifyCode, String action, NetWorkCallbackListener dataCallback){
        this.mDataCallback = dataCallback;
        this.mMobile = mobile;
        this.mAction = action;
        this.mVerifyCode = verifyCode;
        mUrl = "api/smartReg/getTxnId";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
    }

}
