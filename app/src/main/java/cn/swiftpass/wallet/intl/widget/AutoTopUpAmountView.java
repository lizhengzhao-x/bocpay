package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * Created by  2018/8/2.
 * 显示两行
 */

public class AutoTopUpAmountView extends RelativeLayout {
    private Context mContext;
    TextView mCurrencyTV;
    TextView mAmountTV;
    private boolean mCheck = false;


    public boolean isCheck() {
        return mCheck;
    }

    public void setCheck(boolean check) {
        this.mCheck = check;
        if (mCheck) {
            setBackgroundResource(R.drawable.auto_topup_border_hover);
        } else {
            setBackgroundResource(R.drawable.auto_topup_border_normal);
        }
    }

    public AutoTopUpAmountView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public AutoTopUpAmountView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public AutoTopUpAmountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.item_auto_topup, this);
        mCurrencyTV = rootView.findViewById(R.id.tv_auto_topup_currency);
        mAmountTV = rootView.findViewById(R.id.tv_auto_topup_amount);
    }

    public void setCurrency(String currency) {
        if (!TextUtils.isEmpty(currency)) {
            mCurrencyTV.setText(currency);
        }
    }

    public String getCurrency(String currency) {
        return mCurrencyTV.getText().toString();
    }

    public void setAmount(String amount) {
        if (!TextUtils.isEmpty(amount)) {
            mAmountTV.setText(amount);
        }
    }

    public int getAmount() {
        return Integer.valueOf(mAmountTV.getText().toString());
    }

}
