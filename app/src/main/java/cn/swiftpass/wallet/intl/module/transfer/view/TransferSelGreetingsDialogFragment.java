package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.transfer.entity.GreetingTxtEntity;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.TransferGreetingTxtAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * 派利是  选择祝福语句
 */
public class TransferSelGreetingsDialogFragment extends DialogFragment {


    private ImageView mId_back_image;
    private androidx.recyclerview.widget.RecyclerView mId_grettings_recycleview;


    public OnTransferGreetingTxtSelClickListener getOnTransferGreetingTxtSelClickListener() {
        return onTransferGreetingTxtSelClickListener;
    }

    public void setOnTransferGreetingTxtSelClickListener(OnTransferGreetingTxtSelClickListener onTransferGreetingTxtSelClickListener) {
        this.onTransferGreetingTxtSelClickListener = onTransferGreetingTxtSelClickListener;
    }

    private OnTransferGreetingTxtSelClickListener onTransferGreetingTxtSelClickListener;

    public void setmGreetingTxts(List<GreetingTxtEntity> mGreetingTxtsIn) {
        mGreetingTxts = new ArrayList<>();
        this.mGreetingTxts.addAll(mGreetingTxtsIn);
        //mId_grettings_recycleview.getAdapter().notifyDataSetChanged();
    }

    private ArrayList<GreetingTxtEntity> mGreetingTxts;

    private void bindViews(View view) {
        mId_back_image = (ImageView) view.findViewById(R.id.id_back_image);
        mId_back_image = (ImageView) view.findViewById(R.id.id_back_image);
        mId_grettings_recycleview = (androidx.recyclerview.widget.RecyclerView) view.findViewById(R.id.id_grettings_recycleview);

        int spanCount = 3;
        String lan = SpUtils.getInstance(getContext()).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {

        } else if (AndroidUtils.isHKLanguage(lan)) {

        } else {
            spanCount = 1;
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), spanCount);
        mId_grettings_recycleview.setLayoutManager(gridLayoutManager);

        mId_grettings_recycleview.setAdapter(getAdapterWithGreetings(mGreetingTxts));
        mId_grettings_recycleview.getAdapter().notifyDataSetChanged();

        mId_back_image.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (onTransferGreetingTxtSelClickListener != null) {
                    onTransferGreetingTxtSelClickListener.onBackBtnClickListener();
                }
            }
        });
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_sel_transfer_greetings, container, false);
        bindViews(view);
        updateUI();
        return view;
    }

    private void updateUI() {


    }


    public interface OnTransferGreetingTxtSelClickListener {
        void onBackBtnClickListener();

        void onTransferGreetingTxtSeflListener(int positon);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    private CommonRcvAdapter<GreetingTxtEntity> getAdapterWithGreetings(final List<GreetingTxtEntity> data) {
        return new CommonRcvAdapter<GreetingTxtEntity>(data) {

            @Override
            public Object getItemType(GreetingTxtEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new TransferGreetingTxtAdapter(getContext(), new TransferGreetingTxtAdapter.GreetingTxtClickCallback() {
                    @Override
                    public void onItemClick(int position) {
                        super.onItemClick(position);
                        onTransferGreetingTxtSelClickListener.onTransferGreetingTxtSeflListener(position);
                    }
                });
            }
        };
    }

}
