package cn.swiftpass.wallet.intl.module.rewardregister.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class RewardPansListEntity extends BaseEntity {

    // {\"cardType\":\"U\",\"pan\":\"6888888888888880\",\"panShowNumber\":\"************8880\"}

    public List<RewardPan> getCardList() {
        return creditCards;
    }

    public void setCardList(List<RewardPan> cardList) {
        this.creditCards = cardList;
    }

    private List<RewardPan> creditCards;


    public static class RewardPan extends BaseEntity {

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getPan() {
            return pan;
        }

        public void setPan(String pan) {
            this.pan = pan;
        }

        public String getPanShowNumber() {
            return panShowNumber;
        }

        public void setPanShowNumber(String panShowNumber) {
            this.panShowNumber = panShowNumber;
        }

        private String cardType;
        private String pan;
        private String panShowNumber;

        private boolean isSel;

        public boolean isSel() {
            return isSel;
        }

        public void setSel(boolean sel) {
            isSel = sel;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        private String type;
    }


}
