package cn.swiftpass.wallet.intl.module.home;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.widget.recyclerview.ExpandableViewHoldersUtil;

public class MenuItemSecondHolder extends RecyclerView.ViewHolder implements ExpandableViewHoldersUtil.Expandable {
    @BindView(R.id.ll_sec_item)
    LinearLayout llSecondItem;
    @BindView(R.id.ll_second_level)
    LinearLayout llSecondLevel;
    @BindView(R.id.iv_second)
    ImageView ivSecond;
    @BindView(R.id.iv_second_down)
    ImageView ivSecondDown;
    @BindView(R.id.tv_second_title)
    TextView tvSecondTitle;
    @BindView(R.id.red_menu_line)
    ImageView redMenuLine;
    @BindView(R.id.con_second)
    ConstraintLayout conSecond;

    public MenuItemSecondHolder(@NonNull View secondView) {
        super(secondView);
        ButterKnife.bind(this, secondView);
    }

    @Override
    public View getExpandView() {
        return llSecondLevel;
    }
}
