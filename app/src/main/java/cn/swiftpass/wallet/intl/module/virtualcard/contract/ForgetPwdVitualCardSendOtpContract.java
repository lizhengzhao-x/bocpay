package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardForgetVerifyOtpEntity;

/**
 * 发送验证码
 */
public class ForgetPwdVitualCardSendOtpContract {

    public interface View extends IView {

        void vitualCardSenOtpSuccess(ContentEntity response, boolean isTryAgain);

        void vitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void vitualCardVerifyOtpSuccess(VirtualCardForgetVerifyOtpEntity response);

        void vitualCardVerifyOtpError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<ForgetPwdVitualCardSendOtpContract.View> {

        void vitualCardSenOtp(String action, String checkType, boolean isTryAgain);

        void vVitualCardVerifyOtp(String action, String checkType, String verifyCode);
    }

}
