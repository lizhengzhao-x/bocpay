package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.wallet.intl.entity.RegSucEntity;

/**
 * Created by ZhangXinchao on 2019/11/21.
 */
public class VirtualCardRegisterSuccessEntity extends RegSucEntity {
//    /**
//     * 通讯中心  (Y：跳 N：不跳)
//     */
//    private String jumpToMessageCenter;
//
//    /**
//     * 手动登录 确认是新设备 跳转到通知中心
//     * @return
//     */
//    public boolean isJumpToMessageCenter() {
//        if (TextUtils.isEmpty(jumpToMessageCenter)) {
//            return false;
//        } else {
//            return TextUtils.equals(jumpToMessageCenter, "Y");
//        }
//    }

    /**
     * walletId : 1996
     * account : 86-9883448
     * sId : bf1bf414-043b-4fa8-82b0-b0cb15bdf434
     */

    private String walletId;
    private String account;

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


}
