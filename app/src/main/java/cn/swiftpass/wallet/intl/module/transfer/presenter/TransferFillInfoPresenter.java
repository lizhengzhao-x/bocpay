package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferQueryBalanceProtocol;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferFillInfoContract;

/**
 * 转账
 */
public class TransferFillInfoPresenter extends TransferBasePresenter<TransferFillInfoContract.View> implements TransferFillInfoContract.Presenter {


    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartAccountInfo() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();

    }

    @Override
    public void getAccountBalanceInfo() {
        new TransferQueryBalanceProtocol(new NetWorkCallbackListener<AccountBalanceEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getAccountBalanceInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(AccountBalanceEntity response) {
                if (getView() != null) {
                    getView().getAccountBalanceInfoSuccess(response);
                }
            }
        }).execute();
    }


}
