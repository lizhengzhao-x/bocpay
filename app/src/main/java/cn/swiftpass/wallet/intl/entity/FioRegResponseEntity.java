package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/5/22.
 * FIO 注册第二步返回
 */

public class FioRegResponseEntity extends BaseEntity {
    public String getFioServerResponse() {
        return fioServerResponse;
    }

    public void setFioServerResponse(String fioServerResponse) {
        this.fioServerResponse = fioServerResponse;
    }

    //服务端掉 FIO SERVER 的Registration Response接口返回的参数。客户端调用 sdk的Registration Part 2接口需要传入此参数
    String fioServerResponse;

}
