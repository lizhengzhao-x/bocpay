package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/7/16.
 */
public class CheckCcEntity extends BaseEntity {
    /**
     * giftTp : W
     * eVoucherNos : [{"itemName":"屈臣氏0","aswItemCode":"88880","giftCode":"66660","eVoucherStock":"2"},{"itemName":"屈臣氏1","aswItemCode":"88881","giftCode":"66661","eVoucherStock":"2"},{"itemName":"屈臣氏2","aswItemCode":"88882","giftCode":"66662","eVoucherStock":"2"},{"itemName":"屈臣氏3","aswItemCode":"88883","giftCode":"66663","eVoucherStock":"2"}]
     */

    private String giftTp;
    private List<EVoucherNosBean> eVoucherNos;

    public String getGiftTp() {
        return giftTp;
    }

    public void setGiftTp(String giftTp) {
        this.giftTp = giftTp;
    }

    public List<EVoucherNosBean> getEVoucherNos() {
        return eVoucherNos;
    }

    public void setEVoucherNos(List<EVoucherNosBean> eVoucherNos) {
        this.eVoucherNos = eVoucherNos;
    }

    public static class EVoucherNosBean extends BaseEntity {
        /**
         * itemName : 屈臣氏0
         * aswItemCode : 88880
         * giftCode : 66660
         * eVoucherStock : 2
         */

        private String itemName;
        private String aswItemCode;
        private String giftCode;
        private String eVoucherStock;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getAswItemCode() {
            return aswItemCode;
        }

        public void setAswItemCode(String aswItemCode) {
            this.aswItemCode = aswItemCode;
        }

        public String getGiftCode() {
            return giftCode;
        }

        public void setGiftCode(String giftCode) {
            this.giftCode = giftCode;
        }

        public String getEVoucherStock() {
            return eVoucherStock;
        }

        public void setEVoucherStock(String eVoucherStock) {
            this.eVoucherStock = eVoucherStock;
        }
    }

//
//    /**
//     * bindCardStatus : 1
//     * redmId : AG17969167
//     */
//
//    private String bindCardStatus;
//    private String redmId;
//
//    public String getBindCardStatus() {
//        return bindCardStatus;
//    }
//
//    public void setBindCardStatus(String bindCardStatus) {
//        this.bindCardStatus = bindCardStatus;
//    }
//
//    public String getRedmId() {
//        return redmId;
//    }
//
//    public void setRedmId(String redmId) {
//        this.redmId = redmId;
//    }


}
