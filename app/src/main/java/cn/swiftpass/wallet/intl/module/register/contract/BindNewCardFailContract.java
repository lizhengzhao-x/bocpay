package cn.swiftpass.wallet.intl.module.register.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;

/**
 * 绑定失败 重试
 */
public class BindNewCardFailContract {

    public interface View extends IView {


        void bindNewCardListError(String errorCode, String errorMsg);

        void bindNewCardListSuccess(BindNewCardResultEntity response);
    }


    public interface Presenter extends IPresenter<BindNewCardFailContract.View> {


        void bindNewCardList(ArrayList<BindNewCardEntity> selectCardList, String otpBindNewCard);
    }

}
