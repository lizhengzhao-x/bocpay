package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class RegisterCustomerInfo extends BaseEntity {
    public String mRegion;
    public String mArea;
    public String mStreetName;
    public String mBuildingName;
    public String mRoom;
    public String mEmail;
    public String mTaxResidentStatus;
    public ArrayList<TaxInfoParamEntity> mTaxInfos;

    public String mCurrentLimit;
}
