package cn.swiftpass.wallet.intl.entity;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * Created by ZhangXinchao on 2019/12/23.
 */
public class InitBannerEntity extends BaseEntity {


    /**
     * pTwoM : {"imagesUrl":"图片地址","type":"0"}
     * pTwoP : {"imagesUrl":"图片地址","type":"0"}
     * homePage : {"imagesUrl":"图片地址","type":"0"}
     */
/*"otpExpireTime":"55","otpSendIntervalTime":"60","otpErrorLimit":"5","retryTradeLimitTime":"24","otpRetrySendLimitTime":"2","carouselPlayTime":"5",
        "postLoginBgImageUrl":"",
        "postLoginBgImageVer":"",
        "virtualCardUrl":"",
        "virtualCardShareUrl":"",
        "hideTime": ""*/


    /**
     * 派利是成功界面banner
     */
    private HomeCommonDataEntity commonData_zh_CN = new HomeCommonDataEntity();

    private HomeCommonDataEntity commonData_zh_HK = new HomeCommonDataEntity();

    private HomeCommonDataEntity commonData_en_US = new HomeCommonDataEntity();


    private RedpacketPopResourceOutEntity redpacketPopResource;
    private String postLoginBgImageUrl;
    private String postLoginBgImageVer;

    private VirtualUrlEntity virtualCardUrl_zh_HK = new VirtualUrlEntity();
    private VirtualUrlEntity virtualCardUrl_zh_CN = new VirtualUrlEntity();
    private VirtualUrlEntity virtualCardUrl_en_US = new VirtualUrlEntity();

    private long hideTime;

    private String otpExpireTime;
    private String otpSendIntervalTime;
    private String otpErrorLimit;

    private final int DEFAULT_PLAY_TIME = 0;

    /**
     * 图片轮播时间
     */
    private String carouselPlayTime;

    private TransferPatternEntity transferPattern;

    private ArrayList<HomeLifeMenuEntity> lifeMenuData;

    public ArrayList<HomeLifeMenuEntity> getLifeMenuData() {
        return lifeMenuData;
    }

    public RedpacketPopResourceOutEntity getRedpacketPopResource() {
        return redpacketPopResource;
    }

    public void setRedpacketPopResource(RedpacketPopResourceOutEntity redpacketPopResource) {
        this.redpacketPopResource = redpacketPopResource;
    }

    public void setLifeMenuData(ArrayList<HomeLifeMenuEntity> lifeMenuData) {
        this.lifeMenuData = lifeMenuData;
    }

    public TransferPatternEntity getTransferPattern() {
        return transferPattern;
    }

    public void setTransferPattern(TransferPatternEntity transferPatternntity) {
        this.transferPattern = transferPatternntity;
    }


    /**
     * 首页最新消息轮播时间
     *
     * @return
     */
    public int getCarouselPlayTime() {
        if (TextUtils.isEmpty(carouselPlayTime)) {
            return DEFAULT_PLAY_TIME;
        }
        int carouselPlayTimeValue = 0;
        try {
            carouselPlayTimeValue = Integer.valueOf(carouselPlayTime).intValue();
        } catch (NumberFormatException e) {

        }
        return carouselPlayTimeValue;
    }

    public void setCarouselPlayTime(String carouselPlayTime) {
        this.carouselPlayTime = carouselPlayTime;
    }

    public HomeCommonDataEntity getCommonData_zh_CN() {
        return commonData_zh_CN;
    }

    public void setCommonData_zh_CN(HomeCommonDataEntity commonData_zh_CN) {
        this.commonData_zh_CN = commonData_zh_CN;
    }

    public HomeCommonDataEntity getCommonData_zh_HK() {
        return commonData_zh_HK;
    }

    public void setCommonData_zh_HK(HomeCommonDataEntity commonData_zh_HK) {
        this.commonData_zh_HK = commonData_zh_HK;
    }

    public HomeCommonDataEntity getCommonData_en_US() {
        return commonData_en_US;
    }

    public void setCommonData_en_US(HomeCommonDataEntity commonData_en_US) {
        this.commonData_en_US = commonData_en_US;
    }


    public String getOtpExpireTime() {
        return otpExpireTime;
    }

    public void setOtpExpireTime(String otpExpireTime) {
        this.otpExpireTime = otpExpireTime;
    }

    public String getOtpSendIntervalTime() {
        return otpSendIntervalTime;
    }

    public void setOtpSendIntervalTime(String otpSendIntervalTime) {
        this.otpSendIntervalTime = otpSendIntervalTime;
    }

    public VirtualUrlEntity getVirtualCardUrl_zh_HK() {
        return virtualCardUrl_zh_HK;
    }

    public void setVirtualCardUrl_zh_HK(VirtualUrlEntity virtualCardUrl_zh_HK) {
        this.virtualCardUrl_zh_HK = virtualCardUrl_zh_HK;
    }

    public VirtualUrlEntity getVirtualCardUrl_zh_CN() {
        return virtualCardUrl_zh_CN;
    }

    public void setVirtualCardUrl_zh_CN(VirtualUrlEntity virtualCardUrl_zh_CN) {
        this.virtualCardUrl_zh_CN = virtualCardUrl_zh_CN;
    }

    public VirtualUrlEntity getVirtualCardUrl_en_US() {
        return virtualCardUrl_en_US;
    }

    public void setVirtualCardUrl_en_US(VirtualUrlEntity virtualCardUrl_en_US) {
        this.virtualCardUrl_en_US = virtualCardUrl_en_US;
    }

    public String getOtpErrorLimit() {
        return otpErrorLimit;
    }

    public void setOtpErrorLimit(String otpErrorLimit) {
        this.otpErrorLimit = otpErrorLimit;
    }

    public String getPostLoginBgImageUrl() {
        return postLoginBgImageUrl;
    }

    public void setPostLoginBgImageUrl(String postLoginBgImageUrl) {
        this.postLoginBgImageUrl = postLoginBgImageUrl;
    }

    /**
     * 为了保证图片能在版本或者地址变化时更新
     *
     * @return
     */
    public String getPostLoginBgImageVer() {
        return postLoginBgImageUrl + postLoginBgImageVer;
    }

    public void setPostLoginBgImageVer(String postLoginBgImageVer) {
        this.postLoginBgImageVer = postLoginBgImageVer;
    }

    public String getVirtualCardUrl(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getVirtualCardUrl_zh_CN().getVirtualCardUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getVirtualCardUrl_zh_HK().getVirtualCardUrl();
        } else {
            return getVirtualCardUrl_en_US().getVirtualCardUrl();
        }

    }

    public String getVirtualCardShareUrl(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getVirtualCardUrl_zh_CN().getVirtualCardShareUrl();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getVirtualCardUrl_zh_HK().getVirtualCardShareUrl();
        } else {
            return getVirtualCardUrl_en_US().getVirtualCardShareUrl();
        }
    }

    public long getHideTime() {
        return hideTime;
    }

    public void setHideTime(long hideTime) {
        this.hideTime = hideTime;
    }

    public BannerImageEntity getRedPacket(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getRedPacket();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getRedPacket();
        } else {
            return getCommonData_en_US().getRedPacket();
        }
    }


    public String getTxnDetailShareText(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getTxnDetailShareText();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getTxnDetailShareText();
        } else {
            return getCommonData_en_US().getTxnDetailShareText();
        }
    }

    public String getCrossBorderShareText(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getCrossBorderShareText();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getCrossBorderShareText();
        } else {
            return getCommonData_en_US().getCrossBorderShareText();
        }
    }


    public BannerImageEntity getMoneyTransfer(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getMoneyTransfer();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getMoneyTransfer();
        } else {
            return getCommonData_en_US().getMoneyTransfer();
        }
    }


    public BannerImageEntity getRedEnvelopes(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getRedEnvelopes();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getRedEnvelopes();
        } else {
            return getCommonData_en_US().getRedEnvelopes();
        }
    }


    public BannerImageEntity getLocalPage(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getLocalPage();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getLocalPage();
        } else {
            return getCommonData_en_US().getLocalPage();
        }
    }


    public BannerImageEntity getPTwoM(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getpTwoM();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getpTwoM();
        } else {
            return getCommonData_en_US().getpTwoM();
        }
    }

    public BannerImageEntity getTransferRecord(Context context) {

        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getTransferRecord();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getTransferRecord();
        } else {
            return getCommonData_en_US().getTransferRecord();
        }

    }

//    public BannerImageEntity getCrossBorderRecord(Context context) {
//
//        String lan = SpUtils.getInstance(context).getAppLanguage();
//        if (AndroidUtils.isZHLanguage(lan)) {
//            return getCommonData_zh_CN().getCrossBorderRecord();
//        } else if (AndroidUtils.isHKLanguage(lan)) {
//            return getCommonData_zh_HK().getCrossBorderRecord();
//        } else {
//            return getCommonData_en_US().getCrossBorderRecord();
//        }
//
//    }


    public BannerImageEntity getPTwoP(Context context) {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return getCommonData_zh_CN().getpTwoP();
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return getCommonData_zh_HK().getpTwoP();
        } else {
            return getCommonData_en_US().getpTwoP();
        }
    }


}
