package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;

/**
 * 转账确认
 */
public class TransferConfirmContract {

    public interface View extends IView {
        void transferChangeUserNameSuccess(ContentEntity response);

        void transferChangeUserNameError(String errorCode, String errorMsg);

        void confirmTransferSuccess(TransferCompleteEntity response);

        void confirmTransferError(String errorCode, String errorMsg);

        void transferConfirmBocSuccess(TransferCompleteEntity response);

        void transferConfirmBocError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<TransferConfirmContract.View> {

        void transferChangeUserName(String transferOrderId, String crCustNameRemark, String typeIn);

        void confirmTransfer(TransferConfirmReq req,String action);

        void transferConfirmBoc(String transferOrderId, boolean isLishi,String action);
    }

}
