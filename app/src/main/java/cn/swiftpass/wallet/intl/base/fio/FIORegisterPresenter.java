package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IView;


/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：IORegisterPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 18:30
 * @change
 * @chang time
 * @class Fio注册登记
 */
public interface FIORegisterPresenter<V extends IView> {
    void onFioRegister(String walletId, String param, String regType);
}
