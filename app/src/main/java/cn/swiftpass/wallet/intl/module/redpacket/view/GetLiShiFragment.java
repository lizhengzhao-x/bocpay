package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jay.widget.StickyHeadersLinearLayoutManager;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.redpacket.contract.GetLiShiContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.module.redpacket.presenter.GetLiShiPresenter;
import cn.swiftpass.wallet.intl.module.redpacket.view.adapter.GetLiShiRedPacketHistoryAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * 收利是
 */
public class GetLiShiFragment extends BaseFragment<GetLiShiContract.Presenter> implements GetLiShiContract.View {
    private static final String TAG = GetLiShiFragment.class.getSimpleName();


    @BindView(R.id.cv_lishi_history)
    RecyclerView cvLiShiHistory;
    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;


    GetLiShiRedPacketHistoryAdapter redPacketHistoryAdapter;
    List<RedPacketBeanEntity> items = new ArrayList<>();

    GetLiShiHomeEntity getLiShiHomeEntity;


    private UpdateTvTabDot mUpdateTvTabDot;
    /**
     * 是否需要初始化的时候刷新数据
     */
    private boolean isNeedLoad = false;


    public static GetLiShiFragment newInstance(UpdateTvTabDot mUpdateTvTabDot) {
        return GetLiShiFragment.newInstance(mUpdateTvTabDot, false);
    }

    public static GetLiShiFragment newInstance(UpdateTvTabDot mUpdateTvTabDot, boolean isNeedLoad) {
        GetLiShiFragment getLiShiFragment = new GetLiShiFragment();
        getLiShiFragment.mUpdateTvTabDot = mUpdateTvTabDot;
        getLiShiFragment.isNeedLoad = isNeedLoad;
        return getLiShiFragment;
    }


    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {
        redPacketHistoryAdapter = new GetLiShiRedPacketHistoryAdapter(mActivity, getLiShiHomeEntity, items);
        cvLiShiHistory.setAdapter(redPacketHistoryAdapter);

    }


    @Override
    protected GetLiShiContract.Presenter loadPresenter() {
        return new GetLiShiPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_get_lishi;
    }

    @Override
    protected void initView(View v) {
        EventBus.getDefault().register(this);
        if (null == getActivity()) {
            return;
        }
        StickyHeadersLinearLayoutManager layoutManager = new StickyHeadersLinearLayoutManager<GetLiShiRedPacketHistoryAdapter>(getActivity(), LinearLayoutManager.VERTICAL, false);
        cvLiShiHistory.setLayoutManager(layoutManager);
        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        cvLiShiHistory.addItemDecoration(itemDecoration);


        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                //清除通讯录
                ContactCompareUtils.clearAllContactList();
                mPresenter.getGetLiShiHomeData();
            }
        });


        if (isNeedLoad) {
            autoRefreshList();
        }


    }


    public void autoRefreshList() {
        if (mainRefreshLayout == null) {
            return;
        }
        mainRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mPresenter.getGetLiShiHomeData(false);
            }
        });
    }

    private void stopRefreshList() {
        if (mainRefreshLayout == null) {
            return;
        }
        mainRefreshLayout.finishRefresh();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if (null == getLiShiHomeEntity) {
            return;
        }

        redPacketHistoryAdapter.setLiShiHome(getLiShiHomeEntity);
        initPacketHistory();

        if (null != mUpdateTvTabDot) {
            mUpdateTvTabDot.upDateTvTabDot(getLiShiHomeEntity.getReceivedRecordsTurnOffNum());
        }
    }

    private void initPacketHistory() {
        items.clear();
        if (null != getLiShiHomeEntity.getReceivedRecordsTurnOn()) {
            items.addAll(getLiShiHomeEntity.getReceivedRecordsTurnOn());
        }


        ContactCompareUtils.compareHisotryLocalRecord(getActivity(), items, new ContactCompareUtils.CompareRedPacketsListResult() {
            @Override
            public void getRedPackets(List<RedPacketBeanEntity> redPacketBeanEntities) {
                redPacketHistoryAdapter.setDataList(redPacketBeanEntities);
                redPacketHistoryAdapter.notifyDataSetChanged();

            }
        });
    }


    /**
     * 页面添加空布局  直接用该方法
     *
     * @param mContext
     * @param recyclerView
     * @param str
     * @param imgId
     * @return
     */
    protected View getEmptyView(Context mContext, RecyclerView recyclerView, String str, int imgId) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(str);
        if (imgId == -1) {
            noDataImg.setVisibility(View.GONE);
        } else {
            noDataImg.setVisibility(View.VISIBLE);
            noDataImg.setImageResource(imgId);
        }
        return emptyView;
    }


    /**
     * tab 在没有网络上点击会显示弹框
     */
    public void tabClickFresh() {
        if (null == getLiShiHomeEntity && null != mPresenter) {
            mPresenter.getGetLiShiHomeData();
        }
    }

    @Override
    public void getGetLiShiHomeDataSuccess(GetLiShiHomeEntity response) {
        getLiShiHomeEntity = response;
        stopRefreshList();
        initData();
        mainRefreshLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void getGetLiShiHomeDataError(String errorCode, String errorMsg) {
        stopRefreshList();
        showErrorMsgDialog(getContext(), errorMsg);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_RED_PACKET_OPEN) {
            mPresenter.getGetLiShiHomeData(false);
        }
    }

}

