package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transactionRecords.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/21 16:07
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderStatusEntity extends BaseEntity {

    /**
     * {
     * "payName": "",
     * "status": "4",
     * "amountHKD": "60",
     * "businessType": "1",
     * "txnId": "EWAC2019112700000026",
     * "receiverCardType": "D",
     * "currency": "HKD",
     * "shareContent": "",
     * "receiverCardNo": "62135600284848",
     * "fee": "10",
     * "forUsed": "购物",
     * "currencyRMB": "CNY",
     * "txTime": "2019-11-27 19:19:47",
     * "panFour": "2365",
     * "totalAmount": "70",
     * "accountType": "2",
     * "receiverName": "SERVICE NAME",
     * "receiverBankName": "Jingshan BOC Fullerton bank",
     * "referenceNo": "25",
     * "amountCNY": "53.99"
     * }
     */

    public String payName;
    /**
     * txnOptionStr：跨境汇款
     * txnOptionStr：跨境汇款冲正
     */
    public String txnOptionStr;
    public String status;
    public String amountHKD;
    public String businessType;
    public String txnId;
    public String receiverCardType;
    public String currency;
    public String shareContent;
    public String receiverCardNo;
    public String fee;
    public String forUsed;
    public String currencyRMB;
    public String txTime;
    public String panFour;
    public String totalAmount;
    public String accountType;
    public String receiverName;
    public String receiverBankName;
    public String referenceNo;
    public String amountCNY;
    public String rejectReason;//失败原因  1-交易超时 2-处理中
}
