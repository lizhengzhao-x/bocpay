package cn.swiftpass.wallet.intl.event;

import android.app.Activity;
import android.content.DialogInterface;

import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSuccessBannerActivity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_REGISTERBANNER;

public class RegisterPopBannerDialogEvent extends BaseEventType {
    public static final String REGISTERPOPBANNERDIALOGNAME = "RegisterPopBannerDialogEvent";
    public static final String POPUPBANNERURL = "POPUPBANNERURL";

    public RegisterPopBannerDialogEvent() {
        super(EVENT_REGISTERBANNER.eventPriority, EVENT_REGISTERBANNER.eventName);
    }

    @Override
    protected String getCurrentEventType() {
        return REGISTERPOPBANNERDIALOGNAME;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof RegisterPopBannerDialogDealOwner) {
            RegisterPopBannerDialogDealOwner lbsDialogDealOwner = (RegisterPopBannerDialogDealOwner) currentActivity;
            String value = (String) eventParams.get(POPUPBANNERURL);
            RegisterSuccessBannerActivity.startActivity(lbsDialogDealOwner.getCurrentActivity(), value);
        }
        return true;
    }



    public interface RegisterPopBannerDialogDealOwner extends EventDealOwner {
    }
}
