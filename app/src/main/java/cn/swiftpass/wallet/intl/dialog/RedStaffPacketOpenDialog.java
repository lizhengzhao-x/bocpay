package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.MediaPlayerUtils;
import cn.swiftpass.wallet.intl.utils.RedPacketResourceUtil;
import me.jessyan.autosize.utils.AutoSizeUtils;

/**
 * 派利是拆红包
 */
public class RedStaffPacketOpenDialog extends BaseRedPacketOpenDialog {
    public RedStaffPacketOpenDialog(Context context) {
        super(context);
    }

    public RedStaffPacketOpenDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder extends BaseRedPacketOpenDialog.Builder {
        public Builder(Context context) {
            super(context);
        }

        public RedStaffPacketOpenDialog.Builder setGifType(String gifType) {
            this.gifType = gifType;
            return this;
        }

        public RedStaffPacketOpenDialog.Builder setCloseClickListener(View.OnClickListener closeClickListener) {
            this.closeClickListener = closeClickListener;
            return this;
        }

        public RedStaffPacketOpenDialog.Builder setNextRedPacketRefNo(String nextRedPacketRefNo) {
            this.nextRedPacketRefNo = nextRedPacketRefNo;
            return this;
        }

        public RedStaffPacketOpenDialog.Builder setTransferType(String transferType) {
            this.transferType = transferType;
            return this;
        }


        public RedStaffPacketOpenDialog.Builder setGifUri(Uri gifUri) {
            this.gifUri = gifUri;
            return this;
        }

        public RedStaffPacketOpenDialog.Builder setMp3Uri(Uri mp3Uri) {
            this.mp3Uri = mp3Uri;
            return this;
        }

        public RedStaffPacketOpenDialog.Builder setPositiveListener(ConFrimClick listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public RedStaffPacketOpenDialog create() {
            RedStaffPacketOpenDialog dialog = new RedStaffPacketOpenDialog(context, R.style.Dialog_No_KeyBoard);
            dialog.setContentView(getLayout());
            return dialog;
        }


        @Override
        protected void initDate() {
            super.initDate();
            llyCommonContain.setVisibility(View.GONE);
            llyAllContain.setVisibility(View.GONE);
            tvTransferBtnNext.setText(context.getResources().getString(R.string.RP2101_2_1));
            ivClose.setVisibility(View.VISIBLE);
            //先关闭音乐
            MediaPlayerUtils.stopPlayMusic();
            if (null != mp3Uri) {
                MediaPlayerUtils.startPlayMusic(context, mp3Uri);
            }

            String gifUrlStr = gifUri.toString();

            String defaultPath = RedPacketResourceUtil.getDefaultPath(context, gifType);


            //如果是默认图片可以直接加载 网络图片需要区分 gif和png
            if (TextUtils.equals(defaultPath, gifUri.toString())) {
                GlideApp.with(context).asGif().load(gifUri).override(childTotalWidth,childTotalHeight).into(new SimpleTarget<GifDrawable>() {
                    @Override
                    public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                        rlyRedPacketContain.setBackground(resource);
                        updateView();
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                        updateView();
                    }
                });
            } else {
                File file = new File(gifUri.toString());
                if (gifUrlStr.contains(".gif")) {
                    GlideApp.with(context).asGif().override(childTotalWidth,childTotalHeight).load(file).into(new SimpleTarget<GifDrawable>() {
                        @Override
                        public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                            rlyRedPacketContain.setBackground(resource);
                            updateView();
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                            updateView();
                        }
                    });
                } else {
                    GlideApp.with(context).load(file).override(childTotalWidth,childTotalHeight).into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            rlyRedPacketContain.setBackground(resource);
                            updateView();
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                            updateView();
                        }
                    });
                }
            }
        }

        /**
         * 当图片 加载完成才显示所有控件
         */
        private void updateView(){
            if(null != llyRedPacketMainContain){
                llyRedPacketMainContain.setVisibility(View.VISIBLE);
            }

        }

    }
}

