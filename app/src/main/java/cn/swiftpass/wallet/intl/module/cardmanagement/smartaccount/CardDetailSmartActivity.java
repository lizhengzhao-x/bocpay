package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpgradeAccountActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView;

/**
 * @author Created by  on 2018/1/11.
 * //我的账户虚拟卡详细信息
 */

public class CardDetailSmartActivity extends BaseCompatActivity {

    @BindView(R.id.id_card_no)
    TextView idCardNo;
    @BindView(R.id.id_card_no_title)
    TextView mCardNoTitleTV;
    @BindView(R.id.id_linear_view)
    LinearLayout idLinearView;
    @BindView(R.id.id_update_smartaccount)
    LinearLayout idLinearUpdateSmart;
    @BindView(R.id.id_master_account)
    RelativeLayout idMasterAccount;
    @BindView(R.id.id_transaction_limit)
    RelativeLayout id_transaction_limit;
    @BindView(R.id.id_top_up_method)
    RelativeLayout id_top_up_method;
    @BindView(R.id.tv_account_check)
    TextView mAccountCheckTV;
    @BindView(R.id.tv_primary_account)
    TextView mPrimaryAccountTV;
    @BindView(R.id.tv_limit_amount)
    TextView mDailyLimitTV;
    @BindView(R.id.tv_topup_method)
    TextView mTopupMethodTV;
    private MySmartAccountEntity mSmartAccountInfo;
    @BindView(R.id.iav_topup_account)
    ImageArrowNoPaddingView mTopUpAccount;
    private static final int CHECK_EDDA = 5;
    private static final int UPDATE = 6;
    private int currentEventType = 0;

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_card_detail_smart;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.title_title_card);
        final BankCardEntity cardEntity = (BankCardEntity) getIntent().getSerializableExtra(Constants.CARD_ENTITY);
        idCardNo.setText(AndroidUtils.formatCardMastNumberStr(cardEntity.getPanShowNumber()));
        mCardNoTitleTV.setText("");
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idLinearView.getLayoutParams();
        int margin = (int) mContext.getResources().getDimension(R.dimen.space_40_px);
        int width = AndroidUtils.getScreenWidth(mContext) - margin * 2;
        int height = width * 5 / 8;
        lp.width = width;
        lp.height = height;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        idLinearView.setLayoutParams(lp);
        RoundedCorners roundedCorners = new RoundedCorners(10);
        //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContext).load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                .apply(options)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        idLinearView.setBackground(resource);
                    }
                });
        ;
        getSmartAccountInfo();
        idLinearUpdateSmart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentEventType = UPDATE;
                verifyPwdAction();
            }
        });
    }


    private void getSmartAccountInfo() {
        NetWorkCallbackListener<MySmartAccountEntity> callback = new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(CardDetailSmartActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                mSmartAccountInfo = response;
                refreshView();
            }
        };
        ApiProtocolImplManager.getInstance().getSmartAccountInfo(this, callback);
    }

    private void refreshView() {
        if (null != mSmartAccountInfo) {
            mTopupMethodTV.setText(AndroidUtils.getTopUpMethodValue(this, mSmartAccountInfo.getAddedmethod()));
            mDailyLimitTV.setText(mSmartAccountInfo.getCurrency() + mSmartAccountInfo.getPayLimit());

            id_transaction_limit.setVisibility(View.VISIBLE);
            id_top_up_method.setVisibility(View.VISIBLE);
            if (mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
                idLinearUpdateSmart.setVisibility(View.GONE);
                String account = AndroidUtils.getPrimaryAccountDisplay(mSmartAccountInfo.getAccType(), mSmartAccountInfo.getRelevanceAccNo());
                idMasterAccount.setVisibility(View.VISIBLE);
                mAccountCheckTV.setText(account);
            } else {
                //s2用户 显示升级按钮 并判断是否绑定了edda
                idLinearUpdateSmart.setVisibility(View.VISIBLE);
                //如果银行账户绑定银行卡 显示后后四位
                List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
                if (bankCardsBeans != null && bankCardsBeans.size() > 0) {
                    //已经绑定银行卡
                    mTopUpAccount.setVisibility(View.VISIBLE);
                    mTopUpAccount.setRightText(bankCardsBeans.get(0).getBankName() + "(" + AndroidUtils.getFormatBankCardNumber(bankCardsBeans.get(0).getBankNo()) + ")");
                    mTopUpAccount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //查看edda
                            currentEventType = CHECK_EDDA;
                            verifyPwdAction();
                        }
                    });
                } else {
                    //未绑定卡
                    mTopUpAccount.setVisibility(View.VISIBLE);
                    mTopUpAccount.getTvRight().setTextColor(getColor(R.color.green_color));
                    mTopUpAccount.setRightText(getString(R.string.P3_D3_1));
                    mTopUpAccount.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //添加edda
                            getRegEddaInfo();
                        }
                    });
                }
            }
        }
    }

    private void getRegEddaInfo() {
        ApiProtocolImplManager.getInstance().getRegEddaInfo(this, new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(CardDetailSmartActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
                mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
                mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
                mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
                mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
                ActivitySkipUtil.startAnotherActivity(CardDetailSmartActivity.this, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        //绑卡成功之后页面销毁
        if (event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    public void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                onVerifySuc();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
//        if (getActivity() == null) {
//            return;
//        }
//        if (MyFingerPrintManager.getInstance().isRegisterFIO(getActivity()) && !MyFingerPrintManager.getInstance().isChangeFIO(getActivity())) {
//            FIOActivity.startAuthFingerPrint(getActivity());
//        } else {
//            initPwdDialog();
//        }
    }


//    /**
//     * 初始化密码弹出框
//     */
//    private void initPwdDialog() {
//        selectPopupWindow = AndroidUtils.showPwdDialog(getActivity(), this);
//    }
//
//
//    @Override
//    public void onPopWindowClickListener(String psw, boolean complete) {
//        if (complete && getActivity() != null) {
//            veryPwd(psw);
//        }
//    }

//    private void veryPwd(String pwd) {
//        ApiProtocolImplManager.getInstance().getCheckPassword(getActivity(), pwd, new NetWorkCallbackListener<BaseEntity>() {
//            @Override
//            public void onFailed(final String errorCode, String errorMsg) {
//                showErrorMsgDialog(CardDetailSmartActivity.this, errorMsg, new OnMsgClickCallBack() {
//                    @Override
//                    public void onBtnClickListener() {
//                        if (errorCode.equals(ErrorCode.VERIFY_PASSWORD_ERROR.code)) {
//                            selectPopupWindow.clearPwd();
//                        }
//                    }
//                });
//            }
//
//            @Override
//            public void onSuccess(BaseEntity response) {
//                onVerifySuc();
//            }
//        });
//    }

    private void onVerifySuc() {
        if (currentEventType == CHECK_EDDA) {
            getRegEddaInfoForDetail();
        } else {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
            ActivitySkipUtil.startAnotherActivity(this, UpgradeAccountActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

    }

    private void getRegEddaInfoForDetail() {
        ApiProtocolImplManager.getInstance().getRegEddaInfo(this, new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(CardDetailSmartActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
                if (bankCardsBeans != null && bankCardsBeans.get(0) != null) {
                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                    mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
                    mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.CHECKEDDA);
                    mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
                    mHashMapsLogin.put(Constants.BANK_CODE, bankCardsBeans.get(0).getBankNo());
                    mHashMapsLogin.put(Constants.BANK_NAME, bankCardsBeans.get(0).getBankName());
                    mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
                    ActivitySkipUtil.startAnotherActivity(CardDetailSmartActivity.this, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }

//    @Override
//    public void onPopWindowBackClickListener() {
//
//    }
}
