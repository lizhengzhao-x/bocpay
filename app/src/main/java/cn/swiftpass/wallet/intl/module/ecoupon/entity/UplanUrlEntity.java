package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class UplanUrlEntity extends BaseEntity {

    /**
     * couponPictureUrl : http://mycarson.wicp.vip/images/en_US/bg_cny_style_a.png
     * redirectUrl : https://whkbcuat.ftcwifi.com/creditcard/BOCBILL/help/html/zh_HK/02_10.html?idNum=100&idType=04&insCode=012&language=zh_CN&procDt=20200803175538&signCertId=uplancertid&signature=Q8lChLtz4qU6s2Ok/1FM3cGgbdqrSl9MZPwA3zdrv3WjnrqdJCTly1927kmvsw75I9AvwGS/JbD3QnufOcu4Dv2EpJwtlSV27BlVI8c8ApEvrdyrNFnd4oLfS8Iin+F4QS5n/1RcWoUSZQhr7S7mC6JsyIa2d4Td/dTbA2PfjzQ0zt27Kr5lHEyiGy36o9jK9WaYtqOBDin8H5TcmBtdyJUwXqI2XPJiM9ezwYf9se1ceo9m6drYIDBQ57bP8DLxKLgyCMIj0ZH3SF6ZbwJRU2NFDEjJEZt6S3/pSkGkhCXZvNmDjZzJ0KSvfsogJyza2TQCHLIlcURMo+6ZSxfXtw==
     */

    private String couponPictureUrl;
    private String redirectUrl;
    private String myCoupon;
    private String instructions;


    public String getMyCoupon() {
        return myCoupon;
    }

    public void setMyCoupon(String myCoupon) {
        this.myCoupon = myCoupon;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getCouponPictureUrl() {
        return couponPictureUrl;
    }

    public void setCouponPictureUrl(String couponPictureUrl) {
        this.couponPictureUrl = couponPictureUrl;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
