package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EVoucherStatusEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponsDetailEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;

/**
 * 使用电子券
 */
public class EcouponsUseContract {

    public interface View extends IView {
        void geteVoucherDetailsSuccess(EcouponsDetailEntity response);

        void geteVoucherDetailsError(String errorCode, String errorMsg, String referenceNo);

        void redeemeVouchersStatusSuccess(EVoucherStatusEntity response);

        void redeemeVouchersStatusError(String errorCode, String errorMsg);

        void getMyEcouponsListSuccess(MyEVoucherEntity response);

        void getMyEcouponsListError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<EcouponsUseContract.View> {
        void geteVoucherDetails(String referenceNo, boolean showLoading);

        void redeemeVouchersStatus(List<String> referenceNos);

        void getMyEcouponsList(String bu, String reload);
    }

}
