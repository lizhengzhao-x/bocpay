package cn.swiftpass.wallet.intl.entity;


import java.util.List;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.BaseEntity;

public class RegisterAccountEntity extends RegSucEntity {


    private String email;
    private String mobile;
    private String isAudit;

    /**
     * email : vhhjjk@qq.com
     * sId : aec294d5-91d6-4f5e-85fb-94ac2b17faad
     * cards : {"pages":1,"rows":[{"cardFaceId":"SmartAC","cardId":836,"cardStatus":"2","cardType":1,"checked":false,"createTime":1544067970000,"default":true,"isDefault":1,"pageNumber":1,"pageSize":10,"pan":"1544067974768","panFour":"4768","phone":"+86-17620398534","physicFlag":1,"smartAccLevel":2,"smartAccount":false,"total":0,"updateTime":1544067989543,"userId":1775}],"total":1}
     * mobile : +86-17620398534
     */

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIsAudit() {
        return isAudit;
    }

    public void setIsAudit(String isAudit) {
        this.isAudit = isAudit;
    }

    public static class CardsBean extends BaseEntity {
        /**
         * pages : 1
         * rows : [{"cardFaceId":"SmartAC","cardId":836,"cardStatus":"2","cardType":1,"checked":false,"createTime":1544067970000,"default":true,"isDefault":1,"pageNumber":1,"pageSize":10,"pan":"1544067974768","panFour":"4768","phone":"+86-17620398534","physicFlag":1,"smartAccLevel":2,"smartAccount":false,"total":0,"updateTime":1544067989543,"userId":1775}]
         * total : 1
         */

        private int pages;
        private int total;
        private List<BankCardEntity> rows;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public List<BankCardEntity> getRows() {
            return rows;
        }

        public void setRows(List<BankCardEntity> rows) {
            this.rows = rows;
        }


    }
}
