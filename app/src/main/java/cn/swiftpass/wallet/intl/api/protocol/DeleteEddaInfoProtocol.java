package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 充值绑定银行卡
 */
public class DeleteEddaInfoProtocol extends BaseProtocol {


    public DeleteEddaInfoProtocol(NetWorkCallbackListener dataCallback) {
        mUrl = "api/smartAccountManager/delEdda";
        mDataCallback = dataCallback;
    }


}
