package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.h6ah4i.android.widget.advrecyclerview.expandable.ExpandableItemState;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.NewCardManagerFragment;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.OffsetSmoothScroller;

public class CardStackAdapter extends AbstractExpandableItemAdapter<CardGroupViewHolder, CardContentViewHolder> {
    private final BaseCompatActivity context;
    private final CardStackClickListener listener;

    List<CardGroupItem> mItems;
    private RecyclerView recyclerView;
    public static int cardItemHeight = 170;
    public static int cardItemOffset = 87;
    private RecyclerViewExpandableItemManager expandableItemManager;
    private int lastPosition = -1;
    private final int cardImageWidth;
    private final int cardImageHeight;


    public CardStackAdapter(BaseCompatActivity context, CardStackClickListener listener) {
        setHasStableIds(true); // this is required for expandable feature.
        this.context = context;
        mItems = new ArrayList<>();
        this.listener = listener;
        cardImageWidth = AndroidUtils.getScreenWidth(context) / 2 - AndroidUtils.dip2px(context, 20);
        cardImageHeight = (int) (cardImageWidth * AndroidUtils.cardPresent);
        cardItemHeight = (int) (cardImageWidth * AndroidUtils.cardPresent + AndroidUtils.dip2px(context, 55));
        cardItemOffset = cardItemHeight / 3 + AndroidUtils.dip2px(context, 2);
    }

    public void hideAccount() {
        if (mItems != null) {
            for (CardGroupItem item : mItems) {
                item.bankCardEntity.setSel(false);
                CardContentItem contentItem = item.children.get(0);
                if (contentItem != null && contentItem.bankCardEntity != null) {
                    contentItem.bankCardEntity.setSel(false);
                }
            }
        }
    }

    public void updateData(ArrayList<BankCardEntity> list) {
        if (list != null) {
            mItems.clear();
            for (int i = 0; i < list.size(); i++) {
                BankCardEntity bankCardEntity = list.get(i);
                if (bankCardEntity != null) {
                    CardGroupItem group = new CardGroupItem(i, bankCardEntity);

                    group.children.add(new CardContentItem(0, bankCardEntity));
                    mItems.add(group);
                }
            }

        }

    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    @Override
    public int getGroupCount() {
        return mItems.size();
    }

    @Override
    public int getChildCount(int groupPosition) {
        if (groupPosition < 0) {
            groupPosition = 0;
        }
        return mItems.get(groupPosition).children.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        // This method need to return unique value within all group items.
        if (groupPosition < 0) {
            groupPosition = 0;
        }
        return mItems.get(groupPosition).id;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        // This method need to return unique value within the group.
        if (groupPosition < 0) {
            groupPosition = 0;
        }
        return mItems.get(groupPosition).children.get(childPosition).id;
    }

    @Override
    @NonNull
    public CardGroupViewHolder onCreateGroupViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_view, parent, false);
        ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
        layoutParams.height = cardItemHeight;
        CardGroupViewHolder cardGroupViewHolder = new CardGroupViewHolder(v);
        ViewGroup.LayoutParams ivLayoutParams = cardGroupViewHolder.cardImageIv.getLayoutParams();
        ivLayoutParams.width = cardImageWidth;
        ivLayoutParams.height = (int) (cardImageWidth * AndroidUtils.cardPresent);
        return cardGroupViewHolder;
    }

    @Override
    @NonNull
    public CardContentViewHolder onCreateChildViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_open_card_view, parent, false);
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) itemView.getLayoutParams();
        //最后一个条目的高度 比其他条目高度 多一个偏移量高度
        //fragment 整体的高度-顶部账户总积分的高度-卡片的显示高度-卡片的显示高度+偏移的高度
        //卡片的显示高度=（卡片高度-偏移的高度）
        int itemContentHeight = TempSaveHelper.getMainFrameLayoutHeight() - recyclerView.getTop() - 2 * AndroidUtils.dip2px(context, cardItemHeight - cardItemOffset) + AndroidUtils.dip2px(context, cardItemOffset);
        if (layoutParams != null) {
            layoutParams.height = itemContentHeight;
            layoutParams.topMargin = cardItemOffset;
        }
        CardContentViewHolder cardContentViewHolder = new CardContentViewHolder(itemView);
        return cardContentViewHolder;
    }

    @Override
    public void onBindGroupViewHolder(@NonNull final CardGroupViewHolder holder, final int groupPosition, int viewType) {
        CardGroupItem group = mItems.get(groupPosition);
        BankCardEntity cardEntity = group.bankCardEntity;
        final ExpandableItemState expandState = holder.getExpandState();
        if (groupPosition == 0) {
            holder.cardShadowIv.setVisibility(View.INVISIBLE);
        } else {
            //展开时不显示阴影
            if (expandState.isExpanded()) {
                holder.cardShadowIv.setVisibility(View.INVISIBLE);
            } else {
                holder.cardShadowIv.setVisibility(View.VISIBLE);
            }
        }
        onBindCardItem(holder, cardEntity);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();

                if (layoutManager instanceof CardLinearLayoutManager) {
                    CardLinearLayoutManager llm = (CardLinearLayoutManager) layoutManager;
                    holder.cardShadowIv.setVisibility(View.VISIBLE);
                    if (expandState.isExpanded()) {
                        holder.cardNumberTv.setText(AndroidUtils.formatCardMastNumberPanFourStr(cardEntity.getPanFour()));
                        cardEntity.setSel(false);
                        llm.setScrollEnabled(true);
                        OffsetSmoothScroller smoothScroller = new OffsetSmoothScroller(recyclerView.getContext());
                        smoothScroller.setTargetPosition(holder.getFirstVisibleItemPosition());
                        smoothScroller.setOffset(holder.getScrollViewTop());
                        llm.startSmoothScroll(smoothScroller);
                    } else {
                        if (groupPosition != 0) {
                            holder.cardShadowIv.setVisibility(View.INVISIBLE);
                        }
                        llm.setScrollEnabled(true);
                        int firstVisibleItemPosition = llm.findFirstCompletelyVisibleItemPosition();
                        View firstViewByPosition = llm.findViewByPosition(firstVisibleItemPosition);

                        if (holder.itemView != null) {
                            int scrollViewTop = holder.itemView.getTop();
                            recyclerView.smoothScrollBy(0, scrollViewTop, new AccelerateDecelerateInterpolator(), 300);
                            holder.setFirstPosition(firstVisibleItemPosition);
                            if (firstViewByPosition != null) {
                                holder.setScrollViewTop(firstViewByPosition.getTop());
                            }
                        }

                        llm.setScrollEnabled(false);

                    }
                }

                toggleGroup(holder.itemView, groupPosition, expandState);
            }
        });

    }


    @Override
    public void onBindChildViewHolder(@NonNull CardContentViewHolder holder, int groupPosition, int childPosition, int viewType) {

        CardContentItem contentItem = mItems.get(groupPosition).children.get(childPosition);
        RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
        int itemContentHeight = 0;
        //最后一个条目的高度 比其他条目高度 多一个偏移量高度
        //卡片的显示高度=fragment 整体的高度-顶部账户总积分的高度-卡片的显示高度
        if (groupPosition == mItems.size() - 1) {
            itemContentHeight = TempSaveHelper.getMainFrameLayoutHeight() - recyclerView.getTop() - cardItemHeight + cardItemOffset;
        } else {
            itemContentHeight = TempSaveHelper.getMainFrameLayoutHeight() - recyclerView.getTop() - cardItemHeight;
        }
        if (layoutParams != null) {
            layoutParams.height = itemContentHeight;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (expandableItemManager != null) {
                    expandableItemManager.collapseAll();
                    hideAccount();
                    if (recyclerView != null) {
                        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager instanceof CardLinearLayoutManager) {
                            CardLinearLayoutManager llm = (CardLinearLayoutManager) layoutManager;
                            llm.setScrollEnabled(true);
                        }
                    }
                    expandableItemManager.notifyGroupItemChanged(groupPosition);
                }
            }
        });
        onBindCardContent(holder, contentItem.bankCardEntity, groupPosition);

    }


    @Override
    public boolean onCheckCanExpandOrCollapseGroup(@NonNull CardGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
        return false;
    }

    /**
     * 卡面数据绑定
     *
     * @param holder
     * @param cardEntity
     */
    private void onBindCardItem(CardGroupViewHolder holder, BankCardEntity cardEntity) {
        holder.cardApplyCardBtnLayout.setVisibility(View.GONE);
        holder.cardDataLayout.setVisibility(View.GONE);
        holder.cardBindSmartAccountBtnLayout.setVisibility(View.GONE);

        if (cardEntity.getCardFlag() == 2) {
            holder.cardApplyCardBtnLayout.setVisibility(View.GONE);
            holder.cardDataLayout.setVisibility(View.GONE);
            holder.cardBindSmartAccountBtnLayout.setVisibility(View.VISIBLE);
            holder.cardBindSmartAccountTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_BIND_CARD, ""));
                }
            });

        } else if (cardEntity.getCardFlag() == 1) {
            holder.cardApplyCardBtnLayout.setVisibility(View.VISIBLE);
            holder.cardDataLayout.setVisibility(View.GONE);
            holder.cardBindSmartAccountBtnLayout.setVisibility(View.GONE);
            holder.cardApplyVirtualCardTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_VIRTUAL_CARD_APPLY, ""));

                }
            });

        } else {
            holder.cardApplyCardBtnLayout.setVisibility(View.GONE);
            holder.cardBindSmartAccountBtnLayout.setVisibility(View.GONE);
            holder.cardDataLayout.setVisibility(View.VISIBLE);
            if (TextUtils.equals(cardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
                if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
                    //智能账户
                    holder.cardNameTv.setText(context.getString(R.string.AC2101_2_4));
                } else if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
                    //支付账户
                    holder.cardNameTv.setText(context.getString(R.string.AC2101_2_12));
                }
                if (cardEntity.isSel()) {
                    holder.cardNumberTv.setText(cardEntity.getPan());
                } else {
                    holder.cardNumberTv.setText(AndroidUtils.formatCardMastNumberPanFourStr(cardEntity.getPanFour()));
                }
                holder.registerFpsLayout.setVisibility(View.VISIBLE);
                holder.registerFpsLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onClickRegisterFps(cardEntity);
                    }
                });
            } else {
                if (cardEntity.isVirtualCard()) {
                    holder.cardNameTv.setText(context.getString(R.string.AC2101_2_19));
                } else {
                    holder.cardNameTv.setText(context.getString(R.string.AC2101_2_21));
                }

                holder.registerFpsLayout.setVisibility(View.GONE);
                if (cardEntity.isSel()) {
                    holder.cardNumberTv.setText(cardEntity.getPan());
                } else {
                    holder.cardNumberTv.setText(AndroidUtils.formatCardMastNumberPanFourStr(cardEntity.getPanFour()));
                }
            }
        }


        GlideApp.with(context)
                .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .override(cardImageWidth, cardImageHeight)
                .placeholder(R.mipmap.img_card_default)
                .error(R.mipmap.img_card_default)
                .into(holder.cardImageIv);
    }

    /**
     * 卡片展开内容
     *
     * @param holder
     * @param bankCardEntity
     * @param groupPosition
     */
    private void onBindCardContent(CardContentViewHolder holder, BankCardEntity bankCardEntity, int groupPosition) {
        //如果是空卡的时候，不显示账户余额和账户条目 1为空虚拟卡数据
        holder.accountBalanceLayout.setVisibility(View.GONE);
        holder.accountItemLayout.setVisibility(View.GONE);
        holder.virtualCardManagerLayout.setVisibility(View.GONE);
        holder.accountItemLayout.setVisibility(View.GONE);
        holder.updateAccountLayout.setVisibility(View.GONE);
        holder.amountManagerTv.setText(context.getString(R.string.AC2101_2_9));
        if (bankCardEntity.getCardFlag() != 0) {
            holder.accountBalanceLayout.setVisibility(View.GONE);
            holder.accountItemLayout.setVisibility(View.GONE);
            holder.virtualCardManagerLayout.setVisibility(View.GONE);
        } else {
            holder.accountItemLayout.setVisibility(View.VISIBLE);
            //只有智能账户和支付账户显示账户余额
            if (TextUtils.equals(bankCardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
                holder.accountBalanceLayout.setVisibility(View.VISIBLE);
                holder.accountManagerLayout.setVisibility(View.VISIBLE);
                holder.virtualCardManagerLayout.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(bankCardEntity.getSmartAccLevel())
                        && bankCardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
                    //智能账户
                    holder.updateAccountLayout.setVisibility(View.GONE);
                } else if (!TextUtils.isEmpty(bankCardEntity.getSmartAccLevel())
                        && bankCardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
                    //支付账户
                    holder.updateAccountLayout.setVisibility(View.VISIBLE);
                }
                //账户余额区域数据绑定
                if (bankCardEntity != null) {
                    setBalanceData(bankCardEntity, bankCardEntity.getBalance(), holder.amountTv);
                    setBalanceData(bankCardEntity, bankCardEntity.getDailyLimitBalance(), holder.amountUsableTv);
                    holder.toggleSeeIv.setImageResource(bankCardEntity.isSel() ? R.mipmap.icon_eye_open_purple : R.mipmap.icon_eye_close_purple);
                    holder.limitBalanceIv.setVisibility(bankCardEntity.isSel() ? View.VISIBLE : View.INVISIBLE);
                }

                View.OnClickListener toggleSeeListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (context != null) {
                            AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_ACCOUNT_BALANCEEYE, context.startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - context.startTime);
                            context.sendAnalysisEvent(analysisButtonEntity);
                        }

                        if (listener != null) {
                            if (bankCardEntity.isSel()) {
                                bankCardEntity.setSel(false);

                                setBalanceData(bankCardEntity, bankCardEntity.getBalance(), holder.amountTv);
                                setBalanceData(bankCardEntity, bankCardEntity.getDailyLimitBalance(), holder.amountUsableTv);
                                holder.toggleSeeIv.setImageResource(bankCardEntity.isSel() ? R.mipmap.icon_eye_open_purple : R.mipmap.icon_eye_close_purple);
                                holder.limitBalanceIv.setVisibility(bankCardEntity.isSel() ? View.VISIBLE : View.INVISIBLE);
                            } else {
                                if (SystemInitManager.getInstance().isValidShowAccount()) {
                                    bankCardEntity.setSel(true);
                                    setBalanceData(bankCardEntity, bankCardEntity.getBalance(), holder.amountTv);
                                    setBalanceData(bankCardEntity, bankCardEntity.getDailyLimitBalance(), holder.amountUsableTv);
                                    holder.toggleSeeIv.setImageResource(bankCardEntity.isSel() ? R.mipmap.icon_eye_open_purple : R.mipmap.icon_eye_close_purple);
                                    holder.limitBalanceIv.setVisibility(bankCardEntity.isSel() ? View.VISIBLE : View.INVISIBLE);
                                } else {
                                    listener.verifyPwdAction(NewCardManagerFragment.ACTION_TOGGLE_ACCOUNT, holder, bankCardEntity);
                                }
                            }
                            expandableItemManager.notifyGroupItemChanged(groupPosition);
                        }
                    }
                };
                holder.toggleSeeIv.setOnClickListener(toggleSeeListener);
                holder.amountUsableTv.setOnClickListener(toggleSeeListener);
                holder.amountTv.setOnClickListener(toggleSeeListener);
                holder.amountTipTv.setOnClickListener(toggleSeeListener);
                holder.amountUsableTipTv.setOnClickListener(toggleSeeListener);
            } else {
                //信用卡或者虚拟卡
                if (bankCardEntity.isVirtualCard()) {
                    //虚拟卡
                    holder.accountBalanceLayout.setVisibility(View.GONE);
                    holder.accountManagerLayout.setVisibility(View.GONE);
                    holder.virtualCardManagerLayout.setVisibility(View.VISIBLE);
                } else {
                    //信用卡
                    holder.virtualCardManagerLayout.setVisibility(View.GONE);
                    holder.accountManagerLayout.setVisibility(View.VISIBLE);
                    holder.amountManagerTv.setText(context.getString(R.string.AC2101_2_25));
                    holder.accountBalanceLayout.setVisibility(View.GONE);
                }

            }

        }

        holder.virtualCardManagerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickVirtualCardManager(bankCardEntity);
                }
            }
        });

        holder.limitBalanceIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickChangeLimitBalance(bankCardEntity);
                }
            }
        });

        holder.updateAccountLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickUpdateAccount();
                }
            }
        });
        holder.accountManagerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClickAccountManager(bankCardEntity);
                }
            }
        });


        holder.topAccountLayout.setOnClickListener(v -> {
            if (listener != null) {
                listener.onClickTopUp(bankCardEntity);
            }
        });

        holder.withdrawalLayout.setOnClickListener(v -> {
            if (listener != null) {
                listener.onClickWithdraw(bankCardEntity);
            }
        });
    }

    public void setBalanceData(BankCardEntity bankCardEntity, String balance, TextView textView) {
        try {
            if (!TextUtils.isEmpty(balance) && Double.parseDouble(balance) >= 0) {
                String limitBalance = AndroidUtils.formatPriceWithPoint(Double.parseDouble(balance), true);
                textView.setText(bankCardEntity.isSel() ? limitBalance : "*****");
            } else {
                textView.setText("*****");
            }
        } catch (NumberFormatException e) {
            textView.setText("*****");
        }

    }


    public void toggleGroup(View v, int groupPosition, ExpandableItemState expandState) {
        // toggle expanded/collapsed

        if (expandableItemManager.isGroupExpanded(groupPosition)) {
            expandableItemManager.collapseAll();
            hideAccount();
        } else {
            expandableItemManager.expandGroup(groupPosition);
        }
    }

    public void setExpandItemManager(RecyclerViewExpandableItemManager expandableItemManager) {
        this.expandableItemManager = expandableItemManager;
    }
}
