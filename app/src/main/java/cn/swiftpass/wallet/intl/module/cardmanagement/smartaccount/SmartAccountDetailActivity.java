package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;

/**
 * .
 * 我的账户交易详情页
 */

public class SmartAccountDetailActivity extends BaseCompatActivity {
    @BindView(R.id.id_merchant_head)
    ImageView idMerchantHead;
    @BindView(R.id.id_merchant_name)
    TextView idMerchantName;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.id_merchant_money)
    TextView idMerchantMoney;
    @BindView(R.id.id_payment_status)
    TextView idPaymentStatus;
    @BindView(R.id.id_payto)
    TextView idPayto;
    @BindView(R.id.id_ordernumber)
    TextView idOrdernumber;
    @BindView(R.id.id_ordernumberofseller)
    TextView idOrdernumberofseller;
    @BindView(R.id.id_createtime)
    TextView idCreatetime;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_order_fail)
    RelativeLayout idOrderFail;
    @BindView(R.id.id_remark)
    TextView idRemark;
    @BindView(R.id.id_rel_postscript)
    RelativeLayout idRelPostscript;
    @BindView(R.id.id_left_remark)
    TextView idLeftRemark;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.CP3_3a_1);
        SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult = (SmartAccountBillListEntity.OutgroupBean) getIntent().getExtras().getSerializable(Constants.CARD_ENTITY);
        String trxAmt = paymentEnquiryResult.getOuttxamt();
        idMerchantMoney.setText(BigDecimalFormatUtils.forMatWithDigs(trxAmt, 2));
        idOrdernumber.setText(paymentEnquiryResult.getOuttxrefno());
        idCreatetime.setText(paymentEnquiryResult.getOuttxdate());
        //二期兼容"2"

        if (paymentEnquiryResult.getOuttxsts().equals("SUCCESS") || paymentEnquiryResult.getOuttxsts().equals("2") || paymentEnquiryResult.getOuttxsts().equals("S")) {
            idPaymentStatus.setText(getString(R.string.P3_C1_5_1));
            idOrderFail.setVisibility(View.INVISIBLE);
        } else {
            idPaymentStatus.setText(getString(R.string.string_failed));
            idOrderFail.setVisibility(View.VISIBLE);
            idOrdernumberofseller.setText(paymentEnquiryResult.getOutrejtype());
        }
        //控制附言
        if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            idRelPostscript.setVisibility(View.GONE);
        } else {
            idRemark.setText(paymentEnquiryResult.getPostscript());
        }
        String trxCurrency = paymentEnquiryResult.getOuttxccy();
        idPaySymbol.setText(paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency));
        String merchantName = "";
        merchantName = paymentEnquiryResult.getOuttxtype();
        idMerchantName.setText(merchantName);
        idPayto.setText(paymentEnquiryResult.getOuttxchannel());
        if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity())) {
            if (paymentEnquiryResult.getGiftActivity().equals("1")) {
                //派利是图标 区分
                idLeftRemark.setText(getResources().getString(R.string.RP1_6_2_1));
                idMerchantHead.setImageResource(paymentEnquiryResult.getShowType().equals("+") ? R.mipmap.icon_transactions_redpacket_collect : R.mipmap.icon_transactions_redpacket_payment);
            }
        }
        //积分抵扣 低分抵扣的详情页 要动态显示 积分抵扣信息 和积分抵扣失败的message
        if (!TextUtils.isEmpty(paymentEnquiryResult.getGpDetail())) {
            if (paymentEnquiryResult.getGpDetail().equals("1")) {
                idMerchantHead.setImageResource(R.mipmap.icon_transactions_points);
            }
        }

        /**
         * 派利是图标
         */
        if (!TextUtils.isEmpty(paymentEnquiryResult.getRecordType())) {
            if (paymentEnquiryResult.getRecordType().equals("4")) {
                //派利是图标 区分
                idMerchantHead.setImageResource(R.mipmap.mouse);
            }
        }

    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smartaccount_order_detail;
    }


}
