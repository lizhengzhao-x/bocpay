package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/11/5.
 * 转账检测是否支持bocpay
 */
public class TransferFpsCheckProtocol extends BaseProtocol {
    String transferType;//转账类型  0：电话转账，1：邮箱转账
    String tansferToAccount; //账户，邮箱或者电话
    /**
     * 区分是否是派利是 转账
     */
    String transferCategory;

    public TransferFpsCheckProtocol(String transferTypeIn, String tansferToAccountIn,String transferCategoryIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.transferType = transferTypeIn;
        this.tansferToAccount = tansferToAccountIn;
        this.transferCategory = transferCategoryIn;
        mUrl = "api/transfer/fpsCheck";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TRANSFER_TYPE, transferType);
        mBodyParams.put(RequestParams.TRANSFER_ACCOUNT, tansferToAccount);
        mBodyParams.put(RequestParams.TRANSFERCATEGORY, transferCategory);
    }

}
