package cn.swiftpass.wallet.intl.base.fio;

/**
 * @author Created by ramon on 2018/8/17.
 * FIO常量
 */

public interface FioConst {

    int REQUEST_CODE_CHECK_POLICY = 0x20;
    int REQUEST_CODE_REG_FIO = 0x21;
    int REQUEST_CODE_AUTH_FIO = 0x22;
    int REQUEST_CODE_DE_REG_FIO = 0x23;
    int REQUEST_CODE_CHECK_VALID_FIO = 0x24;
    //保证是从FIOActivity返回
    String FIO_ACTION_RESULT = "fio action result";
    String FIO_ACTION = "fio action";
    String ACTION_CHECK_POLICY = "CheckPolicy";
    String ACTION_REG_FIO = "reg pio";
    String ACTION_AUTH_FIO = "auth pio";
    String FIO_ACTION_RESULT_MSG = "fio action msg";
    //注销
    String ACTION_DE_REG_FIO = "de reg pio";
    //判断FIO是否有效（设备是否支持，设备是否输入，设备是否注册，设备指纹是否发生变化，FIO ID是否有效）
    String ACTION_CHECK_VALID_FIO = "check fio valid";


    String FIO_WALLETID = "walletId";
    String FIO_REG_TYPE = "reg_type";


    String HAS_FINGERPRINT_ENROLLED = "hasFingerprintEnrolled";
    String HAS_FINGERPRINT = "hasFingerprint";

    String FIO_ACTION_IGNORE_LOGIN = "FIO_ACTION_IGNORE_LOGIN";

    String FIO_ACTION_RESULT_OTP = "fio action result otp";

    String FIO_REG_TYPE_S = "S";
    String FIO_REG_TYPE_R = "R";


    String INVALID_FINGERPRINT_UNSPORT = "UN SPORT";
    String INVALID_FINGERPRINT_UNENROLL = "UN ENROLL";
    String INVALID_FINGERPRINT_UNREG = "UN REG";
    String INVALID_FINGERPRINT_CHANGE = "CHANGE";
    String INVALID_FINGERPRINT_UNAUTH = "SERVER INVALID";
}
