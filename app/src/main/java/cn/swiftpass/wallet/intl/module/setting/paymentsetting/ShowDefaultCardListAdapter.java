package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PaymentListEntity;


public class ShowDefaultCardListAdapter implements AdapterItem<PaymentListEntity.RowsBean.TypesBean> {
    private Context context;

    private CardItemSelCallback cardItemSelCallback;

    public ShowDefaultCardListAdapter(Context context, CardItemSelCallback cardItemSelCallback) {

        this.context = context;
        this.cardItemSelCallback = cardItemSelCallback;
    }


    private TextView id_tv_left;
    private ImageView id_image_right;
    private ImageView id_left_image;

    private View id_parent_view;

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_show_defalut_card;
    }

    @Override
    public void bindViews(View root) {
        id_tv_left = root.findViewById(R.id.id_tv_left);
        id_image_right = root.findViewById(R.id.id_image_right);
        id_left_image = root.findViewById(R.id.id_left_image);
        id_parent_view = root.findViewById(R.id.id_parent_view);
        id_image_right.setBackground(null);
    }

    @Override
    public void setViews() {
        id_parent_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemSelCallback.onItemCardSelClick(mPosition);
            }
        });
    }

    @Override
    public void handleData(PaymentListEntity.RowsBean.TypesBean cardEntity, final int position) {

        if (cardEntity.getIsDefault().equals("1")) {
            id_image_right.setVisibility(View.VISIBLE);
            id_image_right.setImageResource(R.mipmap.icon_check_choose_tick);
        } else {
            id_image_right.setVisibility(View.INVISIBLE);
        }
        if (!TextUtils.isEmpty(cardEntity.getPayType())) {
            if (cardEntity.getPayType().equals("1")) {
                id_left_image.setImageResource(R.mipmap.img_card_unionpay);
            } else {
                id_left_image.setImageResource(R.mipmap.img_icon_fps);
            }
        }
        id_tv_left.setText(cardEntity.getShowTitle());
        mPosition = position;
    }

    public static class CardItemSelCallback {


        public void onItemCardSelClick(int position) {
            // do nothing
        }

    }
}
