package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;

/**
 * 虚拟卡绑定发送otp
 */
public class VirtualCardSendOtpProtocol extends BaseProtocol {
    VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiy;

    String action;

    public VirtualCardSendOtpProtocol(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String actionIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        vitualCardSendOtpEntiy = vitualCardSendOtpEntiyIn;
        this.action = actionIn;
        if (action.equals(ApiConstant.VIRTUALCARD_BIND)) {
            mUrl = "api/virtual/bind/sendOtp";
        } else {
            mUrl = "api/virtual/query/sendOtp";
        }

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
        mBodyParams.put(RequestParams.CARDID, vitualCardSendOtpEntiy.getCardId());
        if (action.equals(ApiConstant.VIRTUALCARD_BIND)) {
            mBodyParams.put(RequestParams.FACEID, vitualCardSendOtpEntiy.getCrdArtId());
            //虚拟卡绑定
            mBodyParams.put(RequestParams.VIRTUALCARDIND, "Y");
            mBodyParams.put(RequestParams.PAN_ID, vitualCardSendOtpEntiy.getPan());
            mBodyParams.put(RequestParams.EXPDATE, vitualCardSendOtpEntiy.getExpDate());

            mBodyParams.put(RequestParams.EAIACTION, "B");
            mBodyParams.put(RequestParams.OTPACT, "B");
        } else {
            //虚拟卡绑定 查询
        }

    }

}
