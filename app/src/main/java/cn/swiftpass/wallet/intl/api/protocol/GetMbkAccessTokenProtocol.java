package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class GetMbkAccessTokenProtocol extends BaseProtocol {

    private final String channel;
    private final String requesterAccessToken;

    public GetMbkAccessTokenProtocol(String channel, String requesterAccessToken, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/onlineChat/getAccessToken";
        this.channel = channel;
        this.requesterAccessToken = requesterAccessToken;

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CHANNEL, channel);
        mBodyParams.put(RequestParams.REQUESTERACCESSTOKEN, requesterAccessToken);
    }
}
