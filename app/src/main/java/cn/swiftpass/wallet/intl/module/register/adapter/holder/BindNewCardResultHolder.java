package cn.swiftpass.wallet.intl.module.register.adapter.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class BindNewCardResultHolder extends RecyclerView.ViewHolder {
    private final Context context;
    @BindView(R.id.tv_card_type)
    TextView tvCardType;
    @BindView(R.id.tv_card_number)
    TextView tvCardNumber;
    @BindView(R.id.iv_bind_new_card_result)
    ImageView ivBindResult;

    public BindNewCardResultHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }


    public void setData(BindNewCardEntity bindNewCardEntity) {
        if (bindNewCardEntity != null) {
            tvCardNumber.setText("************" + bindNewCardEntity.getPanShowNum());
            if (bindNewCardEntity.isBindStatus()) {
                ivBindResult.setImageResource(R.mipmap.icon_transfer_success_small);
            } else {
                ivBindResult.setImageResource(R.mipmap.icon_transfer_fail_small);
            }
            tvCardType.setText(bindNewCardEntity.getPanName());
        }

    }
}
