package cn.swiftpass.wallet.intl.module.setting.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.entity.InviteShareEntity;

/**
 * @name WalletBasicAppUIMergeVersion
 * @class name：cn.swiftpass.wallet.intl.module.setting.contract
 * @class describe
 * @anthor zhangfan
 * @time 2019/9/6 19:07
 * @change
 * @chang time
 * @class describe
 */
public class InviteShareContract {
    public interface View extends IView {

        void getInviteShareDetailSuccess(InviteShareEntity response,String type);

        void getInviteShareDetailError(String errorCode, String errorMsg);

        void getInviteShareListSuccess(InviteRecordListEntity response);

        void getInviteShareListError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<InviteShareContract.View> {

        void getInviteShareList();

        void getInviteShareDetail(String type);
    }
}
