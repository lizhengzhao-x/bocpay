package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class QrcodeWithPointsProtocol extends BaseProtocol {

    private String tnxId;

    public QrcodeWithPointsProtocol(String tnxIdIn,  NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/points/qrcodeWithPoints";
        this.tnxId = tnxIdIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, tnxId);
    }
}
