package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;


public class InviteServiceActivity extends BaseCompatActivity {


    private TextView mId_btn_copyurl;
    private TextView mCancelBtn;


    private void bindViews() {
        mId_btn_copyurl = (TextView) findViewById(R.id.id_btn_copyurl);
        mCancelBtn = (TextView) findViewById(R.id.tv_cancel);
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        bindViews();
        setToolBarTitle(R.string.TF2101_5_1);
        mId_btn_copyurl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareEvent();
            }
        });

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_invite_transfer;
    }


    private void shareEvent() {
        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
        if (BuildConfig.FLAVOR.equals("uat")) {
            textIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.TFI_7));
        } else {
            textIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.TFI_7_1));
        }
        startActivity(Intent.createChooser(textIntent, "Share"));
    }
}
