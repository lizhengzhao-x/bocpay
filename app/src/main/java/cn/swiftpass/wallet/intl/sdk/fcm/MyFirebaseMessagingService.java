package cn.swiftpass.wallet.intl.sdk.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RedPackageEntity;
import cn.swiftpass.wallet.intl.entity.SevenVersionNotificationEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.guide.SplashActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.utils.LogUtils;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_CREDIT_AWARD_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_MY_DISCOUNT_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_NEW_MESSAGE;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_TO_TAL_GP_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_U_PLAN_HOME_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.MESSAGE_TYPE;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.SEVEN_VERSION_NOTIFICATION_PARAMS;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "Firebase";
    private String CHANNEL_ID = ProjectApp.getContext().getPackageName();
    private String CHANNEL_NAME = ProjectApp.getContext().getPackageName() + "_Notification";
    private static int NOTIFICATION_ID = 100;
    //随机数
    private static int REQUEST_CODE = (int) (1 + Math.random() * (1024557));
    public static final int MSG_VIRTUALCARD_BIND = -1;
    public static final int MSG_REWARD = -2;
//    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        LogUtils.i(TAG, "onMessageReceived From: " + remoteMessage.getFrom());
        // Check if message contains a data payload.
//        // Check if message contains a notification payload.
//        if (remoteMessage.getNotification() != null) {
//            LogUtils.i(TAG, "onMessageReceived Notification Body: " + remoteMessage.getNotification().getBody() + " title:" + remoteMessage.getNotification().getTitle());
//        }
//
//        if (remoteMessage.getData() != null) {
//            LogUtils.i(TAG, "onMessageReceived Notification PushRrcRefNo: " + remoteMessage.getData().get(Constants.SRCREFNO_PARAM)
//                    + " RedPacketType:" + remoteMessage.getData().get(Constants.REDPACKETTYPE_PARAM));
//        }

        //经过测试，如果app进程没有被杀死，推送通知会onMessageReceived 调用，如果进程杀死，推送会托管到App启动的ACTIVITY
        if (remoteMessage.getData() != null && remoteMessage.getData().size() > 0) {
            //MESSAGE_TYPE_VCNTF
//            您收到了一封大利是!立即查看！^恭喜發財！ 您收到了一封大利是!立即查看！^恭喜发财！
            try {
                String messageType = remoteMessage.getData().get(MESSAGE_TYPE);
                //红包单独处理
                if (TextUtils.equals(messageType, Constants.REDMG_PARAM)) {
                    RedPackageEntity redPackageEntity = new RedPackageEntity();
                    redPackageEntity.setTitle(remoteMessage.getNotification().getTitle());
                    redPackageEntity.setSubTitle(remoteMessage.getNotification().getBody());
                    redPackageEntity.setPushRrcRefNo(remoteMessage.getData().get(Constants.SRCREFNO_PARAM));
                    redPackageEntity.setPushRedPacketType(remoteMessage.getData().get(Constants.REDPACKETTYPE_PARAM));
                    sendRedPacketNotification(redPackageEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, messageType);
                } else if (TextUtils.equals(messageType, Constants.REDMG_STAFF_PARAM)) {
                    //员工红包单独处理
                    RedPackageEntity redPackageEntity = new RedPackageEntity();
                    String messageContent = remoteMessage.getData().get("message");
                    //后边是标题 前边是内容
                    String items[] = messageContent.split("\\^");
//                    redPackageEntity.setTitle(items[1]);
//                    redPackageEntity.setSubTitle(items[0]);
                    if (null == items || items.length < 2) {
                        redPackageEntity.setSubTitle(messageContent);
                    } else {
                        redPackageEntity.setTitle(items[1]);
                        redPackageEntity.setSubTitle(items[0]);
                    }

                    LogUtils.i(TAG, "员工红包单独处理 setSubTitle" + items[0] + "  员工红包单独处理 setTitle" + items[1]);
                    //员工红包默认 STAFF
                    redPackageEntity.setPushRedPacketType(Constants.PUSH_REDPACKET_STAFF_TYPE);
                    sendRedPacketNotification(redPackageEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, messageType, 10122);
                } else if (TextUtils.equals(messageType, EWA_TO_TAL_GP_PARAM)
                        || TextUtils.equals(messageType, EWA_U_PLAN_HOME_PARAM)
                        || TextUtils.equals(messageType, EWA_NEW_MESSAGE)
                        || TextUtils.equals(messageType, EWA_MY_DISCOUNT_PARAM)
                        || TextUtils.equals(messageType, EWA_CREDIT_AWARD_PARAM)) {
                    //EWA_TO_TAL_GP_PARAM     查询账户总积分
                    //EWA_U_PLAN_HOME_PARAM   优计划首页
                    //EWA_NEW_MESSAGE         最新消息
                    //EWA_MY_DISCOUNT_PARAM   我的优惠券
                    //EWA_CREDIT_AWARD_PARAM  查询信用卡奖赏记录
                    SevenVersionNotificationEntity sevenVersionNotificationEntity = new SevenVersionNotificationEntity();
//                    sevenVersionNotificationEntity.setTitle(remoteMessage.getNotification().getTitle());
//                    sevenVersionNotificationEntity.setSubTitle(remoteMessage.getNotification().getBody());
                    sevenVersionNotificationEntity.setTitle(remoteMessage.getData().get("message"));
                    sendNotificationSevenVersion(sevenVersionNotificationEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, messageType);
                } else {
                    RedPackageEntity redPackageEntity = new RedPackageEntity();
                    int msgType = 0;
                    if (!TextUtils.isEmpty(messageType)) {
                        if (Constants.MSGTYPE_VCNTF.equals(messageType)) {
                            msgType = MSG_VIRTUALCARD_BIND;
                        } else if (Constants.MSGTYPE_CONRE.equals(messageType)) {
                            //消费奖赏通知类型
                            msgType = MSG_REWARD;
                        }
                    }
                    String messageContent = remoteMessage.getData().get("message");
                    //后边是标题 前边是内容  如果没有分割符^，那么只设定内容
                    String items[] = messageContent.split("\\^");
                    if (null == items || items.length < 2) {
                        redPackageEntity.setSubTitle(messageContent);
                    } else {
                        redPackageEntity.setTitle(items[1]);
                        redPackageEntity.setSubTitle(items[0]);
                    }
                    sendNotification(redPackageEntity, getApplicationContext(), CHANNEL_ID, CHANNEL_NAME, msgType);
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @param sevenVersionNotificationEntity
     * @param mContext
     * @param channelId
     * @param channelName
     * @param messageType
     */
    private void sendNotificationSevenVersion(SevenVersionNotificationEntity sevenVersionNotificationEntity, Context mContext, String channelId, String channelName, String messageType) {
        //中转页面监听点击事件
        Intent intentClick = new Intent(this, SplashActivity.class);

//        1.在Activity上下文之外启动Activity需要给Intent设置FLAG_ACTIVITY_NEW_TASK标志，不然会报异常。
//        2.加了该标志，如果在同一个应用中进行Activity跳转，不会创建新的Task，只有在不同的应用中跳转才会创建新的Task
//        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

//        FLAG_ACTIVITY_NEW_TASK + FLAG_ACTIVITY_CLEAR_TOP的情况下，AndroidManifest.xml中设置activity的启动模式为standard或singleTask时activity入栈方式是不一样的。分为如下3个情况
//        1.当启动模式为standard时，如果activity所需的栈中已经存在该activity的实例了，那么这个实例连同它之上的activity都要出栈，然后再新建一个activity实例入栈
//        2.当启动模式为singleTask时，如果activity所需的栈中已经存在该activity的实例了，那么系统会调用该实例的onNewIntent()方法，且只将该实例之上的activity出栈
//        3.如果activity所需的栈中不存在该activity的实例，则不论启动模式为standard还是singleTask，都是新建activity实例直接入栈 --- 该场景属于此情况
        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        intentClick.putExtra(SEVEN_VERSION_NOTIFICATION_PARAMS, sevenVersionNotificationEntity);
        intentClick.putExtra(MESSAGE_TYPE, messageType);
        sendNotification(mContext, intentClick, sevenVersionNotificationEntity, channelId, channelName, REQUEST_CODE++);
    }

    private void sendRedPacketNotification(RedPackageEntity packageEntity, Context mContext, String id, String name, String redParams, int requestCode) {
        //中转页面监听点击事件
        Intent intentClick = new Intent(this, SplashActivity.class);
        intentClick.putExtra(RED_PACKET_RED_PACKET_PARAMS, packageEntity);
        intentClick.putExtra(MESSAGE_TYPE, redParams);
//        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sendNotification(mContext, intentClick, packageEntity, id, name, requestCode);
    }

    private void sendRedPacketNotification(RedPackageEntity packageEntity, Context mContext, String id, String name, String redParams) {
        //中转页面监听点击事件
        Intent intentClick = new Intent(this, SplashActivity.class);
//        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intentClick.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intentClick.putExtra(RED_PACKET_RED_PACKET_PARAMS, packageEntity);
        intentClick.putExtra(MESSAGE_TYPE, redParams);
        sendNotification(mContext, intentClick, packageEntity, id, name, REQUEST_CODE++);
    }


    private void sendNotification(RedPackageEntity packageEntity, Context mContext, String id, String name, int msgType) {

        // 区分登陆
        if (CacheManagerInstance.getInstance().isLogin()) {
            Intent intent = new Intent(mContext, MainHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (msgType == MSG_VIRTUALCARD_BIND) {
                //虚拟卡推送类型
                intent.putExtra(Constants.MSGTYPE_VCNTF, true);
                //要判断app此时是否在前台 如果在前台需要发通知 弹框提示
                if (ProjectApp.getFrontActivityNumber() != 0) {
                    //app在前台
                    EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_NOTIFIACTION_VIRTUALCARD_BIND, ""));
                } else {
                    sendNotification(mContext, intent, packageEntity, id, name);
                }
            } else if (msgType == MSG_REWARD) {
                //消费奖赏推送类型 不管前台后台都直接发推送
                intent.putExtra(Constants.MSGTYPE_CONRE, true);
                sendNotification(mContext, intent, packageEntity, id, name);
            } else {
                //其他类型
                //intent.putExtra(Constants.AUTO_SHOW_PACKAGE, true);
                sendNotification(mContext, intent, packageEntity, id, name);
            }
        } else {
            Intent intent = new Intent(mContext, PreLoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            sendNotification(mContext, intent, packageEntity, id, name);

        }
    }

    /**
     * 发送红包通知
     *
     * @param mContext
     * @param intent
     * @param packageEntity
     * @param id
     * @param name
     * @param requestCode
     */
    private void sendNotification(Context mContext, Intent intent, RedPackageEntity packageEntity, String id, String name, int requestCode) {
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap iconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        mBuilder.setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle(packageEntity.getTitle()).
                setContentText(packageEntity.getSubTitle()).
                setAutoCancel(true).
                setLargeIcon(iconBitmap).
                setSound(defaultSoundUri).
                setContentIntent(pendingIntent);
        //创建大文本样式
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.bigText(packageEntity.getSubTitle());
        mBuilder.setStyle(bigTextStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mBuilder.setChannelId(id);
            notificationManager.createNotificationChannel(channel);
        }
        String tag =  System.currentTimeMillis() + "";
        int idFinal = NOTIFICATION_ID++;
        LogUtils.i(TAG,"tag:"+tag + "id: " + idFinal  + " requestCode："+requestCode);
        notificationManager.notify(System.currentTimeMillis() + "",idFinal, mBuilder.build());
    }

    /**
     * 发送 7月版 通知
     *
     * @param mContext
     * @param intent
     * @param sevenVersionNotificationEntity
     * @param id
     * @param name
     * @param requestCode
     */
    private void sendNotification(Context mContext, Intent intent, SevenVersionNotificationEntity sevenVersionNotificationEntity, String id, String name, int requestCode) {
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap iconBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        mBuilder.setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle(sevenVersionNotificationEntity.getTitle()).
                setAutoCancel(true).
                setLargeIcon(iconBitmap).
                setSound(defaultSoundUri).
                setContentIntent(pendingIntent);
//        //创建大文本样式
//        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
//        bigTextStyle.bigText(sevenVersionNotificationEntity.getSubTitle());
//        mBuilder.setStyle(bigTextStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mBuilder.setChannelId(id);
            notificationManager.createNotificationChannel(channel);
        }
        String tag =  System.currentTimeMillis() + "";
        int idFinal = NOTIFICATION_ID++;
        LogUtils.i(TAG,"tag:"+tag + "id: " + idFinal  + " requestCode："+requestCode);
        notificationManager.notify(System.currentTimeMillis() + "",idFinal, mBuilder.build());

    }

    private void sendNotification(Context mContext, Intent intent, RedPackageEntity packageEntity, String id, String name) {
        sendNotification(mContext, intent, packageEntity, id, name, 0);
    }
}
