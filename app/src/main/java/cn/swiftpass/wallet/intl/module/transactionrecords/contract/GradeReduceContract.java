package cn.swiftpass.wallet.intl.module.transactionrecords.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;

public class GradeReduceContract {

    public interface View extends IView {

        void gradeReduceInfoSuccess(SweptCodePointEntity sweptCodePointEntity);

        void gradeReduceInfoFailed(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<GradeReduceContract.View> {
        void gradeReduceInfo(String txnId);
    }
}
