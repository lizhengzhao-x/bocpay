package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.DecimalInputTextWatcher;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.module.login.SelectLoginCodeActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferBaseContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.FpsCheckEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferBasePresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

/**
 * Created by ZhangXinchao on 2019/11/4.
 * 转账基类 转账/派利是 个别区别
 */
public abstract class BaseTransferActivity<K extends TransferBaseContract.Presenter<? extends TransferBaseContract.View>> extends BaseCompatActivity<K> implements TransferBaseContract.View {
    /**
     * 账户余额
     */
    protected AccountBalanceEntity accountBalanceEntity;
    protected String mCurrentBankName, mCurrentBankCode;
    protected String currentPhoneStr, currentAreaStr, currentUserName;
    protected int lastSelTransferType;
    protected boolean isSupportBocPay, isSupportFps, isOpenSmartAccount;
    protected boolean isEmailTransfer;
    protected boolean isInviteStatus = false;
    protected boolean isPaiLiShi = false;
    //是否是商户
    protected boolean isMerchantType = false;

    //是否是信用卡
    private String bocpaySign;

    private TextView TvTransferBtnNext;

    MySmartAccountEntity mSmartAccountInfo;

    /**
     * 两位小数过滤
     *
     * @param idTransferMoney
     */
    protected void initEditTextView(EditTextWithDel idTransferMoney) {
        idTransferMoney.getEditText().setFilters(new InputFilter[]{new DecimalInputTextWatcher(9, 2)});
        idTransferMoney.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        currentPhoneStr = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        currentAreaStr = getIntent().getExtras().getString(Constants.CHOICE_AREA);
        currentUserName = getIntent().getExtras().getString(Constants.USER_NAME);
        isEmailTransfer = getIntent().getBooleanExtra(Constants.IS_EMAIL, false);
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 共同的控件初始化操作
     *
     * @param idTransferBankLayout
     * @param idTransferTypeBocpay
     */
    protected void initSelBankLayout(View idTransferBankLayout, View idTransferTypeBocpay, final TextView idTransferBtnNext, final TextView tvRegionCodeStr, ImageView idAvailableAmount) {

        idTransferBankLayout.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onClickSelBank();
            }
        });
        if (!isEmailTransfer) {
            idTransferTypeBocpay.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    showSelTransferTypeDialog();
                }
            });
        }

        idTransferBtnNext.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //判断转账按钮当前是转账 还是邀请好友词条
                if (idTransferBtnNext.getText().toString().equals(getString(R.string.TF2101_20_13))) {
                    transferPreCheck();
                } else {
                    shareEvent();
                }

            }
        });

        tvRegionCodeStr.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onclickSelArea(tvRegionCodeStr.getText().toString());
            }
        });

        if (idAvailableAmount != null) {
            idAvailableAmount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /**
                     * 不需要获取 MySmartAccountEntity，眼睛打开 mSmartAccountInfo一定不为空
                     */
                    if (null != mSmartAccountInfo) {
                        String str = new Gson().toJson(mSmartAccountInfo);
                        try {
                            SmartAccountAdjustDailyLimitActivity.startActivity(getActivity(), mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                        } catch (Exception e) {

                        }
                    }
                }
            });
        }
    }

    private void shareEvent() {
        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
//        if (BuildConfig.FLAVOR.equals(Constants.BUILD_FLAVOR_UAT)) {
        textIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.TFI_7));
//        } else {
//            textIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.TFI_7_1));
//        }
        startActivity(Intent.createChooser(textIntent, "Share"));
    }

    /**
     * 获取我的账户信息
     */
    private void getSmartAccountInfo() {
        mPresenter.getSmartAccountInfo();
    }

    /**
     * 更改手机号 区号
     *
     * @param areaTxt
     */
    protected void onclickSelArea(String areaTxt) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.LAST_SELECT_COUNTRY, areaTxt);
        ActivitySkipUtil.startAnotherActivityForResult(this, SelectLoginCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_COUNTRY);
    }

    /**
     * @param areaStr
     * @param phoneNumberStr
     * @param remark
     * @param transferAmount
     * @param isLiShi
     * @param idSelBankView  判断当前是bocpay 转账 还是 fps
     */
    protected void preTransferBocPayAction(String areaStr, String phoneNumberStr, String remark, String transferAmount, final boolean isLiShi, View idSelBankView, final TextView idTransferBtnNext, String coverId, String billNo) {
        TvTransferBtnNext = idTransferBtnNext;

        //防止页面跳转 按钮多次点击
        idTransferBtnNext.setEnabled(false);
        if (idSelBankView.isShown()) {
            preTransferFpsAction(areaStr, phoneNumberStr, remark, transferAmount, mCurrentBankCode, isLiShi, idTransferBtnNext, coverId, billNo);
            return;
        }
        final TransferPreCheckReq req = new TransferPreCheckReq();
        String countryCode = "";
        if (!TextUtils.isEmpty(areaStr)) {
            countryCode = areaStr.replace("+", "");
        }
        req.transferType = isEmailTransfer ? TransferConst.TRANS_TYPE_EMAIL : TransferConst.TRANS_TYPE_PHONE;
        final String tansferAccount = (countryCode + "-" + phoneNumberStr).replace(" ", "");
        req.phone = tansferAccount;
        req.postscript = remark.trim();
        req.transferAmount = transferAmount;
        req.tansferToAccount = req.phone;
        req.isLiShi = isLiShi;
        req.coverId = coverId;
        req.billReference = billNo;
        mPresenter.transferPreByBoc(isLiShi, req, tansferAccount);
    }

    /**
     * 根据输入金额参数控制next状态
     *
     * @param s
     */
    protected void dealWithExceedMaxMoney(String s, TextView idAvailableAmount, TextView idTransferBtnNext, View idSelBankView, String referenceNo) {
        //在这里先判断交易参考编号
        if (referenceNo != null && referenceNo.length() == 0) {
            updateOkBackground(idTransferBtnNext, false);
            return;
        }
        if (TextUtils.isEmpty(s)) {
            if (accountBalanceEntity != null) {
                if (null != idAvailableAmount) {
                    idAvailableAmount.setText("");
                    idAvailableAmount.setVisibility(View.GONE);
                }
                updateOkBackground(idTransferBtnNext, false);
                checkEditMoney(false);
            }
            return;
        }

        double currentV = 0;
        try {
            currentV = Double.valueOf(s);
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (currentV == 0) {
            updateOkBackground(idTransferBtnNext, false);
            checkEditMoney(false);
            return;
        }
        double maxValue = 0;
        if (accountBalanceEntity != null && !TextUtils.isEmpty(accountBalanceEntity.getBalance())) {
            try {
                maxValue = Double.valueOf(accountBalanceEntity.getBalance());
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            if (null != idAvailableAmount) {
                if (currentV <= maxValue) {
                    idAvailableAmount.setText("");
                    idAvailableAmount.setVisibility(View.GONE);
                } else {
                    idAvailableAmount.setVisibility(View.VISIBLE);
                    idAvailableAmount.setText(getResources().getString(R.string.TF2101_4_11));
                    idAvailableAmount.setTextColor(ContextCompat.getColor(this, R.color.color_FFBF2F4F));
                }
            }


            if (isInviteStatus) {
                updateOkBackground(idTransferBtnNext, true);
                checkEditMoney(false);
                return;
            }
            if (idSelBankView.getVisibility() == View.VISIBLE) {
                //fps转账
                updateOkBackground(idTransferBtnNext, currentV <= maxValue && isSupportFps);
                checkEditMoney(!(currentV <= maxValue && isSupportFps));
                return;
            } else {
                //boc
                updateOkBackground(idTransferBtnNext, currentV <= maxValue && isSupportBocPay);
                checkEditMoney(!(currentV <= maxValue && isSupportBocPay));
            }

        } else {
            updateOkBackground(idTransferBtnNext, false);
            checkEditMoney(false);
        }
    }

    protected void initOriginalView(LinearLayout llyTransferContainName,TextView tvTransferPhoneTitle,TextView idTransferUserName, TextView etwdTransferPhoneNum,
                                    View ivSelRegionArrow, final TextView tvRegionCodeStr, ImageView idTransferUserHead, View isTransferTypeArrow) {
        if (!TextUtils.isEmpty(currentUserName)) {
            idTransferUserName.setText(currentUserName);
            llyTransferContainName.setVisibility(View.VISIBLE);
            tvTransferPhoneTitle.setVisibility(View.INVISIBLE);
        } else {
            llyTransferContainName.setVisibility(View.GONE);
            tvTransferPhoneTitle.setVisibility(View.VISIBLE);
            //idTransferUserName.setText(currentPhoneStr);
        }
        etwdTransferPhoneNum.setText(currentPhoneStr);
        if (null != ivSelRegionArrow) {
            ivSelRegionArrow.setVisibility(isEmailTransfer ? View.GONE : View.VISIBLE);
        }


        tvRegionCodeStr.setVisibility(isEmailTransfer ? View.GONE : View.VISIBLE);

        if (null != idTransferUserHead) {
            idTransferUserHead.setImageResource(isEmailTransfer ? R.mipmap.icon_transfer_profile_email : R.mipmap.icon_transfer_profile_mobile);
        }

        String phoneNumberEnd = currentAreaStr;
        if (!isEmailTransfer) {
            //手机号转账
            if (TextUtils.isEmpty(currentAreaStr)) {
                currentAreaStr = "+852";
            }
            if (currentPhoneStr.length() == Constants.MAX_CHINA_PHONE_LENGTH) {
                //用户要求 如果手机位数是11位 默认要86选择
                currentAreaStr = "+86";
            }
            tvRegionCodeStr.setText(currentAreaStr);
            phoneNumberEnd = currentAreaStr.replace("+", "") + "-" + currentPhoneStr;
            fpsCheckAction(Constants.TRANSFER_SEL_ACCOUNT_PHONE, phoneNumberEnd, false);
        } else {
            //邮箱转账
            isSupportFps = true;
            if (null != isTransferTypeArrow) {
                isTransferTypeArrow.setVisibility(View.INVISIBLE);
            }
            fpsCheckAction(Constants.TRANSFER_SEL_ACCOUNT_EMAIL, currentPhoneStr, false);
        }
        if (null != ivSelRegionArrow) {
            ivSelRegionArrow.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    onclickSelArea(tvRegionCodeStr.getText().toString());
                }
            });
        }
    }

    /**
     * pfs转账预下单
     *
     * @param areaStr
     * @param phoneNumberStr
     * @param remark
     * @param transferAmount
     * @param mBankCode
     * @param isLiShi
     */
    protected void preTransferFpsAction(String areaStr, String phoneNumberStr, String remark, String transferAmount, String mBankCode, final boolean isLiShi, final TextView idTransferBtnNext, String coverId, String billNo) {
        final TransferPreCheckReq req = new TransferPreCheckReq();
        TvTransferBtnNext = idTransferBtnNext;
        req.transferType = isEmailTransfer ? TransferConst.TRANS_TYPE_EMAIL : TransferConst.TRANS_TYPE_PHONE;
        req.bankType = TransferConst.TRANS_BANK_TYPE_FPS;
        if (!TextUtils.isEmpty(mBankCode)) {
            req.bankName = mBankCode;
        }
        String countryCode = "";
        if (!TextUtils.isEmpty(areaStr)) {
            countryCode = areaStr.replace("+", "");
        }
        String tansferAccount = phoneNumberStr;
        if (isEmailTransfer) {
        } else {
            tansferAccount = ("+" + countryCode + "-" + phoneNumberStr).replace(" ", "");
        }
        req.tansferToAccount = tansferAccount;
        req.postscript = remark.trim();
        req.transferAmount = transferAmount;
        req.isQrcode = false;
        req.isLiShi = isLiShi;
        req.coverId = coverId;
        req.billReference = billNo;

        mPresenter.checkTransferPre(true, isLiShi, req);
    }

    /**
     * 切换区号 检查是否支持bocpay/fps
     */
    protected void fpsCheckAction(String transferType, String tansferToAccount, boolean showDialog) {
        mPresenter.fpsCheckAction(transferType, tansferToAccount, showDialog,isPaiLiShi);
    }

    protected abstract void onFpsCheckResult(FpsCheckEntity response);

    protected abstract void onFpsCheckErrorResult(String errorCode, String errorMsg);

    protected abstract TransferResourcesEntity.DefBean.BckImgsBean getBackImageBean();

    /**
     * 选择银行
     */
    public void onClickSelBank() {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.DATA_TYPE, Constants.DATA_SEL_BANK_LIST);
        mHashMapsLogin.put(Constants.HAS_EXTRA_SEL, true);
        ActivitySkipUtil.startAnotherActivityForResult(BaseTransferActivity.this, SelectCountryCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_BANK);
    }


    /**
     * 切换bocpay/fps 转账方式
     */
    private void showSelTransferTypeDialog() {
        TransferSelTypeDialogFragment transferSelTypeDialogFragment = new TransferSelTypeDialogFragment();
        transferSelTypeDialogFragment.setOnTransferSelTypeClickListener(new TransferSelTypeDialogFragment.OnTransferSelTypeClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onTransferTypeSelListener(int positon) {
                lastSelTransferType = positon;
                selCurrentTransferType(positon);
            }
        });
        transferSelTypeDialogFragment.setCurrentSelPosition(lastSelTransferType, isSupportBocPay, isSupportFps, isOpenSmartAccount, isPaiLiShi,bocpaySign);
        transferSelTypeDialogFragment.show(getSupportFragmentManager(), TransferSelTypeDialogFragment.class.getName() + "");
    }


    /**
     * 选择区域 银行
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SEL_BANK) {
                getBankNameResult(data.getExtras().getString(Constants.BANK_CODE), data.getExtras().getString(Constants.BANK_NAME));
            } else if (requestCode == Constants.REQUEST_CODE_SEL_COUNTRY) {
                //bocPay转账 选择 852 86 853 限制输入手机号长度
                String countryCode = data.getExtras().getString(Constants.DATA_COUNTRY_CODE);
                updateAreaCode(countryCode);
            }
        }
    }

    /**
     * @param position
     * @param idTransferType
     * @param idTransferTypeBocpayImg
     * @param idTransferTypeNotSupportTv
     * @param idTransferTypeNotSupportView
     * @param idSelBankView
     * @param idTransferBtnNext
     */
    protected void updateSelTransferType(int position, TextView idTransferType, ImageView idTransferTypeBocpayImg, TextView idTransferTypeNotSupportTv, View idTransferTypeNotSupportView, View idSelBankView, TextView idTransferBtnNext) {
        idTransferTypeNotSupportTv.setTextColor(Color.RED);
        if (position == 1) {
            idTransferTypeNotSupportTv.setText(isSupportFps ? "" : getString(R.string.TR1_9_3_1));
            idTransferTypeNotSupportView.setVisibility(!isSupportFps ? View.VISIBLE : View.GONE);
        } else {
            if(isOpenSmartAccount){
                if (!TextUtils.equals(bocpaySign, Constants.ACCOUNT_TYPE_SMART)) {
                    idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_1));
                } else {
                    idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_2));
                }
            }else {
                idTransferTypeNotSupportTv.setText(getString(R.string.TFX2101_1_2));
            }

            idTransferTypeNotSupportView.setVisibility(!isSupportBocPay ? View.VISIBLE : View.GONE);
        }

        idSelBankView.setVisibility(position == 1 ? View.VISIBLE : View.GONE);
        idTransferType.setText(position == 1 ? getString(R.string.TF2101_4_4) : getString(R.string.TF2101_4_5));

        idTransferTypeBocpayImg.setImageResource(position == 1 ? R.mipmap.logo_transfer_fps : R.mipmap.logo_transfer_bocpay);
        if (idTransferTypeNotSupportView.isShown()) {
            //判断转账按钮当前是转账 还是邀请好友词条
            isInviteStatus = true;
            idTransferBtnNext.setText(getString(R.string.TR1_10_1));
            updateOkBackground(idTransferBtnNext, true);
            hideButtomFillInfoView(true);
        } else {
            isInviteStatus = false;
            hideButtomFillInfoView(false);
            idTransferBtnNext.setText(getString(R.string.TF2101_20_13));
            updateNextStatus();
        }

    }

    protected void setDefaultTransferType(FpsCheckEntity response, TextView idTransferType, ImageView idTransferTypeBocpayImg, View idSelBankView) {
        isSupportBocPay = response.getBocpaySign();
        isSupportFps = response.getFpsSign();
        bocpaySign = response.bocpaySign;
        isMerchantType = response.isMerchantType();
        isOpenSmartAccount = response.isOpenAccount();
        if (isSupportBocPay && isSupportFps) {
            //两种支付方式都支持 默认选中fps
            lastSelTransferType = 1;
            idTransferType.setText(getString(R.string.TR1_9_5));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_fps);
            idSelBankView.setVisibility(View.VISIBLE);
        } else if (!isSupportBocPay && !isSupportFps) {
            //两种方式都不支持 默认bocpay
            lastSelTransferType = 0;
            idTransferType.setText(getString(R.string.TR1_9_4));
            idTransferTypeBocpayImg.setImageResource(R.mipmap.logo_transfer_bocpay);
            idSelBankView.setVisibility(View.GONE);
            hideButtomFillInfoView(true);
        } else {
            //只支持一种
            lastSelTransferType = isSupportBocPay ? 0 : 1;
            idTransferType.setText(isSupportBocPay ? getString(R.string.TR1_9_4) : getString(R.string.TR1_9_5));
            idTransferTypeBocpayImg.setImageResource(isSupportBocPay ? R.mipmap.logo_transfer_bocpay : R.mipmap.logo_transfer_fps);
            idSelBankView.setVisibility(isSupportBocPay ? View.GONE : View.VISIBLE);
        }
    }

//    @Override
//    protected TransferBaseContract.Presenter<TransferBaseContract.View> createPresenter() {
//        return new TransferBasePresenter<>();
//    }


    @Override
    protected K createPresenter() {
        return (K) new TransferBasePresenter<>();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {

    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {

    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        SmartAccountAdjustDailyLimitActivity.startActivity(getActivity(), response, response.getHideMobile());
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    @Override
    public void getAccountBalanceInfoSuccess(AccountBalanceEntity response) {

    }

    @Override
    public void getAccountBalanceInfoError(String errorCode, String errorMsg) {

    }

    @Override
    public void checkTransferPreSuccess(TransferPreCheckEntity response, boolean isLishi, TransferPreCheckReq req) {
        response.setPreCheckReq(req);
        response.setAccountNo(req.tansferToAccount);
        response.setTransferType(req.transferType);
        response.setBocPayTransfer(false);
        response.setUserName(currentUserName);
        if (isLishi) {
            response.setBckImgsBean(getBackImageBean());
            TransferLiShiConfirmMoneyActivity.startActivity(getActivity(), response);
        } else {
            TransferConfirmMoneyActivity.startActivity(getActivity(), response);
        }
    }


    @Override
    public void checkTransferPreError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
        TvTransferBtnNext.setEnabled(true);
    }

    @Override
    public void fpsCheckActionSuccess(FpsCheckEntity response) {
        onFpsCheckResult(response);
    }

    @Override
    public void fpsCheckActionError(String errorCode, String errorMsg) {
        onFpsCheckErrorResult(errorCode, errorMsg);
    }

    @Override
    public void transferPreByBocSuccess(boolean isLiShi, TransferPreCheckReq req, String tansferAccount, TransferPreCheckEntity response) {
        response.setPreCheckReq(req);
        response.setAccountNo(tansferAccount);
        response.setBocPayTransfer(true);
        response.setTransferType(req.transferType);
        response.setUserName(currentUserName);
        if (isLiShi) {
            response.setBckImgsBean(getBackImageBean());
            TransferLiShiConfirmMoneyActivity.startActivity(getActivity(), response);
        } else {
            TransferConfirmMoneyActivity.startActivity(getActivity(), response);
        }
    }


    @Override
    public void transferPreByBocError(String errorCode, String errorMsg) {
        if (errorCode.equals(ErrorCode.TRANSFER_NO_ACCOUNT.code)) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), InviteServiceActivity.class);
        } else {
            AndroidUtils.showTipDialog(getActivity(), errorMsg);
        }
        if (null != TvTransferBtnNext) {
            TvTransferBtnNext.setEnabled(true);
        }
    }

    abstract void hideButtomFillInfoView(boolean isHide);

    abstract void transferPreCheck();

    abstract void updateAreaCode(String code);

    abstract void selCurrentTransferType(int position);

    abstract void getBankNameResult(String mCurrentBankCode, String mCurrentBankName);

    abstract void updateNextStatus();

    /**
     * 判断金额输入框 数字是否大于今日可用交易限额
     *
     * @param isRed
     */
    abstract void checkEditMoney(boolean isRed);
}
