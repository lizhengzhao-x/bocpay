package cn.swiftpass.wallet.intl.module.register;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CVVDialog;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegisterCreditEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.ForgetPwdEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.app.ProjectApp.getContext;

/**
 * 信用卡注册界面 填写cvv界面
 */
public class RegisterInputBankCardInfoActivity extends BaseCompatActivity {
    private static final String TAG = "RegisterInputBankCardInfoActivity";
    @BindView(R.id.fl_cards_show)
    FrameLayout flCardsShow;
    @BindView(R.id.iv_question_mark)
    ImageView ivQuestionMark;
    @BindView(R.id.etwd_expired_date)
    EditTextWithDel etwdExpiredDate;
    @BindView(R.id.etwd_cvv)
    EditTextWithDel etwdCvv;
    @BindView(R.id.tv_card_info_nextPage)
    TextView tvCardInfoNextPage;
    @BindView(R.id.iv_confirm_mark)
    ImageView ivConfirmMark;
    @BindView(R.id.id_confirm_conditions)
    TextView idConfirmConditions;
    @BindView(R.id.id_confirm_conditions_end)
    TextView idConfirmConditionsEnd;
    @BindView(R.id.id_conditions)
    LinearLayout idConditions;
    @BindView(R.id.cet_verify_code)
    CustomTvEditText mVerifyCodeET;
    @BindView(R.id.iv_verify_code)
    ImageView mVerifyCodeIV;
    @BindView(R.id.iv_refresh_code)
    ImageView mRefreshCodeIV;
    @BindView(R.id.id_verifycode_view)
    LinearLayout idVerifycodeView;
    /**
     * 跳转type 1.注册 2.绑卡
     */
    private int mPageFlow;
    private RelativeLayout LayoutCardInfoFront = null;
    private RelativeLayout LayoutCardInfoback = null;
    private FrameLayout.LayoutParams layoutParamsback;
    private FrameLayout.LayoutParams layoutParamsfront;
    private int ScreenWidth;
    private TextView tv_expiry;
    private TextView tv_CVV;
    private TextView tv_Number;
    private String cardStr;

    private CVVDialog Dialog_CVV;
    public String repalceSpaceStr = "/";
    public String lastString = null;
    public int deleteSelect;
    private int beforeL, beforeT, afterL, afterT;
    /**
     * 忘记密码手机号
     */
    private String mForgetPwdPhone;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        EventBus.getDefault().register(this);

        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        AndroidUtils.hideKeyBoardInput(this);
        initView();
        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
            setToolBarTitle(R.string.setting_payment_fgw);
            mForgetPwdPhone = getIntent().getExtras().getString(Constants.MOBILE_PHONE);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            setToolBarTitle(R.string.bind_title);
        } else {
            setToolBarTitle(R.string.IDV_2_1);
        }
    }

    private void initView() {

        cardStr = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        LayoutCardInfoFront = (RelativeLayout) mInflater.inflate(R.layout.item_card_info_front, null);
        LayoutCardInfoback = (RelativeLayout) mInflater.inflate(R.layout.item_card_info_back, null);

        tv_Number = LayoutCardInfoFront.findViewById(R.id.card_info_number);
        tv_expiry = LayoutCardInfoFront.findViewById(R.id.tv_expiry);
        tv_CVV = LayoutCardInfoback.findViewById(R.id.tv_CVV);
        if (!TextUtils.isEmpty(cardStr)) {
            tv_Number.setText(AndroidUtils.formatCardNumberStr(cardStr));
        }
        //控件的宽高
        int width = (int) getResources().getDimension(R.dimen.space_430_px);
        int height = (int) getResources().getDimension(R.dimen.space_280_px);
        //两个控件的宽高错位的尺寸
        int Space_W = (int) getResources().getDimension(R.dimen.space_90_px);
        int Space_H = (int) getResources().getDimension(R.dimen.space_30_px);
        //获取屏幕宽高
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        ScreenWidth = wm.getDefaultDisplay().getWidth();
        //因为限制高度，故此处宽的尺寸要按照控件的尺寸来
        int layout_height = (int) getResources().getDimension(R.dimen.space_340_px);

        beforeL = (ScreenWidth - (width + Space_W)) / 2;
        beforeT = (layout_height - (height + Space_H)) / 2;

        afterL = beforeL + Space_W;
        afterT = beforeT + Space_H;

        layoutParamsback = new FrameLayout.LayoutParams(width, height);
        layoutParamsback.setMargins(afterL, afterT, 0, 0);
        flCardsShow.addView(LayoutCardInfoback, layoutParamsback);

        layoutParamsfront = new FrameLayout.LayoutParams(width, height);
        layoutParamsfront.setMargins(beforeL, beforeT, 0, 0);
        flCardsShow.addView(LayoutCardInfoFront, layoutParamsfront);

        etwdExpiredDate.hideErrorView();
        etwdExpiredDate.hideDelView();
        etwdExpiredDate.addTextChangedListener(textWatcher_etwdExpiredDate);
        etwdExpiredDate.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (hasFocus) {
                    //如果是ExpiredDate，如果文本改变的话，就更改layout层级。front在前面
                    layoutParamsback.setMargins(afterL, afterT, 0, 0);
                    layoutParamsfront.setMargins(beforeL, beforeT, 0, 0);
                    flCardsShow.bringChildToFront(LayoutCardInfoFront);
                }
            }
        });
        etwdCvv.hideErrorView();
        etwdCvv.hideDelView();
        etwdCvv.addTextChangedListener(textWatcher_etwdCvv);
        etwdCvv.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (hasFocus) {
                    //如果是CVV，如果焦点改变的话，就更改layout层级。back在前面
                    layoutParamsfront.setMargins(afterL, afterT, 0, 0);
                    layoutParamsback.setMargins(beforeL, beforeT, 0, 0);
                    flCardsShow.bringChildToFront(LayoutCardInfoback);
                }
            }
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //默认按钮设为不可用状态
        updateOkBackground(false);
        String totalStr = getResources().getString(R.string.TEMP_4_1);
        BasicUtils.initSpannableStrWithTv(totalStr, "##", idConfirmConditions, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitySkipUtil.startAnotherActivity(RegisterInputBankCardInfoActivity.this, ServerAgreementActivity.class);
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RegisterEventEntity event) {
        if (event.getEventType() == RegisterEventEntity.EVENT_REGISTER) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
    }

    private void updateOkBackground(boolean isSel) {
        if (isSel && !ivConfirmMark.isSelected()) {
            return;
        }
        tvCardInfoNextPage.setEnabled(isSel);
        tvCardInfoNextPage.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }


    private TextWatcher textWatcher_etwdExpiredDate = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //因为重新排序之后setText的存在，从而会重新进行一次输入的过程
            //会导致输入框的内容从0开始输入，这里是为了避免这种情况产生一系列问题
            //此方法会替换从start开始的count个字符替换EditText旧的长度为before个字符即旧文本
            //这种情况下不会改变光标的位置，所以直接return掉
            if (start == 0 && count > 0) {
                return;
            }
            //如果是第一次设置值，如果当前的没有内容也直接return掉，默认为不改变光标位置
            String editTextContent = etwdExpiredDate.getText();
            if (TextUtils.isEmpty(editTextContent) || TextUtils.isEmpty(lastString)) {
                return;
            }
            //然后根据输入的内容来得到加完斜杠之后要显示的内容
            editTextContent = BasicUtils.addLineByInputContent(s.toString(), 5, repalceSpaceStr);
            if (tv_expiry != null) {
                tv_expiry.setText(editTextContent);
            }

            //如果最新的长度 < 上次的长度，代表进行了删除
            if (editTextContent.length() < lastString.length()) {
                deleteSelect = start;
            } else {
                deleteSelect = editTextContent.length();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            //对输入的数字，每2位加一个斜杠
            //获取输入框中的内容,不可以去斜杠
            String etContent = etwdExpiredDate.getEditText().getText().toString();
            //重新加入斜杠拼接字符串
            String newContent = BasicUtils.addLineByInputContent(s.toString(), 5, repalceSpaceStr);
            //保存本次字符串数据
            lastString = newContent;
            //如果有改变，则重新填充
            //防止EditText无限setText()产生死循环
            if (!etContent.equals(newContent)) {
                etwdExpiredDate.setContentText(newContent);
                checkButtonState();
                //保证光标的位置
                etwdExpiredDate.getEditText().setSelection(deleteSelect > newContent.length() ? newContent.length() : deleteSelect);
            }
            String cvvStr = etwdCvv.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(etContent) && !TextUtils.isEmpty(cvvStr) && cvvStr.length() >= 3 && newContent.length() >= 5) {
                updateOkBackground(true);
            } else {
                updateOkBackground(false);
            }

            if (etContent.length() >= 5) {
                etwdExpiredDate.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        etwdExpiredDate.setFocusable(false);
                        etwdExpiredDate.hideDelView();
                    }
                }, 100);
                etwdExpiredDate.clearFocus();
                etwdCvv.getEditText().requestFocus();
            }
        }
    };


    private TextWatcher textWatcher_etwdCvv = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //文本修改就更新textView
            if (tv_CVV != null) {
                tv_CVV.setText(s);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkButtonState();
            String etContent = etwdCvv.getText();
            //cvv输入完成之后隐藏软键盘
            if (etContent.length() >= 3) {
                etwdCvv.hideDelView();
                AndroidUtils.hideKeyboard(etwdCvv);
            }
        }
    };

    public void checkButtonState() {
        //如果同意条款，则检查每个输入框的内容
        if (ivConfirmMark.isSelected()) {
            //int length_cardHolder = etwdName.getTextView().length();
            int length_Cvv = etwdCvv.getTextView().length();
            int length_ExpiredDate = etwdExpiredDate.getTextView().length();
            if (length_Cvv > 2 && length_ExpiredDate > 0) {
                //虚拟卡 有验证码 多一条验证规则
                if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER || mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                    updateOkBackground(mVerifyCodeET.getInputText().length() == 4);
                } else {
                    updateOkBackground(true);
                }
            } else {
                updateOkBackground(false);
            }
        } else {
            //如果没有同意条款，则默认按钮设为不可用状态
            updateOkBackground(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
    }

    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (etwdExpiredDate==null) return;
                etwdExpiredDate.getEditText().setFocusable(true);
                etwdExpiredDate.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etwdExpiredDate.getEditText());
            }
        }, 200);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_register_input_bank_card_info;
    }


    @OnClick({R.id.fl_cards_show, R.id.etwd_expired_date, R.id.iv_question_mark, R.id.etwd_cvv, R.id.tv_card_info_nextPage, R.id.iv_confirm_mark})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fl_cards_show:
                break;

            case R.id.etwd_expired_date:
                break;
            case R.id.iv_question_mark:
                Dialog_CVV = new CVVDialog(RegisterInputBankCardInfoActivity.this);
                if (Dialog_CVV != null && !Dialog_CVV.isShowing()) {
                    Dialog_CVV.show();
                }
                Window dialogWindow = Dialog_CVV.getWindow();
                WindowManager m = getActivity().getWindowManager();
                Display d = m.getDefaultDisplay();
                // 获取对话框当前的参数值
                WindowManager.LayoutParams p = dialogWindow.getAttributes();
//                // 高度设置为屏幕的0.45，根据实际情况调整
//                p.height = (int) (d.getHeight() * 0.47);
//                // 宽度设置为屏幕的0.65，根据实际情况调整
//                p.width = (int) (d.getWidth() * 0.65);
                dialogWindow.setAttributes(p);
                break;
            case R.id.etwd_cvv:
                break;
            case R.id.tv_card_info_nextPage:
                if (!ButtonUtils.isFastDoubleClick(R.id.tv_card_info_nextPage)) {
                    submitCardInfo();
                }
                break;
            case R.id.iv_confirm_mark:
                ivConfirmMark.setSelected(!ivConfirmMark.isSelected());
                checkButtonState();
                break;
            default:
                break;
        }
    }


    private void submitCardInfo() {
        final String cardNumber = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        String content = etwdExpiredDate.getEditText().getText().toString().trim();
        //去掉所有斜杠
        final String expiryDateStr = content.replaceAll("/", "");
        String cvvStr = etwdCvv.getEditText().getText().toString().trim();
        registerByCreditCard(cardNumber, cvvStr, expiryDateStr, null);
    }


    //isContinue --卡手机号码已
    public void registerByCreditCard(final String cardNumber, final String cvvStr, final String expiryDateStr, String isContinue) {
        String flow = "";
        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
            flow = ApiConstant.FLOW_FORGET_PASSWORD;
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
            flow = ApiConstant.FLOW_REGISTER;
        } else {
            flow = ApiConstant.FLOW_BINDING;
        }

        ApiProtocolImplManager.getInstance().getRegisterByCreditCard(mContext, cardNumber, mForgetPwdPhone, cvvStr, expiryDateStr, flow, isContinue, new NetWorkCallbackListener<RegisterCreditEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if ((TextUtils.equals(errorCode, ErrorCode.CARD_PHONE_CHANGE_REG.code) && mPageFlow == Constants.PAGE_FLOW_REGISTER) ||
                        (TextUtils.equals(errorCode, ErrorCode.CARD_PHONE_CHANGE_BIND.code) && mPageFlow == Constants.PAGE_FLOW_BIND_CARD) ||
                        (TextUtils.equals(errorCode, ErrorCode.MODIFY_PASSCODE_QUIT.code) && mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD)) {
                    String msg = errorMsg;
                    showErrorMsgDialog(RegisterInputBankCardInfoActivity.this, msg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            registerByCreditCard(cardNumber, cvvStr, expiryDateStr, "1");
                        }
                    });

                } else if ((TextUtils.equals(errorCode, ErrorCode.SERVER_ACCOUNT_NOT_SAME.code) && mPageFlow == Constants.PAGE_FLOW_BIND_CARD) || (TextUtils.equals(errorCode, ErrorCode.CARD_INFO_HAD_REG.code))) {
                    //绑卡操作
//                    "當前登錄賬戶擁有人與BoC Pay註冊客戶並非同一人
//                    (1) 不應跳轉至SMS OTP版面才彈出正確拒納提示
//                    (2) 確認拒納提示後，沒有退出綁定"
                    showErrorMsgDialog(RegisterInputBankCardInfoActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            //登录状态下
                            if (CacheManagerInstance.getInstance().isLogin()) {
                                ActivitySkipUtil.startAnotherActivity(RegisterInputBankCardInfoActivity.this, MainHomeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                MyActivityManager.removeAllTaskExcludeMainStack();
                            } else {
                                ActivitySkipUtil.startAnotherActivity(RegisterInputBankCardInfoActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                            }

                        }
                    });
                } else {
                    showErrorMsgDialog(RegisterInputBankCardInfoActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
//                            EWA5635：资料不正确，退出当前页
//                            EWA5636：资料不正确，锁定24小时，退出到登录页
                            if (TextUtils.equals(errorCode, "EWA5635")) {
                                finish();
                                EventBus.getDefault().post(new ForgetPwdEventEntity(ForgetPwdEventEntity.EVENT_FORGET_PWD, ""));
                            } else if (TextUtils.equals(errorCode, "EWA5636")) {
                                finish();
                                EventBus.getDefault().post(new ForgetPwdEventEntity(ForgetPwdEventEntity.EVENT_FORGET_PWD_LOGIN, ""));
                            }
                        }
                    });
                }
            }

            @Override
            public void onSuccess(RegisterCreditEntity response) {
                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                    mHashMaps.put(Constants.DATA_PHONE_NUMBER, response.getMobile());
                    mHashMaps.put(Constants.DATA_CARD_NUMBER, cardNumber);
                    mHashMaps.put(Constants.WALLET_ID, response.getWalletId());
                    mHashMaps.put(Constants.CARDID, response.getCardId());
                    mHashMaps.put(Constants.EXDATE, expiryDateStr);
                    mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_CREDIT);
                    mHashMaps.put(Constants.USER_ACCOUNT, response.getMobile());
                    ActivitySkipUtil.startAnotherActivity(RegisterInputBankCardInfoActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                    mHashMaps.put(Constants.DATA_PHONE_NUMBER, response.getMobile());
                    mHashMaps.put(Constants.DATA_CARD_NUMBER, cardNumber);
                    mHashMaps.put(Constants.WALLET_ID, response.getWalletId());
                    mHashMaps.put(Constants.CARDID, response.getCardId());
                    mHashMaps.put(Constants.EXDATE, expiryDateStr);
                    mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_CREDIT);
                    mHashMaps.put(Constants.USER_ACCOUNT, response.getMobile());
                    ActivitySkipUtil.startAnotherActivity(RegisterInputBankCardInfoActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }
}
