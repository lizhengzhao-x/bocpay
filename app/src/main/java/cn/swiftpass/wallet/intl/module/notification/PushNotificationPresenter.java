package cn.swiftpass.wallet.intl.module.notification;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.api.protocol.NotificationNewMessageProtocol;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_NEW_MESSAGE;

public class PushNotificationPresenter<P extends PushNotificationContract.View> extends BasePresenter<P> implements PushNotificationContract.Presenter<P> {

    @Override
    public void getUPlanUrl() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetUplanurlProtocol(new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUPlanUrlFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUPlanUrlSuccess(response);
                }
            }
        }).execute();


    }

    @Override
    public void getNewMessageUrl() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new NotificationNewMessageProtocol(EWA_NEW_MESSAGE, new NetWorkCallbackListener<NotificationJumpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNewMessageUrlFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NotificationJumpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNewMessageUrlSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void getMyDiscountUrl() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetUplanurlProtocol("{\"action\":\"MERCHANT\"}", new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyDiscountUrlFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyDiscountUrlSuccess(response);
                }
            }
        }).execute();

    }
}
