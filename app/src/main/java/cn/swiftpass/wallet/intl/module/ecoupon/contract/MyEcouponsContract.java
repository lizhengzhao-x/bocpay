package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;

/**
 * 换领电子券
 */
public class MyEcouponsContract {

    public interface View extends IView {

        void getMyEcouponsListSuccess(MyEVoucherEntity response);

        void getMyEcouponsListError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<MyEcouponsContract.View> {

        void getMyEcouponsList();
    }

}
