package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class KeyBoardEntity extends BaseEntity {

    private String key;
    private String keyEng;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKeyEng() {
        return keyEng;
    }

    public void setKeyEng(String keyEng) {
        this.keyEng = keyEng;
    }
}
