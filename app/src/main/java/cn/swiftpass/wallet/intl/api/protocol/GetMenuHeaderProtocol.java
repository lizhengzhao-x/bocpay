package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class GetMenuHeaderProtocol extends BaseProtocol {

    public GetMenuHeaderProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/getSmartAndGpInfo";

    }


}
