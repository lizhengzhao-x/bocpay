package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.api.protocol.E2eePreProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualcardRegisterSetPwdProtocol extends BaseProtocol {
    VirtualCardListEntity.VirtualCardListBean virtualCardListBean;
    String walletId, payPwd;
    private String mPin, mSid;

    public VirtualcardRegisterSetPwdProtocol(VirtualCardListEntity.VirtualCardListBean virtualCardListBeanIn, String walletIdIn, String payPwdIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        virtualCardListBean = virtualCardListBeanIn;
        payPwd = payPwdIn;
        walletId = walletIdIn;
        mUrl = "api/virtual/reg/setpwd";
    }

    @Override
    public void packData() {
        super.packData();
//        walletId	String	C
//        payPwd	String	C
//        expiryDate	String	C
//        PAN	String	C
//        faceId	String	C
//        cvv	String	C
//        action	String	M	Regist:‘VIRREG ‘  forgetpwd:‘VIRPWDSET
        mBodyParams.put(RequestParams.WALLETID, walletId);
        mBodyParams.put(RequestParams.PAY_PASSCODE, mPin);
        mBodyParams.put(RequestParams.EXPRIYDATE, virtualCardListBean.getExpDate());
        mBodyParams.put(RequestParams.PAN_ID, virtualCardListBean.getPan());
        mBodyParams.put(RequestParams.FACEID, virtualCardListBean.getCrdArtId());
        mBodyParams.put(RequestParams.CVV, virtualCardListBean.getCvv());
        mBodyParams.put(RequestParams.ACTION, "VIRREG");
    }

    @Override
    public Void execute() {
        if (AppTestManager.getInstance().isE2ee()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    mPin = E2eeUtils.getLoginOrSetPin(mSid, payPwd, pubKey, random);
                    if (TextUtils.isEmpty(mPin)) {
                        mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                    } else {
                        VirtualcardRegisterSetPwdProtocol.super.execute();
                    }
                }
            }).execute();
        } else {
            super.execute();
        }
        return null;
    }
}
