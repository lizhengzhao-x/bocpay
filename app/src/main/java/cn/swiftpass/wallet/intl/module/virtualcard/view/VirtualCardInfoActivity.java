package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.CardInfoActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.VirtualCardInfoContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardVeryOtpEntiy;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.VirtualCardInfoPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by ZhangXinchao on 2019/11/13.
 * 虚拟卡绑定 展示卡信息界面
 */
public class VirtualCardInfoActivity extends BaseCompatActivity<VirtualCardInfoContract.Presenter> implements VirtualCardInfoContract.View {
    @BindView(R.id.id_virtualcard_cvv_content)
    TextView idVirtualcardCvvContent;
    @BindView(R.id.id_virtualcard_credit_limit)
    TextView idVirtualcardCreditLimit;
    @BindView(R.id.id_virtualcard_month_limit)
    TextView idVirtualcardMonthLimit;
    @BindView(R.id.id_virtualcard_face_view)
    LinearLayout idVirtualcardFaceView;
    @BindView(R.id.id_virtualcard_credit_card)
    TextView idVirtualcardCreditCard;
    @BindView(R.id.id_virtualcard_validity)
    TextView idVirtualcardValidity;
    @BindView(R.id.card_info_number)
    TextView idVirtualcardNumber;
    @BindView(R.id.id_cardholder_name)
    TextView idVirtualcardCardHolder;
    @BindView(R.id.tv_expiry)
    TextView idVirtualcardExpiry;
    @BindView(R.id.tv_CVV)
    TextView idVirtualcardCvv;
    @BindView(R.id.id_flip_img)
    View mFlipImg;
    @BindView(R.id.id_virtualcard_back_view)
    LinearLayout idVirtualcardBackView;
    @BindView(R.id.id_top_card_view)
    FrameLayout mTopCardView;
    @BindView(R.id.id_flip)
    View mId_flip;
    @BindView(R.id.id_spending_limit)
    View mSpendingImg;

    @BindView(R.id.id_copy)
    ImageView mCopyIV;


    @BindView(R.id.id_linear_view)
    View mFlCardFront;
    @BindView(R.id.id_back_view)
    View mFlCardBack;
    private AnimatorSet mRightOutSet, mLeftInSet;
    private boolean mIsShowBack;
    private VirtualCardVeryOtpEntiy virtualCardInfoEntiy;

    private static final int CLOSE_PAGE = 10000;
    /**
     * 9分钟倒计时 退出当前界面
     */
    private static final long TIME_CLOSE_PAGE = 60 * 1000 * 9;
    private BankCardEntity bankCardEntity;
    private List<BankCardEntity> cardEntities;
    private String mCarId;

    @Override
    public void init() {

        setToolBarTitle(R.string.AC2101_15_1);

        if (getIntent() != null) {
            virtualCardInfoEntiy = (VirtualCardVeryOtpEntiy) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDLISTBEAN);
            if (virtualCardInfoEntiy == null) {
                finish();
            }
            mCarId = getIntent().getStringExtra(Constants.VIRTUALCARDID);
        }
        updateText();
        reSizeLayout();
        RoundedCorners roundedCorners = new RoundedCorners(10);
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContext).load(ApiConstant.CARD_ART_URL + virtualCardInfoEntiy.getCrdArtId() + ".png").diskCacheStrategy(DiskCacheStrategy.RESOURCE).apply(options).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                mFlCardFront.setBackground(resource);
            }
        });

        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + "image/cardRegFace.png").apply(options).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                mFlCardBack.setBackground(resource);
            }
        });
        mSpendingImg.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                ActivitySkipUtil.startAnotherActivity(VirtualCardInfoActivity.this, VirtualCardOnlineSpendingLimitActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        ImageView faqIv = addToolBarRightViewToImage(R.mipmap.icon_navibar_morewhite);
        faqIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardEntities = CacheManagerInstance.getInstance().getCardEntities();
                if (cardEntities == null) {
                    mPresenter.getCardList();
                } else {
                    for (BankCardEntity bankCard : cardEntities) {
                        if (bankCard.getCardId().equals(mCarId)) {
                            bankCardEntity = bankCard;
                        }
                    }
                    if (bankCardEntity != null) {
                        checkCardMoreInfo();
                    }
                }

            }
        });
        initAnimator();
        mId_flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });
        mFlipImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == CLOSE_PAGE) {
                    //菜单打开9分钟 关闭当前页面 返回到首页
                    EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_CLOSE_VIRTUALCADR_PAGE, ""));
                    finish();
                }
            }
        };
        mHandler.sendEmptyMessageDelayed(CLOSE_PAGE, TIME_CLOSE_PAGE);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        //绑卡成功之后页面销毁
        if (event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            finish();
        }
    }

    /**
     * 卡片详情页
     */
    private void checkCardMoreInfo() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CARD_ENTITY, bankCardEntity);
        if (bankCardEntity.isDefaultCard()) {
            //删除的是默认卡 就默认下一张为新的默认卡
            if (CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity() != null) {
                mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity().getCardId());
            }
        } else {
            //删除的不是默认卡 要找一个剩下的默认卡id
            BankCardEntity entity = CacheManagerInstance.getInstance().getDefaultCardEntitity();
            String cardId = "";
            if (null != entity) {
                cardId = entity.getCardId();
            }
            mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, cardId);
        }
        ActivitySkipUtil.startAnotherActivity(VirtualCardInfoActivity.this, CardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void updateText() {
        idVirtualcardCvvContent.setText(virtualCardInfoEntiy.getCvv());
        idVirtualcardCvv.setText(virtualCardInfoEntiy.getCvv());
        try {
            idVirtualcardCreditLimit.setText("HKD " + AndroidUtils.formatPriceWithPoint(Double.valueOf(virtualCardInfoEntiy.getCrLmt()), true));
            idVirtualcardMonthLimit.setText("HKD " + AndroidUtils.formatPriceWithPoint(Double.valueOf(virtualCardInfoEntiy.getOlSpendLmt()), true));
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        idVirtualcardCreditCard.setText(AndroidUtils.formatCardNumberStrShort(virtualCardInfoEntiy.getPan()));
          //log提到隐藏卡号
//        idVirtualcardNumber.setText(AndroidUtils.formatCardMastNumberStr(virtualCardInfoEntiy.getPan()));
//        idVirtualcardNumber.setVisibility(View.VISIBLE);
        idVirtualcardValidity.setText(formatCardExpireDate(virtualCardInfoEntiy.getExpDate()));
        idVirtualcardExpiry.setText(formatCardExpireDate(virtualCardInfoEntiy.getExpDate()));

        mCopyIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyContentToClipboard(virtualCardInfoEntiy.getPan(), VirtualCardInfoActivity.this);
                Toast.makeText(mContext, getString(R.string.VN2101_1_1), LENGTH_LONG).show();
            }
        });
    }

    public void copyContentToClipboard(String content, Context context) {
        //获取剪贴板管理器：
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        // 创建普通字符型ClipData
        ClipData mClipData = ClipData.newPlainText("Label", content);
        // 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
    }


    private void reSizeLayout() {
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mFlCardFront.getLayoutParams();
        int width = AndroidUtils.getScreenWidth(this) - AndroidUtils.dip2px(this, 18) * 2;
        int height = (int) (width / 1.6);
        lp.width = width;
        lp.height = height;
        mFlCardFront.setLayoutParams(lp);

        FrameLayout.LayoutParams lpBack = (FrameLayout.LayoutParams) mFlCardBack.getLayoutParams();
        lpBack.width = width;
        lpBack.height = height;
        mFlCardBack.setLayoutParams(lpBack);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
        EventBus.getDefault().register(this);
    }

    private String formatCardExpireDate(String cardExpireDate) {
        if (TextUtils.isEmpty(cardExpireDate)) {
            return "";
        } else {
            if (cardExpireDate.length() == 4) {
                return cardExpireDate.substring(0, 2) + "/" + cardExpireDate.substring(2, cardExpireDate.length());
            } else {
                return cardExpireDate;
            }
        }
    }

    private void initAnimator() {
        // 设置动画
        setAnimators();
        // 设置镜头距离
        setCameraDistance();
    }


    /**
     * 改变视角距离, 贴近屏幕
     */
    private void setCameraDistance() {
        int distance = 16000;
        float scale = getResources().getDisplayMetrics().density * distance;
        mFlCardFront.setCameraDistance(scale);
        mFlCardBack.setCameraDistance(scale);
    }


    /**
     * 动画初始化
     */
    private void setAnimators() {

        mLeftInSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(null, "rotationY", -180f, 0f);
        animator.setDuration(500);
        animator.setStartDelay(0);
        ObjectAnimator animator2 = ObjectAnimator.ofFloat(null, "alpha", 0.0f, 1f);
        animator2.setStartDelay(250);
        animator2.setDuration(0);
        mLeftInSet.playTogether(animator, animator2);
        mLeftInSet.addListener(animatorListenerAdapter);

        mRightOutSet = new AnimatorSet();
        ObjectAnimator animator_ = ObjectAnimator.ofFloat(null, "rotationY", 0, 180f);
        animator_.setDuration(500);
        animator_.setStartDelay(0);
        ObjectAnimator animator2_ = ObjectAnimator.ofFloat(null, "alpha", 1f, 0f);
        animator2_.setStartDelay(250);
        animator2_.setDuration(0);
        mRightOutSet.playTogether(animator_, animator2_);
        mRightOutSet.addListener(animatorListenerAdapter);
    }

    AnimatorListenerAdapter animatorListenerAdapter = new AnimatorListenerAdapter() {
        @Override
        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            //在动画执行过程中，不许允许接收点击事件
            mId_flip.setClickable(false);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            //在动画执行过程中，不许允许接收点击事件
            mId_flip.setClickable(true);
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);

    }

    /**
     * 点击布局 翻转动画效果
     */
    public void flipCard() {
        // 正面朝上
        if (!mIsShowBack) {
            mRightOutSet.setTarget(mFlCardFront);
            mLeftInSet.setTarget(mFlCardBack);
            mIsShowBack = true;
            idVirtualcardFaceView.setVisibility(View.VISIBLE);
            idVirtualcardBackView.setVisibility(View.GONE);
        } else { // 背面朝上
            idVirtualcardFaceView.setVisibility(View.GONE);
            idVirtualcardBackView.setVisibility(View.VISIBLE);
            mRightOutSet.setTarget(mFlCardBack);
            mLeftInSet.setTarget(mFlCardFront);
            mIsShowBack = false;
        }
        mRightOutSet.start();
        mLeftInSet.start();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_info;
    }

    @Override
    protected VirtualCardInfoContract.Presenter createPresenter() {
        return new VirtualCardInfoPresenter();
    }

    @Override
    public void getCardListSuccess(CardsEntity response) {
        if (response != null) {
            ArrayList<BankCardEntity> rows = response.getRows();
            if (rows != null && rows.size() > 0) {
                CacheManagerInstance.getInstance().setCardEntities(rows);
                cardEntities = rows;
                for (BankCardEntity bankCard : cardEntities) {
                    if (bankCard.getCardId().equals(mCarId)) {
                        bankCardEntity = bankCard;
                    }
                }
                if (bankCardEntity != null) {
                    checkCardMoreInfo();
                }
            }
        }
    }

    @Override
    public void getCardListError(String errorCode, String errorMsg) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
    }
}
