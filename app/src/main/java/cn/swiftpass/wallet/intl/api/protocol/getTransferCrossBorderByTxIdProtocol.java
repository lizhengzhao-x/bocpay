package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class getTransferCrossBorderByTxIdProtocol extends BaseProtocol {

    public static final String TAG = getTransferCrossBorderByTxIdProtocol.class.getSimpleName();

    String txId;

    public getTransferCrossBorderByTxIdProtocol(String txId, NetWorkCallbackListener dataCallback) {
        this.txId = txId;

        mUrl = "api/crossTransfer/queryCrossTransferByTxnId";
        this.mDataCallback = dataCallback;
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txId);
    }


}
