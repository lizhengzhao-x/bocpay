package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 查询用户所有的信用卡列表
 */
public class ActionCreditListProtocol extends BaseProtocol {


    public ActionCreditListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actionCreditList";
    }


}
