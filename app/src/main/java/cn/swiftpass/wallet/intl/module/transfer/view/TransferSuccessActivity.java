package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;
import cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.ShareImageUtils;
import cn.swiftpass.wallet.intl.module.ecoupon.view.CheckEcouponsDetailPop;
import cn.swiftpass.wallet.intl.module.transfer.StaticCodeUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.widget.GlideRoundTransform;

import static cn.swiftpass.wallet.intl.entity.Constants.FILENAME;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferConfirmMoneyActivity.TRANSFER_APPCALLAPP_PAYMENT;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEFROM;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEMONEY;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGE_COLLECTION;

/**
 * 转账成功页面
 */
public class TransferSuccessActivity extends BaseCompatActivity {

    public static final String TRANSFER_RESULT = "TRANSFER RESULT";

    @BindView(R.id.tv_pay_fee_type)
    TextView tvPayFeeType;
    @BindView(R.id.tv_pay_amount)
    TextView tvPayAmount;
    @BindView(R.id.tv_payee_account)
    TextView tvPayeeAccount;
    @BindView(R.id.tv_result_suc)
    TextView mResultSucTV;
    @BindView(R.id.transfer_save_detail)
    TextView mTransfer_save_detail;
    @BindView(R.id.transfer_save_back)
    TextView mTransfer_save_back;
    @BindView(R.id.tv_pay_collection)
    TextView mCollectonAccount;
    @BindView(R.id.tv_transfer_bank)
    TextView tv_transfer_bank;
    @BindView(R.id.tv_transfer_success_bank_name)
    TextView tvTransferSuccessBankName;
    @BindView(R.id.lly_transfer_success_bank_name)
    LinearLayout llyTransferSuccessBankName;
    private TransferConfirmReq mTransferResult;
    @BindView(R.id.tv_pay_Postscript)
    TextView tv_pay_Postscript;
    @BindView(R.id.id_buttom_view)
    View idButtomView;
    @BindView(R.id.id_jifen_send_message)
    ImageView id_jifen_send_message;
    @BindView(R.id.id_jifen_send_message_view)
    View id_jifen_send_message_view;
    @BindView(R.id.tv_pay_bill)
    TextView tvPayBill;
    @BindView(R.id.id_lin_Postscript)
    LinearLayout id_lin_Postscript;
    @BindView(R.id.id_linear_bank)
    LinearLayout id_linear_bank;
    @BindView(R.id.id_linear_bill)
    LinearLayout idLinearBill;
    @BindView(R.id.id_linear_collection)
    LinearLayout id_linear_collection;
    @BindView(R.id.id_status_image)
    ImageView id_status_image;
    @BindView(R.id.id_linear_lishi)
    View lishiView;
    @BindView(R.id.id_right_bac)
    ImageView lishiImage;
    @BindView(R.id.id_bannber_detail)
    ImageView idBannerDetail;
    @BindView(R.id.id_left_commont_label)
    TextView idLeftCommontLabel;

    @BindView(R.id.id_transfer_method)
    TextView idTransferMethod;
    @BindView(R.id.tv_transfer_account)
    TextView tvTransferAccount;
    @BindView(R.id.tv_result_red_tips)
    TextView tvResultRedTips;
    @BindView(R.id.iv_transfer_success_wave)
    ImageView ivTransferSuccesswave;
    @BindView(R.id.tv_transfer_fps_title)
    TextView tvTransferFpsTitle;


    /**
     * tvPayDetail 缴费详情
     * tvFromAccount 付款账户
     * tvTransferReference 参考编号
     */
    @BindView(R.id.tv_pay_detail)
    TextView tvPayDetail;
    @BindView(R.id.tv_from_account_success)
    TextView tvFromAccount;
    @BindView(R.id.tv_transfer_reference)
    TextView tvTransferReference;

    @BindView(R.id.tv_staticCode)
    TextView tvStaticCode;

    private String transferType;
    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;

    private int currentPaymentType;


    private int callBack = 0;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        hideBackIcon();
        String transType = "";
        if (null != getIntent()) {
            mTransferResult = (TransferConfirmReq) getIntent().getSerializableExtra(TransferConst.TRANS_FPS_CHECK_DATA);
            transType = getIntent().getStringExtra(TransferConst.TRANS_COLLECTION_TYPE);
            currentPaymentType = getIntent().getExtras().getInt(Constants.TYPE);
        }
        if (null != mTransferResult) {
            if (mTransferResult.isLiShi) {
                idButtomView.setVisibility(View.VISIBLE);
            }
            String currency = "HKD";
            if (TextUtils.isEmpty(mTransferResult.currency)) {
                String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mTransferResult.transferAmount), true);
                tvPayAmount.setText(balanceStr);
            } else {
                String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mTransferResult.transferAmount), true);
                tvPayAmount.setText(balanceStr);
                currency = mTransferResult.currency;
            }
            tvPayFeeType.setText(currency);
            tvPayeeAccount.setText(mTransferResult.tansferToAccount);

            //bill no
            if (TextUtils.isEmpty(mTransferResult.billReference)) {
                idLinearBill.setVisibility(View.GONE);
            } else {
                idLinearBill.setVisibility(View.VISIBLE);
                tvPayBill.setText(mTransferResult.billReference);
            }

            //附言
            if (TextUtils.isEmpty(mTransferResult.postscript)) {
                id_lin_Postscript.setVisibility(View.GONE);
            } else {
                tv_pay_Postscript.setText(mTransferResult.postscript);
            }


            //FPS转账方法 要显示收款银行
            if (!mTransferResult.isBocPayTransfer) {
                if (TextUtils.isEmpty(mTransferResult.transferBank)) {
                    llyTransferSuccessBankName.setVisibility(View.GONE);
                } else {
                    llyTransferSuccessBankName.setVisibility(View.VISIBLE);
                    tvTransferSuccessBankName.setText(mTransferResult.transferBank);
                    tvTransferFpsTitle.setText(R.string.SRP2101_1_6);
                }
            } else {
                llyTransferSuccessBankName.setVisibility(View.GONE);
            }


            //转账描述标题 根据转账类型显示不同
            transferType = mTransferResult.transferType;
            LogUtils.i(TRANSFER_RESULT, "transferType: " + transferType);
            if (!TextUtils.isEmpty(transferType)) {
                //不同转账类型 转账方式要显示不同的词条
                if (transferType.equals(TransferConst.TRANS_FPS)) {
                    idTransferMethod.setText(getString(R.string.TR1_7_11));
                } else if (transferType.equals(TransferConst.TRANS_TYPE_PHONE)) {
                    idTransferMethod.setText(getString(R.string.STF2101_1_14));
                } else if (transferType.equals(TransferConst.TRANS_TYPE_EMAIL)) {
                    idTransferMethod.setText(getString(R.string.SRP2101_1_9));
                } else if (transferType.equals(TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
                    idTransferMethod.setText(getString(R.string.TF2101_33_4));
                }

            }

            //动态改变标题 TRANS_TYPE_MERCHANT:商户
            if (TextUtils.equals(transType, TransferConst.TRANS_TYPE_MERCHANT)) {
                setToolBarTitle(R.string.FPSBP2101_1_1);
                mResultSucTV.setText(R.string.string_pay_success);
                //商户码转账 需要显示获取积分字段
                id_jifen_send_message_view.setVisibility(View.VISIBLE);

                mTransfer_save_detail.setText(R.string.TF2101_33_9);
                tvPayDetail.setText(R.string.FPSBP2101_4_3);
                tvFromAccount.setText(R.string.RP2101_13_7);
                tvTransferReference.setText(R.string.FPSBP2101_4_5);
                idLeftCommontLabel.setText(R.string.TF2101_4_9);
                mTransfer_save_back.setText(R.string.RP2101_16_12);
                tvStaticCode.setVisibility(View.VISIBLE);

            } else {
                tvStaticCode.setVisibility(View.GONE);
                if (TextUtils.equals(transferType, TransferConst.TRANS_FPS)) {
                    setToolBarTitle(R.string.TF2101_26_1);
                } else if (TextUtils.equals(transferType, TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
                    setToolBarTitle(R.string.TF2101_30_1);
                } else if (TextUtils.equals(transferType, TransferConst.TRANS_TYPE_PHONE)) {
                    setToolBarTitle(R.string.TF2101_5_1);
                } else {
                    setToolBarTitle(R.string.TF2101_5_1);
                }
                mResultSucTV.setText(R.string.transfer_success);

                mTransfer_save_detail.setText(R.string.TF2101_33_9);
                tvPayDetail.setText(R.string.TF2101_26_3);
                tvFromAccount.setText(R.string.RP2101_13_7);
                tvTransferReference.setText(R.string.FPSBP2101_4_5);
                mTransfer_save_back.setText(R.string.RP2101_16_12);
            }
            if (mTransferResult.isLiShi) {
                lishiView.setVisibility(View.VISIBLE);
                id_status_image.setImageResource(R.mipmap.icon_transfer_redpacket_successful);
                setToolBarTitle(R.string.RP2101_13_1);
                idLeftCommontLabel.setText(getResources().getString(R.string.RP2101_12_7));
                GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getBacImageUrl()).diskCacheStrategy(DiskCacheStrategy.NONE).into(lishiImage);
                if (mTransferResult.isBocPayTransfer) {
                    mResultSucTV.setText(getString(R.string.SRP2101_1_7));
                } else {
                    mResultSucTV.setText(getString(R.string.SRP2101_1_8));
                }
            } else {
                if ((transferType.equals(TransferConst.TRANS_TYPE_PHONE) ||
                        transferType.equals(TransferConst.TRANS_TYPE_EMAIL) ||
                        transferType.equals(TransferConst.TRANS_FPS) ||
                        transferType.equals(TransferConst.TRANS_CUSTOMER_ACCOUNT)) &&
                        !mTransferResult.isBocPayTransfer) {
                    mResultSucTV.setText(getString(R.string.P3_ewa_01_034));
                } else {
                    mResultSucTV.setText(getString(R.string.transfer_success));
                }
                ivTransferSuccesswave.setVisibility(View.VISIBLE);
            }

            if (TextUtils.isEmpty(mTransferResult.collectionAccount)) {
                id_linear_collection.setVisibility(View.GONE);
            } else {
                mCollectonAccount.setText(mTransferResult.collectionAccount);
            }

            if (mTransferResult.isLiShi) {
                tvPayDetail.setText(R.string.RP2101_16_4);
            }

            //显示红包的tips
            if (!TextUtils.isEmpty(mTransferResult.bocpayRedPackeNotifyTip)) {
                tvResultRedTips.setVisibility(View.VISIBLE);
                tvResultRedTips.setText(mTransferResult.bocpayRedPackeNotifyTip);
            }

            String str;
            //2为支付账户 3为智能账户
            if (TextUtils.equals(Constants.ACCOUNT_PAY_ACCOUNT, mTransferResult.getSmartAcLevel())) {
                str = getString(R.string.DB2101_1_2);
            } else {
                str = getString(R.string.DB2101_1_1);
            }

            tvTransferAccount.setText(str + "(" + mTransferResult.tansferAccount + ")");
        }
        id_jifen_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEcouponCountDetail(getString(R.string.EPTNC_AC_01));
            }
        });

        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            mTransfer_save_back.setText(getString(R.string.ACA2101_0_4));
        }
        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {
                showDetailBanners();
            }
        });
        //预加载图片
        preLoadSaveLocalImg();

    }

    /**
     * 保存到本地 分享的图片要提前缓存
     */
    private void preLoadSaveLocalImg() {
        //如果不是利是 或者mTransferResult.bckImgsBean对象为空 就缓存
        if (!mTransferResult.isLiShi) {
            return;
        }

        if (null == mTransferResult) {
            return;
        }

        if (null == mTransferResult.bckImgsBean) {
            return;
        }
        String bacImageUrl = AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getRedPackageNewImageTopUrl();
        String buttomImageUrl = AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getBottomBigSizeImageUrl();

        Glide.with(this).load(bacImageUrl).preload();
        Glide.with(this).load(buttomImageUrl).preload();
    }

    private void showDetailBanners() {
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
        if (initBannerEntity != null) {
            BannerImageEntity pTwoPBean = null;
            //付款类别 0：个人，1：商户
            if (mTransferResult != null && !TextUtils.isEmpty(mTransferResult.getPaymentCategory()) && mTransferResult.getPaymentCategory().equals("1")) {
                pTwoPBean = initBannerEntity.getPTwoM(this);
            } else {
                pTwoPBean = initBannerEntity.getPTwoP(this);
            }
            if (mTransferResult.isLiShi) {
                pTwoPBean = initBannerEntity.getRedPacket(this);
            }
            // 0 是不显示 1 利利是 2 邀请
            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                //idBannerDetail.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idBannerDetail.getLayoutParams();
                int width = AndroidUtils.getScreenWidth(this) - AndroidUtils.dip2px(this, 15) * 2;
                // 375 85
                int height = width * 85 / 375;
                lp.width = width;
                lp.height = height;
                idBannerDetail.setLayoutParams(lp);
                String imageUrl = pTwoPBean.getImagesUrl();
                if (imageUrl.endsWith(".gif")) {
                    GlideApp.with(this).asGif().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).load(imageUrl).transform(new GlideRoundTransform(mContext, 5)).into(new SimpleTarget<GifDrawable>() {
                        @Override
                        public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                            idBannerDetail.setVisibility(View.VISIBLE);
                            //gif单独处理
                            resource.setLoopCount(GifDrawable.LOOP_FOREVER);
                            resource.start();
                            idBannerDetail.setBackground(resource);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            idBannerDetail.setVisibility(View.GONE);
                        }
                    });
                } else {
                    GlideApp.with(this).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).transform(new GlideRoundTransform(mContext, 5)).into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            idBannerDetail.setVisibility(View.VISIBLE);
                            idBannerDetail.setImageDrawable(resource);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            idBannerDetail.setVisibility(View.GONE);
                        }
                    });
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        //状态栏文字颜色恢复
        AndroidUtils.setStatusBar(this, true);
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //用户分享图片成功之后 图片比例拉伸
        if (idBannerDetail != null && idBannerDetail.isShown()) {
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idBannerDetail.getLayoutParams();
            int width = AndroidUtils.getScreenWidth(this) - AndroidUtils.dip2px(this, 15) * 2;
            // 375 85
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            idBannerDetail.setLayoutParams(lp);
        }
    }

    private void showEcouponCountDetail(String titleDetail) {
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), "", titleDetail);
        checkEcouponsDetailPop.show();
    }

    /**
     * 具体采用哪种方式分享
     *
     * @param positon
     */
    private void shareImageWithPosition(final int positon) {
        PermissionInstance.getInstance().getStoreImagePermission(TransferSuccessActivity.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                shareImageToOther(positon);
            }

            @Override
            public void rejectPermission() {
                showLackOfPermissionDialog(TransferSuccessActivity.this);
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    private void shareImageToOther(int positon) {

        if (positon == 0) {
            //whatsapp
            shareGenerateImage(new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToWhatsApp(TransferSuccessActivity.this, "", uri);
                    if (!isSuccess) {
                        showErrorMsgDialog(mContext, getResources().getString(R.string.SH2_3_1));
                    }
                }
            });

        } else if (positon == 1) {
            //wechat
            shareGenerateImage(new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareWechatFriend(TransferSuccessActivity.this, uri);
                    if (!isSuccess) {
                        showErrorMsgDialog(mContext, getResources().getString(R.string.SH2_3_1));
                    }
                }
            });
        } else if (positon == 2) {
            //line
            shareGenerateImage(new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToLine(TransferSuccessActivity.this, uri);
                    if (!isSuccess) {
                        showErrorMsgDialog(mContext, getResources().getString(R.string.SH2_3_1));
                    }
                }
            });

        } else {
            //系统组件
            shareGenerateImage(new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    if (uri != null) {
                        AndroidUtils.shareImage(TransferSuccessActivity.this, "", uri);
                    }
                }
            });
        }
    }

    private void initShareView(final OnImageBacBitmapSuccess onImageBitmapSuccess) {
        View shareView;
        if (mTransferResult.isLiShi) {
            shareView = LayoutInflater.from(this).inflate(R.layout.item_lishi_share_view, null);
        } else {
            shareView = LayoutInflater.from(this).inflate(R.layout.item_share_view, null);
        }
        TextView mTv_pay_amount = (TextView) shareView.findViewById(R.id.tv_pay_amount);
        TextView mtTv_pay_amount_currency = (TextView) shareView.findViewById(R.id.tv_pay_amount_currency);
        final View mRootView = shareView.findViewById(R.id.id_root_view);
        ImageView backgroundIv = shareView.findViewById(R.id.iv_red_packet_bg);
        LinearLayout mId_lin_Postscript = (LinearLayout) shareView.findViewById(R.id.id_lin_Postscript);
        TextView mTv_transfer_comment = (TextView) shareView.findViewById(R.id.tv_transfer_comment);
        TextView mTv_transfer_payee = (TextView) shareView.findViewById(R.id.tv_transfer_payee);
        TextView mTv_transfer_method = (TextView) shareView.findViewById(R.id.tv_transfer_method);
        LinearLayout mId_linear_bank = (LinearLayout) shareView.findViewById(R.id.id_linear_bank);
        TextView mTv_transfer_bank = (TextView) shareView.findViewById(R.id.tv_transfer_bank);
        LinearLayout mId_linear_bill = (LinearLayout) shareView.findViewById(R.id.id_linear_bill);
        TextView mTv_transfer_no = (TextView) shareView.findViewById(R.id.tv_transfer_no);
        TextView mTv_transfer_drawee = (TextView) shareView.findViewById(R.id.tv_transfer_payer);
        TextView mTv_transfer_time = (TextView) shareView.findViewById(R.id.tv_transfer_time);
        TextView mTv_transfer_no_two = (TextView) shareView.findViewById(R.id.tv_transfer_no_two);
        TextView mTransferType = (TextView) shareView.findViewById(R.id.id_transfer_type);
        ImageView mId_head_image = shareView.findViewById(R.id.id_head_image);
        ImageView mId_buttom_image = shareView.findViewById(R.id.id_buttom_image);
        TextView leftCommontLabel = (TextView) shareView.findViewById(R.id.id_left_commont_label);
        final ImageView mId_header_shaper_img = shareView.findViewById(R.id.id_header_shaper_img);
        final TextView mTransfer_message = (TextView) shareView.findViewById(R.id.transfer_message);
        final ImageView mButtomBannerVivew = (ImageView) shareView.findViewById(R.id.id_buttom_banner_view);

        mRootView.setMinimumHeight(AndroidUtils.getScreenHeight(mContext));
        //2.底部分享文案 备注涉及到换行，一定要父布局设置宽度

        if (mTransferResult.isLiShi) {
            mRootView.setLayoutParams(new RelativeLayout.LayoutParams(AndroidUtils.getScreenWidth(mContext), ViewGroup.LayoutParams.MATCH_PARENT));
        } else {
            mRootView.setLayoutParams(new FrameLayout.LayoutParams(AndroidUtils.getScreenWidth(mContext), ViewGroup.LayoutParams.MATCH_PARENT));
        }

        //测试底部分享文案
//        mTransferResult.extCmt = "";
        //测试转账备注 换行
//        mTransferResult.postscript = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx2";

        String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mTransferResult.transferAmount), true);
        mTv_pay_amount.setText(balanceStr);
        mtTv_pay_amount_currency.setText(mTransferResult.currency);

        //附言
        if (TextUtils.isEmpty(mTransferResult.postscript)) {
            mId_lin_Postscript.setVisibility(View.GONE);
        } else {
            mTv_transfer_comment.setText(mTransferResult.postscript);
        }


        if (mTransferResult.isLiShi) {
            leftCommontLabel.setText(getResources().getString(R.string.RP2101_12_7));
        } else {
            mId_head_image.setVisibility(View.VISIBLE);
        }

        mTv_transfer_payee.setText(mTransferResult.tansferToAccount);
        mTv_transfer_method.setText(mTransferResult.collectionAccount);
        mTv_transfer_drawee.setText(mTransferResult.dbtrNm);
        mTv_transfer_time.setText(mTransferResult.transferDate);
        //bill no
        if (TextUtils.isEmpty(mTransferResult.billReference)) {
            mId_linear_bill.setVisibility(View.GONE);
        } else {
            mTv_transfer_no.setText(mTransferResult.billReference);
        }
        mTv_transfer_no_two.setText(mTransferResult.scrRefNo);
        if (TextUtils.isEmpty(mTransferResult.transferBank)) {
            mId_linear_bank.setVisibility(View.GONE);
        } else {
            mTv_transfer_bank.setText(mTransferResult.transferBank);
        }
        if (transferType.equals(TransferConst.TRANS_FPS)) {
            mTransferType.setText(getString(R.string.TR1_7_11));
        } else if (transferType.equals(TransferConst.TRANS_TYPE_PHONE)) {
            mTransferType.setText(getString(R.string.STF2101_1_14));
        } else if (transferType.equals(TransferConst.TRANS_TYPE_EMAIL)) {
            mTransferType.setText(getString(R.string.SRP2101_1_9));
        } else if (transferType.equals(TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
            mTransferType.setText(getString(R.string.TF2101_33_4));
        }

        GlideApp.with(mContext).load(ShareImageUtils.getShareImageUrl(mContext)).placeholder(R.mipmap.icon_enveloptop_bocpay_white).into(mId_head_image);
        if (mTransferResult.isLiShi) {
            String bacImageUrl = AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getRedPackageNewImageTopUrl();
            String buttomImageUrl = AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getBottomBigSizeImageUrl();
//            //从后台加载的图片要提前给宽高 否则加载不出来
            RelativeLayout.LayoutParams lpButtom = (RelativeLayout.LayoutParams) mId_buttom_image.getLayoutParams();
            int width = ScreenUtils.getDisplayWidth();
            int height = width * 111 / 318;
            lpButtom.width = width;
            lpButtom.height = height;
            mId_buttom_image.setLayoutParams(lpButtom);


            FrameLayout.LayoutParams mheaderShape = (FrameLayout.LayoutParams) mId_header_shaper_img.getLayoutParams();
            width = ScreenUtils.getDisplayWidth();
            height = width * 94 / 335;
            mheaderShape.width = width;
            mheaderShape.height = height;
            mId_header_shaper_img.setLayoutParams(mheaderShape);

            //图片加载渲染需要时间
            GlideApp.with(mContext).load(bacImageUrl).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    mId_header_shaper_img.setImageDrawable(resource);
                }
            });
            GlideApp.with(mContext).load(buttomImageUrl).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    mId_buttom_image.setImageDrawable(resource);
                }
            });
        }

        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int height;
                    InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
                    if (initBannerEntity != null) {
                        BannerImageEntity pTwoPBean = null;
                        //派利是保存到本地跟转账保存到本地图片不一样
                        if (mTransferResult.isLiShi) {
                            pTwoPBean = initBannerEntity.getRedEnvelopes(mContext);
                        } else {
                            pTwoPBean = initBannerEntity.getLocalPage(mContext);
                        }
                        // 0 是不显示 1 利利是 2 邀请
                        if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                            mButtomBannerVivew.setVisibility(View.VISIBLE);
                            //底部banner
                            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idBannerDetail.getLayoutParams();
                            int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(mContext, 20) * 2;
                            height = width * 85 / 375;
                            lp.width = width;
                            lp.height = height;
                            lp.topMargin = AndroidUtils.dip2px(mContext, 20);
                            lp.leftMargin = AndroidUtils.dip2px(mContext, 20);
                            lp.rightMargin = AndroidUtils.dip2px(mContext, 20);
                            mButtomBannerVivew.setLayoutParams(lp);
                            String imgUrl = pTwoPBean.getImagesUrl();
                            GlideApp.with(TransferSuccessActivity.this).load(imgUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).transform(new GlideRoundTransform(mContext, 5)).into(new SimpleTarget<Drawable>() {
                                @Override
                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                    mButtomBannerVivew.setImageDrawable(resource);
                                    //banner 图片加载成功 底部词条隐藏
                                    mTransfer_message.setVisibility(View.GONE);
                                    getBacImgUrl(shareView, backgroundIv, onImageBitmapSuccess);
                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    super.onLoadFailed(errorDrawable);
                                    mTransfer_message.setText(mTransferResult.getExtCmt());
                                    mButtomBannerVivew.setVisibility(View.GONE);
                                    getBacImgUrl(shareView, backgroundIv, onImageBitmapSuccess);
                                }
                            });
                        } else {
                            mTransfer_message.setVisibility(View.VISIBLE);
                            mTransfer_message.setText(mTransferResult.getExtCmt());
                            getBacImgUrl(shareView, backgroundIv, onImageBitmapSuccess);
                        }
                    } else {
                        mTransfer_message.setVisibility(View.VISIBLE);
                        mTransfer_message.setText(mTransferResult.getExtCmt());
                        getBacImgUrl(shareView, backgroundIv, onImageBitmapSuccess);
                    }
                }
            }, 500);
        }

    }

    /**
     * 在设置背景图 一定要给父布局 因为子布局高度不知道还未测量
     *
     * @param shareView
     * @param backgroundIv
     * @param onImageBitmapSuccess
     */
    private void getBacImgUrl(final View shareView, ImageView backgroundIv, final OnImageBacBitmapSuccess onImageBitmapSuccess) {
        if (mTransferResult.bckImgsBean != null) {
            //需要手动设置控件的宽度，不然控件 保存成图片 宽度会变小
            //e利是 要单独处理 背景图片颜色不同
            String bacImageUrl = AppTestManager.getInstance().getBaseUrl() + mTransferResult.bckImgsBean.getNewRedPackageImageUrl();
            GlideApp.with(mContext)
                    .load(bacImageUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            backgroundIv.setImageDrawable(resource);
                            imgCallBack(onImageBitmapSuccess, shareView);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            backgroundIv.setBackgroundColor(Color.WHITE);
                            imgCallBack(onImageBitmapSuccess, shareView);
                        }
                    });

        } else {
            onImageBitmapSuccess.onImageBacBitmapCallBack(shareView);
        }
    }

    private void imgCallBack(OnImageBacBitmapSuccess onImageBitmapSuccess, View mRootView) {
        onImageBitmapSuccess.onImageBacBitmapCallBack(mRootView);
    }


    private void saveImageToGalley(boolean saveToLocal) {
        initShareView(new OnImageBacBitmapSuccess() {
            @Override
            public void onImageBacBitmapCallBack(final View shareView) {
                final ImageView mId_head_image = shareView.findViewById(R.id.id_head_image);
                final View mRootView = shareView.findViewById(R.id.id_root_view);
                LogUtils.i(TAG, "WIDTH:" + shareView.getHeight() + " height:" + mRootView.getHeight());
                if (mTransferResult.isLiShi) {
                    mId_head_image.setImageResource(R.mipmap.icon_enveloptop_bocpay_white);
                    shareView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                    shareView.layout(0, 0, shareView.getMeasuredWidth(), shareView.getMeasuredHeight() < AndroidUtils.getScreenHeight(mContext) ? AndroidUtils.getScreenHeight(mContext) : shareView.getMeasuredHeight());
                    //如果派利是背景图没有拉取成功  需要设置一个背景色 否则整体透明效果会有问题
                    shareView.setDrawingCacheEnabled(true);
                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                    Bitmap cachebmp = null;
                    shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                    int totalHeight = getBitMapHeight(shareView);
                    cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), totalHeight, Color.WHITE);
                    shareView.destroyDrawingCache();
                    AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cachebmp, "Transfer_" + AndroidUtils.localImageStr(System.currentTimeMillis()));
                    Toast.makeText(mContext, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
                    dismissDialog();
                } else {
                    GlideApp.with(TransferSuccessActivity.this).
                            load(ShareImageUtils.getShareImageUrl(TransferSuccessActivity.this)).
                            override(80, 80)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .into(new SimpleTarget<Drawable>() {
                                @Override
                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                    mId_head_image.setImageDrawable(resource);
                                    shareView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                                    shareView.layout(0, 0, shareView.getMeasuredWidth(), shareView.getMeasuredHeight());
                                    shareView.setDrawingCacheEnabled(true);
                                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext),
                                            getBitMapHeight(shareView), Color.WHITE, Bitmap.Config.ARGB_8888);
                                    AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cachebmp, "Transfer_" + AndroidUtils.localImageStr(System.currentTimeMillis()));
                                    shareView.destroyDrawingCache();
                                    Toast.makeText(mContext, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
                                    dismissDialog();
                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    super.onLoadFailed(errorDrawable);
                                    shareView.setDrawingCacheEnabled(true);
                                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                                    Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext),
                                            getBitMapHeight(shareView), Color.WHITE, Bitmap.Config.ARGB_8888);
                                    AndroidUtils.saveImageToGalleryByScopedStorage(mContext, cachebmp, "Transfer_" + AndroidUtils.localImageStr(System.currentTimeMillis()));
                                    shareView.destroyDrawingCache();
                                    Toast.makeText(mContext, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
                                    dismissDialog();
                                }
                            });
                }

            }
        });
    }

    private int getBitMapHeight(View shareView) {
        return AndroidUtils.getScreenHeight(mContext) > shareView.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareView.getMeasuredHeight();
    }

    /**
     * 分享图片 生成分享路径
     */
    private void shareGenerateImage(final OnImageBitmapSuccess onImageBitmapSuccess) {
        showDialogNotCancel();
        initShareView(new OnImageBacBitmapSuccess() {
            @Override
            public void onImageBacBitmapCallBack(final View shareView) {
                final ImageView mId_head_image = shareView.findViewById(R.id.id_head_image);
                AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), AndroidUtils.getScreenHeight(mContext));
                if (mTransferResult.isLiShi) {
                    mId_head_image.setImageResource(R.mipmap.icon_enveloptop_bocpay_white);
                    shareView.setDrawingCacheEnabled(true);
                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                    shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                    // 把一个View转换成图片
                    shareView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                    shareView.layout(0, 0, shareView.getMeasuredWidth(), shareView.getMeasuredHeight());

                    Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), getBitMapHeight(shareView), Color.WHITE);
                    shareView.destroyDrawingCache();
                    Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, FILENAME);
                    onImageBitmapSuccess.onImageBitmapCallBack(uri);
                    dismissDialog();
                } else {
                    GlideApp.with(TransferSuccessActivity.this).
                            load(ShareImageUtils.getShareImageUrl(TransferSuccessActivity.this)).
                            override(80, 80).
                            into(new SimpleTarget<Drawable>() {
                                @Override
                                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                    mId_head_image.setImageDrawable(resource);
                                    shareView.setDrawingCacheEnabled(true);
                                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                                    shareView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                                    shareView.layout(0, 0, shareView.getMeasuredWidth(), shareView.getMeasuredHeight());
                                    // 把一个View转换成图片
                                    Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), getBitMapHeight(shareView), Color.WHITE);
                                    shareView.destroyDrawingCache();
                                    Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, FILENAME);
                                    onImageBitmapSuccess.onImageBitmapCallBack(uri);
                                    dismissDialog();
                                }

                                @Override
                                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                    super.onLoadFailed(errorDrawable);
                                    shareView.setDrawingCacheEnabled(true);
                                    shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                                    // 把一个View转换成图片
                                    Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), getBitMapHeight(shareView), Color.WHITE);
                                    shareView.destroyDrawingCache();
                                    Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp, FILENAME);
                                    onImageBitmapSuccess.onImageBitmapCallBack(uri);
                                    dismissDialog();
                                }
                            });
                }

            }
        });
    }


    interface OnImageBitmapSuccess {

        void onImageBitmapCallBack(Uri uri);
    }

    interface OnImageBacBitmapSuccess {

        void onImageBacBitmapCallBack(View rootView);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_success;
    }

    @OnClick({R.id.transfer_save_back, R.id.transfer_save_detail, R.id.tv_transfer_success_share, R.id.tv_staticCode})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 2000)) return;
        switch (view.getId()) {
            case R.id.transfer_save_back:
                //转账成功
                if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
//                    ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
                    AppCallAppUtils.goToMerchantAppForFps(this, true);
//                    EventBus.getDefault().postSticky(new AppCallAppEventItem(AppCallAppEventItem.EVENT_APP_CALLAPP_SUCCESS, ""));
                } else {
                    EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_TRANSFER_SUCCESS, ""));
                    MyActivityManager.removeAllTaskExcludeMainStack();
                }
                break;
            case R.id.transfer_save_detail:
                showDialogNotCancel();
                PermissionInstance.getInstance().getStoreImagePermission(TransferSuccessActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        saveImageToGalley(true);
                    }

                    @Override
                    public void rejectPermission() {
                        dismissDialog();
                        showLackOfPermissionDialog(TransferSuccessActivity.this);
                    }
                }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                break;
            case R.id.tv_transfer_success_share:
                // 采用系统分享
                shareImageWithPosition(3);
                break;
            case R.id.tv_staticCode:
                checkBindSmartAccount();
                break;
            default:
                break;
        }

    }

    private void checkBindSmartAccount() {
        StaticCodeUtils.checkBindCard(this, new StaticCodeUtils.BindSmartAccountCallBack() {
            @Override
            public void onBindSmartAccountSuccess() {
                //先通知关闭其他转账流程的页面
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(PAGEFROM, PAGE_COLLECTION);
                mHashMaps.put(PAGEMONEY, mTransferResult.transferAmount);
                ActivitySkipUtil.startAnotherActivity(TransferSuccessActivity.this, TransferSetLimitActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    /**
     * 点击back不可返回
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

}
