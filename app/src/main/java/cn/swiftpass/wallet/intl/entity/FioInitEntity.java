package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/5/22.
 * FIO 初始化返回
 */

public class FioInitEntity extends BaseEntity {
    //the application system key of the end-user account
    String asKey;

    //the fido id belongs to the end-user account
    String fioId;
    //EWA
    String channel;

    public String getAsKey() {
        return asKey;
    }

    public void setAsKey(String asKey) {
        this.asKey = asKey;
    }

    public String getFioId() {
        return fioId;
    }

    public void setFioId(String fioId) {
        this.fioId = fioId;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }
}
