package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.TransferChangeCusNameProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferConfirmByBocProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferConfirmProtocol;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferConfirmContract;

/**
 * 转账确认页面
 */
public class TransferConfirmPresenter extends BasePresenter<TransferConfirmContract.View> implements TransferConfirmContract.Presenter {

    public  static final String ACTION_PAY = "EXTPAY";
    @Override
    public void transferChangeUserName(String transferOrderId, String crCustNameRemark, String typeIn) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferChangeCusNameProtocol(transferOrderId, crCustNameRemark, typeIn, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferChangeUserNameError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferChangeUserNameSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void confirmTransfer(TransferConfirmReq req,String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferConfirmProtocol(req, action,new NetWorkCallbackListener<TransferCompleteEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmTransferError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(TransferCompleteEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmTransferSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void transferConfirmBoc(String transferOrderId, boolean isLishi,String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferConfirmByBocProtocol(transferOrderId, isLishi,action, new NetWorkCallbackListener<TransferCompleteEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferConfirmBocError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(TransferCompleteEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferConfirmBocSuccess(response);
                }
            }
        }).execute();
    }
}
