package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 *  拆利是首页接口
 */
public class ReceivedRedPacketHomePageProtocol extends BaseProtocol {


    public ReceivedRedPacketHomePageProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/redpacket/receivedRedPacketHomePage";
    }

}
