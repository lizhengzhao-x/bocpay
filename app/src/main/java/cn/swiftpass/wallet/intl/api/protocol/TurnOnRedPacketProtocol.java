package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 *  拆利是接口
 */
public class TurnOnRedPacketProtocol extends BaseProtocol {
    private String srcRefNo;

    public TurnOnRedPacketProtocol(String srcRefNo,NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.srcRefNo = srcRefNo;
        mUrl = "api/redpacket/turnOnRedPacket";
    }

    @Override
    public void packData() {
        super.packData();
        if(!TextUtils.isEmpty(srcRefNo)){
            mBodyParams.put(RequestParams.SRCREFNO, srcRefNo);
        }
    }
}
