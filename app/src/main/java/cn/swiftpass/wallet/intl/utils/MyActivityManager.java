package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.view.TotalGradeActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.register.CheckIdvFailedActivity;
import cn.swiftpass.wallet.intl.module.register.CheckIdvProgressActivity;
import cn.swiftpass.wallet.intl.module.register.IdAuthInfoFillActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterConfirmPaymentPswActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterPaymentAccountDescActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSelActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSetPaymentPswActivity;
import cn.swiftpass.wallet.intl.module.register.STwoRegisterByEmailAndPhoneActiviy;
import cn.swiftpass.wallet.intl.module.setting.SettingActivity;
import cn.swiftpass.wallet.intl.module.setting.language.LanguageSettingActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ForgetPaymentPasswordActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.PaymentSettingActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.STwoSelForgetPasswordTypeActivity;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;

/**
 * Created by ramon on 2018/8/30.
 * 流程控制 根据不同流程 关闭不同界面
 */

public class MyActivityManager {
    private static MyActivityManager sInstance = new MyActivityManager();

    private WeakReference<BaseAbstractActivity> sCurrentActivityWeakRef;


    private MyActivityManager() {

    }

    public static MyActivityManager getInstance() {
        return sInstance;
    }

    /**
     * app是否在前台运行
     *
     * @param mContext
     * @return
     */
    public static boolean isAppOnForeground(Context mContext) {
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        String packageName = mContext.getPackageName();
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager
                .getRunningAppProcesses();
        if (appProcesses == null)
            return false;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(packageName)
                    && appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                return true;
            }
        }
        return false;
    }


    /**
     * 当前流程中是否包含app call app页面的流程
     *
     * @return
     */
    public static boolean containAppCallAppPage() {
        boolean isContainPage = false;
        List<Activity> taskStack = ProjectApp.getTempTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && activity instanceof AppCallAppPaymentActivity) {
                    isContainPage = true;
                    break;
                }
            }
        }
        return isContainPage;
    }

    public static boolean containPreloginPage() {
        boolean isContainPage = false;
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && activity instanceof PreLoginActivity) {
                    isContainPage = true;
                    break;
                }
            }
        }
        return isContainPage;
    }

    public static boolean containMainHomePage() {
        boolean isContainPage = false;
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && activity instanceof MainHomeActivity) {
                    isContainPage = true;
                    break;
                }
            }
        }
        return isContainPage;
    }


    /**
     * app 是否在运行中
     *
     * @return
     */
    public static boolean isRunning() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 1) {
            return true;
        }
        return false;
    }

    /**
     * 当前正处于 账户总积分界面
     *
     * @return
     */
    public static boolean isAtTotalGradeActivity() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 1 && taskStack.get(1) instanceof TotalGradeActivity) {
            return true;
        }
        return false;
    }


    public BaseAbstractActivity getCurrentActivity() {
        BaseAbstractActivity currentActivity = null;
        if (sCurrentActivityWeakRef != null) {
            currentActivity = sCurrentActivityWeakRef.get();
        }
        return currentActivity;
    }

    public void setCurrentActivity(BaseAbstractActivity activity) {
        sCurrentActivityWeakRef = new WeakReference<BaseAbstractActivity>(activity);
    }

    public static void removeAllTempTaskStack() {
        List<Activity> taskStack = ProjectApp.getTempTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * 清除出了MainActivity其他页面
     */
    public static void removeAllTaskWithMainActivity() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (null != activity && !(activity instanceof MainHomeActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }


    /**
     * pa注册流程
     */
    public static void removeAllTaskWithRegister() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof PreLoginActivity) &&
                        !(activity instanceof RegisterSelActivity) &&
                        !(activity instanceof LoginActivity) &&
                        !(activity instanceof SelRegisterTypeActivity) &&
                        !(activity instanceof SettingActivity) &&
                        !(activity instanceof RegisterPaymentAccountDescActivity) &&
                        !(activity instanceof STwoRegisterByEmailAndPhoneActiviy)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * 注册中黑名单问题
     */
    public static void removeAllTaskWithRegisterIsHkCust() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof PreLoginActivity) &&
                        !(activity instanceof SettingActivity) &&
                        !(activity instanceof LoginActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }


    /**
     * pa忘记密码 未登录 因为未登录情况下 忘记密码都会跳转到登录界面 点击登录界面的忘记密码界面顺序是 MainHomeActivity ->LoginActivity ->ForgetPaymentPasswordActivity->STwoSelForgetPasswordTypeActivity
     */
    public static void removeAllTaskWithForgetNoLoginStatusPwd() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof MainHomeActivity) &&
                        //等出态忘记密码
                        !(activity instanceof STwoSelForgetPasswordTypeActivity) &&
                        !(activity instanceof ForgetPaymentPasswordActivity) &&
                        !(activity instanceof SettingActivity) &&
                        !(activity instanceof LoginActivity) &&
                        !(activity instanceof PreLoginActivity)

                ) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }

    }

    /**
     * pa登录态忘记密码 忘记密码发起点 很多个 理论上 从哪里发起 应该返回到哪里
     */
    public static void removeAllTaskForForgetPwdWithLoginStatusMainStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null) {
                    if ((activity instanceof IdAuthInfoFillActivity) ||
                            (activity instanceof RegisterSetPaymentPswActivity) ||
                            (activity instanceof RegisterConfirmPaymentPswActivity) ||
                            (activity instanceof RegisterInputOTPActivity) ||
                            (activity instanceof CheckIdvFailedActivity) ||
                            (activity instanceof CheckIdvProgressActivity)) {
                        iterator.remove();
                        activity.finish();
                    }
                }
            }
        }
    }


    /**
     * pa绑卡
     */
    public static void removeAllTaskWithPaBindCard() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null &&
                        !(activity instanceof MainHomeActivity) &&
                        !(activity instanceof SelRegisterTypeActivity)
                ) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * checkidv failed 点击离开
     */
    public static void removeAllTaskTologinPage() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof PreLoginActivity)
                        && !(activity instanceof SettingActivity)
                        && !(activity instanceof LoginActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }


    /**
     * 判定是否有登录界面  来判断是注册流程还是其他
     *
     * @return
     */
    public static boolean containLoginPage() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && (activity instanceof LoginActivity)) {
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 绑卡成功之后，理论上应该返回到首页，这里有个特殊情况就是如果是AppCallAPP流程 要再次发起支付流程
     */
    public static void removeAllTaskForBindCard() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof MainHomeActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish 除了 MainHomeActivity
     */
    public static void removeAllTaskExcludeAppCallAppStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof AppCallAppPaymentActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish 除了 MainHomeActivity
     */
    public static void removeAllTaskExcludeMainStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof MainHomeActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    public static void removeAllTaskExcludeMainStackForRegister() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null &&
                        !(activity instanceof LoginActivity)
                        && !(activity instanceof SettingActivity)
                        && !(activity instanceof PreLoginActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish 除了 PreLoginActivity
     */
    public static void removeAllTaskExcludePreLoginStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof PreLoginActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish 除了 PreLoginActivity
     */
    public static void removeAllTaskExcludePreLoginAndLoginStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof PreLoginActivity) && !(activity instanceof LoginActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * PreLoginActivity是否存在
     */
    public static boolean isPreLoginExistInStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && (activity instanceof PreLoginActivity)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 忘记密码
     */
    public static void backForForgetPwdWithLoginStatus() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null &&
                        !(activity instanceof MainHomeActivity) &&
                        !(activity instanceof PaymentSettingActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * 忘记密码
     */
    public static void removeAllTaskForForgetPwdWithNoLoginStatus() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null &&
                        !(activity instanceof PreLoginActivity) &&
                        !(activity instanceof LoginActivity) &&
                        !(activity instanceof SettingActivity) &&
                        !(activity instanceof STwoSelForgetPasswordTypeActivity) &&
                        !(activity instanceof ForgetPaymentPasswordActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }


    /**
     * 忘记密码
     */
    public static void removeAllTaskForForgetPwdWithLoginStatus() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        int startAct = 0;
        for (int i = 0; i < taskStack.size(); i++) {
            if (taskStack.get(i) instanceof VerifyPasswordCommonActivity) {
                startAct = i;
                break;
            }
        }
        List<Activity> taskStackRemove = new ArrayList<>();
        taskStackRemove.addAll(taskStack.subList(startAct + 1, taskStack.size())) ;
        for (int i = 0; i < taskStackRemove.size(); i++) {
            Activity actItem = taskStackRemove.get(i);
            if (actItem != null) {
                actItem.finish();
                taskStack.remove(actItem);
            }
        }
    }


    public static void removeAllTaskForBindCardStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof MainHomeActivity)
                        && !(activity instanceof SelRegisterTypeActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish
     */
    public static void removeAllTaskIncludeMainStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    /**
     * activity全部finish
     */
    public static void removeAllTaskExcludeLanguageStack() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null && !(activity instanceof LanguageSettingActivity)) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }

    public static void removeAllTaskForLogin() {
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            Iterator<Activity> iterator = taskStack.iterator();
            while (iterator.hasNext()) {
                Activity activity = iterator.next();
                if (activity != null) {
                    iterator.remove();
                    activity.finish();
                }
            }
        }
    }


}
