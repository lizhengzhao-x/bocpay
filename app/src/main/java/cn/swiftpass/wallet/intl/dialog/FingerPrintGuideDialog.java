package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import cn.swiftpass.wallet.intl.R;

/**
 * @author ramon
 * Created by on 2018/1/10.
 */

public class FingerPrintGuideDialog extends Dialog {

    public FingerPrintGuideDialog(Context context) {
        super(context);
    }

    public FingerPrintGuideDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private View.OnClickListener mPosClickListener;
        private View.OnClickListener mNegClickListener;

        public Builder(Context context) {
            this.context = context;
        }

        public void setOnClickListener(View.OnClickListener posListener, View.OnClickListener negListener) {
            mPosClickListener = posListener;
            mNegClickListener = negListener;
        }

        public FingerPrintGuideDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final FingerPrintGuideDialog dialog = new FingerPrintGuideDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_fingerprint_guide, null);
            Button posBtn = layout.findViewById(R.id.btn_open);
            Button negBtn = layout.findViewById(R.id.btn_cancel);

            posBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (null != mPosClickListener) {
                        mPosClickListener.onClick(v);
                    }
                }
            });

            negBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (null != mNegClickListener) {
                        mNegClickListener.onClick(v);
                    }
                }
            });
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

