package cn.swiftpass.wallet.intl.module.home.contract;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.AccountPointDataEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.HomeLifeSceneAreaCountEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;

public class HomeContract {

    public interface View extends IView {


        void queryAccountPointDataSuccess(AccountPointDataEntity response);

        void queryAccountPointDataError(String errorCode, String errorMsg, boolean iShow);

        void getNewMsgListSuccess(NewMsgListEntity response);

        void getNewMsgListError(String errorCode, String errorMsg);

        void checkSetLimitBalanceSuccess(MySmartAccountEntity response);

        void checkSetLimitBalanceError(String errorCode, String errorMsg);

        void getMySmartAccountInfoSuccess(MySmartAccountEntity response);

        void getMySmartAccountInfoError(String errorCode, String errorMsg,boolean iShow);

        void getLifeSceneAreaCountError(String errorCode, String errorMsg);

        void getLifeSceneAreaCountSuccess(HomeLifeSceneAreaCountEntity response);

        void getRegEddaInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);

        void refreshLifeMenuError(String errorCode, String errorMsg);

        void refreshLifeMenuSuccess(InitBannerEntity response);
    }

    public interface Presenter extends IPresenter<View> {

        /**
         * @param iShowLoading 显示加载loading
         */
        void queryAccountPointData(boolean iShowLoading);

        /**
         * 修改限额前，需要查询智能账户信息
         */
        void checkSetLimitBalance();

        /**
         * 统一接口 智能账户信息
         */
        void getMySmartAccountInfo(boolean iShow, Consumer<MySmartAccountEntity> consumerTopUpAccount,@Nullable Consumer<String> consumerFail);


        /**
         * 最新消息列表
         * @param action
         */
        void getNewMsgList(boolean action);


        /**
         * 查询电子券数量
         */
        void getLifeSceneAreaCount();

        void getRegEddaInfo();

        void refreshLifeMenu();
    }
}

