package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetSmartLevelProtocol extends BaseProtocol {
    String mMobile;
    String mVerifyCode;
    String mAction;

    public GetSmartLevelProtocol(String mobile, String verifyCode, String action, NetWorkCallbackListener dataCallback) {
        this.mMobile = mobile;
        this.mVerifyCode = verifyCode;
        this.mDataCallback = dataCallback;
        this.mAction = action;
        mUrl = "api/smartForgot/getSmartLevel";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
    }
}
