package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;

/**
 * @author ramon
 * create date on  on 2018/8/20
 * 转账确认接口
 */

public class TransferConfirmProtocol extends BaseProtocol {
    public static final String TAG = TransferConfirmProtocol.class.getSimpleName();
    //    //债务方账户类型 0：电话，1：邮箱
//    private String transferType;
    TransferConfirmReq mReq;
    String mAction;

    public TransferConfirmProtocol(TransferConfirmReq req, NetWorkCallbackListener callback) {
        mReq = req;
        mDataCallback = callback;
        mUrl = "api/transfer/transferConfirmation";
    }

    public TransferConfirmProtocol(TransferConfirmReq req, String action, NetWorkCallbackListener callback) {
        mReq = req;
        mDataCallback = callback;
        mAction = action;
        mUrl = "api/transfer/transferConfirmation";
    }

    @Override
    public void packData() {
        super.packData();
        if (null != mReq) {
            mBodyParams.put(RequestParams.TRANSFER_ORDER_NUM, mReq.scrRefNo);
            mBodyParams.put(RequestParams.TRANSFER_ACCOUNT, mReq.tansferToAccount);
            mBodyParams.put(RequestParams.TRANSFER_AMOUNT, mReq.transferAmount);
            mBodyParams.put(RequestParams.TRANSFER_BANK_TYPE, mReq.bankType);
            mBodyParams.put(RequestParams.TRANSFER_TYPE, mReq.transferType);
            if (!TextUtils.isEmpty(mReq.billReference)) {
                mBodyParams.put(RequestParams.BILLREFERENCE, mReq.billReference);
            }
            if (mReq.isLiShi) {
                mBodyParams.put(RequestParams.TRANSFERCATEGORY, "7");
            }
            if (!TextUtils.isEmpty(mAction)) {
                mBodyParams.put(RequestParams.ACTION, mAction);
            }
        }
    }
}
