package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderAmountContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.RateInfoEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderAccountListEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderAmountEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderCacheEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.TransferCrossBorderAmountPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_ORDER_ID;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_PURPOSE;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_PURPOSE_CODE;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_SELECT_ACCOUNT;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_SET_LIMIT_DATA;

public class TransferCrossBorderAmountActivity extends BaseCompatActivity<TransferCrossBorderAmountContract.Presenter> implements TransferCrossBorderAmountContract.View {


    public static final int REQ_SELECT_PURPOSE = 101;
    public static final int REQ_SELECT_ACCOUNT = 102;
    public static final int REQ_CHANGE_LIMIT = 103;
    private static final String ACTION_CLICK_TRANSFER_LIMIT = "ACTION_CLICK_TRANSFER_LIMIT";
    private static final String ACTION_REFRESH_ACCOUNT = "ACTION_REFRESH_ACCOUNT";
    @BindView(R.id.tv_currency_hkd)
    TextView tvCurrencyHkd;
    @BindView(R.id.ed_amount_hkd)
    EditTextWithDel edAmountHkd;
    @BindView(R.id.tv_currency_rmb)
    TextView tvCurrencyRmb;
    @BindView(R.id.ed_amount_rmb)
    EditTextWithDel edAmountRmb;
    @BindView(R.id.tv_update_set_limit)
    TextView tvUpdateSetLimitTime;
    @BindView(R.id.tv_rate_rmb)
    TextView tvRateRmb;
    @BindView(R.id.tv_rate_hkd)
    TextView tvRateHkd;
    @BindView(R.id.tv_transfer_limit_daily)
    TextView tvTransferLimitDaily;
    @BindView(R.id.iv_edit_transfer_limit)
    ImageView ivEditTransferLimit;
    @BindView(R.id.ll_purpose)
    LinearLayout llPurpose;
    @BindView(R.id.tv_purpose)
    TextView tvPurpose;
    @BindView(R.id.tv_payment_account)
    TextView tvPaymentAccount;
    @BindView(R.id.ll_payment_account)
    LinearLayout llPaymentAccount;
    @BindView(R.id.tv_next)
    TextView tvNext;
    @BindView(R.id.tv_exceed_amount)
    TextView tvExceedAmount;
    @BindView(R.id.id_tv_denghao)
    TextView tvDengHao;
    @BindView(R.id.view_line_rmb_ed)
    View viewLineRmb;
    @BindView(R.id.view_line_hkd_ed)
    View viewLineHkd;


    private TransferCrossBorderAmountEntity amountData = new TransferCrossBorderAmountEntity();
    private TransferCrossBorderBaseEntity baseEntity;
    private String purpose, purposeCode;
    private TransferCrossBorderAccountListEntity selectAccount;
    private BigDecimal hkdDecimal;
    private BigDecimal rmbDecimal;
    private BigDecimal hkdCurrency;//一港币等于多少人民币
    private BigDecimal rmbCurrency;//一人民币等于多少港币
    private final int TYPE_HKD = 1;
    private final int TYPE_RMB = 2;
    private boolean isPrintHKD=false;


    @Override
    public void init() {
        setToolBarTitle(R.string.SMB1_1_4);
        EventBus.getDefault().register(this);
        if (getIntent() != null) {
            baseEntity = (TransferCrossBorderBaseEntity) getIntent().getSerializableExtra(TransferCrossBorderConst.DATA_TRANSFER_BASE);
            String payeeName = getIntent().getStringExtra(TransferCrossBorderConst.DATA_PAYEE_NAME);
            String payeeCardId = getIntent().getStringExtra(TransferCrossBorderConst.DATA_PAYEE_CARD_ID);
            String payeeBank = getIntent().getStringExtra(TransferCrossBorderConst.DATA_PAYEE_BANK);
            String payeeCardType = getIntent().getStringExtra(TransferCrossBorderConst.DATA_PAYEE_CARD_TYPE);

            //选择了最近汇款联系人 有金额/转账人等等
            String transferAmount = getIntent().getExtras().getString(TransferCrossBorderConst.DATA_TRANSFER_AMOUNT, "");
            purposeCode = getIntent().getExtras().getString(TransferCrossBorderConst.DATA_FOR_USED_CODE, "");
            purpose = getIntent().getExtras().getString(TransferCrossBorderConst.DATA_FOR_USED_VALUE, "");
            selectAccount = (TransferCrossBorderAccountListEntity) getIntent().getExtras().getSerializable(TransferCrossBorderConst.DATA_TRANSFER_ACCOUNT);
            amountData.payeeName = payeeName;
            amountData.payeeCardId = payeeCardId;
            amountData.payeeBank = payeeBank;
            amountData.payeeCardType = payeeCardType;
            ArrayList<TransferCrossBorderAccountListEntity> accountListEntities = baseEntity.accountList;
            if (!accountListEntities.isEmpty()) {
                for (int i = 0; i < accountListEntities.size(); i++) {
                    TransferCrossBorderAccountListEntity transferCrossBorderAccountListEntity = accountListEntities.get(i);
                    if (TextUtils.equals(transferCrossBorderAccountListEntity.cardType, "2")) {
                        String sb = getString(R.string.LYP01_06_4) + "(" + transferCrossBorderAccountListEntity.panFour + ")";
                        tvPaymentAccount.setText(sb);
                        selectAccount = transferCrossBorderAccountListEntity;
                        break;
                    }
                }
            }
            try {
                hkdCurrency = new BigDecimal(baseEntity.HKD2CNYRate);
                rmbCurrency = new BigDecimal(baseEntity.CNY2HKDRate);
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            bindView();
            if (!TextUtils.isEmpty(transferAmount)) {
                edAmountRmb.setContentText(transferAmount);
                rmbDecimal = new BigDecimal(transferAmount);
                updateAmount(TYPE_HKD);
                tvPurpose.setText(purpose);

                if (selectAccount != null) {
                    String sb = ("1".equals(selectAccount.cardType) ? getString(R.string.LYP01_06_6) : getString(R.string.LYP01_06_4)) + "(" + selectAccount.panFour + ")";
                    tvPaymentAccount.setText(sb);
                }
                edAmountRmb.hideDelView();
                edAmountHkd.hideDelView();
                updateNext();
            }

        }

        addTextChangeListener();
        updateNext();
        edAmountHkd.getEditText().requestFocus();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.queryRateInfo();
        }
    }

    private void addTextChangeListener() {
        edAmountHkd.getEditText().setFilters(new InputFilter[]{new DecimalInputTextWatcher(9, 2)});
        edAmountRmb.getEditText().setFilters(new InputFilter[]{new DecimalInputTextWatcher(9, 2)});
        edAmountHkd.getmDelImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAmount();
            }
        });
        edAmountRmb.getmDelImageView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetAmount();
            }
        });

        edAmountHkd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String hkd = TextUtils.isEmpty(s) ? "0" : s.toString();
                hkdDecimal = new BigDecimal(hkd);
                if (edAmountHkd.getEditText().isFocused()) {
                    updateAmount(TYPE_RMB);
                }
                updateNext();
                if (TextUtils.equals(hkd, "0")) {
                    tvExceedAmount.setVisibility(View.GONE);
                    viewLineRmb.setBackgroundColor(getColor(R.color.line_main));
                    viewLineHkd.setBackgroundColor(getColor(R.color.line_main));
                    return;
                }

                try {
                    MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                    boolean isExceed = Double.valueOf(edAmountHkd.getText()) > Double.valueOf(mySmartAccountEntity.getOutavailbal());
                    tvExceedAmount.setVisibility(isExceed ? View.VISIBLE : View.GONE);
                    viewLineRmb.setBackgroundColor(isExceed?getColor(R.color.color_FFBF2F4F):getColor(R.color.line_main));
                    viewLineHkd.setBackgroundColor(isExceed?getColor(R.color.color_FFBF2F4F):getColor(R.color.line_main));
                } catch (NumberFormatException e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edAmountRmb.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String rmb = TextUtils.isEmpty(s) ? "0" : s.toString();
                rmbDecimal = new BigDecimal(rmb);
                if (edAmountRmb.getEditText().isFocused()) {
                    updateAmount(TYPE_HKD);
                }
                updateNext();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void resetAmount() {
        edAmountHkd.setContentText("");
        edAmountRmb.setContentText("");
        viewLineRmb.setBackgroundColor(getColor(R.color.line_main));
        viewLineHkd.setBackgroundColor(getColor(R.color.line_main));
        tvExceedAmount.setVisibility(View.GONE);
        hkdDecimal = new BigDecimal("0");
        updateNext();
    }

    private void bindView() {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        tvRateRmb.setText("RMB 1");
        tvRateHkd.setText("HKD " + baseEntity.CNY2HKDRate);
        tvUpdateSetLimitTime.setText(getString(R.string.CT1_3a_8) + " " + baseEntity.lastUpdatedRate);
        if (mySmartAccountEntity!=null&&!TextUtils.isEmpty(mySmartAccountEntity.getOutavailbal())) {
            tvTransferLimitDaily.setText(getString(R.string.CT1_3a_9) + " HKD " + AndroidUtils.formatPriceWithPoint(Double.valueOf(mySmartAccountEntity.getOutavailbal()), true));
        }

    }

    private void updateAmount(int type) {

        if (type == TYPE_HKD) {
            isPrintHKD=true;
            String hkd = rmbDecimal.multiply(rmbCurrency).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
            if (TextUtils.isEmpty(edAmountRmb.getEditText().getText().toString())) {
                edAmountHkd.setContentText("");
            } else {
                try {
                    edAmountHkd.setContentText(hkd);
                    edAmountHkd.getEditText().setSelection(hkd.length());
                } catch (Throwable e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
            }
            hkdDecimal = new BigDecimal(hkd);
        } else if (type == TYPE_RMB) {
            isPrintHKD=false;
            if (TextUtils.isEmpty(edAmountHkd.getEditText().getText().toString())) {
                edAmountRmb.setContentText("");
            } else {
                String rmb = hkdDecimal.multiply(hkdCurrency).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                edAmountRmb.setContentText(rmb);
            }

        }
    }

    public void updateNext() {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity!=null&&!TextUtils.isEmpty(purposeCode) && selectAccount != null && hkdDecimal != null && hkdDecimal.compareTo(BigDecimal.ZERO) > 0 && hkdDecimal.doubleValue() <= Double.parseDouble(mySmartAccountEntity.getOutavailbal())) {
            tvNext.setEnabled(true);
        } else {
            tvNext.setEnabled(false);
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_cross_border_amount;
    }

    @Override
    protected TransferCrossBorderAmountContract.Presenter createPresenter() {
        return new TransferCrossBorderAmountPresenter();
    }

    @Override
    public void commitTransferUseDataSuccess(TransferCrossBorderDetailEntity response) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(DATA_TRANSFER_ORDER_ID, response.txnId);
        ActivitySkipUtil.startAnotherActivity(TransferCrossBorderAmountActivity.this, TransferCrossBorderDetailActivity.class, params, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void commitTransferUseDataError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_TRANFER_NOT_IN_TIME)) {
            showErrorMsgDialog(this, errorMsg, R.string.RP1_6_7, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(TransferCrossBorderAmountActivity.this, MainHomeActivity.class);
                }
            });
        } else {
            showErrorMsgDialog(TransferCrossBorderAmountActivity.this, errorMsg);
        }
    }

    @Override
    public void getSmartAccountInfoSuccess(String action, MySmartAccountEntity response) {
        if (ACTION_CLICK_TRANSFER_LIMIT.equals(action)){
            SmartAccountAdjustDailyLimitActivity.startActivity(this, response, response.getHideMobile());
        }
        if (response!=null){
            double limitBalance = Double.parseDouble(response.getOutavailbal());
            tvTransferLimitDaily.setText(getString(R.string.CT1_3a_9) + " HKD " + AndroidUtils.formatPriceWithPoint(limitBalance, true));
            try {
                boolean isExceed = Double.parseDouble(edAmountHkd.getText()) > limitBalance;
                tvExceedAmount.setVisibility(isExceed ? View.VISIBLE : View.GONE);
                viewLineRmb.setBackgroundColor(isExceed?getColor(R.color.color_FFBF2F4F):getColor(R.color.line_main));
                viewLineHkd.setBackgroundColor(isExceed?getColor(R.color.color_FFBF2F4F):getColor(R.color.line_main));
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            updateNext();
        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {

        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void queryRateInfoSuccess(RateInfoEntity response) {

        if (!TextUtils.isEmpty(response.getLastUpdatedRate())) {
            //获取汇率更新时间  然后上传给后台  详情页面显示的时间就是这个上传的时间
            baseEntity.lastUpdatedRate = response.getLastUpdatedRate();
            baseEntity.refNo = response.getRefNo();
            baseEntity.HKD2CNYRate = response.getHKD2CNYRate();
            baseEntity.CNY2HKDRate = response.getCNY2HKDRate();
            tvUpdateSetLimitTime.setText(getString(R.string.CT1_3a_8) + " " + response.getLastUpdatedRate());

            tvRateRmb.setText("RMB 1");
            tvRateHkd.setText("HKD " + baseEntity.CNY2HKDRate);
        }
        tvDengHao.setVisibility(View.VISIBLE);
        //更新费率
        try {
            hkdCurrency = new BigDecimal(response.getHKD2CNYRate());
            rmbCurrency = new BigDecimal(response.getCNY2HKDRate());
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        //更新计算
        if (!TextUtils.isEmpty(edAmountRmb.getText())) {
            rmbDecimal = new BigDecimal(edAmountRmb.getText());
            if (isPrintHKD){
                updateAmount(TYPE_HKD);
            }else {
                updateAmount(TYPE_RMB);
            }
        }
        updateNext();
    }

    @Override
    public void queryRateInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }



    private void confirmData() {
        if (selectAccount != null) {
            String isTransferToSelf = getIntent().getExtras().getString(TransferCrossBorderConst.ISTRANSFERTOSELF);
            mPresenter.commitTransferUseData(amountData.payeeName, amountData.payeeCardId, amountData.payeeBank, edAmountHkd.getText(), edAmountRmb.getText(), purposeCode, selectAccount.cardId, amountData.payeeCardType, baseEntity.refNo, baseEntity.lastUpdatedRate, baseEntity.HKD2CNYRate, isTransferToSelf);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mPresenter != null) {
            mPresenter.queryRateInfo();
        }
        if (resultCode == RESULT_OK && requestCode == REQ_SELECT_PURPOSE) {
            if (data != null) {
                purpose = data.getStringExtra(DATA_TRANSFER_PURPOSE);
                purposeCode = data.getStringExtra(DATA_TRANSFER_PURPOSE_CODE);
                if (!TextUtils.isEmpty(purpose)){
                    tvPurpose.setText(purpose);
                }
            }
        } else if (resultCode == RESULT_OK && requestCode == REQ_SELECT_ACCOUNT) {
            if (data != null) {
                selectAccount = (TransferCrossBorderAccountListEntity) data.getSerializableExtra(DATA_TRANSFER_SELECT_ACCOUNT);
                if (selectAccount != null) {
                    String sb = ("1".equals(selectAccount.cardType) ? getString(R.string.LYP01_06_6) : getString(R.string.LYP01_06_4)) + "(" + selectAccount.panFour + ")";
                    tvPaymentAccount.setText(sb);
                }
            }
        } else if (resultCode == RESULT_OK && requestCode == REQ_CHANGE_LIMIT) {
            if (data != null) {
                String limit = data.getStringExtra(DATA_TRANSFER_SET_LIMIT_DATA);
                tvTransferLimitDaily.setText(getString(R.string.CT1_3a_9) + " HKD " + limit);
            }

        }

        updateNext();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            //限额发生变化
            if (mPresenter != null) {
                mPresenter.getSmartAccountInfo(ACTION_REFRESH_ACCOUNT);
            }
        }
    }

    @OnClick({R.id.iv_edit_transfer_limit, R.id.ll_purpose, R.id.ll_payment_account, R.id.tv_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_edit_transfer_limit:
                if (ButtonUtils.isFastDoubleClick()) return;
                mPresenter.getSmartAccountInfo(ACTION_CLICK_TRANSFER_LIMIT);
                break;
            case R.id.ll_purpose:
                HashMap<String, Object> param = new HashMap<>();
                param.put(TransferCrossBorderConst.DATA_TRANSFER_BASE, baseEntity);
                if (!TextUtils.isEmpty(purpose)) {
                    param.put(TransferCrossBorderConst.DATA_TRANSFER_PURPOSE, purpose);
                }
                ActivitySkipUtil.startAnotherActivityForResult(TransferCrossBorderAmountActivity.this, TransferCrossBorderPurposeListActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQ_SELECT_PURPOSE);
                break;
            case R.id.ll_payment_account:
                HashMap<String, Object> paramAccount = new HashMap<>();
                paramAccount.put(TransferCrossBorderConst.DATA_TRANSFER_BASE, baseEntity);
                if (selectAccount != null) {
                    paramAccount.put(TransferCrossBorderConst.DATA_TRANSFER_SELECT_ACCOUNT, selectAccount);
                }
                ActivitySkipUtil.startAnotherActivityForResult(TransferCrossBorderAmountActivity.this, TransferCrossBorderAccountListActivity.class, paramAccount, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQ_SELECT_ACCOUNT);
                break;
            case R.id.tv_next:
                confirmData();
                break;
            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        TransferCrossBorderCacheEntity cacheEntity = new TransferCrossBorderCacheEntity();
        cacheEntity.setForUsedObj(new TransferCrossBorderCacheEntity.UsedObjBean(purposeCode, purpose));
        if (!TextUtils.isEmpty(edAmountRmb.getText()))
            cacheEntity.amount = edAmountRmb.getText();
        if (selectAccount != null)
            cacheEntity.setAccountObj(selectAccount);

        data.putExtra(TransferCrossBorderConst.DATA_TRANSFER_CACHE, cacheEntity);

        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
