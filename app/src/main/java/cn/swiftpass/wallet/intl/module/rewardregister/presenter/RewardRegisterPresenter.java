package cn.swiftpass.wallet.intl.module.rewardregister.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.module.rewardregister.api.CheckRegistrationStatusProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetPansForRewardProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.api.RewardRegisterProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.contract.RewardRegisterContract;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.CheckRegistrationStatus;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardPansListEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardRegisterEntity;

public class RewardRegisterPresenter extends BasePresenter<RewardRegisterContract.View> implements RewardRegisterContract.Presenter {


    private RewardPansListEntity response;

    @Override
    public void rewardRegister(String pan, String campaignId) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new RewardRegisterProtocol(pan, campaignId, new NetWorkCallbackListener<RewardRegisterEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().rewardRegisterError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(RewardRegisterEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().rewardRegisterSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void rewardCardList(String bankCardTye) {
        if (response != null) {
            getView().rewardCardListSuccess(response);
            return;
        }
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetPansForRewardProtocol(bankCardTye, new NetWorkCallbackListener<RewardPansListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().rewardCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RewardPansListEntity responseIn) {
                if (getView() != null) {
                    response = responseIn;
                    getView().dismissDialog();
                    getView().rewardCardListSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void rewardCheckStatus(String cardType, String campaignId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckRegistrationStatusProtocol(cardType, campaignId, new NetWorkCallbackListener<CheckRegistrationStatus>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().rewardCheckStatusError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CheckRegistrationStatus response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().rewardCheckStatusSuccess(response);
                }
            }
        }).execute();

    }
}
