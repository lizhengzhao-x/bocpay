package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.SignUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.NormalLoginSucEntity;
import cn.swiftpass.wallet.intl.utils.HashUtility;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 确认登陆接口，返回对象为 AutoLoginSucEntity
 * e2ee
 */

public class ConfirmLoginProtocol extends E2eeProtocol {
    public static final String TAG = ConfirmLoginProtocol.class.getSimpleName();
    /**
     * 用户id
     */
    String mWalletId;
    /**
     * 支付密码
     */
    //String mPasscode;
    /**
     * 区号，登录时选择的区号
     */
    String mRegion;
    /**
     * 取值：L：登录
     */
    String mAction;
    /**
     * 取值：Y；是否可以在其它设备继续登录
     */
    String mIsContinue;

    //一期加密
    String mOldPin;

    //账号
    String mAccount;


    public ConfirmLoginProtocol(String account, String walletId, String passcode, String region, String action, String isContinue, boolean needVerifyPin, NetWorkCallbackListener dataCallback) {
        this.mWalletId = walletId;
        this.mPasscode = passcode;
        this.mRegion = region;
        this.mAction = action;
        this.mIsContinue = isContinue;
        this.mDataCallback = dataCallback;
        mAccount = account;
        mOldPin = needVerifyPin ? HashUtility.getMD5(passcode) : "";
        mUrl = "api/login/login";
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        mBodyParams.put(RequestParams.PASSCODE, mPasscode);
        mBodyParams.put(RequestParams.REGION, mRegion);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.ISCONTINUE, mIsContinue);
        mBodyParams.put(RequestParams.FIRST_PIN, mOldPin);
        mBodyParams.put(RequestParams.ACCOUNT, mAccount);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        try {
            NormalLoginSucEntity entity = mGson.fromJson(data, mDataCallback.mType);
            if (null != entity && !TextUtils.isEmpty(entity.getServerPubKey())) {
                HttpCoreKeyManager.getInstance().saveServerPublicKey(entity.getServerPubKey());
                return SignUtils.verify(sign, data, entity.getServerPubKey());
            } else {
                return super.verifySign(sign, data);
            }
        } catch (Exception e) {
            return false;
        }
    }

}
