package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryItemList;

public class CreditCardRewardHistoryAdapter extends RecyclerView.Adapter {

    private static final int TYPE_NORMAL_ITEM = 101;
    private static final int TYPE_MORE_ITEM = 102;
    private final Context context;
    private final OnRewadLoadMoreListener loadMoreListener;
    private final OnRewadDetailListener detailListener;
    private List<CreditCardRewardHistoryItemList> data = new ArrayList<>();
    ;

    private boolean hasNext;
    private String lastRewardDate;


    public CreditCardRewardHistoryAdapter(Context context, OnRewadLoadMoreListener loadMoreListener, OnRewadDetailListener detailListener) {
        this.context = context;
        this.loadMoreListener = loadMoreListener;
        this.detailListener = detailListener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_MORE_ITEM) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.item_reward_history_more, parent, false);
            return new RewardHistoryMoreHolder(inflate, loadMoreListener);
        } else {
            View inflate = LayoutInflater.from(context).inflate(R.layout.item_reward_history_action, parent, false);
            return new RewardHistoryActionHolder(context, inflate);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof RewardHistoryActionHolder) {
            RewardHistoryActionHolder cardRewardHolder = (RewardHistoryActionHolder) holder;
            cardRewardHolder.bindData(data.get(position), detailListener);
        } else if (holder instanceof RewardHistoryMoreHolder) {
            RewardHistoryMoreHolder moreRewardHolder = (RewardHistoryMoreHolder) holder;
            moreRewardHolder.bindData(hasNext, lastRewardDate);
        }
    }

    @Override
    public int getItemCount() {
        return data != null && data.size() > 0 ? data.size() + 1 : 0;
    }


    @Override
    public int getItemViewType(int position) {
        if (data != null && data.size() > 0 && position == getItemCount() - 1) {
            return TYPE_MORE_ITEM;
        }
        return TYPE_NORMAL_ITEM;
    }

    public void addData(List<CreditCardRewardHistoryItemList> rows) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.addAll(rows);

    }

    public void setData(List<CreditCardRewardHistoryItemList> rows, boolean hasNext, String lastRewardDate) {
        data = rows;
        this.hasNext = hasNext;
        this.lastRewardDate = lastRewardDate;
    }

    public List<CreditCardRewardHistoryItemList> getData() {
        return data;
    }
}
