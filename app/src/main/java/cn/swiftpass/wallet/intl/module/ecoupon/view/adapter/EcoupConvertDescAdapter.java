package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;

/**
 * 积分兑换记录
 */
public class EcoupConvertDescAdapter implements AdapterItem<RedeemableGiftListEntity.EVoucherListBean> {
    private Context context;

    public EcoupConvertDescAdapter(Context context) {
        this.context = context;
    }

    private TextView mId_ecoupon_name;
    private TextView mId_ecoupon_count;
    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoup_descrip;
    }

    @Override
    public void bindViews(View root) {
        mId_ecoupon_name = (TextView) root.findViewById(R.id.id_ecoup_name);
        mId_ecoupon_count = (TextView) root.findViewById(R.id.id_ecoup_count);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(RedeemableGiftListEntity.EVoucherListBean resultRedmBean, int position) {
        mPosition = position;
        mId_ecoupon_name.setText(resultRedmBean.getEVoucherDesc());
        mId_ecoupon_count.setText(resultRedmBean.getCurrentSelCnt() + context.getString(R.string.EC05c_1));
    }
}
