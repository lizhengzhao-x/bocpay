package cn.swiftpass.wallet.intl.module.setting.contract;

import android.content.Context;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteListEntity;

public class InviteContactContract {
    public interface View extends IView {
        void getInviteContactFail(String errorCode, String errorMsg);

        void getInviteContactSuccess(InviteListEntity inviteListEntity);

        void getDataSuccess(List<InviteContactEntity> dataList,int totalNum);
    }

    public interface Presenter extends IPresenter<InviteContactContract.View> {
        void getInviteContact(List<InviteContactEntity> accountList);

        void initPageData(Context mContext, int currentPage, int pageSize, List<InviteContactEntity> allContactList);
    }
}
