package cn.swiftpass.wallet.intl.base.otp;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;

;

/**
 * @name HKBocBill
 * @class name：cn.swiftpass.bocbill.model.base.otp
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/12 14:09
 * @change
 * @chang time
 * @class describe
 */
public interface OTPSendView<V extends IPresenter> extends IView {
    void sendOTPFailed(String errorCode, String errorMsg);

    void sendOTPSuccess(ContentEntity response);


}
