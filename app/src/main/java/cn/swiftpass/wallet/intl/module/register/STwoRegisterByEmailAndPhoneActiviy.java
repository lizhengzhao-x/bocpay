package cn.swiftpass.wallet.intl.module.register;

import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_REGISTER;
import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_RESET;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.httpcore.utils.PhoneUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetTnxIdEntity;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.register.contract.PARegisterVerifyCodeContract;
import cn.swiftpass.wallet.intl.module.register.presenter.PARegisterVerifyCodePresenter;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.transfer.view.SelCountyCodeActivity;
import cn.swiftpass.wallet.intl.sdk.idv.IdvConstants;
import cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;

/**
 * s2用户注册页面 输入手机号界面
 */

public class STwoRegisterByEmailAndPhoneActiviy extends BaseCompatActivity<PARegisterVerifyCodeContract.Presenter> implements PARegisterVerifyCodeContract.View {

    private TextView mTv_region_code_str;
    private ImageView mTv_register_play_video;
    private ImageView mIv_sel_region_arrow;
    private cn.swiftpass.wallet.intl.widget.EditTextWithDel mEtwd_transfer_phone_num;
    private TextView mId_register_next;
    private static final int REQUEST_IDV_SDK = 30000;
    private String mPhoneNum, mCountryCode;
    private String mTransactionUniqueID;
    private String mIdvFid, mIdvSequnumber;
    private int mCurrentType;
    private int maxLen;
    @BindView(R.id.tv_account_flag)
    TextView tvAccountFlag;


    /**
     * onResume的时候是否需要刷新验证码
     */
    private boolean isNeedRefreshCode = false;


    /**
     * mIv_verify_code 验证码显示框
     * mIv_refresh_code 验证码刷新按钮
     * mCet_verify_code 验证码输入框
     * mLl_verify_code 验证码布局
     */
    @BindView(R.id.iv_verify_code)
    ImageView mIv_verify_code;
    @BindView(R.id.iv_refresh_code)
    ImageView mIv_refresh_code;
    @BindView(R.id.cet_verify_code)
    CustomTvEditText mCet_verify_code;
    @BindView(R.id.id_verifycode_view)
    LinearLayout mLl_verify_code;


    /**
     * 判断是否为sdk回调后跳转 如果是  则该条记录不埋点
     */
    private boolean isAnalysis = true;


    /**
     * idv背景图uri
     */
    private String baseMainBackgroundUri;


    private void bindViews() {
        mTv_region_code_str = (TextView) findViewById(R.id.tv_region_code_str);
        mIv_sel_region_arrow = (ImageView) findViewById(R.id.iv_sel_region_arrow);
        mTv_register_play_video = (ImageView) findViewById(R.id.id_register_play_video);
        mEtwd_transfer_phone_num = (cn.swiftpass.wallet.intl.widget.EditTextWithDel) findViewById(R.id.etwd_transfer_phone_num);
        mId_register_next = (TextView) findViewById(R.id.id_register_next);
        mId_register_next.setOnClickListener(onClickListener);
        mTv_region_code_str.setOnClickListener(onClickListener);
        mIv_sel_region_arrow.setOnClickListener(onClickListener);
        mEtwd_transfer_phone_num.hideErrorView();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mTv_register_play_video.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, Constants.BOCPAYTCT_PLAY_VIDE_URL);
                mHashMaps.put(Constants.WEB_VIEW_ERROR_IS_HINT, true);
                mHashMaps.put(Constants.IS_VIDEO_PLAY, true);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        String lan = SpUtils.getInstance(STwoRegisterByEmailAndPhoneActiviy.this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            mTv_register_play_video.setImageResource(R.mipmap.play_video_bttn_sc);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            mTv_register_play_video.setImageResource(R.mipmap.play_video_bttn_tc);
        } else {
            mTv_register_play_video.setImageResource(R.mipmap.play_video_bttn_en);
        }

        maxLen = 11;
    }

    private void updateEditFlag() {
        //对输入的数字，每四位加一个空格
        //获取输入框中的内容,不可以去空格
        String etContent = mEtwd_transfer_phone_num.getEditText().getText().toString();
        if (TextUtils.isEmpty(etContent)) {
            tvAccountFlag.setText("");
            updateOkBackground(mId_register_next, false);
            return;
        }
        String countryCode = mTv_region_code_str.getText().toString().trim();
        if (!AndroidUtils.validatePhone(countryCode, etContent)) {
            tvAccountFlag.setText(getString(R.string.LG2101_940_1));
            updateOkBackground(mId_register_next, false);
        } else {
            if (AndroidUtils.validatePhoneRegister(countryCode, etContent)) {
                updateOkBackground(mId_register_next, true);
            } else {
                updateOkBackground(mId_register_next, false);
            }
            tvAccountFlag.setText("");
        }

        //PA用户注册有验证码，需单独处理
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA && mCet_verify_code.getInputText().length() != 4) {
            updateOkBackground(mId_register_next, false);
        }


    }

    @Override
    protected void onStart() {
        isAnalysis = true;
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedRefreshCode && mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            if (mPresenter != null) {
                getVerifyCode();
                isNeedRefreshCode = false;
            }
        }
    }


    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            if (isAnalysis) {
                return true;
            }
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.AGREE_TC_PAGE;
        analysisPageEntity.current_address = PagerConstant.INPUT_REGISTRATION_MOBILE;
        return analysisPageEntity;
    }

    @Override
    protected PARegisterVerifyCodeContract.Presenter createPresenter() {
        return new PARegisterVerifyCodePresenter();
    }

    @Override
    public void init() {
        bindViews();
        mCurrentType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            setToolBarTitle(R.string.bind_title);
            mLl_verify_code.setVisibility(View.GONE);
        } else if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
            mCountryCode = Constants.DATA_COUNTRY_CODES[1];
            mTv_region_code_str.setText(mCountryCode);
            initWatcher();
            //初始化验证码控件
            initVerifyCodeView();
            getVerifyCode();
        } else if (mCurrentType == Constants.PAGE_FLOW_FORGETPASSWORD) {
            mLl_verify_code.setVisibility(View.GONE);
            setToolBarTitle(R.string.setting_payment_fgw);
            //忘记密码 绑卡 关闭s3 入口
            mCountryCode = Constants.DATA_COUNTRY_CODES[1];
            mTv_region_code_str.setText(mCountryCode);
            initWatcher();
        }
        //idv的涉及到的一些errorCode errorMsg 需要提前拉取
        CommonRequestUtils.getServerErrorCode(this);
        mEtwd_transfer_phone_num.getEditText().requestFocus();
    }


    /**
     * 初始化验证码控件
     */
    private void initVerifyCodeView() {
        mLl_verify_code.setVisibility(View.VISIBLE);
        mCet_verify_code.getEditText().addTextChangedListener(textWatcher);
        mCet_verify_code.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mIv_refresh_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVerifyCode();
            }
        });
    }


    /**
     * 获取验证码
     */
    private void getVerifyCode() {
        mIv_refresh_code.setEnabled(false);
        if (mPresenter != null) {
            mPresenter.getPARegisterVerifyCode(this);
        }
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ButtonUtils.isFastDoubleClick(v.getId())) {
                return;
            }
            if (v.getId() == R.id.id_register_next) {

                //获取权限并且验证验证码
                getPermissionAndcheckPAAccountVerifyCode();
            } else if (v.getId() == R.id.tv_region_code_str || v.getId() == R.id.iv_sel_region_arrow) {
                HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                mHashMapsLogin.put(Constants.DATA_COUNTRY_CODE, mTv_region_code_str.getText().toString());
                //用来区分登入进入显示字段
                mHashMapsLogin.put(Constants.LOGIN_TYPE, 1);
                ActivitySkipUtil.startAnotherActivityForResult(STwoRegisterByEmailAndPhoneActiviy.this, SelCountyCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_COUNTRY);
            } else {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                ActivitySkipUtil.startAnotherActivity(STwoRegisterByEmailAndPhoneActiviy.this, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    };


    /**
     * 获取权限并且验证验证码
     */
    private void getPermissionAndcheckPAAccountVerifyCode() {
        PermissionInstance.getInstance().getPermission(STwoRegisterByEmailAndPhoneActiviy.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {

                //PA用户注册有验证码，需单独处理
                if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                    String verifyCode = mCet_verify_code.getInputText();
                    String phone = mEtwd_transfer_phone_num.getText();
                    String mCountryCode = mTv_region_code_str.getText().toString().trim();
                    mCountryCode = mCountryCode.replace("+", "");
                    final String finalPhone = (mCountryCode + "-" + phone).replace(" ", "");
                    if (mPresenter != null) {
                        mPresenter.checkPARegisterVerifyCode(STwoRegisterByEmailAndPhoneActiviy.this, finalPhone, verifyCode);
                    }
                } else {
                    registerEvent();
                }
            }

            @Override
            public void rejectPermission() {
                showOpenCameraDialog();
            }
        }, Manifest.permission.CAMERA);
    }


    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateEditFlag();

//                maxLen = 11;
//                if (mCountryCode.equals(Constants.DATA_COUNTRY_CODES[0])) {
//                    maxLen = 11;
//                } else if (mCountryCode.equals(Constants.DATA_COUNTRY_CODES[1])) {
//                    maxLen = 8;
//                } else if (mCountryCode.equals(Constants.DATA_COUNTRY_CODES[2])) {
//                    maxLen = 8;
//                }
//                updateOkBackground(mId_register_next, mEtwd_transfer_phone_num.getEditText().getText().length() >= maxLen);
        }
    };

    private void initWatcher() {
        //mEtwd_transfer_phone_num.getEditText().setFilters(AndroidUtils.getPhoneInputFilter(Constants.DATA_COUNTRY_CODES[1]));
        mEtwd_transfer_phone_num.setLineVisible(false);
        updateOkBackground(mId_register_next, false);
        mEtwd_transfer_phone_num.getEditText().addTextChangedListener(textWatcher);
    }


    /**
     * @param transactionUniqueID 交易号
     */
    private void registerEventAndGetTnxIdRequest(String transactionUniqueID) {
        mPhoneNum = mEtwd_transfer_phone_num.getText().trim();
        mCountryCode = mTv_region_code_str.getText().toString().trim();
        // 2、区号是852的，电话号码长度必须是8位，且首位不能是0 、1、2、3
        if (mCountryCode.equals(Constants.DATA_COUNTRY_CODES[1]) || mCountryCode.equals(Constants.DATA_COUNTRY_CODES_SPACE[1])) {
            if (mPhoneNum.startsWith("0") || mPhoneNum.startsWith("1") || mPhoneNum.startsWith("2") || mPhoneNum.startsWith("3")) {
                showErrorMsgDialog(mContext, getString(R.string.string_invalid_phonenumber));
                return;
            }
        }

        DataCollectManager.getInstance().sendInputPhoneNumber();
        ApiProtocolImplManager.getInstance().loadIdvCerts(STwoRegisterByEmailAndPhoneActiviy.this, new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(STwoRegisterByEmailAndPhoneActiviy.this, errorMsg);
                sendErrorEvent(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(Object response) {
                DataCollectManager.getInstance().sendEventIdvBefore();
                String transactionType = "";
                if (mCurrentType == Constants.PAGE_FLOW_FORGETPASSWORD) {
                    transactionType = TRANSACTION_TYPE_RESET;
                } else {
                    transactionType = TRANSACTION_TYPE_REGISTER;
                }

                isNeedRefreshCode = true;
                baseMainBackgroundUri = SpUtils.getInstance().getBaseBackground();
                IdvTransitionActivity.startActivityForResult(
                        STwoRegisterByEmailAndPhoneActiviy.this,
                        transactionUniqueID,
                        transactionType,
                        AndroidUtils.getIdvLanguage(STwoRegisterByEmailAndPhoneActiviy.this),
                        PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME,
                        TempSaveHelper.getRegisterRecordId(),
                        BuildConfig.VERSION_NAME,
                        REQUEST_IDV_SDK,
                        baseMainBackgroundUri,
                        Constants.BASEMAINBACKGROUNDRESID
                );
            }
        });

    }

    /**
     * idv sdk 需要获取transactionUniqueID
     */
    private void getTnxIdRequest() {
        String phone = (mCountryCode.replace("+", "") + "-" + mPhoneNum).trim();
        ApiProtocolImplManager.getInstance().getTnxId(this, phone, new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);

                AndroidUtils.showTipDialog(STwoRegisterByEmailAndPhoneActiviy.this, errorMsg);
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                mTransactionUniqueID = response.getTransactionUniqueID();
                DataCollectManager.getInstance().sendInputPhoneNumber();
                ApiProtocolImplManager.getInstance().loadIdvCerts(STwoRegisterByEmailAndPhoneActiviy.this, new NetWorkCallbackListener() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        showErrorMsgDialog(STwoRegisterByEmailAndPhoneActiviy.this, errorMsg);
                        sendErrorEvent(errorCode, errorMsg);
                    }

                    @Override
                    public void onSuccess(Object response) {
//                        if (BuildConfig.DOWNLOAD_CER_SERVER) {
                        DataCollectManager.getInstance().sendEventIdvBefore();
                        String transactionType = "";
                        if (mCurrentType == Constants.PAGE_FLOW_FORGETPASSWORD) {
                            transactionType = TRANSACTION_TYPE_RESET;
                        } else {
                            transactionType = TRANSACTION_TYPE_REGISTER;
                        }

                        baseMainBackgroundUri = SpUtils.getInstance().getBaseBackground();
                        IdvTransitionActivity.startActivityForResult(
                                STwoRegisterByEmailAndPhoneActiviy.this,
                                mTransactionUniqueID,
                                transactionType,
                                AndroidUtils.getIdvLanguage(STwoRegisterByEmailAndPhoneActiviy.this),
                                PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME,
                                TempSaveHelper.getRegisterRecordId(),
                                BuildConfig.VERSION_NAME,
                                REQUEST_IDV_SDK,
                                baseMainBackgroundUri,
                                Constants.BASEMAINBACKGROUNDRESID
                        );
//                        } else {
//                            HashMap<String, Object> mHashMaps = new HashMap<>();
//                            mHashMaps.put(IdvConstants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
//                            mHashMaps.put(IdvConstants.IDV_LANGUAGE, AndroidUtils.getIdvLanguage(STwoRegisterByEmailAndPhoneActiviy.this));
//                            if (mCurrentType == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                                mHashMaps.put(IdvConstants.TRANSACTIONTYPE, TRANSACTION_TYPE_RESET);
//                            } else {
//                                mHashMaps.put(IdvConstants.TRANSACTIONTYPE, TRANSACTION_TYPE_REGISTER);
//                            }
//                            ActivitySkipUtil.startAnotherActivityForResult(STwoRegisterByEmailAndPhoneActiviy.this, IdvTransitionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_IDV_SDK);
//                        }
                    }
                });
            }
        });
    }


    /**
     * 用户权限弹框选择关闭之后 提示打开权限
     */
    private void showOpenCameraDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(STwoRegisterByEmailAndPhoneActiviy.this);
        builder.setTitle(getString(R.string.string_camera_use));
        builder.setMessage(getString(R.string.string_turn_on_camera));

        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(STwoRegisterByEmailAndPhoneActiviy.this);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        if (!isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();
        }

    }


    private void registerEvent() {
        mPhoneNum = mEtwd_transfer_phone_num.getText().trim();
        mCountryCode = mTv_region_code_str.getText().toString().trim();
        // 2、区号是852的，电话号码长度必须是8位，且首位不能是0 、1、2、3
        if (mCountryCode.equals(Constants.DATA_COUNTRY_CODES[1]) || mCountryCode.equals(Constants.DATA_COUNTRY_CODES_SPACE[1])) {
            if (mPhoneNum.startsWith("0") || mPhoneNum.startsWith("1") || mPhoneNum.startsWith("2") || mPhoneNum.startsWith("3")) {
                showErrorMsgDialog(mContext, getString(R.string.string_invalid_phonenumber));
                return;
            }
        }
        getTnxIdRequest();
    }

    private void goIdAuthInfoPage(String seqNumber, String filled) {
        isAnalysis = false;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentType);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, mPhoneNum);
        mHashMaps.put(Constants.EXTRA_COUNTRY_CODE, mCountryCode);
        mHashMaps.put(Constants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
        mHashMaps.put(Constants.MIDVSEQNUMBER, seqNumber);
        mHashMaps.put(Constants.MIDVSEQFID, filled);
        ActivitySkipUtil.startAnotherActivity(getActivity(), IdAuthInfoFillActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA || mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.INPUT_REGISTRATION_MOBILE);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    /**
     * 切换国家码
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SEL_COUNTRY) {
                mCountryCode = data.getExtras().getString(Constants.DATA_COUNTRY_CODE);
                mTv_region_code_str.setText(mCountryCode);
                updateEditFlag();
                //mEtwd_transfer_phone_num.getEditText().setText("");
//                mEtwd_transfer_phone_num.getEditText().setFilters(AndroidUtils.getPhoneInputFilterForRegister(mCountryCode));
            } else if (requestCode == REQUEST_IDV_SDK) {
                mIdvFid = data.getExtras().getString(IdvConstants.IDVFID);
                mIdvSequnumber = data.getExtras().getString(IdvConstants.IDVSEQNUM);
                LogUtils.i("IDV", "mIdvFid: " + mIdvFid + " mIdvSequnumber:" + mIdvSequnumber + "transId :" + mTransactionUniqueID);
                //dialog 消失瞬间 还没有完全跳转到activity 防止多次点击
                goIdAuthInfoPage(mIdvSequnumber, mIdvFid);
            }
        } else {
            if (requestCode == REQUEST_IDV_SDK) {
                //TODO --- zhaolizheng跳过idv
                //if (BuildConfig.SKIP_IDV)
                if (AppTestManager.getInstance().isSkipIdvFrp()) {
                    mIdvSequnumber = "dafffffffffff";
                    mIdvFid = "dafffffffffff";
                    goIdAuthInfoPage(mIdvSequnumber, mIdvFid);
                } else {
                    String errorCode = data.getExtras().getString(IdvConstants.ERROR_CODE);
                    String errorMsg = AndroidUtils.getIdvErrorMessage(errorCode, STwoRegisterByEmailAndPhoneActiviy.this);
                    sendErrorEvent(errorCode, errorMsg);
                    //sdk报错上传埋点
                    AndroidUtils.showIdvCodeEvent(errorCode, STwoRegisterByEmailAndPhoneActiviy.this, new AndroidUtils.ConfirmErrorCodeClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
//                            if (TextUtils.equals(errorCode,"DJ002")){
//                                //idv 500s超时
//                                finish();
//                            }

                        }

                        @Override
                        public void onConfirmBtnIngoreErrorCodeClick() {

                        }
                    });
                }
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_s2_register;
    }

    /**
     * 获取验证码成功
     *
     * @param response 返回的验证码
     */
    @Override
    public void getPARegisterVerifyCodeSuccess(PARegisterVerifyCodeEntity response) {
        mIv_refresh_code.setEnabled(true);
        int codeLen = 8;
        try {
            codeLen = Integer.valueOf(response.getVerifyCodeLength());
        } catch (Exception e) {
        }
        try {
            String bmpCode = response.getImageCode();
            Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
            mIv_verify_code.setImageBitmap(bmp);
            mCet_verify_code.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
            mCet_verify_code.setContentText("");
        } catch (Exception e) {
        }
        //dialog 隐藏之后 软键盘需要弹出
    }

    /**
     * 获取验证码失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getPARegisterVerifyCodeFailed(String errorCode, String errorMsg) {
        mIv_refresh_code.setEnabled(true);
        showErrorMsgDialog(STwoRegisterByEmailAndPhoneActiviy.this, errorMsg);
    }

    /**
     * 检查验证码成功
     *
     * @param response 返回的交易号
     */
    @Override
    public void checkPARegisterVerifyCodeSuccess(PARegisterVerifyCodeCheckEntity response) {
        mTransactionUniqueID = response.getTransactionUniqueID();
        registerEventAndGetTnxIdRequest(response.getTransactionUniqueID());
    }

    /**
     * 检查验证码失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void checkPARegisterVerifyCodeFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(STwoRegisterByEmailAndPhoneActiviy.this, errorMsg);
        //重新获取验证码
        if (NetworkUtil.isNetworkAvailable()) {
            getVerifyCode();
        }
    }
}