package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.entity.FpsConfirmRequest;

/**
 * Created by ramon on 2018/10/10.
 */

public class FpsConfirmProtocol extends BaseProtocol {

    FpsConfirmRequest mConfirmReq;
    private String mDelType;

    public FpsConfirmProtocol(FpsConfirmRequest request, NetWorkCallbackListener dataCallback) {
        mConfirmReq = request;
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/operationFPSConfirm";
    }

    public FpsConfirmProtocol(FpsConfirmRequest request, String delType, NetWorkCallbackListener dataCallback) {
        this(request, dataCallback);
        mDelType = delType;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID, mConfirmReq.accountId);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID_TYPE, mConfirmReq.accountIdType);
        mBodyParams.put(FpsConst.FPS_DEFAULT_BANK, mConfirmReq.defaultBank);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_NO, mConfirmReq.accountNo);
        mBodyParams.put(FpsConst.FPS_BANK_CODE, mConfirmReq.bankCode);
        mBodyParams.put(FpsConst.FPS_ACTION, mConfirmReq.action);
        mBodyParams.put(FpsConst.FPS_REF_NO, mConfirmReq.fppRefNo);
        if (!TextUtils.isEmpty(mDelType)) {
            mBodyParams.put(FpsConst.FPS_RECORD_DEL_TYPE, mDelType);
        }
    }

}
