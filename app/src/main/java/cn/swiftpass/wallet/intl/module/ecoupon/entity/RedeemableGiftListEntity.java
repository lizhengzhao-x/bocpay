package cn.swiftpass.wallet.intl.module.ecoupon.entity;


import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class RedeemableGiftListEntity extends BaseEntity {


    /**
     * giftTp : M
     * buMaps : [{"aswItemCode":"77770","aswBu":"屈臣氏1","giftCode":"66660"},{"aswItemCode":"77771","aswBu":"屈臣氏1","giftCode":"66661"},{"aswItemCode":"77772","aswBu":"屈臣氏1","giftCode":"66662"},{"aswItemCode":"77773","aswBu":"屈臣氏1","giftCode":"66663"}]
     * showDscGiftPnt : Y
     * eVoucherList : [{"aswItemCode":"88880","couponImgLarge":"CouponImgLarge","couponImgSmall":"CouponImgSmall","eVoucherBu":"屈臣氏1","eVoucherCost":"voucherCost0","eVoucherDesc":"屈臣氏0的1000电子券","eVoucherOriginalPrice":"10000","eVoucherTc":"屈臣氏0的1000电子券使用细则","eVoucherUnitPrice":"8000","giftCode":"66660","giftImgUrl":"GiftImgUrl","merchantIconUrl":"MerchantIconUrl","merchantName":"屈臣氏1"},{"aswItemCode":"88881","couponImgLarge":"CouponImgLarge","couponImgSmall":"CouponImgSmall","eVoucherBu":"屈臣氏1","eVoucherCost":"voucherCost1","eVoucherDesc":"屈臣氏1的1001电子券","eVoucherOriginalPrice":"10000","eVoucherTc":"屈臣氏1的1001电子券使用细则","eVoucherUnitPrice":"8000","giftCode":"66661","giftImgUrl":"GiftImgUrl","merchantIconUrl":"MerchantIconUrl","merchantName":"屈臣氏1"},{"aswItemCode":"88882","couponImgLarge":"CouponImgLarge","couponImgSmall":"CouponImgSmall","eVoucherBu":"屈臣氏1","eVoucherCost":"voucherCost2","eVoucherDesc":"屈臣氏2的1002电子券","eVoucherOriginalPrice":"10000","eVoucherTc":"屈臣氏2的1002电子券使用细则","eVoucherUnitPrice":"8000","giftCode":"66662","giftImgUrl":"GiftImgUrl","merchantIconUrl":"MerchantIconUrl","merchantName":"屈臣氏1"},{"aswItemCode":"88883","couponImgLarge":"CouponImgLarge","couponImgSmall":"CouponImgSmall","eVoucherBu":"屈臣氏1","eVoucherCost":"voucherCost3","eVoucherDesc":"屈臣氏3的1003电子券","eVoucherOriginalPrice":"10000","eVoucherTc":"屈臣氏3的1003电子券使用细则","eVoucherUnitPrice":"8000","giftCode":"66663","giftImgUrl":"GiftImgUrl","merchantIconUrl":"MerchantIconUrl","merchantName":"屈臣氏1"}]
     */

    private String giftTp;
    private String showDscGiftPnt;
    private List<BuMapsBean> buMaps;

    public ConfigBean getConfigInfo() {
        return configInfo;
    }

    public void setConfigInfo(ConfigBean configInfo) {
        this.configInfo = configInfo;
    }

    private ConfigBean configInfo;
    private List<EVoucherListBean> eVoucherList;

    public String getGiftTp() {
        return giftTp;
    }

    public void setGiftTp(String giftTp) {
        this.giftTp = giftTp;
    }

    public boolean isGiftPnt() {
        if (TextUtils.isEmpty(showDscGiftPnt)) return false;
        return showDscGiftPnt.equals("Y");
    }

    public void setShowDscGiftPnt(String showDscGiftPnt) {
        this.showDscGiftPnt = showDscGiftPnt;
    }

    public List<BuMapsBean> getBuMaps() {
        return buMaps;
    }

    public void setBuMaps(List<BuMapsBean> buMaps) {
        this.buMaps = buMaps;
    }

    public List<EVoucherListBean> getEVoucherList() {
        return eVoucherList;
    }

    public void setEVoucherList(List<EVoucherListBean> eVoucherList) {
        this.eVoucherList = eVoucherList;
    }

    public static class ConfigBean extends BaseEntity {

        private String title;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        private String imageUrl;
        private String content;
    }

    public static class BuMapsBean extends BaseEntity {
        public String getBuName() {
            return buName;
        }

        public void setBuName(String buName) {
            this.buName = buName;
        }

        /**
         * aswItemCode : 77770
         * aswBu : 屈臣氏1
         * giftCode : 66660
         */

        private String buName;
        private String aswBu;

        public String getBuTc() {
            return buTc;
        }

        public void setBuTc(String buTc) {
            this.buTc = buTc;
        }

        private String buTc;


        public boolean isSel() {
            return isSel;
        }

        public void setSel(boolean sel) {
            isSel = sel;
        }

        private boolean isSel;


        public String getAswBu() {
            return aswBu;
        }

        public void setAswBu(String aswBu) {
            this.aswBu = aswBu;
        }


    }

    public static class EVoucherListBean extends BaseEntity {
        public String geteVoucherStock() {
            return eVoucherStock;
        }

        public void seteVoucherStock(String eVoucherStock) {
            this.eVoucherStock = eVoucherStock;
        }

        /**
         * aswItemCode : 88880
         * couponImgLarge : CouponImgLarge
         * couponImgSmall : CouponImgSmall
         * eVoucherBu : 屈臣氏1
         * eVoucherCost : voucherCost0
         * eVoucherDesc : 屈臣氏0的1000电子券
         * eVoucherOriginalPrice : 10000
         * eVoucherTc : 屈臣氏0的1000电子券使用细则
         * eVoucherUnitPrice : 8000
         * giftCode : 66660
         * giftImgUrl : GiftImgUrl
         * merchantIconUrl : MerchantIconUrl
         * merchantName : 屈臣氏1
         */
        private String eVoucherStock;
        private String aswItemCode;
        private String couponImgLarge;
        private String couponImgSmall;
        private String eVoucherBu;
        private String eVoucherCost;
        private String eVoucherDesc;
        private String eVoucherOriginalPrice;
        private String eVoucherTc;
        private String eVoucherUnitPrice;
        private String giftCode;
        private String giftImgUrl;
        private String merchantIconUrl;
        private String merchantName;


        public String getOrderTp() {
            return orderTp;
        }

        public void setOrderTp(String orderTp) {
            this.orderTp = orderTp;
        }

        private String orderTp;

        public String geteVoucherBuCode() {
            return eVoucherBuCode;
        }

        public void seteVoucherBuCode(String eVoucherBuCode) {
            this.eVoucherBuCode = eVoucherBuCode;
        }

        private String eVoucherBuCode;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        private String type;

        public int getCurrentSelCnt() {
            return currentSelCnt;
        }

        public void setCurrentSelCnt(int currentSelCnt) {
            this.currentSelCnt = currentSelCnt;
        }

        private int currentSelCnt;

        public String getAswItemCode() {
            return aswItemCode;
        }

        public void setAswItemCode(String aswItemCode) {
            this.aswItemCode = aswItemCode;
        }

        public String getCouponImgLarge() {
            return couponImgLarge;
        }

        public void setCouponImgLarge(String couponImgLarge) {
            this.couponImgLarge = couponImgLarge;
        }

        public String getCouponImgSmall() {
            return couponImgSmall;
        }

        public void setCouponImgSmall(String couponImgSmall) {
            this.couponImgSmall = couponImgSmall;
        }

        public String getEVoucherBu() {
            return eVoucherBu;
        }

        public void setEVoucherBu(String eVoucherBu) {
            this.eVoucherBu = eVoucherBu;
        }

        public String getEVoucherCost() {
            return eVoucherCost;
        }

        public void setEVoucherCost(String eVoucherCost) {
            this.eVoucherCost = eVoucherCost;
        }

        public String getEVoucherDesc() {
            return eVoucherDesc;
        }

        public void setEVoucherDesc(String eVoucherDesc) {
            this.eVoucherDesc = eVoucherDesc;
        }

        public String getEVoucherOriginalPrice() {
            return eVoucherOriginalPrice;
        }

        public void setEVoucherOriginalPrice(String eVoucherOriginalPrice) {
            this.eVoucherOriginalPrice = eVoucherOriginalPrice;
        }

        public String getEVoucherTc() {
            return eVoucherTc;
        }

        public void setEVoucherTc(String eVoucherTc) {
            this.eVoucherTc = eVoucherTc;
        }

        public String getEVoucherUnitPrice() {
            return eVoucherUnitPrice;
        }

        public void setEVoucherUnitPrice(String eVoucherUnitPrice) {
            this.eVoucherUnitPrice = eVoucherUnitPrice;
        }

        public String getGiftCode() {
            return giftCode;
        }

        public void setGiftCode(String giftCode) {
            this.giftCode = giftCode;
        }

        public String getGiftImgUrl() {
            return giftImgUrl;
        }

        public void setGiftImgUrl(String giftImgUrl) {
            this.giftImgUrl = giftImgUrl;
        }

        public String getMerchantIconUrl() {
            return merchantIconUrl;
        }

        public void setMerchantIconUrl(String merchantIconUrl) {
            this.merchantIconUrl = merchantIconUrl;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }
    }
}
