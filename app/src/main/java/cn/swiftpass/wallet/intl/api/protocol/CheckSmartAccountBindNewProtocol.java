package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/19
 * 校验是否已绑定我的账户
 */

public class CheckSmartAccountBindNewProtocol extends BaseProtocol {
    public static final String TAG = CheckSmartAccountBindNewProtocol.class.getSimpleName();

    public CheckSmartAccountBindNewProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/hasBindSmartAccountOrStatus";
    }
}
