package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.tradelink.boc.authapp.exception.FidoAppException;
import com.tradelink.boc.authapp.utils.AuthCommonUtils;
import com.tradelink.boc.authapp.utils.FidoOperationUtils;
import com.tradelink.boc.sotp.utils.SoftTokenOperationUtils;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FioConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FioCheckValidEntity;
import cn.swiftpass.wallet.intl.entity.FioInitEntity;
import cn.swiftpass.wallet.intl.entity.FioRegRequestEntity;
import cn.swiftpass.wallet.intl.entity.FioRegResponseEntity;
import cn.swiftpass.wallet.intl.entity.FioVerifyOtpEntity;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * @author Created  on 2018/9/3.
 */

public class FIOActivity extends BaseCompatActivity {

    public static final String TAG = FIOActivity.class.getSimpleName();

    //F,P,B
    public static final String FIO_AUTH_TYPE_FINGER_PRINT = "F";

    //The usage code of one-time password. It can be "S" for security only
    // (no transaction data is required) or "T" for transaction confirmation (transaction data is required)
    public static final String USAGE_CODE = "S";

    public static final String FIO_APP_ID = BuildConfig.APPLICATION_ID;

    FioInitEntity mFioInitEntity;

    String mWalletId;

    String mRegType;

    String mAction;
    boolean mIsEcouponAction;

    private int mTypeFlag;
    private String mAccountType = Constants.ACCOUNT_TYPE_CREDIT;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        if (null == getIntent()) {
            finish();
        } else {
            parseIntent(getIntent());
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }


    private void parseIntent(Intent intent) {
        mAction = intent.getStringExtra(FioConst.FIO_ACTION);
        mTypeFlag = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        mAccountType = getIntent().getExtras().getString(Constants.ACCOUNT_TYPE);

        //电子券 单独添加这个参数
        mIsEcouponAction = intent.getBooleanExtra(FioConst.FIO_ACTION_IS_ECOUPON, false);
        ApiProtocolImplManager.getInstance().showDialogNotCancel(this);
        if (TextUtils.equals(mAction, FioConst.ACTION_CHECK_POLICY)) {
            checkPolicy();
        } else if (TextUtils.equals(mAction, FioConst.ACTION_REG_FIO)) {
            mWalletId = intent.getStringExtra(FioConst.FIO_WALLETID);
            mRegType = intent.getStringExtra(FioConst.FIO_REG_TYPE);
            regFingerPrint();
        } else if (TextUtils.equals(mAction, FioConst.ACTION_AUTH_FIO)) {
            authFingerPrint();
        } else if (TextUtils.equals(mAction, FioConst.ACTION_DE_REG_FIO)) {
            deRegisterFingerPrint();
        } else if (TextUtils.equals(mAction, FioConst.ACTION_CHECK_VALID_FIO)) {
            checkValidFingerPrint();
        } else {
            ApiProtocolImplManager.getInstance().dismissDialog();
            finish();
        }

    }

    private void fioInitReg() {
        try {
            SoftTokenOperationUtils.initRegistration(mFioInitEntity.getAsKey(), mFioInitEntity.getChannel(), "", FIO_AUTH_TYPE_FINGER_PRINT, this);

//            SoftTokenOperationUtils.initRegistration(mFioInitEntity.getAsKey(),mFioInitEntity.getChannel(),
//                    (CoreApplication.getContext() != null? AuthCommonUtils.getFioKey(): ""), FIO_AUTH_TYPE_FINGER_PRINT, this);
        } catch (Exception e) {
            onRegFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    private void fioRegStepOne(String fioRegRequestResponse) {
        try {
            //REQ_CODE_REGISTRATION
            SoftTokenOperationUtils.registration(mFioInitEntity.getAsKey(), mFioInitEntity.getChannel(), getDeviceToken(), FIO_APP_ID, fioRegRequestResponse, FIOActivity.this);
        } catch (Exception e) {
            onRegFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }

    }

    private void fioRegStepTwo(String fioServerResponse) {
        try {
            SoftTokenOperationUtils.postRegistration(fioServerResponse);
            onRegFingerPrintResult();
        } catch (FidoAppException e) {
            //AndroidUtils.showTipDialog(this,e.getError().getMessage());
            onRegFingerPrintResultFailed();
        } catch (Exception e) {
            onRegFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    //01
    private void getAskey() {
        ApiProtocolImplManager.getInstance().fioInitCin(this, mWalletId, mRegType, new NetWorkCallbackListener<FioInitEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //AndroidUtils.showTipDialog(FIOActivity.this,errorMsg);
                onRegFingerPrintResultFailed();
            }

            @Override
            public void onSuccess(FioInitEntity response) {
                mFioInitEntity = response;
                fioInitReg();
            }
        });
    }

    private void serverRegRequest(String req) {
        ApiProtocolImplManager.getInstance().fioRegistrationReq(this, mWalletId, req, mRegType, new NetWorkCallbackListener<FioRegRequestEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onRegFingerPrintResultFailed();
                //AndroidUtils.showTipDialog(FIOActivity.this,errorMsg);
            }

            @Override
            public void onSuccess(FioRegRequestEntity response) {
                String fioRegRequestResponse = response.getFioServerResponse();
                fioRegStepOne(fioRegRequestResponse);
            }
        });
    }


    private void serverRegResponse(String resp) {
        ApiProtocolImplManager.getInstance().fioRegistrationResp(this, mWalletId, resp, mRegType, new NetWorkCallbackListener<FioRegResponseEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onRegFingerPrintResultFailed();
                //AndroidUtils.showTipDialog(FIOActivity.this,errorMsg);
            }

            @Override
            public void onSuccess(FioRegResponseEntity response) {
                String fioServerResponse = response.getFioServerResponse();
                fioRegStepTwo(fioServerResponse);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SoftTokenOperationUtils.SOTP_REQ_CODE_PRE_REGISTRATION:
                    String preRegRequestString = data.getStringExtra("response");
                    serverRegRequest(preRegRequestString);
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_REGISTRATION:
                    //Toast.makeText(this, "Registration response returned. Please wait....", Toast.LENGTH_LONG).show();
                    LogUtils.i(TAG, "Registration response returned. Please wait...");
                    String sotpRegResponseString = data.getStringExtra("response");
                    serverRegResponse(sotpRegResponseString);
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_AUTHENTICATION:
                    //验证返回otp
                    boolean sotpHasTransaction = data.getBooleanExtra("hasTransaction", false);
                    if (!sotpHasTransaction) {
                        //Toast.makeText(this, "No Pending Transaction", Toast.LENGTH_LONG).show();
                        LogUtils.i(TAG, "No Pending Transaction");
                        onAuthFingerPrintResultFailed();
                    } else {
                        //String otp = data.getExtras().getString("otp", "123456");
                        //Toast.makeText(this, "OTP " + otp, Toast.LENGTH_LONG).show();
                        String otp = data.getExtras().getString("otp", "");
                        if (TextUtils.isEmpty(otp)) {
                            onAuthFingerPrintResultFailed();
                        } else {
                            serverAuthOtp(otp);
                        }
                    }
                    break;
//                //没用到
//                case SoftTokenOperationUtils.SOTP_REQ_CODE_PRE_AUTHENTICATION:
//                    boolean sotpHasPreTransaction = data.getBooleanExtra("hasTransaction", false);
//                    if (!sotpHasPreTransaction) {
//                        Toast.makeText(this, "No Pending Transaction", Toast.LENGTH_LONG).show();
//                    } else {
//                        String otp = data.getExtras().getString("otp", "123456");
//                        Toast.makeText(this, "OTP " + otp, Toast.LENGTH_LONG).show();
//                    }
//                    break;
                //没用到
                case SoftTokenOperationUtils.SOTP_REQ_CODE_DEREGISTRATION:
                    //Toast.makeText(this, "Deregistration Success", Toast.LENGTH_LONG).show();
                    onDeregisterFingerPrintResult();
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_CHECK_POLICY:
                    onPolicyResult(data,-1);
                    break;
//                //没用到
//                case FidoOperationUtils.DEFAULT_SOTP_REQ_CODE_GET_FIO_KEY:
//                    String response = data.getStringExtra("response");
//                    Toast.makeText(this, response, Toast.LENGTH_LONG).show();
//                    break;
                default:
                    break;
            }
        } else {
//            Error error = (Error) data.getSerializableExtra("error");
//            Toast.makeText(this, "Error: " + error.getCode() + "-" + error.getMessage(), Toast.LENGTH_LONG).show();
//            if (data.hasExtra("hasFingerprint")) {
//                Toast.makeText(this, "hasFingerprint: " + data.getBooleanExtra("hasFingerprint", false), Toast.LENGTH_LONG).show();
//            }
            switch (requestCode) {
                case SoftTokenOperationUtils.SOTP_REQ_CODE_PRE_REGISTRATION:
                    onRegFingerPrintResultFailed();
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_REGISTRATION:
                    onRegFingerPrintResultFailed();
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_AUTHENTICATION:
                    onAuthFingerPrintResultFailed();
                    break;
                //没用到
                case SoftTokenOperationUtils.SOTP_REQ_CODE_DEREGISTRATION:
                    onDeregisterFingerPrintResult();
                    break;
                case SoftTokenOperationUtils.SOTP_REQ_CODE_CHECK_POLICY:
                    onPolicyResult(data,resultCode);
                    break;
                default:
                    setResultCancel(data);
                    break;
            }
        }
    }

    private String getDeviceToken() {
        return "dummyDeviceToken";
    }

    public String getFioId() {
        return AuthCommonUtils.getFioKey();
    }

    public boolean isRegisterFio() {
        return SoftTokenOperationUtils.isRegistered(this);
    }

    private void serverAuthFioId(String fioId) {
        ApiProtocolImplManager.getInstance().fioCheckValidId(this, fioId, new NetWorkCallbackListener<FioCheckValidEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //unRegFingerPrint(FIOActivity.this);
                onAuthFingerPrintResultFailed();
            }

            @Override
            public void onSuccess(FioCheckValidEntity response) {
                String data = response.getTxnData();
                authFio(data);
            }
        });
    }

    private void authFio(String txndata) {
        try {
            SoftTokenOperationUtils.authentication(getDeviceToken(), txndata, FIO_AUTH_TYPE_FINGER_PRINT, USAGE_CODE, false, true, false, "01", "", "",this);
        } catch (Exception e) {
            onAuthFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    private void serverAuthOtp(String otp) {
        ApiProtocolImplManager.getInstance().fioVerifyOtp(this, otp, mIsEcouponAction, new NetWorkCallbackListener<FioVerifyOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //AndroidUtils.showTipDialog(FIOActivity.this,errorMsg);
                onAuthFingerPrintResultFailed();
            }

            @Override
            public void onSuccess(FioVerifyOtpEntity response) {
                //AndroidUtils.showTipDialog(FIOActivity.this,response.getFailCnt());
                onAuthFingerPrintResult();
            }
        });
    }

    private void checkPolicy() {
        try {
            SoftTokenOperationUtils.checkPolicy(this);
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    private void regFingerPrint() {
        getAskey();
    }

    private void authFingerPrint() {
        try {
            if (isRegisterFio()) {
                serverAuthFioId(getFioId());
            } else {
                onAuthFingerPrintResultFailed();
            }
        } catch (Exception e) {
            onAuthFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    private void deRegisterFingerPrint() {
        try {
            SoftTokenOperationUtils.deregistration(this);
        } catch (Exception e) {
            onAuthFingerPrintResultFailed();
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

//    public void unRegFingerPrint(Activity activity) {
//        try {
//            if (isRegisterFio()) {
//                SoftTokenOperationUtils.deregistration(activity);
//            }
//        } catch (Exception e) {
//            LogUtils.e(TAG, Log.getStackTraceString(e));
//        }

//    }

    public static void startCheckPolicy(Activity activity, int mTypeFlag, String accountType) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        intent.putExtra(Constants.ACCOUNT_TYPE, accountType);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_CHECK_POLICY);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_CHECK_POLICY);
        activity.overridePendingTransition(0, 0);
    }

    //对外检测支持指纹
    public static void startCheckPolicy(Activity activity) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_CHECK_POLICY);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_CHECK_POLICY);
        activity.overridePendingTransition(0, 0);
    }

    private void onPolicyResult(Intent data,int resultCode) {
        ApiProtocolImplManager.getInstance().dismissDialog();
        boolean hasPolicyAuthenticators = data.getBooleanExtra("response", false);
        boolean hasFingerprintHardware = data.getBooleanExtra("hasFingerprint", false);
        if (TextUtils.equals(mAction, FioConst.ACTION_CHECK_VALID_FIO)) {
            if (hasPolicyAuthenticators && hasFingerprintHardware) {
                onCheckValidFingerPrintPolicy(true);
            } else {
                onCheckValidFingerPrintPolicy(false);
            }
        } else {
            Intent intent = new Intent();
            intent.putExtra(FioConst.HAS_FINGERPRINT_ENROLLED, hasPolicyAuthenticators);
            intent.putExtra(FioConst.HAS_FINGERPRINT, hasFingerprintHardware);
            intent.putExtra(FioConst.ACTION_CHECK_POLICY, true);
            intent.putExtra(FioConst.ACTION_CHECK_FIO_CODE, resultCode);
            setResultOK(intent);
        }
    }


    //注册boc pay时注册指纹
    public static void startRegFingerPrint(Activity activity, String walleId) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_REG_FIO);
        intent.putExtra(FioConst.FIO_WALLETID, walleId);
        intent.putExtra(FioConst.FIO_REG_TYPE, FioConst.FIO_REG_TYPE_R);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_REG_FIO);
        activity.overridePendingTransition(0, 0);
    }

    //设置中时注册指纹
    public static void startSetFingerPrint(Activity activity, String walleId) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_REG_FIO);
        intent.putExtra(FioConst.FIO_WALLETID, walleId);
        intent.putExtra(FioConst.FIO_REG_TYPE, FioConst.FIO_REG_TYPE_S);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_REG_FIO);
        activity.overridePendingTransition(0, 0);
    }

    private void onRegFingerPrintResult() {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_REG_FIO, true);
        setResultOK(intent);
    }

    private void onRegFingerPrintResultFailed() {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_REG_FIO, false);
        intent.putExtra(FioConst.FIO_ACTION_RESULT_MSG, getString(ErrorCode.FIO_REG_FAILED.msgId));
        setResultOK(intent);
    }


    //对外认证指纹
    public static void startAuthFingerPrint(Activity activity) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_AUTH_FIO);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_AUTH_FIO);
        activity.overridePendingTransition(0, 0);
    }

    //对外认证指纹
    public static void startAuthFingerPrintForEcoupon(Activity activity) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_AUTH_FIO);
        intent.putExtra(FioConst.FIO_ACTION_IS_ECOUPON, true);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_AUTH_FIO);
        activity.overridePendingTransition(0, 0);
    }

    private void onAuthFingerPrintResult() {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_AUTH_FIO, true);
        setResultOK(intent);
    }

    private void onAuthFingerPrintResultFailed() {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_AUTH_FIO, false);
        intent.putExtra(FioConst.FIO_ACTION_RESULT_MSG, getString(ErrorCode.FIO_AUTH_FAILED.msgId));
        setResultOK(intent);
    }

    private void onDeregisterFingerPrintResult() {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_DE_REG_FIO, true);
        setResultOK(intent);
    }


    //对外注册支持指纹
    public static void startDegregisterFingerPrint(Activity activity) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_DE_REG_FIO);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_DE_REG_FIO);
        activity.overridePendingTransition(0, 0);
    }

    public static void startCheckValidFingerPrint(Activity activity) {
        Intent intent = new Intent(activity, FIOActivity.class);
        intent.putExtra(FioConst.FIO_ACTION, FioConst.ACTION_CHECK_VALID_FIO);
        activity.startActivityForResult(intent, FioConst.REQUEST_CODE_CHECK_VALID_FIO);
        activity.overridePendingTransition(0, 0);
    }

    //
    private void checkValidFingerPrint() {
        boolean bReg = SoftTokenOperationUtils.isRegistered(this);
        boolean bChange = FidoOperationUtils.isFingerPrintChanged(this);
        //没注册或者指纹发生变化，指纹就不可用
        if (!bReg || bChange) {
            onCheckValidFingerPrintResult(false);
        } else {
            checkPolicy();
        }
    }

    private void onCheckValidFingerPrintPolicy(boolean bCheckPolicy) {
        if (bCheckPolicy) {
            ApiProtocolImplManager.getInstance().fioCheckValidId(this, AuthCommonUtils.getFioKey(), new NetWorkCallbackListener<FioCheckValidEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    onCheckValidFingerPrintResult(false);
                }

                @Override
                public void onSuccess(FioCheckValidEntity response) {
                    onCheckValidFingerPrintResult(true);
                }
            });
        } else {
            onCheckValidFingerPrintResult(false);
        }
    }

    private void onCheckValidFingerPrintResult(boolean result) {
        ApiProtocolImplManager.getInstance().dismissDialog();
        Intent intent = new Intent();
        intent.putExtra(FioConst.ACTION_CHECK_VALID_FIO, result);
        setResultOK(intent);
    }

    private void setResultOK(Intent intent) {
        intent.putExtra(FioConst.FIO_ACTION_RESULT, true);
        this.setResult(RESULT_OK, intent);
        this.finish();
    }

    private void setResultCancel(Intent intent) {
        intent.putExtra(FioConst.FIO_ACTION_RESULT, true);
        this.setResult(RESULT_CANCELED, intent);
        this.finish();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}
