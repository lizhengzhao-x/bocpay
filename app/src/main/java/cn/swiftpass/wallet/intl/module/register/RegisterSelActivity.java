package cn.swiftpass.wallet.intl.module.register;

import static cn.swiftpass.wallet.intl.module.home.PreLoginActivity.DATA_VIRTUAL_CARD_SHARE_URL;
import static cn.swiftpass.wallet.intl.module.home.PreLoginActivity.DATA_VIRTUAL_CARD_URL;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.LoginCheckEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.home.constants.MenuItemEnum;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.login.contract.LoginContract;
import cn.swiftpass.wallet.intl.module.login.presenter.LoginPresenter;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardApplyActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;


public class RegisterSelActivity extends BaseCompatActivity<LoginContract.Presenter> implements LoginContract.View {

    private TextView mId_sub_title;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_have_account;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_have_no_account;
    private ImageView ivLoginBottomBanner;
    private CheckBox checkBoxPWid;
    private String virtualCardUrl;
    private String shareImageUrl;
//    private boolean isAppCallAppEvent;

    private void bindViews() {
        mId_sub_title = (TextView) findViewById(R.id.id_sub_title);
        ivLoginBottomBanner = (ImageView) findViewById(R.id.iv_login_bottom_banner);
        mIav_have_account = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_have_account);
        mIav_have_no_account = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_have_no_account);
        checkBoxPWid = (CheckBox) findViewById(R.id.id_check_box_p_wid);
        mIav_have_account.getTvLeft().setText(getString(R.string.P4_A1_1_1_2));
        mIav_have_no_account.getTvLeft().setText((getString(R.string.P4_A1_1_1_3)));
        mIav_have_account.setRightImageShow(true);
        mIav_have_no_account.setRightImageShow(true);

        mIav_have_account.setLeftImageIsGone(false);
        mIav_have_no_account.setLeftImageIsGone(false);

        mIav_have_account.getLineView().setVisibility(View.GONE);
        mIav_have_no_account.getLineView().setVisibility(View.GONE);


        mIav_have_account.getImageLeft().setImageResource(R.mipmap.icon_tab_boc);
        mIav_have_no_account.getImageLeft().setImageResource(R.mipmap.icon_tab_nonbocuser);

    }

    @Override
    protected LoginContract.Presenter createPresenter() {
        return new LoginPresenter();
    }


    @Override
    public void init() {
        setToolBarTitle(R.string.IDV_2_1);
        if (getIntent() != null) {
            virtualCardUrl = getIntent().getStringExtra(DATA_VIRTUAL_CARD_URL);
            shareImageUrl = getIntent().getStringExtra(DATA_VIRTUAL_CARD_SHARE_URL);
        }
        bindViews();
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_sc);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_tc);
        } else {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_en);
        }
        ivLoginBottomBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick(v.getId(), 500)) {
                    if (!TextUtils.isEmpty(virtualCardUrl)) {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                        mHashMaps.put(WebViewActivity.NEW_MSG, true);
                        mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
                        ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        mPresenter.getInitData(MenuItemEnum.VIRTUAL_CARD.action);
                    }
                }
            }
        });

        mIav_have_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (checkBoxPWid.isChecked()) {
                        //选中pwid时，显示拒纳信息
                        showPWidRefuseMsg();
                    } else {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                        ActivitySkipUtil.startAnotherActivity(RegisterSelActivity.this, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });

        mIav_have_no_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (checkBoxPWid.isChecked()) {
                        //选中pwid时，显示拒纳信息
                        showPWidRefuseMsg();
                    } else {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
                        ActivitySkipUtil.startAnotherActivity(RegisterSelActivity.this, RegisterPaymentAccountDescActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }
            }
        });
    }


    /**
     * piwd 智障人士 - 显示拒纳信息
     */
    private void showPWidRefuseMsg() {
        showErrorMsgDialog(getActivity(), getString(R.string.PWID2109_1_2), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_sel;
    }


    @Override
    public void getInitDataFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getInitDataSuccess(SystemPagerDataEntity response, String action) {
        if (response != null) {
            virtualCardUrl = response.getVirtualCardUrl(this);
            shareImageUrl = response.getVirtualCardShareUrl(this);
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
            mHashMaps.put(WebViewActivity.NEW_MSG, true);
            mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
            ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getVerifyCodeFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getVerifyCodeSuccess(BankLoginVerifyCodeEntity codeEntity) {

    }

    @Override
    public void verifyCodeFail(String errorCode, String errorMsg) {

    }

    @Override
    public void verifyCodeSuccess() {

    }

    @Override
    public void verifyPwdFail(String errorCode, String errorMsg) {

    }

    @Override
    public void verifyPwdSuccess(LoginCheckEntity loginCheckEntity) {

    }

    @Override
    public void forgetPwdDeviceStatusFail(String errorCode, String errorMsg) {

    }

    @Override
    public void forgetPwdDeviceStatusSuccess(ForgetPasswordEntity response) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //从引导页过来到注册 点击back按键应该到Prelogin界面
        if (!MyActivityManager.containPreloginPage()) {
            ActivitySkipUtil.startAnotherActivity(this, PreLoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

    }
}
