package cn.swiftpass.wallet.intl.base.otp;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;

public interface OTPTryPresenter<V extends IView> extends IPresenter<V> {
    void tryOTP(String walletId, String phone, String action);
}
