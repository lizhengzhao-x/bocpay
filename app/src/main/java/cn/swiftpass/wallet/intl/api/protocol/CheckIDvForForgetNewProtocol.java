package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class CheckIDvForForgetNewProtocol extends BaseProtocol {

    public CheckIDvForForgetNewProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        //不同流程 后台区分 ur不同
        mUrl = "api/smartForgot/checkIdvRetry";

    }

}
