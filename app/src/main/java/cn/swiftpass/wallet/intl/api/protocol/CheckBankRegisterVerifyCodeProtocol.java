package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */

public class CheckBankRegisterVerifyCodeProtocol extends BaseProtocol {

    String mVerifyCode;
    String mAction;

    public CheckBankRegisterVerifyCodeProtocol(String verifyCode, String action, NetWorkCallbackListener dataCallback){
        this.mDataCallback = dataCallback;
        this.mAction = action;
        this.mVerifyCode = verifyCode;
        mUrl = "api/register/checkVerifyCode";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
    }

}
