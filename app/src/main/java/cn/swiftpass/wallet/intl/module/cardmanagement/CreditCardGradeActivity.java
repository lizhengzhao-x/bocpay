package cn.swiftpass.wallet.intl.module.cardmanagement;

import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


/**
 * 信用卡积分查询
 */
public class CreditCardGradeActivity extends BaseCompatActivity {
    @BindView(R.id.id_linear_view)
    LinearLayout idLinearView;
    @BindView(R.id.id_card_grade_sub_title)
    TextView tvCardGradeSubTitle;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.CP4_3a_3);
        final BankCardEntity cardEntity = (BankCardEntity) getIntent().getSerializableExtra(Constants.CARD_ENTITY);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idLinearView.getLayoutParams();
        int margin = (int) mContext.getResources().getDimension(R.dimen.space_40_px);
        int width = AndroidUtils.getScreenWidth(mContext) - margin * 2;
        int height = width * 3 / 8;
        lp.width = width;
        lp.height = height;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        idLinearView.setLayoutParams(lp);
        ApiProtocolImplManager.getInstance().getGradeForCreditCard(this, cardEntity.getCardId(), new NetWorkCallbackListener<CreditCardGradeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(CreditCardGradeActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(CreditCardGradeEntity response) {
                try {
                    tvCardGradeSubTitle.setText(AndroidUtils.formatPrice(Double.parseDouble(response.getCreditCardGp()), false));
                } catch (NumberFormatException e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_credit_carditem_detail;
    }


}
