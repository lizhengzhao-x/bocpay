package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class ContactEntity extends BaseEntity {

    public String getSortLetter() {
        return sortLetter;
    }

    public void setSortLetter(String sortLetter) {
        this.sortLetter = sortLetter;
    }

    private String sortLetter;

    public String getUserName() {
        return note;
    }

    public void setUserName(String note) {
        this.note = note;
    }

    private String note;

    public boolean isEmail() {
        return isEmail;
    }

    public void setEmail(boolean email) {
        isEmail = email;
    }

    private boolean isEmail;


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    private String number;

    public boolean isCollect() {
        return isCollect;
    }

    public void setCollect(boolean collect) {
        isCollect = collect;
    }

    /**
     * 是否是收藏状态
     */
    private boolean isCollect;

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    private String contactID;

    public String getUserRelName() {
        return userRelName;
    }

    public void setUserRelName(String userRelName) {
        this.userRelName = userRelName;
    }

    private String userRelName;

}
