package cn.swiftpass.wallet.intl.module.redpacket.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by jamy on 2018/5/22.
 */

public class RedPacketBeanEntity extends BaseEntity {
    private String srcRefNo;  //交易编号
    private String sendAmt;  //派利是金额 1.00
    private String currency;  //币种（默认HKD）
    private String blessingWords;  //祝福语
    private String transferType;  //交易类型 7:fps派利是  8:bocpay派利是
    private String phone;
    private String time;
    private String name;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSrcRefNo() {
        return srcRefNo;
    }

    public void setSrcRefNo(String srcRefNo) {
        this.srcRefNo = srcRefNo;
    }

    public String getSendAmt() {
        return sendAmt;
    }

    public void setSendAmt(String sendAmt) {
        this.sendAmt = sendAmt;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBlessingWords() {
        return blessingWords;
    }

    public void setBlessingWords(String blessingWords) {
        this.blessingWords = blessingWords;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
