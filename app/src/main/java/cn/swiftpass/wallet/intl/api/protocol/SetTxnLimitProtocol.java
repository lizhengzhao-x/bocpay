package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class SetTxnLimitProtocol extends BaseProtocol {

    public static final String TAG = SetTxnLimitProtocol.class.getSimpleName();

    String mDailyTxnLimit;

    public SetTxnLimitProtocol(String type, String dailyTxnLimit, NetWorkCallbackListener dataCallback) {
        this.mDailyTxnLimit = dailyTxnLimit;
        this.mDataCallback = dataCallback;
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/setTxnLimit";
        } else {
            mUrl = "api/smartReg/setTxnLimit";
        }

    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.DAILYTXNLIMIT, mDailyTxnLimit);
    }


}
