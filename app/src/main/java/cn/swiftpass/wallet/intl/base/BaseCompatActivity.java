package cn.swiftpass.wallet.intl.base;

import android.graphics.Outline;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import org.jetbrains.annotations.NotNull;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public abstract class BaseCompatActivity<P extends IPresenter> extends BaseAbstractActivity<P> implements IView {

    protected Toolbar mToolBarBase;
    protected TextView mToolBarTitle;
    protected LinearLayout mToolBarRightll;
    private int totalHeight;
    private Unbinder bind;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.i(TAG, "onCreate>" + "xxxxxxxxxx" + this);
        if (mInflater == null) return;
        //默认使用 SYSTEM_UI_MODE_LIGHT_BAR 保留状态栏，但是设置状态栏为透明
        if (!isFullScreen()) {
            setSystemUiMode(SYSTEM_UI_MODE_LIGHT_BAR);
            AndroidUtils.setStatusBar(this, true);
        } else {
            setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION);
        }
        if (getLayoutId() != 0) {
            configView();
        }
        bind = ButterKnife.bind(this);
        init();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bind != null) {
            bind.unbind();
        }
    }

    /**
     * 改变屏幕亮度
     */
    public void changeScreenBrightness() {
        //设置屏幕亮度最大
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
    }

    /**
     * 改变屏幕亮度
     */
    public void clearScreenBrightness() {
        //取消屏幕最亮
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
    }


    protected abstract void init();


    /**
     * 如果重新了setToolbar方法，必须重新该方法
     *
     * @param toolbarView
     */
    protected void initToolBar(View toolbarView) {
        if (setToolBar() != R.layout.base_tool_bar) {
            throw new IllegalArgumentException("if you override setToolbar ,you must  override initToolbar too");
        }
        mToolBarBase = toolbarView.findViewById(R.id.toolbar_base);
        mToolBarBase.setNavigationIcon(R.drawable.icon_back);
        mToolBarTitle = toolbarView.findViewById(R.id.toolbar_title);
        mToolBarRightll = toolbarView.findViewById(R.id.toolbar_right_fl);
        setSupportActionBar(mToolBarBase);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            setDisplayHomeAsUpEnabled(true);
        }
        initToolBarRightView(mToolBarRightll);
        //设置返回键的功能
        mToolBarBase.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    public void resetToolbar() {
        if (mToolBarBase != null) {
            mToolBarBase.setNavigationIcon(R.drawable.icon_back);
            //设置返回键的功能
            mToolBarBase.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        if (getSupportActionBar() != null && mToolBarTitle != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            setDisplayHomeAsUpEnabled(true);
        }
        if (mToolBarRightll != null) {
            mToolBarRightll.removeAllViews();
        }
        if (mToolBarTitle != null) {
            mToolBarTitle.setText("");
        }

    }


    public void hideBackIcon() {
        setDisplayHomeAsUpEnabled(false);
    }

    public void showBackIcon() {
        setDisplayHomeAsUpEnabled(true);
    }


    public void setBackBtnEnable(boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(enable);
        }
    }

    public void setDisplayHomeAsUpEnabled(boolean enable) {
        setDisplayHomeAsUpEnabled(mToolBarTitle, enable);
    }

    public void setDisplayHomeAsUpEnabled(View view, boolean enable) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(enable);
            getSupportActionBar().setDisplayShowHomeEnabled(enable);
            getSupportActionBar().setHomeButtonEnabled(enable);

            //TODO 处理title居中
          /*  //处理title居中
            if (view != null) {
                Toolbar.LayoutParams layoutParams;
                int width = AndroidUtils.getScreenWidth(this);
                int padding = AndroidUtils.getActionBarHeight(this);
                int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                mToolBarRightFl.measure(w, h);
                if (enable) {
                    layoutParams = new Toolbar.LayoutParams(width - padding-mToolBarRightFl.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
                    view.setPadding(0, 0, padding, 0);
                } else {
                    layoutParams = new Toolbar.LayoutParams(width-mToolBarRightFl.getMeasuredWidth(), ViewGroup.LayoutParams.WRAP_CONTENT);
                    view.setPadding(padding, 0, padding, 0);
                }
                view.setLayoutParams(layoutParams);
            }*/

        }
    }


    public void setBackBtnListener(View.OnClickListener listener) {
        if (mToolBarBase != null) {
            mToolBarBase.setNavigationOnClickListener(listener);
        }
    }

    public LinearLayout getToolBarRightView() {
        return mToolBarRightll;
    }


    public void initToolBarRightView(LinearLayout toolBarRightFl) {
        if (toolBarRightFl != null) {
            mToolBarRightll = toolBarRightFl;
        }
    }

    public void setToolBarRightViewToView(View rightView) {
        if (mToolBarRightll != null && rightView != null) {
            mToolBarRightll.addView(rightView, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mToolBarRightll.setVisibility(View.VISIBLE);
        }
    }

    public ImageView setToolBarRightViewToImage(int resId) {
        if (resId > 0) {
            ImageView rightIv = new ImageView(this);
            rightIv.setImageResource(resId);
            mToolBarRightll.addView(rightIv);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightIv;
        }
        return null;
    }

    public ImageView addToolBarRightViewToImage(int resId, LinearLayout.LayoutParams params) {
        if (resId > 0) {
            ImageView rightIv = new ImageView(this);
            rightIv.setImageResource(resId);
            mToolBarRightll.addView(rightIv, 0, params);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightIv;
        }
        return null;
    }

    public ImageView addToolBarRightViewToImage(int resId) {
        if (resId > 0) {
            ImageView rightIv = new ImageView(this);
            rightIv.setImageResource(resId);
            mToolBarRightll.addView(rightIv, 0);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightIv;
        }
        return null;
    }

    public TextView setToolBarRightViewToText(int resId) {
        if (resId > 0) {
            TextView rightTv = new TextView(this);
            rightTv.setText(resId);
            rightTv.setTextColor(ContextCompat.getColor(this, R.color.app_white));
            rightTv.setText(resId);
            rightTv.setTextSize(16);
            rightTv.setGravity(Gravity.CENTER);
            rightTv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            mToolBarRightll.addView(rightTv);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightTv;
        }
        return null;
    }

    public TextView setToolBarRightViewToText(String resId) {
        if (!TextUtils.isEmpty(resId)) {
            TextView rightTv = new TextView(this);
            rightTv.setTextColor(ContextCompat.getColor(this, R.color.app_white));
            rightTv.setText(resId);
            rightTv.setTextSize(16);
            rightTv.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            rightTv.setGravity(Gravity.CENTER);
            mToolBarRightll.addView(rightTv);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightTv;
        }
        return null;
    }

    public void setToolBarRightViewToText(TextView rightTv) {
        if (rightTv != null) {
            mToolBarRightll.addView(rightTv);
            mToolBarRightll.setVisibility(View.VISIBLE);
        }
    }

    public ImageView setToolBarRightViewToImage(Uri uri) {
        if (uri != null) {
            ImageView rightIv = new ImageView(this);
            rightIv.setImageURI(uri);
            mToolBarRightll.addView(rightIv);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightIv;
        }
        return null;
    }

    public void setToolBarRightViewToImage(ImageView iv) {
        if (iv != null) {
            mToolBarRightll.addView(iv);
            mToolBarRightll.setVisibility(View.VISIBLE);
        }
    }

    public ImageView addToolBarRightViewToImage(Uri uri) {
        if (uri != null) {
            ImageView rightIv = new ImageView(this);
            rightIv.setImageURI(uri);
            mToolBarRightll.addView(rightIv, 0);
            mToolBarRightll.setVisibility(View.VISIBLE);
            return rightIv;
        }
        return null;
    }

    public void addToolBarRightViewToImage(ImageView iv) {
        if (iv != null) {
            mToolBarRightll.addView(iv, 0);
            mToolBarRightll.setVisibility(View.VISIBLE);
        }
    }

    public void initToolbarTitleView(TextView titleView) {
        if (titleView != null) {
            mToolBarTitle = titleView;
        }
    }

    //    /**
//     * 隐藏toolbar
//     */
    public void hideToolBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

    }

    public void showToolBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
        }
    }


    public void setToolBarTitle(String title) {
        if (mToolBarTitle != null) {
            mToolBarTitle.setText(title);
        }
    }

    public void setToolBarTitle(int title) {
        if (mToolBarTitle != null) {
            mToolBarTitle.setText(title);
        }
    }

    public void setToolBarTitleColor(int color) {
        if (mToolBarTitle != null) {
            mToolBarTitle.setTextColor(color);
        }
    }

    /**
     * 设置标题栏View
     * 如果重写 setToolbar 方法，必须重新 {@link #initToolBar(View)}
     *
     * @return
     */
    protected int setToolBar() {
        return R.layout.base_tool_bar;
    }

    private void configView() {
        LogUtils.i(TAG, " mInflater" + mInflater + " ac:" + this);
        View baseView = mInflater.inflate(getLayoutId(), null);
        if (notUsedToolbar()) {
            //如果使用通用样式 则需要自己自定义Toolbar及背景
            contentView = baseView;
            setContentView(contentView);
        } else {
            if (setToolBar() > 0) {
                View toolbarView = mInflater.inflate(setToolBar(), null);
                initToolBar(toolbarView);
                LinearLayout lpParent = new LinearLayout(this);
                lpParent.setOrientation(LinearLayout.VERTICAL);
                int width = AndroidUtils.getScreenWidth(this);
                toolbarView.setLayoutParams(new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));
                if (!notUsedCustomBackground()) {
                    baseView.setBackgroundResource(R.drawable.bg_base_view);
                    baseView.setOutlineProvider(new ViewOutlineProvider() {
                        @Override
                        public void getOutline(View view, Outline outline) {
                            Rect rect = new Rect();
                            outline.setRoundRect(rect, ScreenUtils.dip2px(20f));
                            int radius = ScreenUtils.dip2px(20);

                            //进行边缘圆角切割时，因为底部2个角不需要切割圆角，所以让底部的切割区域高于整体的区域大小，实现底部不切割圆角
                            outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + radius, radius);
                        }
                    });
                    baseView.setClipToOutline(true);
                }
                baseView.setLayoutParams(new LinearLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
                lpParent.addView(toolbarView);
                lpParent.addView(baseView);
                contentView = lpParent;

             /*   if (TextUtils.isEmpty(bgVersion)) {
                    //bgVersion 这个值不能为空
                    bgVersion = "0.0.1";
                }
                String mainBgVersion = SpUtils.getInstance().getMainBgVersion();
                if (!bgVersion.equals(mainBgVersion)) {
                    if (!TextUtils.isEmpty(mainBgVersion)) {
                        bgVersion = mainBgVersion;
                        backgroudkey = new ObjectKey(mainBgVersion);
                    } else {
                        backgroudkey = new ObjectKey(bgVersion);
                    }
                } else {
                    if (backgroudkey == null) {
                        backgroudkey = new ObjectKey(bgVersion);
                    }
                }*/

                try {
                    //如果不使用缓存，在内存不足的情况下，会频繁GC，然后出现死循环
                    String mainBackground = SpUtils.getInstance().getBaseBackground();
                    RequestOptions options = new RequestOptions()
                            .override(AndroidUtils.getScreenWidth(this), AndroidUtils.getScreenHeight(this));
                    if (!TextUtils.isEmpty(mainBackground)) {
                        Uri parse = Uri.parse(mainBackground);
                        GlideApp.with(this)
                                .load(parse)
                                .apply(options)
                                .into(new CustomTarget<Drawable>() {
                                    @Override
                                    public void onResourceReady(@NonNull @NotNull Drawable resource, @Nullable @org.jetbrains.annotations.Nullable Transition<? super Drawable> transition) {
                                        LogUtils.i(TAG, "--->onResourceReady--->");
                                        getWindow().getDecorView().setBackground(resource);
                                    }


                                    @Override
                                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                        super.onLoadFailed(errorDrawable);
                                        LogUtils.i(TAG, "--->onLoadFailed--->");
                                        SpUtils.getInstance().setBaseBackground("");
                                        SpUtils.getInstance().setMainBgVersion("");
                                        setBaseDefaultBackground();
                                    }


                                    @Override
                                    public void onLoadCleared(@Nullable Drawable placeholder) {
                                        LogUtils.i(TAG, "--->onLoadCleared--->");
                                    }

                                    @Override
                                    public void onLoadStarted(@Nullable Drawable placeholder) {
                                        super.onLoadStarted(placeholder);
                                        LogUtils.i(TAG, "--->onLoadStarted--->");
                                    }


                                });

                    } else {
                        setBaseDefaultBackground();
                    }
                } catch (Exception e) {
                    setBaseDefaultBackground();
                }
                setContentView(contentView);
            } else {
                contentView = baseView;
                setContentView(contentView);
            }
        }
    }


    public void setBaseDefaultBackground() {
        //因为onloadClear调用 activity可能为空
        if (isFinishing() || mContext == null) {
            return;
        }
        RequestOptions options = new RequestOptions()
                .override(AndroidUtils.getScreenWidth(this), Target.SIZE_ORIGINAL);
        GlideApp.with(mContext)
                .load(R.mipmap.bg_base_act)
                .error(R.mipmap.bg_base_act)
                .apply(options)
                .into(new CustomTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        SpUtils.getInstance().setBaseBackground("");
                        SpUtils.getInstance().setMainBgVersion("");
                        getWindow().getDecorView().setBackground(resource);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        SpUtils.getInstance().setBaseBackground("");
                        SpUtils.getInstance().setMainBgVersion("");
                        getWindow().getDecorView().setBackground(errorDrawable);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
//                        if (!BuildConfig.FLAVOR.equals("prd")) {
//                            Toast.makeText(mContext, "--->DefaultOnLoadFailed---", LENGTH_SHORT).show();
//                        }

                    }

                });
    }


    public int getCenterFrameLayoutHeight() {
        return totalHeight;
    }

    protected boolean isFullScreen() {
        return false;
    }

    public View getContentView() {
        return contentView;
    }

}
