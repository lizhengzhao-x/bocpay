package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class CircleWebview extends ProgressWebView {
    private Context context;

    private int width;

    private int height;

    private int radius;

    private int radiusLeftTop;
    private int radiusRightTop;
    private int radiusButtomLeft;
    private int radiusButtomRight;
    private boolean isShowButtomCorner;

    public CircleWebview(Context context) {
        super(context);

        initialize(context);
    }

    public CircleWebview(Context context, AttributeSet attrs) {
        super(context, attrs);

        initialize(context);
    }

    public CircleWebview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initialize(context);
    }

    public void setNoButtomRadius() {
        isShowButtomCorner = false;
        invalidate();
    }

    private void initialize(Context context) {
        this.context = context;
        isShowButtomCorner = true;
    }

    // This method gets called when the view first loads, and also whenever the
    // view changes. Use this opportunity to save the view's width and height.
    @Override
    protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
        width = newWidth;
        height = newHeight;
        radius = AndroidUtils.dip2px(context, 12);
        radiusLeftTop = radius;
        radiusRightTop = radius;
        if (!isShowButtomCorner) {
            radiusButtomLeft = 0;
            radiusButtomRight = 0;
        } else {
            radiusButtomLeft = radius;
            radiusButtomRight = radius;
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Path path = new Path();
        path.setFillType(Path.FillType.INVERSE_WINDING);
        // path.addRoundRect(new RectF(0, getScrollY(), width+getScrollX(), getScrollY() + height), radius, radius, Path.Direction.CW);
        float radiuss[] = {radiusLeftTop, radiusLeftTop, radiusRightTop, radiusRightTop, radiusButtomLeft, radiusButtomLeft, radiusButtomRight, radiusButtomRight};
        path.addRoundRect(new RectF(0, getScrollY(), width + getScrollX(), getScrollY() + height), radiuss, Path.Direction.CW);
        canvas.drawPath(path, createPorterDuffClearPaint());
    }

    private Paint createPorterDuffClearPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        return paint;
    }
}