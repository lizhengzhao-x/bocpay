package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * fps 行内绑定记录
 */

public class FpsBankRecordsEntity extends BaseEntity {


    public List<FpsBankRecordBean> getData() {
        return data;
    }

    public void setData(List<FpsBankRecordBean> data) {
        this.data = data;
    }

    private List<FpsBankRecordBean> data;
}
