package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class ConfirmTransferCrossBorderDataProtocol extends BaseProtocol {

    public static final String TAG = ConfirmTransferCrossBorderDataProtocol.class.getSimpleName();

    String receiverName;
    String cardNo;
    String receiverbankName;
    String amountHKD;
    String amountCNY;
    String forUsed;
    String sendCardId;
    String cardType;
    String refNo;
    String lastUpdatedRate;
    String HKD2CNYRate;
    String isTransferToSelf;

    public ConfirmTransferCrossBorderDataProtocol(
            String receiverName,
            String cardNo,
            String receiverbankName,
            String amountHKD,
            String amountCNY,
            String forUsed,
            String sendCardId,
            String cardType,
            String refNo,
            String lastUpdatedRate,
            String HKD2CNYRate,
            String isTransferToSelf,
            NetWorkCallbackListener dataCallback) {
        this.receiverName = receiverName;
        this.cardNo = cardNo;
        this.receiverbankName = receiverbankName;
        this.amountHKD = amountHKD;
        this.amountCNY = amountCNY;
        this.forUsed = forUsed;
        this.sendCardId = sendCardId;
        this.cardType = cardType;
        this.refNo = refNo;
        this.lastUpdatedRate = lastUpdatedRate;
        this.HKD2CNYRate = HKD2CNYRate;
        this.isTransferToSelf = isTransferToSelf;
        mUrl = "api/crossTransfer/preCheck";
        this.mDataCallback = dataCallback;
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.RECEIVERNAME, receiverName);
        mBodyParams.put(RequestParams.CARD_NUMBER, cardNo);
        mBodyParams.put(RequestParams.RECEIVERBANKNAME, receiverbankName);
        mBodyParams.put(RequestParams.AMOUNTHKD, amountHKD);
        mBodyParams.put(RequestParams.AMOUNTCNY, amountCNY);
        mBodyParams.put(RequestParams.FORUSED, forUsed);
        mBodyParams.put(RequestParams.SENDCARDID, sendCardId);
        mBodyParams.put(RequestParams.CARDTYPE, cardType);
        mBodyParams.put(RequestParams.REFNO, refNo);
        mBodyParams.put(RequestParams.LASTUPDATEDRATE, lastUpdatedRate);
        mBodyParams.put(RequestParams.HKD2CNYRATE, HKD2CNYRate);
        mBodyParams.put(RequestParams.ISTRANSFERTOSELF, isTransferToSelf);
    }


}
