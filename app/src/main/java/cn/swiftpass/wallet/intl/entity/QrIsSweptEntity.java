/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 *
 * Class Description: 单个被扫二维码
 *
 *
 */
public class QrIsSweptEntity extends BaseEntity {
    //二维码信息
    private String eMVQR;
    //条形码信息
    private String barcode;
    //交易单号
    private String txnId;

    public String geteMVQR() {
        return eMVQR;
    }

    public void seteMVQR(String eMVQR) {
        this.eMVQR = eMVQR;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }
}
