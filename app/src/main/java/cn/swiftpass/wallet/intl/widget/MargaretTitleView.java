package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


/**
 * Created by admin on 2017/12/19.
 *
 * @Package cn.swiftpass.wallet.intl.widget
 * @Description: (用一句话描述该文件做什么)
 * @date 2017/12/19.21:25.
 */

public class MargaretTitleView extends RelativeLayout {
    private RelativeLayout mTitleLayout;
    private LinearLayout mLeftLayout;
    private LinearLayout mMiddleLayout;
    private LinearLayout mRightLayout;
    private Context mContext;
    private LayoutInflater mInflater;
    private int spaceHor;
    /**
     * 当是back按键时，需要放大他的点击区域
     */
    private int tmpSpaceRightHor;

    public MargaretTitleView(Context context) {
        super(context);
        init(context);
    }

    public MargaretTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MargaretTitleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        spaceHor = mContext.getResources().getDimensionPixelSize(R.dimen.act_space_hor);
        mInflater = LayoutInflater.from(mContext);
        mTitleLayout = (RelativeLayout) mInflater.inflate(R.layout.margaret_title_view, null);
        addView(mTitleLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        mLeftLayout = (LinearLayout) getView(R.id.llayout_left);
        mMiddleLayout = (LinearLayout) getView(R.id.llayout_middle);
        mRightLayout = (LinearLayout) getView(R.id.llayout_right);
    }

    public void cleanAllView() {
        mLeftLayout.removeAllViews();
        mMiddleLayout.removeAllViews();
        mRightLayout.removeAllViews();
        tmpSpaceRightHor = 0;
    }

    public void setTitleBackgroundColor(int id) {
        mTitleLayout.setBackgroundColor(id);
    }

    public void setTitleBackground(int id) {
        mTitleLayout.setBackgroundResource(id);
    }

    public View getView(int id) {
        return mTitleLayout.findViewById(id);
    }

    public ImageView getImageView(int id) {
        ImageView iv = (ImageView) mInflater.inflate(R.layout.title_imageview, null);
        iv.setImageResource(id);
        return iv;
    }

    public TextView getTextView(Object text) {
        TextView tv = (TextView) mInflater.inflate(R.layout.title_textview, null);
        if (text instanceof CharSequence) {
            tv.setText((CharSequence) text);
        } else if (text instanceof String) {
            tv.setText((String) text);
        } else if (text instanceof Integer) {
            tv.setText((Integer) text);
        }
        return tv;
    }

    public TextView addRightTextView(Object text) {
        TextView tv = getTextView(text);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, mContext.getResources().getDimensionPixelSize(R.dimen.font_size_30));
        tv.setPadding(0, 0, spaceHor, 0);
        mRightLayout.addView(tv);
        return tv;
    }

    public TextView addLeftTextView(Object text) {
        TextView tv = getTextView(text);
        int leftPadding = spaceHor;
        if (tmpSpaceRightHor == spaceHor) {
            leftPadding = 0;
            tmpSpaceRightHor = 0;
        }
        tv.setPadding(leftPadding, 0, 0, 0);
        mLeftLayout.addView(tv);
        return tv;
    }

    public TextView addMiddleTextView(Object text) {
        TextView tv = getTextView(text);
        mMiddleLayout.addView(tv);
        return tv;
    }

    public void addMiddleView(View view) {
        mMiddleLayout.addView(view);
    }

    public LinearLayout getMiddletLayout() {
        return mMiddleLayout;
    }

    public ImageView addRightImageView(int id) {
        ImageView iv = getImageView(id);
        iv.setPadding(0, 0, spaceHor, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mRightLayout.addView(iv, layoutParams);
        return iv;
    }

    public ImageView addLeftImageView(int id, boolean hasRightPadding) {
        ImageView iv = getImageView(id);
        int leftPadding = spaceHor;
        int rightPadding = tmpSpaceRightHor;
        if (tmpSpaceRightHor == spaceHor) {
            leftPadding = 0;
            tmpSpaceRightHor = 0;
        }
        if (hasRightPadding) {
            rightPadding = spaceHor;
            tmpSpaceRightHor = spaceHor;
        } else {
            tmpSpaceRightHor = 0;
        }
        iv.setPadding(leftPadding, 0, rightPadding, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mLeftLayout.addView(iv, layoutParams);
        return iv;
    }

    public ImageView addLeftImageView(int id) {
        return addLeftImageView(id, false);
    }

    public ImageView replaceRightImageView(int position, int id) {
        ImageView iv = getImageView(id);
        iv.setPadding(0, 0, spaceHor, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        View view = mRightLayout.getChildAt(position);
        mRightLayout.addView(iv, position, layoutParams);
        mRightLayout.removeView(view);
        return iv;
    }

    public ImageView replaceLeftImageView(int position, int id, boolean hasRightPadding) {
        ImageView iv = getImageView(id);
        int leftPadding = spaceHor;
        int rightPadding = tmpSpaceRightHor;
        if (tmpSpaceRightHor == spaceHor) {
            leftPadding = 0;
            tmpSpaceRightHor = 0;
        }
        if (hasRightPadding) {
            rightPadding = spaceHor;
            tmpSpaceRightHor = spaceHor;
        } else {
            tmpSpaceRightHor = 0;
        }
        iv.setPadding(leftPadding, 0, rightPadding, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);

        View view = mLeftLayout.getChildAt(position);
        mLeftLayout.addView(iv, position, layoutParams);
        mLeftLayout.removeView(view);
        return iv;
    }

    public ImageView replaceLeftImageView(int position, int id) {
        return replaceLeftImageView(position, id, false);
    }


    public void removeRightChildView() {
        mRightLayout.removeAllViews();
    }


}
