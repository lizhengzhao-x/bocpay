package cn.swiftpass.wallet.intl.module.cardmanagement.fps;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 添加中银FPS账号
 */

public class FpsBindSelTypeActivity extends BaseCompatActivity {


    public static final String TAB_TYPE = "tab type";
    public static final String TAB_TYPE_DEFAULT = "tab type default";
    //无限制
    public static final int TAB_TYPE_NO_LIMIT = -1;
    //在已存在邮箱，只能添加手机
    public static final int TAB_TYPE_MOBILE = 0;
    //在已存在手机，只能添加邮箱
    public static final int TAB_TYPE_EMAIL = 1;
    @BindView(R.id.tv_record_next)
    TextView mRecordNextTV;
    @BindView(R.id.ll_switch_view)
    LinearLayout mSwitchLayout;
    @BindView(R.id.tv_mobile_tab)
    TextView mMobileTabTV;
    @BindView(R.id.tv_email_tab)
    TextView mEmailTabTV;
    @BindView(R.id.tv_email_addr)
    TextView mEmailAddrTV;
    @BindView(R.id.tv_mobile_no)
    TextView mMobileNoTV;

    @BindView(R.id.tv_email_addr_label)
    TextView mEmailAddrLabelTV;
    @BindView(R.id.tv_mobile_no_label)
    TextView mMobileNoLabelTV;
    @BindView(R.id.tv_set_default)
    TextView mSetDefaultTV;
    @BindView(R.id.tv_smart_account_label)
    TextView mSmartAccountLabelTV;


    @BindView(R.id.tv_balance_amount)
    TextView mBalanceTV;

    @BindView(R.id.tv_smart_account)
    TextView mSmartAccountTV;
    @BindView(R.id.ll_left_mobile)
    LinearLayout mMobileLayout;
    @BindView(R.id.ll_right_email)
    LinearLayout mEmailLayout;

    @BindView(R.id.iv_circle)
    ImageView mDefaultCircelIV;

    @BindView(R.id.ll_set_default)
    LinearLayout mSetDefaultLayout;



    MySmartAccountEntity mSmartAccountInfo;

    int mTabType = TAB_TYPE_MOBILE;
    //限制只能哪个TAB能编辑
    int mTabTypeLimit = TAB_TYPE_NO_LIMIT;
    boolean mIsDefaultMobile = true;
    boolean mIsDefaultEmail = true;
    boolean mIsDefault = true;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C1_01_1_1);
        parseIntent();
        updateNextStatus(false);
        getSmartAccountInfo();
    }

    private void parseIntent() {
        if (null != getIntent()) {
            mTabTypeLimit = getIntent().getIntExtra(TAB_TYPE, TAB_TYPE_NO_LIMIT);
            if (mTabTypeLimit == TAB_TYPE_EMAIL) {
                mTabType = mTabTypeLimit;
                mIsDefaultMobile = getIntent().getBooleanExtra(TAB_TYPE_DEFAULT, true);
            }
            if (mTabTypeLimit == TAB_TYPE_MOBILE) {
                mTabType = mTabTypeLimit;
                mIsDefaultEmail = getIntent().getBooleanExtra(TAB_TYPE_DEFAULT, true);
            }
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_set_up;
    }

    /**
     * 获取我的账户信息
     */
    private void getSmartAccountInfo() {
        NetWorkCallbackListener<MySmartAccountEntity> callback = new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(getActivity(), errorMsg);
                refreshView();
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                mSmartAccountInfo = response;
                refreshView();
            }
        };
        ApiProtocolImplManager.getInstance().getSmartAccountInfo(this, callback);
    }

    private void refreshView() {
        if (null != mSmartAccountInfo) {
            if (!mSmartAccountInfo.getMobile().contains("+")) {
                mSmartAccountInfo.setMobile("+" + mSmartAccountInfo.getMobile());
            }
            mEmailAddrTV.setText(AndroidUtils.fpsEmailFormat(mSmartAccountInfo.getEmail()));
            mMobileNoTV.setText(AndroidUtils.fpsPhoneFormat(mSmartAccountInfo.getMobile()));
            String sm = AndroidUtils.getSubSmartCardNumberTitle(AndroidUtils.getSubMasCardNumber(mSmartAccountInfo.getSmartNo()), this);
            mSmartAccountTV.setText(sm);
            mBalanceTV.setText(mSmartAccountInfo.getBalance());
            if (mTabType == TAB_TYPE_EMAIL) {
                showEmailTab();
            } else {
                showMobileTab();
            }
            updateNextStatus(true);
        } else {
            updateNextStatus(false);
        }
    }


    @OnClick({R.id.tv_mobile_tab, R.id.tv_email_tab, R.id.tv_record_next, R.id.ll_set_default})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_mobile_tab:
                if (mTabType != TAB_TYPE_MOBILE) {
                    mTabType = TAB_TYPE_MOBILE;
                    showMobileTab();
                }
                break;
            case R.id.tv_email_tab:
                if (mTabType != TAB_TYPE_EMAIL) {
                    mTabType = TAB_TYPE_EMAIL;
                    showEmailTab();
                }
                break;
            case R.id.tv_record_next:
                onNext();
                break;
            case R.id.ll_set_default:
                if (mTabTypeLimit == TAB_TYPE_NO_LIMIT || mTabTypeLimit == mTabType) {
                    if (mTabType == TAB_TYPE_EMAIL) {
                        mIsDefaultEmail = !mIsDefaultEmail;
                        mIsDefault = mIsDefaultEmail;
                    } else {
                        mIsDefaultMobile = !mIsDefaultMobile;
                        mIsDefault = mIsDefaultMobile;
                    }
                    switchDefaultStatus(mIsDefault);
                }
                break;
            default:
                break;
        }
    }

    private void switchDefaultStatus(boolean check) {
        if (!check) {
            mDefaultCircelIV.setImageResource(R.mipmap.icon_check_choose_register_default);
        } else {
            mDefaultCircelIV.setImageResource(R.mipmap.icon_check_choose_circle);
        }
    }

    private void onNext() {
        FpsBankRecordBean bean = new FpsBankRecordBean();
        if (mTabType == TAB_TYPE_MOBILE) {
            bean.setAccountId(mSmartAccountInfo.getMobile());
        } else {
            bean.setAccountId(mSmartAccountInfo.getEmail());
        }
        bean.setAccountIdType(getAccountIdType());
        bean.setAccountNo(mSmartAccountInfo.getSmartNo());
        bean.setDefaultBank(getDefaultValue());
        bean.setAccountType(getString(R.string.smart_account));
        bean.setAccountTypeName(getString(R.string.C3_01_2_3));
        FpsInfoConfirmActivity.startActivity(this, bean);
    }

    private void showEmailTab() {
        mMobileLayout.setVisibility(View.GONE);
        mEmailLayout.setVisibility(View.VISIBLE);
        mEmailTabTV.setBackgroundResource(R.drawable.right_china_btn_bac);
        mMobileTabTV.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        mEmailTabTV.setTextColor(getResources().getColor(R.color.app_white));
        mMobileTabTV.setTextColor(getResources().getColor(R.color.font_gray_three));
        boolean enable = true;
        if (mTabTypeLimit == TAB_TYPE_MOBILE) {
            showAddedTip();
            enable = false;
            mEmailAddrLabelTV.setTextColor(getColor(R.color.font_gray_three));
            mEmailAddrTV.setTextColor(getColor(R.color.font_gray_three));
            mSmartAccountLabelTV.setTextColor(getColor(R.color.font_gray_three));
            mSmartAccountTV.setTextColor(getColor(R.color.font_gray_three));
            mSetDefaultTV.setTextColor(getColor(R.color.font_gray_three));
        } else {
            mSmartAccountLabelTV.setTextColor(getColor(R.color.font_black_high));
            mSmartAccountTV.setTextColor(getColor(R.color.font_black_high));
            mSetDefaultTV.setTextColor(getColor(R.color.font_black_high));
        }
        updateNextStatus(enable);
        mIsDefault = mIsDefaultEmail;
        switchDefaultStatus(mIsDefault);
    }

    private void showMobileTab() {
        mMobileLayout.setVisibility(View.VISIBLE);
        mEmailLayout.setVisibility(View.GONE);
        mMobileTabTV.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
        mEmailTabTV.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        mMobileTabTV.setTextColor(getResources().getColor(R.color.app_white));
        mEmailTabTV.setTextColor(getResources().getColor(R.color.font_gray_three));
        boolean enable = true;
        if (mTabTypeLimit == TAB_TYPE_EMAIL) {
            showAddedTip();
            enable = false;
            mMobileNoLabelTV.setTextColor(getColor(R.color.font_gray_three));
            mMobileNoTV.setTextColor(getColor(R.color.font_gray_three));
            mSmartAccountLabelTV.setTextColor(getColor(R.color.font_gray_three));
            mSmartAccountTV.setTextColor(getColor(R.color.font_gray_three));
            mSetDefaultTV.setTextColor(getColor(R.color.font_gray_three));
        } else {
            mSmartAccountLabelTV.setTextColor(getColor(R.color.font_black_high));
            mSmartAccountTV.setTextColor(getColor(R.color.font_black_high));
            mSetDefaultTV.setTextColor(getColor(R.color.font_black_high));
        }
        updateNextStatus(enable);
        mIsDefault = mIsDefaultMobile;
        switchDefaultStatus(mIsDefault);
    }

    private void updateNextStatus(boolean enable) {
        if (null != mSmartAccountInfo) {
            mRecordNextTV.setEnabled(enable);
            mRecordNextTV.setBackgroundResource(enable ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
        } else {
            mRecordNextTV.setEnabled(false);
            mRecordNextTV.setBackgroundResource(false ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
        }
    }

    private void showAddedTip() {
        AndroidUtils.showTipDialog(getActivity(), getString(R.string.fps_record_has_added));
    }


    private String getDefaultValue() {
        return mIsDefault ? FpsConst.FPS_DEFAULT_YES : FpsConst.FPS_DEFAULT_NO;
    }

    private String getAccountIdType() {
        return TAB_TYPE_MOBILE == mTabType ? FpsConst.ACCOUNT_ID_TYPE_MOBILE : FpsConst.ACCOUNT_ID_TYPE_EMAIL;
    }

    public static void startActivity(Activity activity, int type, boolean isDefault) {
        Intent intent = new Intent(activity, FpsBindSelTypeActivity.class);
        intent.putExtra(TAB_TYPE, type);
        intent.putExtra(TAB_TYPE_DEFAULT, isDefault);
        activity.startActivity(intent);
    }
}
