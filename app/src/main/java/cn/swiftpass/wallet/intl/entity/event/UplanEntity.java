package cn.swiftpass.wallet.intl.entity.event;

public class UplanEntity extends BaseEventEntity {


    public static final int EVENT_UPLAN_BACK_SCAN = 100;
    String action;

    public String getAction() {
        return action;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    String couponInfo;

    public UplanEntity(int eventType, String message) {
        super(eventType, message);
    }

    public UplanEntity(int eventType, String actionIn, String couponInfoIn) {
        super(eventType, "");
        this.action = actionIn;
        this.couponInfo = couponInfoIn;
    }
}
