package cn.swiftpass.wallet.intl.module.redpacket.presenter;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.module.redpacket.contract.RedPacketAddressContract;

/**
 * 派利是 通讯录选择
 */
public class RedPacketAddressPresenter extends BasePresenter<RedPacketAddressContract.View> implements RedPacketAddressContract.Presenter {


    @Override
    public void checkSmartAccountNewBind(final String phone, final String name, final String relName, final boolean isEmail) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkSmartAccountNewBindError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkSmartAccountNewBindSuccess(response, phone, name, relName, isEmail);
                }

            }
        }).execute();
    }

    @Override
    public List<ContractListEntity.RecentlyBean> dealWithAllList(List<ContactEntity> frequentCcontactsIn) {

        List<ContractListEntity.RecentlyBean> recentlyBeans = new ArrayList<>();
        List<ContactEntity> contactEntities = new ArrayList<>();
        List<ContactEntity> frequentCcontacts = new ArrayList<>();
        if (frequentCcontactsIn != null) {
            frequentCcontacts.addAll(frequentCcontactsIn);
        }

        for (int i = 0; i < recentlyBeans.size(); i++) {
            recentlyBeans.get(i).setUiShowType(ContractListEntity.RecentlyBean.UI_RECENTLY);
        }
        if (contactEntities != null) {
            for (int i = 0; i < contactEntities.size(); i++) {
                ContactEntity contactEntity = contactEntities.get(i);
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(ContractListEntity.RecentlyBean.UI_FREQUENT);
                recentlyBean.setContactID(contactEntity.getContactID());
                recentlyBean.setCollect(contactEntity.isCollect());
                recentlyBean.setDisCustName(contactEntity.getUserName());
                if (contactEntity.isEmail()) {
                    recentlyBean.setDisEmail(contactEntity.getNumber());
                    recentlyBean.setEmail(contactEntity.getNumber());
                } else {
                    recentlyBean.setPhoneNo(contactEntity.getNumber());
                    recentlyBean.setDisPhoneNo(contactEntity.getNumber());
                }
                recentlyBean.setEmailType(contactEntity.isEmail() ? "1" : null);
                recentlyBeans.add(0, recentlyBean);
            }
        }

        if (frequentCcontacts != null) {
            for (int i = 0; i < frequentCcontacts.size(); i++) {
                ContactEntity contactEntity = frequentCcontacts.get(i);
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(ContractListEntity.RecentlyBean.UI_ADDRESSBOOK);
                recentlyBean.setContactID(contactEntity.getContactID());
                recentlyBean.setCollect(contactEntity.isCollect());
                recentlyBean.setDisCustName(contactEntity.getUserName());
                if (contactEntity.isEmail()) {
                    recentlyBean.setDisEmail(contactEntity.getNumber());
                    recentlyBean.setEmail(contactEntity.getNumber());
                } else {
                    recentlyBean.setPhoneNo(contactEntity.getNumber());
                    recentlyBean.setDisPhoneNo(contactEntity.getNumber());
                }

                recentlyBean.setEmailType(contactEntity.isEmail() ? "1" : null);
                recentlyBeans.add(recentlyBean);
            }
        }
        return recentlyBeans;
    }

}
