package cn.swiftpass.wallet.intl.module.register.model;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.BankRegisterVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckBankRegisterVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeEntity;

public class BankRegisterVerifyCodeModel {

    /**
     * 获取信用卡注册界面验证码
     *
     * @param action
     * @param dataCallback
     */
    public static void getBankRegisterVerifyCode(String action, NetWorkCallbackListener dataCallback) {
        new BankRegisterVerifyCodeProtocol(action, new NetWorkCallbackListener<BankRegisterVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dataCallback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BankRegisterVerifyCodeEntity response) {
                dataCallback.onSuccess(response);
            }
        }).execute();
    }

    /**
     * 检查信用卡注册界面验证码
     *
     * @param verifyCode
     * @param action
     * @param dataCallback
     */
    public static void checkBankRegisterVerifyCode(String verifyCode, String action, NetWorkCallbackListener dataCallback) {
        new CheckBankRegisterVerifyCodeProtocol(verifyCode, action, new NetWorkCallbackListener<BankRegisterVerifyCodeCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dataCallback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BankRegisterVerifyCodeCheckEntity response) {
                dataCallback.onSuccess(response);
            }
        }).execute();
    }

}
