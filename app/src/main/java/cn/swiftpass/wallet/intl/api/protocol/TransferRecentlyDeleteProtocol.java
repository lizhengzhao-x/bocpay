package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 转账用户删除列表
 */
public class TransferRecentlyDeleteProtocol extends BaseProtocol {
    String mTransferRecentlyId;

    String type;

    public TransferRecentlyDeleteProtocol(String transferRecentlyId, String typeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mTransferRecentlyId = transferRecentlyId;
        this.type = typeIn;
        mUrl = "api/transfer/bocPayTransferDeleteRecently";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TRANSFERRECENTLYID, mTransferRecentlyId);
        mBodyParams.put(RequestParams.TRANSTYPE, type);
    }

}
