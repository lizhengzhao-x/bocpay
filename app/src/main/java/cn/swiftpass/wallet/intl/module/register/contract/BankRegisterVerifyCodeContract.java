package cn.swiftpass.wallet.intl.module.register.contract;

import android.content.Context;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeEntity;

/**
 * 信用卡注册页面验证码功能 V、P层  关系定义
 */
public class BankRegisterVerifyCodeContract {

    public interface View extends IView {
        void getBankRegisterVerifyCodeSuccess(BankRegisterVerifyCodeEntity response);

        void getBankRegisterVerifyCodeFailed(String errorCode, String errorMsg);

        void checkBankRegisterVerifyCodeSuccess(BankRegisterVerifyCodeCheckEntity response);

        void checkBankRegisterVerifyCodeFailed(String errorCode, String errorMsg);
    }

    public interface Presenter extends IPresenter<View> {
        //获取验证码
        void getBankRegisterVerifyCode(final Context context);

        //检查验证码
        void checkBankRegisterVerifyCode(final Context context, String verifyCode);
    }

}
