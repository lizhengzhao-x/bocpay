package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferFpsCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferPreCheckByBocProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferPreCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferQueryBalanceProtocol;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferBaseContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.FpsCheckEntity;

/**
 * 转账
 */
public class TransferBasePresenter<P extends TransferBaseContract.View> extends BasePresenter<P> implements TransferBaseContract.Presenter<P> {

    @Override
    public void transferPreByBoc(boolean isLiShi, TransferPreCheckReq req, String tansferAccount) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new TransferPreCheckByBocProtocol(req, new NetWorkCallbackListener<TransferPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferPreByBocError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferPreCheckEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferPreByBocSuccess(isLiShi, req, tansferAccount, response);
                }
            }
        }).execute();
    }


    @Override
    public void fpsCheckAction(String transferType, String tansferToAccount, boolean show, boolean isPaiLiShi) {
        if (show && getView() != null) {
            getView().showDialogNotCancel();
        }
        //后台要求如果是派利是的话
        new TransferFpsCheckProtocol(transferType, tansferToAccount, isPaiLiShi ? "7" : null, new NetWorkCallbackListener<FpsCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (show && getView() != null) {
                    getView().dismissDialog();
                }
                if (getView() != null) {
                    getView().fpsCheckActionError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(FpsCheckEntity response) {
                if (show && getView() != null) {
                    getView().dismissDialog();
                }
                if (getView() != null) {
                    getView().fpsCheckActionSuccess(response);
                }
            }
        }).execute();
    }


    @Override
    public void checkTransferPre(boolean show, boolean isLishi, TransferPreCheckReq req) {
        if (show && getView() != null) {
            getView().showDialogNotCancel();
        }

        new TransferPreCheckProtocol(req, new NetWorkCallbackListener<TransferPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (show) {
                    getView().dismissDialog();
                }
                if (getView() != null) {
                    getView().checkTransferPreError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferPreCheckEntity response) {
                if (show) {
                    getView().dismissDialog();
                }
                if (getView() != null) {
                    getView().checkTransferPreSuccess(response, isLishi, req);
                }
            }
        }).execute();
    }

    @Override
    public void getRegEddaInfo() {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    //getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    //getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartAccountInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();

    }

    @Override
    public void getAccountBalanceInfo() {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new TransferQueryBalanceProtocol(new NetWorkCallbackListener<AccountBalanceEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    //getView().dismissDialog();
                    getView().getAccountBalanceInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(AccountBalanceEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getAccountBalanceInfoSuccess(response);
                }
            }
        }).execute();
    }

}
