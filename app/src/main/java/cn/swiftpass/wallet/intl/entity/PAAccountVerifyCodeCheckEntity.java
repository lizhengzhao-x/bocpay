package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */
public class PAAccountVerifyCodeCheckEntity extends BaseEntity {

    String data;
    private String transactionUniqueID;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTransactionUniqueID() {
        return transactionUniqueID;
    }

    public void setTransactionUniqueID(String transactionUniqueID) {
        this.transactionUniqueID = transactionUniqueID;
    }
}
