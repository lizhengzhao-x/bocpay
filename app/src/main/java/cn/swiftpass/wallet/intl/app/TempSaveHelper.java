package cn.swiftpass.wallet.intl.app;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.entity.AppParameters;
import cn.swiftpass.wallet.intl.entity.CurrencyCodeEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;

/**
 * 临时的静态内存变量
 */
public class TempSaveHelper {


//    private static class TempSaveHelperHolder {
//        private static TempSaveHelper instance = new TempSaveHelper();
//    }
//
//    public static TempSaveHelper getInstance() {
//        return TempSaveHelper.TempSaveHelperHolder.instance;
//    }
//
//
//    private void TempSaveHelper(){
//
//    }


    /**
     * 货币符号列表 解析country.json得到
     */
    @Nullable
    private static List<CurrencyCodeEntity.DataBean> currencyList;


    /**
     * p2p p2m 结果页面banner显示
     */
    @Nullable
    private static InitBannerEntity initBannerUrlParams;


    /**
     * app call app link维护清除
     */
    private static String appLink;

    public static String getNetworkErrorLink() {
        return networkErrorLink;
    }

    /**
     * 支付网络异常返回商户app
     */
    private static String networkErrorLink;

    public static boolean isIsH5Pay() {
        return isH5Pay;
    }

    private static boolean isH5Pay;

    public static String getMerchantLink() {
        return merchantLink;
    }

    public static void setAppCallAppConfig(String merchantLink, String appLink, String errorLink, boolean isFromH5) {
        TempSaveHelper.merchantLink = merchantLink;
        TempSaveHelper.appLink = appLink;
        TempSaveHelper.isH5Pay = isFromH5;
        TempSaveHelper.networkErrorLink = errorLink;
    }

    public static void updateMerchantLink(String merchantLink, boolean isFromH5) {
        TempSaveHelper.merchantLink = merchantLink;
        TempSaveHelper.isH5Pay = isFromH5;
    }
    public static void updateMerchantLink(String merchantLink) {
        TempSaveHelper.merchantLink = merchantLink;
    }
    /**
     * app call app 返回商户link维护清除
     */
    private static String merchantLink;
    /**
     * 侧边栏数据模型
     */
    @Nullable
    private static ArrayList<MenuItemEntity> menuList;

    /**
     * 是否初始化url参数成功
     */
    private static boolean initBannerUrlSuccess;
    /**
     * 注册 绑卡 的时候 checkid流程控制
     */
    private static boolean isCheckIdSuccess;

    /**
     * idv检测300s 开始
     */
    private static long mCheckIdStartTime;

    /**
     * 注册流程需要一个recordId
     */
    private static String registerRecordId;

    @Nullable
    private static Certificate[] certificates;


    /**
     * app错误码
     */
    @Nullable
    private static JSONObject errorCodeList;

    /**
     * 输入卡号码页面  为了处理 ocr识别国际化问题
     */
    private static int bankCardNumberType;

    private static int mainFrameLayoutHeight = 0;

    @Nullable
    private static AppParameters lbsAppParameters;


    public static void setIsCheckIdSuccess(boolean isCheckIdSuccess) {
        TempSaveHelper.isCheckIdSuccess = isCheckIdSuccess;
    }


    public static void setMainFrameHeight(int height) {
        mainFrameLayoutHeight = height;
    }

    public static int getMainFrameLayoutHeight() {
        return mainFrameLayoutHeight;
    }


    public static long getmCheckIdStartTime() {
        return mCheckIdStartTime;
    }

    public static void setCheckIdStartTime(long checkIdStartTime) {
        mCheckIdStartTime = checkIdStartTime;
    }


    public static String getRegisterRecordId() {
        return registerRecordId;
    }


    public static void setRegisterRecordId(String registerRecordIdIn) {

        registerRecordId = registerRecordIdIn;

    }

    public static AppParameters getLbsAppParameters() {
        return lbsAppParameters;
    }


    public static void setLbsSwitch(AppParameters appParameters) {

        lbsAppParameters = appParameters;
    }


    public static Certificate[] getCertificates() {
        return certificates;
    }


    public static void setSdkCertifacates(Certificate[] certificatesIn) {
        certificates = certificatesIn;
    }


    public static JSONObject getErrorCodeList() {
        return errorCodeList;
    }

    public static void setErrorCodeObject(JSONObject jsonObject) {
        errorCodeList = jsonObject;
    }

    public static InitBannerEntity getInitBannerUrlParams() {
        return initBannerUrlParams;
    }


    public static boolean isInitBannerUrlSuccess() {
        return initBannerUrlSuccess;
    }


    public static void setInitBannerUrlParams(InitBannerEntity initBannerUrlParamsIn, boolean initBannerUrlSuccessIn) {
        initBannerUrlParams = initBannerUrlParamsIn;
        initBannerUrlSuccess = initBannerUrlSuccessIn;
    }

    public static int getBankCardNumberType() {
        return bankCardNumberType;
    }

    public static void setBankCardNumberType(int bankCardNumberTypeIn) {
        bankCardNumberType = bankCardNumberTypeIn;
    }

    public static List<CurrencyCodeEntity.DataBean> getCurrencyList() {
        return currencyList;
    }

    public static void setCurrencyList(@Nullable List<CurrencyCodeEntity.DataBean> currencyList) {
        TempSaveHelper.currencyList = currencyList;
    }


    /**
     * 初始化，防止静态变量无法释放
     */
    public static void init() {
        currencyList = null;
        initBannerUrlParams = null;
        initBannerUrlSuccess = false;
        isCheckIdSuccess = false;
        mCheckIdStartTime = 0;
        registerRecordId = null;
        certificates = null;
        errorCodeList = null;
        bankCardNumberType = 0;
        mainFrameLayoutHeight = 0;
        lbsAppParameters = null;
        appLink = null;
        merchantLink = null;
    }


    public static String getAppLink() {
        return appLink;
    }

    public static void clearLink() {
        appLink = null;
        merchantLink = null;
        isH5Pay = false;
    }

    public static void setMenuList(@Nullable ArrayList<MenuItemEntity> list) {
        menuList = list;
    }

    public static ArrayList<MenuItemEntity> getMenuListData() {
        return menuList;
    }
}
