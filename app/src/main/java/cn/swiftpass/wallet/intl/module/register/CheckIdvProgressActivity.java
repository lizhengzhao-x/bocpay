package cn.swiftpass.wallet.intl.module.register;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisParams;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * 新版checkIdv轮询界面
 * 忘记密码/注册/绑卡流程
 */
public class CheckIdvProgressActivity extends BaseCompatActivity {


    @BindView(R.id.vp_new_func)
    ViewPager mNewFuncVP;
    @BindView(R.id.iv_indicator_first)
    ImageView mIndicatorFirst;
    @BindView(R.id.iv_indicator_second)
    ImageView ivIndicatorSecond;
    @BindView(R.id.iv_indicator_third)
    ImageView ivIndicatorThird;
    @BindView(R.id.id_progress_view)
    LinearLayout idProgressView;
    @BindView(R.id.id_progress_tv)
    TextView idProgressTv;
    @BindView(R.id.id_head_image)
    ImageView idHeadImage;
    @BindView(R.id.id_parent_view)
    FrameLayout idParentView;
    private int mTotalRetainTime;
    private IdAuthIinfoEntity mIdAuthInfoResponse;
    private Handler mCheckHandler;
    private boolean isQuerySuccess;
    private int mPageFlow;
    /**
     * idv 检测是否失败 失败的话 会跳转失败界面 方式
     */
    private boolean isAlreadyJumpPage;
    /**
     * 整个流程是300s
     */
    private static final int TOTAL_TIME = 300;
    private static final int REQUEST_CODE_VIDEO = 10000;
    /**
     * idv报错流程 定时器应该结束 进度条应该直接变为100%
     */
    private boolean isStopTimer = false;
    private List<View> mArrayPage = new ArrayList<>();
    private int[] mImages_cn = new int[]{R.mipmap.banner_cn_1, R.mipmap.banner_cn_2, R.mipmap.banner_cn_3};
    private int[] mImages_hk = new int[]{R.mipmap.banner_hk_1, R.mipmap.banner_hk_2, R.mipmap.banner_hk_3};
    private int[] mImages_en = new int[]{R.mipmap.banner_en_1, R.mipmap.banner_en_2, R.mipmap.banner_en_3};

    /**
     * 判断是否为点击webview视频播放后跳转页面 如果是  则该条记录不埋点
     */
    private boolean isAnalysis = true;
    private String mPhoneForForgetPwd;

    public static final String BLACK_LIST[] = {"EWA5182", "EWA5183", "EWA5279", "EWA5280"};


    private Runnable reQurtQrStatusRunnable = new Runnable() {
        @Override
        public void run() {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                checkIdvStatusForRegister();
            } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                checkIdvStatusForBindCard();
            } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                checkIdvStatusForForgetPwd();
            }
        }
    };

    @Override
    protected void onStart() {
        isAnalysis = true;
        super.onStart();
    }

    @Override
    public boolean isAnalysisPage() {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            if (isAnalysis) {
                return true;
            }
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.ALL_INFORMATION_CONFIRM;
        analysisPageEntity.current_address = PagerConstant.REGISTRATION_WAITING;
        return analysisPageEntity;
    }

    @Override
    public void init() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        } else {
            finish();
            return;
        }
        hideBackIcon();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
            setToolBarTitle(R.string.IDV_3_1a);
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            setToolBarTitle(R.string.IDV_3a_19_1);
        }
        long currentTime = System.currentTimeMillis();
        //根据当前的时间戳还有checkidv的时间 计算时间间隔 开始倒计时
        mTotalRetainTime = (int) ((currentTime - TempSaveHelper.getmCheckIdStartTime()) / 1000);
        mTotalRetainTime = TOTAL_TIME - mTotalRetainTime;
        if (mTotalRetainTime < 0) {
            mTotalRetainTime = 0;
        }
        updateText(mTotalRetainTime);
        GlideApp.with(CheckIdvProgressActivity.this).asGif().diskCacheStrategy(DiskCacheStrategy.RESOURCE).load(R.mipmap.waiting).into(idHeadImage);
        mCheckHandler = new Handler();
        initViewPager();
        initStartTimer();
    }

    private void updateText(int millisUntilFinished) {
        if (idProgressTv == null) return;
        int currentProgress = (int) (((TOTAL_TIME - millisUntilFinished) * 1.0 / TOTAL_TIME * 1.0) * 100);
        idProgressTv.setText(getString(R.string.IDV_27_2) + " " + currentProgress + "%");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_checkidv_progress;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    /**
     * 整个倒计时逻辑
     */
    private void initStartTimer() {
        AdvancedCountdownTimer.getInstance().startCountDown(mTotalRetainTime, new AdvancedCountdownTimer.OnCountDownListener() {
            @Override
            public void onTick(int millisUntilFinished) {
                if (!isStopTimer) {
                    updateText(millisUntilFinished);
                }
            }

            @Override
            public void onFinish() {
                if (!isStopTimer) {
                    //倒计时结束 轮询停止
                    mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                    if (!isAlreadyJumpPage && !isQuerySuccess) {
                        if (!frontPageIsWebView()) {
                            goFailedPage();
                        } else {
                            EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_FAILED, ""));
                        }
                    }
                }
            }
        });

        mCheckHandler.postDelayed(reQurtQrStatusRunnable, 1000);
    }


    /**
     * 当前activity顶层是否是webview,如果用户在看视频是在其他页面 轮询不能自动停止 页面不能自动跳转 需要弹框提示
     *
     * @return
     */
    private boolean frontPageIsWebView() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity instanceof CheckIdvProgressActivity) {
            return false;
        }
        return true;
    }


    /**
     * viewPager初始化
     */
    private void initViewPager() {
        int width = ScreenUtils.getDialogWidth() - AndroidUtils.dip2px(this, 15) * 2;
        int height = (int) (width / 1.5);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
        lp.topMargin = 50;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        idParentView.setLayoutParams(lp);
        initNewFuncImage();
        mNewFuncVP.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return mArrayPage.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(mArrayPage.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, final int position) {
                View view = mArrayPage.get(position);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (position == 0) {
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.DETAIL_URL, Constants.REGISTER_VIDEO_URL);
                            mHashMaps.put(Constants.DETAIL_TITLE, "");
                            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                            ActivitySkipUtil.startAnotherActivityForResult(getActivity(), WebViewVideoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_CODE_VIDEO);
                        }
                    }
                });
                container.addView(mArrayPage.get(position));
                return mArrayPage.get(position);
            }
        });
        mNewFuncVP.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    private void initNewFuncImage() {
        String lan = SpUtils.getInstance(this).getAppLanguage();
        int[] mImages = null;
        if (AndroidUtils.isZHLanguage(lan)) {
            mImages = mImages_cn;
        } else if (AndroidUtils.isHKLanguage(lan)) {
            mImages = mImages_hk;
        } else {
            mImages = mImages_en;
        }
        for (int i = 0; i < mImages.length; i++) {
            ImageView photo = new ImageView(this);

            photo.setScaleType(ImageView.ScaleType.FIT_XY);
            photo.setImageResource(mImages[i]);
            mArrayPage.add(photo);
        }
    }


    private void updateIndicator(int postion) {
        if (postion == 0) {
            mIndicatorFirst.setImageResource(R.mipmap.scrollbar_idv_selected);
            ivIndicatorSecond.setImageResource(R.mipmap.scrollbar_idv_empty);
            ivIndicatorThird.setImageResource(R.mipmap.scrollbar_idv_empty);
        } else if (postion == 1) {
            mIndicatorFirst.setImageResource(R.mipmap.scrollbar_idv_empty);
            ivIndicatorSecond.setImageResource(R.mipmap.scrollbar_idv_selected);
            ivIndicatorThird.setImageResource(R.mipmap.scrollbar_idv_empty);
        } else if (postion == 2) {
            mIndicatorFirst.setImageResource(R.mipmap.scrollbar_idv_empty);
            ivIndicatorSecond.setImageResource(R.mipmap.scrollbar_idv_empty);
            ivIndicatorThird.setImageResource(R.mipmap.scrollbar_idv_selected);
        }
    }


    /**
     * 轮询对于忘记密码流程
     */
    private void checkIdvStatusForForgetPwd() {
        if (isQuerySuccess) return;
        ApiProtocolImplManager.getInstance().checkIdForForgetRequest(this, new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //DJ321
                onErrorCode(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                mPhoneForForgetPwd = response.getMobile();
                if (isQuerySuccess) return;
                //DJ321
                isQuerySuccess = true;
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                stopTimer();
                if (!frontPageIsWebView()) {
                    goS2ForgetPasswordPage();
                } else {
                    EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_SUCCESS, ""));
                }
            }
        });
    }


    /**
     * 跳转到检测失败界面
     */
    private void goS2ForgetPasswordPage() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, mPhoneForForgetPwd);
        ActivitySkipUtil.startAnotherActivity(CheckIdvProgressActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    /**
     * 轮询对于Pa注册流程
     */
    private void checkIdvStatusForRegister() {
        if (isQuerySuccess) return;
        ApiProtocolImplManager.getInstance().checkIdvNewRequest(this, new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onErrorCode(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                if (isQuerySuccess) return;
                //DJ321
                isQuerySuccess = true;
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                stopTimer();
                if (!frontPageIsWebView()) {
                    goS2RegisterSuccessPage();
                } else {
                    EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_SUCCESS, ""));
                }

            }
        });
    }

    /**
     * 轮询对于绑定Pa流程
     */
    private void checkIdvStatusForBindCard() {
        ApiProtocolImplManager.getInstance().checkIdForBnindRequest(this, new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

                //DJ321
                onErrorCode(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                if (isQuerySuccess) return;
                isQuerySuccess = true;
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                mIdAuthInfoResponse = response;
                stopTimer();
                if (!frontPageIsWebView()) {
                    goSuccessPageForBindCard(mIdAuthInfoResponse.getMobile());
                } else {
                    EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_SUCCESS, ""));
                }
            }
        });
    }


    /**
     * 异常流程报错 进度条更新到100%
     */
    private void stopTimer() {
        isStopTimer = true;
        updateText(0);
    }

    private void onErrorCode(final String errorCode, String errorMsg) {
        if (isAlreadyJumpPage) return;
        if (!TextUtils.isEmpty(errorCode)) {
            if (errorCode.equals(ErrorCode.CONTENT_TIME_OUT.code) ||
                    errorCode.equals(ErrorCode.CHECK_ID_RETRY.code) ||
                    errorCode.equals(ErrorCode.NETWORK_ERROR.code) ||
                    errorCode.equals(ErrorCode.SERVER_INTER_FAILED.code)) {
                //网络超时情况 继续轮询查询结果
                mCheckHandler.postDelayed(reQurtQrStatusRunnable, 3000);
            } else if (errorCode.equals(ErrorCode.CHECK_ID_ZHONGYIN_CURTOMER.code) || errorCode.equals(ErrorCode.CHECK_ID_ZHONGYIN_CUS.code)) {
                //只会在注册/绑卡情况出现 中银客户
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                isAlreadyJumpPage = true;
                stopTimer();
                if (!frontPageIsWebView()) {
                    showErrorMsgDialog(CheckIdvProgressActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            //已经是中银客户 点击跳转到 s3页面注册
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                            } else {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                            }
                            MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                            ActivitySkipUtil.startAnotherActivity(CheckIdvProgressActivity.this, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                } else {
                    EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_ERROR, errorMsg, errorCode));
                }


            } else if (errorCode.equals(ErrorCode.CHECK_IDV_FAILED.code)) {
                //检查结果异常 结束流程
                sendErrorEvent(errorCode, errorMsg);
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                stopTimer();
                goFailedPage();
            } else {
                stopTimer();
                mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                sendErrorEvent(errorCode, errorMsg);
                //其他错误 报错 点击 退回到上个界面 在当前界面弹框
                if (isAlreadyJumpPage) return;
                isAlreadyJumpPage = true;
                if (!frontPageIsWebView()) {
                    showErrorMsgDialog(CheckIdvProgressActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            boolean isBlackStatus = false;
                            for (int i = 0; i < BLACK_LIST.length; i++) {
                                if (TextUtils.equals(errorCode, BLACK_LIST[i])) {
                                    isBlackStatus = true;
                                    break;
                                }
                            }
                            if (isBlackStatus) {
                                //中了黑名单 只可能注册/绑卡出现 需要跳转到首页
                                MyActivityManager.removeAllTaskExcludeMainStack();
                            } else {
                                if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                                    //注册流程 不管是黑名单 还是已经注册bocpay 都跳转到登录界面
                                    MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                                } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                                    //绑卡 应该只存在黑名单情况 跳转到选择绑卡界面
                                    MyActivityManager.removeAllTaskWithPaBindCard();
                                } else {
                                    //异常 结束流程
                                    mCheckHandler.removeCallbacks(reQurtQrStatusRunnable);
                                    goFailedPageEvent();
                                }
                            }
                        }
                    });
                } else {
                    EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_ERROR_TWO, errorMsg, errorCode));
                }
            }
        }
    }

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.REGISTRATION_WAITING);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    private void goFailedPageEvent() {
        isAnalysis = false;
        isAlreadyJumpPage = true;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        ActivitySkipUtil.startAnotherActivity(this, CheckIdvFailedActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void goFailedPage() {
        if (isAlreadyJumpPage) return;
        if (!frontPageIsWebView()) {
            goFailedPageEvent();
        } else {
            EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_IDV_SHOW_FAILED, ""));
        }
    }

    /**
     * 绑卡成功
     *
     * @param phoneNumber
     */
    private void goSuccessPageForBindCard(String phoneNumber) {
        isAnalysis = false;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(AnalysisParams.CURRENT_ADDRESS, PagerConstant.REGISTRATION_WAITING);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNumber);
        ActivitySkipUtil.startAnotherActivity(this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    /**
     * 注册成功
     */
    private void goS2RegisterSuccessPage() {
        isAnalysis = false;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
        ActivitySkipUtil.startAnotherActivity(this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_VIDEO && resultCode == RESULT_OK) {
            int eventType = data.getExtras().getInt(Constants.EVENT_TYPE);
            if (eventType == RegisterEventEntity.EVENT_IDV_CHECK_SUCCESS) {
                if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                    goS2RegisterSuccessPage();
                } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                    goSuccessPageForBindCard(mIdAuthInfoResponse.getMobile());
                } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                    goS2ForgetPasswordPage();
                }
            } else if (eventType == RegisterEventEntity.EVENT_IDV_CHECK_FAILED) {
                goFailedPageEvent();
            }
        }
    }


    @Override
    public void onBackPressed() {
    }


}