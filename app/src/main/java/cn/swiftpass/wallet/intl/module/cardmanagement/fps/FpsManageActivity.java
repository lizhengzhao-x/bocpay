package cn.swiftpass.wallet.intl.module.cardmanagement.fps;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckRequest;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.STwoRegisterSuccessActiviy;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 2018/10/9.
 * FPS BOC银行记录管理
 */

public class FpsManageActivity extends BaseCompatActivity {


    @BindView(R.id.tv_fps_default_tip)
    TextView mFpsDefaultTipTV;
    @BindView(R.id.ll_fps_default)
    View mFpsDefaultLayout;
    @BindView(R.id.tv_fps_add)
    TextView mFpsAddTV;
    @BindView(R.id.rv_bank_record)
    RecyclerView mBankRecordRV;
    List<FpsBankRecordBean> mFpsRecordList;
    public static final String ACTION_CALL_BACK = "ACTION_CALL_BACK";
    public static final String ACTION_BACK_TO_HOME = "ACTION_BACK_TO_HOME";
    private String action;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C1_01_1_1);
        if (getIntent() != null) {
            action = getIntent().getStringExtra(ACTION_CALL_BACK);
        }
        mFpsAddTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (null == mFpsRecordList || mFpsRecordList.isEmpty()) {
                        FpsBindSelTypeActivity.startActivity(getActivity(), FpsBindSelTypeActivity.TAB_TYPE_NO_LIMIT, true);
                    } else {
                        boolean isDefault = FpsConst.FPS_DEFAULT_YES.equalsIgnoreCase(mFpsRecordList.get(0).getDefaultBank());
                        if (TextUtils.equals(mFpsRecordList.get(0).getAccountIdType(), FpsConst.ACCOUNT_ID_TYPE_MOBILE)) {
                            FpsBindSelTypeActivity.startActivity(getActivity(), FpsBindSelTypeActivity.TAB_TYPE_EMAIL, isDefault);
                        } else if (TextUtils.equals(mFpsRecordList.get(0).getAccountIdType(), FpsConst.ACCOUNT_ID_TYPE_EMAIL)) {
                            FpsBindSelTypeActivity.startActivity(getActivity(), FpsBindSelTypeActivity.TAB_TYPE_MOBILE, isDefault);
                        }
                    }
                }
            }
        });
        mFpsRecordList = new ArrayList<>();
        mBankRecordRV.setAdapter(getAdapter(null));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mBankRecordRV.setLayoutManager(layoutManager);
        setBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isPageFromStwoRegister()) {
            //注册成功 绑定fps会来到这个界面 如果用户绑定fps成功 此时点击back 应该返回首页
            if (mFpsRecordList != null && mFpsRecordList.size() > 0) {
                boolean isBindCardEvent = isBindCardEvent();
                MyActivityManager.removeAllTaskExcludeMainStack();
                ActivitySkipUtil.startAnotherActivity(FpsManageActivity.this, MainHomeActivity.class);
                if (isBindCardEvent) {
                    EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
                } else {
                    EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            } else {
                super.onBackPressed();
            }
        } else if (ACTION_BACK_TO_HOME.equals(action)) {
            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
            finish();
        } else {
            //智能账户管理 fps管理 用户点击back 正常退出流程
            super.onBackPressed();
        }
    }


    /**
     * 根据当前taskStack中是否存在STwoRegisterSuccessActiviy判断是否是从注册成功界面来到此界面
     *
     * @return
     */
    private boolean isPageFromStwoRegister() {
        boolean isFromRegisterPage = false;
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            for (int x = 0, y = taskStack.size(); x < y; x++) {
                Activity activity = taskStack.get(x);
                if (activity != null && (activity instanceof STwoRegisterSuccessActiviy)) {
                    isFromRegisterPage = true;
                    break;
                }
            }
        }
        return isFromRegisterPage;
    }

    /**
     * s2注册成功 s2绑卡是一个公用界面 用于区分流程
     *
     * @return
     */
    private boolean isBindCardEvent() {
        boolean isFromBindCard = false;
        List<Activity> taskStack = ProjectApp.getTaskStack();
        if (taskStack != null && taskStack.size() > 0) {
            for (int x = 0, y = taskStack.size(); x < y; x++) {
                Activity activity = taskStack.get(x);
                if (activity != null && (activity instanceof STwoRegisterSuccessActiviy)) {
                    STwoRegisterSuccessActiviy ac = (STwoRegisterSuccessActiviy) activity;
                    isFromBindCard = ac.isBindCardEvent();
                    break;
                }
            }
        }
        return isFromBindCard;
    }




    @Override
    protected void onResume() {
        super.onResume();
        refreshRecords();
    }

    private void refreshRecords() {
        ApiProtocolImplManager.getInstance().fpsQueryBankRecords(this, new NetWorkCallbackListener<FpsBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(FpsManageActivity.this, errorMsg);
                //mFpsAddTV.setVisibility(View.GONE);
            }

            @Override
            public void onSuccess(FpsBankRecordsEntity response) {
                if (null != mFpsRecordList && !mFpsRecordList.isEmpty()) {
                    mFpsRecordList.clear();
                }
                if (null != response && null != response.getData() && !response.getData().isEmpty()) {
                    mBankRecordRV.setVisibility(View.VISIBLE);
                    mFpsDefaultLayout.setVisibility(View.GONE);
                    mFpsRecordList = response.getData();
                    ((IAdapter<FpsBankRecordBean>) mBankRecordRV.getAdapter()).setData(mFpsRecordList);
                    mBankRecordRV.getAdapter().notifyDataSetChanged();
                } else {
                    mFpsDefaultLayout.setVisibility(View.VISIBLE);
                    mBankRecordRV.setVisibility(View.GONE);
                }
                if (mFpsRecordList.size() > 1) {
                    mFpsAddTV.setVisibility(View.GONE);
                } else {
                    mFpsAddTV.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_manage;
    }


    private CommonRcvAdapter<FpsBankRecordBean> getAdapter(List<FpsBankRecordBean> data) {
        return new CommonRcvAdapter<FpsBankRecordBean>(data) {
            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new FpsBankRecordAdapter(mContext, new FpsBankRecordAdapter.BankRecordCallback() {
                    @Override
                    public void onDelete(int position) {
                        if (null != mFpsRecordList.get(position)) {
                            final FpsBankRecordBean bankBean = mFpsRecordList.get(position);
                            FpsPreCheckRequest request = new FpsPreCheckRequest();
                            request.accountId = bankBean.getAccountId();
                            request.accountIdType = bankBean.getAccountIdType();
                            request.accountNo = bankBean.getAccountNo();
                            request.bankCode = bankBean.getBankCode();
                            request.action = FpsConst.FPS_ACTION_DEL;
                            request.defaultBank = bankBean.getDefaultBank();
                            ApiProtocolImplManager.getInstance().fpsPreCheckDel(getActivity(), request, FpsConst.FPS_RECORD_DEL_TYPE_OTHER, new NetWorkCallbackListener<FpsPreCheckEntity>() {
                                @Override
                                public void onFailed(String errorCode, String errorMsg) {
                                    AndroidUtils.showTipDialog(getActivity(), errorMsg);
                                }

                                @Override
                                public void onSuccess(FpsPreCheckEntity response) {
                                    if (null != response) {
                                        FpsRecordDeleteActivity.startActivity(getActivity(), bankBean, response.getFppRefNo());
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onRecord(int position) {
                        FpsBankRecordBean bean = mFpsRecordList.get(position);
                        Intent intent = new Intent(getActivity(), RegisterInputOTPActivity.class);
                        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
                        //这个accountId,可以是手机、邮箱
                        intent.putExtra(Constants.DATA_PHONE_NUMBER, bean.getAccountId());
                        intent.putExtra(Constants.CURRENT_PAGE_FLOW, Constants.DATA_TYPE_FPS_QUERY);
                        startActivity(intent);
                    }
                });
            }
        };
    }

    public static void startActivity(Activity activity) {
        Intent intent = new Intent(activity, FpsManageActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
    }


}
