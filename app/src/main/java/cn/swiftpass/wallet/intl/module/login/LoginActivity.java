package cn.swiftpass.wallet.intl.module.login;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.PaymentPasswordDialogWithFioFragment;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.LoginCheckEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.entity.event.LoginEventEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.home.constants.MenuItemEnum;
import cn.swiftpass.wallet.intl.module.login.contract.LoginContract;
import cn.swiftpass.wallet.intl.module.login.presenter.LoginPresenter;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSelActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ForgetPaymentPasswordActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardApplyActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardRegisterSelActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static android.view.View.IMPORTANT_FOR_AUTOFILL_NO;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;
import static cn.swiftpass.wallet.intl.module.home.PreLoginActivity.DATA_VIRTUAL_CARD_SHARE_URL;
import static cn.swiftpass.wallet.intl.module.home.PreLoginActivity.DATA_VIRTUAL_CARD_URL;
import static cn.swiftpass.wallet.intl.module.login.SelectLoginCodeActivity.FROM_TYPE;
import static cn.swiftpass.wallet.intl.module.login.SelectLoginCodeActivity.FROM_TYPE_LOGIN;


public class LoginActivity extends BaseCompatActivity<LoginContract.Presenter> implements LoginContract.View {
    @BindView(R.id.etwd_account)
    EditTextWithDel etwdAccount;
    @BindView(R.id.tv_login)
    TextView tvLongin;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.id_sel_county_code)
    ImageView idSelCountyCode;
    @BindView(R.id.id_sel_arrow_code)
    ImageView idSelArrowCode;
    @BindView(R.id.id_tv_code_str)
    TextView idTvCodeStr;
    @BindView(R.id.tv_forgetpwd)
    TextView tvForgetpwd;
    @BindView(R.id.iv_imageLogo)
    ImageView ivImageLogo;
    @BindView(R.id.iv_login_bottom_banner)
    ImageView ivLoginBottomBanner;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.tv_confirm_credit_register)
    TextView creditConfirmRegister;
    @BindView(R.id.tv_account_flag)
    TextView tvAccountFlag;

    @BindView(R.id.id_verifycode_view)
    LinearLayout idVerifycodeView;


    @BindView(R.id.cet_verify_code)
    CustomTvEditText idCerVerifyCode;

    @BindView(R.id.iv_verify_code)
    ImageView mIv_verify_code;


    @BindView(R.id.iv_refresh_code)
    ImageView mRefreshCode;


    private boolean isBackHome;
    public static final String TYPE_BACK_IN_LOGIN = "TYPE_BACK_IN_LOGIN";

    private static final String ACTION = "L";
    private String virtualCardUrl;
    private String shareImageUrl;
//    private String mAppCallApplink = "";
//    private boolean isAppCallAppEvent;
//    private String deepLinkUrl = "";

    /**
     * onResume的时候是否需要刷新验证码
     */
    private boolean isNeedRefreshCode = false;

    /**
     * 涉及到onNewIntent调用 intent会覆盖
     */
    private Intent currentIntent;

    private String phoneNumber;

    private PaymentPasswordDialogWithFioFragment paymentPasswordDialogFragment;

    public static String JEMP_ERRORMSG = "JEMP_ERRORMSG";

    /**
     * 跳转到登录界面是因为异常错误码 用此来做标记 清除异常流程的appcallapp事件
     */
    private String mIsJumpPageWithErrorCode;


    @Override
    protected LoginContract.Presenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    public void init() {
        currentIntent = getIntent();
        idVerifycodeView.setVisibility(View.VISIBLE);
        EventBus.getDefault().register(this);
        //不自动
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                idCerVerifyCode.getEditText().setImportantForAutofill(IMPORTANT_FOR_AUTOFILL_NO);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        setToolBarTitle(R.string.IDV_1_1);
        if (getIntent() != null) {
            isBackHome = getIntent().getBooleanExtra(TYPE_BACK_IN_LOGIN, false);
            virtualCardUrl = getIntent().getStringExtra(DATA_VIRTUAL_CARD_URL);
            shareImageUrl = getIntent().getStringExtra(DATA_VIRTUAL_CARD_SHARE_URL);
            //TODO 这里有一个bug,如果因为流程异常错误码导致切换到这个界面 appcallapp事件要清除掉？
            mIsJumpPageWithErrorCode = getIntent().getStringExtra(JEMP_ERRORMSG);
            if (!TextUtils.isEmpty(mIsJumpPageWithErrorCode)) {
                //APPCALLAPP 很多错误码 app call app流程中 /登出
                if (!TextUtils.isEmpty(TempSaveHelper.getMerchantLink())) {
                    TempSaveHelper.clearLink();
                    UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
                }
            }
/*            isAppCallAppEvent = getIntent().getBooleanExtra(Constants.ISAPPCALLAPP, false);
            deepLinkUrl = getIntent().getStringExtra(Constants.DETAIL_URL);
            if (isAppCallAppEvent) {
                mAppCallApplink = getIntent().getStringExtra(Constants.DETAIL_URL);
            } else {
                //APPCALLAPP 很多错误码 app call app流程中 /登出
                if (!TextUtils.isEmpty(TempSaveHelper.getMerchantLink())) {
                    TempSaveHelper.clearLink();
                }
            }*/
        }
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_sc);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_tc);
        } else {
            ivLoginBottomBanner.setImageResource(R.mipmap.bg_banner1_en);
        }
        etwdAccount.hideErrorView();
        etwdAccount.setLineVisible(false);
        etwdAccount.hideDelView();
        updateOkBackground(false);
        etwdAccount.addTextChangedListener(textWatcher);
        idSelCountyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAreaCode();
            }
        });

        idCerVerifyCode.getEditText().addTextChangedListener(onTextChangedListener);
        idCerVerifyCode.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        ivLoginBottomBanner.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (!TextUtils.isEmpty(virtualCardUrl)) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                    mHashMaps.put(WebViewActivity.NEW_MSG, true);
                    mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    mPresenter.getInitData(MenuItemEnum.VIRTUAL_CARD.action);
                }
            }
        });

        idSelArrowCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAreaCode();
            }
        });

        idTvCodeStr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAreaCode();
            }
        });


        idSelCountyCode.setImageResource(Constants.DATA_COUNTRY_CODES_RESOURCE[1]);
        idTvCodeStr.setText(Constants.DATA_COUNTRY_CODES[1]);
        etwdAccount.setBottomLineHighSel();

        etwdAccount.getEditText().setTextSize(15);
        if (!Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
            ivImageLogo.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    ActivitySkipUtil.startAnotherActivity(LoginActivity.this, TestActivity.class);
                    return false;
                }
            });
        }


        if (BuildConfig.INCLUDE_VITUAL_ACTION) {
            //虚拟卡
            creditConfirmRegister.setText(getString(R.string.LI10_1_1));
            creditConfirmRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ButtonUtils.isFastDoubleClick()) return;
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    ActivitySkipUtil.startAnotherActivity(LoginActivity.this, VirtualCardRegisterSelActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
        } else {
            //之前信用卡快速入口注册
            creditConfirmRegister.setText(getString(R.string.CCA01_01_1));
            creditConfirmRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ButtonUtils.isFastDoubleClick()) return;
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                    ActivitySkipUtil.startAnotherActivity(LoginActivity.this, InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
        }

        if (mPresenter != null) {
            mPresenter.getVerifyCode();
        }
    }


    private TextWatcher onTextChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            updateEditFlag();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        if (isNeedRefreshCode) {
            if (mPresenter != null) {
                mPresenter.getVerifyCode();
                isNeedRefreshCode = false;
            }
        }
    }

    /*    */

    /**
     * app call app
     *
     * @param intent
     *//*
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        currentIntent = intent;
        if (intent != null) {
            isAppCallAppEvent = intent.getBooleanExtra(Constants.ISAPPCALLAPP, false);
            deepLinkUrl = intent.getStringExtra(Constants.DETAIL_URL);
            if (isAppCallAppEvent) {
                mAppCallApplink = intent.getStringExtra(Constants.DETAIL_URL);
            } else {
                //APPCALLAPP 很多错误码 app call app流程中 /登出
                if (!TextUtils.isEmpty(TempSaveHelper.getMerchantLink())) {
                    TempSaveHelper.clearLink();
                }
            }
        }
    }*/
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        currentIntent = intent;
        if (intent != null) {
            mIsJumpPageWithErrorCode = intent.getStringExtra(JEMP_ERRORMSG);
            if (!TextUtils.isEmpty(mIsJumpPageWithErrorCode)) {
                //APPCALLAPP 很多错误码 app call app流程中 /登出
                if (!TextUtils.isEmpty(TempSaveHelper.getMerchantLink())) {
                    TempSaveHelper.clearLink();
                    UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
                }
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(LoginEventEntity event) {
        if (event.getEventType() == LoginEventEntity.EVENT_KEY_DISMISS_PWD) {
            updateView();
        } else if (event.getEventType() == LoginEventEntity.EVENT_LOGIN_OVER_TIME_CODE) {
            //操作逾时，EWA5632 5633 5634
            updateView();
        }
    }

    private void updateView() {
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismiss();
        }
        clearEditText();
        idCerVerifyCode.setContentText("");
        if (mPresenter != null) {
            mPresenter.getVerifyCode();
        }
    }


    private void changeAreaCode() {
        if (ButtonUtils.isFastDoubleClick()) return;
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.LAST_SELECT_COUNTRY, idTvCodeStr.getText().toString());
        mHashMapsLogin.put(FROM_TYPE, FROM_TYPE_LOGIN);
        ActivitySkipUtil.startAnotherActivityForResult(LoginActivity.this, SelectLoginCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_COUNTRY);
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateEditFlag();
        }
    };

    private void updateEditFlag() {
        //对输入的数字，每四位加一个空格
        //获取输入框中的内容,不可以去空格
        String etContent = etwdAccount.getEditText().getText().toString();
        if (TextUtils.isEmpty(etContent)) {
            tvAccountFlag.setText("");
            updateOkBackground(false);
            return;
        }
        String countryCode = idTvCodeStr.getText().toString().trim();
        if (!AndroidUtils.validatePhoneRegister(countryCode, etContent)) {
            //如果手机号超过长度才需要显示tip
            if (!AndroidUtils.validatePhone(countryCode, etContent)) {
                tvAccountFlag.setText(getString(R.string.LG2101_940_1));
            }
            updateOkBackground(false);
        } else {
            int codeLength = idCerVerifyCode.getInputText().length();
            if (codeLength == 4) {
                updateOkBackground(true);
            } else {
                updateOkBackground(false);
            }

            tvAccountFlag.setText("");
        }
    }

    private void updateOkBackground(boolean isSel) {
        tvLongin.setEnabled(isSel);
        tvLongin.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    @Override
    public void getInitDataSuccess(SystemPagerDataEntity response, String action) {
        if (response != null) {
            virtualCardUrl = response.getVirtualCardUrl(this);
            shareImageUrl = response.getVirtualCardShareUrl(this);
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
            mHashMaps.put(WebViewActivity.NEW_MSG, true);
            mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
            ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void getVerifyCodeFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }

    @Override
    public void getVerifyCodeSuccess(BankLoginVerifyCodeEntity codeEntity) {
        int codeLen = 8;
        try {
            codeLen = Integer.valueOf(codeEntity.getVerifyCodeLength());
        } catch (Exception e) {
        }
        try {
            String bmpCode = codeEntity.getImageCode();
            Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
            mIv_verify_code.setImageBitmap(bmp);
            idCerVerifyCode.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
            idCerVerifyCode.setContentText("");
        } catch (Exception e) {
        }
    }

    /**
     * 验证  验证码  失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void verifyCodeFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                //EWA5630 账户资料错误5次，已经被锁
                //EWA5632 5633 5634 操作逾时（10分钟）
                if (TextUtils.equals(errorCode, "EWA5630") || TextUtils.equals(errorCode, "EWA5632") || TextUtils.equals(errorCode, "EWA5633") || TextUtils.equals(errorCode, "EWA5634")) {
                    updateView();
                }
            }
        });
        if (mPresenter != null) {
            mPresenter.getVerifyCode();
        }
    }

    @Override
    public void verifyCodeSuccess() {
        initPwdWindow();
    }

    private void initPwdWindow() {
        paymentPasswordDialogFragment = new PaymentPasswordDialogWithFioFragment();
        PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener() {
            @Override
            public void onPwdCompleteListener(String psw, boolean complete) {
                if (complete) {
                    paymentPasswordDialogFragment.showProgressDialog();
                    mPresenter.verifyPwd(psw, phoneNumber);
                }
            }

            @Override
            public void onPwdBackClickListener() {
                if (mPresenter != null) {
                    mPresenter.getVerifyCode();
                }
            }

            @Override
            public void onPwdFailed(String errorCode, String errorMsg) {

            }
        };
        paymentPasswordDialogFragment.initParams(onPwdDialogClickListener, false);
        paymentPasswordDialogFragment.show(getSupportFragmentManager(), "PaymentPasswordDialogWithFioFragment");
    }

    @Override
    public void dismissDialog() {
        super.dismissDialog();
        //有一种场景是错误码EWA5600 顶层拦截 需要取消加载框 这里是dialog需要特殊处理
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismissProgressDialog();
            paymentPasswordDialogFragment.clearPwd();
        }
    }

    /**
     * 验证  密码  失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void verifyPwdFail(String errorCode, String errorMsg) {
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismissProgressDialog();
        }

        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (paymentPasswordDialogFragment != null) {
                    paymentPasswordDialogFragment.clearPwd();
                }
                //EWA5630 账户资料错误5次，已经被锁
                //EWA5632 5633 5634 操作逾时（10分钟）
                if (TextUtils.equals(errorCode, "EWA5630") || TextUtils.equals(errorCode, "EWA5632") || TextUtils.equals(errorCode, "EWA5633") || TextUtils.equals(errorCode, "EWA5634")) {
                    updateView();
                }
            }
        });
    }

    @Override
    public void verifyPwdSuccess(LoginCheckEntity loginCheckEntity) {
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismiss();
        }
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_CARD_TYPE_LOGIN);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, phoneNumber);
        if (null != currentIntent && null != currentIntent.getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS)) {
            mHashMapsLogin.put(RED_PACKET_RED_PACKET_PARAMS, currentIntent.getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
        }
//        if (!TextUtils.isEmpty(mAppCallApplink)) {
//            mHashMapsLogin.put(AppCallAppLinkEntity.APP_LINK_URL, mAppCallApplink);
//        } else {
//            mHashMapsLogin.put(AppCallAppLinkEntity.APP_LINK_URL, deepLinkUrl);
//        }
        ActivitySkipUtil.startAnotherActivity(LoginActivity.this, RegisterInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        isNeedRefreshCode = true;
    }

    @Override
    public void forgetPwdDeviceStatusFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }

    @Override
    public void forgetPwdDeviceStatusSuccess(ForgetPasswordEntity response) {
        ActivitySkipUtil.startAnotherActivity(getActivity(), ForgetPaymentPasswordActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getInitDataFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_login;
    }


    @Override
    public void onBackPressed() {
        if (isBackHome) {
            ActivitySkipUtil.startAnotherActivity(LoginActivity.this, PreLoginActivity.class);
            MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
        } else if (UserLoginEventManager.getInstance().isLeaveAppCallAppEvent()) {
            AppCallAppUtils.exitMerchantApp(this, false, false);
        } else if (!MyActivityManager.isPreLoginExistInStack()) {
            //PreLogin 是否在栈中,如果栈中没有PreLogin界面，Login界面回退将出现错误的页面跳转，或者直接退出的APP
            ActivitySkipUtil.startAnotherActivity(LoginActivity.this, PreLoginActivity.class, ActivitySkipUtil.ANIM_TYPE.LEFT_IN);
            MyActivityManager.removeAllTaskExcludePreLoginStack();
        } else {
            super.onBackPressed();
        }
    }


    @OnClick({R.id.tv_login, R.id.tv_register, R.id.iv_refresh_code})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) return;
        switch (view.getId()) {
            case R.id.tv_login:
                if (mPresenter != null) {
                    String code = (idTvCodeStr.getText().toString().trim().replace("+", "") + "-").trim();
                    phoneNumber = code + etwdAccount.getEditText().getText().toString().trim();
                    mPresenter.verifyCode(idCerVerifyCode.getInputText());
                }
                break;
            case R.id.tv_register:

                if (ButtonUtils.isFastDoubleClick()) return;
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
                ActivitySkipUtil.startAnotherActivity(this, RegisterSelActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.iv_refresh_code:
                if (mPresenter != null) {
                    mPresenter.getVerifyCode();
                }
            default:
                break;
        }
    }

    public void clearEditText() {
        if (etwdAccount == null) return;
        etwdAccount.getEditText().setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_SEL_COUNTRY && resultCode == RESULT_OK) {
            String countryCode = data.getExtras().getString(Constants.DATA_COUNTRY_CODE);
            //如果区号和之前区号不同 需要重新判断手机号是否正确
            if (countryCode != null && !countryCode.equals(idTvCodeStr.getText().toString().trim())) {
                idTvCodeStr.setText(countryCode);
                //每次切换区号都要重新判断手机号是否匹配规则
                updateEditFlag();
            }
        }
    }


    @OnClick(R.id.tv_forgetpwd)
    public void onViewClicked() {
        if (ButtonUtils.isFastDoubleClick()) return;
        //检测设备状态
        mPresenter.forgetPwdDeviceStatus();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
