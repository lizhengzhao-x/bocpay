package cn.swiftpass.wallet.intl.module.staging.contract;

import android.app.Activity;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.StagingItemEntity;

/**
 * 获取信用卡分期信息
 */
public class StagingItemContract {


    public interface View extends IView {

        void queryStagesInfoSuccess(StagingItemEntity response);

        void queryStagesInfoError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<StagingItemContract.View> {

        void queryStagesInfo(String key);
        void dealWithHtmlShow(String content, TextView tv, Activity activity);

    }

}
