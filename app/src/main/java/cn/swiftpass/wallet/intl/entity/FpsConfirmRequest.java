package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class FpsConfirmRequest extends BaseEntity {
    //账户 手机号或者邮箱
    public String accountId;
    //账户类型 0：手机号 1：邮箱
    public String accountIdType;
    //是否默认收款银行 Y:是 N:否
    public String defaultBank;
    //收款账户 action = A 或者 U的时候必传
    public String accountNo;
    //银行代码 action = D 必传
    public String bankCode;
    // A:添加 U:修改 D:删除
    public String action;
    //FPP唯一标识 check的时候会返回
    public String fppRefNo;
}
