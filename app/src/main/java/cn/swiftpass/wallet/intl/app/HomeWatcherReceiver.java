package cn.swiftpass.wallet.intl.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * 主要监听home按键 需要退出被扫 主扫
 */

public class HomeWatcherReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "HomeReceiver";
    private static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    private static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
    private static final String SYSTEM_DIALOG_REASON_LOCK = "lock";
    private static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";
    // 红包弹出 黑屏显示的是这个 type
    private static final String SYSTEM_DIALOG_REASON_RED_DIALOG_KEY = "dream";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        LogUtils.i(LOG_TAG, "onReceive: action: " + action);
        if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
            // android.intent.action.CLOSE_SYSTEM_DIALOGS
            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
            LogUtils.i(LOG_TAG, "reason: " + reason);
            if (SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {
                // 短按Home键
                LogUtils.i(LOG_TAG, "home key");
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER, ""));
            } else if (SYSTEM_DIALOG_REASON_RECENT_APPS.equals(reason)) {
                // 长按Home键 或者 activity切换键
                LogUtils.i(LOG_TAG, "long press home key or activity switch");
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER, ""));
            } else if(SYSTEM_DIALOG_REASON_RED_DIALOG_KEY.equals(reason)){
                // 锁屏
                LogUtils.i(LOG_TAG, "screen lock");
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER, ""));
            }else if (SYSTEM_DIALOG_REASON_LOCK.equals(reason)) {
                // 锁屏
                LogUtils.i(LOG_TAG, "screen lock");
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER, ""));
            } else if (SYSTEM_DIALOG_REASON_ASSIST.equals(reason)) {
                // samsung 长按Home键
                LogUtils.i(LOG_TAG, "assist");
            } else if (Intent.ACTION_USER_PRESENT.equals(action)) {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER, ""));
            }
        }

    }
}
