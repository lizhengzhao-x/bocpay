package cn.swiftpass.wallet.intl.module.rewardregister.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 信用卡消费奖赏列表
 */
public class GetRewardRegistrationListProtocol extends BaseProtocol {

    public GetRewardRegistrationListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "parameter/getValue";
        mEncryptFlag = false;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.NAME, "CREDIT_CARD_REGISTER_REWARD");
    }
}
