package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;

/**
 页面初始化数据
 */

public class SystemPageInitDataProtocol extends BaseProtocol {

    public SystemPageInitDataProtocol(NetWorkCallbackListener<SystemPagerDataEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/preLogin/initData";
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }

}
