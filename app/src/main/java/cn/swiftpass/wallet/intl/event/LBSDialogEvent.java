package cn.swiftpass.wallet.intl.event;

import android.app.Activity;

import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_LBSDIALOG;

public class LBSDialogEvent extends BaseEventType {
    public static final String LBSDIALOGEVENTNAME = "LBSDialogEvent";

    public LBSDialogEvent() {
        super(EVENT_LBSDIALOG.eventPriority, EVENT_LBSDIALOG.eventName, null);
    }

    @Override
    protected String getCurrentEventType() {
        return LBSDIALOGEVENTNAME;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof LBSDialogDealOwner) {
            LBSDialogDealOwner lbsDialogDealOwner = (LBSDialogDealOwner) currentActivity;
            lbsDialogDealOwner.dealLBSDialog();
        }
        return true;
    }

    public interface LBSDialogDealOwner extends EventDealOwner {
        void dealLBSDialog();
    }
}
