package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 获取FIO的CIN,返回FioInitEntity
 */

public class FioInitCinProtocol extends BaseProtocol {

    String mWalletId;
    //R注册流程  S 设定流程
    String fioRegisterType;

    public FioInitCinProtocol(String walletId, String type, NetWorkCallbackListener dataCallback) {
        mWalletId = walletId;
        fioRegisterType = type;
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/getAsKey";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        mBodyParams.put(RequestParams.FIO_REG_TYPE, fioRegisterType);
    }
}
