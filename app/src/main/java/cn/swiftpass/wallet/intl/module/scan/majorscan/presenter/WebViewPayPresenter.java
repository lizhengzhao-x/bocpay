package cn.swiftpass.wallet.intl.module.scan.majorscan.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.ActExtQrInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GenerateAuthTokenProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetPayTypeListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTradeStatusProtocol;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.AuthTokenEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.WebViewPayContract;
import cn.swiftpass.wallet.intl.utils.LogUtils;

public class WebViewPayPresenter extends BasePresenter<WebViewPayContract.View> implements WebViewPayContract.Presenter {
    @Override
    public void generateAuthToken(String cardId, final String url) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GenerateAuthTokenProtocol(cardId, new NetWorkCallbackListener<AuthTokenEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().generateAuthTokenError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AuthTokenEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    //webView加载拼接好的redirectUrl+userToken
                    //xxxx&userToken=xxxx&respCode=xxxx
                    //https://qr.95516.com/qrcGtwWebweb/api/userAuth?version=1.0.0&payOrderInfo=xxxx&redirectUrl=xxxx
                    String respCode = response.getRespCode();
                    String userToken = response.getUserToken();
                    String urlResult = url + "&userAuthCode=" + userToken + "&respCode=" + respCode;
                    LogUtils.i("WebViewPayPresenter", "urlResult: " + urlResult);
                    getView().generateAuthTokenSuccess(urlResult);
                }
            }
        }).execute();
    }

    @Override
    public void actExtQrInfo(String payOrderInfo, String cardId) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
//        HashMap<String, String> maps = new HashMap<>();
//        String[] params = payOrderInfo.split("&");
//        for (int i = 0; i < params.length; i++) {
//            String[] itemParams = params[i].split("=");
//            maps.put(itemParams[0], itemParams[1]);
//        }
        new ActExtQrInfoProtocol(cardId, payOrderInfo, new NetWorkCallbackListener<ActionTrxGpInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().actExtQrInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ActionTrxGpInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().actExtQrInfoSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void getMainScanOrder(MainScanOrderRequestEntity orderRequest) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MainScanOrderProtocol(orderRequest, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getPayMentResult(String cardId, String cpQrCode, String txnId) {
        new QueryTradeStatusProtocol(cardId, cpQrCode, txnId, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                getView().getPayMentResultError(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
                getView().getPayMentResultSuccess(response);
            }
        }).execute();
    }

    @Override
    public void getPaymentTypeList(String qrcode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetPayTypeListProtocol(qrcode, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListSuccess(response.getRows());
                }
            }
        }).execute();
    }


}
