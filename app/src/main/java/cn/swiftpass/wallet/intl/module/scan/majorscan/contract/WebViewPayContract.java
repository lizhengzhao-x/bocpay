package cn.swiftpass.wallet.intl.module.scan.majorscan.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;


public class WebViewPayContract {

    public interface View extends IView {

        void generateAuthTokenError(String errorCode, String errorMsg);

        void generateAuthTokenSuccess(String url);

        void actExtQrInfoError(String errorCode, String errorMsg);

        void actExtQrInfoSuccess(ActionTrxGpInfoEntity content);


        /**
         * 主扫成功
         *
         * @param response
         */
        void getMainScanOrderSuccess(ContentEntity response);

        /**
         * 主扫下单报错
         *
         * @param errorCode
         * @param errorMsg
         */
        void getMainScanOrderError(String errorCode, String errorMsg);

        void getPayMentResultSuccess(PaymentEnquiryResult response);

        void getPayMentResultError(String errorCode, String errorMsg);

        void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards);

        void getPaymentTypeListError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<WebViewPayContract.View> {

        /**
         * server生成userAuthToken
         *
         * @param cardId
         */
        void generateAuthToken(String cardId, String url);

        /**
         * 解析收单机构生成的订单信息，并验签，若成功，则生成ewa订单信息入ewa.txn_order表，返回客户端订单信息
         *
         * @param payOrderInfo
         * @param cardId
         */
        void actExtQrInfo(String payOrderInfo, String cardId);

        /**
         * 主扫下单
         *
         * @param orderRequest
         */
        void getMainScanOrder(MainScanOrderRequestEntity orderRequest);

        /**
         * 主扫下单轮训接口
         *
         * @param cardId
         * @param cpQrCode
         * @param txnId
         */
        void getPayMentResult(String cardId, String cpQrCode, String txnId);

        /**
         * 拉取当前二维码支付的卡列表
         *
         * @param qrcode
         */
        void getPaymentTypeList(String qrcode);


    }

}
