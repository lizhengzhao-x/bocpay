package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/7/16.
 * 兑换代金券 加减号
 */
public class AddVouchersView extends LinearLayout {

    private Context mContext;
    private ImageView mId_ecoupon_reduce;
    private TextView mId_ecoupon_text;
    private ImageView mId_ecoupon_add;
    private int mMaxCount, mMinCount;
    private int mCurrentCount;

    public void setOnItemEcouponCountChangeListener(OnItemEcouponCountChangeListener onItemEcouponCountChangeListener) {
        this.onItemEcouponCountChangeListener = onItemEcouponCountChangeListener;
    }

    private OnItemEcouponCountChangeListener onItemEcouponCountChangeListener;

    public AddVouchersView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }


    public AddVouchersView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    public AddVouchersView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, defStyle);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.vouchers_add_view, this);
        mId_ecoupon_reduce = (ImageView) rootView.findViewById(R.id.id_ecoupon_reduce);
        mId_ecoupon_text = (TextView) rootView.findViewById(R.id.id_ecoupon_text);
        mId_ecoupon_add = (ImageView) rootView.findViewById(R.id.id_ecoupon_add);

        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.AddVourcherView, defStyle, 0);

        mMaxCount = a.getInt(R.styleable.AddVourcherView_max_count, 100);
        mMinCount = a.getInt(R.styleable.AddVourcherView_min_count, 0);
        a.recycle();
        mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min_notavailable);
        mId_ecoupon_reduce.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                reduceEvent();
            }
        });
        mId_ecoupon_add.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                addEvent();
            }
        });


        mId_ecoupon_text.setOnClickListener(new OnProhibitFastClickListener() {

            @Override
            public void onFilterClick(View v) {
                if (onItemEcouponCountChangeListener != null) {
                    onItemEcouponCountChangeListener.onEditTextValue();
                }
            }
        });

    }

    public int getCurrentCount() {
        return mCurrentCount;
    }

    private void reduceEvent() {
        if (mCurrentCount <= mMinCount + 1) {
            mId_ecoupon_text.setText("0");
            if (mCurrentCount > 0) {
                mCurrentCount--;
            }
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min_notavailable);
            onItemEcouponCountChangeListener.onItemEcouponCountChanged(mCurrentCount, -1);
            return;
        }
        mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min);
        mCurrentCount--;
        mId_ecoupon_text.setText(mCurrentCount + "");
        mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus);
        mId_ecoupon_add.setEnabled(true);
        if (onItemEcouponCountChangeListener != null) {
            onItemEcouponCountChangeListener.onItemEcouponCountChanged(mCurrentCount, -1);
        }
    }

    private void addEvent() {
        mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min);
        if (!mId_ecoupon_reduce.isShown()) {
            mId_ecoupon_reduce.setVisibility(VISIBLE);
            mId_ecoupon_text.setVisibility(VISIBLE);
        }
        if (mCurrentCount >= mMaxCount) {
            // mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus_notavailable);
            if (onItemEcouponCountChangeListener != null) {
                onItemEcouponCountChangeListener.onReachMaxValue();
            }
            return;
        }
        mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus);
        mCurrentCount++;
        mId_ecoupon_text.setText(mCurrentCount + "");
        if (onItemEcouponCountChangeListener != null) {
            onItemEcouponCountChangeListener.onItemEcouponCountChanged(mCurrentCount, -1);
        }
    }

    public void setEnabledStatus(boolean enabled) {
        if (enabled) {
            mId_ecoupon_reduce.setEnabled(true);
            mId_ecoupon_add.setEnabled(true);
            mId_ecoupon_text.setTextColor(Color.parseColor("#000000"));
        } else {
            mId_ecoupon_reduce.setEnabled(false);
            mId_ecoupon_add.setEnabled(false);
            mId_ecoupon_text.setTextColor(Color.parseColor("#CCCCCC"));
        }
    }

    public void setEnabledStatusWithZero(boolean enabled) {
        if (enabled) {
            mId_ecoupon_text.setEnabled(true);
            mId_ecoupon_reduce.setEnabled(true);
            mId_ecoupon_add.setEnabled(true);
            mId_ecoupon_text.setTextColor(Color.parseColor("#000000"));
        } else {
            mId_ecoupon_text.setEnabled(false);
            mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus_notavailable);
            mId_ecoupon_reduce.setEnabled(false);
            mId_ecoupon_add.setEnabled(false);
            mId_ecoupon_text.setTextColor(Color.parseColor("#CCCCCC"));
        }

    }

    public void setEnabledEditAddStatus(boolean enabled) {
        if (enabled) {
            mId_ecoupon_add.setEnabled(true);
            mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus);
        } else {
            mId_ecoupon_add.setEnabled(false);
            mId_ecoupon_add.setImageResource(R.mipmap.icon_card_plus_notavailable);
        }
    }

    public void setEnabledEditReduceStatus(boolean enabled) {
        if (enabled) {
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min);
            mId_ecoupon_reduce.setEnabled(true);
        } else {
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min_notavailable);
            mId_ecoupon_reduce.setEnabled(false);
        }
    }

    public void setCurrentCnt(int currentSelCnt) {
        mCurrentCount = currentSelCnt;
        mId_ecoupon_reduce.setVisibility(VISIBLE);
        mId_ecoupon_text.setVisibility(VISIBLE);
        mId_ecoupon_text.setText(mCurrentCount + "");
        if (mCurrentCount > 0) {
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min);
        } else {
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min_notavailable);
        }
    }

    public void updateCurrentCnt(int currentSelCnt) {
        mCurrentCount = currentSelCnt;
        if (currentSelCnt > 0) {
            mId_ecoupon_reduce.setVisibility(VISIBLE);
            mId_ecoupon_text.setVisibility(VISIBLE);
            mId_ecoupon_text.setText(mCurrentCount + "");
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min);
        } else {
            mId_ecoupon_text.setText("0");
            mId_ecoupon_reduce.setImageResource(R.mipmap.icon_card_min_notavailable);
        }
    }

    public void setMaxCount(String geteVoucherStock) {
        int maxCount = 0;
        try {
            maxCount = Integer.valueOf(geteVoucherStock);
        } catch (NumberFormatException e) {
            if (!BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        mMaxCount = maxCount;
    }

    public interface OnItemEcouponCountChangeListener {

        void onItemEcouponCountChanged(int count, int positon);

        void onItemCheckMore(int position);

        void onReachMaxValue();

        void onEditTextValue();
    }
}
