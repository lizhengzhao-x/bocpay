package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.register.RegisterSetPaymentPswActivity;
import cn.swiftpass.wallet.intl.sdk.safekeyboard.SafeKeyNumberBoard;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.PasswordInputView;


public class OldPaymentPswActivity extends BaseCompatActivity implements PasswordInputView.OnFinishListener {
    @BindView(R.id.etwd_payment_psw)
    PasswordInputView passwordView;

    private SafeKeyNumberBoard safeKeyboardLeft;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {


        setToolBarTitle(R.string.setting_payment_rgw);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (passwordView.getText().toString().trim().length() < 6) {
                        showErrorMsgDialog(mContext, getString(R.string.payment_psw_instruction), new OnMsgClickCallBack() {

                            @Override
                            public void onBtnClickListener() {
                                showKeyBoard();
                            }
                        });
                    }
                    return true;
                }
                return false;
            }
        });
        AndroidUtils.forbidCopyForEditText(passwordView);
        passwordView.setOnFinishListener(this);
        passwordView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        //监听验证码是否够6位 去校验otp接口
        initKeyBoard();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //软键盘弹出 用户手势滑动的时候 隐藏软键盘
                if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()
                        && !AndroidUtils.inRangeOfView(safeKeyboardLeft.getKeyboardView(), ev)
                        && !AndroidUtils.inRangeOfView(passwordView, ev)) {
                    safeKeyboardLeft.hideKeyboard();
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (passwordView==null) return;
                passwordView.setFocusable(true);
                passwordView.setFocusableInTouchMode(true);
                passwordView.requestFocus();
//                AndroidUtils.showKeyboard(OldPaymentPswActivity.this, passwordView);
            }
        }, 100);
    }


    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
            safeKeyboardLeft.hideKeyboard();
            safeKeyboardLeft = null;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD || event.getEventType() == PasswordEventEntity.EVENT_RESET_PWD_SUCCESS) {
            finish();
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_old_payment;
    }


    @Override
    public void setOnPasswordFinished() {
        if (passwordView.getOriginText().length() == passwordView.getMaxPasswordLength()) {
            final String currentPwd = passwordView.getOriginText();
            ApiProtocolImplManager.getInstance().getCheckPassword(mContext, currentPwd, new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (!TextUtils.isEmpty(errorMsg)) {
                        showErrorMsgDialog(OldPaymentPswActivity.this, errorMsg, new OnMsgClickCallBack() {
                            @Override
                            public void onBtnClickListener() {
                                passwordView.setText("");
                                showKeyBoard();
                            }
                        });
                    } else {
                        //验证失败弹出软键盘 清除密码
                        passwordView.setText("");
                        showKeyBoard();
                    }
                }

                @Override
                public void onSuccess(BaseEntity response) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                    mHashMaps.put(Constants.DATA_OLD_PAY_PWD, currentPwd);
                    ActivitySkipUtil.startAnotherActivity(OldPaymentPswActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    passwordView.setText("");
                }
            });

        }

    }


    private void initKeyBoard() {
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.number_keyboard_containor, null);
        safeKeyboardLeft = new SafeKeyNumberBoard(getApplicationContext(), keyboardContainer, passwordView, R.layout.number_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId());
        safeKeyboardLeft.setDelDrawable(this.getResources().getDrawable(R.drawable.icon_del));
    }



/*    // 当点击返回键时, 如果软键盘正在显示, 则隐藏软键盘并是此次返回无效
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (safeKeyboardLeft!=null &&safeKeyboardLeft.isShow()) {
                safeKeyboardLeft.hideKeyboard();
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }*/
}
