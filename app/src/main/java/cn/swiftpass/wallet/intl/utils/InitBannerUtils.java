package cn.swiftpass.wallet.intl.utils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;

public class InitBannerUtils {

    public interface LoadBannerListener {
        void onLoadBannerSuccess();
    }

    /**
     * banner全局获取 有可能失败的情况 需要重新触发拉取
     * @param loadBannerListener
     */
    public static void loadDetailBanners(LoadBannerListener loadBannerListener) {
        if (TempSaveHelper.isInitBannerUrlSuccess()) {
            if (loadBannerListener != null) {
                loadBannerListener.onLoadBannerSuccess();
            }
        } else {
            ApiProtocolImplManager.getInstance().getInitBannerUrls(new NetWorkCallbackListener<InitBannerEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                }

                @Override
                public void onSuccess(InitBannerEntity response) {
                    TempSaveHelper.setInitBannerUrlParams(response, true);
                    if (loadBannerListener != null) {
                        loadBannerListener.onLoadBannerSuccess();
                    }
                }
            });
        }
    }
}
