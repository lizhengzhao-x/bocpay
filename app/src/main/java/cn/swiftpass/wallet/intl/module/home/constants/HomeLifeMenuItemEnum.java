package cn.swiftpass.wallet.intl.module.home.constants;

import android.content.Context;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.HomeLifeMenuEntity;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.CreditCardRewardFragment;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.TransferCrossBorderPayeeFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.view.ECouponsManagerFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteShareFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.FPSPaymentScanFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferRedPacketFragment;

/**
 * @author fan.zhang
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.home.constants
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 17:46
 * @change
 * @chang time
 * @class describe
 */
public enum HomeLifeMenuItemEnum {

    //已使用的入口
    //UPLAN
    UPLAN(MenuKey.UPLAN, R.string.H2107_1_2, R.mipmap.icon_circle_uplan, R.mipmap.icon_circle_uplan, UplanActivity.class, false),
    //电子券
    ECOUPON(MenuKey.ECOUPON, R.string.HE2101_1_2, R.mipmap.icon_circle_evoucher, R.mipmap.icon_circle_evoucher, ECouponsManagerFragment.class),
    //新年派利是
    NEW_YEAR_RED_POCKET(MenuKey.NEW_YEAR_RED_POCKET, R.string.HE2101_1_4, R.mipmap.icon_circle_redpacket, R.mipmap.icon_circle_redpacket, TransferRedPacketFragment.class),
    //FPS 缴费
    PAYMENT_BY_FPS(MenuKey.PAYMENT_BY_FPS, R.string.HE2101_1_5, R.mipmap.icon_circle_fpsbillpayment, R.mipmap.icon_circle_fpsbillpayment, FPSPaymentScanFragment.class),
    //推荐亲友
    INVITE_SHARE(MenuKey.INVITE_SHARE, R.string.HE2101_1_3, R.mipmap.icon_circle_invitation_a, R.mipmap.icon_circle_invitation_a, InviteShareFragment.class),
    //信用卡奖赏
    CREDIT_CARD_REWARD(MenuKey.CREDIT_CARD_REWARD, R.string.H2107_1_1, R.mipmap.icon_livingarea_cardrewards, R.mipmap.icon_livingarea_cardrewards, CreditCardRewardFragment.class),
    //跨境汇款
    CROSS_TRANSFER(MenuKey.CROSS_TRANSFER, R.string.M10_1_15, R.mipmap.icon_livingarea_crossborder, R.mipmap.icon_livingarea_crossborder, TransferCrossBorderPayeeFragment.class);

    //未使用的入口 使用入口时，需要设置item图片 以及跳转事件
   /* HOME(MenuKey.HOME, R.string.M10_1_1, R.mipmap.icon_sidemenu_home, R.mipmap.icon_sidemenu_home, MainHomeActivity.class),
    //我的账户
    MY_ACCOUNT(MenuKey.MY_ACCOUNT, R.string.M10_1_2, R.mipmap.icon_sidemenu_myacc, R.mipmap.icon_sidemenu_myacc, NewCardManagerFragment.class),
    //虚拟卡
    VIRTUAL_CARD(MenuKey.VIRTUAL_CARD, R.string.M10_1_3, R.mipmap.icon_sidemenu_virtualcard, R.mipmap.icon_sidemenu_virtualcard_on, MainHomeActivity.class),
    //收款或者付款
    PAYMENT_OR_COLLECT(MenuKey.PAYMENT_OR_COLLECT, R.string.M10_1_6, R.mipmap.icon_sidemenu_collect, R.mipmap.icon_sidemenu_collect_on, MainHomeActivity.class),
    //跨境
    CROSS_BORDER(MenuKey.CROSS_BORDER, R.string.M10_1_14, R.mipmap.icon_sidemenu_crossborder, R.mipmap.icon_sidemenu_crossborder_on, MainHomeActivity.class),
    //著数优惠
    PREFERENCES_LIST(MenuKey.PREFERENCES_LIST, R.string.M10_1_17, R.mipmap.icon_sidemenu_hotoffers_promotion, R.mipmap.icon_sidemenu_hotoffers_promotion_on, MainHomeActivity.class),
    //虚拟卡申请
    VIRTUAL_CARD_APPLY(MenuKey.VIRTUAL_CARD_APPLY, R.string.M10_1_4, R.mipmap.icon_list_virtualcard, R.mipmap.icon_list_virtualcard, MainHomeActivity.class),
    //虚拟卡确认
    VIRTUAL_CARD_CONFIRM(MenuKey.VIRTUAL_CARD_CONFIRM, R.string.M10_1_5, R.mipmap.icon_list_virtualcard, R.mipmap.icon_list_virtualcard, MainHomeActivity.class),
    //主扫
    MAJOR_SCAN_QR_CODE(MenuKey.MAJOR_SCAN_QR_CODE, R.string.M10_1_8, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, MajorScanFragment.class),
    //被扫
    BACK_SCAN_QR_CODE(MenuKey.BACK_SCAN_QR_CODE, R.string.M10_1_7, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, BackScanFragment.class),
    //转账方式选择列表
    TRANSFER_SELECT_LIST(MenuKey.TRANSFER_SELECT_LIST, R.string.M10_1_9, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferSelTypeFragment.class),
    //派利是
    RED_POCKET(MenuKey.RED_POCKET, R.string.M10_1_12, R.mipmap.icon_list_redpocket, R.mipmap.icon_list_redpocket, TransferRedPacketFragment.class),
    //FPS转账
    TRANSFER_BY_FPS(MenuKey.TRANSFER_BY_FPS, R.string.M10_1_10, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferByFpsIDFragment.class),
    //银行转账
    TRANSFER_BY_BANK(MenuKey.TRANSFER_BY_BANK, R.string.M10_1_11, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferByBankAccountFragment.class),
    //静态码收款
    TRANSFER_BY_STATIC_CODE(MenuKey.TRANSFER_BY_STATIC_CODE, R.string.M10_1_13, R.mipmap.icon_sidemenu_collect, R.mipmap.icon_sidemenu_collect_on, TransferByStaticCodeFragment.class),

    //跨境转账
    TRANSFER_CROSS_BORDER(MenuKey.TRANSFER_CROSS_BORDER, R.string.M10_1_15, R.mipmap.icon_list_crossboarder_transfer, R.mipmap.icon_list_crossboarder_transfer, TransferCrossBorderPayeeFragment.class),
    //跨境收款
    PAYMENT_CROSS_BORDER(MenuKey.PAYMENT_CROSS_BORDER, R.string.M10_1_16, R.mipmap.icon_list_crossboarder_payment, R.mipmap.icon_list_crossboarder_payment, CrossBorderPaymentFragment.class),
    //最新消息
    NEW_MESSAGE(MenuKey.NEW_MESSAGE, R.string.M10_1_18, R.mipmap.icon_list_crossboarder_payment, R.mipmap.icon_list_crossboarder_payment, NewMsgFragment.class),
    //信用卡奖赏登记
    REWARD_REGISTER(MenuKey.REWARD_REGISTER, R.string.M10_1_21, R.mipmap.icon_sidemenu_creditcardpromotion, R.mipmap.icon_sidemenu_creditcardpromotion, RewardRegisterSelectListFragment.class),
    //推荐亲友
    INVITE_SHARE(MenuKey.INVITE_SHARE, R.string.M10_1_22, R.mipmap.icon_list_transfer_limit_setting, R.mipmap.icon_list_transfer_limit_setting, InviteShareFragment.class),

    //交易记录
    TRANSACTION_MANAGEMENT(MenuKey.TRANSACTION_MANAGEMENT, R.string.M10_1_23, R.mipmap.icon_sidemenu_transactions, R.mipmap.icon_sidemenu_transactions, TransactionManagementFragment.class);
*/
    public final String action;
    public final int name;
    public int flag;
    public int flagSelect;
    public boolean levelFirst;
    private final Class<?> cls;

    HomeLifeMenuItemEnum(String action, int name, int flag, int flagSelect, Class<?> cls) {
        this(action, name, flag, flagSelect, cls, true);
    }

    HomeLifeMenuItemEnum(String action, int name, int flag, int flagSelect, Class<?> cls, boolean levelFirst) {
        this.action = action;
        this.name = name;
        this.flag = flag;
        this.flagSelect = flagSelect;
        this.cls = cls;
        this.levelFirst = levelFirst;
    }


    public static ArrayList<HomeLifeMenuEntity> toList(Context context) {
        ArrayList<HomeLifeMenuEntity> list = new ArrayList<>();
        for (HomeLifeMenuItemEnum menuItem : HomeLifeMenuItemEnum.values()) {
            list.add(new HomeLifeMenuEntity(menuItem.action,
                    context.getString(menuItem.name),
                    menuItem.flag));
        }
        return list;
    }

    public static HomeLifeMenuEntity getMenuItmByEnum(Context context, HomeLifeMenuItemEnum itemEnum) {
        return new HomeLifeMenuEntity(itemEnum.action,
                context.getString(itemEnum.name),
                itemEnum.flag);
    }


    public static void mapMenuToList(Context context, ArrayList<HomeLifeMenuEntity> menuItemList) {
        for (HomeLifeMenuEntity itemEntity : menuItemList) {
            for (HomeLifeMenuItemEnum menuItemEnum : HomeLifeMenuItemEnum.values()) {
                if (menuItemEnum.action.equals(itemEntity.getMenuKey())) {
                    itemEntity.setName(context.getString(menuItemEnum.name));
                    itemEntity.setResIdByImage(menuItemEnum.flag);
                }
            }
        }
    }


}
