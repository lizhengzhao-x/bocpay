package cn.swiftpass.wallet.intl.module.register;

import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import java.io.Serializable;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.AmountSetDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.module.register.model.RegisterDetailsData;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;


/**
 * 注册设置交易限额
 */
public class SetTransactionLimitActivity extends BaseCompatActivity {


    private cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView mId_edit_daily_amount;
    private SeekBar mId_seekbar;
    private TextView mId_money_min;
    private TextView mId_money_max;
    private LinearLayout mId_conditions;
    private ImageView mIv_confirm_mark;
    private TextView mId_confirm_conditions;
    private TextView mTv_card_info_nextPage;
    private int mCurrentLimit;
    private String mCurrency;
    private String mMaxLimit;
    private String mMinLimit;
    private int mCurrentType;
    private RegisterDetailsData registerDetailsData;
    private RegisterIDEntity registerIDEntity;
    private Serializable ocrInfo;
    private RegisterCustomerInfo registerCustomerInfo;


    private void bindViews() {
        mId_edit_daily_amount = (cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView) findViewById(R.id.id_edit_daily_amount);
        mId_seekbar = (SeekBar) findViewById(R.id.id_seekbar);
        mId_money_min = (TextView) findViewById(R.id.id_money_min);
        mId_money_max = (TextView) findViewById(R.id.id_money_max);
        mId_conditions = (LinearLayout) findViewById(R.id.id_conditions);
        mIv_confirm_mark = (ImageView) findViewById(R.id.iv_confirm_mark);
        mId_confirm_conditions = (TextView) findViewById(R.id.id_confirm_conditions);
        mTv_card_info_nextPage = (TextView) findViewById(R.id.tv_card_info_nextPage);
        mId_edit_daily_amount.getImageRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickAmountLimit();
            }
        });
    }

    private void updateOkBackground(boolean isSel) {
        mTv_card_info_nextPage.setEnabled(isSel);
        mTv_card_info_nextPage.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.INPUT_REGISTRATION_TAX;
        analysisPageEntity.current_address = PagerConstant.DAILY_LIMIT_SETTING;
        return analysisPageEntity;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        if (getIntent() != null) {
            mCurrentType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, 0);
            registerIDEntity = (RegisterIDEntity) getIntent().getSerializableExtra(Constants.MAX_LITMIT_INFO);
            ocrInfo = getIntent().getSerializableExtra(Constants.OCR_INFO);
            registerCustomerInfo = (RegisterCustomerInfo) getIntent().getSerializableExtra(Constants.CUSTOMER_INFO);

            Serializable detailSerData = getIntent().getSerializableExtra(RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA);
            if (detailSerData != null) {
                registerDetailsData = (RegisterDetailsData) detailSerData;
            }
        }
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);

            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        } else {
            setToolBarTitle(R.string.IDV_3_1a);
        }
        bindViews();
        DataCollectManager.getInstance().sendTransactionLimit();
        mCurrency = "HKD";
        mMaxLimit = registerIDEntity.getSmartMaxPay();
        mId_money_min.setText(mCurrency + " 0");
        mId_money_max.setText(mCurrency + " " + AndroidUtils.formatPrice(Double.valueOf(mMaxLimit), false));

        if (registerDetailsData != null && registerDetailsData.currentLimit > 0) {
            mCurrentLimit = registerDetailsData.currentLimit;
        } else {
            mCurrentLimit = Integer.valueOf(registerIDEntity.getSmartMaxDefault());
        }

        mId_edit_daily_amount.setLeftTextTitle(mCurrency + " " + AndroidUtils.formatPrice(Double.valueOf(mCurrentLimit), false));
        mIv_confirm_mark.setImageResource(mIv_confirm_mark.isSelected() ? R.mipmap.icon_check_choose_circle : R.mipmap.icon_check_choose_circle_default);
        mIv_confirm_mark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIv_confirm_mark.setSelected(!mIv_confirm_mark.isSelected());
                mIv_confirm_mark.setImageResource(mIv_confirm_mark.isSelected() ? R.mipmap.icon_check_choose_circle : R.mipmap.icon_check_choose_circle_default);
                updateOkBackground(mIv_confirm_mark.isSelected());
            }
        });
        mId_seekbar.setMax(100);
        if (registerDetailsData != null && registerDetailsData.currentLimit > 0) {
            if (registerDetailsData.currentLimit > Float.valueOf(mMaxLimit)) {
                registerDetailsData.currentLimit = Integer.valueOf(mMaxLimit);
            }
            int progress = (int) ((registerDetailsData.currentLimit / Float.valueOf(mMaxLimit)) * 100);
            mId_seekbar.setProgress(progress);
        } else {
            int progress = (int) ((Integer.valueOf(registerIDEntity.getSmartMaxDefault())) / Float.valueOf(mMaxLimit)) * 100;
            mId_seekbar.setProgress(progress);
        }
        mId_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCurrentLimit = (int) ((progress * 1.0 / 100.0) * Float.valueOf(mMaxLimit));
                mCurrentLimit = mCurrentLimit / 100 * 100;
                mId_edit_daily_amount.setLeftTextTitle(mCurrency + " " + AndroidUtils.formatPrice(Double.valueOf(mCurrentLimit), false));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        mTv_card_info_nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    setTransDailyLimit();
                }
            }
        });
        //注册流程如果DJ601 activity 退出
        ProjectApp.getTempTaskStack().add(this);
    }

    public void initSDKListener() {

        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);

        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_SETDAILYTRANSACTIONLIMIT);
        ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.hide();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ProjectApp.getTempTaskStack().remove(this);
    }




    /**
     * 注册设置校验日限额
     */
    private void setTransDailyLimit() {
        if (mCurrentLimit == 0) {
            showErrorMsgDialog(mContext, getString(R.string.title_string_daily_litmit_error));
            return;
        }
        if (registerCustomerInfo != null) {
            registerCustomerInfo.mCurrentLimit = mCurrentLimit + "";
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentType);
        mHashMaps.put(Constants.CUSTOMER_INFO, registerCustomerInfo);
        if (registerDetailsData != null) {
            registerDetailsData.currentLimit = mCurrentLimit;
            mHashMaps.put(RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA, registerDetailsData);
        }
        mHashMaps.put(Constants.OCR_INFO, ocrInfo);
        ActivitySkipUtil.startAnotherActivity(SetTransactionLimitActivity.this, RegisterDetailsActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    private void onClickAmountLimit() {
        AmountSetDialog.Builder builder = new AmountSetDialog.Builder(this);
        AmountSetDialog dialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                int limitAmount = ((AmountSetDialog) dlg).getmAmount();
                mId_seekbar.setProgress((int) ((limitAmount / Float.valueOf(mMaxLimit)) * 100));
                mId_edit_daily_amount.setLeftTextTitle(mCurrency + " " + AndroidUtils.formatPrice(Double.valueOf(limitAmount), false));
                mCurrentLimit = limitAmount;
            }
        }).create(-1, Integer.valueOf(mMaxLimit));
        dialog.show();
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_set_transaction;
    }
}
