package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class AreaEntity extends BaseEntity {
    public List<AreaItem> getAreaItems() {
        return areaItems;
    }

    public void setAreaItems(List<AreaItem> areaItems) {
        this.areaItems = areaItems;
    }

    private List<AreaItem> areaItems;


    public static class AreaItem extends BaseEntity {
        private List<String> childNames;
        private String pName;

        public AreaItem(String pName) {
            this.pName = pName;
        }

        public String getpName() {
            return pName;
        }

        public void setpName(String pName) {
            this.pName = pName;
        }

        public List<String> getChildNames() {
            return childNames;
        }

        public void setChildNames(List<String> childNames) {
            this.childNames = childNames;
        }

    }
}
