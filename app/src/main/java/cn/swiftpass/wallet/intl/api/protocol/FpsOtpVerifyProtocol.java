package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 验证FIO的OTP
 */

public class FpsOtpVerifyProtocol extends BaseProtocol {
    //客户端调用Authtication 接口验证用户指纹，之后SDK返回的base64字符串
    String mOtp;
    String mAction;

    public FpsOtpVerifyProtocol(String otp, String action, NetWorkCallbackListener dataCallback) {
        this.mOtp = otp;
        mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/verifyOtpFPS";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(FpsConst.FPS_OTP, mOtp);
        mBodyParams.put(FpsConst.FPS_ACTION, mAction);
    }
}
