package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByBankAccounContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferByBankAccountPresenter;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static android.app.Activity.RESULT_OK;

/**
 * bank account 转账
 */
public class TransferByBankAccountFragment extends BaseFragment<TransferByBankAccounContract.Presenter> implements TransferByBankAccounContract.View {


    @BindView(R.id.id_transfer_payee)
    TextView idTransferPayeeTitle;
    @BindView(R.id.et_name)
    EditTextWithDel et_transfer_payee_content;
    @BindView(R.id.et_fps_bank_name)
    TextView mEt_fps_bank_name;
    @BindView(R.id.et_fps_card_number)
    EditTextWithDel mEt_fps_card_number;
    @BindView(R.id.et_fps_amount)
    EditTextWithDel mEt_fps_amount;
    @BindView(R.id.id_available_amount)
    TextView mId_available_amount;
    @BindView(R.id.transfer_email_next)
    TextView mTransfer_email_next;
    @BindView(R.id.id_remark_size)
    TextView mId_remark_size;
    @BindView(R.id.id_transfer_bank_layout)
    RelativeLayout mId_transfer_bank_layout;
    @BindView(R.id.id_transfer_remark)
    EditTextWithDel mId_transfer_remark;
    @BindView(R.id.rl_softkeyboard_view)
    SoftKeyboardTranslationView mSoftKeyboardTranslationView;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.iv_bg_wave)
    ImageView ivBgWave;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.tv_transfer_info_title)
    TextView tvTransferInfoTitle;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;

    String mCurrentBankName, mCurrentBankCode;

    AccountBalanceEntity accountBalanceEntity;

    MySmartAccountEntity mSmartAccountInfo;
    //需不要隐藏眼睛
    private boolean isHideEye = true;
    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;


    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String payeeName = et_transfer_payee_content.getText().toString();
            if (!et_transfer_payee_content.isShown()) {
                //当前选择的是中国银行 不需要输入收款人名称
                payeeName = "";
            }
            String cardNumber = mEt_fps_card_number.getText();
            String transferAmount = mEt_fps_amount.getText();
            final TransferPreCheckReq req = new TransferPreCheckReq();
            req.transferType = TransferConst.TRANS_CUSTOMER_ACCOUNT;
            req.bankType = TransferConst.TRANS_BANK_TYPE_FPS;
            if (!TextUtils.isEmpty(mCurrentBankCode)) {
                req.bankName = mCurrentBankCode;
            }
            req.tansferToAccount = cardNumber;
            req.postscript = mId_transfer_remark.getText().toString().trim();
            req.transferAmount = transferAmount;
            req.transferToName = payeeName;
            req.isQrcode = false;
            mPresenter.transferPreCheck(req);

        }
    };

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    /**
     * 选择银行
     */
    public void onClickSelBank() {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.DATA_TYPE, Constants.DATA_SEL_BANK_LIST);
        mHashMapsLogin.put(Constants.HAS_EXTRA_SEL, false);
        ActivitySkipUtil.startAnotherActivityForResult(this, SelectCountryCodeActivity.class, mHashMapsLogin, Constants.REQUEST_CODE_SEL_BANK);
    }


    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.TF2101_30_1);
            //因为MainActivity 使用了全屏的标记位， 会导致adjustResize失效
            mSoftKeyboardTranslationView.initPopView(mActivity, llContent);
        }

    }

    @Override
    protected TransferByBankAccounContract.Presenter loadPresenter() {
        return new TransferByBankAccountPresenter();
    }

    @Override
    protected void initView(View v) {
        AndroidUtils.setLimit(mId_transfer_remark.getEditText(), mId_remark_size);
        mEt_fps_card_number.hideErrorView();
        mEt_fps_card_number.hideDelView();
        mEt_fps_amount.hideErrorView();
        mEt_fps_amount.hideDelView();
        et_transfer_payee_content.hideErrorView();
        et_transfer_payee_content.hideDelView();
        //两位小数过滤
        mEt_fps_amount.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(9)});
        mEt_fps_amount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);


        mId_transfer_bank_layout.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onClickSelBank();
            }
        });

        et_transfer_payee_content.getEditText().setFilters(new InputFilter[]{AndroidUtils.getEmjoFilter()});
        mTransfer_email_next.setOnClickListener(onClickListener);
        updateNextStatus();

        initTextWatcher();

        mPresenter.getSmartAccountInfo();

        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });

        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));

        //防止软键盘换车换行
        mId_transfer_remark.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    private void initTextWatcher() {
        mEt_fps_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dealWithExceedMaxMoney(mEt_fps_amount.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateNextStatus();
            }
        });
        mEt_fps_card_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateNextStatus();
            }
        });
        et_transfer_payee_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateNextStatus();
            }
        });

        EventBus.getDefault().register(this);
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }


    @OnClick({R.id.tv_balance_title,
            R.id.tv_amount,
            R.id.tv_balance_tody_title,
            R.id.tv_amount_usable,
            R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance, R.id.tv_transfer_top_up})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 300)) return;
        switch (view.getId()) {
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    try {

                        SmartAccountAdjustDailyLimitActivity.startActivity(getActivity(), mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                    } catch (Exception e) {

                    }
                }
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_ACCOUNTNO_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.tv_transfer_top_up:
                TransferUtils.checkTopUpEvent(mSmartAccountInfo, new TransferUtils.RegEddaEventListener() {
                    @Override
                    public void getRegEddaInfo() {
                        if (mPresenter != null) {
                            mPresenter.getRegEddaInfo();
                        }
                    }

                    @Override
                    public Activity getContext() {
                        return getActivity();
                    }

                    @Override
                    public void showErrorDialog(String errorCode, String message) {
                        showErrorMsgDialog(getContext(), message);
                    }
                });
                break;
        }
    }


    private void getRegEddaInfo() {
        mPresenter.getRegEddaInfo();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {

            mPresenter.getSmartAccountInfo();
        }
    }

    /**
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }
        }

    }

    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    private void updateNextStatus() {
        if (et_transfer_payee_content.isShown()) {
            if (!TextUtils.isEmpty(et_transfer_payee_content.getText().toString()) && !TextUtils.isEmpty(mEt_fps_amount.getText()) && !TextUtils.isEmpty(mEt_fps_card_number.getText()) && !TextUtils.isEmpty(mCurrentBankCode)) {
                dealWithExceedMaxMoney(mEt_fps_amount.getEditText().getText().toString());
                return;
            }
        } else {
            if (!TextUtils.isEmpty(mEt_fps_amount.getText()) && !TextUtils.isEmpty(mEt_fps_card_number.getText()) && !TextUtils.isEmpty(mCurrentBankCode)) {
                dealWithExceedMaxMoney(mEt_fps_amount.getEditText().getText().toString());
                return;
            }
        }
        mTransfer_email_next.setEnabled(false);
    }


    /**
     * 根据输入金额参数控制next状态
     *
     * @param s
     */
    private void dealWithExceedMaxMoney(String s) {
        checkEditMoney(false);
        if (TextUtils.isEmpty(s)) {
            if (accountBalanceEntity != null) {
                mId_available_amount.setText("");
                updateOkBackground(mTransfer_email_next, false);
            }
            return;
        }
        double currentV = 0;
        try {
            currentV = Double.valueOf(s);
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (currentV == 0) {
            updateOkBackground(mTransfer_email_next, false);
            return;
        }
        double maxValue = 0;
        if (accountBalanceEntity != null && !TextUtils.isEmpty(accountBalanceEntity.getBalance())) {
            try {
                maxValue = Double.valueOf(accountBalanceEntity.getBalance());
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            if (currentV <= maxValue) {
                mId_available_amount.setText("");
                mId_available_amount.setTextColor(getResources().getColor(R.color.font_gray_four));
            } else {
                mId_available_amount.setText(getResources().getString(R.string.TF2101_4_11));
                mId_available_amount.setTextColor(ContextCompat.getColor(mContext, R.color.color_FFBF2F4F));

                checkEditMoney(true);

            }
            updateOkBackground(mTransfer_email_next, currentV <= maxValue);
        } else {
            updateOkBackground(mTransfer_email_next, false);
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transfer_by_bankaccount;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != cbToggleSee) {
            if (isHideEye) {
                cbToggleSee.setChecked(false);
            } else {
                isHideEye = true;
            }
        }
    }


    /**
     * 选择区域 银行
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Constants.REQUEST_CODE_SEL_BANK) {
                mCurrentBankCode = data.getExtras().getString(Constants.BANK_CODE);
                mCurrentBankName = data.getExtras().getString(Constants.BANK_NAME);
                mEt_fps_bank_name.setText(mCurrentBankName);
                mEt_fps_bank_name.setTextColor(Color.parseColor("#24272B"));
                updateNextStatus();
                boolean isBochkBank = mCurrentBankCode.equals("012");
                //012 需求是 如果是选择了中国银行 不需要再输入收款人
                idTransferPayeeTitle.setVisibility(isBochkBank ? View.GONE : View.VISIBLE);
                et_transfer_payee_content.setVisibility(isBochkBank ? View.GONE : View.VISIBLE);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        mSmartAccountInfo = response;

        CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);

        //这里也重新设置AccountBalanceEntity
        accountBalanceEntity = new AccountBalanceEntity();
        accountBalanceEntity.setBalance(mSmartAccountInfo.getOutavailbal());
        accountBalanceEntity.setCurrency(mSmartAccountInfo.getCurrency());

        getRetainBalanceInfo();


        if (null != cbToggleSee && cbToggleSee.isChecked()) {
            openAccount();
        } else {
            hideAccount();
        }

        if (isEyeDateNull) {
            cbToggleSee.setChecked(true);
            isEyeDateNull = false;
        }

        mSoftKeyboardTranslationView.setVisibility(View.VISIBLE);
        ivBgWave.setVisibility(View.VISIBLE);
        llContent.setVisibility(View.VISIBLE);

        dealWithExceedMaxMoney(mEt_fps_amount.getEditText().getText().toString());
    }

    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }
        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();

        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);

            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }

    void getRetainBalanceInfo() {
        if (null == accountBalanceEntity) {
            return;
        }
        String str = getString(R.string.TF2101_4_8);
        str = str.replace("(HKD)", "");
        tvTransferInfoTitle.setText(str + "(" + accountBalanceEntity.getCurrency() + ")");
    }

    /**
     * 判断金额输入框 数字是否大于今日可用交易限额
     *
     * @param isRed
     */
    void checkEditMoney(boolean isRed) {
        if (null == mEt_fps_amount) {
            return;
        }

        if (isRed) {
            mEt_fps_amount.setLineRedVisible(true);
            mEt_fps_amount.setLineVisible(false);
        } else {
            mEt_fps_amount.setLineRedVisible(false);
            mEt_fps_amount.setLineVisible(true);
        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);

        ivBgWave.setVisibility(View.VISIBLE);
        llContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void transferPreCheckSuccess(TransferPreCheckReq req, TransferPreCheckEntity response) {
        response.setPreCheckReq(req);
        response.setTransferType(TransferConst.TRANS_CUSTOMER_ACCOUNT);
        response.setBankShowName(mEt_fps_bank_name.getText().toString());
        TransferConfirmMoneyActivity.startActivity(mActivity, response);
    }

    @Override
    public void transferPreCheckError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
    }
}
