package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class TaxResidentEntity extends BaseEntity {
    private String residenceJurisdiction;
    private String taxResidentRegionName;
    private String taxNumberRegular;
    //判断在国家列表中是否选中，只做缓存使用，在UI上有显示，不影响数据
    private boolean selectCacheStatus;
    //判断是否在税务界面已经添加了这个国家，影响逻辑
    private boolean selectStatus;

    public boolean isSelectCacheStatus() {
        return selectCacheStatus;
    }

    public void setSelectCacheStatus(boolean selectCacheStatus) {
        this.selectCacheStatus = selectCacheStatus;
    }

    public String getTaxNumberRegular() {
        return taxNumberRegular;
    }

    public void setTaxNumberRegular(String taxNumberRegular) {
        this.taxNumberRegular = taxNumberRegular;
    }

    public boolean isSelectStatus() {
        return selectStatus;
    }

    public void setSelectStatus(boolean selectStatus) {
        this.selectStatus = selectStatus;
    }

    public String getResidenceJurisdiction() {
        return residenceJurisdiction;
    }

    public void setResidenceJurisdiction(String residenceJurisdiction) {
        this.residenceJurisdiction = residenceJurisdiction;
    }

    public String getTaxResidentRegionName() {
        return taxResidentRegionName;
    }

    public void setTaxResidentRegionName(String taxResidentRegionName) {
        this.taxResidentRegionName = taxResidentRegionName;
    }
}
