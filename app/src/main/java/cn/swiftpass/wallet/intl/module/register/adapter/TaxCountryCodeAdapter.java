package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TaxResidentEntity;

public class TaxCountryCodeAdapter extends RecyclerView.Adapter {
    private final ArrayList<TaxResidentEntity> data;
    private final Context context;
    private final OnClickTaxResidenceJurisdictionListener listener;
    private TaxResidentEntity currentCountryData;
    private int selectIndex = -1;

    public TaxCountryCodeAdapter(Context context, ArrayList<TaxResidentEntity> taxCountryList, OnClickTaxResidenceJurisdictionListener listener) {
        this.context = context;
        this.data = taxCountryList;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tax_country, parent, false);
        return new TaxCountryHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        TaxCountryHolder countryHolder = (TaxCountryHolder) holder;
        TaxResidentEntity residenceEntity = data.get(position);
        if (residenceEntity != null) {
            countryHolder.tvCountryName.setText(residenceEntity.getTaxResidentRegionName());
            if (residenceEntity.isSelectCacheStatus()) {
                selectIndex = position;
                countryHolder.ivSelectCountry.setVisibility(View.VISIBLE);
            } else {
                countryHolder.ivSelectCountry.setVisibility(View.INVISIBLE);
            }
        }
        countryHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    if (selectIndex > -1) {
                        TaxResidentEntity lastEntity = data.get(selectIndex);
                        lastEntity.setSelectCacheStatus(false);
                    }
                    if (residenceEntity != null) {
                        selectIndex = position;
                        residenceEntity.setSelectCacheStatus(true);
                        listener.OnClickTaxResidenceJurisdiction(residenceEntity);
                    }
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    public void setCurrentCountry(TaxResidentEntity currentCountryData) {
        this.currentCountryData = currentCountryData;
        if (data != null && currentCountryData != null) {
            for (TaxResidentEntity taxResidentEntity : data) {
                if (taxResidentEntity.getResidenceJurisdiction().equals(currentCountryData.getResidenceJurisdiction())) {
                    taxResidentEntity.setSelectCacheStatus(true);
                }
            }
        }
    }

}
