package cn.swiftpass.wallet.intl.module.home.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetAppCallAppLinkProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetBackgroundProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetMenuListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SystemPageInitDataProtocol;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.BackgroundUrlEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.module.home.contract.PreLoginContract;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;
import cn.swiftpass.wallet.intl.utils.GlideImageDownloadManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class PreLoginPresenter extends BasePresenter<PreLoginContract.View> implements PreLoginContract.Presenter {

    private boolean requestGetBackgroundUrl;

    @Override
    public void getAppCallAppLink(String token, String url, String checkSum, String digest) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetAppCallAppLinkProtocol(token, checkSum, digest, url, new NetWorkCallbackListener<AppCallAppLinkEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().appCallAppLinkError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AppCallAppLinkEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().appCallAppLinkSuccess(response, url);
                }
            }
        }).execute();
    }

    @Override
    public void getMenuList() {
        new GetMenuListProtocol(new NetWorkCallbackListener<MenuListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getMenuListFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MenuListEntity response) {
                if (getView() != null) {
                    getView().getMenuListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getInitData(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SystemPageInitDataProtocol(new NetWorkCallbackListener<SystemPagerDataEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInitDataFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SystemPagerDataEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInitDataSuccess(response, action);
                }
            }
        }).execute();
    }

    @Override
    public void getBackgroundUrl(GlideImageDownLoadListener listener) {
        if (requestGetBackgroundUrl){
            return;
        }
        requestGetBackgroundUrl=true;
        new GetBackgroundProtocol(new NetWorkCallbackListener<BackgroundUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                requestGetBackgroundUrl=false;
            }

            @Override
            public void onSuccess(BackgroundUrlEntity response) {
                requestGetBackgroundUrl=false;
                if (response != null) {
                    if (!SpUtils.getInstance().getMainBgVersion().equals(response.getPostLoginBgImageVer())) {
                        GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_MAIN_BG, response.getPostLoginBgImageUrl(), response.getPostLoginBgImageVer());
                    }

                    if (!SpUtils.getInstance().getPreLoginBgVersion().equals(response.getPreLoginBgImageVer())) {
                        GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_PRE_LOGIN_BG, response.getPreLoginBgImageUrl(), response.getPreLoginBgImageVer(),listener);
                    }
                }

            }
        }).execute();
    }
}
