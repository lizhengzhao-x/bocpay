package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteListEntity;
import cn.swiftpass.wallet.intl.entity.InviteShareEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.InviteContactContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.InviteContactPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

public class InviteContactActivity extends BaseCompatActivity<InviteContactContract.Presenter> implements InviteContactContract.View {

    @BindView(R.id.iv_search)
    ImageView ivSearch;
    @BindView(R.id.et_search)
    ClearEditText etSearch;
    @BindView(R.id.con_search)
    ConstraintLayout conSearch;
    @BindView(R.id.ry_invite)
    RecyclerView ryInvite;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;

    private InviteContactAdapter inviteContactAdapter;

    public List<InviteContactEntity> allContactList = new ArrayList<>();

    private List<InviteContactEntity> contactList = new ArrayList<>();

    /**
     * 记录请求失败的页面情况
     */
    private static int savePage = 1;

    /**
     * 当前页面  默认为第一页
     */
    private int currentPage = 1;
    /**
     * 每一页数据  默认为20条
     */
    private int mgmMobilePageSize = 20;

    private int dataSize = 0;

    /**
     * 可邀请好友 总数据 来判断要不要显示查看更多
     */
    private int totalNum = 0;

    private boolean hasData = true;

    private InviteShareEntity mInviteShareEntity;
    /**
     * 分享的内容
     */
    private String shareContent;
    private View emptyView;

    private boolean isLastItem;

    @Override
    public void init() {
        setToolBarTitle(R.string.MGM4_2_1);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mInviteShareEntity = (InviteShareEntity) bundle.getSerializable(InviteShareFragment.SHARE_INFO);
            if (mInviteShareEntity != null) {
                shareContent = mInviteShareEntity.getShareInfo();
                mgmMobilePageSize = mInviteShareEntity.getMgmMobilePageSize();
            }
        }
        initRecycleView();

        etSearch.setCursorVisible(false);
        etSearch.setFocusable(false);
        etSearch.setFocusableInTouchMode(false);
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInviteShareEntity != null) {
                    HashMap<String, Object> shareMap = new HashMap<>();
                    shareMap.put(InviteShareFragment.SHARE_INFO, mInviteShareEntity);
                    shareMap.put(InviteShareFragment.HAS_DATA, hasData);
                    InviteSearchActivity.setAllContactList(allContactList);
                    ActivitySkipUtil.startAnotherActivity(InviteContactActivity.this, InviteSearchActivity.class, shareMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });

        mPresenter.initPageData(mContext, currentPage, mgmMobilePageSize, allContactList);
    }

    private void initRecycleView() {
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                isLastItem = false;
                currentPage = 1;
                mPresenter.initPageData(mContext, currentPage, mgmMobilePageSize, allContactList);
            }
        });


        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 0.5f);
        ryInvite.addItemDecoration(MyItemDecoration.createVertical(getColor(R.color.line_common), ryLineSpace));
        ryInvite.setLayoutManager(mLayoutManager);
        inviteContactAdapter = new InviteContactAdapter(contactList);
        inviteContactAdapter.bindToRecyclerView(ryInvite);

        emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) ryInvite.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        emptyView.setVisibility(View.GONE);
        noDataText.setText(getString(R.string.TR1_2b_1));
        noDataImg.setImageResource(R.mipmap.icon_transaction_noresult);
        inviteContactAdapter.setEmptyView(emptyView);

        inviteContactAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                currentPage += 1;
                mPresenter.initPageData(mContext, currentPage, mgmMobilePageSize, allContactList);
            }
        }, ryInvite);

        inviteContactAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (baseRecyclerAdapter.getDataList().size() == 0) {
                    return;
                }
                final InviteContactEntity inviteContactEntity = (InviteContactEntity) baseRecyclerAdapter.getDataList().get(position);
                switch (view.getId()) {
                    case R.id.iv_sms:
                        if (inviteContactEntity != null) {
                            SdkShareUtil.sendSMS(mContext, shareContent, inviteContactEntity.getAccount());
                        }
                        break;
                    case R.id.iv_whatsapp:
                        //判断是否安装了whatsApp
                        boolean isSuccess = SdkShareUtil.isAppInstalled(mContext, SdkShareUtil.WHATS_APP);
                        if (!isSuccess) {
                            showErrorMsgDialog(mContext, getResources().getString(R.string.SH2_3_1));
                        } else {
                            //需求变动 拿掉弹框
                            if (inviteContactEntity != null) {
                                String phoneNumber = inviteContactEntity.getAccount();
                                if (phoneNumber != null && phoneNumber.length() == 11) {
                                    if (phoneNumber.startsWith("853") || phoneNumber.startsWith("852")) {

                                    } else {
                                        phoneNumber = "86" + phoneNumber;
                                    }
                                } else if (phoneNumber != null && phoneNumber.length() == 8) {
                                    phoneNumber = "852" + phoneNumber;
                                }
                                SdkShareUtil.openWhatsApp(mContext, shareContent, phoneNumber);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        });

        ryInvite.addOnScrollListener(new RecyclerView.OnScrollListener() {
            //用来标记是否正在向下滑动
            boolean isSlidingToLast = false;

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                // 当不滚动时
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                    //获取最后一个完全显示的ItemPosition
                    if (manager != null) {
                        int lastVisibleItem = manager.findLastCompletelyVisibleItemPosition();
                        int totalItemCount = manager.getItemCount();

                        // 判断是否滚动到底部，并且是向下滚动
                        if (lastVisibleItem == (totalItemCount - 1) && isSlidingToLast) {
                            isLastItem = true;
                        }
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //dx用来判断横向滑动方向，dy用来判断纵向滑动方向
                if (dy > 0) {
                    //大于0表示正在向下滚动
                    isSlidingToLast = true;
                } else {
                    //小于等于0表示停止或向上滚动
                    isSlidingToLast = false;
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_invite_contact;
    }

    @Override
    protected InviteContactPresenter createPresenter() {
        return new InviteContactPresenter();
    }

    @Override
    public void getInviteContactFail(String errorCode, String errorMsg) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        }
        if (currentPage != 1) {
            inviteContactAdapter.loadMoreFail();
        } else {
            if (inviteContactAdapter.getDataList().size() == 0) {
                hasData = false;
                emptyView.setVisibility(View.VISIBLE);
            }
        }
        showErrorMsgDialog(this, errorMsg, R.string.dialog_right_btn_ok, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
            }
        });

        currentPage = savePage;
    }

    @Override
    public void getInviteContactSuccess(InviteListEntity inviteListEntity) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        }
        if (inviteListEntity != null) {
            contactList = inviteListEntity.result;
            if (contactList.size() > 0) {
                if (currentPage == 1) {
                    hasData = true;
                    inviteContactAdapter.setDataList(contactList);
                } else {
                    inviteContactAdapter.addData(contactList);
                }
                if (!isLastItem && inviteContactAdapter.getDataList().size() < 10) {
                    inviteContactAdapter.loadMoreClick();
                } else if (dataSize == mgmMobilePageSize) {
                    inviteContactAdapter.loadMoreComplete();
                } else {
                    inviteContactAdapter.loadMoreEnd(currentPage == 1);
                }
                savePage = currentPage;

                /**
                 * 如果list数据比总数据少 显示查看更多1
                 */
                if (inviteContactAdapter.getDataList().size() < totalNum) {
                    inviteContactAdapter.loadMoreClick();
                } else {
                    inviteContactAdapter.loadMoreEnd(true);
                }
            } else {
                if (currentPage == 1) {
                    hasData = false;
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    inviteContactAdapter.loadMoreEnd(false);
                }
            }
        }
    }


    @Override
    public void getDataSuccess(List<InviteContactEntity> dataList, int total) {
        totalNum = total;
        dataSize = dataList.size();
        mPresenter.getInviteContact(dataList);
    }

}
