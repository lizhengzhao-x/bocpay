package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.text.TextUtils;

import com.bumptech.glide.request.FutureTarget;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.utils.transformation.HomeBgTransformation;
import cn.swiftpass.wallet.intl.utils.transformation.PreLoginBgTransformation;


public class GlideImageDownloadManager {
    private static final Object INSTANCE_LOCK = new Object();
    private static GlideImageDownloadManager sInstance;
    private static Context applicationContext;
    public static final int ACTION_DOWN_PRE_LOGIN_BG = 101;

    public static final int ACTION_DOWN_MAIN_BG = 103;
    private ExecutorService preLoginExecutorService;
    private ExecutorService homeExecutorService;

    private GlideImageDownloadManager() {

    }

    public static void init(Context context) {
        applicationContext = context;
    }

    public static GlideImageDownloadManager getInstance() {
        if (sInstance == null) {
            synchronized (INSTANCE_LOCK) {
                if (sInstance == null) {
                    sInstance = new GlideImageDownloadManager();
                }
            }
        }
        return sInstance;
    }


    /**
     * 下载Prelogin界面背景图
     *
     * @param url
     * @param version
     */
    public void downloadBackgroundInThread(int action, String url, String version, GlideImageDownLoadListener listener) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        switch (action) {
            case ACTION_DOWN_PRE_LOGIN_BG:
                if (preLoginExecutorService == null) {
                    preLoginExecutorService = Executors.newSingleThreadExecutor();
                }
                preLoginExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        downloadPreLoginBgImage(url, version, listener);
                    }
                });

                break;

            case ACTION_DOWN_MAIN_BG:
                if (homeExecutorService == null) {
                    homeExecutorService = Executors.newSingleThreadExecutor();
                }
                homeExecutorService.execute(new Runnable() {
                    @Override
                    public void run() {
                        downloadMainBgImage(url, version, listener);
                    }
                });
                break;
            default:
                break;
        }


    }

    /**
     * 下载Prelogin界面背景图
     *
     * @param url
     * @param version
     */
    public void downloadBackgroundInThread(int action, String url, String version) {
        downloadBackgroundInThread(action, url, version, null);

    }

    /**
     * 下载Prelogin界面背景图
     *
     * @param url
     * @param version
     * @param listener
     */
    private synchronized void downloadPreLoginBgImage(String url, String version, GlideImageDownLoadListener listener) {
        downloadPreImage(url, version, listener);
    }



    /**
     * 下载首页背景图片，同时设置为基础背景图片
     *
     * @param url
     * @param version
     * @param listener
     */
    private synchronized void downloadMainBgImage(String url, String version, GlideImageDownLoadListener listener) {
        downloadMainImage(url, version, listener);
    }


    public void downloadMainImage(String url, String version, GlideImageDownLoadListener listener) {
        Uri uri;
        try {

            if (version.equals(SpUtils.getInstance().getMainBgVersion()) ) {
                if (listener != null) {
                    listener.onSuccessImageDownLoad();
                }
                return;
            }
            //通过Glide 下载 得到图片的 bitmap
            FutureTarget<Bitmap> submit = GlideApp.with(applicationContext)
                    .asBitmap()
                    .skipMemoryCache(true)
                    .transform(new HomeBgTransformation(url + version))
                    .load(url)
                    .submit();
            Bitmap bitmap = submit.get();


            if (bitmap != null) {
                //保存二级背景图
                saveBaseBackGround(bitmap,version);
                //保存bitmap 得到uri
                String fileName = AndroidUtils.getImageSaveFileName(SpUtils.getInstance().getMainBgName(), "mainBg");
                uri = AndroidUtils.saveImageToAppInternalSpace(applicationContext, bitmap, fileName, false);
                if (uri != null) {
                    LogUtils.i("GlideImageDownloadManager", "downloadMainImage SUCCESS");
                    SpUtils.getInstance().setMainBackground(uri.toString());
                    SpUtils.getInstance().setMainBgVersion(version);
                    SpUtils.getInstance().setMainBgName(fileName);
                    if (listener != null) {
                        listener.onSuccessImageDownLoad();
                    }

                } else {
                    if (listener != null) {
                        listener.onFailImageDownLoad();
                    }
                }
                if (bitmap != null) {
                    bitmap.recycle();
                }
                return;
            } else {
                if (listener != null) {
                    listener.onFailImageDownLoad();
                }
            }

        } catch (Exception e) {
            if (listener != null) {
                listener.onFailImageDownLoad();
            }
        }


    }

    private void saveBaseBackGround(Bitmap inBitmap, String version) {
        int width = AndroidUtils.getScreenWidth(ProjectApp.getContext());
        int height = AndroidUtils.getScreenHeight(ProjectApp.getContext());
        Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Matrix m = new Matrix();
        m.setScale(1, 1);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        float present = 0.3f;
        int top = (int) (AndroidUtils.getScreenHeight(ProjectApp.getContext()) * present);
        int bottom = (int) (AndroidUtils.getScreenHeight(ProjectApp.getContext()));
        RectF rect = new RectF(0, top, AndroidUtils.getScreenWidth(ProjectApp.getContext()), bottom);

            Canvas canvas = new Canvas(result);
            //互换位置就可以实现部分遮罩
            canvas.drawBitmap(inBitmap, m, paint);
            canvas.drawRect(rect, paint);
            String fileName = AndroidUtils.getImageSaveFileName(SpUtils.getInstance().getBaseBgName(), "baseBg");

            Uri uriBase = AndroidUtils.saveImageToAppInternalSpace(applicationContext, result, fileName, false);
            if (uriBase != null) {
                LogUtils.i("GlideImageDownloadManager", "downloadBaseImage SUCCESS==" + fileName);
                SpUtils.getInstance().setBaseBackground(uriBase.toString());
                SpUtils.getInstance().setBaseBgName(fileName);
            }
            if (result != null) {
                result.recycle();
            }
            canvas.setBitmap(null);


    }


    public void downloadPreImage(String url, String version, GlideImageDownLoadListener listener) {
        Uri uri;
        Bitmap bitmap=null;
        try {
            if (version.equals(SpUtils.getInstance().getPreLoginBgVersion())) {
                if (listener != null) {
                    listener.onSuccessImageDownLoad();
                }
                return;
            }

            //通过Glide 下载 得到图片的 bitmap
            FutureTarget<Bitmap> submit = GlideApp.with(applicationContext)
                    .asBitmap()
                    .skipMemoryCache(true)
                    .transform(new PreLoginBgTransformation(url + version))
                    .load(url)
                    .submit();
             bitmap = submit.get();
            if (bitmap != null) {
                //保存bitmap 得到uri
                String fileName = AndroidUtils.getImageSaveFileName(SpUtils.getInstance().getPreLoginBgName(), "preLoginBg");

                uri = AndroidUtils.saveImageToAppInternalSpace(applicationContext, bitmap, fileName, false);
                if (uri != null) {
                    LogUtils.i("GlideImageDownloadManager", "downloadPreLoginBgImage SUCCESS");
                    SpUtils.getInstance().setPreLoginBackground(uri.toString());
                    SpUtils.getInstance().setPreLoginBgVersion(version);
                    SpUtils.getInstance().setPreLoginBgName(fileName);
                    if (listener != null) {
                        listener.onSuccessImageDownLoad();
                    }
                } else {
                    if (listener != null) {
                        listener.onFailImageDownLoad();
                    }
                }

            } else {
                if (listener != null) {
                    listener.onFailImageDownLoad();
                }
            }

        } catch (Exception e) {
            if (listener != null) {
                listener.onFailImageDownLoad();
            }
        }  finally {
            if (bitmap != null) {
                bitmap.recycle();
            }
        }

    }
}
