package cn.swiftpass.wallet.intl.module.cardmanagement.fps;


import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 删除FPS的某个银行绑定记录中银
 */

public class FpsRecordDeleteActivity extends BaseCompatActivity {

    //邮箱或者手机
    @BindView(R.id.tv_fps_account)
    TextView mAccountTV;
    @BindView(R.id.tv_account_type)
    TextView mAccountTypeTV;
    @BindView(R.id.tv_account_no)
    TextView mAccountTypeNo;

    @BindView(R.id.tv_default_flag)
    TextView mDefaultTV;
    @BindView(R.id.tv_fps_delete)
    TextView mDeleteTV;


    public FpsBankRecordBean mRecordBean;
    String mFppRefNo;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C3_04_3_1);
        mDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete();
            }
        });
        if (null != getIntent()) {
            mRecordBean = (FpsBankRecordBean) getIntent().getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
            mFppRefNo = getIntent().getStringExtra(FpsConst.FPS_REF_NO);
        }
        if (null != mRecordBean) {
            initView();
        } else {
            finish();
        }
    }

    private void initView() {
        if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mRecordBean.getAccountIdType())) {
            mAccountTV.setText(AndroidUtils.fpsEmailFormat(mRecordBean.getAccountId()));
        } else {
            mAccountTV.setText(AndroidUtils.fpsPhoneFormat(mRecordBean.getAccountId()));
        }
        mAccountTypeNo.setText(mRecordBean.getAccountTypeName() + AndroidUtils.getSubNumberBrackets(mRecordBean.getAccountNo()));
        mDefaultTV.setText(AndroidUtils.getDefaultBankFlag(mRecordBean.getDefaultBank()));
    }

    private void onDelete() {
        ApiProtocolImplManager.getInstance().fpsConfirmDel(getActivity(), mRecordBean, mFppRefNo, mRecordBean.getBankCode(), FpsConst.FPS_RECORD_DEL_TYPE_OTHER, new NetWorkCallbackListener<FpsConfirmEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(getActivity(), errorMsg);
            }

            @Override
            public void onSuccess(FpsConfirmEntity response) {
                String tip = getActivity().getString(R.string.C3_02_2_1);
                showErrorMsgDialog(getActivity(), tip, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        finish();
                    }
                });
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_record_delete;
    }


    public static void startActivity(Activity activity, FpsBankRecordBean fpsBankRecordBean, String fppRefNo) {
        Intent intent = new Intent(activity, FpsRecordDeleteActivity.class);
        intent.putExtra(FpsConst.FPS_REF_NO, fppRefNo);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, fpsBankRecordBean);
        activity.startActivity(intent);
    }
}
