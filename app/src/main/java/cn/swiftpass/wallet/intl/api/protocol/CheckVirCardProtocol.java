package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/12/2.
 * 验证虚拟卡
 */
public class CheckVirCardProtocol extends BaseProtocol {
    String pan;
    String expiryDate;
    String action;

    public CheckVirCardProtocol(String pan, String expiryDate, String action, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.pan = pan;
        this.expiryDate = expiryDate;
        this.action = action;
        mUrl = "api/virtual/checkVirCard";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAN, pan);
        mBodyParams.put(RequestParams.EXPRIYDATE, expiryDate);
        mBodyParams.put(RequestParams.ACTION, action);
    }


}
