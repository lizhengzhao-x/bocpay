package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetIdCardImageProtocol extends BaseProtocol {

    String seqNumber;
    String fileId;

    //    /api/vss/downloadFile    参数seqNumber，fileId，返回files
    public GetIdCardImageProtocol(String seqNumber, String fileId, NetWorkCallbackListener dataCallback) {
        this.seqNumber = seqNumber;
        this.fileId = fileId;
        this.mDataCallback = dataCallback;
        mUrl = "api/vss/downloadFile";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.SEQNUMBER, seqNumber);
        mBodyParams.put(RequestParams.FILEID, fileId);
    }

}
