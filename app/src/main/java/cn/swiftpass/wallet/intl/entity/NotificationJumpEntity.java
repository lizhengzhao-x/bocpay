package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author lizheng.zhao
 * @date 2021/06/02
 * @description push通知跳转链接获取接口返回参数实体 - 最新消息
 */
public class NotificationJumpEntity extends BaseEntity {

    private String target;
    private String type;
    private String shareTarget;

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTarget() {
        return target;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getShareTarget() {
        return shareTarget;
    }

    public void setShareTarget(String shareTarget) {
        this.shareTarget = shareTarget;
    }
}
