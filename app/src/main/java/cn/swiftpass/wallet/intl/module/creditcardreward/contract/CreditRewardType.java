package cn.swiftpass.wallet.intl.module.creditcardreward.contract;

public interface CreditRewardType {

    //日期类型
    String REWARD_DATE_TYPE_DEFAULT = "default";
    String REWARD_DATE_TYPE_ALL = "";
    String REWARD_DATE_TYPE_EVERY_MONTH = "M";
    String REWARD_DATE_TYPE_EVERY_YEAR = "Y";
    //奖赏类型 现金
    String REWARD_TYPE_CASH = "RB";
    //奖赏类型 积分
    String REWARD_TYPE_POINT = "GP";
    //奖赏类型 全部
    String REWARD_TYPE_ALL = "";
}
