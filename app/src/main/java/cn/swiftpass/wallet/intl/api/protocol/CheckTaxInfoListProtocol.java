package cn.swiftpass.wallet.intl.api.protocol;

import java.util.ArrayList;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.TaxInfoParamEntity;


public class CheckTaxInfoListProtocol extends BaseProtocol {

    private final String ifHKPeople;
    private ArrayList<TaxInfoParamEntity> data = new ArrayList<>();
    private final String ifOtherPeople;
    private String type;

    public CheckTaxInfoListProtocol(ArrayList<TaxInfoParamEntity> data, String ifHKPeople, String ifOtherPeople, String type, NetWorkCallbackListener<ContentEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartReg/judgeTaxInfo";
        this.type = type;
        this.data = data;
        this.ifHKPeople = ifHKPeople;
        this.ifOtherPeople = ifOtherPeople;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ENTRANCE, type);
        mBodyParams.put(RequestParams.TAXRESIDENTINFOLIST, data);
        mBodyParams.put(RequestParams.IFHKPEOPLE, ifHKPeople);
        mBodyParams.put(RequestParams.IFOTHERPEOPLE, ifOtherPeople);

    }

}
