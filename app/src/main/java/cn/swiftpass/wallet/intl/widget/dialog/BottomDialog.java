package cn.swiftpass.wallet.intl.widget.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class BottomDialog extends Dialog {

    private Context context;

    public BottomDialog(Context context) {
        this(context, R.style.CustomDialog);
        this.context = context;
    }

    public BottomDialog(Context context, int themeResId) {
        super(context, themeResId);
        initStyle(themeResId);
    }

    private void initStyle(int themeResId) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (getWindow() != null) {
            getWindow().getAttributes().windowAnimations = themeResId;
            getWindow().setBackgroundDrawableResource(android.R.color.background_light);
        }
        setCanceledOnTouchOutside(true);
        setCancelable(true);
    }

    @Override
    public void dismiss() {
        if (this.isShowing()) {
            super.dismiss();
        }
    }

    /**
     * 宽度全屏显示在底部
     */
    public void showMatchParentWidthAndGravityBottom() {
        if (context == null || getWindow() == null) {
            return;
        }
        getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = AndroidUtils.getScreenWidth(getContext());
        getWindow().setAttributes(lp);
        show();
    }

    /**
     * 宽度全屏显示在底部，从底部动画冒出
     */
    public void showWithBottomAnim() {
        if (getWindow() == null) {
            return;
        }
        getWindow().setWindowAnimations(R.style.dialog_style);
        showMatchParentWidthAndGravityBottom();
    }

}
