package cn.swiftpass.wallet.intl.module.cardmanagement.fps;


import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordDisplayBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 登记FPS成功
 */

public class FpsBindSucActivity extends BaseCompatActivity {

    @BindView(R.id.rv_bank_list)
    RecyclerView mBankListRV;
    @BindView(R.id.tv_delete_label)
    TextView mListDeleteLabelTV;

    @BindView(R.id.tv_fps_account_id)
    TextView mFpsAccountIdTV;
    @BindView(R.id.tv_account_type)
    TextView mAccountTypeTV;
    @BindView(R.id.tv_account_no)
    TextView mAccountNoTV;
    @BindView(R.id.tv_default_account_flag)
    TextView mDefaultTV;
    @BindView(R.id.tv_fps_done)
    TextView mDoneTV;


    FpsBankRecordBean mBankBean;
    private ArrayList<FpsOtherBankRecordBean> mSelectDeleteList;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C1_01_1_1);
        if (null != getIntent()) {
            mBankBean = (FpsBankRecordBean) getIntent().getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
            mSelectDeleteList = (ArrayList<FpsOtherBankRecordBean>) getIntent().getSerializableExtra(FpsConst.FPS_DEL_RECORDS);
        }
        if (null != mBankBean) {
            initView();
        }
    }

    private void initView() {
        if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mBankBean.getAccountIdType())) {
            mFpsAccountIdTV.setText(AndroidUtils.fpsEmailFormat(mBankBean.getAccountId()));
        } else {
            mFpsAccountIdTV.setText(AndroidUtils.fpsPhoneFormat(mBankBean.getAccountId()));
        }
        mAccountNoTV.setText(mBankBean.getAccountType() + AndroidUtils.getSubNumberBrackets(mBankBean.getAccountNo()));
        mDefaultTV.setText(AndroidUtils.getDefaultBankFlag(mBankBean.getDefaultBank()));
        mDoneTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        initListView();
    }

    private void initListView() {
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            mBankListRV.setVisibility(View.VISIBLE);
            mListDeleteLabelTV.setVisibility(View.VISIBLE);
            ArrayList<FpsOtherBankRecordDisplayBean> beanList = new ArrayList<>();
            for (FpsOtherBankRecordBean recordBean : mSelectDeleteList) {
                FpsOtherBankRecordDisplayBean displayBean = new FpsOtherBankRecordDisplayBean();
                displayBean.setRecordBean(recordBean);
                displayBean.setHideCheck(true);
                beanList.add(displayBean);
            }
            if (!beanList.isEmpty()) {
                mBankListRV.setVisibility(View.VISIBLE);
                mBankListRV.setAdapter(getAdapter(beanList));
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                layoutManager.setRecycleChildrenOnDetach(true);
                mBankListRV.setLayoutManager(layoutManager);
                ((IAdapter<FpsOtherBankRecordDisplayBean>) mBankListRV.getAdapter()).setData(beanList);
                mBankListRV.getAdapter().notifyDataSetChanged();
            } else {
                mBankListRV.setVisibility(View.GONE);
                mListDeleteLabelTV.setVisibility(View.GONE);
            }
        } else {
            mBankListRV.setVisibility(View.GONE);
            mListDeleteLabelTV.setVisibility(View.GONE);
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_bind_suc;
    }


    private CommonRcvAdapter<FpsOtherBankRecordDisplayBean> getAdapter(List<FpsOtherBankRecordDisplayBean> data) {
        return new CommonRcvAdapter<FpsOtherBankRecordDisplayBean>(data) {
            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new FpsBindBankListAdapter(mContext, null);
            }
        };
    }

    @Override
    public void onBackPressed() {
        FpsManageActivity.startActivity(getActivity());
        finish();
    }

    public static void startActivity(Activity activity, FpsBankRecordBean bean, ArrayList<FpsOtherBankRecordBean> delList) {
        Intent intent = new Intent(activity, FpsBindSucActivity.class);
        intent.putExtra(FpsConst.FPS_DEL_RECORDS, delList);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
        activity.startActivity(intent);
    }

    public static void startActivity(Activity activity, FpsBankRecordBean bean) {
        Intent intent = new Intent(activity, FpsBindSucActivity.class);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
        activity.startActivity(intent);
    }
}
