package cn.swiftpass.wallet.intl.module.scan.majorscan.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;


public class InputMoneyContract {

    public interface View extends IView {


        void actionMpqrPaymentWithPointFail(String errorCode, String errorMsg);

        void actionMpqrPaymentWithPointSuccess(ContentEntity response);

        void getPaymentResultFail(String errorCode, String errorMsg);

        void getPaymentResultSuccess(PaymentEnquiryResult response);

        void getMainScanOrderFail(String errorCode, String errorMsg);

        void getMainScanOrderSuccess(ContentEntity response);

        void getActionTrxGpInfoFail(String errorCode, String errorMsg);

        void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity response);

        void getPaymentTypeListFail(String errorCode, String errorMsg);

        void getPaymentTypeListSuccess(CardsEntity response, boolean isUrlMode);

        void parserTransferCodeFail(String errorCode, String errorMsg);

        void parserTransferCodeSuccess(ParserQrcodeEntity response);
    }


    public interface Presenter extends IPresenter<InputMoneyContract.View> {


        void actionMpqrPaymentWithPoint(MainScanOrderRequestEntity orderRequest);

        void getPaymentResult(String pandId, String qrcodeStr, String tansId);

        void getMainScanOrder(MainScanOrderRequestEntity orderRequest);

        void getActionTrxGpInfo(MpQrUrlInfoResult urlQRInfoEntity);

        void getPaymentTypeList(String qrcode, boolean isUrlMode);

        void parserTransferCode(String code);
    }

}
