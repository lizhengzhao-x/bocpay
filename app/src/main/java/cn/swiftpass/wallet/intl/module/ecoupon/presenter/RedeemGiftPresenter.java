package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.module.ecoupon.api.RedeemGiftProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedeemGiftContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * Created by ZhangXinchao on 2019/8/9.
 */
public class RedeemGiftPresenter extends BasePresenter<RedeemGiftContract.View> implements RedeemGiftContract.Presenter {
    @Override
    public void redeemGift(EcouponConvertEntity ecouponConvertEntity) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new RedeemGiftProtocol(ecouponConvertEntity, new NetWorkCallbackListener<RedeemResponseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().redeemGiftError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RedeemResponseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().redeemGiftSuccess(response);
                }
            }
        }).execute();
    }
}
