package cn.swiftpass.wallet.intl.module.register.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckCreditOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckFioOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ConfirmLoginProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsConfirmProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsOtpSendProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsOtpVerifyProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsQueryBankOtherRecords;
import cn.swiftpass.wallet.intl.api.protocol.LoginSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.LoginVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegisterByCreditProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegisterBySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SendCreditOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SendFioOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SendOtpBaseProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SendSmartAccountOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SwForgetPwdVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VerifyOtpBaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ForgetpwdEntity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.entity.FpsConfirmRequest;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtpEntity;
import cn.swiftpass.wallet.intl.entity.LoginSendOtpEntity;
import cn.swiftpass.wallet.intl.entity.NormalLoginSucEntity;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.RegisterAccountEntity;
import cn.swiftpass.wallet.intl.entity.SendCreditOTPEntity;
import cn.swiftpass.wallet.intl.entity.VerifyOTPEntity;
import cn.swiftpass.wallet.intl.module.register.contract.OtpContract;

public class OtpPresenter extends BasePresenter<OtpContract.View> implements OtpContract.Presenter {


    @Override
    public void sendOtp(int action, boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SendOtpBaseProtocol(action, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendOtpError(action, errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendOtpSuccess(action, response, isTryAgain);
                }
            }
        }).execute();


    }

    @Override
    public void sendFioOtp(boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SendFioOtpProtocol(new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendFioOtpError(errorCode, errorMsg, isTryAgain);
                }

            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendFioOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();
    }

    @Override
    public void sendFpsOtp(String accountId, String accountIdType, String action, String repeatOtp, boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FpsOtpSendProtocol(accountId, accountIdType, action, repeatOtp, new NetWorkCallbackListener<FpsOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendFpsOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(FpsOtpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendFpsOtpSuccess(response, accountIdType, isTryAgain);
                }
            }
        }).execute();
    }

    @Override
    public void sendCreditOtp(String account, String walletId, String pan, String expDate, String action, boolean isTryAgain) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SendCreditOtpProtocol(account, walletId, pan, expDate, action, new NetWorkCallbackListener<SendCreditOTPEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendCreditOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(SendCreditOTPEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendCreditOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();
    }

    @Override
    public void verifyCreditOtp(String account, String walletId, String pan, String expDate, String action, String otp) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckCreditOtpProtocol(account, walletId, pan, expDate, action, otp, new NetWorkCallbackListener<VerifyOTPEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyCreditOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VerifyOTPEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyCreditOtpSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void verifyOtp(int action, String otpNo, String smartLevel) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VerifyOtpBaseProtocol(action, otpNo, smartLevel, new NetWorkCallbackListener<RegisterAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOtpError(action, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RegisterAccountEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOtpSuccess(action, response);
                }
            }
        }).execute();
    }

    @Override
    public void verifyOtpWithPwd(int action, String otpNo, String pwd) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SwForgetPwdVerifyOtpProtocol(otpNo, pwd, new NetWorkCallbackListener<ForgetpwdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOtpWithPwdError(action, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ForgetpwdEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyOtpWithPwdSuccess(action, response);
                }
            }
        }).execute();
    }

    @Override
    public void verifyFioOtp(String otpNo) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckFioOtpProtocol(otpNo, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyFioOtpError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyFioOtpSuccess(response);
                }
            }
        }).execute();


    }

    @Override
    public void verifyFpsOtp(String otpNo, String action, String accountId, String accountIdType, int pageAction) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FpsOtpVerifyProtocol(otpNo, action, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyFpsOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    fpsQueryBankOtherRecords(accountId, accountIdType, pageAction);
                }
            }
        }).execute();
    }

    @Override
    public void verifySmartAccountOtp(String walletId, String action, String otpNo, BankLoginResultEntity smartInfo) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckSmartAccountOtpProtocol(walletId, action, otpNo, smartInfo, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifySmartAccountOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifySmartAccountOtpSuccess(response);
                }

            }
        }).execute();
    }

    @Override
    public void registerBySmartAccount(String passcode, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new RegisterBySmartAccountProtocol(passcode, action, new NetWorkCallbackListener<RegSucBySmartEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerBySmartAccountError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(RegSucBySmartEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerBySmartAccountSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getConfirmRegister(String passCode, String walletId, String cardId, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new RegisterByCreditProtocol(passCode, walletId, cardId, action, new NetWorkCallbackListener<RegSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmRegisterError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RegSucEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmRegisterSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getConfirmLogin(String account, String walletId, String passcode, String region, String action, String isContinue, boolean needVerify) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ConfirmLoginProtocol(account, walletId, passcode, region, action, isContinue, needVerify, new NetWorkCallbackListener<NormalLoginSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmLoginError(errorCode, errorMsg, isContinue, passcode, region, action, needVerify);
                }

            }

            @Override
            public void onSuccess(NormalLoginSucEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().confirmLoginSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void fpsQueryBankOtherRecords(String accountId, String accountIdType, int pageAction) {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new FpsQueryBankOtherRecords(accountId, accountIdType, new NetWorkCallbackListener<FpsOtherBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (pageAction == Constants.DATA_TYPE_FPS_ADD) {
                        fpsConfirmAdd(getView().getBankRecord(), getView().getFppRefNo(), false);
                    } else {
                        getView().dismissDialog();
                        getView().fpsQueryBankOtherRecordsError(errorCode, errorMsg);
                    }
                }
            }

            @Override
            public void onSuccess(FpsOtherBankRecordsEntity response) {
                if (getView() != null) {
                    boolean flag = false;
                    if (null != response) {
                        if ((null != response.getBankChina() && !response.getBankChina().isEmpty()) || (null != response.getData() && !response.getData().isEmpty())) {
                            flag = true;
                        }
                    }
                    if (pageAction == Constants.DATA_TYPE_FPS_ADD) {
                        if (!flag) {
                            fpsConfirmAdd(getView().getBankRecord(), getView().getFppRefNo(), false);
                        } else {
                            getView().dismissDialog();
                            getView().fpsQueryBankOtherRecordsSuccess(response);
                        }
                    } else {
                        getView().dismissDialog();
                        getView().fpsQueryBankOtherRecordsSuccess(response);
                    }
                }
            }
        }).execute();
    }

    @Override
    public void fpsConfirmAdd(FpsBankRecordBean record, String fppRefNo, boolean showDialog) {
        if (getView() != null && showDialog) {
            getView().showDialogNotCancel();
        }
        FpsConfirmRequest request = new FpsConfirmRequest();
        request.accountId = record.getAccountId();
        request.accountIdType = record.getAccountIdType();
        request.accountNo = record.getAccountNo();
        request.action = FpsConst.FPS_ACTION_ADD;
        request.bankCode = "";
        request.defaultBank = record.getDefaultBank();
        request.fppRefNo = fppRefNo;

        new FpsConfirmProtocol(request, new NetWorkCallbackListener<FpsConfirmEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().fpsConfirmAddError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(FpsConfirmEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().fpsConfirmAddSuccess(response);
                }
            }
        }).execute();

    }


    @Override
    public void loginVerifyOtp(String mobile, String action, String cardId, boolean isContinue, String otpCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new LoginVerifyOtpProtocol(mobile, cardId, otpCode, action, isContinue, new NetWorkCallbackListener<NormalLoginSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().loginVerifyError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NormalLoginSucEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().loginVerifySuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void loginSendOtp(String mobile,boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new LoginSendOtpProtocol(mobile, "L", new NetWorkCallbackListener<LoginSendOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().loginSendOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(LoginSendOtpEntity loginSendOtpEntity) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().loginSendOtpSuccess(loginSendOtpEntity,isTryAgain);
                }

            }
        }).execute();
    }

    @Override
    public void sendSmartAccountOtp(String action, String walletId, boolean isTryAgain) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SendSmartAccountOtpProtocol(action, walletId, new NetWorkCallbackListener<ForgetpwdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendSmartAccountOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(ForgetpwdEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sendSmartAccountOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();
    }
}
