package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 11:08
 * 主扫下单请求参数
 */

public class MainScanOrderRequestEntity extends BaseEntity {

    //M 二维码类型 E和U。E代表EMV码，U代表url码
    String qrcType;

    //M银行卡号（全数字）
    String cardId;
    //M交易金额
    String trxAmt;
    //费率,EPEMV码需要费率,目前传的是0
    String trxFeeAmt;
    //M二维码信息
    String mpQrCode;
    //商家名称
    String merchantName;
    //优惠券信息
    String couponInfo;
    //账单号
    //String billNumber;
    //手机号
    String mobile;
    //M交易Id
    String txnId;
    //店铺标签
    String storeLabel;
    //Loyalty Number
    String loyaltyNo;
    //Reference Label
    String refLabel;
    //买主标签
    String customerLable;
    //终端标签
    String terminalLabel;
    //交易目的
    String trxPurpose;
    //消费者邮箱
    String consumerEmail;
    //消费这地址
    String consumerAddress;
    //消费者电话
    String consumerMobile;
    //C交易币种
    String txnCurr;

    String gpRequired;

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    String payAmt;
    String avaGpCount;

    public String getGpRequired() {
        return gpRequired;
    }

    public void setGpRequired(String gpRequired) {
        this.gpRequired = gpRequired;
    }

    public String getAvaGpCount() {
        return avaGpCount;
    }

    public void setAvaGpCount(String avaGpCount) {
        this.avaGpCount = avaGpCount;
    }

    public String getRedeemGpCount() {
        return redeemGpCount;
    }

    public void setRedeemGpCount(String redeemGpCount) {
        this.redeemGpCount = redeemGpCount;
    }

    public String getRedeemGpAmt() {
        return redeemGpAmt;
    }

    public void setRedeemGpAmt(String redeemGpAmt) {
        this.redeemGpAmt = redeemGpAmt;
    }

    public String getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(String redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getGpCode() {
        return gpCode;
    }

    public void setGpCode(String gpCode) {
        this.gpCode = gpCode;
    }

    public String getYearHolding() {
        return yearHolding;
    }

    public void setYearHolding(String yearHolding) {
        this.yearHolding = yearHolding;
    }

    String redeemGpCount;
    String redeemGpAmt;
    String redeemFlag;
    String gpCode;
    String yearHolding;

    public void setQrcType(String qrcType) {
        this.qrcType = qrcType;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public void setTrxAmt(String trxAmt) {
        this.trxAmt = trxAmt;
    }

    public void setTrxFeeAmt(String trxFeeAmt) {
        this.trxFeeAmt = trxFeeAmt;
    }

    public void setMpQrCode(String mpQrCode) {
        this.mpQrCode = mpQrCode;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }


    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public void setStoreLabel(String storeLabel) {
        this.storeLabel = storeLabel;
    }

    public void setLoyaltyNo(String loyaltyNo) {
        this.loyaltyNo = loyaltyNo;
    }

    public void setRefLabel(String refLabel) {
        this.refLabel = refLabel;
    }

    public void setCustomerLable(String customerLable) {
        this.customerLable = customerLable;
    }

    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public void setTrxPurpose(String trxPurpose) {
        this.trxPurpose = trxPurpose;
    }

    public void setConsumerEmail(String consumerEmail) {
        this.consumerEmail = consumerEmail;
    }

    public void setConsumerAddress(String consumerAddress) {
        this.consumerAddress = consumerAddress;
    }

    public void setConsumerMobile(String consumerMobile) {
        this.consumerMobile = consumerMobile;
    }

    public void setTxnCurr(String txnCurr) {
        this.txnCurr = txnCurr;
    }

    public String getQrcType() {
        return qrcType;
    }

    public String getCardId() {
        return cardId;
    }

    public String getTrxAmt() {
        return trxAmt;
    }

    public String getTrxFeeAmt() {
        return trxFeeAmt;
    }

    public String getMpQrCode() {
        return mpQrCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getCouponInfo() {
        return couponInfo;
    }


    public String getMobile() {
        return mobile;
    }

    public String getTxnId() {
        return txnId;
    }

    public String getStoreLabel() {
        return storeLabel;
    }

    public String getLoyaltyNo() {
        return loyaltyNo;
    }

    public String getRefLabel() {
        return refLabel;
    }

    public String getCustomerLable() {
        return customerLable;
    }

    public String getTerminalLabel() {
        return terminalLabel;
    }

    public String getTrxPurpose() {
        return trxPurpose;
    }

    public String getConsumerEmail() {
        return consumerEmail;
    }

    public String getConsumerAddress() {
        return consumerAddress;
    }

    public String getConsumerMobile() {
        return consumerMobile;
    }

    public String getTxnCurr() {
        return txnCurr;
    }


}
