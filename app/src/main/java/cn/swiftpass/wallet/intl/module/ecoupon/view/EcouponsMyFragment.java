package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.EcouponsItemEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.GridLinearDivider;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.MyEcouponsContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.MyEcouponsPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.utils.FilterUtils;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.MyEcouponsItemHorizontalAdapter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.MyEcouponsItemVerticalAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 兑换电子券/我的电子券
 */
public class EcouponsMyFragment extends BaseFragment<MyEcouponsContract.Presenter> implements MyEcouponsContract.View {
    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    @BindView(R.id.id_total_grade_text)
    TextView idTotalGradeText;
    @BindView(R.id.id_filter_view)
    RelativeLayout idFilterView;
    @BindView(R.id.id_total_filter)
    TextView idTotalFilter;
    @BindView(R.id.id_filter)
    LinearLayout idFilter;
    @BindView(R.id.id_no_ecoupon_view)
    LinearLayout idNoEcouponView;

    private List<MyEVoucherEntity.EvoucherItem> mECoupItems;
    private GridLayoutManager mGridLayoutManager;
    private MyEVoucherEntity myEVoucherEntity;
    private List<MyEVoucherEntity.EvoucherItem> mFilterECoupItems;
    private List<MyEVoucherEntity.EvoucherItem> evoucherItemsTotal;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    private GridDivider gridDivider;
    private GridLinearDivider gridLinearDivider;

    @Override
    public void initTitle() {

    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    protected MyEcouponsContract.Presenter loadPresenter() {
        return new MyEcouponsPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_ecoupon_my;
    }

    @Override
    protected void initView(View v) {
        mFilterECoupItems = new ArrayList<>();
        evoucherItemsTotal = new ArrayList<>();
        mECoupItems = new ArrayList<>();
        mGridLayoutManager = new GridLayoutManager(getActivity(), 1);

        idRecyclerview.setLayoutManager(mGridLayoutManager);
        if (getContext() != null) {
            gridDivider = new GridDivider(AndroidUtils.dip2px(getContext(), 20), this.getResources().getColor(R.color.app_white));
            gridLinearDivider = new GridLinearDivider(AndroidUtils.dip2px(getContext(), 30), this.getResources().getColor(R.color.app_white));

        }
        idRecyclerview.addItemDecoration(gridLinearDivider);
        idRecyclerview.setAdapter(getAdapter(null));
        if (idRecyclerview.getAdapter() != null) {
            ((IAdapter<MyEVoucherEntity.EvoucherItem>) idRecyclerview.getAdapter()).setData(mECoupItems);
        }
        idRecyclerview.getAdapter().notifyDataSetChanged();
        idFilterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initFilterPopView();
            }
        });
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPresenter.getMyEcouponsList();
            }
        });

        autoRefreshList();
        idTotalFilter.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                if (myEVoucherEntity == null || myEVoucherEntity.getBuMaps() == null) return;
                for (int i = 0; i < myEVoucherEntity.getBuMaps().size(); i++) {
                    myEVoucherEntity.getBuMaps().get(i).setSel(true);
                }
                filterCurrentEcoupons(myEVoucherEntity.getBuMaps());
            }
        });
        idNoEcouponView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (getParentFragment() instanceof ECouponsManagerFragment) {
                    ECouponsManagerFragment eCouponsManagerFragment = (ECouponsManagerFragment) getParentFragment();
                    eCouponsManagerFragment.setSelLeftView();
                }
            }
        });
    }

    private void initFilterPopView() {
        if (myEVoucherEntity.getBuMaps() == null) return;
        FilterUtils.initFilterPopView(getActivity(), idFilterView, myEVoucherEntity.getBuMaps(), new FilterUtils.OnFilterConfimListener() {
            @Override
            public void onFilterConfrim(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities) {
                filterCurrentEcoupons(menuFilterEntities);
            }
        });
    }

    public void autoRefreshList() {
        if (smartRefreshLayout == null) return;
        smartRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mPresenter.getMyEcouponsList();
            }
        });
    }

    /**
     * 过滤筛选的类型
     *
     * @param menuFilterEntities
     */
    private void filterCurrentEcoupons(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities) {
        List<MyEVoucherEntity.EvoucherItem> eVoucherListBeansTotal = myEVoucherEntity.getRows();
        mECoupItems.clear();
        for (int i = 0; i < eVoucherListBeansTotal.size(); i++) {
            for (int k = 0; k < menuFilterEntities.size(); k++) {
                if (eVoucherListBeansTotal.get(i).getBU().equals(menuFilterEntities.get(k).getAswBu()) && menuFilterEntities.get(k).isSel()) {
                    mECoupItems.add(eVoucherListBeansTotal.get(i));
                    break;
                }
            }
        }
        if (idRecyclerview.getAdapter() != null) {
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }


    private CommonRcvAdapter<MyEVoucherEntity.EvoucherItem> getAdapter(final List<MyEVoucherEntity.EvoucherItem> data) {
        return new CommonRcvAdapter<MyEVoucherEntity.EvoucherItem>(data) {

            @Override
            public Object getItemType(MyEVoucherEntity.EvoucherItem demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                if (type.equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL)) {
                    return new MyEcouponsItemHorizontalAdapter(getContext(), new MyEcouponsItemHorizontalAdapter.OnItemEcouponItemUseClickListener() {
                        @Override
                        public void onItemEcouponClick(int position) {
                            goUseEcouponEvent(position);
                        }

                        @Override
                        public void onItemShowMore(int position) {
                            showMorePopView(position);
                        }
                    });
                } else {
                    return new MyEcouponsItemVerticalAdapter(getContext(), new MyEcouponsItemHorizontalAdapter.OnItemEcouponItemUseClickListener() {
                        @Override
                        public void onItemEcouponClick(int position) {
                            goUseEcouponEvent(position);
                        }

                        @Override
                        public void onItemShowMore(int position) {
                            showMorePopView(position);
                        }
                    });
                }
            }
        };
    }

    private void showMorePopView(int positon) {
        if (evoucherItemsTotal == null) return;
        ShowMoreRecordPop showMoreRecordPop = new ShowMoreRecordPop(getActivity(), evoucherItemsTotal.get(positon), evoucherItemsTotal.get(positon).getItemName());
        showMoreRecordPop.show();
    }


    private void goUseEcouponEvent(int position) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.ECOUPONCONVERTENTITY, evoucherItemsTotal.get(position));
        mHashMaps.put(Constants.ECOUPON_TITLE, evoucherItemsTotal.get(position).geteVoucherTc());
        ActivitySkipUtil.startAnotherActivity(getActivity(), EcouponsDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public void changeHorizonal() {
        for (int i = 0; i < mECoupItems.size(); i++) {
            String type = mECoupItems.get(i).getType();
            mECoupItems.get(i).setType(type.equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL) ? EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL : EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL);
        }
        if (mECoupItems.size() > 0) {
            if (mECoupItems.get(0).getType().equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL)) {
                mGridLayoutManager.setSpanCount(2);
                idRecyclerview.removeItemDecoration(gridLinearDivider);
                idRecyclerview.addItemDecoration(gridDivider);
            } else {
                idRecyclerview.removeItemDecoration(gridDivider);
                idRecyclerview.addItemDecoration(gridLinearDivider);
                mGridLayoutManager.setSpanCount(1);
            }
        }
        if (idRecyclerview.getAdapter() != null) {
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }


    @Override
    public void getMyEcouponsListSuccess(MyEVoucherEntity response) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        } else {
            return;
        }
        myEVoucherEntity = response;
        evoucherItemsTotal.clear();
        evoucherItemsTotal.addAll(response.getRows());
        for (int i = 0; i < evoucherItemsTotal.size(); i++) {
            evoucherItemsTotal.get(i).setType(isHorinzonal() ? EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL : EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL);
        }
        mECoupItems.clear();
        mECoupItems.addAll(evoucherItemsTotal);
        idNoEcouponView.setVisibility(evoucherItemsTotal.size() == 0 ? View.VISIBLE : View.GONE);
        for (int i = 0; i < myEVoucherEntity.getBuMaps().size(); i++) {
            myEVoucherEntity.getBuMaps().get(i).setSel(true);
        }
        showChangeBtn();
        if (idRecyclerview.getAdapter() != null) {
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void getMyEcouponsListError(String errorCode, String errorMsg) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        } else {
            return;
        }
        showErrorMsgDialog(getContext(), errorMsg);
    }

    public boolean isHorinzonal() {
        if (mGridLayoutManager == null) return true;
        return mGridLayoutManager.getSpanCount() == 1;
    }


    public void showChangeBtn() {
        if (getParentFragment() instanceof ECouponsManagerFragment) {
            if (evoucherItemsTotal == null) return;
            ECouponsManagerFragment eCouponsManagerFragment = (ECouponsManagerFragment) getParentFragment();
            if (eCouponsManagerFragment.getCurrentSelPosition() == 0) return;
            eCouponsManagerFragment.setShowChangeView(evoucherItemsTotal.size() != 0);
        }
    }

    public boolean isCreatSuccess() {
        return smartRefreshLayout != null;
    }
}
