package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/20
 * 转账确认接口
 */

public class TransferConfirmByBocProtocol extends BaseProtocol {
    public static final String TAG = TransferConfirmByBocProtocol.class.getSimpleName();

    String mTransferOrderId;
    private boolean isLishi;
    String mAction;

    public TransferConfirmByBocProtocol(String transferOrderId, boolean isLishiIn, NetWorkCallbackListener callback) {
        mTransferOrderId = transferOrderId;
        mDataCallback = callback;
        isLishi = isLishiIn;
        mUrl = "api/transfer/bocPayTransferExcute";
    }

    public TransferConfirmByBocProtocol(String transferOrderId, boolean isLishiIn, String action, NetWorkCallbackListener callback) {
        mTransferOrderId = transferOrderId;
        mDataCallback = callback;
        isLishi = isLishiIn;
        mAction = action;
        mUrl = "api/transfer/bocPayTransferExcute";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TRANSFERORDERID, mTransferOrderId);
        if (isLishi) {
            mBodyParams.put(RequestParams.TRANSFERCATEGORY, "7");
        }
        if (!TextUtils.isEmpty(mAction)) {
            mBodyParams.put(RequestParams.ACTION, "EXTPAY");
        }
    }
}
