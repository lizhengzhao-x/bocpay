package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.ButterKnife;

public class LishiHistoryEmptyHolder extends RecyclerView.ViewHolder {
    public LishiHistoryEmptyHolder(Activity activity, View itemHolder) {
        super(itemHolder);
        ButterKnife.bind(this, itemView);
    }
}
