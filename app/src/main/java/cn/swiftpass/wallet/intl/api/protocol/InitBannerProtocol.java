package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * Created by ZhangXinchao on 2019/12/4.
 */
public class InitBannerProtocol extends BaseProtocol {

    @Override
    public boolean isNeedLogin() {
        return true;
    }


    public InitBannerProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/postLogin/initData";
    }
}
