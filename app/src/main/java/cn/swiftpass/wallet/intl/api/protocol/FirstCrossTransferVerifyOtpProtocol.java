package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class FirstCrossTransferVerifyOtpProtocol extends BaseProtocol {

    String txnId;
    String verifyCode;

    public FirstCrossTransferVerifyOtpProtocol(String txnIdIn, String verifyCodeIn, NetWorkCallbackListener dataCallback) {
        this.txnId = txnIdIn;
        this.mDataCallback = dataCallback;
        this.verifyCode = verifyCodeIn;
        mUrl = "api/crossTransfer/verifyOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);
        mBodyParams.put(RequestParams.ACTION, "CROSSTRANSFER");
        mBodyParams.put(RequestParams.VERIFY, verifyCode);
    }
}
