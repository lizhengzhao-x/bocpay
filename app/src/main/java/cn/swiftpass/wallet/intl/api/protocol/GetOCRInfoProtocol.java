package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetOCRInfoProtocol extends BaseProtocol {

    public static final String TAG = LoginCheckProtocol.class.getSimpleName();

    String seqNumber;
    String fileId;

    public GetOCRInfoProtocol(String type, String seqNumber, String fileId, NetWorkCallbackListener dataCallback) {
        this.seqNumber = seqNumber;
        this.fileId = fileId;
        this.mDataCallback = dataCallback;
        //不同流程 后台区分 ur不同
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/getOCRInfo";
        } else {
            mUrl = "api/smartReg/getOCRInfo";
        }
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.SEQNUMBER, seqNumber);
        mBodyParams.put(RequestParams.FILEID, fileId);
    }
}
