package cn.swiftpass.wallet.intl.module.rewardregister.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 信用卡奖赏登记
 */
public class RewardRegisterProtocol extends BaseProtocol {

    String pan;

    String campaignId;

    public RewardRegisterProtocol(String pan, String campaignId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/creditCardReward/rewardRegister";
        this.pan = pan;
        this.campaignId = campaignId;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAN_ID, pan);
        mBodyParams.put(RequestParams.CAMPAIGNID, campaignId);
    }


}
