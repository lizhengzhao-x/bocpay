package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;

/**
 * Created by ramon on 2018/10/9.
 */

public class NormalLoginSucEntity extends AutoLoginSucEntity {

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
    }

    private String serverPubKey;
}

