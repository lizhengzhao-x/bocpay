package cn.swiftpass.wallet.intl.module.transfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/10/31.
 */
public class ContractTitleEntity extends BaseEntity {

    public ContractTitleEntity(String titleName) {
        this.titleName = titleName;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    private String titleName;
}
