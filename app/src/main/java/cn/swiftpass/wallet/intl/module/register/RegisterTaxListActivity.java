package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.entity.RegisterTaxResultEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoParamEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoReason;
import cn.swiftpass.wallet.intl.entity.TaxInfoStatusEntity;
import cn.swiftpass.wallet.intl.entity.TaxResidentEntity;
import cn.swiftpass.wallet.intl.module.register.adapter.OnTaxDataChangeListener;
import cn.swiftpass.wallet.intl.module.register.adapter.TaxListAdapter;
import cn.swiftpass.wallet.intl.module.register.contract.RegisterTaxContact;
import cn.swiftpass.wallet.intl.module.register.model.RegisterDetailsData;
import cn.swiftpass.wallet.intl.module.register.presenter.RegTaxListPresent;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.entity.Constants.DATA_COUNTRY_NAME_CODE;

public class RegisterTaxListActivity extends BaseCompatActivity<RegisterTaxContact.Presenter> implements RegisterTaxContact.View, OnTaxDataChangeListener {
    public static final int REQ_SELECT_TAX_COUNTRY_CODE = 101;
    public static final int REQ_SELECT_TAX_REASON_CODE = 102;
    public static final String DATA_CURRENT_TAX_COUNTRY = "DATA_CURRENT_TAX_COUNTRY";
    public static final String DATA_CURRENT_TAX_REASON = "DATA_CURRENT_TAX_REASON";
    public static final String DATA_TAX_COUNTRY_LIST = "DATA_TAX_COUNTRY_LIST";
    public static final String DATA_TAX_REASON_LIST = "DATA_TAX_REASON_LIST";
    public static final String DATA_TAX_INDEX = "DATA_TAX_INDEX";
    @BindView(R.id.ry_tax_list)
    RecyclerView ryTaxList;
    @BindView(R.id.tv_next)
    TextView tvNext;

    private int mCurrentType;
    private ArrayList<TaxInfoEntity> mTaxInfosList;
    private S2RegisterInfoEntity registerInfoEntity;
    private RegisterCustomerInfo registerCustomerInfo;
    private TaxListAdapter taxListAdapter;
    private RegisterTaxResultEntity registerTaxResult;
    private RegisterDetailsData registerDetailsData;


    @Override
    protected void init() {
        if (getIntent() != null) {
            registerInfoEntity = (S2RegisterInfoEntity) getIntent().getSerializableExtra(Constants.OCR_INFO);
            mCurrentType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, 0);
            registerCustomerInfo = (RegisterCustomerInfo) getIntent().getSerializableExtra(Constants.REGISTER_CUSTOM_INFO);
            Serializable detailSerData = getIntent().getSerializableExtra(RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA);
            if (detailSerData != null) {
                registerDetailsData = (RegisterDetailsData) detailSerData;
                if (registerDetailsData.registerTaxResult != null) {
                    registerTaxResult = registerDetailsData.registerTaxResult;
                }
            }
        }

        //注册流程如果DJ601 activity 退出
        ProjectApp.getTempTaskStack().add(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        }

        setToolBarTitle(R.string.P3_A1_16_1);
        DataCollectManager.getInstance().sendInputTaxInfo();
        initView();
        initData();
        tvNext.setEnabled(false);
    }

    private void initView() {
        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ryTaxList.setLayoutManager(layout);
        taxListAdapter = new TaxListAdapter(this);
        if (registerDetailsData != null) {
            taxListAdapter.initData(registerDetailsData);
        }
        taxListAdapter.setRegisterInfoData(registerInfoEntity);
        taxListAdapter.setChangeTaxDataListener(this);
        ryTaxList.setAdapter(taxListAdapter);
    }

    private void initData() {

        String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;

        mPresenter.getTaxResidentRelationInfo(type, null);

    }


    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);

        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_INPUTRESIDENTNUMBER);
    }


    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.INPUT_REGISTRATION_ADDRESS;
        analysisPageEntity.current_address = PagerConstant.INPUT_REGISTRATION_TAX;
        return analysisPageEntity;
    }


    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA || mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.INPUT_REGISTRATION_TAX);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.hide();
            }
        }
    }

    private void sendRegisterInfo() {

        if (taxListAdapter.resultSelectQuestionOne == TaxListAdapter.DATA_QUESTION_NO && taxListAdapter.resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_NO) {
            showErrorMsgDialog(this, getString(R.string.PA2109_1_17));
            return;
        }

        if (registerInfoEntity != null) {
            String ifHKPeople = taxListAdapter.resultSelectQuestionOne == TaxListAdapter.DATA_QUESTION_YES ? "Y" : "N";
            String ifOtherPeople = taxListAdapter.resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_YES ? "Y" : "N";
            String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;

            ArrayList<TaxInfoStatusEntity> data = taxListAdapter.getData();
            ArrayList<TaxInfoParamEntity> sendDataList = new ArrayList<>();
            if (data != null) {
                if (taxListAdapter.resultSelectQuestionOne == TaxListAdapter.DATA_QUESTION_YES) {
                    sendDataList.add(new TaxInfoParamEntity(DATA_COUNTRY_NAME_CODE[0], registerInfoEntity.getCertNo(), getString(R.string.P3_A1_8_7_3)));
                }
                for (TaxInfoStatusEntity taxInfoStatusEntity : data) {
                    TaxInfoParamEntity statusEntity = new TaxInfoParamEntity();
                    if (taxInfoStatusEntity.getReason() != null) {
                        statusEntity.setNoResidentReason(taxInfoStatusEntity.getReason().getNoResidentReasonCode());
                        statusEntity.setNoResidentReasonInfo(taxInfoStatusEntity.getReason().getNoResidentReason());
                    }
                    if (taxInfoStatusEntity.getTaxNumber() != null) {
                        statusEntity.setTaxNumber(taxInfoStatusEntity.getTaxNumber());
                    }
                    if (taxInfoStatusEntity.getTaxResidence() != null) {
                        statusEntity.setTaxResidentRegionName(taxInfoStatusEntity.getTaxResidence().getTaxResidentRegionName());
                        statusEntity.setResidenceJurisdiction(taxInfoStatusEntity.getTaxResidence().getResidenceJurisdiction());
                    }

                    sendDataList.add(statusEntity);
                }
            }

            mPresenter.checkTaxList(sendDataList, ifHKPeople, ifOtherPeople, type);

        }

    }


    private void checkIdRequest(final RegisterCustomerInfo registerCustomerInfo) {
        String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;
        mPresenter.checkIdvRequest(type, registerCustomerInfo);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_reg_tax;
    }

    @Override
    protected RegisterTaxContact.Presenter createPresenter() {
        return new RegTaxListPresent();
    }

    @Override
    public void onCheckTaxFail(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        showErrorMsgDialog(RegisterTaxListActivity.this, errorMsg);
    }

    @Override
    public void onCheckTaxSuccess(RegisterIDEntity response, RegisterCustomerInfo registerCustomerInfo) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentType);
        mHashMaps.put(Constants.MAX_LITMIT_INFO, response);
        if (taxListAdapter != null) {
            RegisterDetailsData taxListUIData = taxListAdapter.getTaxListUIData();
            //如果编辑传递过来的数据中，有修改限额的数据，则保留修改限额的数据
            if (registerDetailsData != null) {
                taxListUIData.currentLimit = registerDetailsData.currentLimit;
            }
            taxListUIData.registerTaxResult = registerTaxResult;
            mHashMaps.put(RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA, taxListUIData);
        }
        mHashMaps.put(Constants.CUSTOMER_INFO, registerCustomerInfo);
        mHashMaps.put(Constants.OCR_INFO, getIntent().getExtras().getSerializable(Constants.OCR_INFO));
        ActivitySkipUtil.startAnotherActivity(RegisterTaxListActivity.this, SetTransactionLimitActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    @Override
    public void getTaxResidentRelationInfoFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getTaxResidentRelationInfoSuccess(RegisterTaxResultEntity response) {

        if (registerTaxResult != null && response.getTaxResidentRegionList() != null && registerTaxResult.getTaxResidentRegionList() != null) {
            for (TaxResidentEntity taxResidentEntity : registerTaxResult.getTaxResidentRegionList()) {
                if (taxResidentEntity.isSelectStatus()) {
                    for (TaxResidentEntity tax : response.getTaxResidentRegionList()) {
                        if (tax.getResidenceJurisdiction().equals(taxResidentEntity.getResidenceJurisdiction())) {
                            tax.setSelectStatus(true);
                        }
                    }
                }
            }

        }
        this.registerTaxResult = response;
    }

    @Override
    public void checkTaxInfoListProtocolFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void checkTaxInfoListProtocolSuccess(ContentEntity response, ArrayList<TaxInfoParamEntity> data) {
        registerCustomerInfo.mTaxInfos = data;
        checkIdRequest(registerCustomerInfo);
    }

    @OnClick({R.id.tv_next, R.id.tv_customer_statement, R.id.tv_important_tips})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) return;
        String lan = SpUtils.getInstance(RegisterTaxListActivity.this).getAppLanguage();
        HashMap<String, Object> mHashMaps = new HashMap<>();
        switch (view.getId()) {
            case R.id.tv_next:
                sendRegisterInfo();
                break;
            case R.id.tv_customer_statement:

                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_US);

                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.P3_A1_16_5));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_important_tips:

                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_US);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.P3_A1_16_6));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                break;
            default:
                break;
        }
    }


    @Override
    public void OnChangeNextBtn(boolean status) {
        tvNext.setEnabled(status);
    }


    @Override
    public void selectCountryData(int index, TaxResidentEntity residenceEntity) {
        if (registerTaxResult != null) {
            skipTaxResidencePage(index, residenceEntity);
        } else {
            String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;
            mPresenter.getTaxResidentRelationInfo(type, new SkipPageFromTaxFunction() {
                @Override
                public void skipNextPage() {
                    skipTaxResidencePage(index, residenceEntity);
                }
            });
        }

    }

    private void skipTaxResidencePage(int index, TaxResidentEntity residenceEntity) {
        HashMap<String, Object> param = new HashMap<>();
        if (residenceEntity != null) {
            param.put(RegisterTaxListActivity.DATA_CURRENT_TAX_COUNTRY, residenceEntity);
        }
        param.put(RegisterTaxListActivity.DATA_TAX_INDEX, index);
        param.put(RegisterTaxListActivity.DATA_TAX_COUNTRY_LIST, registerTaxResult.getTaxResidentRegionList());
        ActivitySkipUtil.startAnotherActivityForResult(this, SelectTaxResidenceJurisdictionCodeActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQ_SELECT_TAX_COUNTRY_CODE);
    }

    private void skipTaxReasonPage(int index, TaxInfoReason taxInfoReason) {
        HashMap<String, Object> param = new HashMap<>();
        if (taxInfoReason != null) {
            param.put(RegisterTaxListActivity.DATA_CURRENT_TAX_REASON, taxInfoReason);
        }
        param.put(RegisterTaxListActivity.DATA_TAX_INDEX, index);
        param.put(RegisterTaxListActivity.DATA_TAX_REASON_LIST, registerTaxResult.getNoResidentReasonList());
        ActivitySkipUtil.startAnotherActivityForResult(this, SelectTaxReasonActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQ_SELECT_TAX_REASON_CODE);
    }


    @Override
    public void selectTaxReasonData(int index, TaxInfoReason reasonEntity) {
        if (registerTaxResult != null) {
            skipTaxReasonPage(index, reasonEntity);
        } else {
            String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;
            mPresenter.getTaxResidentRelationInfo(type, new SkipPageFromTaxFunction() {
                @Override
                public void skipNextPage() {
                    skipTaxReasonPage(index, reasonEntity);
                }
            });
        }
    }

    @Override
    public void deleteTaxInfo(int index, TaxInfoStatusEntity taxInfoStatusEntity) {
        //删除时，需要把国家列表中的选中状态改为false
        if (taxInfoStatusEntity.getTaxResidence() != null) {
            for (TaxResidentEntity taxResidentEntity : registerTaxResult.getTaxResidentRegionList()) {
                if (taxResidentEntity.getResidenceJurisdiction().equals(taxInfoStatusEntity.getTaxResidence().getResidenceJurisdiction())) {
                    taxResidentEntity.setSelectStatus(false);
                }
            }
        }

    }

    @Override
    public void resetDataResidentList() {
        if (registerTaxResult != null && registerTaxResult.getTaxResidentRegionList() != null) {
            for (TaxResidentEntity taxResidentEntity : registerTaxResult.getTaxResidentRegionList()) {
                taxResidentEntity.setSelectStatus(false);
            }
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data != null && requestCode == REQ_SELECT_TAX_COUNTRY_CODE) {
                int index = data.getIntExtra(RegisterTaxListActivity.DATA_TAX_INDEX, -1);
                TaxResidentEntity countryData = (TaxResidentEntity) data.getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_COUNTRY);
                if (index != -1 && countryData != null) {
                    ArrayList<TaxInfoStatusEntity> otherTaxList = taxListAdapter.getData();
                    String oldResidenceJurisdiction = "";
                    //改变当前页面，税务地区中条目的信息
                    if (index < otherTaxList.size()) {
                        TaxInfoStatusEntity taxInfoStatusEntity = otherTaxList.get(index);
                        //如果改变的税务条目中原来的地区不为空，需要把原来的地区从不可选择改为可以选择
                        if (taxInfoStatusEntity.getTaxResidence() != null &&
                                !TextUtils.isEmpty(taxInfoStatusEntity.getTaxResidence().getResidenceJurisdiction())) {
                            oldResidenceJurisdiction = taxInfoStatusEntity.getTaxResidence().getResidenceJurisdiction();
                        }


                        TaxInfoStatusEntity newTaxInfo = new TaxInfoStatusEntity();
                        countryData.setSelectStatus(true);
                        newTaxInfo.setTaxResidence(countryData);
                        otherTaxList.set(index, newTaxInfo);
                        taxListAdapter.notifyDataSetChanged();
                        taxListAdapter.refreshCheckItem(index);
                    }
                    //改变当前200多个国家的列表中的选择状态，方便在国家地区列表选择时进行判断
                    for (TaxResidentEntity taxResidentEntity : registerTaxResult.getTaxResidentRegionList()) {
                        if (taxResidentEntity.getResidenceJurisdiction().equals(countryData.getResidenceJurisdiction())) {
                            taxResidentEntity.setSelectStatus(true);
                        } else if (!TextUtils.isEmpty(oldResidenceJurisdiction) && taxResidentEntity.getResidenceJurisdiction().equals(oldResidenceJurisdiction)) {
                            //如果改变的税务条目中原来的地区不为空，需要把原来的地区从不可选择改为可以选择
                            taxResidentEntity.setSelectStatus(false);
                        }

                    }
                }
            } else if (data != null && requestCode == REQ_SELECT_TAX_REASON_CODE) {
                int index = data.getIntExtra(RegisterTaxListActivity.DATA_TAX_INDEX, -1);
                TaxInfoReason taxInfoReason = (TaxInfoReason) data.getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_REASON);
                if (index != -1 && taxInfoReason != null) {
                    ArrayList<TaxInfoStatusEntity> otherTaxList = taxListAdapter.getData();
                    //改变当前页面，税务地区中条目的信息
                    if (index < otherTaxList.size()) {
                        TaxInfoStatusEntity taxInfoStatusEntity = otherTaxList.get(index);
                        taxInfoStatusEntity.setReason(taxInfoReason);
                        taxListAdapter.notifyDataSetChanged();
                        taxListAdapter.refreshCheckItem(index);
                    }
                }
            }
        }
    }


}
