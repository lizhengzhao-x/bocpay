package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.UnOpenLiShiListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

public class UnOpenLishiListAdapter extends BaseRecyclerAdapter<UnOpenLiShiListEntity.RowsBean> {
    private Activity mActivity;

    //手动计算每个child宽度
    private int childTotalWidth;
    //手动计算每个child宽度
    private int childTotalHeight;

    public UnOpenLishiListAdapter(@Nullable List<UnOpenLiShiListEntity.RowsBean> data, Activity mActivity, int childTotalWidth, int childTotalHeight) {
        super(R.layout.item_un_open_lishi_list, data);
        this.mActivity = mActivity;
        this.childTotalWidth = childTotalWidth;
        this.childTotalHeight = childTotalHeight;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, UnOpenLiShiListEntity.RowsBean rowsBean, int position) {

        //设置控件等比宽高
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) baseViewHolder.getView(R.id.lly_get_lishi_bg).getLayoutParams();
        params.width = childTotalWidth;
        params.height = childTotalHeight;


        baseViewHolder.setText(R.id.tv_get_lishi_title_name, AndroidUtils.getLitterRedNameEllipStr(rowsBean.getName()));


        baseViewHolder.setTextColor(R.id.tv_get_lishi_title, Color.parseColor(rowsBean.getBgColor()));
        baseViewHolder.setTextColor(R.id.tv_get_lishi_title_flag, Color.parseColor(rowsBean.getBgColor()));
        TextView mTv_get_lishi_open = baseViewHolder.getView(R.id.tv_get_lishi_open);

        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(GradientDrawable.RECTANGLE);//形状
        gradientDrawable.setCornerRadius(AndroidUtils.dip2px(mContext, 30));//设置圆角Radius
        gradientDrawable.setColor(Color.parseColor(rowsBean.getBgColor()));//颜色
        mTv_get_lishi_open.setBackground(gradientDrawable);//设置为background

        baseViewHolder.addOnClickListener(R.id.tv_get_lishi_open);

        LinearLayout lly_get_lishi_bg = baseViewHolder.getView(R.id.lly_get_lishi_bg);


        lly_get_lishi_bg.setBackgroundResource(R.mipmap.bg_smallcny_styledefauly_bg);
        GlideApp.with(mContext).load(rowsBean.getCoverUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        lly_get_lishi_bg.setBackground(resource);
                    }
                });

        ImageView mIv_getlishi_bottom_bg = baseViewHolder.getView(R.id.iv_getlishi_bottom_bg);
        setBottomWidthHeight(mIv_getlishi_bottom_bg, childTotalWidth);
        GlideApp.with(mContext).load(rowsBean.getBottomImageUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mIv_getlishi_bottom_bg.setImageDrawable(resource);
                    }
                });

        ImageView mIv_getlishi_arrow = baseViewHolder.getView(R.id.iv_getlishi_arrow);
        GlideApp.with(mContext).load(rowsBean.getCenterImageUrl()).skipMemoryCache(true)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mIv_getlishi_arrow.setBackground(resource);
                    }
                });

    }

    /**
     * 设置红包bottomView的宽高
     *
     * @param width
     * @param height
     */
    private void setBottomWidthHeight(ImageView imageView, int width) {
        //设置控件等比宽高
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) imageView.getLayoutParams();
        params.height = width * 111 / 318;
        params.width = width;
        imageView.setLayoutParams(params);
    }
}
