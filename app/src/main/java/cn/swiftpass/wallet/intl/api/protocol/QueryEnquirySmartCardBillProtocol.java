package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author Jamy
 * create date on  on 2018/7/26 10:39
 * 账单查询接口
 * 对应的返回对象为 BillListEntity
 */
public class QueryEnquirySmartCardBillProtocol extends BaseProtocol {
    public static final String TAG = QueryEnquirySmartCardBillProtocol.class.getSimpleName();
    /**
     * 卡id
     */
    String mInnexttstmp;

    public QueryEnquirySmartCardBillProtocol(String innexttstmpIn, NetWorkCallbackListener dataCallback) {
        this.mInnexttstmp = innexttstmpIn;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actionTxnHistEnquirySmartCard";
    }

    @Override
    public void packData() {
        super.packData();
//        mBodyParams.put(RequestParams.INNEXTTSTMP, mInnexttstmp);

    }
}
