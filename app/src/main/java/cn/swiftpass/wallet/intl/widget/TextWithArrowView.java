package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * 左边textview 右边textview 右边imageview布局
 */

public class TextWithArrowView extends RelativeLayout {

    private Context mContext;
    private String mLeftText = "";
    private String mRightText = "";
    private boolean showRightArrow;
    private boolean showLine;

    private TextView mTvLeft;
    private TextView mTvRight;

    public ImageView getImageRight() {
        return imageRight;
    }

    private ImageView imageRight;

    public TextWithArrowView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public TextWithArrowView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public TextWithArrowView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    public void setmLeftText(String text) {
        mTvLeft.setText(text);
    }

    public void setRightText(String text) {
        mTvRight.setText(text);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_left_right_tv_view, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ArrowRightView, defStyle, 0);

        mLeftText = a.getString(R.styleable.ArrowRightView_left_text);
        mRightText = a.getString(R.styleable.ArrowRightView_right_text);
        showRightArrow = a.getBoolean(R.styleable.ArrowRightView_show_right_arrow, true);
        showLine = a.getBoolean(R.styleable.ArrowRightView_show_bottom_line, true);
        int d = a.getResourceId(R.styleable.ArrowRightView_right_image, R.mipmap.icon_button_nextxhdpi);

        mTvLeft = rootView.findViewById(R.id.id_tv_left);
        mTvRight = rootView.findViewById(R.id.id_tv_right);
        View lineView = rootView.findViewById(R.id.id_line);

        imageRight = rootView.findViewById(R.id.id_image_right);
        imageRight.setImageResource(d);

        a.recycle();
        mTvLeft.setText(mLeftText);
        mTvRight.setText(mRightText);
        imageRight.setVisibility(showRightArrow ? VISIBLE : INVISIBLE);
        lineView.setVisibility(showLine ? VISIBLE : INVISIBLE);
    }


    public void setRightImageShow(boolean b) {
        imageRight.setVisibility(b ? VISIBLE : INVISIBLE);
    }
}
