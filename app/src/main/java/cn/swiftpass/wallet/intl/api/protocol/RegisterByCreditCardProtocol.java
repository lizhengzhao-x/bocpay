package cn.swiftpass.wallet.intl.api.protocol;


import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/21 15:44
 * 通过信用卡注册，提交信用卡信息检查
 * 对应的返回对象为 RegisterCreditEntity
 */
public class RegisterByCreditCardProtocol extends BaseProtocol {
    public static final String TAG = RegisterByCreditCardProtocol.class.getSimpleName();

    String mCardNo;
    String mCvv;
    String mExpiryDate;
    /**
     * 标识：B：绑定检查；V：仅验证；F：忘记密码
     */
    String mAction;
    String mContinue;
    String mPhone;

    public RegisterByCreditCardProtocol(String cardNo, String phone, String cvv, String expiryDate, String action, String isContinue, NetWorkCallbackListener dataCallback) {
        this.mCardNo = cardNo;
        this.mPhone = phone;
        this.mCvv = cvv;
        this.mExpiryDate = expiryDate;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        this.mContinue = TextUtils.isEmpty(isContinue) ? "" : isContinue;
        mUrl = "api/register/checkCardInfoCc";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARDNO, mCardNo);
        mBodyParams.put(RequestParams.CVV, mCvv);
        mBodyParams.put(RequestParams.EXPRIYDATE, mExpiryDate);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.IS_CONTINUE, mContinue);
        if (!TextUtils.isEmpty(mPhone)){
            mBodyParams.put(RequestParams.PHONENO, mPhone);
        }
    }
}
