package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;


public class SmartAccountBillListCheckMoreAdapter implements AdapterItem<SmartAccountBillListEntity.OutgroupBean> {

    private TextView transfer_ok;

    private Context mContext;
    private CheckMoreItemCallback checkMoreItemCallback;

    public SmartAccountBillListCheckMoreAdapter(Context mContextIn, CheckMoreItemCallback checkMoreItemCallbackIn) {
        mContext = mContextIn;
        checkMoreItemCallback = checkMoreItemCallbackIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_check_more_view;
    }

    @Override
    public void bindViews(View root) {
        transfer_ok = root.findViewById(R.id.transfer_ok);
    }

    @Override
    public void setViews() {

        transfer_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkMoreItemCallback != null) {
                    checkMoreItemCallback.checkMoreClick();
                }
            }
        });
    }

    @Override
    public void handleData(SmartAccountBillListEntity.OutgroupBean cardItemEntity, int position) {
    }

    public static class CheckMoreItemCallback {


        public void checkMoreClick() {
            // do nothing
        }

    }
}
