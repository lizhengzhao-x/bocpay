package cn.swiftpass.wallet.intl.module;

import android.text.TextUtils;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.GetAppParameter;
import cn.swiftpass.wallet.intl.api.protocol.ApkVerifyWithSHAProtocol;
import cn.swiftpass.wallet.intl.api.protocol.AppResponseForNotifactionProtocol;
import cn.swiftpass.wallet.intl.api.protocol.AppResponseProtocol;
import cn.swiftpass.wallet.intl.api.protocol.AutoLoginAsynProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CardListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsQueryBankRecords;
import cn.swiftpass.wallet.intl.api.protocol.GetAppCallAppLinkProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetBackgroundProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetMenuListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferBaseDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetVitualCarditListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.InitBannerProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.NotificationNewMessageProtocol;
import cn.swiftpass.wallet.intl.api.protocol.NotificationStatusProtocol;
import cn.swiftpass.wallet.intl.api.protocol.StaffRedPacketQueryProtocol;
import cn.swiftpass.wallet.intl.dialog.CustomNotificationDialog;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.AppParameters;
import cn.swiftpass.wallet.intl.entity.BackgroundUrlEntity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.entity.TurnOnRedPackEtntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.ApkVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;
import cn.swiftpass.wallet.intl.utils.GlideImageDownloadManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_NEW_MESSAGE;

/**
 * 主界面管理
 */
public class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter {

    private boolean requestGetBackgroundUrl;


    /**
     * push通知跳转链接获取接口 - 我的优惠券
     */
    @Override
    public void getMyDiscountUrl() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetUplanurlProtocol("{\"action\":\"MERCHANT\"}", new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyDiscountUrlError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyDiscountUrlSuccess(response);
                }
            }
        }).execute();
    }

    /**
     * push通知跳转链接获取接口 - 最新消息
     */
    @Override
    public void getNewMessageUrl() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new NotificationNewMessageProtocol(EWA_NEW_MESSAGE, new NetWorkCallbackListener<NotificationJumpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNewMessageUrlError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NotificationJumpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNewMessageUrlSuccess(response);
                }
            }
        }).execute();

    }

    /**
     * 这里需要在当前页面显示网络请求弹框
     *
     * @param isShowDialog
     */
    @Override
    public void getStaffRedPacket(boolean isShowDialog, int isPush) {
        if (getView() != null && isShowDialog) {
            ApiProtocolImplManager.getInstance().showDialog(MyActivityManager.getInstance().getCurrentActivity());
        }
        new StaffRedPacketQueryProtocol(new NetWorkCallbackListener<TurnOnRedPackEtntity.ReceivedRedPacketDetailBean>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (isShowDialog) {
                        ApiProtocolImplManager.getInstance().dismissDialog();
                    }
                    getView().getStaffRedPacketError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TurnOnRedPackEtntity.ReceivedRedPacketDetailBean response) {
                if (getView() != null) {
                    if (isShowDialog) {
                        ApiProtocolImplManager.getInstance().dismissDialog();
                    }
                    getView().getStaffRedPacketSuccess(response);
                }
            }
        }, isPush).execute();

    }


    @Override
    public void getAppLoginConfigParams(String action) {
        if (getView() != null && !TextUtils.isEmpty(action)) {
            getView().showDialogNotCancel();
        }
        new InitBannerProtocol(new NetWorkCallbackListener<InitBannerEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (!TextUtils.isEmpty(action)) {
                        getView().dismissDialog();
                    }
                    getView().getAppLoginConfigError(errorCode, errorMsg, action);
                }
            }

            @Override
            public void onSuccess(InitBannerEntity response) {
                if (getView() != null) {
                    if (!TextUtils.isEmpty(action)) {
                        getView().dismissDialog();
                    }
                    getView().getAppLoginConfigSuccess(response, action);
                }
            }
        }).execute();
    }

    @Override
    public void checkSmartAccountBind(final MenuItemEntity cls) {
        checkSmartAccountBind(cls, true);
    }

    @Override
    public void checkSmartAccountBind(MenuItemEntity cls, boolean showDialog) {
        if (getView() != null) {
            if (showDialog) {
                getView().showDialogNotCancel();
            }

        }
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (showDialog) {
                        getView().dismissDialog();
                    }

                    getView().checkSmartAccountBindError(errorCode, errorMsg, cls);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    if (showDialog) {
                        getView().dismissDialog();
                    }
                    getView().checkSmartAccountBindSuccess(response, cls);
                }

            }
        }).execute();
    }

    @Override
    public void getAppCallAppLink(String token, String url, String checkSum, String digest) {
        LogUtils.i("MainPresenter", "----->getAppCallAppLink");
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetAppCallAppLinkProtocol(token, checkSum, digest, url, new NetWorkCallbackListener<AppCallAppLinkEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().appCallAppLinkError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AppCallAppLinkEntity response) {
                if (getView() != null) {
                    //这里有一个问题，在华为手机上 透明样式的activity showDialog 偶现不显示 经过测试需要延迟处理，为了防止多次dialog隐藏造成的多次点击问题 dialog对于appcallapp特殊处理
                    if (AppCallAppLinkEntity.TYPE_APPCALLAPP.equals(response.getJumpType())) {
                    } else {
                        getView().dismissDialog();
                    }
                    getView().appCallAppLinkSuccess(response, url);
                }
            }
        }).execute();
    }


    @Override
    public void getMenuList() {
        new GetMenuListProtocol(new NetWorkCallbackListener<MenuListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMenuListFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MenuListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMenuListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getTransferBaseData() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetTransferBaseDataProtocol(new NetWorkCallbackListener<TransferCrossBorderBaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferBaseDataFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderBaseEntity response) {
                if (getView() != null) {

                    getView().getTransferBaseDataSuccess(response);

                    getView().dismissDialog();
                }
            }
        }).execute();

    }

    @Override
    public void getVirtualCardList() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetVitualCarditListProtocol(new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getVirtualCardListError(errorCode, errorMsg);
                    getView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                if (getView() != null) {
                    getView().getVirtualCardListSuccess(response);
                    //屏幕跳转之间 按钮点击问题 后dismissDialog
                    getView().dismissDialog();
                }
            }
        }).execute();
    }

//    @Override
//    public void getOtpCountdownInfo() {
//        new ActivityProtocol("OTP_CONFIG", new NetWorkCallbackListener<OtpCountDownInfoEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//
//            }
//
//            @Override
//            public void onSuccess(OtpCountDownInfoEntity response) {
//                if (getView() != null) {
//                    getView().getOtpCountdownInfoSuccess(response);
//                }
//            }
//        }).execute();
//    }

    @Override
    public void apkVerifyWithSHA(String shaInfo) {
        new ApkVerifyWithSHAProtocol(shaInfo, new NetWorkCallbackListener<ApkVerifyEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().apkVerifyWithSHAError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ApkVerifyEntity response) {
                if (getView() != null) {
                    getView().apkVerifyWithSHASuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void doAutoLogin(boolean iShowLoading) {
        if (getView() != null && iShowLoading) {
            getView().showDialogNotCancel();
        }

        new AutoLoginAsynProtocol(new NetWorkCallbackListener<AutoLoginSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();

                    getView().doAutoLoginError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AutoLoginSucEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().doAutoLoginSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void pwdTipCallServer() {
        new AppResponseProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ContentEntity response) {

            }
        }).execute();
    }

    @Override
    public void notificationTipCallServer() {

        new AppResponseForNotifactionProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ContentEntity response) {

            }
        }).execute();


    }

    @Override
    public void queryBankRecordsByFps() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FpsQueryBankRecords(new NetWorkCallbackListener<FpsBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryBankRecordsByFpsError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(FpsBankRecordsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryBankRecordsByFpsSuccess(response);
                }
            }
        }).execute();
    }

//    @Override
//    public void getAppParameters() {
//        new GetAppParameter(new NetWorkCallbackListener<AppParameters>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView() != null) {
//                    getView().getAppParametersError(errorCode, errorMsg);
//                }
//            }
//
//            @Override
//            public void onSuccess(AppParameters response) {
//                if (getView() != null) {
//                    getView().getAppParametersSuccess(response);
//                }
//            }
//        }).execute();
//    }

    @Override
    public void getUplanUrl() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetUplanurlProtocol(new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUplanUrlError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUplanUrlSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void refreshCardList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CardListProtocol(HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().refreshCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().refreshCardListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartAccountInfo() {
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();
    }


    @Override
    public void getBackgroundUrl(GlideImageDownLoadListener listener) {
        if (requestGetBackgroundUrl) {
            return;
        }
        requestGetBackgroundUrl = true;
        new GetBackgroundProtocol(new NetWorkCallbackListener<BackgroundUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                requestGetBackgroundUrl = false;
            }

            @Override
            public void onSuccess(BackgroundUrlEntity response) {
                requestGetBackgroundUrl = false;
                if (response != null) {
                    //下载通用图片时，不刷新通用图片，下个界面打开时，会发生改变
                    //下载首页图片时，需要相应的更新这个界面图片 页面不可见时，获取的不需要刷新，拉下刷新时，需要立即改变
                    if (!SpUtils.getInstance().getMainBgVersion().equals(response.getPostLoginBgImageVer())) {
                        GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_MAIN_BG, response.getPostLoginBgImageUrl(), response.getPostLoginBgImageVer(), listener);
                    }
                    //下载prelogin界面的图片后，不需要马上改变，等prelogin onstart 自然会发生改变
                    if (!SpUtils.getInstance().getPreLoginBgVersion().equals(response.getPreLoginBgImageVer())) {
                        GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_PRE_LOGIN_BG, response.getPreLoginBgImageUrl(), response.getPreLoginBgImageVer());
                    }
                }

            }
        }).execute();
    }

    @Override
    public void getNotificationStatus(CustomNotificationDialog mNotificationDialog, boolean showDialog, String status, String action) {
        if (getView() != null) {
//            getView().showDialogNotCancel();
        }
        new NotificationStatusProtocol(status, action, new NetWorkCallbackListener<NotificationStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().getNotificationStatusFailed(mNotificationDialog,errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NotificationStatusEntity response) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().getNotificationStatusSuccess(mNotificationDialog,response);
                }
            }
        }).execute();
    }


}
