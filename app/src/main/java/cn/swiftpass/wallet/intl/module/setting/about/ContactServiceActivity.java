package cn.swiftpass.wallet.intl.module.setting.about;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;

/**
 * 联系人
 */
public class ContactServiceActivity extends BaseCompatActivity {
    private String[] perms = {Manifest.permission.CALL_PHONE};
    private final int PERMS_REQUEST_CODE = 200;
    @BindView(R.id.ll_personal_customer_hotline)
    LinearLayout llPersonalCustomerHotline;
    @BindView(R.id.ll_boc_hotline)
    LinearLayout llBocHotline;
    private static final String PHONE_PERSONAL_NUMBER = "+852 3988 2388";
    private static final String PHONE_NUMBER = "+852 2853 8828";
    private boolean isPersonal = true;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.setting_help_tontact);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_contact_service;
    }


    @OnClick({R.id.ll_personal_customer_hotline, R.id.ll_boc_hotline})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_personal_customer_hotline:
                isPersonal = true;
                callPhone(PHONE_PERSONAL_NUMBER);
                break;
            case R.id.ll_boc_hotline:
                isPersonal = false;
                callPhone(PHONE_NUMBER);
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case PERMS_REQUEST_CODE:
                boolean storageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (storageAccepted) {
                    if (isPersonal) {
                        callPhoneNumber(PHONE_PERSONAL_NUMBER);
                    } else {
                        callPhoneNumber(PHONE_NUMBER);
                    }

                } else {
                }
                break;

        }
    }

    public void callPhone(String phoneNum) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            //Android 6.0以上版本需要获取权限
            requestPermissions(perms, PERMS_REQUEST_CODE);//请求权限
        } else {
            callPhoneNumber(phoneNum);
        }
    }

    private void callPhoneNumber(String phoneNum) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + phoneNum);
        intent.setData(data);
        startActivity(intent);
    }
}
