package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardRegisterSuccessEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;

/**
 * 发送验证码
 */
public class RegisterVitualCardSendOtpContract {

    public interface View extends IView {

        void registerVitualCardSenOtpSuccess(VirtualCardSendOtpEntity response, boolean isTryAgain);

        void registerVitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void registerVitualCardVerifyOtpSuccess(VirtualCardRegisterSuccessEntity response);

        void registerVitualCardVerifyOtpError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<RegisterVitualCardSendOtpContract.View> {

        void registerVitualCardSenOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, boolean isTryAgain);

        void registerVitualCardVerifyOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn,String walletId, String verifyCode, String cardId);
    }

}
