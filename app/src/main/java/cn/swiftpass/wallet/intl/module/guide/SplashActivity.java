package cn.swiftpass.wallet.intl.module.guide;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_CREDIT_AWARD_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_MY_DISCOUNT_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_NEW_MESSAGE;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_TO_TAL_GP_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_U_PLAN_HOME_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.MESSAGE_TYPE;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.SEVEN_VERSION_NOTIFICATION_PARAMS;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RedPackageEntity;
import cn.swiftpass.wallet.intl.entity.SevenVersionNotificationEntity;
import cn.swiftpass.wallet.intl.entity.event.UpdateTotalGradeEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.PushNotificationEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.deepLink.DeepLinkActivity;
import cn.swiftpass.wallet.intl.module.deepLink.RedPackageActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.notification.PushNotificationActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BocLaunchSDK;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;


/**
 * @author fan.zhang
 */
public class SplashActivity extends BaseCompatActivity {
    private static final String TAG = "SplashActivity";
    @BindView(R.id.llayout_welcome)
    LinearLayout welcomeLayout;


    @Override
    protected boolean notUsedToolbar() {
        return true;
    }


    @Override
    public void init() {
        //检测手机是否root 如果root 直接弹窗提示 退出
        if (AndroidUtils.isRooted()) {
            showErrorMsgDialog(mContext, getString(R.string.string_root_error), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        } else {
            //1.如果是appLink吊起来的Bocpay,判断是否有没有同意TNC，
            boolean appLinkAction = appLinkAction(getIntent());
            if (!appLinkAction) {
                //2.判断是否是点击通知栏过来
                if (getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(MESSAGE_TYPE))) {
                    goNextPageEvent();
                } else {
                    //3.正常唤醒app
                    if (welcomeLayout != null) {
                        welcomeLayout.startAnimation(getAlphaInAnimation(1000));
                    }
                }

            }
        }
    }


    @Override
    protected boolean useScreenOrientation() {
        return true;
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //闪屏也全屏显示
        LogUtils.i(TAG, "onCreate----->");
//        ToastUtils.getInsctance().show(this, "coming in ..........");
//        printAllInfo(getIntent().getExtras());
        super.onCreate(savedInstanceState);
        setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION);
        //解决按home按键退到后台 然后点击桌面图标app重启问题
        Uri appLinkData = getIntent().getData();
        if (!isTaskRoot() && appLinkData == null) {
            finish();
            return;
        }
    }


    @Override
    protected int getLayoutId() {
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
        }
        return R.layout.act_splash;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    /**
     * 存在一种问题 如果applink解析报错 有种场景导致app未被唤醒到前台
     */
    private void bring2Front() {
        ActivityManager activtyManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfos = activtyManager.getRunningTasks(3);
        for (ActivityManager.RunningTaskInfo runningTaskInfo : runningTaskInfos) {
            if (this.getPackageName().equals(runningTaskInfo.topActivity.getPackageName())) {
                activtyManager.moveTaskToFront(runningTaskInfo.id, ActivityManager.MOVE_TASK_WITH_HOME);
                return;
            }
        }
    }


    private Animation getAlphaInAnimation(int time) {
        Animation ani = new AlphaAnimation(1.0f, 1.0f);
        ani.setDuration(time);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                goNextPageEvent();
            }
        });
        return ani;
    }

    /**
     * 1.点击通知进来，首先判断是否app是第一次打开 还是升级打开，这两种情况都需要到TNC界面，然后再往后透传
     * 2.如果已经打开过TNC,需要判断是否登录态，一级消息类型。 非登录态需要跳转到PreLogin界面，登录态直接到达首页
     */
    private void goNextPageEvent() {


        String messageType = null;
        boolean isRedPackageIntent = false;
        boolean isSevenVersionNotification = false;
        if (getIntent() != null && getIntent().getExtras() != null) {
            messageType = getIntent().getExtras().getString(MESSAGE_TYPE);
        }
        if (!TextUtils.isEmpty(messageType)) {
            RedPackageEntity redPackageEntity = null;
            //普通红包和员工红包都要获取RedPackageEntity 参数
            isRedPackageIntent = TextUtils.equals(messageType, Constants.REDMG_PARAM) || TextUtils.equals(messageType, Constants.REDMG_STAFF_PARAM);
            if (isRedPackageIntent) {
                redPackageEntity = new RedPackageEntity();
                if (null != getIntent().getExtras() && null != getIntent().getExtras().getSerializable(RED_PACKET_RED_PACKET_PARAMS)) {
                    redPackageEntity = (RedPackageEntity) getIntent().getExtras().getSerializable(RED_PACKET_RED_PACKET_PARAMS);
                }
                if (null != getIntent().getExtras() && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.SRCREFNO_PARAM))) {
                    redPackageEntity.setPushRrcRefNo(getIntent().getExtras().getString(Constants.SRCREFNO_PARAM));
                    redPackageEntity.setPushRedPacketType(getIntent().getExtras().getString(Constants.REDPACKETTYPE_PARAM));
                }
            }

            //7月版 push 通知
            SevenVersionNotificationEntity sevenVersionNotificationEntity = null;
            isSevenVersionNotification = TextUtils.equals(messageType, EWA_TO_TAL_GP_PARAM)
                    || TextUtils.equals(messageType, EWA_U_PLAN_HOME_PARAM)
                    || TextUtils.equals(messageType, EWA_NEW_MESSAGE)
                    || TextUtils.equals(messageType, EWA_MY_DISCOUNT_PARAM)
                    || TextUtils.equals(messageType, EWA_CREDIT_AWARD_PARAM);
            if (isSevenVersionNotification) {
                sevenVersionNotificationEntity = new SevenVersionNotificationEntity();
                if (null != getIntent().getExtras() && null != getIntent().getExtras().getSerializable(SEVEN_VERSION_NOTIFICATION_PARAMS)) {
                    sevenVersionNotificationEntity = (SevenVersionNotificationEntity) getIntent().getExtras().getSerializable(SEVEN_VERSION_NOTIFICATION_PARAMS);
                }
            }

            PushNotificationEvent pushNotificationEvent = UserLoginEventManager.getInstance().getEventDetail(PushNotificationEvent.class);
            if (pushNotificationEvent == null) {
                pushNotificationEvent = new PushNotificationEvent();
                UserLoginEventManager.getInstance().addUserLoginEvent(pushNotificationEvent);
            }
            if (isRedPackageIntent) {
                pushNotificationEvent.updateEventParams(RED_PACKET_RED_PACKET_PARAMS, redPackageEntity);
            } else if (isSevenVersionNotification) {
                pushNotificationEvent.updateEventParams(SEVEN_VERSION_NOTIFICATION_PARAMS, sevenVersionNotificationEntity);
            }
            pushNotificationEvent.updateEventParams(MESSAGE_TYPE, messageType);

        }


        HashMap<String, Object> mHashMaps = new HashMap<>();
        if (BocLaunchSDK.checkIsFirstTimeTC(this)) {
            //如果检测到升级 要更新当前升级要做的弹框状态
            BocLaunchSDK.checkIsUpdateToJanuaryVersion(this);
            mHashMaps.put(MainHomeActivity.IS_AUTO_LOGIN, CacheManagerInstance.getInstance().isLogin());
            //按照生产来 只有tnc界面需要动画
            tryToAnimationToNextPage(mHashMaps);
        } else {
            if (!SpUtils.getInstance().HasShowGuidePageStatus()) {
                //升级过来 但是在引导页杀死 需要重新走引导页流程
                HashMap<String, Object> param = new HashMap<>();
                param.put(MainHomeActivity.IS_AUTO_LOGIN, CacheManagerInstance.getInstance().isLogin());
                ActivitySkipUtil.startAnotherActivity(this, GuideNewActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
                return;
            } else {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    //通知登录态单独处理红包单独处理
                    mHashMaps.put(MainHomeActivity.IS_AUTO_LOGIN, !MyActivityManager.containMainHomePage());
                    //需求是 派利是弹框尽量不要退到首页，要在用户当前页面弹框，所以这里不能暴力直接启动MainHomeActivity 因为MainHomeActivity 是 singleTask
                    if (MyActivityManager.containMainHomePage()) {
                        //TODO 这里逻辑要特殊处理
                        if (isRedPackageIntent) {
                            ActivitySkipUtil.startAnotherActivity(this, RedPackageActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        } else if (isSevenVersionNotification) {
                            if (TextUtils.equals(messageType, EWA_U_PLAN_HOME_PARAM)
                                    || TextUtils.equals(messageType, EWA_NEW_MESSAGE)
                                    || TextUtils.equals(messageType, EWA_MY_DISCOUNT_PARAM)) {
                                //UPlan首页、最新消息、我的优惠券 --- 二级界面
                                //二级界面要保证返回之前界面，所以需要一个透明Activity中转一下
                                //透明Activity处理了Push通知后，MainHomeActivity就不需要再处理了，所以要移除该Event
                                UserLoginEventManager.getInstance().removeItemEvent(SevenVersionNotificationEntity.class);
                                mHashMaps.put(MESSAGE_TYPE, messageType);
                                ActivitySkipUtil.startAnotherActivity(this, PushNotificationActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            } else {
                                //账户总积分、信用卡奖赏记录 --- 一级界面
                                //一级界面先跳转至MainHomeActivity再切换Fragment
                                if (TextUtils.equals(messageType, EWA_TO_TAL_GP_PARAM) && MyActivityManager.isAtTotalGradeActivity()) {
                                    //当前正处于 账户总积分Activity，刷新数据即可
                                    EventBus.getDefault().post(new UpdateTotalGradeEntity(UpdateTotalGradeEntity.UPDATE_TOTAL_GRADE, "update total grade"));
                                } else {
                                    //信用卡奖赏记录
                                    ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }
                            }
                        }
                    } else {
                        ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    finish();
                } else {
                    //通知未登录态红包单独处理
                    //未登录 跳转到登录界面
                    boolean isPushNotification = isRedPackageIntent || isSevenVersionNotification;
                    mHashMaps.put(Constants.IS_PUSH_NOTIFICATION, isPushNotification);
                    ActivitySkipUtil.startAnotherActivity(SplashActivity.this, PreLoginActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    finish();
                }
            }


        }
    }


    private void tryToAnimationToNextPage(HashMap<String, Object> mHashMaps) {
        Animation ani = new AlphaAnimation(1.0f, 1.0f);
        ani.setDuration(1000);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                ActivitySkipUtil.startAnotherActivity(SplashActivity.this, TCWebActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
            }
        });
        if (welcomeLayout != null) {
            welcomeLayout.startAnimation(ani);
        } else {
            ActivitySkipUtil.startAnotherActivity(this, TCWebActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }


    /**
     * 是否是appLink吊起我们BocPay
     * <p>
     * 通过连接 跳转到我们app中 授权
     * "https://ewasit3.ftcwifi.com/bocpay?checksum=781cd50cf2941574aed781d9183a83f919e0136b3c303319c905bd857f83b9bfa6bf4188d50db289cdf2b48bbefa55e96faebbec7774ac5e59d5b3890019d36d7930d3367935d7f4c1c97da7b2bfcf1cc2a52b753948cb3bfead888641b39ddc47eac92d5fc901ef898782cb47dff096199373b34cee96f5c0a3ed386bfc5d378d3dd4d2fafcbb8028f8ca3ea9bf692b5e1206dd99e1bce8107bccce0b972930fae561eec96207ab49463097e0db76133f5e70a0ccc8b8885efa849e48c291f6e6eb154a0a745647672d6c569912713e2cf5536d67dfd7ded7cb85eeea08e7b2c6d6d53d98ff530633d76d7b70d3d241d12a76c07495fd8abf9542990f26da5e&digest=c59b85c2b521ae4aa05a8ee24c3bedf685038be888bdd5d44c23a318008196300f7a4e783a429999b1c8bfc8b965017b07335bd1f43260e93485f564b134f17560aa23e70f40259d3ffa4512348d53f5b0ab270a06a7ea9c017b5f83c11e22adca0533b98a3a77eb498099cdeece1f45c8d1bf5c9a10af46db15b57811507f934a21a79ab00724b0df21ad14578c76005a8e114bd68d1e48c22461c2d71545953053de6eec1ac7a0605c8d022b39dc96d7669d1b2d054e5f2c968aded2546df9d0313a5265564c68d66ec20a882385812125049ceae22b4cefa00463817582b05ac68db2901a8053d5d2b5f7cc92b1551167fa304ee11369acc9549e312cb27d&token=69DC91B988C6C916254AB3B3D4589D8C&appName=mbk&jumpType=2&qrCode=000201010211153147980344000203447980344599900015204599953033445802HK5923BOCI - MBD - CUP - MPOS6009Hong Kong626001200000000000000000000005200000000000000000000007089999610163046be1";
     * <p>
     * 目前做法 如果是其他app call 起我们bocpay 这种情况 我们就去掉闪屏动画
     *
     * @param intent
     * @return
     */
    private boolean appLinkAction(Intent intent) {
        Uri appLinkData = intent.getData();
        String actionUrl = null;
        //如果单单是link吊起bocPay 就不需要在SplashActivity页面停留 等待动画
//        boolean isNeedPlayAnimation = false;
        if (appLinkData != null) {
            actionUrl = appLinkData.toString();
            LogUtils.i(TAG, "actionUrl: " + actionUrl);
        }
        //首先判断是否是applink唤醒app,=
        // 1.如果是要判断是否同意过TNC
        // 2.是否显示过引导页，
        // 3.app是否在前台运行
        if (!TextUtils.isEmpty(actionUrl)) {
            AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
            if (appCallAppEvent == null) {
                appCallAppEvent = new AppCallAppEvent();
                UserLoginEventManager.getInstance().addUserLoginEvent(appCallAppEvent);
            }
            //添加appcallapp的事件
            appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, actionUrl);
            return nextEvent(actionUrl);
        } else {
            return false;
        }

//        else {
//            //FPS call 起来 取参数形式有所不同
//            Bundle fpsBundleInfo = intent.getExtras();
//            if (fpsBundleInfo != null) {
//                String fpsUrlInfo = fpsBundleInfo.getString("url”");
////                String callBackurl = fpsBundleInfo.getString("callback","");
//                if (!TextUtils.isEmpty(fpsUrlInfo)) {
//                    AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
//                    if (appCallAppEvent == null) {
//                        appCallAppEvent = new AppCallAppEvent();
//                        UserLoginEventManager.getInstance().addUserLoginEvent(appCallAppEvent);
//                    }
//                    //添加appcallapp的事件
//                    appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, fpsUrlInfo);
////                    appCallAppEvent.updateEventParams(AppCallAppEvent.APP_MERCHANT_CALLBACK_URL, callBackurl);
//                    return nextEvent(fpsUrlInfo);
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        }
    }

    private boolean nextEvent(String actionUrl) {
        Intent intentNext = null;
        //检测是否同意了TNC,必须同意之后才可以往后走
        if (BocLaunchSDK.checkIsFirstTimeTC(this)) {
            //如果检测到升级 要更新当前升级要做的弹框状态
            BocLaunchSDK.checkIsUpdateToJanuaryVersion(this);
            intentNext = new Intent(SplashActivity.this, TCWebActivity.class);
            //没有同意过tnc只有两种情况 第一种第一次安装一定不是登录态 第二种升级安装 登录态依赖本地保存
            intentNext.putExtra(MainHomeActivity.IS_AUTO_LOGIN, CacheManagerInstance.getInstance().isLogin());
            startActivity(intentNext);
            finish();
            return true;
        } else {
            if (!SpUtils.getInstance().HasShowGuidePageStatus()) {
                //升级过来 但是在引导页杀死 需要重新走引导页流程
                HashMap<String, Object> param = new HashMap<>();
                param.put(MainHomeActivity.IS_AUTO_LOGIN, CacheManagerInstance.getInstance().isLogin());
//                    param.put(AppCallAppLinkEntity.APP_LINK_URL, actionUrl);
                ActivitySkipUtil.startAnotherActivity(this, GuideNewActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
                return true;
            } else {
                //已经走过引导页逻辑
                //涉及到applink 有些需要调用接口api会报错 没有走startActivity 导致应用切换不到前台
                bring2Front();
                //之前已经同意了TNC操作 根据登录态应该跳转到PreLogin/首页
                if (CacheManagerInstance.getInstance().isLogin()) {
                    //app运行中(比如app已登陆并且运行中，此时点击第三方浏览器中的deepLink跳转时，执行此处代码)
                    if (MyActivityManager.containMainHomePage()) {
//                            intentNext.putExtra(AppCallAppLinkEntity.APP_LINK_URL, actionUrl);
                        intentNext = new Intent(SplashActivity.this, DeepLinkActivity.class);
                        intentNext.putExtra(AppCallAppLinkEntity.APP_LINK_URL, actionUrl);
                    } else {
                        //app未运行(比如app登录后被杀死，此时点击第三方浏览器中的deepLink跳转时，执行此处代码，需自动登录判断逻辑)
                        intentNext = new Intent(SplashActivity.this, MainHomeActivity.class);
//                            intentNext.putExtra(AppCallAppLinkEntity.APP_LINK_URL, actionUrl);
                        intentNext.putExtra(MainHomeActivity.IS_AUTO_LOGIN, CacheManagerInstance.getInstance().isLogin());
                    }
                    startActivity(intentNext);
                    finish();
                    return true;
                } else {
                    intentNext = new Intent(SplashActivity.this, PreLoginActivity.class);
                    startActivity(intentNext);
                    finish();
                    return true;
                }
            }
        }
    }

}
