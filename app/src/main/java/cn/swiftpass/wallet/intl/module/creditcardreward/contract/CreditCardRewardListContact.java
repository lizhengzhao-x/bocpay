package cn.swiftpass.wallet.intl.module.creditcardreward.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryList;

public class CreditCardRewardListContact {
    public interface View extends IView {
        void getHistoryCreditCardRewardListError(String errorCode, String errorMsg, boolean isRefresh);

        void getHistoryCreditCardRewardListSuccess(CreditCardRewardHistoryList response, boolean isRefresh);
    }

    public interface Presenter extends IPresenter<View> {
        void getRewardHistoryList(String dateKey, String rewardsType, int pageNum, boolean isRefresh);
    }
}
