package cn.swiftpass.wallet.intl.module.cardmanagement.fps;


import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordDisplayBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckRequest;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 同一手机或者邮箱下，FPS绑定了个多个银行,可选择删除
 */

public class FpsBankRecordsActivity extends BaseCompatActivity {

    @BindView(R.id.rv_bank_list)
    RecyclerView mBankListRV;

    @BindView(R.id.tv_account_id)
    TextView mAccountIdTV;

    @BindView(R.id.tv_account_label)
    TextView mAccountLabelTV;

    @BindView(R.id.tv_fps_next)
    TextView mFpsNextTV;

    @BindView(R.id.tv_fps_delete)
    TextView mFpsDeleteTV;

    @BindView(R.id.tv_fps_back)
    TextView mFpsBackTV;

    @BindView(R.id.tv_delete_label)
    TextView mDeleteLabelTV;

    CommonRcvAdapter<FpsOtherBankRecordDisplayBean> mAdapter;


    List<FpsOtherBankRecordDisplayBean> mFpsBankList;
    ArrayList<FpsOtherBankRecordBean> mSelectDeleteList;
    String mFpsAction;
    //添加时的中银银行记录
    FpsBankRecordBean mBankBean;
    FpsPreCheckEntity mFpsPreCheckEntityAdd;
    //所有银行FPS记录
    FpsOtherBankRecordsEntity mBankRecords;

    // 涉及到隐藏资料 添加一个10分钟计时器  10分钟后自动关闭该页面


    boolean mIsCreate;

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_bank_records;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C3_02_1_6);
        mFpsBankList = new ArrayList<>();
        mSelectDeleteList = new ArrayList<>();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 10 * 60 * 1000);
        mAdapter = getAdapter(mFpsBankList);
        mBankListRV.setAdapter(mAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mBankListRV.setLayoutManager(layoutManager);
        parseIntent();
        initData();
        mIsCreate = true;
    }

    private void parseIntent() {
        Intent intent = getIntent();
        if (null != intent) {
            mFpsAction = intent.getStringExtra(FpsConst.FPS_ACTION);
            mBankRecords = (FpsOtherBankRecordsEntity) intent.getSerializableExtra(FpsConst.FPS_OTHER_BANK_RECORDS);
            mBankBean = (FpsBankRecordBean) intent.getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
            if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
                mFpsNextTV.setVisibility(View.VISIBLE);
                mFpsPreCheckEntityAdd = (FpsPreCheckEntity) intent.getSerializableExtra(FpsConst.FPS_PRE_CHECK_ENTITY_ADD);
                setToolBarTitle(R.string.C1_01_1_1);
            } else {
                mFpsDeleteTV.setVisibility(View.VISIBLE);
                setToolBarTitle(R.string.C3_02_1_6);
            }
        }
    }

    private void initData() {
        if (null != mBankRecords) {
            if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mBankRecords.getAccountIdType())) {
                mAccountLabelTV.setText(R.string.C3_01_2_2);
                mAccountIdTV.setText(AndroidUtils.fpsEmailFormat(mBankRecords.getAccountId()));
            } else {
                mAccountLabelTV.setText(R.string.C3_01_2_1);
                mAccountIdTV.setText(AndroidUtils.fpsPhoneFormat(mBankRecords.getAccountId()));
            }
            refreshListView();
        }
    }

    private void refreshListView() {
        if (null != mBankRecords) {
            List<FpsOtherBankRecordBean> bocRecords = mBankRecords.getBankChina();
            mFpsBankList.clear();
            if (null != bocRecords && !bocRecords.isEmpty()) {
                for (FpsOtherBankRecordBean recordBean : bocRecords) {
                    FpsOtherBankRecordDisplayBean displayBean = new FpsOtherBankRecordDisplayBean();
                    displayBean.setRecordBean(recordBean);
                    if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
                        displayBean.setBanClick(true);
                        displayBean.setStatusCheck(true);
                    } else if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_QUERY)) {
                        displayBean.setHideCheck(true);
                    }
                    mFpsBankList.add(displayBean);
                }
            }
            List<FpsOtherBankRecordBean> otherRecords = mBankRecords.getData();
            if (null != otherRecords && !otherRecords.isEmpty()) {
                for (FpsOtherBankRecordBean recordBean : otherRecords) {
                    FpsOtherBankRecordDisplayBean displayBean = new FpsOtherBankRecordDisplayBean();
                    displayBean.setRecordBean(recordBean);
                    displayBean.setHideCheck(false);
                    mFpsBankList.add(displayBean);
                }
                if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_QUERY)) {
                    mFpsDeleteTV.setVisibility(View.VISIBLE);
                }
            } else {
                if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_QUERY)) {
                    mFpsDeleteTV.setVisibility(View.GONE);
                    mFpsBackTV.setVisibility(View.VISIBLE);
                }
            }

            if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
                if (!mFpsBankList.isEmpty()) {
                    mDeleteLabelTV.setVisibility(View.VISIBLE);
                } else {
                    mDeleteLabelTV.setVisibility(View.GONE);
                }
            } else if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_QUERY)) {
                if (null != otherRecords && !otherRecords.isEmpty()) {
                    mDeleteLabelTV.setVisibility(View.VISIBLE);
                } else {
                    mDeleteLabelTV.setVisibility(View.GONE);
                }
            }

            if (!mFpsBankList.isEmpty()) {
                mBankListRV.setVisibility(View.VISIBLE);
                ((IAdapter<FpsOtherBankRecordDisplayBean>) mBankListRV.getAdapter()).setData(mFpsBankList);
                mBankListRV.getAdapter().notifyDataSetChanged();
            } else {
                mBankListRV.setVisibility(View.GONE);
            }
        } else {
            if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_QUERY)) {
                mFpsDeleteTV.setVisibility(View.GONE);
                mFpsBackTV.setVisibility(View.VISIBLE);
            }
            mDeleteLabelTV.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsCreate) {
            queryBankRecords();
        } else {
            mIsCreate = false;
        }
    }

    @OnClick({R.id.tv_fps_next, R.id.tv_fps_delete, R.id.tv_fps_back})
    public void onViewClicked(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.tv_fps_next:
                onNext();
                break;
            case R.id.tv_fps_delete:
                onDelete();
                break;
            case R.id.tv_fps_back:
                finish();
                break;
            default:
                break;
        }
    }

    private void collectDeleteList() {
        mSelectDeleteList.clear();
        for (FpsOtherBankRecordDisplayBean displayBean : mFpsBankList) {
            if (displayBean.isStatusCheck()) {
                mSelectDeleteList.add(displayBean.getRecordBean());
            }
        }
    }

    //添加时下一步
    private void onNext() {
        collectDeleteList();
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            onDeletePre();
        } else {
            onAddContinue(null);
        }
    }

    //查询他行进行删除
    private void onDelete() {
        collectDeleteList();
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            onDeletePre();
        }
    }

    private void onDeletePre() {
        FpsPreCheckRequest request = new FpsPreCheckRequest();
        request.accountId = mBankBean.getAccountId();
        request.accountIdType = mBankBean.getAccountIdType();
        request.accountNo = "";
        request.bankCode = getDeleteBankCodes();
        request.action = FpsConst.FPS_ACTION_DEL;
        request.defaultBank = "";
        String delType = FpsConst.FPS_RECORD_DEL_TYPE_OTHER;
        if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
            delType = FpsConst.FPS_RECORD_DEL_TYPE_ADD;
        }
        ApiProtocolImplManager.getInstance().fpsPreCheckDel(getActivity(), request, delType, new NetWorkCallbackListener<FpsPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
                            FpsManageActivity.startActivity(getActivity());
                        }
                    }
                });
                //AndroidUtils.showTipDialog(getActivity(),errorMsg);
            }

            @Override
            public void onSuccess(final FpsPreCheckEntity response) {
                if (null != response) {
                    if (TextUtils.equals(mFpsAction, FpsConst.FPS_ACTION_ADD)) {
                        onAddContinue(response);
                    } else {
                        onDeleteConfirm(response);
                    }
                }
            }

        });
    }

    private String getDeleteBankCodes() {
        String bankCode = "";
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            bankCode = mSelectDeleteList.get(0).getBankCode();
            if (mSelectDeleteList.size() > 1) {
                for (int i = 1; i < mSelectDeleteList.size(); i++) {
                    bankCode = bankCode + "||" + mSelectDeleteList.get(i).getBankCode();
                }
            }
        }
        return bankCode;
    }

    //添加时检查完删除继续
    private void onAddContinue(FpsPreCheckEntity delEntity) {
        FpsBankRecordsConfirmActivity.startActivity(getActivity(), mBankBean, mFpsPreCheckEntityAdd, delEntity, mSelectDeleteList);
    }

    //查询删除
    private void onDeleteConfirm(FpsPreCheckEntity response) {
        FpsBankRecordsConfirmDelActivity.startActivity(getActivity(), mBankBean, response, mSelectDeleteList);
    }


    private CommonRcvAdapter<FpsOtherBankRecordDisplayBean> getAdapter(List<FpsOtherBankRecordDisplayBean> data) {
        return new CommonRcvAdapter<FpsOtherBankRecordDisplayBean>(data) {
            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new FpsBindBankListAdapter(mContext, new FpsBindBankListAdapter.BankItemCallback() {
                    @Override
                    public void onSelectClick(int position) {
//                        FpsOtherBankRecordDisplayBean displayBean = mFpsBankList.get(position);
//                        if(!mSelectDeleteList.contains(displayBean.getRecordBean())){
//                            mSelectDeleteList.add(displayBean.getRecordBean());
//                        }else{
//                            mSelectDeleteList.remove(displayBean.getRecordBean());
//                        }
                    }
                });
            }
        };
    }

    private void queryBankRecords() {
        NetWorkCallbackListener<FpsOtherBankRecordsEntity> callback = new NetWorkCallbackListener<FpsOtherBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(FpsOtherBankRecordsEntity response) {
                mBankRecords = response;
                refreshListView();
            }
        };
        ApiProtocolImplManager.getInstance().fpsQueryBankOtherRecords(mContext, mBankBean.getAccountId(), mBankBean.getAccountIdType(), callback);
    }


    public static void startActivityNew(Activity activity, FpsPreCheckEntity preCheckEntity, FpsBankRecordBean bean, FpsOtherBankRecordsEntity records) {
        Intent intent = new Intent(activity, FpsBankRecordsActivity.class);
        intent.putExtra(FpsConst.FPS_PRE_CHECK_ENTITY_ADD, preCheckEntity);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
        intent.putExtra(FpsConst.FPS_OTHER_BANK_RECORDS, records);
        intent.putExtra(FpsConst.FPS_ACTION, FpsConst.FPS_ACTION_ADD);
        activity.startActivity(intent);
    }

    public static void startActivityQuery(Activity activity, FpsBankRecordBean bean, FpsOtherBankRecordsEntity records) {
        Intent intent = new Intent(activity, FpsBankRecordsActivity.class);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
        intent.putExtra(FpsConst.FPS_OTHER_BANK_RECORDS, records);
        intent.putExtra(FpsConst.FPS_ACTION, FpsConst.FPS_ACTION_QUERY);
        activity.startActivity(intent);
    }

    //test
    public FpsOtherBankRecordBean getTestRecordBean() {
        FpsOtherBankRecordBean bankRecordBean = new FpsOtherBankRecordBean();
        bankRecordBean.setBankCode("512");
        bankRecordBean.setCreateDate(" 20181201142611");
        bankRecordBean.setDefaultBank("N");
        bankRecordBean.setDisplayEngName("CHAN T** M** P****CHAN T** M** P****2");
        bankRecordBean.setLastUpdateDate("20181201152611");
        return bankRecordBean;
    }
}
