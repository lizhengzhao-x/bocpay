package cn.swiftpass.wallet.intl.module.crossbordertransfer.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.app.ProjectApp.mContext;
import static cn.swiftpass.wallet.intl.entity.Constants.FILENAME;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_FAILED;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_PROCESS;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_SUCCESS;

/**
 * Created by ZhangXinchao on 2019/12/9.
 * 跨境汇款成功界面/交易记录详情界面 分享组件功能一样 代码封装
 */
public class CrossBorderShareImageUtils {

    public static void showLackOfPermissionDialog(final Context mContext) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.SH2_4_1a));
        builder.setMessage(mContext.getString(R.string.SH2_4_1b));

        builder.setPositiveButton(mContext.getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mContext);
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);


        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    public static void saveImageToGalleyByNewUI(final Activity mActivity, ShareImageParams shareImageParam) {
        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
        baseActivity.showDialogNotCancel();
        final View shareView = initShareView(mActivity, shareImageParam);
        NestedScrollView shareBodySl = shareView.findViewById(R.id.sl_share_body);
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

        final ImageView mButtomBannerVivew = shareView.findViewById(R.id.id_buttom_banner_view);
        if (!TextUtils.isEmpty(shareImageParam.buttomShareImageUrl)) {
            mButtomBannerVivew.setVisibility(View.VISIBLE);
            String imgUrl = shareImageParam.buttomShareImageUrl;
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mButtomBannerVivew.getLayoutParams();
            int width = AndroidUtils.getScreenWidth(mActivity) - AndroidUtils.dip2px(mActivity, 15) * 2;
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            mButtomBannerVivew.setLayoutParams(lp);
            //设置图片圆角角度
            RoundedCorners roundedCorners = new RoundedCorners(10);
            //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
            RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
            GlideApp.with(mActivity).load(imgUrl)
                    .apply(options)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            mButtomBannerVivew.setImageDrawable(resource);
                            shareBodySl.measure(measureSpec, measureSpec);

                            //判断图片高度是否比屏幕高度高 如果大约屏幕高度 就取图片高度
                            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
                            getImageToplog(mActivity, shareView, shareViewHeight);
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            mButtomBannerVivew.setVisibility(View.GONE);
                            shareBodySl.measure(measureSpec, measureSpec);

                            //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
                            getImageToplog(mActivity, shareView, shareViewHeight);
                        }
                    });
        } else {
            shareBodySl.measure(measureSpec, measureSpec);

            //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
            getImageToplog(mActivity, shareView, shareViewHeight);
        }
    }


    private static void getImageToplog(final Activity mActivity, final View shareView, int shareViewHeight) {
        GlideApp.with(mActivity).asBitmap().load(getShareImageUrl(mActivity)).override(80, 80)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        shareView.setDrawingCacheEnabled(true);
                        shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                        shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight, mActivity.getResources().getColor(R.color.white));
                        AndroidUtils.saveImageToGalleryByScopedStorage(mActivity, cachebmp);
                        shareView.destroyDrawingCache();
                        Toast.makeText(mActivity, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
                        disMissDialog(mActivity);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        disMissDialog(mActivity);
                    }
                });
    }


    private static void disMissDialog(final Activity mActivity) {
        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
        baseActivity.dismissDialog();
    }


    public static void shareImageToOtherByNewUI(final Activity mActivity, int positon, ShareImageParams shareImageParams) {
        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
        baseActivity.showDialogNotCancel();
        if (positon == 0) {
            //whatsapp
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToWhatsApp(mActivity, "", uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(mActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });

        } else if (positon == 1) {
            //wechat
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareWechatFriend(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, baseActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });
        } else if (positon == 2) {
            //line
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToLine(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });

        } else {
            //系统组件
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    if (uri != null) {
                        AndroidUtils.shareImage(mActivity, "", uri);
                    }
                    disMissDialog(mActivity);
                }
            });
        }
    }


    /**
     * 分享图片 生成分享路径
     *
     * @param onImageBitmapSuccess
     */
    private static void shareGenerateImage(final Activity mActivity, ShareImageParams shareImageParams, final OnImageBitmapSuccess onImageBitmapSuccess) {
        final View shareView = initShareView(mActivity, shareImageParams);
        NestedScrollView shareBodySl = shareView.findViewById(R.id.sl_share_body);
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

        final ImageView mButtomBannerVivew = shareView.findViewById(R.id.id_buttom_banner_view);
        if (!TextUtils.isEmpty(shareImageParams.buttomShareImageUrl)) {
            mButtomBannerVivew.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mButtomBannerVivew.getLayoutParams();
            int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(mActivity, 15) * 2;
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            mButtomBannerVivew.setLayoutParams(lp);
            String imgUrl = shareImageParams.buttomShareImageUrl;
            GlideApp.with(mActivity).load(imgUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    mButtomBannerVivew.setImageDrawable(resource);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);

                    getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    super.onLoadFailed(errorDrawable);
                    mButtomBannerVivew.setVisibility(View.GONE);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);
                    getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
                }
            });
        } else {
            shareBodySl.measure(measureSpec, measureSpec);

            //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);
            getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
        }
    }

    private static void getImageToplogForShare(final Activity mContext, final View shareView, final OnImageBitmapSuccess onImageBitmapSuccess, int shareViewHeight) {
        GlideApp.with(mContext).asBitmap().load(getShareImageUrl(mContext)).override(80, 80).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                shareView.setDrawingCacheEnabled(true);
                shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                // 把一个View转换成图片
                Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight, mContext.getResources().getColor(R.color.white));
                shareView.destroyDrawingCache();
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp,FILENAME);
                onImageBitmapSuccess.onImageBitmapCallBack(uri);
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                shareView.setDrawingCacheEnabled(true);
                shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                // 把一个View转换成图片
                Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight, mContext.getResources().getColor(R.color.white));
                shareView.destroyDrawingCache();
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp,FILENAME);
                onImageBitmapSuccess.onImageBitmapCallBack(uri);
            }
        });

    }


    /**
     * 获取分享的图标 国际化支持
     *
     * @return
     */
    public static String getShareImageUrl(final Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        String shareImageUrl = "en_US/";
        if (AndroidUtils.isZHLanguage(lan)) {
            shareImageUrl = "zh_CN/";
        } else if (AndroidUtils.isHKLanguage(lan)) {
            shareImageUrl = "zh_HK/";
        } else {
            shareImageUrl = "en_US/";
        }


        String url = AppTestManager.getInstance().getBaseUrl() + "images/" + shareImageUrl + "shareImg.png";
        return url;
    }

    private static View initShareView(Context mContext, ShareImageParams shareImageParam) {
        View shareView = LayoutInflater.from(mContext).inflate(R.layout.item_share_view_by_transfer_cross_border_new_ui, null);
        TextView tvPayAmount = shareView.findViewById(R.id.tv_pay_amount);
        TextView tvShareView = shareView.findViewById(R.id.id_share_tv);
        ImageView headImage = shareView.findViewById(R.id.id_head_img);
        TextView tvTransferPayee = shareView.findViewById(R.id.tv_transfer_payee);
        TextView transferFee = shareView.findViewById(R.id.tv_transfer_fee);

        TextView tvTransferCardId = shareView.findViewById(R.id.tv_transfer_card_id);
        TextView tvTransferBank = shareView.findViewById(R.id.tv_transfer_bank);
        TextView tvTransferAccount = shareView.findViewById(R.id.tv_transfer_account);
        TextView tvTransferPayer = shareView.findViewById(R.id.tv_transfer_payer);
        TextView tvTransferStatus = shareView.findViewById(R.id.tv_transfer_status);
        TextView tvTransferType = shareView.findViewById(R.id.tv_transfer_type);
        TextView tvTransferTime = shareView.findViewById(R.id.tv_transfer_time);
        TextView tvTransferNo = shareView.findViewById(R.id.tv_transfer_no);
        //这里网络异常拉取图片有可能出问题 暂时用一个展位图
        GlideApp.with(mContext).load(CrossBorderShareImageUtils.getShareImageUrl(mContext)).placeholder(R.mipmap.shareimg).into(headImage);
        if (shareImageParam != null) {
            if (!TextUtils.isEmpty(shareImageParam.amountCNY)) {
                try {
                    double amountCNY = Double.parseDouble(shareImageParam.amountCNY);
                    String price = AndroidUtils.formatPriceWithPoint(amountCNY, true);
                    tvPayAmount.setText(" " + price);
                } catch (NumberFormatException e) {
                    tvPayAmount.setText(" " + shareImageParam.amountCNY);
                }
            }
            tvTransferPayee.setText(shareImageParam.receiverName);
            tvTransferCardId.setText(shareImageParam.receiverCardNo);
            tvTransferBank.setText(shareImageParam.receiverBankName + " - " + AndroidUtils.getCardType(shareImageParam.receiverCardType, mContext));
            String sb = ("1".equals(shareImageParam.accountType) ? mContext.getString(R.string.LYP01_06_6) : mContext.getString(R.string.LYP01_06_4)) + "(" + shareImageParam.panFour + ")";
            tvTransferAccount.setText(sb);
            tvTransferPayer.setText(shareImageParam.payName);

            if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_SUCCESS)) {
                tvTransferStatus.setText(R.string.CT2_1_5);
            } else if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_FAILED)) {
                //失败时不显示手续费
                tvTransferStatus.setText(R.string.CT2_1_6);
            } else if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_PROCESS)) {
                tvTransferStatus.setText(R.string.CT2_1_7);
            } else {
            }
            if (!TextUtils.isEmpty(shareImageParam.fee)) {
                try {
                    double feeCNY = Double.parseDouble(shareImageParam.fee);
                    String fee = AndroidUtils.formatPriceWithPoint(feeCNY, true);
                    transferFee.setText(mContext.getString(R.string.CT2_1_9a) + " " + fee);
                } catch (NumberFormatException e) {
                    transferFee.setText(mContext.getString(R.string.CT2_1_9a) + " " + shareImageParam.fee);
                }
            }
            tvTransferType.setText(shareImageParam.txnOptionStr);
            tvTransferTime.setText(shareImageParam.txTime);
            tvTransferNo.setText(shareImageParam.referenceNo);
            if (!TextUtils.isEmpty(shareImageParam.buttomShareTv)) {
                tvShareView.setText(shareImageParam.buttomShareTv);
                tvShareView.setVisibility(View.VISIBLE);
            }
        }

        return shareView;
    }


    /**
     * 跨境汇款donepage 模型转换
     *
     * @param statusEntity
     * @return
     */
    public static ShareImageParams generateSuccessShareParams(Context context, TransferCrossBorderStatusEntity statusEntity) {
        ShareImageParams shareImageParams = new ShareImageParams();
        shareImageParams.currencyRMB = statusEntity.currencyRMB;
        shareImageParams.amountCNY = statusEntity.amountCNY;
        shareImageParams.receiverName = statusEntity.receiverName;
        shareImageParams.fee = statusEntity.fee;
        shareImageParams.receiverCardNo = statusEntity.receiverCardNo;
        shareImageParams.receiverBankName = statusEntity.receiverBankName;
        shareImageParams.receiverCardType = statusEntity.receiverCardType;
        shareImageParams.accountType = statusEntity.accountType;
        shareImageParams.panFour = statusEntity.panFour;
        shareImageParams.payName = statusEntity.payName;
        shareImageParams.txTime = statusEntity.txTime;
        shareImageParams.txnId = statusEntity.txnId;
        shareImageParams.status = statusEntity.status;
        shareImageParams.txnOptionStr = statusEntity.txnOptionStr;
        shareImageParams.referenceNo = statusEntity.referenceNo;
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();

        if (initBannerEntity != null) {
            BannerImageEntity pTwoPBean = initBannerEntity.getMoneyTransfer(context);
            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                shareImageParams.buttomShareImageUrl = pTwoPBean.getImagesUrl();
            }
        }

        if (initBannerEntity != null) {
            String txnDetailShareText = initBannerEntity.getCrossBorderShareText(context);
            shareImageParams.buttomShareTv = txnDetailShareText;
        }

        return shareImageParams;
    }


}
