package cn.swiftpass.wallet.intl.module.ecoupon.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;

/**
 * Created by ZhangXinchao on 2019/8/9.
 */
public class ActRedmIdProtocol extends BaseProtocol {

    String giftTp;
    EcouponConvertEntity ecouponConvertEntity;

    public ActRedmIdProtocol(String giftTp, EcouponConvertEntity ecouponConvertEntityIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/actRedmId";
        this.giftTp = giftTp;
        this.ecouponConvertEntity = ecouponConvertEntityIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.GIFTTP, giftTp);
        mBodyParams.put(RequestParams.REDEEMITEMS, ecouponConvertEntity.getRedeemItems());
        mBodyParams.put(RequestParams.REDEEMPOINT, ecouponConvertEntity.getRedeemPoint());
        mBodyParams.put(RequestParams.PAN_ID, ecouponConvertEntity.getPan());
    }

}
