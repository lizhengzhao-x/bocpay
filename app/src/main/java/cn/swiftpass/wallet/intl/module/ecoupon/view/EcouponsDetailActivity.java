package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/8/11.
 * 电子券详情
 */
public class EcouponsDetailActivity extends BaseCompatActivity {
    @BindView(R.id.id_ecoupon_name)
    TextView idEcouponName;
    @BindView(R.id.id_terms_condition)
    TextView id_terms_condition;
    @BindView(R.id.id_ecoupon_date)
    TextView id_ecoupon_date;
    @BindView(R.id.tv_ecoupon_date_tip)
    TextView ecouponDateTipTv;
    @BindView(R.id.tv_use)
    TextView tvUse;
    @BindView(R.id.id_top_image)
    ImageView idTopImage;
    private MyEVoucherEntity.EvoucherItem evoucherItem;
    private String titleDetail;

    @Override
    public void init() {
        EventBus.getDefault().register(this);
        if (getIntent() != null && getIntent().getExtras() != null) {
            evoucherItem = (MyEVoucherEntity.EvoucherItem) getIntent().getExtras().getSerializable(Constants.ECOUPONCONVERTENTITY);
        }
        titleDetail = getIntent().getExtras().getString(Constants.ECOUPON_TITLE);
        setToolBarTitle(evoucherItem.getItemName());
        id_ecoupon_date.setText(evoucherItem.getExpireDate());
        if (evoucherItem.isRed()) {
            id_ecoupon_date.setTextColor(Color.RED);
            ecouponDateTipTv.setTextColor(Color.RED);
        }
        setToolBarTitle(evoucherItem.getItemName());
        idEcouponName.setText(evoucherItem.getItemName() + getString(R.string.EC05c_1_1_a) + evoucherItem.getEVoucherInfos().size() + getString(R.string.EC05c_1));
        RoundedCorners roundedCorners = new RoundedCorners(20);
        //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + evoucherItem.getCouponImgLarge())
                .apply(options).into(idTopImage);
        tvUse.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                verifyPwdAction();
            }
        });
        initTextMsg();
    }

    private void initTextMsg() {
        String totalStr = getResources().getString(R.string.EC11_4);
        final String items[] = totalStr.split("##");
        int firstSart = totalStr.indexOf(items[1]) - 2;
        int firstEnd = firstSart + items[1].length();
        SpannableString spannableString = new SpannableString(totalStr.replace("##", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                showEcouponCountDetail();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, firstSart, firstEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), firstSart, firstEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        id_terms_condition.setText(spannableString);
        id_terms_condition.setMovementMethod(LinkMovementMethod.getInstance());
    }


    private void showEcouponCountDetail() {
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), evoucherItem.getItemName(), titleDetail);
        checkEcouponsDetailPop.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_SHOW_HOMEPAGE) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UplanEntity event) {
        if (event.getEventType() == UplanEntity.EVENT_UPLAN_BACK_SCAN) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void useEcouponsEvent() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.BU_NUMBER, evoucherItem.getBU());
        mHashMaps.put(Constants.CURRAMOUNT, evoucherItem.getCurrAmount());
        ActivitySkipUtil.startAnotherActivity(getActivity(), EcouponsUseActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForActionResult(getActivity(), "E", new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                useEcouponsEvent();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoupon_detail;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

}
