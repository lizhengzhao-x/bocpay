package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;


/**
 * 主扫支付下单使用积分接口
 */
public class MainScanOrderWithPointProtocol extends BaseProtocol {
    public static final String TAG = MainScanOrderWithPointProtocol.class.getSimpleName();

    MainScanOrderRequestEntity mRequestEntity;
    String action;

    public MainScanOrderWithPointProtocol(MainScanOrderRequestEntity requestEntity, NetWorkCallbackListener dataCallback) {
        this.mRequestEntity = requestEntity;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actGpPayment";
    }



    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.QRCTYPE, mRequestEntity.getQrcType());
        mBodyParams.put(RequestParams.CARDID, mRequestEntity.getCardId());
        mBodyParams.put(RequestParams.TRXAMOUNT, mRequestEntity.getTrxAmt());
        mBodyParams.put(RequestParams.TRXFEEAMOUNT, mRequestEntity.getTrxFeeAmt());
        mBodyParams.put(RequestParams.MPQRCODE, mRequestEntity.getMpQrCode());
        mBodyParams.put(RequestParams.MERCHANTNAME, mRequestEntity.getMerchantName());
        mBodyParams.put(RequestParams.COUPINFO, mRequestEntity.getCouponInfo());
        mBodyParams.put(RequestParams.MOBILE, mRequestEntity.getMobile());
        mBodyParams.put(RequestParams.TXNID, mRequestEntity.getTxnId());
        mBodyParams.put(RequestParams.STORELABEL, mRequestEntity.getStoreLabel());
        mBodyParams.put(RequestParams.LOYALTYNO, mRequestEntity.getLoyaltyNo());
        mBodyParams.put(RequestParams.REFLABEL, mRequestEntity.getRefLabel());
        mBodyParams.put(RequestParams.CUSTOMERLABEL, mRequestEntity.getCustomerLable());
        mBodyParams.put(RequestParams.TERMINALABEL, mRequestEntity.getTerminalLabel());
        mBodyParams.put(RequestParams.TRXPURPOSE, mRequestEntity.getTrxPurpose());
        mBodyParams.put(RequestParams.CONSUMEREMAIL, mRequestEntity.getConsumerEmail());
        mBodyParams.put(RequestParams.CONSUMERADDRESS, mRequestEntity.getConsumerAddress());
        mBodyParams.put(RequestParams.CONSUMERMOBILE, mRequestEntity.getConsumerMobile());
        mBodyParams.put(RequestParams.TXNCURR, mRequestEntity.getTxnCurr());

        mBodyParams.put(RequestParams.REDEEMGPAMT, mRequestEntity.getRedeemGpAmt());
        mBodyParams.put(RequestParams.AVAGPCOUNT, mRequestEntity.getAvaGpCount());
        mBodyParams.put(RequestParams.REDEEMGPCOUNT, mRequestEntity.getRedeemGpCount());

        mBodyParams.put(RequestParams.GPREQUIRED, mRequestEntity.getGpRequired());
        mBodyParams.put(RequestParams.REDEEMFLAG, mRequestEntity.getRedeemFlag());
        mBodyParams.put(RequestParams.GPCODE, mRequestEntity.getGpCode());
        mBodyParams.put(RequestParams.YEARHOLDING, mRequestEntity.getYearHolding());
        mBodyParams.put(RequestParams.PAYAMT, mRequestEntity.getPayAmt());
        if (!TextUtils.isEmpty(action)){
            mBodyParams.put(RequestParams.ACTION, "EXTPAY");
        }


    }
}
