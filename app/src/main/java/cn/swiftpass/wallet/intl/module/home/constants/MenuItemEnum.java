package cn.swiftpass.wallet.intl.module.home.constants;

import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.activitys.NewMsgActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.NewCardManagerFragment;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.CreditCardRewardFragment;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.TransferCrossBorderPayeeFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.view.ECouponsManagerFragment;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.redpacket.view.RedPacketMainFragment;
import cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterSelectListFragment;
import cn.swiftpass.wallet.intl.module.scan.backscan.BackScanFragment;
import cn.swiftpass.wallet.intl.module.scan.majorscan.view.MajorScanFragment;
import cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteShareActivity;
import cn.swiftpass.wallet.intl.module.staging.view.StagingItemMonthFragment;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.TransactionManagementFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.CrossBorderPaymentFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.FPSPaymentScanFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferByBankAccountFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferByFpsIDFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferByStaticCodeFragment;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferSelTypeFragment;
import cn.swiftpass.wallet.intl.utils.encry.InitJsonContants;

/**
 * @author fan.zhang
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.home.constants
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 17:46
 * @change
 * @chang time
 * @class describe
 */
public enum MenuItemEnum {

    HOME(MenuKey.HOME, R.string.M10_1_1, R.mipmap.icon_sidemenu_home, R.mipmap.icon_sidemenu_home, MainHomeActivity.class),
    //我的账户
    MY_ACCOUNT(MenuKey.MY_ACCOUNT, R.string.M10_1_2, R.mipmap.icon_sidemenu_myacc, R.mipmap.icon_sidemenu_myacc_on, NewCardManagerFragment.class),
    //我的账户概览
    MY_ACCOUNT_GUIDE(MenuKey.MY_ACCOUNT_GUIDE, R.string.ME2101_1_3, R.mipmap.icon_sidemenu_myacc, R.mipmap.icon_sidemenu_myacc_on, NewCardManagerFragment.class),
    //虚拟卡
    VIRTUAL_CARD(MenuKey.VIRTUAL_CARD, R.string.M10_1_3, R.mipmap.icon_sidemenu_virtualcard, R.mipmap.icon_sidemenu_virtualcard_on, MainHomeActivity.class),
    //收款或者付款
    PAYMENT_OR_COLLECT(MenuKey.PAYMENT_OR_COLLECT, R.string.M10_1_6, R.mipmap.icon_sidemenu_collect, R.mipmap.icon_sidemenu_collect_on, MainHomeActivity.class),
    //跨境
    CROSS_BORDER(MenuKey.CROSS_BORDER, R.string.M10_1_14, R.mipmap.icon_sidemenu_crossborder, R.mipmap.icon_sidemenu_crossborder_on, MainHomeActivity.class),
    //著数优惠
    PREFERENCES_LIST(MenuKey.PREFERENCES_LIST, R.string.M10_1_17, R.mipmap.icon_sidemenu_hotoffers_promotion, R.mipmap.icon_sidemenu_hotoffers_promotion_on, MainHomeActivity.class),
    //虚拟卡申请
    VIRTUAL_CARD_APPLY(MenuKey.VIRTUAL_CARD_APPLY, R.string.M10_1_4, R.mipmap.icon_list_virtualcard, R.mipmap.icon_list_virtualcard, MainHomeActivity.class),
    //虚拟卡确认
    VIRTUAL_CARD_CONFIRM(MenuKey.VIRTUAL_CARD_CONFIRM, R.string.M10_1_5, R.mipmap.icon_list_virtualcard, R.mipmap.icon_list_virtualcard, MainHomeActivity.class),
    //主扫
    MAJOR_SCAN_QR_CODE(MenuKey.MAJOR_SCAN_QR_CODE, R.string.M10_1_8, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, MajorScanFragment.class),
    //被扫
    BACK_SCAN_QR_CODE(MenuKey.BACK_SCAN_QR_CODE, R.string.M10_1_7, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, BackScanFragment.class),
    //转账方式选择列表
    TRANSFER_SELECT_LIST(MenuKey.TRANSFER_SELECT_LIST, R.string.M10_1_9, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferSelTypeFragment.class),
    //派利是
    RED_POCKET(MenuKey.RED_POCKET, R.string.M10_1_12, R.mipmap.icon_list_redpocket, R.mipmap.icon_list_redpocket, RedPacketMainFragment.class),
    //派利是 需要进入拆利是页面
    RED_POCKET_GET_LISHI(MenuKey.RED_POCKET_GET_LISHI, R.string.TF2101_3_9, R.mipmap.icon_list_redpocket, R.mipmap.icon_list_redpocket, RedPacketMainFragment.class),
    //新年派利是
    NEW_YEAR_RED_POCKET(MenuKey.NEW_YEAR_RED_POCKET, R.string.TF2101_3_9, R.mipmap.icon_list_redpocket, R.mipmap.icon_list_redpocket, RedPacketMainFragment.class),
    //FPS转账
    TRANSFER_BY_FPS(MenuKey.TRANSFER_BY_FPS, R.string.M10_1_10, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferByFpsIDFragment.class),
    //银行转账
    TRANSFER_BY_BANK(MenuKey.TRANSFER_BY_BANK, R.string.M10_1_11, R.mipmap.icon_list_transfer_setting_default, R.mipmap.icon_list_transfer_setting_default, TransferByBankAccountFragment.class),
    //静态码收款
    TRANSFER_BY_STATIC_CODE(MenuKey.TRANSFER_BY_STATIC_CODE, R.string.M10_1_13, R.mipmap.icon_sidemenu_collect, R.mipmap.icon_sidemenu_collect_on, TransferByStaticCodeFragment.class),
    //FPS 缴费
    PAYMENT_BY_FPS(MenuKey.PAYMENT_BY_FPS, R.string.M10_1_13, R.mipmap.icon_sidemenu_collect, R.mipmap.icon_sidemenu_collect_on, FPSPaymentScanFragment.class),

    //跨境转账
    TRANSFER_CROSS_BORDER(MenuKey.TRANSFER_CROSS_BORDER, R.string.M10_1_15, R.mipmap.icon_list_crossboarder_transfer, R.mipmap.icon_list_crossboarder_transfer, TransferCrossBorderPayeeFragment.class),
    //跨境收款
    PAYMENT_CROSS_BORDER(MenuKey.PAYMENT_CROSS_BORDER, R.string.M10_1_16, R.mipmap.icon_list_crossboarder_payment, R.mipmap.icon_list_crossboarder_payment, CrossBorderPaymentFragment.class),
    //最新消息（二级界面）
    NEW_MESSAGE(MenuKey.NEW_MESSAGE, R.string.ME2101_1_18, R.mipmap.icon_list_crossboarder_payment, R.mipmap.icon_list_crossboarder_payment, NewMsgActivity.class, false),
    //电子券
    ECOUPON(MenuKey.ECOUPON, R.string.M10_1_19, R.mipmap.icon_sidemenu_evoucher, R.mipmap.icon_sidemenu_evoucher_on, ECouponsManagerFragment.class),
    //UPLAN
    UPLAN(MenuKey.UPLAN, R.string.H2107_1_2, R.mipmap.icon_sidemenu_uplan_2, R.mipmap.icon_sidemenu_uplan_on, UplanActivity.class, false),
    //信用卡奖赏登记
    REWARD_REGISTER(MenuKey.REWARD_REGISTER, R.string.M10_1_21, R.mipmap.icon_sidemenu_creditcardpromotion, R.mipmap.icon_sidemenu_creditcardpromotion, RewardRegisterSelectListFragment.class),
    //信用卡奖赏
    CREDIT_CARD_REWARD(MenuKey.CREDIT_CARD_REWARD, R.string.CRQ2107_1_1, R.mipmap.icon_sidemenu_creditcardpromotion, R.mipmap.icon_sidemenu_creditcardpromotion, CreditCardRewardFragment.class),
    //推荐亲友（二级界面）
    INVITE_SHARE(MenuKey.INVITE_SHARE, R.string.M10_1_22, R.mipmap.icon_list_transfer_limit_setting, R.mipmap.icon_list_transfer_limit_setting, InviteShareActivity.class, false),

    //交易记录
    TRANSACTION_MANAGEMENT(MenuKey.TRANSACTION_MANAGEMENT, R.string.M10_1_23, R.mipmap.icon_sidemenu_transactions, R.mipmap.icon_sidemenu_transactions, TransactionManagementFragment.class),

    //月结单分期
    MONTH_STAGING(MenuKey.MONTH_STAGING, R.string.MB2103_1_1, R.mipmap.icon_sidemenu_transactions, R.mipmap.icon_sidemenu_transactions, StagingItemMonthFragment.class),

    //现金分期
    CASH_STAGING(MenuKey.CASH_STAGING, R.string.MB2103_1_2, R.mipmap.icon_sidemenu_transactions, R.mipmap.icon_sidemenu_transactions, StagingItemMonthFragment.class);


    public final String action;
    public final int name;
    public int flag;
    public int flagSelect;
    public boolean levelFirst;
    private final Class<?> cls;

    MenuItemEnum(String action, int name, int flag, int flagSelect, Class<?> cls) {
        this(action, name, flag, flagSelect, cls, true);
    }

    MenuItemEnum(String action, int name, int flag, int flagSelect, Class<?> cls, boolean levelFirst) {
        this.action = action;
        this.name = name;
        this.flag = flag;
        this.flagSelect = flagSelect;
        this.cls = cls;
        this.levelFirst = levelFirst;
    }


    public static ArrayList<MenuItemEntity> toList(Context context) {
        ArrayList<MenuItemEntity> list = new ArrayList<>();
        for (MenuItemEnum menuItem : MenuItemEnum.values()) {
            list.add(new MenuItemEntity(menuItem.action,
                    context.getString(menuItem.name),
                    menuItem.flag, menuItem.flagSelect, menuItem.cls, menuItem.levelFirst));
        }
        return list;
    }

    public static MenuItemEntity getMenuItmByEnum(Context context, MenuItemEnum itemEnum) {
        return new MenuItemEntity(itemEnum.action,
                context.getString(itemEnum.name),
                itemEnum.flag, itemEnum.flagSelect, itemEnum.cls, itemEnum.levelFirst);
    }

    public static ArrayList<MenuItemEntity> toDefaultList(Context context) {
        if (TempSaveHelper.getMenuListData() == null || TempSaveHelper.getMenuListData().size() == 0) {
            Gson gson = new Gson();
            MenuListEntity menuListEntity = gson.fromJson(InitJsonContants.menuJson, MenuListEntity.class);
            ArrayList<MenuItemEntity> cache = new ArrayList<>();
            ArrayList<MenuItemEntity> menuItemList = MenuItemEnum.toList(context);
            ArrayList<MenuItemEntity> menuList = menuListEntity.getMenuData();
            if (menuList != null && menuList.size() > 0) {
                MenuItemEnum.mapMenuToList(cache, menuList, menuItemList);
            }
            return cache;
        } else {

            ArrayList<MenuItemEntity> cache = new ArrayList<>();
            ArrayList<MenuItemEntity> menuItemList = MenuItemEnum.toList(context);
            ArrayList<MenuItemEntity> menuList = TempSaveHelper.getMenuListData();
            if (menuList != null && menuList.size() > 0) {
                MenuItemEnum.mapMenuToList(cache, menuList, menuItemList);
            }
            return cache;
        }


    }


    public static void mapMenuToList(ArrayList<MenuItemEntity> cache, ArrayList<MenuItemEntity> menuList, ArrayList<MenuItemEntity> menuItemList) {
        for (MenuItemEntity itemEntity : menuList) {
            //循环获得一级菜单
            if (menuItemList.contains(itemEntity)) {
                int index = menuItemList.indexOf(itemEntity);
                MenuItemEntity item = menuItemList.get(index);
                if (itemEntity.childMenus != null && itemEntity.childMenus.size() > 0) {
                    item.childMenus.clear();
                    //循环获得二级菜单
                    for (MenuItemEntity childMenuItemEntity : itemEntity.childMenus) {
                        if (menuItemList.contains(childMenuItemEntity)) {
                            int indexChild = menuItemList.indexOf(childMenuItemEntity);
                            MenuItemEntity itemChild = menuItemList.get(indexChild);
                            itemChild.setChildItem(true);
                            item.childMenus.add(itemChild);
                        }

                    }
                }
                cache.add(item);
            }
        }
    }

}
