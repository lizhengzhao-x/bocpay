package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.FioRegResponseEntity;


public interface FIOVerifyRegisterView<V extends IPresenter> {
    void onFioVerifyRegSuccess(FioRegResponseEntity response);

    void onFioVerifyRegFail(String errorCode, String errorMsg);
}
