package cn.swiftpass.wallet.intl.base.otp;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
//import cn.swiftpass.httpcore.constant.ContentEntity;;


/**
 * @name cn.swiftpass.bocbill.model.base.otp
 * @class name：OTPVerifyView
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/12 14:17
 * @change
 * @chang time
 * @class describe
 */
public interface OTPVerifyView<V extends IPresenter> extends IView {
    void verifyOTPFailed(String errorCode, String errorMsg);

    void verifyOTPSuccess(ContentEntity response);
}
