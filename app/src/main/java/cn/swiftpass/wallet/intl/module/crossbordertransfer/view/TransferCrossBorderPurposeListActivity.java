package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Intent;
import android.text.TextUtils;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderUsedEntity;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_PURPOSE;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_PURPOSE_CODE;

public class TransferCrossBorderPurposeListActivity extends BaseCompatActivity implements PurposeListAdapter.OnItemClickListener {


    @BindView(R.id.ry_purpose_list)
    RecyclerView ryPurposeList;
    private ArrayList<TransferCrossBorderUsedEntity> forUsed;

    @Override
    public void init() {
        setToolBarTitle(R.string.CR2101_14_1);
        if (getIntent() != null) {
            TransferCrossBorderBaseEntity baseEntity = (TransferCrossBorderBaseEntity) getIntent().getSerializableExtra(TransferCrossBorderConst.DATA_TRANSFER_BASE);
            String purpose = getIntent().getStringExtra(TransferCrossBorderConst.DATA_TRANSFER_PURPOSE);
            if (baseEntity != null) {
                forUsed = baseEntity.forUsed;
                ryPurposeList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                PurposeListAdapter adapter = new PurposeListAdapter(this, forUsed, this);
                if (!TextUtils.isEmpty(purpose)) adapter.setSelect(purpose);
                ryPurposeList.setAdapter(adapter);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_cross_border_purpose_list;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    public void onItemClick(int position, String select, String code) {
        Intent data = new Intent();
        data.putExtra(DATA_TRANSFER_PURPOSE, select);
        data.putExtra(DATA_TRANSFER_PURPOSE_CODE, code);
        setResult(RESULT_OK, data);
        finish();
    }
}
