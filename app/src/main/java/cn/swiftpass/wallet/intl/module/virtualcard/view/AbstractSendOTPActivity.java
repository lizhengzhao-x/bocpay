package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.manage.OtpCountDownManager;
import cn.swiftpass.wallet.intl.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;


public abstract class AbstractSendOTPActivity extends BaseCompatActivity {

    @BindView(R.id.tv_smsCode)
    protected TextView tvSmsCode;
    @BindView(R.id.etwd_otp_code)
    protected EditTextWithDel etwdOtpCode;
    @BindView(R.id.id_send_msg)
    protected TextView tvSendMsg;
    @BindView(R.id.id_sub_title)
    protected TextView idSubTitle;
    @BindView(R.id.tv_one_time_title)
    protected TextView mOneTimeTitle;
    protected String phoneNo, mSendAgainTip;

    /**
     * 用于暂时解决 loading操作退到后台 startactiviy无效问题 借助activity onRestart
     */
    protected HashMap<String, Object> mHashMaps;

    private final int MAX_OTP_CODE_LENGTH = 8;

    protected void setResetTime(boolean isReset, String phoneNo) {
        if (isReset) {
            tvSendMsg.setClickable(false);
            tvSendMsg.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setText(String.format(getString(R.string.register_send_msg_time), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                    tvSendMsg.setClickable(false);
                    tvSendMsg.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(mSendAgainTip);
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setText(String.format(getString(R.string.register_send_msg_time), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(mSendAgainTip);
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        }
    }


    @Override
    public void init() {
        tvSmsCode.setText(AndroidUtils.formatCenterPhoneNumberStr(phoneNo));
        tvSendMsg.setVisibility(View.INVISIBLE);
        mSendAgainTip = getString(R.string.IDV_3a_20_5);
        etwdOtpCode.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etwdOtpCode.getEditText().getText().toString().trim().length() == MAX_OTP_CODE_LENGTH) {
                    //验证码够长度之后再接口未调用成功之后
                    etwdOtpCode.getmDelImageView().setEnabled(false);
                    //防止按钮多次点击吊起接口之间back按键不能响应事件
                    setBackBtnEnable(false);

                    verifyOtpAction();
                    if (mHandler != null) {
                        //延迟响应 防止多次点击多个事件触发
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                //做一个延迟响应的事情
                                setBackBtnEnable(true);
                                if (etwdOtpCode == null || etwdOtpCode.getmDelImageView() == null)
                                    return;
                                etwdOtpCode.getmDelImageView().setEnabled(true);
                            }
                        });
                    }
                } else {
                    setBackBtnEnable(true);
                    etwdOtpCode.getmDelImageView().setEnabled(true);
                }
            }
        });
        tvSendMsg.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                sendOtpAction(true);

            }
        });

        etwdOtpCode.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (etwdOtpCode.getEditText().getText().toString().trim().length() == MAX_OTP_CODE_LENGTH) {
                        verifyOtpAction();
                    }
                    return true;
                }
                return false;
            }
        });
        etwdOtpCode.getEditText().setGravity(Gravity.CENTER);
        sendOtpAction(false);
    }


    /**
     * 验证码验证
     */
    protected abstract void verifyOtpAction();


    /**
     * 验证码重发
     *
     * @param isTryAgain
     */
    protected abstract void sendOtpAction(boolean isTryAgain);


    public void showRetrySendOtp() {
        tvSendMsg.setClickable(true);
        tvSendMsg.setEnabled(true);
        tvSendMsg.setText(mSendAgainTip);
        tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
        tvSendMsg.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
    }


    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        if (mHandler == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (etwdOtpCode == null) return;
                etwdOtpCode.getEditText().setFocusable(true);
                etwdOtpCode.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etwdOtpCode.getEditText());
            }
        }, 1000);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_input_otp;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void clearOtpInput() {
        etwdOtpCode.getEditText().setText("");
        showKeyBoard();
    }

    public String getVerifyCode() {
        return etwdOtpCode.getEditText().getText().toString().trim();
    }


}
