package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertRecordEntity;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

/**
 * 电子券使用记录
 */
public class EcoupUseRecordAdapter extends BaseRecyclerAdapter<EcouponConvertRecordEntity.RowsBean> {

    public EcoupUseRecordAdapter(@Nullable List<EcouponConvertRecordEntity.RowsBean> data) {
        super(R.layout.item_ecoupuse_record, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, EcouponConvertRecordEntity.RowsBean rowsBean, int position) {


        baseViewHolder.setText(R.id.id_msg_name, rowsBean.getItemName());
        baseViewHolder.setText(R.id.id_msg_date, mContext.getString(R.string.EC04_1_a) + rowsBean.getExpireDate());


        TextView mId_ecoupon_time = baseViewHolder.getView(R.id.id_msg_time);
        if (!TextUtils.isEmpty(rowsBean.getConsumeDate())) {
            mId_ecoupon_time.setText(mContext.getString(R.string.EC16_5_a) + rowsBean.getConsumeDate());
        } else {
            mId_ecoupon_time.setVisibility(View.GONE);
            mId_ecoupon_time.setText("");
        }

        TextView mId_ecoupon_no = baseViewHolder.getView(R.id.id_msg_no);

        if (!TextUtils.isEmpty(rowsBean.getReferenceNo())) {
            mId_ecoupon_no.setText(mContext.getString(R.string.EC15_7_a) + rowsBean.getReferenceNo());
        }
        TextView mId_ecoupon_status = baseViewHolder.getView(R.id.id_ecoupon_status);
        //状态 35：已使用  36：已过期  33：已失效
        // 32：未使用，领取完成就是这个状态。 剩下的是其他
        mId_ecoupon_status.setVisibility(View.VISIBLE);
        if (rowsBean.getStatus().equals("35")) {
            mId_ecoupon_status.setText(mContext.getString(R.string.EC16_2));
            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_used);
            mId_ecoupon_time.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(rowsBean.getConsumeDate())) {
                mId_ecoupon_time.setVisibility(View.GONE);
            } else {
                mId_ecoupon_time.setVisibility(View.VISIBLE);
            }
        } else if (rowsBean.getStatus().equals("36")) {
            mId_ecoupon_status.setText(mContext.getString(R.string.EC16_3));
            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_outdated);
            if (TextUtils.isEmpty(rowsBean.getConsumeDate())) {
                mId_ecoupon_time.setVisibility(View.GONE);
            } else {
                mId_ecoupon_time.setVisibility(View.VISIBLE);
            }
        } else if (rowsBean.getStatus().equals("33")) {
            mId_ecoupon_status.setText(mContext.getString(R.string.EC16_4));
            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_invalid);
            if (TextUtils.isEmpty(rowsBean.getConsumeDate())) {
                mId_ecoupon_time.setVisibility(View.GONE);
            } else {
                mId_ecoupon_time.setVisibility(View.VISIBLE);
            }
        } else if (rowsBean.getStatus().equals("32")) {
            mId_ecoupon_status.setVisibility(View.INVISIBLE);
            if (TextUtils.isEmpty(rowsBean.getConsumeDate())) {
                mId_ecoupon_time.setVisibility(View.GONE);
            } else {
                mId_ecoupon_time.setVisibility(View.VISIBLE);
            }
        }
        ImageView mId_right_message = baseViewHolder.getView(R.id.id_right_message);
        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + rowsBean.getMerchantIconUrl()).into(mId_right_message);
    }
}
//implements AdapterItem<EcouponConvertRecordEntity.RowsBean> {
//    private Context context;
//
//    public EcoupUseRecordAdapter(Context context) {
//        this.context = context;
//    }
//
//    private LinearLayout mId_root_view;
//    private ImageView mId_right_message;
//    private TextView mId_msg_name;
//    private TextView mId_msg_date;
//    private TextView mId_ecoupon_status;
//    private TextView mId_ecoupon_time;
//    private TextView mId_ecoupon_no;
//    private int mPosition;
//
//    @Override
//    public int getLayoutResId() {
//        return R.layout.item_ecoupuse_record;
//    }
//
//    @Override
//    public void bindViews(View root) {
//        mId_root_view = (LinearLayout) root.findViewById(R.id.id_root_view);
//        mId_right_message = (ImageView) root.findViewById(R.id.id_right_message);
//        mId_msg_name = (TextView) root.findViewById(R.id.id_msg_name);
//        mId_msg_date = (TextView) root.findViewById(R.id.id_msg_date);
//        mId_ecoupon_status = (TextView) root.findViewById(R.id.id_ecoupon_status);
//
//        mId_ecoupon_time = (TextView) root.findViewById(R.id.id_msg_time);
//        mId_ecoupon_no = (TextView) root.findViewById(R.id.id_msg_no);
//    }
//
//
//    @Override
//    public void setViews() {
//
//
//    }
//
//    @Override
//    public void handleData(EcouponConvertRecordEntity.RowsBean rowsBean, int position) {
//        mPosition = position;
//        mId_msg_name.setText(rowsBean.getItemName());
//        mId_msg_date.setText(context.getString(R.string.EC04_1_a)+rowsBean.getExpireDate());
//
//        if (!TextUtils.isEmpty(rowsBean.getConsumeDate())){
//            mId_ecoupon_time.setText(context.getString(R.string.EC16_5_a)+rowsBean.getConsumeDate());
//        }else {
//            mId_ecoupon_time.setVisibility(View.GONE);
//            mId_ecoupon_time.setText("");
//        }
//        if (!TextUtils.isEmpty(rowsBean.getReferenceNo())){
//            mId_ecoupon_no.setText(context.getString(R.string.EC15_7_a)+rowsBean.getReferenceNo());
//        }
//        //状态 35：已使用  36：已过期  33：已失效
//        // 32：未使用，领取完成就是这个状态。 剩下的是其他
//        mId_ecoupon_status.setVisibility(View.VISIBLE);
//        if (rowsBean.getStatus().equals("35")){
//            mId_ecoupon_status.setText(context.getString(R.string.EC16_2));
//            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_used);
//            mId_ecoupon_time.setVisibility(View.VISIBLE);
//            if (TextUtils.isEmpty(rowsBean.getConsumeDate())){
//                mId_ecoupon_time.setVisibility(View.GONE);
//            }else {
//                mId_ecoupon_time.setVisibility(View.VISIBLE);
//            }
//        }else if (rowsBean.getStatus().equals("36")){
//            mId_ecoupon_status.setText(context.getString(R.string.EC16_3));
//            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_outdated);
//            if (TextUtils.isEmpty(rowsBean.getConsumeDate())){
//                mId_ecoupon_time.setVisibility(View.GONE);
//            }else {
//                mId_ecoupon_time.setVisibility(View.VISIBLE);
//            }
//        }else if (rowsBean.getStatus().equals("33")){
//            mId_ecoupon_status.setText(context.getString(R.string.EC16_4));
//            mId_ecoupon_status.setBackgroundResource(R.mipmap.icon_ecoupon_invalid);
//            if (TextUtils.isEmpty(rowsBean.getConsumeDate())){
//                mId_ecoupon_time.setVisibility(View.GONE);
//            }else {
//                mId_ecoupon_time.setVisibility(View.VISIBLE);
//            }
//        }else if (rowsBean.getStatus().equals("32")){
//            mId_ecoupon_status.setVisibility(View.INVISIBLE);
//            if (TextUtils.isEmpty(rowsBean.getConsumeDate())){
//                mId_ecoupon_time.setVisibility(View.GONE);
//            }else {
//                mId_ecoupon_time.setVisibility(View.VISIBLE);
//            }
//        }
//        GlideApp.with(context).load(AppTestManager.getInstance().getBaseUrl() + rowsBean.getMerchantIconUrl()).into(mId_right_message);
//    }
//}
