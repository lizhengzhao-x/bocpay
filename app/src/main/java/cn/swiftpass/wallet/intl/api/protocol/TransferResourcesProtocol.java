package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 转账派利是 拉取图片列表 金额列表
 */
public class TransferResourcesProtocol extends BaseProtocol {

    public TransferResourcesProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/resources";
    }

}
