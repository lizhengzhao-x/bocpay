package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.LruBitmapPool;
import com.bumptech.glide.load.engine.cache.ExternalPreferredCacheDiskCacheFactory;
import com.bumptech.glide.load.engine.cache.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;

import cn.swiftpass.wallet.intl.base.WalletConstants;

/**
 * Android 10 以上 glide升级配置路径
 */
@GlideModule
public class CustomGlideModule extends AppGlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        //多种缓存机制同时存在 https://www.jianshu.com/p/0c5c809957c0
        int cacheSize = WalletConfig.getImageCashTotalSize();
        //diskcache /storage/emulated/0/Android/data/com.bochk.ewallet/cache
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            builder.setDiskCache(new ExternalPreferredCacheDiskCacheFactory(context, WalletConfig.getImageCashSDPath(), cacheSize));
        } else {
            int checkSelfPermission = ActivityCompat.checkSelfPermission(context, WalletConstants.PERMISSIONS_WRITE_WRITE[0]);
            if (checkSelfPermission == PackageManager.PERMISSION_GRANTED) {
                builder.setDiskCache(new ExternalPreferredCacheDiskCacheFactory(context, WalletConfig.getImageCashSDPath(), cacheSize));
            } else {
                builder.setDiskCache(new InternalCacheDiskCacheFactory(context, WalletConfig.getImageCashSDPath(), cacheSize));
            }
        }
        builder.setBitmapPool(new LruBitmapPool(10));
        builder.setDefaultRequestOptions(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.RESOURCE));
    }


    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        registry.replace(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory());
    }

    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }

}