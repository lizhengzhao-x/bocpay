package cn.swiftpass.wallet.intl.app.constants;

/**
 * @author Created by ramon on 2018/8/17.
 * 转账常量
 */

public interface TransferConst {
    //boc转账方式
    String TRANS_METHOD_BOC = "1";
    //fps转账方式
    String TRANS_METHOD_FPS = "2";

    //FPS转账参数
    String TRANS_FPS_DATA = "TRANS_FPS_DATA";
    //FPS转账参数
    String TRANS_FPS_CHECK_DATA = "TRANS_FPS_CHECK_DATA";

    //0：电话，1：邮箱 3.FPS identifier 4：Customer / bank account number
    String TRANS_TYPE_PHONE = "0";
    String TRANS_TYPE_EMAIL = "1";
    String TRANS_FPS = "2";
    String TRANS_CUSTOMER_ACCOUNT = "4";

    //0：FPS-Transfer to other banks     1: Bank of China (HK)
    String TRANS_BANK_TYPE_FPS = "0";
    String TRANS_BANK_TYPE_BOC = "1";

    String TRANS_COLLECTION_TYPE = "trans_collection_type";
    // 付款类别 0：个人，1：商户
    String TRANS_TYPE_PERSON = "0";
    String TRANS_TYPE_MERCHANT = "1";

    //转账查询结果
    /*
    ACPT - Txn Accept
    OTWD - Txn Confirmed and Sent to ICL
    RJCT - Txn Rejected
    FAIL - Txn Failed
    CNCL - Txn Cancelled
    SUCC - Txn Success
     */
    String TRANSFER_RESULT_ACPT = "ACPT";
    String TRANSFER_RESULT_OTWD = "OTWD";
    String TRANSFER_RESULT_RJCT = "RJCT";
    String TRANSFER_RESULT_FAIL = "FAIL";
    String TRANSFER_RESULT_SUCC = "SUCC";
}
