package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 发送我的账户OTP接口，返回对象为 BaseEntity
 */

public class SendSmartAccountOtpProtocol extends BaseProtocol {
    public static final String TAG = SendSmartAccountOtpProtocol.class.getSimpleName();
    /**
     * action
     * V：注册流程  B：绑定流程  F：忘记密码流程 L:登录流程
     */
    String mAction;
    //用户ID
    String mWalletId;

    public SendSmartAccountOtpProtocol(String action, String walletId, NetWorkCallbackListener dataCallback) {
        this.mAction = action;
        mWalletId = walletId;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/triggerOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
    }
}
