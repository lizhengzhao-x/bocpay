package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class DeleteFioProtocol extends BaseProtocol {
    public static final String TAG = DeleteFioProtocol.class.getSimpleName();
    private final String fioId;

    public DeleteFioProtocol(String fioId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.fioId = fioId;
        mUrl = "api/fio/deregister";
    }

    @Override
    public boolean isNeedLogin() {
        return true;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.FIO_ID, fioId);
    }
}
