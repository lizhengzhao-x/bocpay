package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/27 20:50
 */

public class UpdateSmartAcccountVerifyCodeProtocol extends BaseProtocol {
    public static final String TAG = UpdateSmartAcccountVerifyCodeProtocol.class.getSimpleName();

    String width = "200";
    //验证码长度，不大于200
    String height = "60";
    //验证码高度，不大于150
    String codeCount = "4";
    //验证码长度，一般是4位，不大于8位
    //V：注册流程  B：绑定流程  F：忘记密码流程   接口输入：V  （大写）
    String action;

    public UpdateSmartAcccountVerifyCodeProtocol(String action, NetWorkCallbackListener dataCallback) {
        this.action = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/verifyCode/getVerifyCode";
        mEncryptFlag = false;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CODE_HEIGHT, height);
        mBodyParams.put(RequestParams.CODE_WIDTH, width);
        mBodyParams.put(RequestParams.CODE_NUM, codeCount);
        mBodyParams.put(RequestParams.ACTION, action);
    }
}
