package cn.swiftpass.wallet.intl.module.register;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * 我的账户介绍页面
 */

public class RegisterSmartAccountDescActivity extends BaseCompatActivity {

    //正常不显示右上角菜单
    public static final String SHOW_MENU_MORE = "show menu more";
    //正常显示返回图标
    public static final String SHOW_MENU_BACK = "show menu back";
    @BindView(R.id.iv_smart_account_intro)
    ImageView mSmartIntroTV;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    int mTypeFlag;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.smart_account_old);
        Intent intent = getIntent();
        if (null == intent || null == intent.getExtras()) {
            finish();
            return;
        } else {
            mTypeFlag = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
            if (getIntent().getExtras().getBoolean(SHOW_MENU_MORE, false)) {
                setToolBarRightViewToText(getString(R.string.not_now));
            }
            if (!getIntent().getExtras().getBoolean(SHOW_MENU_BACK, true)) {
                hideBackIcon();
            }

            if (mTypeFlag == Constants.PAGE_FLOW_BIND_CARD || mTypeFlag == Constants.DATA_CARD_TYPE_SHOW_INVITE) {
                setToolBarTitle(R.string.bind_title);
            }

            if (mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
                setToolBarTitle(R.string.smart_account_old);
            }
            getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTypeFlag == Constants.DATA_CARD_TYPE_SHOW_INVITE) {
                        boolean showInviteCode = getIntent().getBooleanExtra(Constants.SHOWINVITEPAGE, false);
                        Intent intent = null;
                        if (showInviteCode) {
                            //信用卡注册成功要不要输入邀请码
                            intent = new Intent(RegisterSmartAccountDescActivity.this, RegisterInviteActivity.class);
                            intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
                        } else {
                            //防止解绑重新绑定 要返回到首页
                            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
                            intent = new Intent(RegisterSmartAccountDescActivity.this, MainHomeActivity.class);
                            MyActivityManager.removeAllTaskExcludeMainStack();
                        }
                        intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
                        startActivity(intent);
                    } else {
                        //TODO fix zhangxinchao按道理这里是不是不可能有执行 需要删除代码
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
                        finish();
                    }
                }
            });

        }

        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            mSmartIntroTV.setImageResource(R.mipmap.img_smartaccount_open_sc);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            mSmartIntroTV.setImageResource(R.mipmap.img_smartaccount_open_zh);
        } else {
            mSmartIntroTV.setImageResource(R.mipmap.img_smartaccount_open_en);
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_smart_desc;
    }


    @OnClick({R.id.tv_register})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) return;
        switch (view.getId()) {
            case R.id.tv_register:
                onNext();
                break;
            default:
                break;
        }
    }

    private void onNext() {
        HashMap<String, Object> maps = new HashMap<>();
        if (mTypeFlag == Constants.DATA_CARD_TYPE_SHOW_INVITE) {
            //信用卡注册成功直接绑定智能账户  CURRENT_PAGE_FLOW 要转一下
            maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        } else {
            maps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        }
        ActivitySkipUtil.startAnotherActivity(this, RegisterBankActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startActivity(Activity activity, HashMap maps) {
        int type = (int) maps.get(Constants.CURRENT_PAGE_FLOW);
        if (type == Constants.PAGE_FLOW_FORGETPASSWORD) {
            ActivitySkipUtil.startAnotherActivity(activity, RegisterBankActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(activity, RegisterSmartAccountDescActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }
}
