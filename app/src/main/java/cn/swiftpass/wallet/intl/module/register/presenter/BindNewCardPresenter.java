package cn.swiftpass.wallet.intl.module.register.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetBankCardListProtocol;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardContact;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardListEntity;

public class BindNewCardPresenter extends BasePresenter<BindNewCardContact.View> implements BindNewCardContact.Presenter {

    @Override
    public void getBankCardList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetBankCardListProtocol(new NetWorkCallbackListener<BindNewCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getBankCardListFailed(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(BindNewCardListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getBankCardListSuccess(response);
                }
            }
        }).execute();
    }
}
