package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertRecordEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

public class EcouponRecordAdapter extends BaseRecyclerAdapter<EcouponConvertRecordEntity.RowsBean> {

    public EcouponRecordAdapter(@Nullable List<EcouponConvertRecordEntity.RowsBean> data) {
        super(R.layout.item_ecoupconvert_record, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, EcouponConvertRecordEntity.RowsBean rowsBean, int position) {
        ImageView mId_right_message = baseViewHolder.getView(R.id.id_right_message);

        baseViewHolder.setText(R.id.id_msg_name, rowsBean.getItemName());
        baseViewHolder.setText(R.id.id_msg_date, mContext.getString(R.string.EC15_4_a) + rowsBean.getExchangeDate());
//        String points = rowsBean.getPoints();
        if (rowsBean.hidePoint()) {
            baseViewHolder.setGone(R.id.id_msg_jifen, true);
        } else {
            baseViewHolder.setGone(R.id.id_msg_jifen, false);
        }

        baseViewHolder.setText(R.id.id_msg_count, mContext.getString(R.string.EC15_6_a) + rowsBean.getExchangeNum() + mContext.getString(R.string.EC05c_1));
        if (rowsBean.isShowDistribute()) {
            //消费奖赏
            baseViewHolder.setGone(R.id.tv_reward, false);
        } else {
            baseViewHolder.setGone(R.id.tv_reward, true);
            try {
                baseViewHolder.setText(R.id.id_msg_jifen, mContext.getString(R.string.EC15_5_a) + AndroidUtils.formatPrice(Double.parseDouble(rowsBean.getPoints()), false) + mContext.getString(R.string.EC05_3b_a));
            } catch (Exception e) {
            }
        }

        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + rowsBean.getMerchantIconUrl()).into(mId_right_message);
    }
}
