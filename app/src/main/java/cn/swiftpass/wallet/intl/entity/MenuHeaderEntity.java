package cn.swiftpass.wallet.intl.entity;

import java.io.Serializable;

import cn.swiftpass.httpcore.entity.MySmartAccountEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 17:54
 * @change
 * @chang time
 * @class describe
 */
public class MenuHeaderEntity implements Serializable {
    public String flagBalance;
    public String currency;
    public String sumGp;
    public String flagSumGp;
    public String dailyLimitBalance;
    public String balance;
    public MySmartAccountEntity smartAccountInfo;
    public boolean isPullSuccess = false;
    public boolean isLoading = false;

    public MenuHeaderEntity() {
    }

    public MenuHeaderEntity(boolean isPullSuccess) {
        this.isPullSuccess = isPullSuccess;
    }

    public MenuHeaderEntity(boolean isPullSuccess, boolean isLoading) {
        this.isPullSuccess = isPullSuccess;
        this.isLoading = isLoading;
    }

    public MenuHeaderEntity(String flagBalance, String currency, String sumGp, String flagSumGp, String dailyLimitBalance, String balance, MySmartAccountEntity smartAccountInfo) {
        this.flagBalance = flagBalance;
        this.currency = currency;
        this.sumGp = sumGp;
        this.flagSumGp = flagSumGp;
        this.dailyLimitBalance = dailyLimitBalance;
        this.balance = balance;
        this.smartAccountInfo = smartAccountInfo;
    }
}
