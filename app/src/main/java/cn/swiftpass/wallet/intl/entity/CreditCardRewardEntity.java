package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardRewardEntity extends BaseEntity {

    /**
     * "account": "信用卡（1234）",
     * "accumulatedSpendingAmt": "HKD 100,000.00",
     * "bannerUrl": "xxxxx.jpg",
     * "completionRate": "0.75",
     * "title": "大湾区一卡通",
     * "content": "大湾区一卡通test test test test test test test test test test test test test test test test test test test test",
     * "deadline": "2021-07-01",
     * "details": "xxxxxxx.html",
     * "earnPointsForReachingTarget": "可获得12万积分",
     * "lastUpdateDate": "2021-07-01",
     * "obtainReward": "60000分",
     * "oweArrivalsSpendingAmt": "HKD 5,000.00",
     * "rewardsType": "RB"
     */

    private String account;
    private String accumulatedSpendingAmt;
    private String bannerUrl;
    private String completionRate;
    private String content;
    private String title;
    private String deadline;
    private String details;
    private String earnPointsForReachingTarget;
    private String lastUpdateDate;
    private String obtainReward;
    private String oweArrivalsSpendingAmt;
    private String rewardsType; // 奖赏类型 RB：现金 GP：积分

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccumulatedSpendingAmt() {
        return accumulatedSpendingAmt;
    }

    public void setAccumulatedSpendingAmt(String accumulatedSpendingAmt) {
        this.accumulatedSpendingAmt = accumulatedSpendingAmt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getCompletionRate() {
        return completionRate;
    }

    public void setCompletionRate(String completionRate) {
        this.completionRate = completionRate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getEarnPointsForReachingTarget() {
        return earnPointsForReachingTarget;
    }

    public void setEarnPointsForReachingTarget(String earnPointsForReachingTarget) {
        this.earnPointsForReachingTarget = earnPointsForReachingTarget;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getObtainReward() {
        return obtainReward;
    }

    public void setObtainReward(String obtainReward) {
        this.obtainReward = obtainReward;
    }

    public String getOweArrivalsSpendingAmt() {
        return oweArrivalsSpendingAmt;
    }

    public void setOweArrivalsSpendingAmt(String oweArrivalsSpendingAmt) {
        this.oweArrivalsSpendingAmt = oweArrivalsSpendingAmt;
    }

    public String getRewardsType() {
        return rewardsType;
    }

    public void setRewardsType(String rewardsType) {
        this.rewardsType = rewardsType;
    }
}
