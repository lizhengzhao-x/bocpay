package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * fps 发送otp后要不要倒计时
 */

public class FpsOtpEntity extends BaseEntity {

    //1-显示重新发送，其他-正常倒计时
    String countDownFlag;

    public String getCountDownFlag() {
        return countDownFlag;
    }

    public void setCountDownFlag(String countDownFlag) {
        this.countDownFlag = countDownFlag;
    }


}
