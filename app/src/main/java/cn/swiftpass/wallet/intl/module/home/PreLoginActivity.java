package cn.swiftpass.wallet.intl.module.home;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;
import static cn.swiftpass.wallet.intl.module.staging.view.StagingItemActivity.STAGINGITEM_CASH;
import static cn.swiftpass.wallet.intl.module.staging.view.StagingItemActivity.STAGINGITEM_ITEM;
import static cn.swiftpass.wallet.intl.module.staging.view.StagingItemActivity.STAGINGITEM_MONTH;
import static cn.swiftpass.wallet.intl.utils.statusbar.StatusBarUtil.createTranslucentStatusBarView;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bocpay.analysis.AnalysisManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.bumptech.glide.signature.ObjectKey;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.adapter.CommonPagerSnapHelper;
import cn.swiftpass.boc.commonui.base.adapter.TopDividerItemDecoration;
import cn.swiftpass.httpcore.entity.GreetingEntity;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.HttpInitUtils;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GreetingTime;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.PreLoginBannerEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.activitys.NewMsgActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.home.adapter.HomeBannerAdapter;
import cn.swiftpass.wallet.intl.module.home.constants.MenuItemEnum;
import cn.swiftpass.wallet.intl.module.home.contract.PreLoginContract;
import cn.swiftpass.wallet.intl.module.home.presenter.PreLoginPresenter;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.login.adapter.PreLoginBannerAdapter;
import cn.swiftpass.wallet.intl.module.register.RegisterSelActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterSelectListActivity;
import cn.swiftpass.wallet.intl.module.setting.SettingActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.help.HelpActivity;
import cn.swiftpass.wallet.intl.module.staging.view.StagingItemActivity;
import cn.swiftpass.wallet.intl.module.transfer.view.CrossBorderPaymentActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardApplyActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;
import cn.swiftpass.wallet.intl.utils.GlideImageDownloadManager;
import cn.swiftpass.wallet.intl.utils.GlideRequest;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.utils.encry.InitJsonContants;

public class PreLoginActivity extends BaseCompatActivity<PreLoginContract.Presenter> implements PreLoginContract.View, OnMenuItemClickListener, GlideImageDownLoadListener {

    @BindView(R.id.toolbar_pre_login)
    Toolbar mToolbarPreLogin;

    @BindView(R.id.ll_pre_login_body)
    LinearLayout llPreLoginBody;
    @BindView(R.id.ry_menu)
    RecyclerView menuRy;
    @BindView(R.id.ry_pre_login_bottom_banner)
    RecyclerView bottomBannerRy;
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @BindView(R.id.toolbar_right_fl)
    LinearLayout mToolbarRightFl;
    @BindView(R.id.tv_version)
    TextView versionTv;
    @BindView(R.id.drawer_layout_home)
    LinearLayout mDrawerLayoutHome;
    @BindView(R.id.tv_login)
    TextView tvLogin;
    @BindView(R.id.tv_pre_login_greetings)
    TextView tvPreLoginGreetings;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.ll_setting)
    LinearLayout llSetting;
    @BindView(R.id.ll_help)
    LinearLayout llHelp;

    public static final String DATA_VIRTUAL_CARD_URL = "DATA_VIRTUAL_CARD_URL";
    public static final String DATA_VIRTUAL_CARD_SHARE_URL = "DATA_VIRTUAL_CARD_SHARE_URL";
    /**
     * 判断是否需要检测，防止不停的弹框
     */
    private boolean isNeedCheck = true;
    //退出时间
    private long mExitTime;
    private String appLinkType;
    private AppCallAppLinkEntity appLinkData;
    private MenuItemAdapter menuItemAdapter;
    private ArrayList<MenuItemEntity> menuItemList;
    private String menuEncryptionData = "";
    private String virtualCardUrl;
    private ArrayList<PreLoginBannerEntity> preLoginBannerEntities;
    private ConcurrentHashMap<GreetingTime, String> greetingSpare = new ConcurrentHashMap<>();
    private CountDownTimer timerGreetings;
    private ArrayList<GreetingEntity> resultGreeting;
    private String shareImageUrl;
    private GlideRequest<Bitmap> glideRequest;

    private String bgVersion;
    private ObjectKey backgroudkey;


    /**
     * 涉及到onNewIntent调用 intent会覆盖
     */
    private Intent currentIntent;

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        LogUtils.i(TAG, "onNewIntent--->");
        initLinkAction();
        currentIntent = intent;
        initNotificationAction(intent);
    }


    @Override
    protected void init() {
        currentIntent = getIntent();
//        BocLaunchSDK.saveAppVersion(PreLoginActivity.this);
        setSystemUiMode(SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS);
        initPreLoginToolbar();
        iniPreLoginBg();
        //整个HTTP请求初始化 必须加载
        HttpInitUtils.init(this);
        initLinkAction();
        initNotificationAction(getIntent());
        //进入主界面
//        checkHasPermission();
        initMenuView();
        initData();
        bindBannerData();
        mPresenter.getInitData("");
        startTimer();
        if (!Constants.BUILD_FLAVOR_SIT.equals(BuildConfig.FLAVOR)) {
            if (BuildConfig.DOWNLOAD_CER_SERVER) {
                if (TempSaveHelper.getCertificates() != null) {
                    //如果内存中已经保存 不需要再次拉取
                    LogUtils.i(TAG, "Certificates EXIT");
                } else {
                    AndroidUtils.loadIdvCerts(this);
                }
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        iniPreLoginBg();
        if (mPresenter != null) {
            mPresenter.getBackgroundUrl(this);
        }
    }


    private void iniPreLoginBg() {

        try {
            String preLoginBackground = SpUtils.getInstance().getPreLoginBackground();
            String preLoginBgVersion = SpUtils.getInstance().getPreLoginBgVersion();
            if (TextUtils.isEmpty(bgVersion)) {
                //bgVersion 这个值不能为空
                bgVersion = "0.0.1";
            }
            if (!bgVersion.equals(preLoginBgVersion)) {
                if (!TextUtils.isEmpty(preLoginBgVersion)) {
                    bgVersion = preLoginBgVersion;
                    backgroudkey = new ObjectKey(preLoginBgVersion);
                } else {
                    backgroudkey = new ObjectKey(bgVersion);
                }
            } else {
                if (backgroudkey == null) {
                    backgroudkey = new ObjectKey(bgVersion);
                }
            }
            if (!TextUtils.isEmpty(preLoginBackground)) {
                Uri parse = Uri.parse(preLoginBackground);
                loadBacImage(parse);
            } else {
                setDefaultBackground();
            }
        } catch (Exception e) {
            setDefaultBackground();
        }

    }

    public void setDefaultBackground() {

        if (getActivity() != null && !getActivity().isFinishing()) {
            RequestOptions options = new RequestOptions()
                    .override(AndroidUtils.getScreenWidth(this), Target.SIZE_ORIGINAL);
            Glide.with(this)
                    .asBitmap()
                    .skipMemoryCache(true)
                    .apply(options)
                    .load(R.mipmap.bg_pre_login)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            getWindow().getDecorView().setBackground(new BitmapDrawable(resource));
                        }
                    });
        }
    }

    private void loadBacImage(Uri uri) {
        Glide.with(this)
                .asBitmap()
                .load(uri)
                .signature(backgroudkey)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        getWindow().getDecorView().setBackground(new BitmapDrawable(resource));
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        //本地图片有可能会被清除掉 重新加载本地的图片
                        bgVersion = "0.0.1";
                        //1.清除掉本地的图片资源路径
                        SpUtils.getInstance().setPreLoginBackground("");
                        //2.本地保存图片版本清除掉
                        SpUtils.getInstance().setPreLoginBgVersion("");
                        //3本地保存的图片的文件名重置
                        SpUtils.getInstance().setPreLoginBgName("");
                        setDefaultBackground();
                    }
                });
    }


    /**
     * 如果是通知跳转过来，需要自动跳转到登录界面 然后登陆成功之后弹框
     *
     * @param intent
     */
    private void initNotificationAction(Intent intent) {
        if (intent != null && intent.getBooleanExtra(Constants.IS_PUSH_NOTIFICATION, false)) {
            ActivitySkipUtil.startAnotherActivity(this, LoginActivity.class);
        }
    }


    private void startTimer() {
        //2秒一次判断，相比1秒一次，能少一半的触发判断
        timerGreetings = new CountDownTimer(24 * 60 * 60 * 1000L, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (mContext != null && !PreLoginActivity.this.isFinishing() && tvPreLoginGreetings != null) {
                    //还剩下多少秒millisUntilFinished/1000，依次为2、1、0
                    for (GreetingTime time : greetingSpare.keySet()) {
                        //判断当前时间是否超过问候语的
                        long yesterDayTime = System.currentTimeMillis() - 24 * 60 * 60 * 1000L;
                        if (System.currentTimeMillis() >= time.getStartTime() && System.currentTimeMillis() <= time.getEndTime()) {
                            String greeting = greetingSpare.get(time);
                            if (!TextUtils.isEmpty(greeting) && !greeting.equals(String.valueOf(tvPreLoginGreetings.getText()))) {
                                tvPreLoginGreetings.setText(greeting);
                            }
                        } else if (yesterDayTime >= time.getStartTime() && yesterDayTime <= time.getEndTime()) {
                            //如果倒计时刚好经过23:59:59秒，到了第二天，这时候需要判断为前一天的时间值
                            String greeting = greetingSpare.get(time);
                            if (!TextUtils.isEmpty(greeting) && !greeting.equals(String.valueOf(tvPreLoginGreetings.getText()))) {
                                tvPreLoginGreetings.setText(greeting);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFinish() {//结束后的操作
                if (mContext != null && !PreLoginActivity.this.isFinishing() && tvPreLoginGreetings != null) {
                    updateGreetings(resultGreeting, false);
                    timerGreetings.start();
                }
            }
        };
        timerGreetings.start();
    }

    /**
     * 卡片缓存需要读写sd卡权限(如果用户没授权 只能是内存存储 app仍可以使用)
     */
    private void checkHasPermission() {
        if (isNeedCheck && AndroidUtils.needRequestApplyPermissien()) {
            checkPermissions(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE);
        }
    }

    private void initData() {
        ArrayList<GreetingEntity> defaultGreeting = InitJsonContants.getDefaultGreeting(this);
        resultGreeting = defaultGreeting;
        updateGreetings(defaultGreeting, false);
    }

    private void initPreLoginToolbar() {
        AndroidUtils.setStatusBar(this, true);
        llPreLoginBody.addView(createTranslucentStatusBarView(this, 0), 0);
        mToolbarPreLogin.setNavigationIcon(R.drawable.ic_menu_white);
        setSupportActionBar(mToolbarPreLogin);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu_white);
        }

        initToolBarRightView(mToolbarRightFl);
    }


    private void appLinkAction(String url) {
        if (AppCallAppLinkEntity.TYPE_WEB_VIEW.equals(appLinkType)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            if (appLinkData != null) {
                mHashMaps.put(Constants.DETAIL_URL, appLinkData.getAppLink());
                ActivitySkipUtil.startAnotherActivity(this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        } else {
            AppCallAppPaymentActivity.startAppCallApp(PreLoginActivity.this, url, appLinkData.getMerchantLink(), appLinkData.getNetworkErrorLink(), appLinkData.isAppCallAppFromH5());

//            AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
//            if (appCallAppEvent != null) {
//                appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, url);
//                appCallAppEvent.updateEventParams(AppCallAppEvent.APP_CALL_APP_TTPE, AppCallAppEvent.APPCALLAPPTTPE.APP_CALL_APP_PAYMENT);
//                TempSaveHelper.setAppCallAppConfig(url, appLinkData.getMerchantLink(), appLinkData.getNetworkErrorLink(), appLinkData.isAppCallAppFromH5());
//            } else {
//                LogUtils.e(TAG, "ERROR 每次从SplashActivity进入link应该有值才对");
//            }
//            ActivitySkipUtil.startAnotherActivity(this, LoginActivity.class);

//            AppCallAppPaymentActivity.startAppCallApp(PreLoginActivity.this, url, appLinkData.getMerchantLink(), appLinkData.getNetworkErrorLink(), appLinkData.isAppCallAppFromH5());
//        } else {
//            ActivitySkipUtil.startAnotherActivity(this, LoginActivity.class);
//        }
        }
    }

    public void updateGreetings(ArrayList<GreetingEntity> greetings, boolean useDefault) {
        Date now = new Date();
        String greetStr = "";
        if (greetings != null) {
            greetingSpare.clear();
            for (GreetingEntity greeting : greetings) {
                String dateFormat = !TextUtils.isEmpty(greeting.getDateFormat()) ? greeting.getDateFormat() : "HH:mm:ss";
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd " + dateFormat);
                SimpleDateFormat ds = new SimpleDateFormat("yyyy-MM-dd ");
                try {
                    Date startTime = df.parse(ds.format(now) + greeting.getStartTime());
                    Date endTime = df.parse(ds.format(now) + greeting.getEndTime());

                    if (endTime != null && startTime != null) {
                        GreetingTime greetingTime = new GreetingTime(startTime.getTime(), endTime.getTime());
                        greetingSpare.put(greetingTime, greeting.getGreeting());
                        if (now.getTime() < endTime.getTime() && now.getTime() > startTime.getTime()) {
                            greetStr = greeting.getGreeting();
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (!TextUtils.isEmpty(greetStr)) {
                tvPreLoginGreetings.setText(greetStr);
            } else if (useDefault) {
                ArrayList<GreetingEntity> defaultGreeting = InitJsonContants.getDefaultGreeting(this);
                updateGreetings(defaultGreeting, false);
            }
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_pre_login;
    }

    @Override
    protected PreLoginContract.Presenter createPresenter() {
        return new PreLoginPresenter();
    }

    //对返回键进行监听
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (timerGreetings != null) {
                timerGreetings.cancel();
            }
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void exit() {
        if ((System.currentTimeMillis() - mExitTime) > 2000) {
            mExitTime = System.currentTimeMillis();
        } else {
            //只有杀死app的时候才需要关闭服务 设置语言重启等等不需要
            AnalysisManager.getInstance().stopAnalysisService();
            UserLoginEventManager.getInstance().clearAllEvent();
            finish();
            System.exit(0);
        }
    }


    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!verifyPermissions(paramArrayOfInt)) {
                isNeedCheck = false;
            }
        }

    }


    private void initMenuView() {
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        ActionBarDrawerToggle mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbarPreLogin, R.string.LBS209_7_3, R.string.LBS209_4_3) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AndroidUtils.setStatusBar(PreLoginActivity.this, false);
                if (mPresenter != null) {
                    mPresenter.getMenuList();
                }
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                AndroidUtils.setStatusBar(PreLoginActivity.this, true);
                if (menuItemAdapter != null) {
                    menuItemAdapter.clearSelect();
                    menuItemAdapter.notifyDataSetChanged();
                }
            }
        };
        mDrawerLayout.addDrawerListener(mToggle);
        //同步DrawerLayout的状态
        mToggle.syncState();
        //setNavigationIcon  必须是mToggle.syncState()之后设定
        mToolbarPreLogin.setNavigationIcon(R.drawable.ic_menu_white);
        versionTv.setText("v" + BuildConfig.VERSION_NAME);
        menuRy.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        menuRy.setItemAnimator(null);
        menuItemAdapter = new MenuItemAdapter(this, this);
        menuItemList = MenuItemEnum.toList(this);
        menuItemAdapter.setList(MenuItemEnum.toDefaultList(this));
        menuRy.setAdapter(menuItemAdapter);
    }


    private void bindBannerData() {
        preLoginBannerEntities = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        bottomBannerRy.setLayoutManager(layoutManager);
        PreLoginBannerAdapter adapter = new PreLoginBannerAdapter(mContext, new HomeBannerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                PreLoginBannerEntity preLoginBannerEntity = preLoginBannerEntities.get(position);
                if (!NetworkUtil.isNetworkAvailable()) {
                    showErrorMsgDialog(PreLoginActivity.this, getString(R.string.string_network_available));
                    return;
                }
                if (!preLoginBannerEntity.isInitData()) {
                    if (preLoginBannerEntity.isVideoView()) {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.DETAIL_URL, preLoginBannerEntity.getActionUrl());
                        mHashMaps.put(Constants.DETAIL_TITLE, preLoginBannerEntity.getTitle());
                        ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else if (preLoginBannerEntity.isVirtualCardApplyView()) {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.DETAIL_URL, preLoginBannerEntity.getActionUrl());
                        mHashMaps.put(WebViewActivity.NEW_MSG, true);
//                        mHashMaps.put(Constants.DETAIL_TITLE, preLoginBannerEntity.getTitle());
                        mHashMaps.put(WebViewActivity.SHARE_CONTENT, preLoginBannerEntity.getShareImageUrl());
                        ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        ActivitySkipUtil.startAnotherActivity(PreLoginActivity.this, NewMsgActivity.class);
                    }
                }
            }
        });
        preLoginBannerEntities.add(new PreLoginBannerEntity(R.mipmap.img_banner_skeleton, true));
        preLoginBannerEntities.add(new PreLoginBannerEntity(R.mipmap.img_banner_skeleton, true));
        preLoginBannerEntities.add(new PreLoginBannerEntity(R.mipmap.img_banner_skeleton, true));
        preLoginBannerEntities.add(new PreLoginBannerEntity(R.mipmap.img_banner_skeleton, true));
        preLoginBannerEntities.add(new PreLoginBannerEntity(R.mipmap.img_banner_skeleton, true));

        adapter.setData(preLoginBannerEntities);
        TopDividerItemDecoration mDividerItemDecoration = new TopDividerItemDecoration(mContext,
                RecyclerView.HORIZONTAL);
        Drawable itemDividerDrawable = ContextCompat.getDrawable(mContext, R.drawable.item_divider_home_banner);
        if (itemDividerDrawable != null) {
            mDividerItemDecoration.setDrawable(itemDividerDrawable);
        }
        bottomBannerRy.addItemDecoration(mDividerItemDecoration);
        bottomBannerRy.setAdapter(adapter);
        new CommonPagerSnapHelper().attachToRecyclerView(bottomBannerRy);
    }


    @OnClick({R.id.tv_login, R.id.tv_register, R.id.ll_setting, R.id.ll_help})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                if (ButtonUtils.isFastDoubleClick()) return;
                HashMap<String, Object> param = new HashMap<>();
                param.put(DATA_VIRTUAL_CARD_URL, virtualCardUrl);
                param.put(DATA_VIRTUAL_CARD_SHARE_URL, shareImageUrl);

                if (currentIntent != null && null != currentIntent.getExtras() && currentIntent.getExtras().get(RED_PACKET_RED_PACKET_PARAMS) != null) {
                    param.put(RED_PACKET_RED_PACKET_PARAMS, currentIntent.getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
                }
                ActivitySkipUtil.startAnotherActivity(PreLoginActivity.this, LoginActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.tv_register:
                if (ButtonUtils.isFastDoubleClick()) return;
                HashMap<String, Object> params = new HashMap<>();
                params.put(DATA_VIRTUAL_CARD_URL, virtualCardUrl);
                params.put(DATA_VIRTUAL_CARD_SHARE_URL, shareImageUrl);
                params.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
                ActivitySkipUtil.startAnotherActivity(this, RegisterSelActivity.class, params, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.ll_setting:
                ActivitySkipUtil.startAnotherActivity(this, SettingActivity.class);
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
                break;
            case R.id.ll_help:
//                ActivitySkipUtil.startAnotherActivity(this, RegisterTaxListActivity.class);

                ActivitySkipUtil.startAnotherActivity(this, HelpActivity.class);
                if (mDrawerLayout != null) {
                    mDrawerLayout.closeDrawers();
                }
                break;
            default:
                break;
        }
    }

    private void initLinkAction() {
        AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
        if (appCallAppEvent != null) {
            if (appCallAppEvent.getCurrentEventHashParams() != null && !TextUtils.isEmpty((String) appCallAppEvent.getCurrentEventHashParams().get(AppCallAppEvent.APP_LINK_URL))) {
                String actionUrl = (String) appCallAppEvent.getCurrentEventHashParams().get(AppCallAppEvent.APP_LINK_URL);
                if (!TextUtils.isEmpty(actionUrl)) {
                    if (DeepLinkUtils.isNewDeepLink(actionUrl) || AppCallAppUtils.isFromFpsUrl(actionUrl)) {
                        //新需求中的Deep Link
                        Intent data = new Intent(PreLoginActivity.this, LoginActivity.class);
                        startActivity(data);
                    } else {
                        mPresenter.getAppCallAppLink(null, actionUrl, null, null);
                    }
                }
            }
        }
    }


    @Override
    public void appCallAppLinkSuccess(AppCallAppLinkEntity response, String url) {

        if (!TextUtils.isEmpty(response.getJumpType())) {
            appLinkData = response;
            if (AppCallAppLinkEntity.TYPE_CREATE_QRCODE.equals(response.getJumpType())) {
                appLinkType = AppCallAppLinkEntity.TYPE_CREATE_QRCODE;
            } else if (AppCallAppLinkEntity.TYPE_SCAN_QRCODE.equals(response.getJumpType())) {
                appLinkType = AppCallAppLinkEntity.TYPE_SCAN_QRCODE;
            } else if (AppCallAppLinkEntity.TYPE_ECOUPON.equals(response.getJumpType())) {
                appLinkType = AppCallAppLinkEntity.TYPE_ECOUPON;
            } else if (AppCallAppLinkEntity.TYPE_APPCALLAPP.equals(response.getJumpType())) {
                appLinkType = AppCallAppLinkEntity.TYPE_APPCALLAPP;
            }
        } else {
            if (!TextUtils.isEmpty(response.getAppLink())) {
                appLinkType = AppCallAppLinkEntity.TYPE_WEB_VIEW;
                appLinkData = response;
            }
        }
        appLinkAction(url);
    }

    @Override
    public void appCallAppLinkError(String errorCode, String errorMsg) {

    }

    @Override
    public void getMenuListSuccess(MenuListEntity data) {
        ArrayList<MenuItemEntity> cache = new ArrayList<>();
        if (data != null) {
            //如果2次拉取菜单栏得到的结果相同 不需要刷新侧边栏
            if (menuEncryptionData.equals(data.getMenuEncryptionData())) {
                return;
            }
            menuEncryptionData = data.getMenuEncryptionData();
            ArrayList<MenuItemEntity> menuList = data.getMenuData();
            if (menuList != null && menuList.size() > 0) {
                MenuItemEnum.mapMenuToList(cache, menuList, menuItemList);
                menuItemAdapter.setList(cache);
                menuItemAdapter.notifyDataSetChanged();
            }
        }
    }


    @Override
    public void getMenuListFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getInitDataSuccess(SystemPagerDataEntity response, String action) {
        if (response != null) {
            //设置问候语规则
            SystemInitManager.getInstance().setGreetings(response.getGreetings());
            resultGreeting = response.getGreetings();
            updateGreetings(response.getGreetings(), false);
            if (!SpUtils.getInstance().getMainBgVersion().equals(response.getPostLoginBgImageVer())) {
                GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_MAIN_BG, response.getPostLoginBgImageUrl(), response.getPostLoginBgImageVer());
            }
            if (!SpUtils.getInstance().getPreLoginBgVersion().equals(response.getPreLoginBgImageVer())) {
                GlideImageDownloadManager.getInstance().downloadBackgroundInThread(GlideImageDownloadManager.ACTION_DOWN_PRE_LOGIN_BG, response.getPreLoginBgImageUrl(), response.getPreLoginBgImageVer(), this);
            }
            preLoginBannerEntities.clear();
            preLoginBannerEntities.addAll(response.getBottomBanner());
            if (bottomBannerRy != null && bottomBannerRy.getAdapter() != null) {
                bottomBannerRy.getAdapter().notifyDataSetChanged();
            }
            virtualCardUrl = response.getVirtualCardUrl(this);
            shareImageUrl = response.getVirtualCardShareUrl(this);
            if (MenuItemEnum.VIRTUAL_CARD.action.equals(action)) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void getInitDataFail(String errorCode, String errorMsg) {

    }

    @Override
    public boolean onMenuItemClick(MenuItemEntity item) {
        mDrawerLayout.closeDrawers();
        if (MenuItemEnum.VIRTUAL_CARD_APPLY.action.equals(item.getMenuKey())) {
            if (!TextUtils.isEmpty(virtualCardUrl)) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                mPresenter.getInitData(MenuItemEnum.VIRTUAL_CARD.action);
            }
        } else if (MenuItemEnum.VIRTUAL_CARD_CONFIRM.action.equals(item.getMenuKey())) {
            HashMap<String, Object> param = new HashMap<>();
            param.put(DATA_VIRTUAL_CARD_URL, virtualCardUrl);
            param.put(DATA_VIRTUAL_CARD_SHARE_URL, shareImageUrl);
            ActivitySkipUtil.startAnotherActivity(PreLoginActivity.this, LoginActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (MenuItemEnum.NEW_MESSAGE.action.equals(item.getMenuKey())) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), NewMsgActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (MenuItemEnum.HOME.action.equals(item.getMenuKey())) {

        } else if (MenuItemEnum.PAYMENT_CROSS_BORDER.action.equals(item.getMenuKey())) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), CrossBorderPaymentActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (MenuItemEnum.REWARD_REGISTER.action.equals(item.getMenuKey())) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), RewardRegisterSelectListActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (MenuItemEnum.MONTH_STAGING.action.equals(item.getMenuKey())) {
            HashMap<String, Object> param = new HashMap<>();
            param.put(STAGINGITEM_ITEM, STAGINGITEM_MONTH);
            ActivitySkipUtil.startAnotherActivity(getActivity(), StagingItemActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (MenuItemEnum.CASH_STAGING.action.equals(item.getMenuKey())) {
            HashMap<String, Object> param = new HashMap<>();
            param.put(STAGINGITEM_ITEM, STAGINGITEM_CASH);
            ActivitySkipUtil.startAnotherActivity(getActivity(), StagingItemActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            HashMap<String, Object> param = new HashMap<>();
            param.put(DATA_VIRTUAL_CARD_URL, virtualCardUrl);
            param.put(DATA_VIRTUAL_CARD_SHARE_URL, shareImageUrl);
            ActivitySkipUtil.startAnotherActivity(PreLoginActivity.this, LoginActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timerGreetings != null) {
            timerGreetings.cancel();
        }
    }

    @Override
    public void onSuccessImageDownLoad() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                BaseAbstractActivity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
                if (currentActivity instanceof PreLoginActivity) {
                    iniPreLoginBg();
                }

            }
        });

    }


    @Override
    public void onFailImageDownLoad() {

    }
}
