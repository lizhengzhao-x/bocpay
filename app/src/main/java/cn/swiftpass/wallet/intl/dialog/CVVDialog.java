package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import cn.swiftpass.wallet.intl.R;

/**
 * Created by aijingya on 2018/1/17.
 *
 * @Package cn.swiftpass.wallet.intl.dialog
 * @Description:
 * @date 2018/1/17.17:52.
 */

public class CVVDialog extends Dialog {
    private static final String TAG = "CVVDialog";
    private TextView tv_message;
    private TextView bt_ok;
    private Context mContext;

    public CVVDialog(Activity context) {
        super(context, R.style.Dialog_No_KeyBoard);
        mContext = context;
        init(context);
        setCancelable(true);
    }

    public void init(Context context) {
        View rootView = View.inflate(context, R.layout.cvv_dialog, null);
        setContentView(rootView);

        tv_message = rootView.findViewById(R.id.tv_message);
        bt_ok = rootView.findViewById(R.id.bt_ok);
        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    public void setMessage(String message) {
        if (!TextUtils.isEmpty(message)) {
            tv_message.setText(message);
        } else {
            Toast.makeText(mContext, "with no message", Toast.LENGTH_SHORT).show();
        }
    }
}
