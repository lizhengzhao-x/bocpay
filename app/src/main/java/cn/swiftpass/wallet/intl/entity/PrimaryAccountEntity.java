package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class PrimaryAccountEntity extends BaseEntity {


    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }

    private boolean isSel;
}
