package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * fps 绑定预检查返回
 */

public class FpsPreCheckEntity extends BaseEntity {


    //如果为空，说明没有登记
    String accountId;


    String fppRefNo;
    String srcRefNo;
    String accountIdType;
    String customerFullName;
    String displayName;

    private List<FpsBankBean> data;

    public String getFppRefNo() {
        return fppRefNo;
    }

    public void setFppRefNo(String fppRefNo) {
        this.fppRefNo = fppRefNo;
    }

    public String getSrcRefNo() {
        return srcRefNo;
    }

    public void setSrcRefNo(String srcRefNo) {
        this.srcRefNo = srcRefNo;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }


    public List<FpsBankBean> getData() {
        return data;
    }

    public void setData(List<FpsBankBean> data) {
        this.data = data;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountIdType() {
        return accountIdType;
    }

    public void setAccountIdType(String accountIdType) {
        this.accountIdType = accountIdType;
    }


}
