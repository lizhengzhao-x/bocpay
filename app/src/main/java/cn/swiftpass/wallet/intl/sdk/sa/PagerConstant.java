package cn.swiftpass.wallet.intl.sdk.sa;

/**
 * @name WalletBasicAppUIMergeVersion
 * @class name：cn.swiftpass.wallet.intl.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/23 18:24
 * @change
 * @chang time
 * @class describe
 */
public class PagerConstant {

    public final static String ADDRESS_PAGE_FRONT = "Front Page";
    public final static String ADDRESS_PAGE_TRANSFER_PAGE = "Transfer Page";
    public final static String ADDRESS_PAGE_REFER_FRIENDS_PAGE = "Refer Friends Page";
    public final static String ADDRESS_PAGE_EVOUCHER_PAGE = "eVoucher Page";
    public final static String ADDRESS_PAGE_QR_CODE_PAYMENT_PAGE = "QR Code Payment Page";
    public final static String ADDRESS_PAGE_FRONT_REGISTRATION_PAGE = "Registration Page";
    public final static String ADDRESS_PAGE_SCAN_PAGE = "Scan Page";
    public final static String ADDRESS_PAGE_QR_CODE_PAGE = "QR Code Page";
    public final static String ADDRESS_PAGE_SCAN_TO_PAY_PAGE = "Scan to Pay Page";

    public final static String AGREE_TC_PAGE = "Agree T&C";
    public final static String INPUT_REGISTRATION_MOBILE = "Input registration mobile";
    public final static String BACK_PHOTO_CONFIRM = "Back photo confirm";
    public final static String PHOTO_DETAILS_CONFIRM = "Photo details confirm";
    public final static String FACIAL_RECOGNITION_GUIDE = "Facial recognition guide";
    public final static String FACIAL_RECOGNITION_THIRD_ACTION = "Facial recognition third action";
    public final static String INPUT_REGISTRATION_ADDRESS = "Address";
    public final static String INPUT_REGISTRATION_TAX = "Tax";
    public final static String DAILY_LIMIT_SETTING = "Daily limit setting";
    public final static String ALL_INFORMATION_CONFIRM = "All information confirm";
    public final static String REGISTRATION_WAITING = "Registration waiting";
    public final static String REGISTRATION_FAILED = "Registration failed";
    public final static String PAYMENT_PASSCODE_SETTING = "Payment passcode setting";
    public final static String ONE_TIME_PASSCODE = "One time passcode";
    public final static String REGISTRATION_DONE = "Registration done";
    public final static String ADDRESS_PAGE_UPLAN = "Uplan Page";

}
