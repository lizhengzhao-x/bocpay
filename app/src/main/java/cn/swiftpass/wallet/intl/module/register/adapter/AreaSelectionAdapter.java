package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.BaseRecycleAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.AreaSelectionEntity;

public class AreaSelectionAdapter extends BaseRecycleAdapter<AreaSelectionEntity> {

    private List<AreaSelectionEntity> mList = new ArrayList<>();
    private Context mContext;
    private TextView tv_area;

    public AreaSelectionAdapter(Context context, List<AreaSelectionEntity> datas) {
        super(datas);
        mList = datas;
        mContext = context;
    }

    @Override
    protected void bindData(BaseViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    mListener.onItemClick(position, mList.get(position).getArea());
                }
            }
        });
        tv_area = (TextView) holder.getView(R.id.tv_area);
        Drawable drawable = mContext.getResources().getDrawable(R.mipmap.icon_check_choose_tick);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        if (mList.get(position).isSelection()) {
            tv_area.setCompoundDrawables(null, null, drawable, null);
        } else {
            tv_area.setCompoundDrawables(null, null, null, null);
        }
        tv_area.setText(mList.get(position).getArea());
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_area_selection;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, String area);
    }

    private OnItemClickListener mListener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }

}
