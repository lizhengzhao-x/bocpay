package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.AddVouchersView;
import cn.swiftpass.wallet.intl.module.ecoupon.view.EcouponSelChangedListener;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 电子券 横向布局
 */
public class EcouponsItemHorizontalAdapter implements AdapterItem<RedeemableGiftListEntity.EVoucherListBean> {
    private Context context;
    private ImageView mId_ecoupon_url;
    private TextView mId_ecoupon_title;
    private AddVouchersView mId_switch_view;
    private TextView mId_jifen_discount;
    private TextView mId_jifen_count;
    private TextView mId_check_more;
    private TextView id_need_title;
    private View id_discount_grade_layout;
    private CardView id_card_view;
    private View id_need_grade_layout, id_frame_view, id_grade_view, id_click_view;
    private ImageView id_grade_info;
    /**
     * 此用户是否能参与优化
     */
    public boolean isDiscount = false;

    private EcouponSelChangedListener onItemEcouponCountChangeListener;

    public EcouponsItemHorizontalAdapter(Context context, boolean isDiscount, EcouponSelChangedListener onItemEcouponCountChangeListenerIn) {
        this.context = context;
        this.onItemEcouponCountChangeListener = onItemEcouponCountChangeListenerIn;
        this.isDiscount = isDiscount;
    }

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoupon_horizontal;
    }

    @Override
    public void bindViews(View root) {
        mId_ecoupon_url = (ImageView) root.findViewById(R.id.id_ecoupon_url);
        mId_ecoupon_title = (TextView) root.findViewById(R.id.id_ecoupon_title);
        mId_switch_view = (AddVouchersView) root.findViewById(R.id.id_switch_view);
        mId_jifen_discount = (TextView) root.findViewById(R.id.id_jifen_discount);
        id_need_title = (TextView) root.findViewById(R.id.id_left_title);
        mId_jifen_count = (TextView) root.findViewById(R.id.id_jifen_count);
        mId_check_more = (TextView) root.findViewById(R.id.id_check_detail);
        id_discount_grade_layout = root.findViewById(R.id.id_discount_grade_layout);
        id_grade_info = (ImageView) root.findViewById(R.id.id_grade_info);
        id_need_grade_layout = root.findViewById(R.id.id_need_grade_layout);
        id_card_view = root.findViewById(R.id.id_card_view);
        id_frame_view = root.findViewById(R.id.id_frame_view);
        id_grade_view = root.findViewById(R.id.id_grade_view);
        id_click_view = root.findViewById(R.id.id_click_view);
        addListener();
    }

    private void addListener() {
        mId_switch_view.setOnItemEcouponCountChangeListener(new AddVouchersView.OnItemEcouponCountChangeListener() {
            @Override
            public void onItemEcouponCountChanged(int count, int position) {
                if (onItemEcouponCountChangeListener == null) return;
                onItemEcouponCountChangeListener.onItemEcouponCountChanged(count, mPosition);
            }

            @Override
            public void onItemCheckMore(int position) {

            }

            @Override
            public void onReachMaxValue() {
                onItemEcouponCountChangeListener.onReachMaxValue();
            }

            @Override
            public void onEditTextValue() {
                onItemEcouponCountChangeListener.onEditTextValue(mPosition);
            }


        });
        mId_check_more.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onItemEcouponCountChangeListener.onItemCheckMore(mPosition);
            }
        });
        mId_ecoupon_url.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onItemEcouponCountChangeListener.onItemCheckMore(mPosition);
            }
        });
        id_grade_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponCountChangeListener.onShowGradeInFo();
            }
        });
        id_grade_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponCountChangeListener.onShowGradeInFo();
            }
        });
        id_click_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponCountChangeListener.onShowGradeInFo();
            }
        });
        mId_switch_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponCountChangeListener.onItemCheckMore(mPosition);
            }
        });
        mId_ecoupon_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponCountChangeListener.onItemCheckMore(mPosition);
            }
        });
    }


    @Override
    public void setViews() {
        //cardview 左右阴影效果 边距是 5dp
        int width = (AndroidUtils.getScreenWidth(context) - AndroidUtils.dip2px(context, 20) * 3 - AndroidUtils.dip2px(context, 5) * 4) / 2;
        mId_ecoupon_url.setLayoutParams(new FrameLayout.LayoutParams(width, (int) (width / 1.5)));
    }

    @Override
    public void handleData(RedeemableGiftListEntity.EVoucherListBean eVoucherListBean, int position) {
        mPosition = position;
        mId_ecoupon_title.setText(eVoucherListBean.getEVoucherDesc());
        GlideApp.with(context).load(AppTestManager.getInstance().getBaseUrl() + eVoucherListBean.getCouponImgSmall()).placeholder(R.mipmap.banner_ecoupon_short).into(mId_ecoupon_url);
        if (!TextUtils.isEmpty(eVoucherListBean.getEVoucherOriginalPrice())) {
            mId_jifen_count.setText(AndroidUtils.formatPrice(Double.valueOf(eVoucherListBean.getEVoucherOriginalPrice()), false) + context.getString(R.string.EC05_3b_a));
        }
        mId_switch_view.updateCurrentCnt(eVoucherListBean.getCurrentSelCnt());
        mId_switch_view.setMaxCount(50 + "");
        if (isDiscount) {
            if (!TextUtils.isEmpty(eVoucherListBean.getEVoucherUnitPrice())) {
                mId_jifen_discount.setText(AndroidUtils.formatPrice(Double.valueOf(eVoucherListBean.getEVoucherUnitPrice()), false) + context.getString(R.string.EC05_3b_a));
            }
            mId_jifen_count.getPaint().setAntiAlias(true);
            mId_jifen_count.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            id_need_title.getPaint().setAntiAlias(true);
            id_need_title.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            mId_jifen_discount.setVisibility(View.GONE);
            id_discount_grade_layout.setVisibility(View.GONE);
        }
    }
}
