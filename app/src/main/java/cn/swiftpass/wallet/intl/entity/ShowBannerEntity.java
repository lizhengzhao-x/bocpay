package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class ShowBannerEntity extends BaseEntity {

    public String getAppBannerShow() {
        return AppBannerShow;
    }

    public void setAppBannerShow(String appBannerShow) {
        AppBannerShow = appBannerShow;
    }

    private String AppBannerShow;
}
