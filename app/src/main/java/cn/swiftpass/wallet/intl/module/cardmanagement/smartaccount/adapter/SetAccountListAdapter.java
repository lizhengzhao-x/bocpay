package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class SetAccountListAdapter implements AdapterItem<StaticCodeEntity.MobnQRBean> {
    private Context context;

    private CardItemSelCallback cardItemSelCallback;

    public SetAccountListAdapter(Context context, CardItemSelCallback cardItemSelCallback) {

        this.context = context;
        this.cardItemSelCallback = cardItemSelCallback;
    }


    private TextView id_tv_left;
    private ImageView id_image_right;

    private View id_parent_view;

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_set_default_account;
    }

    @Override
    public void bindViews(View root) {
        id_tv_left = root.findViewById(R.id.id_tv_left);
        id_image_right = root.findViewById(R.id.id_image_right);
        id_parent_view = root.findViewById(R.id.id_parent_view);
        id_image_right.setBackground(null);
    }

    @Override
    public void setViews() {
        id_parent_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemSelCallback.onItemCardSelClick(mPosition);
            }
        });
    }

    @Override
    public void handleData(StaticCodeEntity.MobnQRBean cardEntity, final int position) {
        String accountInfo = AndroidUtils.getPrimaryAccountDisplay(cardEntity.getAcTp(), cardEntity.getAcNo());
        id_tv_left.setText(accountInfo);
        //if (cardEntity.getIsDefault().equals("Y")) {
        if (cardEntity.isSel()) {
            id_image_right.setVisibility(View.VISIBLE);
            id_image_right.setImageResource(R.mipmap.icon_check_choose_tick);
        } else {
            id_image_right.setVisibility(View.INVISIBLE);
        }
        mPosition = position;
    }

    public static class CardItemSelCallback {


        public void onItemCardSelClick(int position) {
            // do nothing
        }

    }
}
