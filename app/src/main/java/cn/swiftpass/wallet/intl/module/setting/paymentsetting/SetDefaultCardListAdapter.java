package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PaymentListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;


public class SetDefaultCardListAdapter implements AdapterItem<PaymentListEntity.RowsBean> {
    private Context context;

    private CardItemSelCallback cardItemSelCallback;

    public SetDefaultCardListAdapter(Context context, CardItemSelCallback cardItemSelCallback) {

        this.context = context;
        this.cardItemSelCallback = cardItemSelCallback;
    }


    private TextView id_tv_left;
    private TextView id_tv_sub_title;
    private ImageView id_image_right;
    private ImageView id_left_image;

    private View id_parent_view;

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_set_defalut_card;
    }

    @Override
    public void bindViews(View root) {
        id_tv_left = root.findViewById(R.id.id_tv_left);
        id_tv_sub_title = root.findViewById(R.id.id_tv_sub_title);
        id_image_right = root.findViewById(R.id.id_image_right);
        id_left_image = root.findViewById(R.id.id_left_image);
        id_parent_view = root.findViewById(R.id.id_parent_view);
        id_image_right.setBackground(null);
    }

    @Override
    public void setViews() {
        id_parent_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemSelCallback.onItemCardSelClick(mPosition);
            }
        });
    }

    @Override
    public void handleData(PaymentListEntity.RowsBean cardEntity, final int position) {

        id_tv_left.setText(AndroidUtils.getSubMasCardNumberTitle(context, cardEntity.getPanShowNumber(), cardEntity.getCardType() + ""));
        id_image_right.setVisibility(View.INVISIBLE);
        id_image_right.setImageResource(R.mipmap.icon_check_choose_tick);
        GlideApp.with(context)
                .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                .into(id_left_image);
        if (cardEntity.getIsDefault().equals("1")) {
            id_image_right.setVisibility(View.VISIBLE);
            id_image_right.setImageResource(R.mipmap.icon_check_choose_tick);
        } else {
            id_image_right.setVisibility(View.INVISIBLE);
        }
        if (!TextUtils.isEmpty(cardEntity.getSubTitle())) {
            id_tv_sub_title.setVisibility(View.VISIBLE);
            id_tv_sub_title.setText(cardEntity.getSubTitle());
        } else {
            id_tv_sub_title.setVisibility(View.GONE);
        }
        mPosition = position;
    }

    public static class CardItemSelCallback {


        public void onItemCardSelClick(int position) {
            // do nothing
        }

    }
}
