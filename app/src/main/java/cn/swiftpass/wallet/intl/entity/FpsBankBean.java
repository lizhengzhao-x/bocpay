package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/10/9.
 * FPS银行信息
 */

public class FpsBankBean extends BaseEntity {


    // -- 银行代码 012-中银
    String bankcode;
    // -- 银行名称
    String bankName;
    // --- 账户状态
    String status;

    public String getBankcode() {
        return bankcode;
    }

    public void setBankcode(String bankcode) {
        this.bankcode = bankcode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
