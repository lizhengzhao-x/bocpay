package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 查询行内绑定fps的记录
 */

public class FpsQueryBankRecords extends BaseProtocol {

    public FpsQueryBankRecords(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/queryBankRecords";
    }
}
