package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetRewardRegistrationListProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardActivityListEntity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CAMPAIGNID;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.IMAGEURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.TERMINFOURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.BANKCARDTYPE;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.URL_REWARD;

public class RewardRegisterSelectListFragment extends BaseFragment {

    @BindView(R.id.id_recycleview)
    RecyclerView recyclerView;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    private RewardSelListAdapter rewardSelListAdapter;
    private List<RewardActivityListEntity.RewardActivity> rewardActivities;
    private View emptyView;

    BottomDividerItemDecoration itemDecoration;

    @Override
    public void initTitle() {

        mActivity.setToolBarTitle(R.string.CR209_7_1);
    }

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_reward_sellist;
    }

    @Override
    protected void initView(View v) {
        rewardActivities = new ArrayList<>();
        rewardSelListAdapter = new RewardSelListAdapter(rewardActivities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.setAdapter(rewardSelListAdapter);

        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getActivityList();
            }
        });

        emptyView = getEmptyView(mContext, recyclerView);
        rewardSelListAdapter.setEmptyView(emptyView);
        rewardSelListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(CAMPAIGNID, rewardActivities.get(position).getCampaignId());
                maps.put(URL_REWARD, rewardActivities.get(position).getActivityUrl());
                maps.put(IMAGEURL, rewardActivities.get(position).getImagesUrl());
                maps.put(TERMINFOURL, rewardActivities.get(position).getTermsAndConditionsUrl());
                maps.put(BANKCARDTYPE, rewardActivities.get(position).getBankCardType());
                maps.put(REMARK, rewardActivities.get(position).getExplain());
                ActivitySkipUtil.startAnotherActivity(getActivity(), RewardRegisterWebviewActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        getActivityList();
    }

    protected View getEmptyView(Context mContext, RecyclerView recyclerView) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(R.string.LI10_1_2));
        noDataImg.setVisibility(View.GONE);
        return emptyView;
    }

    private void getActivityList() {
        showDialogNotCancel();
        new GetRewardRegistrationListProtocol(new NetWorkCallbackListener<RewardActivityListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
                showErrorMsgDialog(getContext(), errorMsg);
            }

            @Override
            public void onSuccess(RewardActivityListEntity response) {
                dismissDialog();
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
                if (response != null && !response.getValue().isEmpty()) {

                    /**
                     * 判断itemDecor的个数来要不要添加itemDecoration
                     */
                    if (recyclerView.getItemDecorationCount() <= 0) {
                        recyclerView.addItemDecoration(itemDecoration);
                    }

                    emptyView.setVisibility(View.GONE);
                    rewardActivities.clear();
                    rewardActivities.addAll(response.getValue());
                    rewardSelListAdapter.notifyDataSetChanged();
                } else {
                    /**
                     * 需要删除itemDecoration  保证emptyView的位置是居中的
                     */
                    recyclerView.removeItemDecoration(itemDecoration);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        }).execute();
    }


}
