package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/5/22.
 * FIO 注册第二步返回
 */

public class FioCheckValidEntity extends BaseEntity {

    public String getTxnData() {
        return txnData;
    }

    public void setTxnData(String txnData) {
        this.txnData = txnData;
    }

    //客户端调用SDK Authentication接口，需要此参数  To FIO-HK SDK
    String txnData;

}
