package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;


import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSetPaymentPswActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

import static cn.swiftpass.wallet.intl.utils.AndroidUtils.CURRENCY_SYMBOL;


public class SmartAccountInfoRegisterActivity extends BaseCompatActivity {

    @BindView(R.id.id_account_detail)
    TextView idAccountDetail;
    @BindView(R.id.id_daily_money)
    TextView idDetailMoney;
    @BindView(R.id.tv_topup_value)
    TextView mTopupMethod;
    @BindView(R.id.tv_account_type)
    TextView mAccountTypeTV;


    @BindView(R.id.tv_link_bo_bocpay)
    TextView tvLinkBoBocpay;
    int mTypeFlag;
    BankLoginResultEntity mSmartAccountInfo;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.string_regist_smart_account);
        if (null != getIntent()) {
            mTypeFlag = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
            mSmartAccountInfo = (BankLoginResultEntity) getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO);
            initData();
            if (mTypeFlag == Constants.PAGE_FLOW_FORGETPASSWORD) {
                //重设密码成功
                setToolBarTitle(R.string.setting_payment_fgw);
                tvLinkBoBocpay.setText(R.string.next_page);
            } else if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
                //升级
                setToolBarTitle(R.string.P3_ewa_01_028);
            }
        }
    }

    private void initData() {
        idAccountDetail.setText(AndroidUtils.getPrimaryAccountDisplay(mSmartAccountInfo.getAccType(), mSmartAccountInfo.getRelevanceAccNo()));
        String payLimit = CURRENCY_SYMBOL + " " + AndroidUtils.formatPriceInt(Integer.valueOf(mSmartAccountInfo.getPayLimit()), true);
        idDetailMoney.setText(payLimit);
        mTopupMethod.setText(AndroidUtils.getTopUpMethodValue(this, mSmartAccountInfo.getAddedmethod()));
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smart_info_register;
    }


    @OnClick(R.id.tv_link_bo_bocpay)
    public void onViewClicked() {
        if (mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
            mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_SMART);
            mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, mSmartAccountInfo);
            mHashMaps.put(Constants.WALLET_ID, mSmartAccountInfo.getUserId());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getPhone());
            mHashMaps.put(Constants.USE_BIOMETRICAUTH, getIntent().getBooleanExtra(Constants.USE_BIOMETRICAUTH, true));
            ActivitySkipUtil.startAnotherActivity(SmartAccountInfoRegisterActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getPhone());
            mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_SMART);
            mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, mSmartAccountInfo);
            mHashMaps.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_U);
            ActivitySkipUtil.startAnotherActivity(SmartAccountInfoRegisterActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
            mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_SMART);
            mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, mSmartAccountInfo);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getPhone());
            ActivitySkipUtil.startAnotherActivity(SmartAccountInfoRegisterActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }


}
