package cn.swiftpass.wallet.intl.module.home;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.adapter.HomeLifeMenuDividerItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.HomeLifeMenuEntity;
import cn.swiftpass.wallet.intl.module.home.adapter.HomeLifeMenuItemHolder;
import cn.swiftpass.wallet.intl.module.home.constants.MenuKey;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class HomeLifeMenuAdapter extends RecyclerView.Adapter {


    private final Context context;
    private final OnLifeMenuItemClickListener listener;
    private final RecyclerView ryLifeMenu;
    private ArrayList<HomeLifeMenuEntity> data;
    private float itemDivider = 0f;
    private int itemWidth = 0;
    private int imageWidthPx;
    private int defaultItemPadding;
    private int leftPadding;
    private HomeLifeMenuDividerItem itemDecoration;


    public HomeLifeMenuAdapter(Context context, OnLifeMenuItemClickListener listener, RecyclerView ryLifeMenu) {
        this.context = context;
        this.listener = listener;
        this.ryLifeMenu = ryLifeMenu;
        imageWidthPx = AndroidUtils.dip2px(context, 64);
        defaultItemPadding = AndroidUtils.dip2px(context, 10);
        leftPadding = AndroidUtils.dip2px(context, 20);
    }

    public void setData(ArrayList<HomeLifeMenuEntity> data) {
        this.data = data;
        itemDivider = 0f;
        itemWidth = 0;
        imageWidthPx = AndroidUtils.dip2px(context, 64);
        defaultItemPadding = AndroidUtils.dip2px(context, 10);
        leftPadding = AndroidUtils.dip2px(context, 20);
        if (itemDecoration != null) {
            ryLifeMenu.removeItemDecoration(itemDecoration);
        }
        measureItemWidth();
        setItemDivider();
    }

    private void setItemDivider() {
        if (itemDivider > 0) {
            itemDecoration = new HomeLifeMenuDividerItem((int) itemDivider, leftPadding);
            ryLifeMenu.addItemDecoration(itemDecoration);
        }

    }

    private void measureItemWidth() {
        if (data != null && data.size() > 3) {
            int textWidth = calcTextWidth(data);
            int imageWidth = imageWidthPx;
            int defaultPadding = defaultItemPadding;
            //为了防止可能存在文本前后相接，在Item宽度多添加一定的宽度
            //如果文本宽度小于图片宽度 则使用图片宽度作为item的宽度
            if (textWidth < imageWidth) {
                itemWidth = imageWidth + defaultPadding;
            } else {
                itemWidth = textWidth + defaultPadding;
            }
            itemDivider = calcItemDivider(itemWidth);

        } else {
            itemWidth = AndroidUtils.getScreenWidth(context) / 3;
            itemDivider = 0f;
        }
    }

    /**
     * 计算条目之间的间距值
     *
     * @param itemWidth
     * @return
     */
    private float calcItemDivider(int itemWidth) {


        int screenWidth = AndroidUtils.getScreenWidth(context);
        //屏幕宽度-左侧填充宽度=可用于item布局的宽度
        int visibleWidth = screenWidth - leftPadding;
        //如果再减去右边的空白区域后，还填不满一屏，那么把剩下空间用于间距
        if (itemWidth * getItemCount() < visibleWidth-leftPadding) {
            //如果条目铺不满一屏
            int rightPadding=leftPadding;
            float dividerTotal = visibleWidth* 1.0f-rightPadding - itemWidth * getItemCount();
            return dividerTotal / (getItemCount()-1);
        } else {
            //得到一共有多少个完整的可视条目
            int itemCompleteVisibleCount = visibleWidth / itemWidth;
            //显示了多少个条目 精确到小数
            float itemVisibleCount = visibleWidth * 1.0f / itemWidth;
            //总体可用间距值
            float dividerTotal = 0f;
            float dividerItemCount = itemCompleteVisibleCount;
            //如果视图上最后一个可见的Item条目超过了他本身的一半，则把多出的宽度用于间距使用
            if (itemVisibleCount > (itemCompleteVisibleCount + 0.5f)) {
                dividerTotal = (itemVisibleCount - itemCompleteVisibleCount - 0.5f) * itemWidth;
                dividerItemCount = itemCompleteVisibleCount;
            } else {
                //如果视图上最后一个可见的Item条目少于了他本身的一半，则把多出的宽度+前一个条目的一半用于间距使用
                dividerTotal = (itemVisibleCount - itemCompleteVisibleCount + 0.5f) * itemWidth;
                //因为取了前一个条目的一半，所以需要均分的条目少一个
                dividerItemCount = itemCompleteVisibleCount - 1;
            }

            //平摊间距总宽度到每个条目中
            float dividerItem = dividerTotal / dividerItemCount;

            //小于0时，不需要间隔
            if (dividerItem < 1) {
                dividerItem = 0;
            }
            return dividerItem;
        }


    }

    /**
     * 计算文本宽度，
     *
     * @param data
     * @return
     */
    private int calcTextWidth(ArrayList<HomeLifeMenuEntity> data) {
        String maxTextWidth = "";

        for (int i = 0; i < data.size(); i++) {
            HomeLifeMenuEntity lifeMenuEntity = data.get(i);
            if (lifeMenuEntity != null && !TextUtils.isEmpty(lifeMenuEntity.getName())) {
                String lifeName = "";
                if (lifeMenuEntity.getName().contains("\\n")) {
                    String[] split = lifeMenuEntity.getName().split("\\n");
                    for (int s = 0; s < split.length; s++) {
                        if (!TextUtils.isEmpty(split[s]) && split[s].length() > lifeName.length()) {
                            lifeName = split[s];
                        }
                    }
                } else {
                    lifeName = lifeMenuEntity.getName();
                }


                if (lifeMenuEntity.getName().length() > maxTextWidth.length()) {
                    maxTextWidth = lifeName;
                }
            }
        }
        if (!TextUtils.isEmpty(maxTextWidth)) {
            TextView calcTextTv = new TextView(context);
            calcTextTv.setTextSize(16);
            //得到最长文本的宽度
            float width = calcTextTv.getPaint().measureText(maxTextWidth);
            //得到第一个条目的文本的宽度
            float widthFirst = calcTextTv.getPaint().measureText(data.get(0).getName());

            if (widthFirst<imageWidthPx){
                widthFirst=imageWidthPx;
            }
            //如果第一个文本的宽度小于最长文本的宽度，则相应的leftpadding减少一定的量，直至leftpadding为0
            if (widthFirst < width) {
                float tmp = leftPadding - (width - widthFirst);
                if (tmp < 0) {
                    leftPadding = 0;
                } else {
                    leftPadding = (int) tmp;
                }
            }
            return (int) width;
        } else {
            return 0;
        }
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_home_life_menu, parent, false);
        RecyclerView.LayoutParams params = new RecyclerView.LayoutParams(itemWidth, WRAP_CONTENT);
        itemView.setLayoutParams(params);
        return new HomeLifeMenuItemHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        HomeLifeMenuItemHolder homeLifeMenuItemHolder = (HomeLifeMenuItemHolder) holder;
        HomeLifeMenuEntity homeLifeMenuEntity = data.get(position);
        homeLifeMenuItemHolder.tvItemName.setText(homeLifeMenuEntity.getName());
        GlideApp.with(context)
                .load(homeLifeMenuEntity.getResIdByImage())
                .placeholder(R.mipmap.list_cycle)
                .error(R.mipmap.list_cycle)
                .into(homeLifeMenuItemHolder.ivItemImage);

        if (MenuKey.CREDIT_CARD_REWARD.equals(homeLifeMenuEntity.getMenuKey())) {
            homeLifeMenuItemHolder.tvItemRightTop.setVisibility(View.VISIBLE);
            homeLifeMenuItemHolder.tvItemRightTop.setText(context.getString(R.string.H10_2_26));
        } else {
            if (homeLifeMenuEntity.getCount() > 0) {
                homeLifeMenuItemHolder.tvItemRightTop.setVisibility(View.VISIBLE);
                if (homeLifeMenuEntity.getCount() > 99) {
                    homeLifeMenuItemHolder.tvItemRightTop.setText("99+");
                } else {
                    homeLifeMenuItemHolder.tvItemRightTop.setText(String.valueOf(homeLifeMenuEntity.getCount()));
                }
            } else {
                homeLifeMenuItemHolder.tvItemRightTop.setVisibility(View.GONE);
            }
        }

        homeLifeMenuItemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onHomeLifeMenuItemClick(homeLifeMenuEntity);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }
}
