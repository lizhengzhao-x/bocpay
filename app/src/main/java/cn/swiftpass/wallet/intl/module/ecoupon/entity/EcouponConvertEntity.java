package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/7/16.
 */
public class EcouponConvertEntity extends BaseEntity {

    private String redeemPoint;
    private String giftTp;

    public List<RedeemItem> getRedeemItems() {
        return redeemItems;
    }

    public void setRedeemItems(List<RedeemItem> redeemItems) {
        this.redeemItems = redeemItems;
    }

    private List<RedeemItem> redeemItems;
    private String pan;

    public String getRedeemPoint() {
        return redeemPoint;
    }

    public void setRedeemPoint(String redeemPoint) {
        this.redeemPoint = redeemPoint;
    }

    public String getGiftTp() {
        return giftTp;
    }

    public void setGiftTp(String giftTp) {
        this.giftTp = giftTp;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getReqAct() {
        return reqAct;
    }

    public void setReqAct(String reqAct) {
        this.reqAct = reqAct;
    }

    public String getRedmId() {
        return redmId;
    }

    public void setRedmId(String redmId) {
        this.redmId = redmId;
    }

    private String reqAct;
    private String redmId;

    public static class RedeemItem extends BaseEntity {
        private String giftCode;

        public String getGiftCode() {
            return giftCode;
        }

        public void setGiftCode(String giftCode) {
            this.giftCode = giftCode;
        }

        public String geteVoucherQty() {
            return eVoucherQty;
        }

        public void seteVoucherQty(String eVoucherQty) {
            this.eVoucherQty = eVoucherQty;
        }

        public String getAswItemCode() {
            return aswItemCode;
        }

        public void setAswItemCode(String aswItemCode) {
            this.aswItemCode = aswItemCode;
        }

        private String eVoucherQty;
        private String aswItemCode;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        private String itemName;

        public String getOrderTp() {
            return orderTp;
        }

        public void setOrderTp(String orderTp) {
            this.orderTp = orderTp;
        }

        private String orderTp;

    }

}
