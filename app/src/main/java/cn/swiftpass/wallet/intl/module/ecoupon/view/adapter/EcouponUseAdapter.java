package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.boc.commonui.base.utils.QRCodeUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponsDetailEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;


public class EcouponUseAdapter extends RecyclerView.Adapter<EcouponUseAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    private OnQrCodeRetryListener onQrCodeRetryListener;
    private OnArrowClickListener onArrowClickListener;
    private List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> eVoucherInfosBeans;
    private int totalWith;

    public EcouponUseAdapter(Context context, List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> eVoucherInfosBeansIn, int totalWithIn, OnQrCodeRetryListener onQrCodeRetryListenerIn, OnArrowClickListener onArrowClickListener) {
        mInflater = LayoutInflater.from(context);
        eVoucherInfosBeans = eVoucherInfosBeansIn;
        this.mContext = context;
        this.onQrCodeRetryListener = onQrCodeRetryListenerIn;
        this.totalWith = totalWithIn;
        this.onArrowClickListener = onArrowClickListener;
    }

    @Override
    public EcouponUseAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_qrcode, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvTitle = (TextView) view.findViewById(R.id.id_ecoupon_title);
        viewHolder.tvName = (TextView) view.findViewById(R.id.id_ecoupon_name);
        viewHolder.tvNumber = (TextView) view.findViewById(R.id.id_ecoupon_number);
        viewHolder.tvExpireDate = (TextView) view.findViewById(R.id.id_ecoupon_expire_date);
        viewHolder.centerImage = (ImageView) view.findViewById(R.id.id_ecoupon_qrcode);
        viewHolder.merchantImage = (ImageView) view.findViewById(R.id.id_ecoupon_image);
        viewHolder.id_already_use = view.findViewById(R.id.id_already_use);
        viewHolder.id_already_use_tv = view.findViewById(R.id.id_already_use_tv);
        viewHolder.id_linear_view = view.findViewById(R.id.id_linear_view);
        viewHolder.id_no_netword = view.findViewById(R.id.id_no_netword);
        viewHolder.leftArrowImage = view.findViewById(R.id.id_img_e_gift_left);
        viewHolder.rightArrowImage = view.findViewById(R.id.id_img_e_gift_right);
        viewHolder.id_linear_view.setLayoutParams(new LinearLayout.LayoutParams((int) (totalWith * 0.7), ViewGroup.LayoutParams.WRAP_CONTENT));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final EcouponUseAdapter.ViewHolder holder, final int position) {
        MyEVoucherEntity.EvoucherItem.EVoucherInfosBean eVoucherInfosBean = eVoucherInfosBeans.get(position);
        EcouponsDetailEntity ecouponsDetailEntity = eVoucherInfosBean.getEcouponsDetail();

        holder.tvTitle.setText("");
        holder.tvName.setText("");
        holder.tvNumber.setText("");
        holder.tvExpireDate.setText("");
        holder.centerImage.setImageBitmap(null);
        holder.merchantImage.setImageBitmap(null);
        holder.id_no_netword.setVisibility(View.GONE);
        holder.id_already_use.setVisibility(View.GONE);

        //点击左箭头
        holder.leftArrowImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //当前位置position，点击左箭头应切换至 position - 1 位置
                if (position - 1 < 0) {
                    //若下一位置position-1 < 0, 则当前position为第一张
                    return;
                }
                onArrowClickListener.onLeftArrowClick(position - 1);
            }
        });
        //点击右箭头
        holder.rightArrowImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //当前位置position，点击右箭头应切换至 position + 1 位置
                if (position + 1 > getItemCount() - 1) {
                    //若下一位置position+1 > getItemCount() - 1, 则当前position为最后一张
                    return;
                }
                onArrowClickListener.onRightArrowClick(position + 1);
            }
        });

        if (eVoucherInfosBean.isGetDetailSuccess()) {
            if (ecouponsDetailEntity != null) {
                holder.tvTitle.setText(ecouponsDetailEntity.getBuName());
                holder.tvName.setText(ecouponsDetailEntity.getItemName());
                holder.tvNumber.setText(ecouponsDetailEntity.getReferenceNo());
                holder.tvExpireDate.setText(mContext.getString(R.string.EC16_1) + ":" + ecouponsDetailEntity.getExpireDate());
                int width = (int) (AndroidUtils.dip2px(mContext, 140));
                if (!TextUtils.isEmpty(ecouponsDetailEntity.getQRCode())) {
                    holder.centerImage.setImageBitmap(QRCodeUtils.createQRCode(ecouponsDetailEntity.getQRCode(), width));
                }
                GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + ecouponsDetailEntity.getMerchantIconUrl()).placeholder(R.mipmap.icon_coupon_brand_loading).into(holder.merchantImage);
                if (ecouponsDetailEntity.isUsed()) {
                    //已使用
                    holder.id_already_use.setVisibility(View.VISIBLE);
                    holder.id_already_use_tv.setText(mContext.getString(R.string.EC16_2));
                } else if (ecouponsDetailEntity.getStatus().equals("36")) {
                    //已过期
                    holder.id_already_use.setVisibility(View.VISIBLE);
                    holder.id_already_use_tv.setText(mContext.getString(R.string.EC16_3));
                } else if (ecouponsDetailEntity.getStatus().equals("33")) {
                    //已失效
                    holder.id_already_use.setVisibility(View.VISIBLE);
                    holder.id_already_use_tv.setText(mContext.getString(R.string.EC16_4));
                } else {
                    holder.id_already_use.setVisibility(View.INVISIBLE);
                }
                holder.id_no_netword.setVisibility(View.GONE);
            }
        } else {
            holder.tvTitle.setText("");
            holder.tvName.setText("");
            holder.tvNumber.setText("");
            holder.tvExpireDate.setText("");
            holder.centerImage.setImageBitmap(null);
            holder.merchantImage.setImageBitmap(null);
            holder.id_no_netword.setVisibility(View.VISIBLE);
            holder.id_already_use.setVisibility(View.GONE);
            holder.id_no_netword.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    onQrCodeRetryListener.OnQrCodeRetry(position);
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return eVoucherInfosBeans.size();
    }


    public interface OnQrCodeRetryListener {

        void OnQrCodeRetry(int position);
    }


    /**
     * 箭头点击事件监听
     */
    public interface OnArrowClickListener {

        /**
         * 点击左箭头
         *
         * @param toLeftPosition
         */
        void onLeftArrowClick(int toLeftPosition);

        /**
         * 点击右箭头
         *
         * @param toRightPosition
         */
        void onRightArrowClick(int toRightPosition);

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvName, tvNumber, tvExpireDate, id_already_use_tv;
        ImageView centerImage, merchantImage;
        View id_already_use, id_linear_view, id_no_netword;
        /**
         * leftArrowImage  左向箭头
         * rightArrowImage 右向箭头
         */
        ImageView leftArrowImage, rightArrowImage;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

}
