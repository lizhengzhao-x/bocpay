package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * 带tax资料填写状态的 TaxInfo 发送数据
 */
public class TaxInfoParamEntity extends BaseEntity {

    private String residenceJurisdiction;
    private String taxNumber;
    private String noResidentReason;
    private String noResidentReasonInfo;
    private String taxResidentRegionName;

    public TaxInfoParamEntity() {
    }

    public TaxInfoParamEntity(String residenceJurisdiction, String taxNumber) {
        this.residenceJurisdiction = residenceJurisdiction;
        this.taxNumber = taxNumber;
    }

    public TaxInfoParamEntity(String residenceJurisdiction, String taxNumber, String taxResidentRegionName) {
        this.residenceJurisdiction = residenceJurisdiction;
        this.taxNumber = taxNumber;
        this.taxResidentRegionName = taxResidentRegionName;
    }

    public String getTaxResidentRegionName() {
        return taxResidentRegionName;
    }

    public void setTaxResidentRegionName(String taxResidentRegionName) {
        this.taxResidentRegionName = taxResidentRegionName;
    }

    public String getResidenceJurisdiction() {
        return residenceJurisdiction;
    }

    public void setResidenceJurisdiction(String residenceJurisdiction) {
        this.residenceJurisdiction = residenceJurisdiction;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getNoResidentReason() {
        return noResidentReason;
    }

    public void setNoResidentReason(String noResidentReason) {
        this.noResidentReason = noResidentReason;
    }

    public String getNoResidentReasonInfo() {
        return noResidentReasonInfo;
    }

    public void setNoResidentReasonInfo(String noResidentReasonInfo) {
        this.noResidentReasonInfo = noResidentReasonInfo;
    }
}
