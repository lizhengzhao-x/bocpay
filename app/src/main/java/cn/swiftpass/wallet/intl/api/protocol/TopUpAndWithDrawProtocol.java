package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class TopUpAndWithDrawProtocol extends BaseProtocol {
    public static final String TAG = TopUpAndWithDrawProtocol.class.getSimpleName();
    /**
     * 支付密码
     */
    String trxAmount;
    /**
     * D 充值 W提现
     */
    String mAction;
    String mOrderNo;

    public TopUpAndWithDrawProtocol(String action, String trxAmount, String orderNo, NetWorkCallbackListener dataCallback) {
        this.trxAmount = trxAmount;
        this.mAction = action;
        this.mOrderNo = orderNo;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/updateSmartInfo";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, mAction);
        if (!TextUtils.isEmpty(mOrderNo)) {
            mBodyParams.put(RequestParams.ORDERNO, mOrderNo);
        }
        mBodyParams.put(RequestParams.TRXAMOUNT, trxAmount);
    }


}
