package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.STwoSelForgetPasswordTypeActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.safekeyboard.SafeKeyNumberBoard;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.PasswordInputView;

/**
 * 设置支付密码第一个界面
 */
public class RegisterSetPaymentPswActivity extends BaseCompatActivity implements PasswordInputView.OnFinishListener {
    @BindView(R.id.password_view)
    PasswordInputView passwordView;
    @BindView(R.id.id_subtitle)
    TextView idSubtitle;

    private int mPageFlow;
    private String walletId;
    private String cardId;
    private String phoneNo;
    private String mAccountType;
    private String cardNo;
    private SafeKeyNumberBoard safeKeyboardLeft;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //软键盘弹出 用户手势滑动的时候 隐藏软键盘
                if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()
                        && !AndroidUtils.inRangeOfView(safeKeyboardLeft.getKeyboardView(), ev)
                        && !AndroidUtils.inRangeOfView(passwordView, ev)) {
                    safeKeyboardLeft.hideKeyboard();
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void init() {

        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        walletId = getIntent().getExtras().getString(Constants.WALLET_ID);
        cardId = getIntent().getExtras().getString(Constants.CARDID);
        phoneNo = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        mAccountType = getIntent().getExtras().getString(Constants.ACCOUNT_TYPE);
        cardNo = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);

        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD || mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //忘记密码
            setToolBarTitle(R.string.IDV_3a_19_1);
            setBackBtnListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AndroidUtils.showConfrimExitOtherDialog(RegisterSetPaymentPswActivity.this, new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            if (CacheManagerInstance.getInstance().isLogin()) {
                                //登录态忘记密码
                                ActivitySkipUtil.startAnotherActivity(getActivity(), STwoSelForgetPasswordTypeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            } else {
                                //登出态忘记密码
                                MyActivityManager.removeAllTaskForForgetPwdWithNoLoginStatus();
                            }
                        }

                    });
                }
            });
        } else if (mPageFlow == Constants.PAGE_FLOW_RESET_PASSWORD) {
            //重设密码
            setToolBarTitle(R.string.setting_payment_rgw);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册设置密码
            setToolBarTitle(R.string.set_payment_psw);
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            //initSDKLinstener();
            setToolBarTitle(R.string.IDV_3_0);
            hideBackIcon();
            setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
            getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    AndroidUtils.showConfrimExitDialog(RegisterSetPaymentPswActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskWithRegister();
                            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                                ChatSDKManager.hide();
                            }
                        }
                    });
                }
            });
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //initSDKLinstener();
            setToolBarTitle(R.string.IDV_3a_19_1);
            hideBackIcon();
            setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
            getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    AndroidUtils.showConfrimExitDialog(RegisterSetPaymentPswActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {

                            if (CacheManagerInstance.getInstance().isLogin()) {
                                //登录态忘记密码
                                MyActivityManager.removeAllTaskForForgetPwdWithLoginStatusMainStack();
                            } else {
                                //登出态忘记密码
                                MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                            }
                        }
                    });
                }
            });
        } else {
            //注册流程
            setToolBarTitle(R.string.IDV_3_0);
        }

        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                initSDKListener();
            }
        }

        idSubtitle.setText(getString(R.string.IDV_3a_17_3) + "\n" + getString(R.string.IDV_3a_17_4));
        AndroidUtils.forbidCopyForEditText(passwordView);
        initListener();
        initKeyBoard();
        passwordView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (passwordView.getText().toString().trim().length() < 6) {
                        showErrorMsgDialog(mContext, getString(R.string.payment_psw_instruction), new OnMsgClickCallBack() {

                            @Override
                            public void onBtnClickListener() {
                                showKeyBoard();
                            }
                        });
                    }
                    return true;
                }
                return false;
            }
        });
        passwordView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (safeKeyboardLeft != null && !safeKeyboardLeft.isShow()) {
                    safeKeyboardLeft.showKeyboard();
                }
            }
        });
    }

    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);
        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_SETPAYMENTPASSCODE);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        passwordView.setText("");
        showKeyBoard();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.hide();
            }
        }
    }

    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        if (mHandler == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (passwordView == null) return;
                passwordView.setFocusable(true);
                passwordView.setFocusableInTouchMode(true);
                passwordView.requestFocus();

            }
        }, 500);
    }

    private void initKeyBoard() {
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.number_keyboard_containor, null);
        safeKeyboardLeft = new SafeKeyNumberBoard(getApplicationContext(), keyboardContainer, passwordView, R.layout.number_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId());
        safeKeyboardLeft.setDelDrawable(this.getResources().getDrawable(R.drawable.icon_del));
    }


    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }
    }

    private void initListener() {
        passwordView.setOnFinishListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
            safeKeyboardLeft.hideKeyboard();
            safeKeyboardLeft = null;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD
                || event.getEventType() == PasswordEventEntity.EVENT_RESET_PWD_SUCCESS) {
            finish();
        }
    }


    @Override
    public void setOnPasswordFinished() {
        if (passwordView.getOriginText().length() == passwordView.getMaxPasswordLength()) {
            if (!AndroidUtils.checkPwdIsLegal(passwordView.getOriginText())) {
                showErrorMsgDialog(mContext, getString(R.string.error_msg_input_restricts), new OnMsgClickCallBack() {


                    @Override
                    public void onBtnClickListener() {
                        passwordView.setText("");
                        showKeyBoard();
                    }
                });
                return;
            }
            String currentPwd = passwordView.getOriginText();
            HashMap<String, Object> mHashMaps = new HashMap<>();
            //重设密码不需要card_number
            if (mPageFlow != Constants.PAGE_FLOW_RESET_PASSWORD) {
                mHashMaps.put(Constants.DATA_CARD_NUMBER, getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER));
            } else {
                //旧的支付密码
                mHashMaps.put(Constants.DATA_OLD_PAY_PWD, getIntent().getExtras().getString(Constants.DATA_OLD_PAY_PWD));
                if (currentPwd.equals(getIntent().getExtras().getString(Constants.DATA_OLD_PAY_PWD))) {
                    showErrorMsgDialog(mContext, getString(R.string.EW035), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            passwordView.setText("");
                            showKeyBoard();
                        }
                    });
                    return;
                }
            }
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                //信用卡 智能账户注册
                mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
                mHashMaps.put(Constants.USE_BIOMETRICAUTH, getIntent().getBooleanExtra(Constants.USE_BIOMETRICAUTH, true));
                if (TextUtils.equals(mAccountType, Constants.ACCOUNT_TYPE_SMART)) {
                    mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO));
                } else {
                    mHashMaps.put(Constants.EXDATE, getIntent().getSerializableExtra(Constants.EXDATE));
                    mHashMaps.put(Constants.DATA_CARD_NUMBER, cardNo);
                }
            } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                //s2用户注册
                mHashMaps.put(Constants.DATA_OLD_PWD, currentPwd);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                ActivitySkipUtil.startAnotherActivity(RegisterSetPaymentPswActivity.this, RegisterConfirmPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                passwordView.setText("");
                return;
            } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                //s2忘记密码
                mHashMaps.put(Constants.DATA_OLD_PWD, currentPwd);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getStringExtra(Constants.DATA_PHONE_NUMBER));
                ActivitySkipUtil.startAnotherActivity(RegisterSetPaymentPswActivity.this, RegisterConfirmPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                passwordView.setText("");
                return;
            } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
                //虚拟卡用户注册
                mHashMaps.put(Constants.DATA_OLD_PWD, currentPwd);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, getIntent().getSerializableExtra(Constants.VIRTUALCARDLISTBEAN));
                mHashMaps.put(Constants.WALLET_ID, getIntent().getStringExtra(Constants.WALLET_ID));
                ActivitySkipUtil.startAnotherActivity(RegisterSetPaymentPswActivity.this, RegisterConfirmPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                passwordView.setText("");
                return;
            } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                //虚拟卡用户忘记密码
                mHashMaps.put(Constants.DATA_OLD_PWD, currentPwd);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getStringExtra(Constants.MOBILE_PHONE));
                mHashMaps.put(Constants.FORGET_PASSWORD_TYPE, getIntent().getExtras().getString(Constants.FORGET_PASSWORD_TYPE));
                ActivitySkipUtil.startAnotherActivity(RegisterSetPaymentPswActivity.this, RegisterConfirmPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                passwordView.setText("");
                return;
            }
            //激活我的账户也需要显示号码 临时处理
            mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO));
            //旧密码
            mHashMaps.put(Constants.DATA_OLD_PWD, currentPwd);
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
            mHashMaps.put(Constants.WALLET_ID, walletId);
            mHashMaps.put(Constants.CARDID, cardId);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
            mHashMaps.put(Constants.ACCOUNT_TYPE, mAccountType);
            ActivitySkipUtil.startAnotherActivity(RegisterSetPaymentPswActivity.this, RegisterConfirmPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            passwordView.setText("");
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_set_payment_psw;
    }


    @OnClick({R.id.tv_payment_psw_nextStep})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_payment_psw_nextStep:
                break;
        }
    }


}
