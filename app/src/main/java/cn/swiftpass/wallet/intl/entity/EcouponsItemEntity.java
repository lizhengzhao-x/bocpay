package cn.swiftpass.wallet.intl.entity;


public class EcouponsItemEntity {

    public static final String ECOUPONSITEMENTITY_TYPE_HORIZONAL = "0";
    public static final String ECOUPONSITEMENTITY_TYPE_VERCITAL = "1";
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    private String desc;
    private String imageUrl;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * 横向布局 还是纵向布局 0 1
     */
    private String type;

}
