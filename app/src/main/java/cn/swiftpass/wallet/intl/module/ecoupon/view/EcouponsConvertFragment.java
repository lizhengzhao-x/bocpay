package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.EcouponsItemEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.GridLinearDivider;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.RedemptionPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.utils.FilterUtils;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcouponsItemHorizontalAdapter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcouponsItemVerticalAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 兑换电子券
 */
public class EcouponsConvertFragment extends BaseFragment<RedemPtionContract.Presenter> implements RedemPtionContract.View {
    private static final int TYPE_RESET_GRADE = -1;
    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    @BindView(R.id.id_left_title)
    TextView idLeftTitle;
    @BindView(R.id.id_jifen_count)
    TextView idJifenCount;
    @BindView(R.id.id_jigen_convert)
    TextView idJigenConvert;
    @BindView(R.id.id_total_grade_text)
    TextView idTotalGradeText;
    @BindView(R.id.id_convert_reset)
    TextView idConvertReset;
    @BindView(R.id.id_total_filter)
    TextView idTotalFilter;
    @BindView(R.id.id_buttom_tab)
    LinearLayout idButtomTab;
    @BindView(R.id.id_filter_view)
    RelativeLayout idFilterView;
    /**
     * 电子券筛选类型
     */
    private List<RedeemableGiftListEntity.EVoucherListBean> mFilterECoupItems;
    /**
     * 控制九宫格显示 还是列表显示
     */
    private GridLayoutManager mGridLayoutManager;
    /**
     * 兑换电子券列表对象
     */
    private RedeemableGiftListEntity mRedeemableGiftList;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    /**
     * 此用户是否能参与优化
     */
    public boolean isDiscount = false;
    private List<RedeemResponseEntity.ResultRedmBean> mRedmeList;
    private GridDivider gridDivider;

    private GridLinearDivider gridLinearDivider;

    @Override
    public void initTitle() {
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    protected RedemPtionContract.Presenter loadPresenter() {
        return new RedemptionPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_ecoupon_view;
    }

    @Override
    protected void initView(View v) {
        mFilterECoupItems = new ArrayList<>();
        mGridLayoutManager = new GridLayoutManager(getActivity(), 1);

        if (getContext() != null) {
            gridDivider = new GridDivider(AndroidUtils.dip2px(getContext(), 20), this.getResources().getColor(R.color.app_white));
            gridLinearDivider = new GridLinearDivider(AndroidUtils.dip2px(getContext(), 20), this.getResources().getColor(R.color.app_white));
        }
        idRecyclerview.setLayoutManager(mGridLayoutManager);
        idRecyclerview.addItemDecoration(gridLinearDivider);
        idRecyclerview.setAdapter(getAdapter(null));
        ((IAdapter<RedeemableGiftListEntity.EVoucherListBean>) idRecyclerview.getAdapter()).setData(mFilterECoupItems);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        idJigenConvert.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                mFmgHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!isSel() || smartRefreshLayout.isRefreshing()) return;
                        if (getParentFragment() instanceof ECouponsManagerFragment) {
                            ECouponsManagerFragment eCouponsManagerFragment = (ECouponsManagerFragment) getParentFragment();
                            if (!eCouponsManagerFragment.isAlreadyVerifyPwd()) {
                                return;
                            }
                        }
                        redeemGiftCheckStock();
                    }
                }, 100);
            }
        });
        idFilterView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                initFilterPopView();
            }
        });
        idConvertReset.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //数据重置
                resetCurrentStatus();
            }
        });
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                idButtomTab.setVisibility(View.GONE);
                mPresenter.getRedeemGiftList();
            }
        });

        autoRefreshList();

        idTotalFilter.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (mRedeemableGiftList == null || mRedeemableGiftList.getBuMaps() == null) return;
                for (int i = 0; i < mRedeemableGiftList.getBuMaps().size(); i++) {
                    mRedeemableGiftList.getBuMaps().get(i).setSel(true);
                }
                filterCurrentEcoupons(mRedeemableGiftList.getBuMaps());
            }
        });
    }

    public void showChangeBtn() {
        if (getParentFragment() instanceof ECouponsManagerFragment) {
            if (mFilterECoupItems == null) return;
            ECouponsManagerFragment eCouponsManagerFragment = (ECouponsManagerFragment) getParentFragment();
            if (eCouponsManagerFragment.getCurrentSelPosition() == 1) return;
            eCouponsManagerFragment.setShowChangeView(mFilterECoupItems.size() != 0);
        }
    }

    public void autoRefreshList() {
        if (smartRefreshLayout == null) return;
        smartRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                idButtomTab.setVisibility(View.GONE);
                mPresenter.getRedeemGiftList();
            }
        });
    }


    /**
     * 重置数据 每一个item选中个数为0
     */
    public void resetCurrentStatus() {
        if (mRedeemableGiftList == null) return;
        for (int i = 0; i < mRedeemableGiftList.getEVoucherList().size(); i++) {
            mRedeemableGiftList.getEVoucherList().get(i).setCurrentSelCnt(0);
        }
        mFilterECoupItems.clear();
        mFilterECoupItems.addAll(mRedeemableGiftList.getEVoucherList());
        idRecyclerview.getAdapter().notifyDataSetChanged();
        updateTotalNeedGrade(TYPE_RESET_GRADE);
    }

    /**
     * 筛选数据
     */
    private void initFilterPopView() {
        if (mRedeemableGiftList == null || mRedeemableGiftList.getBuMaps() == null) return;
        FilterUtils.initFilterPopView(getActivity(), idFilterView, mRedeemableGiftList.getBuMaps(), new FilterUtils.OnFilterConfimListener() {
            @Override
            public void onFilterConfrim(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities) {
                filterCurrentEcoupons(menuFilterEntities);
            }
        });
    }

    public void redeemGiftCheckStock() {
        //电子券兑换开始
        mRedmeList = new ArrayList<>();
        for (int i = 0; i < mFilterECoupItems.size(); i++) {
            if (mFilterECoupItems.get(i).getCurrentSelCnt() != 0) {
                RedeemResponseEntity.ResultRedmBean resultRedmBean = new RedeemResponseEntity.ResultRedmBean();
                resultRedmBean.setAswItemCode(mFilterECoupItems.get(i).getAswItemCode());
                resultRedmBean.setEVoucherQty(mFilterECoupItems.get(i).getCurrentSelCnt() + "");
                resultRedmBean.setGiftCode(mFilterECoupItems.get(i).getGiftCode());
                resultRedmBean.setOrderTp(mFilterECoupItems.get(i).getOrderTp());
                mRedmeList.add(resultRedmBean);
            }
        }
        mPresenter.redeemGiftCheckStock(mRedeemableGiftList.getGiftTp(), mRedmeList);
    }


    /**
     * 过滤筛选的类型
     *
     * @param menuFilterEntities
     */
    private void filterCurrentEcoupons(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities) {
        List<RedeemableGiftListEntity.EVoucherListBean> eVoucherListBeansTotal = mRedeemableGiftList.getEVoucherList();
        mFilterECoupItems.clear();
        for (int i = 0; i < eVoucherListBeansTotal.size(); i++) {
            for (int k = 0; k < menuFilterEntities.size(); k++) {
                if (eVoucherListBeansTotal.get(i).geteVoucherBuCode().equals(menuFilterEntities.get(k).getAswBu()) && menuFilterEntities.get(k).isSel()) {
                    mFilterECoupItems.add(eVoucherListBeansTotal.get(i));
                    break;
                }
            }
        }
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }


    private CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean> getAdapter(final List<RedeemableGiftListEntity.EVoucherListBean> data) {
        return new CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean>(data) {

            @Override
            public Object getItemType(RedeemableGiftListEntity.EVoucherListBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                //列表排版
                if (type.equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL)) {
                    return new EcouponsItemHorizontalAdapter(getContext(), isDiscount, new EcouponSelChangedListener() {
                        @Override
                        public void onItemEcouponCountChanged(int count, int positon) {
                            mFilterECoupItems.get(positon).setCurrentSelCnt(count);
                            updateTotalNeedGrade(positon);
                        }

                        @Override
                        public void onItemCheckMore(int position) {
                            showDetailPop(position);
                        }

                        @Override
                        public void onReachMaxValue() {
                            showErrorMsgDialog(getActivity(), getString(R.string.EC03_4));
                        }

                        @Override
                        public void onShowGradeInFo() {
                            showGradeInfoRules();
                        }

                        @Override
                        public void onEditTextValue(int positon) {
                            editValueWithDialog(positon);
                        }
                    });
                } else {
                    return new EcouponsItemVerticalAdapter(getContext(), isDiscount, new EcouponSelChangedListener() {
                        @Override
                        public void onItemEcouponCountChanged(int count, int positon) {
                            mFilterECoupItems.get(positon).setCurrentSelCnt(count);
                            updateTotalNeedGrade(positon);
                        }

                        @Override
                        public void onItemCheckMore(int position) {
                            showDetailPop(position);
                        }

                        @Override
                        public void onReachMaxValue() {
                            showErrorMsgDialog(getActivity(), getString(R.string.EC03_4));
                        }

                        @Override
                        public void onShowGradeInFo() {
                            showGradeInfoRules();
                        }

                        @Override
                        public void onEditTextValue(int positon) {

                            editValueWithDialog(positon);
                        }
                    });
                }
            }
        };
    }

    private void editValueWithDialog(final int position) {
        int defaultStr = mFilterECoupItems.get(position).getCurrentSelCnt() > 0 ? mFilterECoupItems.get(position).getCurrentSelCnt() : -1;
        final EcouponEditCntDialog ecouponEditCntDialog = new EcouponEditCntDialog(getActivity(), defaultStr, 50, new EcouponEditCntDialog.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (value == -1) return;
                mFilterECoupItems.get(position).setCurrentSelCnt(value);
                idRecyclerview.getAdapter().notifyDataSetChanged();
                updateTotalNeedGrade(position);
            }
        });
        ecouponEditCntDialog.show();
        mFmgHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ecouponEditCntDialog.showKeyboard();
            }
        }, 100);
    }

    private void showGradeInfoRules() {
        IntegralRulePopWindow popWindow = new IntegralRulePopWindow(getActivity(), mRedeemableGiftList.getConfigInfo());
        popWindow.show();
    }

    /**
     * 显示电子券查看详情详情
     *
     * @param position
     */
    private void showDetailPop(int position) {
        RedeemableGiftListEntity.EVoucherListBean eVoucherListBean = mFilterECoupItems.get(position);
        String detail = "";
        for (int i = 0; i < mRedeemableGiftList.getBuMaps().size(); i++) {
            if (mRedeemableGiftList.getBuMaps().get(i).getAswBu().equals(eVoucherListBean.geteVoucherBuCode())) {
                detail = mRedeemableGiftList.getBuMaps().get(i).getBuTc();
                break;
            }
        }
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), eVoucherListBean.getEVoucherDesc(), detail);
        checkEcouponsDetailPop.show();
    }

    /**
     * 电子券 点击+-操作更新所需积分
     */
    private void updateTotalNeedGrade(int changePos) {
        if (changePos != TYPE_RESET_GRADE) {
            int currentSelTypeCnt = 0;
            for (int i = 0; i < mFilterECoupItems.size(); i++) {
                if (mFilterECoupItems.get(i).getCurrentSelCnt() > 0) {
                    currentSelTypeCnt += 1;
                }
            }
            if (currentSelTypeCnt > 5) {
                //积分兑换最多只能是5种
                mFilterECoupItems.get(changePos).setCurrentSelCnt(0);
                idRecyclerview.getAdapter().notifyDataSetChanged();
                showErrorMsgDialog(getContext(), getString(R.string.EC03_4));
                return;
            }
        }

        int cnt = 0;
        for (int i = 0; i < mFilterECoupItems.size(); i++) {
            if (isDiscount) {
                cnt = cnt + mFilterECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mFilterECoupItems.get(i).getEVoucherUnitPrice());
            } else {
                cnt = cnt + mFilterECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mFilterECoupItems.get(i).getEVoucherOriginalPrice());
            }
        }
        idJifenCount.setText(AndroidUtils.formatPrice(Double.valueOf(cnt), false) + "");
        if (cnt > 0) {
            idButtomTab.setVisibility(View.VISIBLE);
            adjustRecyclerView(changePos);
        } else {
            idButtomTab.setVisibility(View.GONE);
        }
    }

    /**
     * @param position RecyclerView position
     * @return void
     * @author shilei.mo
     * @description 调整RecyclerView位置  如果遮挡则滑动
     * @date 2020/4/10 17:13
     */
    private void adjustRecyclerView(int position) {
        if (idRecyclerview.getLayoutManager() == null) {
            return;
        }
        View itemView = idRecyclerview.getLayoutManager().findViewByPosition(position);
        if (itemView == null) {
            return;
        }

        int[] itemLocation = new int[2];
        int[] bottomTabLocation = new int[2];
        itemView.getLocationOnScreen(itemLocation);
        idButtomTab.getLocationOnScreen(bottomTabLocation);
        final int diff = bottomTabLocation[1] - itemLocation[1] - itemView.getHeight();
        if (diff < 0) {
            //设延时解决点击最后一个item 滑动不了
            idButtomTab.postDelayed(new Runnable() {
                @Override
                public void run() {
                    idRecyclerview.smoothScrollBy(0, -diff);
                }
            }, 100);
        }
    }

    /**
     * 解决同事点击减号/验密两个按钮的bug
     *
     * @return
     */
    private boolean isSel() {
        int cnt = 0;
        for (int i = 0; i < mFilterECoupItems.size(); i++) {
            if (isDiscount) {
                cnt = cnt + mFilterECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mFilterECoupItems.get(i).getEVoucherUnitPrice());
            } else {
                cnt = cnt + mFilterECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mFilterECoupItems.get(i).getEVoucherOriginalPrice());
            }
        }
        return cnt > 0;
    }

    public void changeHorizonal() {
        for (int i = 0; i < mFilterECoupItems.size(); i++) {
            String type = mFilterECoupItems.get(i).getType();
            mFilterECoupItems.get(i).setType(type.equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL) ? EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL : EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL);
        }
        if (mFilterECoupItems.size() > 0) {
            if (mFilterECoupItems.get(0).getType().equals(EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL)) {
                mGridLayoutManager.setSpanCount(1);
                idRecyclerview.removeItemDecoration(gridDivider);
                idRecyclerview.addItemDecoration(gridLinearDivider);
            } else {
                idRecyclerview.removeItemDecoration(gridLinearDivider);
                idRecyclerview.addItemDecoration(gridDivider);
                mGridLayoutManager.setSpanCount(2);
            }
        }
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }


    @Override
    public void checkCCardSuccess(CheckCcEntity response) {
        List<RedeemableGiftListEntity.EVoucherListBean> mECoupDescItems = new ArrayList<>();
        for (int i = 0; i < mFilterECoupItems.size(); i++) {
            if (mFilterECoupItems.get(i).getCurrentSelCnt() != 0) {
                RedeemableGiftListEntity.EVoucherListBean resultRedmBean = new RedeemableGiftListEntity.EVoucherListBean();
                resultRedmBean.setAswItemCode(mFilterECoupItems.get(i).getAswItemCode());

                resultRedmBean.setGiftCode(mFilterECoupItems.get(i).getGiftCode());
                resultRedmBean.setEVoucherOriginalPrice(mFilterECoupItems.get(i).getEVoucherOriginalPrice());
                resultRedmBean.setEVoucherUnitPrice(mFilterECoupItems.get(i).getEVoucherUnitPrice());

                resultRedmBean.setEVoucherDesc(mFilterECoupItems.get(i).getEVoucherDesc());
                resultRedmBean.setEVoucherTc(mFilterECoupItems.get(i).getEVoucherTc());
                resultRedmBean.setCouponImgSmall(mFilterECoupItems.get(i).getCouponImgSmall());
                resultRedmBean.setCouponImgLarge(mFilterECoupItems.get(i).getCouponImgLarge());
                resultRedmBean.setMerchantIconUrl(mFilterECoupItems.get(i).getMerchantIconUrl());
                resultRedmBean.setOrderTp(mFilterECoupItems.get(i).getOrderTp());
                //更新 电子券兑换列表的库存数量
                for (int k = 0; k < response.getEVoucherNos().size(); k++) {
                    CheckCcEntity.EVoucherNosBean resultRedmItemBean = response.getEVoucherNos().get(k);
                    if (resultRedmItemBean.getGiftCode().equals(resultRedmBean.getGiftCode())) {
                        resultRedmBean.seteVoucherStock(resultRedmItemBean.getEVoucherStock());
                    }
                }
                resultRedmBean.setCurrentSelCnt(mFilterECoupItems.get(i).getCurrentSelCnt());
                if (!TextUtils.isEmpty(resultRedmBean.geteVoucherStock())) {
                    //如果选择的兑换数量大于库存 选择数量变为库存数量
                    if (resultRedmBean.getCurrentSelCnt() > Integer.valueOf(resultRedmBean.geteVoucherStock())) {
                        resultRedmBean.setCurrentSelCnt(Integer.valueOf(resultRedmBean.geteVoucherStock()));
                    }
                }
                mECoupDescItems.add(resultRedmBean);
            }
        }

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.GIFTTP, mRedeemableGiftList.getGiftTp());
        mHashMaps.put(Constants.USEDISCOUNT, isDiscount);
        mHashMaps.put(Constants.EVOUCHERLISTBEANLIST, mECoupDescItems);
        ActivitySkipUtil.startAnotherActivity(getActivity(), EcouponsConvertDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        //为什么要加这个代码，因为用户在dialog dismiss 到跳转activity 之间 疯狂点击出现异常，为了解决这个问题 就要控制职能页面跳转成功之后页面才能相应事件。
        if (mFmgHandler != null) {
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissDialog();
                }
            }, 200);
        }
    }

    @Override
    public void checkCCardError(String errorCode, String errorMsg) {
        showErrorMsgDialog(getContext(), errorMsg);
        //清楚当前积分查询/验证密码状态
        //resetCurrentStatus();
        if (getParentFragment() instanceof ECouponsManagerFragment) {
            ECouponsManagerFragment eCouponsManagerFragment = (ECouponsManagerFragment) getParentFragment();
            eCouponsManagerFragment.dismissDialog();//取消RedemPtionManagerPresenter中verifyPwd的进度框
        }
    }

    @Override
    public void showRedeemGiftListInfo(String errorCode, String errorMsg, RedeemableGiftListEntity redeemableGiftListEntity) {
        if (!TextUtils.isEmpty(errorMsg)) {
            showErrorMsgDialog(getContext(), errorMsg);
            //停止刷新
            if (smartRefreshLayout != null) {
                smartRefreshLayout.finishRefresh();
            }
            return;
        }
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        } else {
            return;
        }
        if (redeemableGiftListEntity != null) {
            this.mRedeemableGiftList = redeemableGiftListEntity;
            isDiscount = redeemableGiftListEntity.isGiftPnt();
//            isDiscount=true;
            if (redeemableGiftListEntity.getEVoucherList() != null) {
                for (int i = 0; i < redeemableGiftListEntity.getEVoucherList().size(); i++) {
                    redeemableGiftListEntity.getEVoucherList().get(i).setType(isHorinzonal() ? EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_HORIZONAL : EcouponsItemEntity.ECOUPONSITEMENTITY_TYPE_VERCITAL);
                    redeemableGiftListEntity.getEVoucherList().get(i).setCurrentSelCnt(0);
                }
                mFilterECoupItems.clear();
                mFilterECoupItems.addAll(redeemableGiftListEntity.getEVoucherList());
            }
            idRecyclerview.getAdapter().notifyDataSetChanged();
            showChangeBtn();
        } else {
            showErrorMsgDialog(getContext(), errorMsg);
        }
    }


    public boolean isHorinzonal() {
        if (mGridLayoutManager == null) return true;
        return mGridLayoutManager.getSpanCount() == 1;
    }
}
