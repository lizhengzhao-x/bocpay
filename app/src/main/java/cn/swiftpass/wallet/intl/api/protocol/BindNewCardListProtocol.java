package cn.swiftpass.wallet.intl.api.protocol;

import java.util.ArrayList;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class BindNewCardListProtocol extends BaseProtocol {


    private final ArrayList<BindNewCardEntity> selectCardList;
    private final String action;

    public BindNewCardListProtocol(ArrayList<BindNewCardEntity> selectCardList, String actionIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.selectCardList = selectCardList;
        this.action = actionIn;
        mUrl = "api/register/bindBatchCc";

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
        if (selectCardList != null && selectCardList.size() > 0) {
            mBodyParams.put(RequestParams.PAN_LIST, selectCardList);
        }
    }
}
