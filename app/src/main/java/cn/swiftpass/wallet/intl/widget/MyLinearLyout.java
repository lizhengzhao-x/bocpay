package cn.swiftpass.wallet.intl.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

/**
 *
 */
public class MyLinearLyout extends LinearLayout {
    public MyLinearLyout(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public MyLinearLyout(Context context) {
        this(context,null);
    }

    public MyLinearLyout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    public void addViews(List<View> views) {
        for (View view : views) {
            addViewInLayout(view, -1, view.getLayoutParams(), true);
        }
        requestLayout();
        invalidate();
    }



}
