package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/11.
 */
public class EcouponsDetailEntity extends BaseEntity {


    /**
     * itemName : $50 eGift Voucher
     * currAmount : 50.0
     * bU : EGP
     * referenceNo : 9000105179
     * itemCode : 900001
     * qRCode : 9201737312331728948
     * expireDate : 20191231
     * currency : HKD
     * status : 32
     */

    private String itemName;
    private double currAmount;
    private String bU;

    public String getMerchantIconUrl() {
        return merchantIconUrl;
    }

    public void setMerchantIconUrl(String merchantIconUrl) {
        this.merchantIconUrl = merchantIconUrl;
    }

    private String merchantIconUrl;

    public String getbUName() {
        return buName;
    }

    public void setbUName(String bUName) {
        this.buName = bUName;
    }

    private String buName;
    private String referenceNo;
    private String itemCode;
    private String qRCode;
    private String expireDate;
    private String currency;
    private String status;
    private String itemTitle;

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemDetail() {
        return itemDetail;
    }

    public void setItemDetail(String itemDetail) {
        this.itemDetail = itemDetail;
    }

    private String itemDetail;

    public boolean isShowpwd() {
        return showpwd;
    }

    public void setShowpwd(boolean showpwd) {
        this.showpwd = showpwd;
    }

    private boolean showpwd;

    public boolean getGetDetailSuccess() {
        return getDetailSuccess;
    }

    public void setGetDetailSuccess(boolean getDetailSuccess) {
        this.getDetailSuccess = getDetailSuccess;
    }

    private boolean getDetailSuccess = true;

    public String getBuName() {
        return buName;
    }

    public void setBuName(String buName) {
        this.buName = buName;
    }

    public boolean isUsed() {
        if (TextUtils.isEmpty(status)) return false;

        return status.equals("1") || status.equals("35");
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getCurrAmount() {
        return currAmount;
    }

    public void setCurrAmount(double currAmount) {
        this.currAmount = currAmount;
    }

    public String getBU() {
        return bU;
    }

    public void setBU(String bU) {
        this.bU = bU;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getQRCode() {
        return qRCode;
    }

    public void setQRCode(String qRCode) {
        this.qRCode = qRCode;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public void updateParams(EcouponsDetailEntity response) {
        setStatus(response.getStatus());
        setQRCode(response.getQRCode());
        setReferenceNo(response.getReferenceNo());
        setBU(response.getBU());
        setBuName(response.getBuName());
        setbUName(response.getbUName());
        setCurrAmount(response.getCurrAmount());
        setCurrency(response.getCurrency());
        setExpireDate(response.getExpireDate());
        setItemCode(response.getItemCode());
        setItemName(response.getItemName());
        setMerchantIconUrl(response.getMerchantIconUrl());
        setGetDetailSuccess(true);
    }
}
