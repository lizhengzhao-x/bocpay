package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TransferBillEntity;


public class BillListTransferAdapter implements AdapterItem<TransferBillEntity.TransferInfosBean> {

    private TextView id_bill_title;


    private Context mContext;


    public BillListTransferAdapter(Context mContextIn) {
        mContext = mContextIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_bill_title_view;
    }

    @Override
    public void bindViews(View root) {
        id_bill_title = root.findViewById(R.id.id_bill_title);
    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(TransferBillEntity.TransferInfosBean cardItemEntity, int position) {
        id_bill_title.setText(cardItemEntity.getUiTitle());
    }
}
