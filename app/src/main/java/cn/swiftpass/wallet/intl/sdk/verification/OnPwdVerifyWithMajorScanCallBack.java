package cn.swiftpass.wallet.intl.sdk.verification;

public interface OnPwdVerifyWithMajorScanCallBack {

    void onActivityResume();
}
