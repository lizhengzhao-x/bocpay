package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class CreditBindSmartAccountVerifyOtpProtocol extends BaseProtocol {


    String mVerifyCode;
    String mSmartAccLevel;

    public CreditBindSmartAccountVerifyOtpProtocol(String verifyCode, String smartAccLevel, NetWorkCallbackListener dataCallback) {
        mVerifyCode = verifyCode;
        mSmartAccLevel = smartAccLevel;
        mUrl = "api/smartBind/verifyOtp";
        mDataCallback = dataCallback;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
        mBodyParams.put(RequestParams.SMARTACCLEVEL, mSmartAccLevel);
    }

}
