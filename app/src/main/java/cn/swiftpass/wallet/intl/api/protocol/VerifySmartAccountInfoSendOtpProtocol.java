package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


/**
 * 修改我的账户信息 校验otp接口
 */
public class VerifySmartAccountInfoSendOtpProtocol extends BaseProtocol {
    public static final String TAG = VerifySmartAccountInfoSendOtpProtocol.class.getSimpleName();
    /**
     * action
     * A:修改主账号，M:修改限额，增值方式  S:暂停我的账户 R:重新激活 C:注销我的账户 RE:EDDA
     */
    String mAction;
    String verifyCode;

    public VerifySmartAccountInfoSendOtpProtocol(String action, String verifyCode, NetWorkCallbackListener dataCallback) {
        this.mAction = action;
        this.verifyCode = verifyCode;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountFactory/verifyOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, verifyCode);
    }
}
