package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;


public class TransferLiShiFillInfoContract {

    public interface View extends TransferBaseContract.View {
        void getTransferResourceInfoSuccess(TransferResourcesEntity response);

        void getTransferResourceInfoError(String errorCode, String errorMsg);
    }


    public interface Presenter extends TransferBaseContract.Presenter<View> {
        void getTransferResourceInfo();
    }

}
