package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferByTnxIdProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderStatusContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;


public class TransferCrossBorderStatusPresenter extends BasePresenter<TransferCrossBorderStatusContract.View> implements TransferCrossBorderStatusContract.Presenter {


    @Override
    public void getTransferStatus(String tnxId, String orderId) {
        if (getView() != null) {
            getView().showDialogNotCancel();

        }
        new GetTransferByTnxIdProtocol(tnxId, orderId, new NetWorkCallbackListener<TransferCrossBorderStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferStatusError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderStatusEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferStatusSuccess(response);
                }
            }
        }).execute();


    }
}
