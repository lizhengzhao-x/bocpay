package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSelTypeContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferSelTypePresenter;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.AppBarScrollUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ClearEditText;


/**
 * Created by ZhangXinchao on 2019/10/30.
 * 转账优化 选择转账方式界面
 * 1.派利是选择转账/一般转账界面共用
 * 2.派利是底部没有tab
 */
public class TransferSelTypeFragment extends BaseFragment<TransferSelTypeContract.Presenter> implements TransferSelTypeContract.View, GetTransferSelTypeListener {
    private static final String TAG = "TransferSelTypeActivity";
    @BindView(R.id.id_transfer_search_et)
    ClearEditText idTransferSearchEt;

    @BindView(R.id.id_transfer_search_view)
    RelativeLayout idTransferSearchView;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;
    @BindView(R.id.apl_appBarLayout)
    AppBarLayout aplAppBarLayout;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.rly_transfer_eye)
    RelativeLayout rlyTransferEye;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;

    //修改tab字体需要自定要cunstomeView  需要手动设置字体颜色和大小
    private TextView tvContent1, tvContent2, tvContent3;
    private ArrayList<Fragment> mFragments;
    private List<String> tabNames;

    private int dialogPull = 0;
    /**
     * 当前选择转账方式是否是派利是
     */
    private boolean isLiShi = false;
    private RecentlyContactFragment recentlyContactFragment;
    private FrequentContactFragment frequentContactFragment;
    private AddressBookFragment addressBookFragment;


    private boolean isAllowClickEvent = true;
    private MySmartAccountEntity mSmartAccountInfo;
    //需不要隐藏眼睛
    private boolean isHideEye = true;

    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(getString(R.string.TR1_7_7));
        }
    }

    @Override
    protected void initView(View v) {
        EventBus.getDefault().register(this);
        recentlyContactFragment = RecentlyContactFragment.newInstance();
        frequentContactFragment = FrequentContactFragment.newInstance();

        addressBookFragment = AddressBookFragment.newInstance();
        mFragments = new ArrayList<>();
        mFragments.add(recentlyContactFragment);
        mFragments.add(frequentContactFragment);
        mFragments.add(addressBookFragment);
        bindData();

        //修改AppBar 状态 悬浮也可以拖动
        AppBarScrollUtil.setAppBarScroll(aplAppBarLayout);
        getSmartAccountInfo();

        initSmartFresh();

        idTransferSearchEt.setCursorVisible(false);
        idTransferSearchEt.setFocusable(false);
        idTransferSearchEt.setFocusableInTouchMode(false);
        idTransferSearchEt.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                List<ContractListEntity.RecentlyBean> recentlyBeans = mPresenter.dealWithAllList(recentlyContactFragment.getItemCommonCollectionList(), frequentContactFragment.getCollections(), addressBookFragment.getItemCommonCollectionList());
                TransferItemSearchActivity.setRecentlyInfo(recentlyBeans);
                HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
                mHashMapTransferInfo.put(Constants.TRANSFER_IS_LISHI, false);
                ActivitySkipUtil.startAnotherActivity(mActivity, TransferItemSearchActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });


        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });


        recentlyContactFragment.setOnContactListRefresh(new RecentlyContactFragment.ContactListRefresh() {
            @Override
            public void onSuccess() {
                freshEnd();
            }

            @Override
            public void onFail() {
                freshEnd();
            }
        });

        cbToggleSee.setChecked(false);
        hideAccount();

        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));
    }

    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }
        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);
            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }


    private void initSmartFresh() {
        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getSmartAccountInfo();
                dialogPull++;

                if (null == viewPager) {
                    return;
                }

                //刷新最近转账列表
                if (0 == viewPager.getCurrentItem()) {
                    if (recentlyContactFragment != null) {
                        recentlyContactFragment.getContractList();
                        dialogPull++;
                    }
                }

                //刷新常用联络人
                if (1 == viewPager.getCurrentItem()) {
                    EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_LOCAL_CACHE_CONTACT, ""));
                }

                //刷新通讯录
                if (2 == viewPager.getCurrentItem()) {
                    EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_ADDRESS_COLLECTION_STATUS, ""));
                }
            }
        });
    }

    private void freshEnd() {
        dialogPull--;
        if (dialogPull <= 0) {
            dialogPull = 0;
        }
        //弹框也要关闭
        dismissDialog();
        mainRefreshLayout.finishRefresh();
    }


    private void bindData() {
        tabNames = new ArrayList<>();
        tabNames.add(getString(R.string.TR1_2_2));
        tabNames.add(getString(R.string.TR1_2_3));
        tabNames.add(getString(R.string.TF2101_3_7));

        TabLayout.Tab tab1 = tabLayout.newTab();
        tab1.setText(tabNames.get(0));
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());
        tabLayout.addTab(tabLayout.newTab());


        for (int i = 0; i < tabNames.size(); i++) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_transfer_tab, tabLayout, false);

            if (0 == i) {
                tvContent1 = view.findViewById(R.id.tv_content);
                tvContent1.setText(tabNames.get(i));
            }else if(1 == i){
                tvContent2 = view.findViewById(R.id.tv_content);
                tvContent2.setText(tabNames.get(i));
            }else if(2 == i){
                tvContent3 = view.findViewById(R.id.tv_content);
                tvContent3.setText(tabNames.get(i));
            }
            tabLayout.getTabAt(i).setCustomView(view);
        }

        setTabTextColor(0);
        String language = SpUtils.getInstance(mContext).getAppLanguage();
        //如果是英文字体设置 12  中文14
        if (!TextUtils.isEmpty(language) && language.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US)) {
            tvContent1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            tvContent2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
            tvContent3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        }else {
            tvContent1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            tvContent2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            tvContent3.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        }

        viewPager.setOffscreenPageLimit(mFragments.size());
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabSelected: ");
                viewPager.setCurrentItem(tabLayout.getSelectedTabPosition(), true);

                int position = tabLayout.getSelectedTabPosition();
                setTabTextColor(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabUnselected: ");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabReselected: ");
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return tabNames.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });
    }

    /**
     * 设置自定义tab 需要手动设置tab的color
     * @param currentTab
     */
    private void setTabTextColor(int currentTab){
        if(null == tvContent1){
            return;
        }

        tvContent1.setTextColor(getActivity().getColor(R.color.color_FF9B9B9B));
        tvContent2.setTextColor(getActivity().getColor(R.color.color_FF9B9B9B));
        tvContent3.setTextColor(getActivity().getColor(R.color.color_FF9B9B9B));
        if(0 == currentTab){
            tvContent1.setTextColor(getActivity().getColor(R.color.color_FF24272B));
        }else if(1 == currentTab){
            tvContent2.setTextColor(getActivity().getColor(R.color.color_FF24272B));
        }else if(2 == currentTab){
            tvContent3.setTextColor(getActivity().getColor(R.color.color_FF24272B));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.UPDATE_TRANSFER_LIST) {
            //更改昵称 需要刷新最近转账列表
            if (recentlyContactFragment != null) {
                recentlyContactFragment.getContractList();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //搜索界面 如果收藏/取消收藏联系人 需要刷新 AddressBookFrament
        if (mFmgHandler != null && recentlyContactFragment != null) {
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (recentlyContactFragment != null) {
                        frequentContactFragment.updateRecycleView(true);
                        addressBookFragment.updateItemList();
                    }
                }
            }, 200);

            if (null != cbToggleSee) {
                if (isHideEye) {
                    cbToggleSee.setChecked(false);
                } else {
                    isHideEye = true;
                }

            }
        }

    }

    @OnClick({R.id.rly_transfer_type_bocpay, R.id.tv_transfer_top_up, R.id.tv_amount_usable, R.id.tv_balance_title, R.id.tv_balance_tody_title, R.id.tv_amount, R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(),300)) return;
        switch (view.getId()) {
            case R.id.rly_transfer_type_bocpay:
                showSelTransferTypeDialog();
                break;
            case R.id.tv_transfer_top_up:
                TransferUtils.checkTopUpEvent(mSmartAccountInfo, new TransferUtils.RegEddaEventListener() {
                    @Override
                    public void getRegEddaInfo() {
                        if (mPresenter != null) {
                            mPresenter.getRegEddaInfo();
                        }
                    }

                    @Override
                    public Activity getContext() {
                        return getActivity();
                    }

                    @Override
                    public void showErrorDialog(String errorCode, String message) {
                        showErrorMsgDialog(getContext(), message);
                    }
                });
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    String str = new Gson().toJson(mSmartAccountInfo);
                    try {
                        SmartAccountAdjustDailyLimitActivity.startActivity(mActivity, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                    } catch (Exception e) {

                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * 切换 转账方式
     */
    private void showSelTransferTypeDialog() {
        TransferSelectDialogFragment transferSelTypeDialogFragment = new TransferSelectDialogFragment();
        transferSelTypeDialogFragment.setOnTransferSelTypeClickListener(new TransferSelectDialogFragment.OnTransferSelTypeClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onTransferTypeSelListener(int position) {
                switch (position) {
                    case TransferSelectDialogFragment.FPS_BOC:
                        break;
                    case TransferSelectDialogFragment.RED_PACKET:
                        EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_LISHI, ""));
                        break;
                    case TransferSelectDialogFragment.FPS_ID:
                        EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_FPS_ID, ""));
                        break;
                    case TransferSelectDialogFragment.ACCOUNT_NO:
                        EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_BANK, ""));
                        break;
                    case TransferSelectDialogFragment.TYPE_CROSS:
                        EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_CROSS_BORDER_PAYEE, ""));
                        break;

                }
            }
        });
        transferSelTypeDialogFragment.show(getChildFragmentManager(), TransferSelTypeDialogFragment.class.getName() + "");
    }

    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    /**
     * 获取我的账户信息
     */
    private void getSmartAccountInfo() {
        mPresenter.getSmartAccountInfo();
    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        mSmartAccountInfo = response;
        CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);


        if (null != cbToggleSee && cbToggleSee.isChecked()) {
            openAccount();
        } else {
            hideAccount();
        }

        if (isEyeDateNull) {
            cbToggleSee.setChecked(true);
            isEyeDateNull = false;
        }
        freshEnd();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        freshEnd();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_sel_type;
    }



    /**
     * Activity 中 Fragment 需要知道当前操作跳转是到转账界面 还是 派利是界面
     *
     * @return
     */
    @Override
    public boolean isNormalTransferType() {
        return !isLiShi;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected TransferSelTypeContract.Presenter loadPresenter() {
        return new TransferSelTypePresenter();
    }

    /**
     * 很多 例如绑卡/解绑完成之后要退到首页 刷新卡列表等
     *
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }
        }

    }


}
