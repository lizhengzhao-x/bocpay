package cn.swiftpass.wallet.intl.module.register.adapter;

import cn.swiftpass.wallet.intl.entity.TaxInfoReason;

public interface OnClickTaxReasonListener {
    void OnClickTaxReason(TaxInfoReason reasonInfo);
}
