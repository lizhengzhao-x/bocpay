package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class PaymentListEntity extends BaseEntity {


    private List<RowsBean> rows;

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean extends BaseEntity {
        /**
         * isDefault : 1
         * types : [{"isDefault":0,"payType":1},{"isDefault":1,"payType":2}]
         * defaultPayType : 2
         * cardLogo : url
         * phone : 852-99999999
         * cardId : 962
         * cardType : 2
         * panShowNumber : *********9614
         * pan : 012048002365
         * userId : 1916
         */

        private String isDefault;
        private String defaultPayType;
        private String cardLogo;
        private String phone;
        private String cardId;
        private String cardType;
        private String panShowNumber;
        private String pan;
        private String userId;

        public String getCardFaceId() {
            return cardFaceId;
        }

        public void setCardFaceId(String cardFaceId) {
            this.cardFaceId = cardFaceId;
        }

        private String cardFaceId;
        private List<TypesBean> types;

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }

        private String subTitle;

        public String getIsDefault() {
            return isDefault;
        }

        public void setIsDefault(String isDefault) {
            this.isDefault = isDefault;
        }

        public String getDefaultPayType() {
            return defaultPayType;
        }

        public void setDefaultPayType(String defaultPayType) {
            this.defaultPayType = defaultPayType;
        }

        public String getCardLogo() {
            return cardLogo;
        }

        public void setCardLogo(String cardLogo) {
            this.cardLogo = cardLogo;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        public String getPanShowNumber() {
            return panShowNumber;
        }

        public void setPanShowNumber(String panShowNumber) {
            this.panShowNumber = panShowNumber;
        }

        public String getPan() {
            return pan;
        }

        public void setPan(String pan) {
            this.pan = pan;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public List<TypesBean> getTypes() {
            return types;
        }

        public void setTypes(List<TypesBean> types) {
            this.types = types;
        }

        public static class TypesBean extends BaseEntity {
            /**
             * isDefault : 0
             * payType : 1
             */

            private String isDefault;
            private String payType;

            public String getShowTitle() {
                return showTitle;
            }

            public void setShowTitle(String showTitle) {
                this.showTitle = showTitle;
            }

            private String showTitle;

            public String getIsDefault() {
                return isDefault;
            }

            public void setIsDefault(String isDefault) {
                this.isDefault = isDefault;
            }

            public String getPayType() {
                return payType;
            }

            public void setPayType(String payType) {
                this.payType = payType;
            }
        }
    }
}
