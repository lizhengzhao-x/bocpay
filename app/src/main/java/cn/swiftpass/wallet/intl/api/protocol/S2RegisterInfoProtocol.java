package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;

public class S2RegisterInfoProtocol extends BaseProtocol {

    public static final String TAG = ScavengingProtocol.class.getSimpleName();

    S2RegisterInfoEntity mRegisterInfoEntity;


    public S2RegisterInfoProtocol(String type, S2RegisterInfoEntity registerInfoEntity, NetWorkCallbackListener dataCallback) {
        this.mRegisterInfoEntity = registerInfoEntity;
        this.mDataCallback = dataCallback;
        if (RequestParams.REGISTER_STWO_ACCOUNT.equals(type)) {
            mUrl = "api/smartReg/initSmartInfo";
        } else if (RequestParams.FORGET_PWD.equals(type)) {
            mUrl = "api/smartForgot/verifyCertInfo";
        } else {
            mUrl = "api/smartBind/initSmartInfo";
        }
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.EMAIL, mRegisterInfoEntity.getEmail());
        mBodyParams.put(RequestParams.MOBILE, mRegisterInfoEntity.getPhone());
        mBodyParams.put(RequestParams.CHINESSNAME, mRegisterInfoEntity.getChinessName());
        mBodyParams.put(RequestParams.ENGLISHNAME, mRegisterInfoEntity.getEnglishName());
        mBodyParams.put(RequestParams.SEX, mRegisterInfoEntity.getSex());
        mBodyParams.put(RequestParams.BIRTHDAY, mRegisterInfoEntity.getBirthday());
        mBodyParams.put(RequestParams.CERTTYPE, mRegisterInfoEntity.getCertType());
        mBodyParams.put(RequestParams.CERTNO, mRegisterInfoEntity.getCertNo());
        mBodyParams.put(RequestParams.NATIONALCODE, mRegisterInfoEntity.getNationalCode());
        mBodyParams.put(RequestParams.ISSUEDATE, mRegisterInfoEntity.getIssueDate());
    }

}
