package cn.swiftpass.wallet.intl.module.cardmanagement.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.STwoTopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpPreCheckProtocol;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountDrawDetailContract;

public class SmartAccountDrawDetailPresenter  extends BasePresenter<SmartAccountDrawDetailContract.View> implements SmartAccountDrawDetailContract.Presenter {


    @Override
    public void preCheckTopUpWithSmartAccountTwo(String money) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new STwoTopUpAndWithDrawProtocol("WP", money, "", new NetWorkCallbackListener<STwoTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().preCheckTopUpWithSmartAccountTwoFail(errorCode,errorMsg);
                }

            }

            @Override
            public void onSuccess(STwoTopUpEntity response) {
                if (getView()!=null){
                    getView().preCheckTopUpWithSmartAccountTwoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void preCheckTopUpWithSmartAccountThree( String money) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new TopUpPreCheckProtocol("W",  money, new NetWorkCallbackListener<SThreeTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().preCheckTopUpWithSmartAccountThreeFail(errorCode,errorMsg);
                }
            }

            @Override
            public void onSuccess(SThreeTopUpEntity response) {
                if (getView()!=null){
                    getView().preCheckTopUpWithSmartAccountThreeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void topUpWithSmartAccountTwo( String money, String referenceNo) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new STwoTopUpAndWithDrawProtocol("W", money, referenceNo, new NetWorkCallbackListener<STwoTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null) {
                    getView().dismissDialog();
                    getView().topUpWithSmartAccountTwoFail(errorCode,  errorMsg);
                }
            }

            @Override
            public void onSuccess(STwoTopUpEntity response) {
                if (getView()!=null) {
                    getView().dismissDialog();
                    getView().topUpWithSmartAccountTwoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void topUpWithSmartAccountThree( String money, String orderNo) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new TopUpAndWithDrawProtocol("W", money, orderNo, new NetWorkCallbackListener<TopUpResponseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().topUpWithSmartAccountThreeFail(errorCode,  errorMsg);
                }
            }

            @Override
            public void onSuccess(TopUpResponseEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().topUpWithSmartAccountThreeSuccess(response);
                }
            }
        }).execute();
    }
}
