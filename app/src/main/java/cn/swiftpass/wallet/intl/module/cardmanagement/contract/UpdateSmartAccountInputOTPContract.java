package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.base.otp.OTPSendPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPSendView;
import cn.swiftpass.wallet.intl.base.otp.OTPTryPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPTryView;
import cn.swiftpass.wallet.intl.base.otp.OTPVerifyPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPVerifyView;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderOTPContract;

/**
 * 激活账户发送OTP
 */
public class UpdateSmartAccountInputOTPContract {

    public interface View extends OTPSendView<Presenter>, OTPTryView<Presenter>, OTPVerifyView<Presenter> {


    }


    public interface Presenter extends OTPSendPresenter<View>, OTPTryPresenter<View>, OTPVerifyPresenter<View> {


    }

}
