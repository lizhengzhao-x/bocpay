package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.text.TextUtils;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.CrossTransferFirstSendOtpContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.CrossTransferFirstSendPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.view.AbstractSendOTPActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_ORDER_ID;

;

/**
 * 第一次转账需要发送otp
 */
public class CrossTransferFirstSendOtpActivity extends AbstractSendOTPActivity implements CrossTransferFirstSendOtpContract.View {

    private CrossTransferFirstSendOtpContract.Presenter mPresenterDetail;
    public static final String EXTRA_TNXID = "EXTRA_TNXID";
    public static final String EXTRA_MOBILE = "EXTRA_MOBILE";

    @Override
    public void transferConfrimSuccess(ContentEntity response) {
        HashMap<String, Object> param = new HashMap<>();
        param.put(DATA_TRANSFER_ORDER_ID, mTnxId);
        ActivitySkipUtil.startAnotherActivity(CrossTransferFirstSendOtpActivity.this, TransferCrossBorderStatusActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private String mTnxId;

    @Override
    public void transferFirstSenOtpSuccess(ContentEntity response, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void transferFirstSenOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(CrossTransferFirstSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                showRetrySendOtp();
            }
        });
    }

    private void verifyPwdAction() {
        mPresenterDetail.getTransferConfrim(mTnxId);
    }

    /**
     * EVENT_TRANSFER_SUCCESS 转账成功 关闭当前页面
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS || event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
    }


    @Override
    public void transferFirstVerifyOtpSuccess(ContentEntity response) {
        verifyPwdAction();
    }

    @Override
    public void transferFirstVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(CrossTransferFirstSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }


    @Override
    public void transferConfirmError(final String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_TRANFER_NOT_IN_TIME)) {
            showErrorMsgDialog(this, errorMsg, R.string.RP1_6_7, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(CrossTransferFirstSendOtpActivity.this, MainHomeActivity.class);
                }
            });
        } else {
            showErrorMsgDialog(CrossTransferFirstSendOtpActivity.this, errorMsg);
        }
    }


    @Override
    public void init() {
        if (mPresenter instanceof CrossTransferFirstSendOtpContract.Presenter) {
            mPresenterDetail = (CrossTransferFirstSendOtpContract.Presenter) mPresenter;
        }
        mTnxId = getIntent().getExtras().getString(EXTRA_TNXID);
        phoneNo = getIntent().getExtras().getString(EXTRA_MOBILE, "");
        super.init();
        setToolBarTitle(R.string.SMB1_1_4);
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.transferFirstVerifyOtp(mTnxId, getVerifyCode());
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {

        mPresenterDetail.transferFirstSenOtp(mTnxId, isTryAgain);
    }

    @Override
    protected IPresenter createPresenter() {
        return new CrossTransferFirstSendPresenter();
    }


}
