package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.DecimalInputTextWatcher;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferFillInfoContract;
import cn.swiftpass.wallet.intl.module.transfer.entity.FpsCheckEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferFillInfoPresenter;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

/**
 * Created by ZhangXinchao on 2019/10/31.
 * 转账输入信息UI       电邮转账
 */
public class TransferFillInfoActivity extends BaseTransferActivity<TransferFillInfoContract.Presenter> implements TransferFillInfoContract.View {

    private final int CURRENTPAGEDEPENDREQUESTCALLBACKSIZE = 3;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.tv_toggle_see)
    TextView tvToggleSee;
    @BindView(R.id.rly_transfer_eye)
    RelativeLayout rlyTransferEye;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.ll_account_balance)
    LinearLayout llAccountBalance;
    @BindView(R.id.iv_progress)
    ImageView ivProgress;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.id_transfer_user_name)
    TextView idTransferUserName;
    @BindView(R.id.tv_region_code_str)
    TextView tvRegionCodeStr;
    @BindView(R.id.etwd_transfer_phone_num)
    TextView etwdTransferPhoneNum;
    @BindView(R.id.iv_transfer_type_bocpay_img)
    ImageView ivTransferTypeBocpayImg;
    @BindView(R.id.id_transfer_type)
    TextView idTransferType;
    @BindView(R.id.id_sel_transfer_type_arrow)
    ImageView idSelTransferTypeArrow;
    @BindView(R.id.id_transfer_type_bocpay)
    LinearLayout idTransferTypeBocpay;
    @BindView(R.id.id_buttom_view)
    LinearLayout idButtomView;
    @BindView(R.id.id_transfer_bank_name)
    TextView idTransferBankName;
    @BindView(R.id.iv_transfer_bank_edit)
    LinearLayout ivTransferBankEdit;
    @BindView(R.id.id_transfer_type_not_support_tv)
    TextView idTransferTypeNotSupportTv;
    @BindView(R.id.id_transfer_type_not_support_view)
    LinearLayout idTransferTypeNotSupportView;
    @BindView(R.id.id_transfer_money)
    EditTextWithDel idTransferMoney;
    @BindView(R.id.tv_transfer_info_title)
    TextView tvTransferInfoTitle;
    @BindView(R.id.id_transfer_remark)
    EditTextWithDel idTransferRemark;
    @BindView(R.id.id_remark_size)
    TextView idRemarkSize;
    @BindView(R.id.id_transfer_btn_next)
    TextView idTransferBtnNext;
    @BindView(R.id.id_root_view)
    LinearLayout idRootView;
    private int currentRequestCallBackSize;
    @BindView(R.id.id_transfer_fill_info_view)
    LinearLayout idTransferFillInfoView;
    @BindView(R.id.id_sel_bank_view)
    LinearLayout idSelBankView;
    @BindView(R.id.tv_available_amount_flag)
    TextView tvAvailableAmountFlag;
    @BindView(R.id.rl_softkeyboard_view)
    SoftKeyboardTranslationView rlSoftkeyboardView;
    @BindView(R.id.iv_bg_wave)
    ImageView ivBgWave;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;
    @BindView(R.id.lly_transfer_contain_name)
    LinearLayout llyTransferContainName;
    @BindView(R.id.tv_transfer_phone_title)
    TextView tvTransferPhoneTitle;


    @BindView(R.id.id_lin_billno)
    LinearLayout llBillNoView;
    @BindView(R.id.id_transfer_billno)
    EditTextWithDel etBillNo;

    //需不要隐藏眼睛
    private boolean isHideEye = true;
    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;


    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName, boolean isEmail) {
        HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
        mHashMapTransferInfo.put(Constants.DATA_PHONE_NUMBER, phone);
        mHashMapTransferInfo.put(Constants.CHOICE_AREA, area);
        if (!TextUtils.isEmpty(userName)) {
            mHashMapTransferInfo.put(Constants.USER_NAME, userName);
        }
        mHashMapTransferInfo.put(Constants.IS_EMAIL, isEmail);
        ActivitySkipUtil.startAnotherActivity(activity, TransferFillInfoActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName, String relUserName, boolean isEmail) {
        HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
        mHashMapTransferInfo.put(Constants.DATA_PHONE_NUMBER, phone);
        mHashMapTransferInfo.put(Constants.CHOICE_AREA, area);
        mHashMapTransferInfo.put(Constants.USER_NAME, userName);
        mHashMapTransferInfo.put(Constants.IS_EMAIL, isEmail);
        ActivitySkipUtil.startAnotherActivity(activity, TransferFillInfoActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName) {
        startActivityWithPhoneNumber(activity, area, phone, userName, false);
    }

    public static void startActivityWithPhoneNumber(Activity activity, String area, String phone, String userName, String realName) {
        startActivityWithPhoneNumber(activity, area, phone, userName, realName, false);
    }


    @Override
    public void init() {
        idTransferMoney.getEditText().setFilters(new InputFilter[]{new DecimalInputTextWatcher(9, 2)});
//        etBillNo.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(35), new NormalInputFilter(CHARSEQUENCE)});
        etBillNo.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(35)});
        idTransferMoney.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        initSelBankLayout(ivTransferBankEdit, idTransferTypeBocpay, idTransferBtnNext, tvRegionCodeStr, ivSetDailyLimitBalance);
        showDialogNotCancel();
        initOriginalView(llyTransferContainName,tvTransferPhoneTitle,idTransferUserName, etwdTransferPhoneNum, null, tvRegionCodeStr, null, idSelTransferTypeArrow);
        //防止软键盘换车换行
        idTransferRemark.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        AndroidUtils.setLimit(idTransferRemark.getEditText(), idRemarkSize);
        initView();
        setToolBarTitle(getString(R.string.TR1_7_7));
        idTransferBtnNext.setText(getString(R.string.TR1_12_6));


        initTextWatcher();
        updateNextStatus();
        checkRetainBalanceInfo();
        getSmartAccountInfo();

        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });


        rlSoftkeyboardView.initPopView(this, idRootView);
        cbToggleSee.setChecked(false);
        hideAccount();

        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (idTransferBtnNext != null) {
            //防止按钮多次点击
            idTransferBtnNext.setEnabled(true);
        }

        if (null != cbToggleSee) {
            if (isHideEye) {
                cbToggleSee.setChecked(false);
            } else {
                isHideEye = true;
            }

        }

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void initView() {
        initEditTextView(idTransferMoney);
    }

    @Override
    void transferPreCheck() {
        if (llBillNoView.getVisibility() == View.VISIBLE) {
            //商户转账需要输入交易参考编号
            preTransferBocPayAction(tvRegionCodeStr.getText().toString(), etwdTransferPhoneNum.getText().toString(), idTransferRemark.getText().toString(), idTransferMoney.getText(), false, idSelBankView, idTransferBtnNext, "", etBillNo.getEditText().getText().toString());
        } else {
            preTransferBocPayAction(tvRegionCodeStr.getText().toString(), etwdTransferPhoneNum.getText().toString(), idTransferRemark.getText().toString(), idTransferMoney.getText(), false, idSelBankView, idTransferBtnNext, "", null);
        }

    }


    private void initTextWatcher() {
        idTransferMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (llBillNoView.getVisibility() == View.VISIBLE) {
                    //商户转账需要输入交易参考编号
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
                } else {
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, null);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        etBillNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        etwdTransferPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateNextStatus();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick({R.id.tv_transfer_top_up, R.id.tv_balance_title,
            R.id.tv_amount,
            R.id.tv_balance_tody_title,
            R.id.tv_amount_usable, R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(),300)) return;
        switch (view.getId()) {
            case R.id.tv_transfer_top_up:
                TransferUtils.checkTopUpEvent(mSmartAccountInfo, new TransferUtils.RegEddaEventListener() {
                    @Override
                    public void getRegEddaInfo() {
                        if (mPresenter != null) {
                            mPresenter.getRegEddaInfo();
                        }
                    }

                    @Override
                    public Activity getContext() {
                        return getActivity();
                    }

                    @Override
                    public void showErrorDialog(String errorCode, String message) {
                        showErrorMsgDialog(getContext(), message);
                    }
                });
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_BOCPAY_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    SmartAccountAdjustDailyLimitActivity.startActivity(this, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                }
                break;
        }
    }


    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_fill_info;
    }

    @Override
    protected TransferFillInfoContract.Presenter createPresenter() {
        return new TransferFillInfoPresenter();
    }


    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        if (response != null) {
            idRootView.setVisibility(View.VISIBLE);
            mSmartAccountInfo = (MySmartAccountEntity) response;
            CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);
            currentRequestCallBackSize++;
            judgeDismissDialog();
            //这里也重新设置AccountBalanceEntity
            accountBalanceEntity = new AccountBalanceEntity();
            accountBalanceEntity.setBalance(mSmartAccountInfo.getOutavailbal());
            accountBalanceEntity.setCurrency(mSmartAccountInfo.getCurrency());
            if (null != cbToggleSee && cbToggleSee.isChecked()) {
                openAccount();
            } else {
                hideAccount();
            }
            if (isEyeDateNull) {
                cbToggleSee.setChecked(true);
                isEyeDateNull = false;
            }
            if (!idTransferTypeNotSupportView.isShown()){
                if (llBillNoView.getVisibility() == View.VISIBLE) {
                    //商户转账需要输入交易参考编号
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
                } else {
                    dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), tvAvailableAmountFlag, idTransferBtnNext, idSelBankView, null);
                }
            }

        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        getRetainBalanceInfo(null);
        dismissDialog();
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                TransferFillInfoActivity.this.finish();
            }
        });
    }

    @Override
    public void getAccountBalanceInfoSuccess(AccountBalanceEntity response) {
        if (response != null) {
            getRetainBalanceInfo(response);
            currentRequestCallBackSize++;
            judgeDismissDialog();
        }
    }

    @Override
    public void getAccountBalanceInfoError(String errorCode, String errorMsg) {
        getRetainBalanceInfo(null);
        dismissDialog();
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                TransferFillInfoActivity.this.finish();
            }
        });
    }

    @Override
    protected void updateNextStatus() {
        if (!TextUtils.isEmpty(idTransferMoney.getText()) && !TextUtils.isEmpty(etwdTransferPhoneNum.getText())) {
            if (llBillNoView.getVisibility() == View.VISIBLE) {
                //商户转账需要输入交易参考编号
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), null, idTransferBtnNext, idSelBankView, etBillNo.getEditText().getText().toString());
            } else {
                dealWithExceedMaxMoney(idTransferMoney.getEditText().getText().toString(), null, idTransferBtnNext, idSelBankView, null);
            }
            return;
        }
        idTransferBtnNext.setEnabled(false);
        idTransferBtnNext.setBackgroundResource(R.drawable.bg_btn_next_page_disable);
    }


    public void getSmartAccountInfo() {
        idRootView.setVisibility(View.GONE);
        mPresenter.getSmartAccountInfo();
    }

    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }
        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();

        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);

            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }

    @Override
    void checkEditMoney(boolean isRed) {
        if (null == idTransferMoney) {
            return;
        }

        if (isRed) {
            idTransferMoney.setLineRedVisible(true);
            idTransferMoney.setLineVisible(false);
        } else {
            idTransferMoney.setLineRedVisible(false);
            idTransferMoney.setLineVisible(true);
        }
    }

    /**
     * 查询账户余额
     */
    protected void checkRetainBalanceInfo() {
        mPresenter.getAccountBalanceInfo();
    }

    /**
     * 请求成功之后判断是否显示dialog
     */
    private void judgeDismissDialog() {
        if (currentRequestCallBackSize >= CURRENTPAGEDEPENDREQUESTCALLBACKSIZE) {
            dismissDialog();
        }
    }

    void getRetainBalanceInfo(AccountBalanceEntity response) {
        if (null == response) {
            return;
        }

        accountBalanceEntity = response;

        String str = getString(R.string.TF2101_4_8);
        str = str.replace("(HKD)", "");
        tvTransferInfoTitle.setText(str + "(" + accountBalanceEntity.getCurrency() + ")");
    }

    @Override
    void getBankNameResult(String mCurrentBankCodeIn, String mCurrentBankNameIn) {
        mCurrentBankName = mCurrentBankNameIn;
        mCurrentBankCode = mCurrentBankCodeIn;
        idTransferBankName.setText(mCurrentBankName);
        updateNextStatus();
    }

    @Override
    void updateAreaCode(String code) {
        tvRegionCodeStr.setText(code);
        currentAreaStr = code;
        String phoneNumberEnd = currentAreaStr.replace("+", "") + "-" + currentPhoneStr;
        fpsCheckAction(Constants.TRANSFER_SEL_ACCOUNT_PHONE, phoneNumberEnd, true);
    }

    @Override
    void selCurrentTransferType(int position) {
        updateSelTransferType(position, idTransferType, ivTransferTypeBocpayImg, idTransferTypeNotSupportTv, idTransferTypeNotSupportView, idSelBankView, idTransferBtnNext);
        //如果准许转账才显示交易参考号
        if (isMerchantType && !idTransferTypeNotSupportView.isShown()) {
            //如果是商户 需要填写交易参考号
            llBillNoView.setVisibility(View.VISIBLE);
        }else {
            llBillNoView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onFpsCheckResult(FpsCheckEntity response) {
        currentRequestCallBackSize++;
        judgeDismissDialog();
        setDefaultTransferType(response, idTransferType, ivTransferTypeBocpayImg, idSelBankView);
        updateSelTransferType(lastSelTransferType, idTransferType, ivTransferTypeBocpayImg, idTransferTypeNotSupportTv, idTransferTypeNotSupportView, idSelBankView, idTransferBtnNext);
        //如果准许转账才显示交易参考号
        if (isMerchantType && !idTransferTypeNotSupportView.isShown()) {
            //如果是商户 需要填写交易参考号
            llBillNoView.setVisibility(View.VISIBLE);
        }else {
            llBillNoView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onFpsCheckErrorResult(String errorCode, String errorMsg) {
        dismissDialog();
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    protected TransferResourcesEntity.DefBean.BckImgsBean getBackImageBean() {
        return null;
    }

    @Override
    void hideButtomFillInfoView(boolean isHide) {
        idRootView.setVisibility(View.VISIBLE);
        ivBgWave.setVisibility(View.VISIBLE);
        idTransferFillInfoView.setVisibility(isHide ? View.GONE : View.VISIBLE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
        //修改昵称后需要更新昵称
        else if (event.getEventType() == TransferEventEntity.UPDATE_TRANSFER_LIST) {
            currentUserName = event.getEventMessage();
            if (!TextUtils.isEmpty(currentUserName)) {
                idTransferUserName.setText(currentUserName);
                llyTransferContainName.setVisibility(View.VISIBLE);
                tvTransferPhoneTitle.setVisibility(View.INVISIBLE);
            } else {
                llyTransferContainName.setVisibility(View.GONE);
                tvTransferPhoneTitle.setVisibility(View.VISIBLE);
                //idTransferUserName.setText(currentPhoneStr);
            }

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        //支付后要更新
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            checkRetainBalanceInfo();
        }
    }

    /**
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }

        }

    }


}
