package cn.swiftpass.wallet.intl.module.register;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.utils.PhoneUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetTnxIdEntity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.sdk.idv.IdvConstants;
import cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static android.view.View.IMPORTANT_FOR_AUTOFILL_NO;
import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_REGISTER;
import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_RESET;

public class RegisterPaymentAccountDescActivity extends BaseCompatActivity {

    //    @BindView(R.id.id_vitualcard_bind_confirm_mark)
//    ImageView idVitualcardBindConfirmMark;
//    @BindView(R.id.id_top_title)
//    TextView idTopTitle;
    @BindView(R.id.tv_continue)
    TextView tvContinue;
    @BindView(R.id.id_open_title)
    TextView idOpenTitle;
    @BindView(R.id.id_sub_title)
    TextView idSubTitle;

    @BindView(R.id.id_buttom_tv)
    TextView idButtomTv;
    @BindView(R.id.id_ll_phonenumber)
    LinearLayout idLlPhonenumber;
    @BindView(R.id.id_ll_idcard)
    LinearLayout idLlIdcard;
    @BindView(R.id.id_ll_personinfo)
    LinearLayout idLlPersoninfo;
    //    @BindView(R.id.id_term_info)
//    LinearLayout idTermInfo;
    private String mTransactionUniqueID;
    private static final int REQUEST_IDV_SDK = 30000;
    private int eventType;

    /**
     * 判断是否为sdk回调后跳转 如果是  则该条记录不埋点
     */
    private boolean isAnalysis = true;

    /**
     * idv背景图uri
     */
    private String baseMainBackgroundUri;


    @Override
    protected void onStart() {
        isAnalysis = true;
        super.onStart();
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.IDV_2_1);
        //整个注册流程需要一个record_id 包括idv/frp/埋点
        TempSaveHelper.setRegisterRecordId(UUID.randomUUID().toString().replace("-", ""));
        updateOkBackground(tvContinue, false);
        if (getIntent() != null && getIntent().getExtras() != null) {
            eventType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        } else {
            finish();
            return;
        }
        if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //忘记密码
            idLlPhonenumber.setVisibility(View.GONE);
            idLlPersoninfo.setVisibility(View.GONE);
            idOpenTitle.setText(getString(R.string.IDV_3b_2_2));
            idSubTitle.setText(getString(R.string.IDV_3b_2_6));
            updateOkBackground(tvContinue, true);
            setToolBarTitle(R.string.IDV_3b_2_1);
        } else if (eventType == Constants.PAGE_FLOW_BIND_PA) {
            //绑卡
            setToolBarTitle(R.string.IDV_3_1a);
            idLlPhonenumber.setVisibility(View.GONE);
        } else {

        }

        updateOkBackground(tvContinue, true);
    }


    @Override
    public boolean isAnalysisPage() {
        if (eventType == Constants.PAGE_FLOW_BIND_PA || eventType == Constants.PAGE_FLOW_REGISTER_PA) {
            if (isAnalysis) {
                return true;
            }
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = "";
        analysisPageEntity.current_address = PagerConstant.AGREE_TC_PAGE;
        return analysisPageEntity;
    }

    /**
     * idv sdk 需要获取transactionUniqueID
     */
    private void getTnxIdRequest() {
        ApiProtocolImplManager.getInstance().getTnxIdForForgetPwd(this, new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                showErrorMsgDialog(RegisterPaymentAccountDescActivity.this, errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        if (TextUtils.equals(ErrorCode.LOGIN_NEW_DEVICE_FORGETPWD.code, errorCode)) {
                            AndroidUtils.clearMemoryCacheOnly();
                            ActivitySkipUtil.startAnotherActivity(RegisterPaymentAccountDescActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                        } else {
                            finish();
                        }
                    }
                });
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                mTransactionUniqueID = response.getTransactionUniqueID();
                idvSdkAuth();
            }
        });
    }


    private void idvSdkAuth() {
        ApiProtocolImplManager.getInstance().loadIdvCerts(RegisterPaymentAccountDescActivity.this, new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(RegisterPaymentAccountDescActivity.this, errorMsg);
                sendErrorEvent(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(Object response) {


                DataCollectManager.getInstance().sendEventIdvBefore();
                String transactionType = "";
                if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
                    transactionType = TRANSACTION_TYPE_RESET;
                } else {
                    transactionType = TRANSACTION_TYPE_REGISTER;
                }

                baseMainBackgroundUri = SpUtils.getInstance().getBaseBackground();
                IdvTransitionActivity.startActivityForResult(
                        RegisterPaymentAccountDescActivity.this,
                        mTransactionUniqueID,
                        transactionType,
                        AndroidUtils.getIdvLanguage(RegisterPaymentAccountDescActivity.this),
                        PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME,
                        TempSaveHelper.getRegisterRecordId(),
                        BuildConfig.VERSION_NAME,
                        REQUEST_IDV_SDK,
                        baseMainBackgroundUri,
                        Constants.BASEMAINBACKGROUNDRESID);


//                if (BuildConfig.DOWNLOAD_CER_SERVER) {
//                    HashMap<String, Object> mHashMaps = new HashMap<>();
//                    mHashMaps.put(Constants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
//                    if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                        mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_RESET);
//                    } else {
//                        mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_REGISTER);
//                    }
//                    ActivitySkipUtil.startAnotherActivityForResult(RegisterPaymentAccountDescActivity.this, IdvTransitionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_IDV_SDK);
//
//                } else {
//                    HashMap<String, Object> mHashMaps = new HashMap<>();
//                    mHashMaps.put(Constants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
//                    if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                        mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_RESET);
//                    } else {
//                        mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_REGISTER);
//                    }
//                    ActivitySkipUtil.startAnotherActivityForResult(RegisterPaymentAccountDescActivity.this, IdvTransitionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_IDV_SDK);
//                }
            }
        });

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_prepare_bcc;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @OnClick(R.id.tv_continue)
    public void onClick() {
        if (ButtonUtils.isFastDoubleClick()) {
            return;
        }
        if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            PermissionInstance.getInstance().getPermission(RegisterPaymentAccountDescActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    getTnxIdRequest();
                }

                @Override
                public void rejectPermission() {
                    showOpenCameraDialog();
                }
            }, Manifest.permission.CAMERA);

        } else if (eventType == Constants.PAGE_FLOW_REGISTER_PA) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
            ActivitySkipUtil.startAnotherActivity(RegisterPaymentAccountDescActivity.this, STwoRegisterByEmailAndPhoneActiviy.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (eventType == Constants.PAGE_FLOW_BIND_PA) {
            PermissionInstance.getInstance().getPermission(RegisterPaymentAccountDescActivity.this, new PermissionInstance.PermissionCallback() {
                @Override
                public void acceptPermission() {
                    getTnxIdForBindSmartAccountRequest();
                }

                @Override
                public void rejectPermission() {
                    showOpenCameraDialog();
                }
            }, Manifest.permission.CAMERA);
        }

    }


    /**
     * 用户权限弹框选择关闭之后 提示打开权限
     */
    private void showOpenCameraDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(RegisterPaymentAccountDescActivity.this);
        builder.setTitle(getString(R.string.string_camera_use));
        builder.setMessage(getString(R.string.string_turn_on_camera));

        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AndroidUtils.startAppSetting(RegisterPaymentAccountDescActivity.this);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        if (!isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.show();
        }

    }


    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (eventType == Constants.PAGE_FLOW_BIND_PA || eventType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.AGREE_TC_PAGE);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    private void getTnxIdForBindSmartAccountRequest() {
        ApiProtocolImplManager.getInstance().getTnxIdForBindAccount(this, "", new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);
                showErrorMsgDialog(RegisterPaymentAccountDescActivity.this, errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        finish();
                    }
                });
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                mTransactionUniqueID = response.getTransactionUniqueID();
                ApiProtocolImplManager.getInstance().loadIdvCerts(RegisterPaymentAccountDescActivity.this, new NetWorkCallbackListener() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        showErrorMsgDialog(RegisterPaymentAccountDescActivity.this, errorMsg);
                        sendErrorEvent(errorCode, errorMsg);
                    }

                    @Override
                    public void onSuccess(Object response) {

                        DataCollectManager.getInstance().sendEventIdvBefore();
                        String transactionType = "";
                        if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
                            transactionType = TRANSACTION_TYPE_RESET;
                        } else {
                            transactionType = TRANSACTION_TYPE_REGISTER;
                        }

                        baseMainBackgroundUri = SpUtils.getInstance().getBaseBackground();
                        IdvTransitionActivity.startActivityForResult(
                                RegisterPaymentAccountDescActivity.this,
                                mTransactionUniqueID,
                                transactionType,
                                AndroidUtils.getIdvLanguage(RegisterPaymentAccountDescActivity.this),
                                PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME,
                                TempSaveHelper.getRegisterRecordId(),
                                BuildConfig.VERSION_NAME,
                                REQUEST_IDV_SDK,
                                baseMainBackgroundUri,
                                Constants.BASEMAINBACKGROUNDRESID
                                );


//                        if (BuildConfig.DOWNLOAD_CER_SERVER) {
//                            HashMap<String, Object> mHashMaps = new HashMap<>();
//                            mHashMaps.put(Constants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
//                            if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                                mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_RESET);
//                            } else {
//                                mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_REGISTER);
//                            }
//                            ActivitySkipUtil.startAnotherActivityForResult(RegisterPaymentAccountDescActivity.this, IdvTransitionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_IDV_SDK);
//
//                        } else {
//                            HashMap<String, Object> mHashMaps = new HashMap<>();
//                            if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD) {
//                                mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_RESET);
//                            } else {
//                                mHashMaps.put(Constants.TRANSACTIONTYPE, TRANSACTION_TYPE_REGISTER);
//                            }
//                            ActivitySkipUtil.startAnotherActivityForResult(RegisterPaymentAccountDescActivity.this, IdvTransitionActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_IDV_SDK);
//                        }

                    }
                });
            }
        });
    }


    /**
     * 获取ocr信息
     */
    private void getOcrInfoRequest(final String mIdvFID4, final String mIdvSeqNum4) {
        goIdAuthInfoPage(mIdvFID4, mIdvSeqNum4);
    }


    /**
     * 登录态忘记密码 不需要传手机号
     *
     * @param fid
     * @param seq
     */
    private void goIdAuthInfoPage(String fid, String seq) {
        isAnalysis = false;
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, eventType);
        mHashMaps.put(Constants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
        mHashMaps.put(Constants.MIDVSEQNUMBER, seq);
        mHashMaps.put(Constants.MIDVSEQFID, fid);
        if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //非登录态忘记密码 需要传手机号
            if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.MOBILE_PHONE))) {
                mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
            }
        } else if (eventType == Constants.PAGE_FLOW_BIND_PA) {
            //非登录态忘记密码 需要传手机号
            if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER))) {
                mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
                mHashMaps.put(Constants.EXTRA_COUNTRY_CODE, getIntent().getExtras().getString(Constants.EXTRA_COUNTRY_CODE));
            }
        }
        ActivitySkipUtil.startAnotherActivity(getActivity(), IdAuthInfoFillActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        if (eventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA || eventType == Constants.PAGE_FLOW_BIND_PA) {
            finish();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_IDV_SDK) {
            String idvFid = data.getExtras().getString(IdvConstants.IDVFID);
            String idvSequnumber = data.getExtras().getString(IdvConstants.IDVSEQNUM);
            getOcrInfoRequest(idvFid, idvSequnumber);
        } else {
            if (requestCode == REQUEST_IDV_SDK) {
                if (BuildConfig.SKIP_IDV) {
                    String mIdvSequnumber = "dafffffffffff";
                    String mIdvFid = "dafffffffffff";
                    getOcrInfoRequest(mIdvFid, mIdvSequnumber);
                } else {
                    final String errorCode = data.getExtras().getString(IdvConstants.ERROR_CODE);
                    String errorMsg = data.getExtras().getString(IdvConstants.ERROR_MESSAGE);
                    sendErrorEvent(errorCode, errorMsg);
                    AndroidUtils.showIdvCodeEvent(errorCode, RegisterPaymentAccountDescActivity.this, new AndroidUtils.ConfirmErrorCodeClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            isAnalysis = false;
                            finish();
                        }

                        @Override
                        public void onConfirmBtnIngoreErrorCodeClick() {
                            isAnalysis = false;
                            finish();
                        }
                    });

                }

            }
        }
    }


}
