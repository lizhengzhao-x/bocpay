package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ListView;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class CornerListView extends ListView {
    private int width;

    private int height;

    private int radius;
    private Context context;

    public CornerListView(Context contextIn) {
        super(contextIn);
        this.context = contextIn;
    }

    public CornerListView(Context contextIn, AttributeSet attrs) {
        super(contextIn, attrs);
        this.context = contextIn;
    }

    public CornerListView(Context contextIn, AttributeSet attrs, int defStyleAttr) {
        super(contextIn, attrs, defStyleAttr);
        this.context = contextIn;
    }

    public CornerListView(Context contextIn, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(contextIn, attrs, defStyleAttr, defStyleRes);
        this.context = contextIn;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Path path = new Path();
        path.setFillType(Path.FillType.INVERSE_WINDING);
        path.addRoundRect(new RectF(0, getScrollY(), width + getScrollX(), getScrollY() + height), radius, radius, Path.Direction.CW);
        canvas.drawPath(path, createPorterDuffClearPaint());
    }

    private Paint createPorterDuffClearPaint() {
        Paint paint = new Paint();
        paint.setColor(Color.TRANSPARENT);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        return paint;
    }

    @Override
    protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
        width = newWidth;
        height = newHeight;
        radius = AndroidUtils.dip2px(context, 12);
    }
}
