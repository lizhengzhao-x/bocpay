package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 利是记录翻页查询
 */
public class RedPacketRecordsProtocol extends BaseProtocol {
    /**
     * 当前页面  默认为第一页
     */
    private int mCurrentPage = 1;
    /**
     * 每一页数据  默认为20条
     */
    private int mPageSize = 20;

    private String action;

    public RedPacketRecordsProtocol(String action,int mCurrentPage,int mPageSize, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this. mCurrentPage = mCurrentPage;
        this. mPageSize = mPageSize;
        this.action = action;
        mUrl = "api/redpacket/redPacketRecords";
    }

    @Override
    public void packData() {
        super.packData();
        //appVertion 1.0.18_98
        //shaInfo   xxxxxxx
        mBodyParams.put(RequestParams.PAGENUM, mCurrentPage);
        mBodyParams.put(RequestParams.PAGE_SIZE, mPageSize);
        mBodyParams.put(RequestParams.ACTION, action);
    }
}
