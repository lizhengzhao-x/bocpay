package cn.swiftpass.wallet.intl.module.home.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class HomeLifeMenuItemHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_home_life_menu_item)
    public ImageView ivItemImage;
    @BindView(R.id.tv_home_life_menu_item)
    public TextView tvItemName;
    @BindView(R.id.tv_item_right_top)
    public  TextView tvItemRightTop;

    public HomeLifeMenuItemHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
