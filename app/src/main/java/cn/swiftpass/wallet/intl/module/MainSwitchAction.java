package cn.swiftpass.wallet.intl.module;

public interface MainSwitchAction {
    int ACTION_SWITCH_PAGES_TO_BACK_SCAN=101;
    int ACTION_SWITCH_PAGES_TO_STATIC_QR_CODE=102;
}
