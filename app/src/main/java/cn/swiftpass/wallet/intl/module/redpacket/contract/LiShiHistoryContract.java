package cn.swiftpass.wallet.intl.module.redpacket.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.redpacket.entity.LishiHistoryEntity;

/**
 * 利是 历史记录
 */
public class LiShiHistoryContract {

    public interface View extends IView {

        void getLiShiHistorySuccess(LishiHistoryEntity response);

        void getLiShiHistoryError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<LiShiHistoryContract.View> {

        void getLiShiHistory(int pageNum, int pageSize, String action);
    }

}
