package cn.swiftpass.wallet.intl.entity;


import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class GradeEntity extends BaseEntity {

    private String gradePoint;
    private String name;
    public List<SmaGpInfoBean> child = new ArrayList<>();

    public GradeEntity(String gradePoint, String name, List<SmaGpInfoBean> child) {
        this.gradePoint = gradePoint;
        this.name = name;
        this.child = child;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGradePoint() {
        return gradePoint;
    }

    public void setGradePoint(String gradePoint) {
        this.gradePoint = gradePoint;
    }

    public List<SmaGpInfoBean> getChild() {
        return child;
    }

    public void setChild(List<SmaGpInfoBean> child) {
        this.child = child;
    }
}
