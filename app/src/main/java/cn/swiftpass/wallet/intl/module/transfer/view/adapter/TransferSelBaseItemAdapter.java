package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;

/**
 * Created by ZhangXinchao on 2019/10/31.
 */
public class TransferSelBaseItemAdapter implements AdapterItem<ContractListEntity.RecentlyBean> {


    private ImageView mId_contact_head_img;
    private TextView mId_contact_user_name;
    private TextView mId_contact_user_number;
    private ImageView mId_contact_collect_imag;

    public TransferSelBaseItemAdapter(Context context) {
    }


    @Override
    public int getLayoutResId() {
        return R.layout.item_contact_view;
    }

    @Override
    public void bindViews(View root) {
        mId_contact_head_img = (ImageView) root.findViewById(R.id.id_contact_head_img);
        mId_contact_user_name = (TextView) root.findViewById(R.id.id_contact_user_name);
        mId_contact_user_number = (TextView) root.findViewById(R.id.id_contact_user_number);
        mId_contact_collect_imag = (ImageView) root.findViewById(R.id.id_contact_collect_imag);
    }

    @Override
    public void setViews() {
    }

    @Override
    public void handleData(ContractListEntity.RecentlyBean addressBookItemEntity, int position) {
        mId_contact_user_name.setText(addressBookItemEntity.getDisCustName());
        mId_contact_user_number.setText(addressBookItemEntity.getDisPhoneNo());

    }
}
