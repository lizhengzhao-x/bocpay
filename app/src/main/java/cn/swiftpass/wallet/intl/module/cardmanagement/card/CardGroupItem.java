package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BankCardEntity;

class CardGroupItem extends BaseCardItem {
    public final List<CardContentItem> children;

    public CardGroupItem(long id, BankCardEntity bankCardEntity) {
        super(id, bankCardEntity);
        children = new ArrayList<>();
    }
}
