package cn.swiftpass.wallet.intl.module.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.adapter.CommonPagerSnapHelper;
import cn.swiftpass.boc.commonui.base.adapter.TopDividerItemDecoration;
import cn.swiftpass.httpcore.entity.AccountPointDataEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.HomeLifeMenuEntity;
import cn.swiftpass.wallet.intl.entity.HomeLifeSceneAreaCountEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.NewMsgEntity;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkEvent;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountManageActivity;
import cn.swiftpass.wallet.intl.module.home.adapter.HomeBannerAdapter;
import cn.swiftpass.wallet.intl.module.home.constants.HomeLifeMenuItemEnum;
import cn.swiftpass.wallet.intl.module.home.constants.MenuKey;
import cn.swiftpass.wallet.intl.module.home.contract.HomeContract;
import cn.swiftpass.wallet.intl.module.home.presenter.HomePresenter;
import cn.swiftpass.wallet.intl.module.home.utils.BannerUtils;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;

public class HomeMainFragment extends BaseFragment<HomeContract.Presenter> implements HomeContract.View, OnLifeMenuItemClickListener {

    private static final String ACTION_TOGGLE_ACCOUNT = "ACTION_TOGGLE_ACCOUNT";
    private static final String ACTION_TOGGLE_POINT = "ACTION_TOGGLE_POINT";
    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;
    @BindView(R.id.id_tv_more)
    TextView idTvMore;
    @BindView(R.id.ry_main_bottom_banner)
    RecyclerView mainBannerRy;
    @BindView(R.id.iv_toggle_see)
    ImageView ivToggleSee;
    @BindView(R.id.iv_toggle_see_point)
    ImageView ivToggleSeePoint;

    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.iv_progress)
    ImageView ivPorgress;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.tv_point)
    TextView tvPoint;

    @BindView(R.id.tv_register_account)
    TextView tvRegisterAccount;
    @BindView(R.id.ll_account_balance)
    LinearLayout llAccountBalance;
    @BindView(R.id.ll_account_register)
    LinearLayout llAccountRegister;
    @BindView(R.id.ll_point)
    LinearLayout llPoint;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    @BindView(R.id.ll_network_error)
    LinearLayout llNetworkError;
    @BindView(R.id.ll_home_top_up_account)
    LinearLayout llHomeTopUpAccount;
    @BindView(R.id.ry_life_menu)
    RecyclerView ryLifeMenu;
    @BindView(R.id.tv_account_balance_limit_tip)
    TextView tvAccountBalanceLimitTip;
    @BindView(R.id.tv_point_tip)
    TextView tvPointTip;
    @BindView(R.id.ll_account)
    LinearLayout llAccount;
    @BindView(R.id.id_img_cloud_pay)
    ImageView imgCloudPay;
    @BindView(R.id.id_img_fps)
    ImageView imgFps;


    private ArrayList<NewMsgEntity> mPromotionAllBeanList;


    /**
     * 当前页面创建时间
     */
    private long currentPageCreateTime;
    private HomeBannerAdapter homeBannerAdapter;
    //账户是否显示中
    private boolean isShowSmartAccountData;
    //积分是否显示中
    private boolean isShowCreditCardData;

    private boolean ignoreLoadAccount;
    private String verifyPwdAction;
    private AnimationDrawable animationDrawable;


    private boolean initNewMsgListSuccess;
    private boolean initHomeLifeMenuAdapter;
    private HomeLifeMenuAdapter homeLifeMenuAdapter;
    private ArrayList<HomeLifeMenuEntity> lifeMenuData = new ArrayList<>();
    private HomeLifeSceneAreaCountEntity lifeMenuCountData;

    Consumer<MySmartAccountEntity> consumerTopUpAccount = new Consumer<MySmartAccountEntity>() {
        @Override
        public void accept(MySmartAccountEntity item) {
            topUpAccountImp(item);
        }
    };


    @Override
    public void initTitle() {

        if (mActivity != null) {
            if (!initHomeLifeMenuAdapter) {
                initHomeLifeMenuAdapter = true;
            } else {
                //防止生活场景区出现空白
                if (ryLifeMenu.getAdapter() == null) {
                    bindLifeMenu();
                } else {
                    refreshLifeMenu(null);
                }
            }
            ivToggleSee.setSelected(false);
            //因为切换时，需要重置initTitle，但是却不能重置是否需要验密的标记位
            if (!ignoreLoadAccount) {
                llLoading.setVisibility(View.GONE);
                llNetworkError.setVisibility(View.GONE);
                hideAccountData(false);
            }
            if (mFmgHandler != null) {
                mFmgHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mActivity instanceof MainHomeActivity) {
                            MainHomeActivity activity = (MainHomeActivity) getActivity();
                            activity.getToolBarIconMain().setImageResource(R.mipmap.logo_boc_red);
                        }
                        View rightView = LayoutInflater.from(mContext).inflate(R.layout.view_home_account, null);
                        mActivity.setToolBarRightViewToView(rightView);
                        mActivity.getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT, ""));
                            }
                        });
                        //toolBar绘制完成，通知主页，然后主页进行fragment切换
                        EventBus.getDefault().postSticky(new DeepLinkEvent(DeepLinkEvent.DEEP_LINK, "title draw over"));
                    }
                }, 100);
            }

        }
    }

    private void bindLifeMenu() {
        if (homeLifeMenuAdapter != null) {
            homeBannerAdapter = null;
        }
        ryLifeMenu.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        homeLifeMenuAdapter = new HomeLifeMenuAdapter(mContext, this, ryLifeMenu);
        if (TempSaveHelper.getInitBannerUrlParams() != null) {
            lifeMenuData = TempSaveHelper.getInitBannerUrlParams().getLifeMenuData();
            if (lifeMenuData != null) {
                HomeLifeMenuItemEnum.mapMenuToList(mContext, lifeMenuData);
                if (lifeMenuCountData != null) {
                    mapMenuCountByList();
                }
                homeLifeMenuAdapter.setData(lifeMenuData);
            }
        }
        ryLifeMenu.setAdapter(homeLifeMenuAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        //主界面重新可见时，需要重新静默拉取账户余额和积分数据 因为是fragment所以onHiddenChanged也可能被触发
        LogUtils.d("home", "=====onStart");

        //只有没有账户资料区 智能账户数据的时候才显示 错误刷新和页面更新中..
        hideAccountData(false);
        //防止当处于其他fragment时，activity触发onStart时也触发HomeMainFragment的onStart
        if (getActivity() != null && getActivity() instanceof MainHomeActivity) {
            MainHomeActivity mainHomeActivity = (MainHomeActivity) getActivity();
            if (mainHomeActivity.mCurrentFragment instanceof HomeMainFragment) {
                if (CacheManagerInstance.getInstance().isLogin() && CacheManagerInstance.getInstance().isHasSessionId()) {
                    if (!ignoreLoadAccount) {
                        if (CacheManagerInstance.getInstance().isSmartAccount()) {
                            mPresenter.getMySmartAccountInfo(false, null, null);
                        }
//                        mPresenter.queryAccountPointData(false);
                        mPresenter.getLifeSceneAreaCount();
                        if (!initNewMsgListSuccess) {
                            mPresenter.getNewMsgList(true);
                        }
                    }
                }
            }

        }


    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.d("home", "=====onHiddenChanged");
        if (hidden) {   // 不在最前端显示 相当于调用了onPause();
            return;
        } else {  // 在最前端显示 相当于调用了onResume();
            hideAccountData(false);

            if (CacheManagerInstance.getInstance().isLogin() && CacheManagerInstance.getInstance().isHasSessionId()) {
                if (!ignoreLoadAccount) {
                    //获取积分数据
//                    mPresenter.queryAccountPointData(false);
                    //获取账户金额
                    if (CacheManagerInstance.getInstance().isSmartAccount()) {
                        mPresenter.getMySmartAccountInfo(false, null, null);
                    }
                    //获取生活场景区 角标数量
                    mPresenter.getLifeSceneAreaCount();

                    if (!initNewMsgListSuccess) {
                        mPresenter.getNewMsgList(true);
                    }
                }
            }


        }
    }

    @Override
    protected HomeContract.Presenter loadPresenter() {
        return new HomePresenter();
    }


    @Override
    protected boolean notUsedCustomBackground() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_home_main;
    }

    @Override
    protected void initView(View v) {

        currentPageCreateTime = System.currentTimeMillis();
        EventBus.getDefault().register(this);
        addListener();
        bindLifeMenu();
        bindBannerData();


        mPresenter.getNewMsgList(true);

        llAccountBalance.setVisibility(View.GONE);
        llAccountRegister.setVisibility(View.GONE);
        llNetworkError.setVisibility(View.GONE);
        llPoint.setVisibility(View.GONE);
        llLoading.setVisibility(View.GONE);
        if (!NetworkUtil.isNetworkAvailable()) {
            llNetworkError.setVisibility(View.VISIBLE);
        } else {
            showLoading();
        }

        tvAccountBalanceLimitTip.setText(AndroidUtils.addStringColonSpace(getString(R.string.H10_2_2_1)));
        tvPointTip.setText(AndroidUtils.addStringColonSpace(getString(R.string.H10_2_3_1)));

        if (getActivity() != null && AndroidUtils.isSmallPixels(getActivity())) {
            //适配小分辨率手机(小于16 : 9)，防止图标遮挡

            //扩大图标栏占高比  0.28875 -> 0.31
            ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) llAccount.getLayoutParams();
            layoutParams.matchConstraintPercentHeight = 0.31f;
            llAccount.setLayoutParams(layoutParams);

            //云闪付图标高度减小 38dp -> 34dp
            LinearLayout.LayoutParams imgCloudPayParams = (LinearLayout.LayoutParams) imgCloudPay.getLayoutParams();
            imgCloudPayParams.height = AndroidUtils.dip2px(getActivity(), 34);
            imgCloudPay.setLayoutParams(imgCloudPayParams);

            //fps图标高度减小 38dp -> 34dp
            LinearLayout.LayoutParams imgFpsParams = (LinearLayout.LayoutParams) imgFps.getLayoutParams();
            imgFpsParams.height = AndroidUtils.dip2px(getActivity(), 34);
            imgFps.setLayoutParams(imgFpsParams);
        }

    }

    private void mapMenuCountByList() {
        if (lifeMenuData != null && lifeMenuCountData != null) {
            HashMap<String, Integer> lifeMenuMap = lifeMenuCountData.getLifeMenuMap();
            for (HomeLifeMenuEntity lifeMenuDatum : lifeMenuData) {
                if (lifeMenuMap.containsKey(lifeMenuDatum.getMenuKey())) {
                    Integer count = lifeMenuMap.get(lifeMenuDatum.getMenuKey());
                    if (count != null) {
                        lifeMenuDatum.setCount(count);
                    } else {
                        lifeMenuDatum.setCount(0);
                    }
                }
            }
        }
    }

    /**
     * 查询账户数据
     *
     * @param iShow 是否显示蒙层loading转圈
     */
    public void queryAccountData(boolean iShow) {
        //展示UI不同，所以需要区分参数
        if (CacheManagerInstance.getInstance().isSmartAccount()) {
            mPresenter.getMySmartAccountInfo(iShow, null, null);
            //智能账户不能别积分查询接口阻塞
            mPresenter.queryAccountPointData(false);
        } else {
            mPresenter.queryAccountPointData(iShow);
        }
    }

    /**
     * 默认有5个空白的banner
     */
    private void bindBannerData() {
        mPromotionAllBeanList = new ArrayList<>();
        mPromotionAllBeanList.add(new NewMsgEntity(R.mipmap.img_banner_skeleton));
        mPromotionAllBeanList.add(new NewMsgEntity(R.mipmap.img_banner_skeleton));
        mPromotionAllBeanList.add(new NewMsgEntity(R.mipmap.img_banner_skeleton));
        mPromotionAllBeanList.add(new NewMsgEntity(R.mipmap.img_banner_skeleton));
        mPromotionAllBeanList.add(new NewMsgEntity(R.mipmap.img_banner_skeleton));

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        mainBannerRy.setLayoutManager(layoutManager);
        homeBannerAdapter = new HomeBannerAdapter(mContext, new HomeBannerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                BannerUtils.clickBannerEvent(getActivity(), mPromotionAllBeanList.get(position));
            }
        });
        homeBannerAdapter.setmCurrentData(mPromotionAllBeanList);
        TopDividerItemDecoration mDividerItemDecoration = new TopDividerItemDecoration(mContext,
                RecyclerView.HORIZONTAL);
        Drawable itemDividerDrawable = ContextCompat.getDrawable(mContext, R.drawable.item_divider_home_banner);
        if (itemDividerDrawable != null) {
            mDividerItemDecoration.setDrawable(itemDividerDrawable);
        }
        mainBannerRy.addItemDecoration(mDividerItemDecoration);
        mainBannerRy.setAdapter(homeBannerAdapter);
        new CommonPagerSnapHelper().attachToRecyclerView(mainBannerRy);
    }


    private void addListener() {
        idTvMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_FRONT_PAGE_REP, currentPageCreateTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - currentPageCreateTime);
                    sendAnalysisEvent(analysisButtonEntity);
                }
                EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_NEWS_PAGE, ""));
            }
        });
        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                queryAccountData(false);
                if (mActivity != null) {
                    if (mActivity instanceof MainHomeActivity) {
                        MainHomeActivity activity = (MainHomeActivity) mActivity;
                        activity.refreshBackground();
                    }
                }
                mPresenter.refreshLifeMenu();
                mPresenter.getLifeSceneAreaCount();
                mPresenter.getNewMsgList(true);
            }
        });

    }

    private void initAccountData() {
        if (CacheManagerInstance.getInstance().isSmartAccount()) {
            updateUIBySmartAccount(isShowSmartAccountData);
            //防止来回解绑卡，账户级别改变标记未重置
            isShowCreditCardData = false;
        } else {
            updateUIByCreditCard(isShowCreditCardData);
            //防止来回解绑卡，账户级别改变标记未重置
            isShowSmartAccountData = false;
        }
    }


    @Override
    public void queryAccountPointDataSuccess(AccountPointDataEntity response) {

        if (response != null) {
            CacheManagerInstance.getInstance().setAccountPointData(response);
        }
        //如果是信用卡 根据积分接口来判断是否需要结束刷新 智能账户根据账户信息接口来判定是否需要隐藏刷新
        if (CacheManagerInstance.getInstance().isSmartAccount()) {
            initAccountData();
        } else {
            initAccountData();
            if (mainRefreshLayout != null) {
                mainRefreshLayout.finishRefresh();
            }
        }

    }


    @Override
    public void queryAccountPointDataError(String errorCode, String errorMsg, boolean iShow) {
        //如果是信用卡 根据积分接口来判断是否需要结束刷新 智能账户根据账户信息接口来判定是否需要隐藏刷新
        if (CacheManagerInstance.getInstance().isSmartAccount()) {

        } else {
            if (mainRefreshLayout != null) {
                mainRefreshLayout.finishRefresh();
            }
        }

        //如果是静默刷新，不显示错误弹出窗口
        if (iShow) {
            showErrorMsgDialog(mContext, errorMsg);
        }
    }

    @Override
    public void getMySmartAccountInfoSuccess(MySmartAccountEntity response) {
        CacheManagerInstance.getInstance().setMySmartAccountEntity(response);
        //如果是信用卡 根据积分接口来判断是否需要结束刷新 智能账户根据账户信息接口来判定是否需要隐藏刷新
        if (CacheManagerInstance.getInstance().isSmartAccount()) {
            initAccountData();
            if (mainRefreshLayout != null) {
                mainRefreshLayout.finishRefresh();
            }
        } else {
            initAccountData();
        }

    }


    @Override
    public void getMySmartAccountInfoError(String errorCode, String errorMsg, boolean iShow) {
        //如果是信用卡 根据积分接口来判断是否需要结束刷新 智能账户根据账户信息接口来判定是否需要隐藏刷新
        if (CacheManagerInstance.getInstance().getMySmartAccountEntity() != null) {
            if (CacheManagerInstance.getInstance().isSmartAccount()) {
                if (mainRefreshLayout != null) {
                    mainRefreshLayout.finishRefresh();
                }
            } else {

            }
        } else {
            showNetworkError();
        }

    }


    @Override
    public void getNewMsgListSuccess(NewMsgListEntity response) {
        if (response != null && response.getPromotionResults().size() > 0) {
            if (mPromotionAllBeanList == null) {
                mPromotionAllBeanList = new ArrayList<>();
            }

            mPromotionAllBeanList.clear();
            mPromotionAllBeanList.addAll(response.getPromotionResults());
            initNewMsgListSuccess = true;
            if (homeBannerAdapter != null) {
                homeBannerAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * 最新消息报错消息不需要弹出窗口
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getNewMsgListError(String errorCode, String errorMsg) {

    }


    @Override
    public void getLifeSceneAreaCountError(String errorCode, String errorMsg) {

    }

    @Override
    public void getLifeSceneAreaCountSuccess(HomeLifeSceneAreaCountEntity response) {
        if (response != null) {
            lifeMenuCountData = response;
            if (TempSaveHelper.getInitBannerUrlParams() != null) {
                lifeMenuData = TempSaveHelper.getInitBannerUrlParams().getLifeMenuData();
                if (lifeMenuData != null && lifeMenuData.size() > 0) {
                    HomeLifeMenuItemEnum.mapMenuToList(mActivity, lifeMenuData);
                    if (lifeMenuCountData != null) {
                        mapMenuCountByList();
                    }
                    homeLifeMenuAdapter.setData(lifeMenuData);
                    homeLifeMenuAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mySmartAccountEntity.getHideMobile());
            mHashMapsLogin.put(Constants.SMART_ACCOUNT, mySmartAccountEntity);
            mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
            ActivitySkipUtil.startAnotherActivity(mActivity, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        }
    }

    @Override
    public void refreshLifeMenuError(String errorCode, String errorMsg) {

    }

    @Override
    public void refreshLifeMenuSuccess(InitBannerEntity response) {
        if (response != null) {
            TempSaveHelper.setInitBannerUrlParams(response, true);
            refreshLifeMenu(response);
        }
    }

    @Override
    public void checkSetLimitBalanceSuccess(MySmartAccountEntity response) {
        if (response != null) {
            SmartAccountAdjustDailyLimitActivity.startActivity(mActivity, response, response.getHideMobile());
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            hideAccountData(true);
        }

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void checkSetLimitBalanceError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }

    @OnClick({R.id.ll_network_error, R.id.ll_account_balance, R.id.ll_point, R.id.ll_home_top_up_account, R.id.iv_set_daily_limit_balance, R.id.iv_toggle_see_point, R.id.iv_toggle_see})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(), 300)) return;
        switch (view.getId()) {
            case R.id.ll_home_top_up_account:
                topUpAccount();
                break;
            case R.id.iv_toggle_see:
            case R.id.ll_account_balance:
            case R.id.ll_point:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_HOME_EYE, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                if (SystemInitManager.getInstance().isValidShowAccount()) {
                    isShowSmartAccountData = !isShowSmartAccountData;
                    updateUIBySmartAccount(isShowSmartAccountData);
                    //关闭眼睛就不用再调用积分了
                    if (isShowSmartAccountData) {
                        mPresenter.queryAccountPointData(isShowQueryDialog());
                    }
                } else {
                    verifyPwdAction = ACTION_TOGGLE_ACCOUNT;
                    verifyPwdAction();
                }
                break;
            case R.id.iv_toggle_see_point:
                AnalysisButtonEntity analysisButtonEntity2 = new AnalysisButtonEntity(ActionIdConstant.BUT_HOME_EYE, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity2);
                if (SystemInitManager.getInstance().isValidShowAccount()) {
                    isShowCreditCardData = !isShowCreditCardData;
                    updateUIByCreditCard(isShowCreditCardData);
                    if (isShowCreditCardData) {
                        mPresenter.queryAccountPointData(isShowQueryDialog());
                    }
                } else {
                    verifyPwdAction = ACTION_TOGGLE_POINT;
                    verifyPwdAction();
                }
                break;
            case R.id.iv_set_daily_limit_balance:
                mPresenter.checkSetLimitBalance();
                break;
            case R.id.ll_network_error:
                //网络异常的时候，点击需要把首页涉及到的所有接口全部调用一下
                if (!NetworkUtil.isNetworkAvailable()) {
                    //如果没网络状态空转一个loading，恢复network UI
                    showLoading();
                    mFmgHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showNetworkError();
                        }
                    }, 1000);
                } else {
                    reconnectNetwork();
                }

                break;
            default:
                break;
        }
    }

    public void reconnectNetwork() {
        if (mPresenter != null) {
            showLoading();
            mPresenter.getMySmartAccountInfo(false, null, errorCode -> {
                showNetworkError();
            });
            mPresenter.queryAccountPointData(false);
            mPresenter.getLifeSceneAreaCount();
            mPresenter.getNewMsgList(true);
            setIgnoreLoadAccount(false);
        }
    }


    private void topUpAccount() {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            topUpAccountImp(mySmartAccountEntity);
        } else {
            if (mPresenter != null) {
                mPresenter.getMySmartAccountInfo(true, consumerTopUpAccount, null);
            }
        }

    }

    private void topUpAccountImp(MySmartAccountEntity mySmartAccountEntity) {
        if (MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
            TransferUtils.checkTopUpEvent(mySmartAccountEntity, new TransferUtils.RegEddaEventListener() {
                @Override
                public void getRegEddaInfo() {
                    mPresenter.getRegEddaInfo();
                }

                @Override
                public Activity getContext() {
                    return getActivity();
                }

                @Override
                public void showErrorDialog(String errorCode, String message) {
                    showErrorMsgDialog(getContext(), message);
                }
            });
        } else {
            showActiveAccount();
        }
    }

    public void showActiveAccount() {
        AndroidUtils.showTipDialog(mActivity, getString(R.string.AC2101_19_11), getString(R.string.AC2101_19_8),
                getString(R.string.AC2101_19_10), getString(R.string.AC2101_19_9), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (CacheManagerInstance.getInstance().getMySmartAccountEntity() != null) {
                            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                            mHashMapsLogin.put(Constants.CURRENT_BANK_CARD, CacheManagerInstance.getInstance().getMySmartAccountEntity().getBindCard());
                            mHashMapsLogin.put(Constants.CARD_ENTITY, CacheManagerInstance.getInstance().getCardEntities());
                            ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountManageActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                        }
                    }
                });
    }


    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (ACTION_TOGGLE_ACCOUNT.equals(verifyPwdAction)) {
                    SystemInitManager.getInstance().setValidShowAccount(true);
                    isShowSmartAccountData = true;
                    updateUIBySmartAccount(isShowSmartAccountData);
                    if (mPresenter != null) {
                        mPresenter.queryAccountPointData(isShowQueryDialog());
                    }
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                } else if (ACTION_TOGGLE_POINT.equals(verifyPwdAction)) {
                    SystemInitManager.getInstance().setValidShowAccount(true);
                    isShowCreditCardData = true;
                    if (mPresenter != null) {
                        mPresenter.queryAccountPointData(isShowQueryDialog());
                    }
                    updateUIByCreditCard(isShowCreditCardData);
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    /**
     * 更新 智能账户类型的 账户资料区
     *
     * @param isShow
     */
    private void updateUIBySmartAccount(boolean isShow) {
        if (llAccountRegister == null)
            return;
        llAccountRegister.setVisibility(View.GONE);
        llPoint.setVisibility(View.VISIBLE);
        llLoading.setVisibility(View.GONE);
        llNetworkError.setVisibility(View.GONE);
        ivToggleSeePoint.setVisibility(View.GONE);
        llAccountBalance.setVisibility(View.VISIBLE);
        ivSetDailyLimitBalance.setVisibility(View.GONE);
        //如果用户点眼睛需要展示数据的时候（如果隐藏数据，无需这样处理），这时候没有可以加载的数据，那么需要主动触发请求数据
        //isShowSmartAccountData数值的变换只和按钮的点击事件挂钩，以及页面隐藏的时候
        //首页账户和转账账户用同一个MySmartAccountEntity 保持数据的一致
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        AccountPointDataEntity response = CacheManagerInstance.getInstance().getAccountPointData();
        if (mySmartAccountEntity == null && isShowSmartAccountData) {
            mPresenter.getMySmartAccountInfo(true, null, null);
            return;
        }
//        //如果没有积分数据时，发起请求获取积分数据
//        if (response == null) {
//            mPresenter.queryAccountPointData(false);
//        }

        if (!isShowSmartAccountData) {//隐藏数据
            //支付账户/智能账户
            ivToggleSee.setImageResource(R.drawable.icon_eye_close_home);
            ivToggleSeePoint.setImageResource(R.drawable.icon_eye_close_home);
            tvAmount.setVisibility(View.VISIBLE);
            tvPoint.setVisibility(View.VISIBLE);
            tvAmountUsable.setVisibility(View.VISIBLE);
            tvAmount.setText("*****");
            tvPoint.setText("*****");
            tvAmountUsable.setText("*****");
            //隐藏数据时，不应该展示修改限额的 铅笔图标
            ivSetDailyLimitBalance.setVisibility(View.GONE);
        } else {
            try {
                //支付账户/智能账户
                ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
                ivToggleSee.setImageResource(R.drawable.icon_eye_open_home);
                //账户金额报错，显示为*****
                if (response != null && AccountPointDataEntity.ERROR.equals(response.getFlagBalance())) {
                    tvAmount.setText("*****");
                    tvAmountUsable.setText("*****");
                    //弹框内容不为空时，弹出窗口
                    if (!TextUtils.isEmpty(response.getBalanceMsg())) {
                        showErrorMsgDialog(mContext, response.getBalanceMsg());
                    }
                } else {//数据类型正确（不一定内部的数据是正确的，只是代表类型正确）
                    if (null != mySmartAccountEntity) {
                        double price = Double.parseDouble(mySmartAccountEntity.getBalance());
                        //账户冻结时，返回 -1 需要展示 *****
                        if (price < 0) {
                            tvAmount.setText("*****");
                        } else {
                            String newPrice = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mySmartAccountEntity.getBalance()), true);
                            tvAmount.setText(newPrice);
                            if (!TextUtils.isEmpty(mySmartAccountEntity.getOutavailbal())) {
                                double dailyLimitBalance = Double.parseDouble(mySmartAccountEntity.getOutavailbal());
                                String dailyLimitBalanceStr = AndroidUtils.formatPriceWithPoint(dailyLimitBalance, true);
                                //账户冻结时，返回 -1 需要展示 *****
                                if (dailyLimitBalance < 0) {
                                    tvAmountUsable.setText("*****");
                                } else {
                                    tvAmountUsable.setText(dailyLimitBalanceStr);
                                }
                            } else {
                                tvAmountUsable.setText("*****");
                            }
                        }
                    } else {
                        tvAmount.setText("*****");
                        tvAmountUsable.setText("*****");
                    }
                }

                //虽然在智能账户类型下，积分数据后不展示眼睛图标，
                // 但是为了防止账户类型改变时，页面上的图标状态没有改变，需要同步改变积分后面的眼睛状态
                ivToggleSeePoint.setImageResource(R.drawable.icon_eye_open_home);
                if (response != null && AccountPointDataEntity.NORMAL.equals(response.getFlagSumGp())) {
                    double pointSum = Double.parseDouble(response.getSumGp());
                    if (pointSum < 0) {
                        tvPoint.setText("*****");
                    } else {
                        String sumGpStr = AndroidUtils.formatPrice(pointSum, true);
                        tvPoint.setText(sumGpStr);
                    }
                } else if (response != null && AccountPointDataEntity.INVALID.equals(response.getFlagSumGp())) {
                    tvPoint.setText(response.getSumGpMsg());
                } else {
                    tvPoint.setText("*****");
                    if (response != null && !TextUtils.isEmpty(response.getSumGpMsg())) {
                        showErrorMsgDialog(mContext, response.getSumGpMsg());
                    }
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * 更新 信用卡账户类型的 账户资料区
     *
     * @param isShow
     */
    private void updateUIByCreditCard(boolean isShow) {
        llAccountRegister.setVisibility(View.VISIBLE);
        llPoint.setVisibility(View.VISIBLE);
        ivToggleSeePoint.setVisibility(View.VISIBLE);
        llLoading.setVisibility(View.GONE);
        llNetworkError.setVisibility(View.GONE);
        llAccountBalance.setVisibility(View.GONE);
        ivSetDailyLimitBalance.setVisibility(View.GONE);
        tvRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_BIND_CARD_BACK_HOME, ""));
            }
        });
        AccountPointDataEntity response = CacheManagerInstance.getInstance().getAccountPointData();

        if (isShowCreditCardData) {
            if (response == null) {
                mPresenter.queryAccountPointData(true);
            } else {
                ivToggleSeePoint.setImageResource(R.drawable.icon_eye_open_home);
                if (response != null && AccountPointDataEntity.NORMAL.equals(response.getFlagSumGp())) {
                    String sumGpStr = AndroidUtils.formatPrice(Double.parseDouble(response.getSumGp()), true);
                    tvPoint.setText(sumGpStr);
                } else if (response != null && AccountPointDataEntity.INVALID.equals(response.getFlagSumGp())) {
                    tvPoint.setText(response.getSumGpMsg());
                } else {
                    tvPoint.setText("*****");
                    if (response != null && !TextUtils.isEmpty(response.getSumGpMsg())) {
                        showErrorMsgDialog(mContext, response.getSumGpMsg());
                    }
                }
            }
        } else {
            ivToggleSeePoint.setImageResource(R.drawable.icon_eye_close_home);
            tvPoint.setText("*****");
        }
    }

    private boolean isShowQueryDialog() {
        //异步获取积分数据 根据内存中是否有已经有积分来判断是否要加载dialog
        AccountPointDataEntity accountPointDataEntity = CacheManagerInstance.getInstance().getAccountPointData();
        boolean showDialog = accountPointDataEntity == null || AccountPointDataEntity.ERROR.equals(accountPointDataEntity.getFlagBalance());
        return showDialog;
    }


    /**
     * 是否忽略拉取账户资料   目的：自动登录时，防止onStart触发接口
     *
     * @param ignoreLoadAccount
     */
    public void setIgnoreLoadAccount(boolean ignoreLoadAccount) {
        this.ignoreLoadAccount = ignoreLoadAccount;
    }

    /**
     * 隐藏页面账户数据或者积分数据
     */
    public void hideAccountData(boolean resetValid) {

        if (CacheManagerInstance.getInstance().getMySmartAccountEntity() != null) {
            if (CacheManagerInstance.getInstance().isSmartAccount()) {
                //支付账户/智能账户
                isShowSmartAccountData = false;
                updateUIBySmartAccount(isShowSmartAccountData);
                if (resetValid) {
                    SystemInitManager.getInstance().setValidShowAccount(false);
                }
            } else {
                isShowCreditCardData = false;
                updateUIByCreditCard(isShowCreditCardData);

                if (resetValid) {
                    SystemInitManager.getInstance().setValidShowAccount(false);
                }
            }
        } else if (CacheManagerInstance.getInstance().isLogin() && CacheManagerInstance.getInstance().isHasSessionId()) {
            showAccountTypeUI();
        } else {
            SystemInitManager.getInstance().setValidShowAccount(false);
            llNetworkError.setVisibility(View.GONE);
            llLoading.setVisibility(View.GONE);
            llAccountRegister.setVisibility(View.GONE);
            llPoint.setVisibility(View.GONE);
            ivToggleSeePoint.setVisibility(View.GONE);
            ivSetDailyLimitBalance.setVisibility(View.GONE);
            llAccountBalance.setVisibility(View.GONE);
            isShowSmartAccountData = false;
            isShowCreditCardData = false;
            llNetworkError.setVisibility(View.VISIBLE);

            //自动登录接口可能再onStart前发起，也可能是之后，取决于手机性能
            if (getActivity() instanceof MainHomeActivity) {
                MainHomeActivity homeActivity = (MainHomeActivity) getActivity();
                if (homeActivity.needShowLoading) {
                    showLoading();
                }
            }


        }
    }

    /**
     * 展示隐藏状态下的账户资料区
     */
    public void showAccountTypeUI() {
        if (CacheManagerInstance.getInstance().isSmartAccount()) {
            //支付账户/智能账户
            isShowSmartAccountData = false;
            updateUIBySmartAccount(isShowSmartAccountData);
            SystemInitManager.getInstance().setValidShowAccount(false);
        } else {
            isShowCreditCardData = false;
            updateUIByCreditCard(isShowCreditCardData);
            SystemInitManager.getInstance().setValidShowAccount(false);
        }
    }

    public void showNetworkError() {
        llLoading.setVisibility(View.GONE);
        llAccountRegister.setVisibility(View.GONE);
        llPoint.setVisibility(View.GONE);
        ivToggleSeePoint.setVisibility(View.GONE);
        ivSetDailyLimitBalance.setVisibility(View.GONE);
        llAccountBalance.setVisibility(View.GONE);
        if (llNetworkError != null) {
            llNetworkError.setVisibility(View.VISIBLE);
        }
    }

    public void showLoading() {
        llNetworkError.setVisibility(View.GONE);
        llAccountRegister.setVisibility(View.GONE);
        llPoint.setVisibility(View.GONE);
        ivToggleSeePoint.setVisibility(View.GONE);
        ivSetDailyLimitBalance.setVisibility(View.GONE);
        llAccountBalance.setVisibility(View.GONE);
        if (llLoading != null) {
            llLoading.setVisibility(View.VISIBLE);
            //loading 类型展示时，需要显示loading动画
            animationDrawable = (AnimationDrawable) ivPorgress.getBackground();
            animationDrawable.start();
        }
    }

    /**
     * 隐藏账户资料区的loading
     */
    public void hideLoading() {
        if (llLoading != null) {
            llLoading.setVisibility(View.GONE);
            if (animationDrawable != null) {
                animationDrawable.stop();
            }
        }
    }

    /**
     * 刷新生活场景区
     *
     * @param response
     */
    public void refreshLifeMenu(InitBannerEntity response) {
        if (response != null) {
            lifeMenuData = response.getLifeMenuData();
            if (lifeMenuData != null) {
                HomeLifeMenuItemEnum.mapMenuToList(mActivity, lifeMenuData);
                if (lifeMenuCountData != null) {
                    mapMenuCountByList();
                }
                homeLifeMenuAdapter.setData(lifeMenuData);
                homeLifeMenuAdapter.notifyDataSetChanged();
            }
        } else if (TempSaveHelper.getInitBannerUrlParams() != null) {
            lifeMenuData = TempSaveHelper.getInitBannerUrlParams().getLifeMenuData();
            if (lifeMenuData != null) {
                HomeLifeMenuItemEnum.mapMenuToList(mActivity, lifeMenuData);
                if (lifeMenuCountData != null) {
                    mapMenuCountByList();
                }
                homeLifeMenuAdapter.setData(lifeMenuData);
                homeLifeMenuAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public boolean onHomeLifeMenuItemClick(HomeLifeMenuEntity item) {
        if (item != null && !TextUtils.isEmpty(item.getMenuKey())) {
            switch (item.getMenuKey()) {
                case MenuKey.UPLAN:

                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_UPLAN, ""));
                    break;
                case MenuKey.ECOUPON:
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_FRONT_PAGE_E_VOU, currentPageCreateTime, "", PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - currentPageCreateTime);
                    sendAnalysisEvent(analysisButtonEntity);
                    //MainActivity 接收
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ECOUPON, ""));
                    break;
                case MenuKey.NEW_YEAR_RED_POCKET:
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_LISHI, ""));
                    break;
                case MenuKey.PAYMENT_BY_FPS:
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_BY_SCAN, ""));
                    break;
                case MenuKey.INVITE_SHARE:
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_INVITE_SHARE, ""));
                    break;
                case MenuKey.CROSS_TRANSFER:
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_TRANSFER_CROSS_BORDER_PAYEE, ""));
                    break;
                case MenuKey.CREDIT_CARD_REWARD:
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_CREDIT_CARD_REWARD, ""));
                    break;
                default:
                    break;

            }

        }
        return false;
    }

    public void queryLifeSceneAreaCount() {
        mPresenter.getLifeSceneAreaCount();
    }


    @Override
    public void onSaveInstanceState(@NonNull @NotNull Bundle outState) {

    }
}
