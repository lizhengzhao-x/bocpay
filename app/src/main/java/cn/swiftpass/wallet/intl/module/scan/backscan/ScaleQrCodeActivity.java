package cn.swiftpass.wallet.intl.module.scan.backscan;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.utils.QRCodeUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;


public class ScaleQrCodeActivity extends BaseCompatActivity {
    private static final String TAG = ScaleQrCodeActivity.class.getSimpleName();
    @BindView(R.id.iv_code)
    ImageView ivCode;
    private String orderNoMch;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        EventBus.getDefault().register(this);
        orderNoMch = getIntent().getStringExtra(Constants.DATA_QRCODE);
        //设置屏幕亮度最大
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
        initView();
    }

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_scale_qrcode;
    }

    private void initView() {

        try {
            Bitmap bitmapBar = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(orderNoMch, AndroidUtils.dip2px(getActivity(), 300)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_appicon_bocpay), this);
            ivCode.setImageBitmap(bitmapBar);
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME || event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER) {
            finish();
        }
    }

}
