package cn.swiftpass.wallet.intl.module.transfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/10/31.
 */
public class AddressBookItemEntity extends BaseEntity {

    private String phoneNumber;
    private String contractName;
    private int currentType;
    private boolean isCollect;

    public AddressBookItemEntity(ContractTitleEntity contractTitleEntity) {
        this.contractTitleEntity = contractTitleEntity;
    }

    public String getUiType() {
        return uiType;
    }

    public void setUiType(String uiType) {
        this.uiType = uiType;
    }

    /**
     * 主要是搜索界面 区分标题item 还是显示的内容item
     */
    private String uiType;

    public ContractTitleEntity getContractTitleEntity() {
        return contractTitleEntity;
    }

    public void setContractTitleEntity(ContractTitleEntity contractTitleEntity) {
        this.contractTitleEntity = contractTitleEntity;
    }

    private ContractTitleEntity contractTitleEntity;

    public AddressBookItemEntity(String phoneNumber, String contractName) {
        this.phoneNumber = phoneNumber;
        this.contractName = contractName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    public int getCurrentType() {
        return currentType;
    }

    public void setCurrentType(int currentType) {
        this.currentType = currentType;
    }

    public boolean isCollect() {
        return isCollect;
    }

    public void setCollect(boolean collect) {
        isCollect = collect;
    }


}
