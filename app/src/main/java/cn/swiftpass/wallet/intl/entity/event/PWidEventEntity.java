package cn.swiftpass.wallet.intl.entity.event;

public class PWidEventEntity extends BaseEventEntity {

    public static final int EVENT_HOME_PAGE = 6489;

    public PWidEventEntity(int eventType, String message) {
        super(eventType, message);
    }
}
