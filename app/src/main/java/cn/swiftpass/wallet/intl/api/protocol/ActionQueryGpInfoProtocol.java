package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 积分查询
 */

public class ActionQueryGpInfoProtocol extends BaseProtocol {
    String cardId;

    public ActionQueryGpInfoProtocol(String cardIdIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        cardId = cardIdIn;
        mUrl = "api/card/actionQueryGpInfo";
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(cardId)) {
            mBodyParams.put(RequestParams.CARDID, cardId);

        }

    }


}
