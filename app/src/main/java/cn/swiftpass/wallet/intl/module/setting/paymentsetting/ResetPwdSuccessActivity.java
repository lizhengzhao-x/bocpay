package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;


public class ResetPwdSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.tv_card_info_nextPage)
    TextView tvCardInfoNextPage;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.IDV_3a_21_1);

        hideBackIcon();
        tvCardInfoNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    MyActivityManager.removeAllTaskForForgetPwdWithLoginStatus();
//                    EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE,""));
                } else {
                    ActivitySkipUtil.startAnotherActivity(ResetPwdSuccessActivity.this, LoginActivity.class);
                    MyActivityManager.removeAllTaskExcludePreLoginStack();
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_reset_pwd;
    }


}
