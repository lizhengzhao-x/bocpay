package cn.swiftpass.wallet.intl.module.transfer.presenter;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSelTypeContract;

/**
 * 转账选择 控制 是否显示派利是按钮
 */
public class TransferSelTypePresenter extends BasePresenter<TransferSelTypeContract.View> implements TransferSelTypeContract.Presenter {



    @Override
    public void getSmartAccountInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                   // getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                   // getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(response);
                }

            }
        })).execute();
    }



    @Override
    public List<ContractListEntity.RecentlyBean> dealWithAllList(List<ContractListEntity.RecentlyBean> recentlyBeansIn, List<ContactEntity> contactEntitiesIn, List<ContactEntity> frequentCcontactsIn) {

        List<ContractListEntity.RecentlyBean> recentlyBeans = new ArrayList<>();
        List<ContactEntity> contactEntities = new ArrayList<>();
        List<ContactEntity> frequentContacts = new ArrayList<>();
        if (recentlyBeansIn != null) {
            recentlyBeans.addAll(recentlyBeansIn);
        }
        if (contactEntitiesIn != null) {
            contactEntities.addAll(contactEntitiesIn);
        }
        if (frequentCcontactsIn != null) {
            frequentContacts.addAll(frequentCcontactsIn);
        }

        for (int i = 0; i < recentlyBeans.size(); i++) {
            recentlyBeans.get(i).setUiShowType(ContractListEntity.RecentlyBean.UI_RECENTLY);
        }
        if (contactEntities != null) {
            for (int i = 0; i < contactEntities.size(); i++) {
                ContactEntity contactEntity = contactEntities.get(i);
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(ContractListEntity.RecentlyBean.UI_FREQUENT);
                recentlyBean.setContactID(contactEntity.getContactID());
                recentlyBean.setCollect(contactEntity.isCollect());
                recentlyBean.setDisCustName(contactEntity.getUserName());
                if (contactEntity.isEmail()) {
                    recentlyBean.setDisEmail(contactEntity.getNumber());
                    recentlyBean.setEmail(contactEntity.getNumber());
                } else {
                    recentlyBean.setPhoneNo(contactEntity.getNumber());
                    recentlyBean.setDisPhoneNo(contactEntity.getNumber());
                }
                recentlyBean.setEmailType(contactEntity.isEmail() ? "1" : null);
                recentlyBeans.add(0, recentlyBean);
            }
        }

        if (frequentContacts != null) {
            for (int i = 0; i < frequentContacts.size(); i++) {
                ContactEntity contactEntity = frequentContacts.get(i);
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(ContractListEntity.RecentlyBean.UI_ADDRESSBOOK);
                recentlyBean.setContactID(contactEntity.getContactID());
                recentlyBean.setCollect(contactEntity.isCollect());
                recentlyBean.setDisCustName(contactEntity.getUserName());
                if (contactEntity.isEmail()) {
                    recentlyBean.setDisEmail(contactEntity.getNumber());
                    recentlyBean.setEmail(contactEntity.getNumber());
                } else {
                    recentlyBean.setPhoneNo(contactEntity.getNumber());
                    recentlyBean.setDisPhoneNo(contactEntity.getNumber());
                }

                recentlyBean.setEmailType(contactEntity.isEmail() ? "1" : null);
                recentlyBeans.add(recentlyBean);
            }
        }
        return recentlyBeans;
    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }
}
