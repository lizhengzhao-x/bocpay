package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.view
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:42
 * @change
 * @chang time
 * @class describe
 */
public class RecordViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_payee_name)
    TextView tvPayeeName;
    @BindView(R.id.tv_transfer_date)
    TextView tvTransferDate;
    @BindView(R.id.tv_record_amount)
    TextView tvRecordAmount;
    @BindView(R.id.tv_payee_card)
    TextView tvRecordCardNumber;

    public RecordViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
