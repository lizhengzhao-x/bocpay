package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.boc.commonui.base.adapter.util.CommonDividerItemDecoration;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryItemList;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class RewardHistoryActionHolder extends RecyclerView.ViewHolder {
    private final Context context;
    private final CreditCardRewardHistoryInfoAdapter adapter;
    @BindView(R.id.iv_credit_card_banner)
    ImageView ivCreditCardBanner;
    @BindView(R.id.tv_credit_card_msg_title_tip)
    TextView tvCreditCardMsgTitleTip;
    @BindView(R.id.tv_credit_card_msg_tip)
    TextView tvCreditCardMsgTip;
    @BindView(R.id.tv_reward_detail)
    TextView tvRewardDetail;
    @BindView(R.id.ry_reward_history_action)
    RecyclerView ryRewardHistoryAction;

    public RewardHistoryActionHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
        tvRewardDetail.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        ryRewardHistoryAction.setLayoutManager(layoutManager);
        adapter = new CreditCardRewardHistoryInfoAdapter(context);
        ryRewardHistoryAction.setAdapter(adapter);
        CommonDividerItemDecoration itemDecoration = CommonDividerItemDecoration.createVertical(context.getColor(R.color.line_common), AndroidUtils.dip2px(context, 1));
        ryRewardHistoryAction.addItemDecoration(itemDecoration);

    }

    public void bindData(CreditCardRewardHistoryItemList actionItem, OnRewadDetailListener listener) {
        //设置头部banner图片
        RequestOptions options = new RequestOptions()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        GlideApp.with(context)
                .load(actionItem.getBannerUrl())
                .placeholder(R.mipmap.img_placeholder_reward)
                .apply(options)
                .into(ivCreditCardBanner);
        //设置详情跳转
        if (!TextUtils.isEmpty(actionItem.getDetails())) {
            tvRewardDetail.setVisibility(View.VISIBLE);
            tvRewardDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.OnJumpDetail(actionItem.getDetails());

                }
            });
        } else {
            tvRewardDetail.setVisibility(View.INVISIBLE);
        }

        //设置头部banner中的文字说明
        tvCreditCardMsgTitleTip.setText(actionItem.getTitle());
        tvCreditCardMsgTip.setText(actionItem.getContent());
        adapter.setData(actionItem.getRewardsInfo(), actionItem.getRewardsType());
        adapter.notifyDataSetChanged();


    }
}
