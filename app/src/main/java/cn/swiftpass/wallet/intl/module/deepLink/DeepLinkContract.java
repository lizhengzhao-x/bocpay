package cn.swiftpass.wallet.intl.module.deepLink;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.BusinessResultsEntity;

public class DeepLinkContract {


    public interface View extends IView {

        void goTpUPlanSuccess(UplanUrlEntity response);

        void goTpUPlanFailed(String errorCode, String errorMsg);

        void goToUPlanStoreSuccess(UplanUrlEntity response);

        void goToUPlanStoreFailed(String errorCode, String errorMsg);

        void goToRemittancePageSuccess(TransferCrossBorderBaseEntity response);

        void goToRemittancePageFailed(String errorCode, String errorMsg);

        void goToAppointRewardRegisterSuccess(BusinessResultsEntity response);

        void goToAppointRewardRegisterFailed(String errorCode, String errorMsg);
    }


    public interface Presenter<P extends DeepLinkContract.View> extends IPresenter<P> {
        /**
         * 跳转至 UPlan 主页
         */
        void goTpUPlan();

        /**
         * 跳转至 UPlan  指定门店
         */
        void goToUPlanStore(String deepLinkUrl);

        /**
         * 跳转至 跨境汇款
         */
        void goToRemittancePage();

        /**
         * 跳转至 信用卡登记奖赏
         */
        void goToAppointRewardRegister(String deepLinkUrl);
    }


}
