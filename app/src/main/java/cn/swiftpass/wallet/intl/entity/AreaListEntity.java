package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class AreaListEntity extends BaseEntity {
    /**
     * top : [{"countryCode":"+852","searchKey":"zhongguoxianggang","name":"中国香港"},{"countryCode":"+86","searchKey":"zhongguo","name":"中国"},{"countryCode":"+853","searchKey":"zhongguoaomen","name":"中国澳门"}]
     * version :
     * telList : [{"childItems":[{"countryCode":355,"searchKey":"aerbaniya","name":"阿尔巴尼亚"},{"countryCode":213,"searchKey":"aerjiliya","name":"阿尔及利亚"},{"countryCode":93,"searchKey":"afuhan","name":"阿富汗"},{"countryCode":54,"searchKey":"agenting","name":"阿根廷"},{"countryCode":971,"searchKey":"alianqiu","name":"阿联酋"},{"countryCode":297,"searchKey":"aluba（he）","name":"阿鲁巴（菏）"},{"countryCode":968,"searchKey":"aman","name":"阿曼"},{"countryCode":994,"searchKey":"asaibaijiang","name":"阿塞拜疆"},{"countryCode":20,"searchKey":"aiji","name":"埃及"},{"countryCode":251,"searchKey":"aisaiebiya","name":"埃塞俄比亚"},{"countryCode":353,"searchKey":"aierlan","name":"爱尔兰"},{"countryCode":372,"searchKey":"aishaniya","name":"爱沙尼亚"},{"countryCode":376,"searchKey":"andaoer","name":"安道尔"},{"countryCode":244,"searchKey":"angela","name":"安哥拉"},{"countryCode":43,"searchKey":"aodili","name":"奥地利"},{"countryCode":61,"searchKey":"aodaliya","name":"澳大利亚"}],"key":"A"},{"childItems":[{"countryCode":675,"searchKey":"babuyaxinjineiya","name":"巴布亚新几内亚"},{"countryCode":92,"searchKey":"bajisitan","name":"巴基斯坦"},{"countryCode":595,"searchKey":"balagui","name":"巴拉圭"},{"countryCode":970,"searchKey":"balesitan","name":"巴勒斯坦"},{"countryCode":973,"searchKey":"balin","name":"巴林"},{"countryCode":507,"searchKey":"banama","name":"巴拿马"},{"countryCode":55,"searchKey":"baxi","name":"巴西"},{"countryCode":375,"searchKey":"baieluosi","name":"白俄罗斯"},{"countryCode":359,"searchKey":"baojialiya","name":"保加利亚"},{"countryCode":229,"searchKey":"beining","name":"贝宁"},{"countryCode":32,"searchKey":"bilishi","name":"比利时"},{"countryCode":354,"searchKey":"bingdao","name":"冰岛"},{"countryCode":387,"searchKey":"bohei","name":"波黑"},{"countryCode":48,"searchKey":"bolan","name":"波兰"},{"countryCode":591,"searchKey":"boliweiya","name":"玻利维亚"},{"countryCode":501,"searchKey":"bolizi","name":"伯利兹"},{"countryCode":267,"searchKey":"bociwana","name":"博茨瓦纳"},{"countryCode":975,"searchKey":"budan","name":"不丹"},{"countryCode":226,"searchKey":"bujinafasuo","name":"布基纳法索"},{"countryCode":257,"searchKey":"bulongdi","name":"布隆迪"}],"key":"B"},{"childItems":[{"countryCode":850,"searchKey":"chaoxian","name":"朝鲜"},{"countryCode":240,"searchKey":"chidaojineiya","name":"赤道几内亚"}],"key":"C"},{"childItems":[{"countryCode":45,"searchKey":"danmai","name":"丹麦"},{"countryCode":49,"searchKey":"deguo","name":"德国"},{"countryCode":670,"searchKey":"dongdiwen","name":"东帝汶"},{"countryCode":228,"searchKey":"duoge","name":"多哥"}],"key":"D"},{"childItems":[{"countryCode":7,"searchKey":"eluosilianbang","name":"俄罗斯联邦"},{"countryCode":593,"searchKey":"eguaduoer","name":"厄瓜多尔"},{"countryCode":291,"searchKey":"eliteliya","name":"厄立特里亚"}],"key":"E"},{"childItems":[{"countryCode":33,"searchKey":"faguo","name":"法国"},{"countryCode":298,"searchKey":"faluoqundao（dan）","name":"法罗群岛（丹）"},{"countryCode":689,"searchKey":"fashubolinixiya","name":"法属波利尼西亚"},{"countryCode":594,"searchKey":"fashuguiyanei","name":"法属圭亚那"},{"countryCode":379,"searchKey":"fandigang","name":"梵蒂冈"},{"countryCode":63,"searchKey":"feilvbin","name":"菲律宾"},{"countryCode":679,"searchKey":"feiji","name":"斐济"},{"countryCode":358,"searchKey":"fenlan","name":"芬兰"},{"countryCode":238,"searchKey":"fodejiao","name":"佛得角"},{"countryCode":500,"searchKey":"fukelanqundao（maerweinasi）","name":"福克兰群岛（马尔维纳斯）"}],"key":"F"},{"childItems":[{"countryCode":220,"searchKey":"gangbiya","name":"冈比亚"},{"countryCode":242,"searchKey":"gangguo（bu）","name":"刚果（布）"},{"countryCode":243,"searchKey":"gangguo（jin）","name":"刚果（金）"},{"countryCode":57,"searchKey":"gelunbiya","name":"哥伦比亚"},{"countryCode":506,"searchKey":"gesidalijia","name":"哥斯达黎加"},{"countryCode":44,"searchKey":"geenxi","name":"格恩西"},{"countryCode":299,"searchKey":"gelinglan（dan）","name":"格陵兰（丹）"},{"countryCode":995,"searchKey":"gelujiya","name":"格鲁吉亚"},{"countryCode":53,"searchKey":"guba","name":"古巴"},{"countryCode":590,"searchKey":"guadeluopu（fa）","name":"瓜德罗普（法）"},{"countryCode":592,"searchKey":"guiyanei","name":"圭亚那"}],"key":"G"},{"childItems":[{"countryCode":73,"searchKey":"hasakesitan","name":"哈萨克斯坦"},{"countryCode":509,"searchKey":"haidi","name":"海地"},{"countryCode":82,"searchKey":"hanguo","name":"韩国"},{"countryCode":31,"searchKey":"helan","name":"荷兰"},{"countryCode":382,"searchKey":"heishan","name":"黑山"},{"countryCode":504,"searchKey":"hongdoulasi","name":"洪都拉斯"}],"key":"H"},{"childItems":[{"countryCode":686,"searchKey":"jilibasi","name":"基里巴斯"},{"countryCode":253,"searchKey":"jibuti","name":"吉布提"},{"countryCode":996,"searchKey":"jierjisisitan","name":"吉尔吉斯斯坦"},{"countryCode":224,"searchKey":"jineiya","name":"几内亚"},{"countryCode":245,"searchKey":"jineiyabishao","name":"几内亚比绍"},{"countryCode":1,"searchKey":"jianada","name":"加拿大"},{"countryCode":233,"searchKey":"jiana","name":"加纳"},{"countryCode":241,"searchKey":"jiapeng","name":"加蓬"},{"countryCode":855,"searchKey":"jianpuzhai","name":"柬埔寨"},{"countryCode":420,"searchKey":"jieke","name":"捷克"},{"countryCode":263,"searchKey":"jinbabuwei","name":"津巴布韦"}],"key":"J"},{"childItems":[{"countryCode":237,"searchKey":"kamailong","name":"喀麦隆"},{"countryCode":974,"searchKey":"kataer","name":"卡塔尔"},{"countryCode":61,"searchKey":"kekesi（jilin）qundao","name":"科科斯（基林）群岛"},{"countryCode":672,"searchKey":"kekesi（jilin）qundao","name":"科科斯（基林）群岛"},{"countryCode":269,"searchKey":"kemoluo","name":"科摩罗"},{"countryCode":225,"searchKey":"ketediwa","name":"科特迪瓦"},{"countryCode":965,"searchKey":"keweite","name":"科威特"},{"countryCode":385,"searchKey":"keluodiya","name":"克罗地亚"},{"countryCode":254,"searchKey":"kenniya","name":"肯尼亚"},{"countryCode":682,"searchKey":"kukequndao（xin）","name":"库克群岛（新）"},{"countryCode":599,"searchKey":"kulasuo","name":"库拉索"}],"key":"K"},{"childItems":[{"countryCode":371,"searchKey":"latuoweiya","name":"拉脱维亚"},{"countryCode":266,"searchKey":"laisuotuo","name":"莱索托"},{"countryCode":856,"searchKey":"laozhua","name":"老挝"},{"countryCode":961,"searchKey":"libanen","name":"黎巴嫩"},{"countryCode":370,"searchKey":"litaowan","name":"立陶宛"},{"countryCode":231,"searchKey":"libiliya","name":"利比里亚"},{"countryCode":218,"searchKey":"libiya","name":"利比亚"},{"countryCode":423,"searchKey":"liezhidunshideng","name":"列支敦士登"},{"countryCode":262,"searchKey":"liuniwang（fa）","name":"留尼汪（法）"},{"countryCode":352,"searchKey":"lusenbao","name":"卢森堡"},{"countryCode":250,"searchKey":"luwangda","name":"卢旺达"},{"countryCode":40,"searchKey":"luomaniya","name":"罗马尼亚"}],"key":"L"},{"childItems":[{"countryCode":261,"searchKey":"madajiasijia","name":"马达加斯加"},{"countryCode":960,"searchKey":"maerdaifu","name":"马尔代夫"},{"countryCode":356,"searchKey":"maerta","name":"马耳他"},{"countryCode":265,"searchKey":"malawei","name":"马拉维"},{"countryCode":60,"searchKey":"malaixiya","name":"马来西亚"},{"countryCode":223,"searchKey":"mali","name":"马里"},{"countryCode":389,"searchKey":"maqidun","name":"马其顿"},{"countryCode":692,"searchKey":"mashaoerqundao","name":"马绍尔群岛"},{"countryCode":596,"searchKey":"matinike（fa）","name":"马提尼克（法）"},{"countryCode":262,"searchKey":"mayuete","name":"马约特"},{"countryCode":269,"searchKey":"mayuete","name":"马约特"},{"countryCode":230,"searchKey":"maoliqiusi","name":"毛里求斯"},{"countryCode":222,"searchKey":"maolitaniya","name":"毛里塔尼亚"},{"countryCode":1,"searchKey":"meiguo","name":"美国"},{"countryCode":976,"searchKey":"menggu","name":"蒙古"},{"countryCode":880,"searchKey":"mengjialaguo","name":"孟加拉国"},{"countryCode":51,"searchKey":"milu","name":"秘鲁"},{"countryCode":691,"searchKey":"mikeluonixiyalianbang","name":"密克罗尼西亚联邦"},{"countryCode":95,"searchKey":"miandian","name":"缅甸"},{"countryCode":373,"searchKey":"moerduowa","name":"摩尔多瓦"},{"countryCode":212,"searchKey":"moluoge","name":"摩洛哥"},{"countryCode":377,"searchKey":"monage","name":"摩纳哥"},{"countryCode":258,"searchKey":"mosangbike","name":"莫桑比克"},{"countryCode":52,"searchKey":"moxige","name":"墨西哥"}],"key":"M"},{"childItems":[{"countryCode":264,"searchKey":"namibiya","name":"纳米比亚"},{"countryCode":27,"searchKey":"nanfei","name":"南非"},{"countryCode":672,"searchKey":"nanjizhou","name":"南极洲"},{"countryCode":211,"searchKey":"nansudan","name":"南苏丹"},{"countryCode":674,"searchKey":"naolu","name":"瑙鲁"},{"countryCode":977,"searchKey":"niboer","name":"尼泊尔"},{"countryCode":505,"searchKey":"nijialagua","name":"尼加拉瓜"},{"countryCode":227,"searchKey":"nirier","name":"尼日尔"},{"countryCode":234,"searchKey":"niriliya","name":"尼日利亚"},{"countryCode":683,"searchKey":"niuai","name":"纽埃"},{"countryCode":47,"searchKey":"nuowei","name":"挪威"},{"countryCode":672,"searchKey":"nuofukedao","name":"诺福克岛"}],"key":"N"},{"childItems":[{"countryCode":680,"searchKey":"palao","name":"帕劳"},{"countryCode":351,"searchKey":"putaoya","name":"葡萄牙"}],"key":"P"},{"childItems":[{"countryCode":1,"searchKey":"qitabeimeiguojia","name":"其他北美国家"}],"key":"Q"},{"childItems":[{"countryCode":81,"searchKey":"riben","name":"日本"},{"countryCode":46,"searchKey":"ruidian","name":"瑞典"},{"countryCode":41,"searchKey":"ruishi","name":"瑞士"}],"key":"R"},{"childItems":[{"countryCode":503,"searchKey":"saerwaduo","name":"萨尔瓦多"},{"countryCode":685,"searchKey":"samoya","name":"萨摩亚"},{"countryCode":381,"searchKey":"saierweiya","name":"塞尔维亚"},{"countryCode":232,"searchKey":"sailaliang","name":"塞拉利昂"},{"countryCode":221,"searchKey":"saineijiaer","name":"塞内加尔"},{"countryCode":357,"searchKey":"saipulusi","name":"塞浦路斯"},{"countryCode":248,"searchKey":"saisheer","name":"塞舌尔"},{"countryCode":966,"searchKey":"shatealabo","name":"沙特阿拉伯"},{"countryCode":61,"searchKey":"shengdandao","name":"圣诞岛"},{"countryCode":672,"searchKey":"shengdandao","name":"圣诞岛"},{"countryCode":239,"searchKey":"shengduomeihepulinxibi","name":"圣多美和普林西比"},{"countryCode":290,"searchKey":"shenghelena（ying）","name":"圣赫勒拿（英）"},{"countryCode":378,"searchKey":"shengmalinuo","name":"圣马力诺"},{"countryCode":508,"searchKey":"shengpiaierhemikelong（fa）","name":"圣皮埃尔和密克隆（法）"},{"countryCode":94,"searchKey":"sililanka","name":"斯里兰卡"},{"countryCode":421,"searchKey":"siluofake","name":"斯洛伐克"},{"countryCode":386,"searchKey":"siluowenniya","name":"斯洛文尼亚"},{"countryCode":47,"searchKey":"siwaerbadaoheyangmayandao","name":"斯瓦尔巴岛和扬马延岛"},{"countryCode":268,"searchKey":"siweishilan","name":"斯威士兰"},{"countryCode":249,"searchKey":"sudan","name":"苏丹"},{"countryCode":597,"searchKey":"sulinan","name":"苏里南"},{"countryCode":677,"searchKey":"suoluomenqundao","name":"所罗门群岛"},{"countryCode":252,"searchKey":"suomali","name":"索馬里"}],"key":"S"},{"childItems":[{"countryCode":992,"searchKey":"tajikesitan","name":"塔吉克斯坦"},{"countryCode":66,"searchKey":"taiguo","name":"泰国"},{"countryCode":255,"searchKey":"tansangniya","name":"坦桑尼亚"},{"countryCode":676,"searchKey":"tangjia","name":"汤加"},{"countryCode":216,"searchKey":"tunisi","name":"突尼斯"},{"countryCode":688,"searchKey":"tuwalu","name":"图瓦卢"},{"countryCode":90,"searchKey":"tuerqi","name":"土耳其"},{"countryCode":993,"searchKey":"tukumansitan","name":"土库曼斯坦"},{"countryCode":690,"searchKey":"tuokelao（xin）","name":"托克劳（新）"}],"key":"T"},{"childItems":[{"countryCode":681,"searchKey":"walisihefutuna（fa）","name":"瓦利斯和富图纳（法）"},{"countryCode":678,"searchKey":"wanuatu","name":"瓦努阿图"},{"countryCode":502,"searchKey":"weidimala","name":"危地马拉"},{"countryCode":58,"searchKey":"weineiruila","name":"委内瑞拉"},{"countryCode":673,"searchKey":"wenlai","name":"文莱"},{"countryCode":256,"searchKey":"wuganda","name":"乌干达"},{"countryCode":380,"searchKey":"wukelan","name":"乌克兰"},{"countryCode":598,"searchKey":"wulagui","name":"乌拉圭"},{"countryCode":998,"searchKey":"wuzibiekesitan","name":"乌兹别克斯坦"}],"key":"W"},{"childItems":[{"countryCode":34,"searchKey":"xibanya","name":"西班牙"},{"countryCode":210,"searchKey":"xisahala","name":"西撒哈拉"},{"countryCode":30,"searchKey":"xila","name":"希腊"},{"countryCode":65,"searchKey":"xinjiapo","name":"新加坡"},{"countryCode":687,"searchKey":"xinkaliduoniya （fa）","name":"新喀里多尼亚 （法）"},{"countryCode":64,"searchKey":"xinxilan","name":"新西兰"},{"countryCode":36,"searchKey":"xiongyali","name":"匈牙利"},{"countryCode":963,"searchKey":"xuliya","name":"叙利亚"}],"key":"X"},{"childItems":[{"countryCode":374,"searchKey":"yameiniya","name":"亚美尼亚"},{"countryCode":967,"searchKey":"yemen","name":"也门"},{"countryCode":964,"searchKey":"yilake","name":"伊拉克"},{"countryCode":98,"searchKey":"yilang","name":"伊朗"},{"countryCode":972,"searchKey":"yiselie","name":"以色列"},{"countryCode":39,"searchKey":"yidali","name":"意大利"},{"countryCode":91,"searchKey":"yindu","name":"印度"},{"countryCode":62,"searchKey":"yindunixiya","name":"印度尼西亚"},{"countryCode":44,"searchKey":"yingguo","name":"英国"},{"countryCode":962,"searchKey":"yuedan","name":"约旦"},{"countryCode":84,"searchKey":"yuenan","name":"越南"}],"key":"Y"},{"childItems":[{"countryCode":260,"searchKey":"zanbiya","name":"赞比亚"},{"countryCode":235,"searchKey":"zhade","name":"乍得"},{"countryCode":350,"searchKey":"zhibuluotuo","name":"直布罗陀"},{"countryCode":56,"searchKey":"zhili","name":"智利"},{"countryCode":236,"searchKey":"zhongfei","name":"中非"},{"countryCode":886,"searchKey":"zhongguotaiwan","name":"中国台湾"}],"key":"Z"}]
     */

    private String version;
    //    private List<TelListBean.ChildItemsBean> top;
    private List<TelListBean> telList;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


    public List<TelListBean> getTelList() {
        return telList;
    }

    public void setTelList(List<TelListBean> telList) {
        this.telList = telList;
    }


    public static class TelListBean {
        /**
         * childItems : [{"countryCode":355,"searchKey":"aerbaniya","name":"阿尔巴尼亚"},{"countryCode":213,"searchKey":"aerjiliya","name":"阿尔及利亚"},{"countryCode":93,"searchKey":"afuhan","name":"阿富汗"},{"countryCode":54,"searchKey":"agenting","name":"阿根廷"},{"countryCode":971,"searchKey":"alianqiu","name":"阿联酋"},{"countryCode":297,"searchKey":"aluba（he）","name":"阿鲁巴（菏）"},{"countryCode":968,"searchKey":"aman","name":"阿曼"},{"countryCode":994,"searchKey":"asaibaijiang","name":"阿塞拜疆"},{"countryCode":20,"searchKey":"aiji","name":"埃及"},{"countryCode":251,"searchKey":"aisaiebiya","name":"埃塞俄比亚"},{"countryCode":353,"searchKey":"aierlan","name":"爱尔兰"},{"countryCode":372,"searchKey":"aishaniya","name":"爱沙尼亚"},{"countryCode":376,"searchKey":"andaoer","name":"安道尔"},{"countryCode":244,"searchKey":"angela","name":"安哥拉"},{"countryCode":43,"searchKey":"aodili","name":"奥地利"},{"countryCode":61,"searchKey":"aodaliya","name":"澳大利亚"}]
         * key : A
         */

        private String key;
        private List<ChildItemsBean> childItems;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public List<ChildItemsBean> getChildItems() {
            return childItems;
        }

        public void setChildItems(List<ChildItemsBean> childItems) {
            this.childItems = childItems;
        }

        public static class ChildItemsBean {
            /**
             * countryCode : 355
             * searchKey : aerbaniya
             * name : 阿尔巴尼亚
             */

            private String countryCode;
            private String searchKey;
            private String name;

            public boolean isSel() {
                return isSel;
            }

            public void setSel(boolean sel) {
                isSel = sel;
            }

            private boolean isSel;

            public String getKey() {
                return key;
            }

            public void setKey(String key) {
                this.key = key;
            }

            private String key;

            public String getCountryCode() {
                return countryCode;
            }

            public void setCountryCode(String countryCode) {
                this.countryCode = countryCode;
            }

            public String getSearchKey() {
                return searchKey;
            }

            public void setSearchKey(String searchKey) {
                this.searchKey = searchKey;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }
    }


//    private String version;
//    private List<TelListBean.ChildItemsBean> top;
//    private List<TelListBean> telList;
//
//    public String getVersion() {
//        return version;
//    }
//
//    public void setVersion(String version) {
//        this.version = version;
//    }
//
//    public List<TelListBean.ChildItemsBean> getTop() {
//        return top;
//    }
//
//    public void setTop(List<TelListBean.ChildItemsBean> top) {
//        this.top = top;
//    }
//
//    public List<TelListBean> getTelList() {
//        return telList;
//    }
//
//    public void setTelList(List<TelListBean> telList) {
//        this.telList = telList;
//    }
//
//
//    public static class TelListBean {
//        /**
//         * childItems : [{"countryCode":355,"searchKey":"aerbaniya","name":"阿尔巴尼亚"},{"countryCode":213,"searchKey":"aerjiliya","name":"阿尔及利亚"},{"countryCode":93,"searchKey":"afuhan","name":"阿富汗"},{"countryCode":54,"searchKey":"agenting","name":"阿根廷"},{"countryCode":971,"searchKey":"alianqiu","name":"阿联酋"},{"countryCode":297,"searchKey":"aluba（he）","name":"阿鲁巴（菏）"},{"countryCode":968,"searchKey":"aman","name":"阿曼"},{"countryCode":994,"searchKey":"asaibaijiang","name":"阿塞拜疆"},{"countryCode":20,"searchKey":"aiji","name":"埃及"},{"countryCode":251,"searchKey":"aisaiebiya","name":"埃塞俄比亚"},{"countryCode":353,"searchKey":"aierlan","name":"爱尔兰"},{"countryCode":372,"searchKey":"aishaniya","name":"爱沙尼亚"},{"countryCode":376,"searchKey":"andaoer","name":"安道尔"},{"countryCode":244,"searchKey":"angela","name":"安哥拉"},{"countryCode":43,"searchKey":"aodili","name":"奥地利"},{"countryCode":61,"searchKey":"aodaliya","name":"澳大利亚"}]
//         * key : A
//         */
//
//        private String key;
//        private List<ChildItemsBean> childItems;
//
//        public String getItem() {
//            return key;
//        }
//
//        public void setKey(String key) {
//            this.key = key;
//        }
//
//        public List<ChildItemsBean> getChildItems() {
//            return childItems;
//        }
//
//        public void setChildItems(List<ChildItemsBean> childItems) {
//            this.childItems = childItems;
//        }
//
//        public static class ChildItemsBean {
//            /**
//             * countryCode : 355
//             * searchKey : aerbaniya
//             * name : 阿尔巴尼亚
//             */
//
//            private String countryCode;
//            private String searchKey;
//            private String name;
//
//            public String getItem() {
//                return key;
//            }
//
//            public void setKey(String key) {
//                this.key = key;
//            }
//
//            private String key;
//
//            public String getCountryCode() {
//                return countryCode;
//            }
//
//            public void setCountryCode(String countryCode) {
//                this.countryCode = countryCode;
//            }
//
//            public String getSearchKey() {
//                return searchKey;
//            }
//
//            public void setSearchKey(String searchKey) {
//                this.searchKey = searchKey;
//            }
//
//            public String getName() {
//                return name;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//        }
//    }


}
