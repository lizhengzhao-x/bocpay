package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 *
 */
public class StaffRedPacketQueryProtocol extends BaseProtocol {
    //1表示是点击员工利是push通知调用此接口
    private int isPush = 0;

    @Override
    public boolean isNeedLogin() {
        return true;
    }


    public StaffRedPacketQueryProtocol(NetWorkCallbackListener dataCallback,int isPush) {
        this.mDataCallback = dataCallback;
        this.isPush = isPush;
        mUrl = "api/redpacket/staffRedPacketQuery";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ISPUSH, isPush);
    }
}
