package cn.swiftpass.wallet.intl.module.rewardregister.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CheckRegistrationStatus extends BaseEntity {

    private String validRegisterCode;

    public String getValidRegisterCode() {
        return validRegisterCode;
    }

    public void setValidRegisterCode(String validRegisterCode) {
        this.validRegisterCode = validRegisterCode;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    private String refId;


}
