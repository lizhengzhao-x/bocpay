package cn.swiftpass.wallet.intl.module.transfer.view;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;

public class CrossBorderPaymentActivity extends BaseCompatActivity  {


    private FragmentManager mFragmentManager;
    private CrossBorderPaymentFragment crossBorderPaymentFragment;

    @Override
    public void init() {
        setToolBarTitle(R.string.home_discover);

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        crossBorderPaymentFragment = new CrossBorderPaymentFragment();
        fragmentTransaction.add(R.id.fl_main, crossBorderPaymentFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_cross_border_payment;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


}
