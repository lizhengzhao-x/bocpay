package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 10:39
 * 主扫下单接口
 * 对应的请求参数对象为 MainScanOrderRequestEntity,返回对象为 MainScanOrderEntity
 */

public class MainScanApppCallAppOrderProtocol extends BaseProtocol {
    public static final String TAG = MainScanApppCallAppOrderProtocol.class.getSimpleName();
    private String action;
    private MainScanOrderRequestEntity mRequestEntity;


    public MainScanApppCallAppOrderProtocol(MainScanOrderRequestEntity requestEntity, String actionIn, NetWorkCallbackListener dataCallback) {
        this.mRequestEntity = requestEntity;
        this.mDataCallback = dataCallback;
        this.action = actionIn;
        mUrl = "api/transaction/actionMpqrPaymentForAppCallApp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.QRCTYPE, mRequestEntity.getQrcType());
        mBodyParams.put(RequestParams.CARDID, mRequestEntity.getCardId());
        mBodyParams.put(RequestParams.TRXAMOUNT, mRequestEntity.getTrxAmt());
        mBodyParams.put(RequestParams.TRXFEEAMOUNT, mRequestEntity.getTrxFeeAmt());
        mBodyParams.put(RequestParams.MPQRCODE, mRequestEntity.getMpQrCode());
        mBodyParams.put(RequestParams.MERCHANTNAME, mRequestEntity.getMerchantName());
        mBodyParams.put(RequestParams.COUPINFO, mRequestEntity.getCouponInfo());
        mBodyParams.put(RequestParams.MOBILE, mRequestEntity.getMobile());
        mBodyParams.put(RequestParams.TXNID, mRequestEntity.getTxnId());
        mBodyParams.put(RequestParams.STORELABEL, mRequestEntity.getStoreLabel());
        mBodyParams.put(RequestParams.LOYALTYNO, mRequestEntity.getLoyaltyNo());
        mBodyParams.put(RequestParams.REFLABEL, mRequestEntity.getRefLabel());
        mBodyParams.put(RequestParams.CUSTOMERLABEL, mRequestEntity.getCustomerLable());
        mBodyParams.put(RequestParams.TERMINALABEL, mRequestEntity.getTerminalLabel());
        mBodyParams.put(RequestParams.TRXPURPOSE, mRequestEntity.getTrxPurpose());
        mBodyParams.put(RequestParams.CONSUMEREMAIL, mRequestEntity.getConsumerEmail());
        mBodyParams.put(RequestParams.CONSUMERADDRESS, mRequestEntity.getConsumerAddress());
        mBodyParams.put(RequestParams.CONSUMERMOBILE, mRequestEntity.getConsumerMobile());
        mBodyParams.put(RequestParams.TXNCURR, mRequestEntity.getTxnCurr());
        if (!TextUtils.isEmpty(action)){
            mBodyParams.put(RequestParams.ACTION, "EXTPAY");
        }
    }
}
