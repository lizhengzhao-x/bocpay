package cn.swiftpass.wallet.intl.base.card;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.CardsEntity;

;

/**
 * @name HKBocBill
 * @class name：cn.swiftpass.bocbill.model.base.otp
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/12 14:09
 * @change
 * @chang time
 * @class describe
 */
public interface CardListQueryView<V extends IPresenter> extends IView {
    void queryCardListError(String errorCode, String errorMsg);

    void queryCardListSuccess(CardsEntity response, String action);


}
