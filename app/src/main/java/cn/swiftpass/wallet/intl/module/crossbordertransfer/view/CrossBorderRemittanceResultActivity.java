package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.Manifest;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.CrossBorderRemittanceContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.CrossBorderPresenter;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.ShareImageUtils;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.CrossBorderListAdapter;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

/**
 * 跨境汇款结果记录页面
 */
public class CrossBorderRemittanceResultActivity extends BaseCompatActivity<CrossBorderRemittanceContract.Presenter> implements CrossBorderRemittanceContract.View {

    @BindView(R.id.list)
    RecyclerView mRecyclerView;
    private String tnxId;

    private CrossBorderListAdapter mCrossBorderListAdapter;
    private TransferCrossBorderStatusEntity transferCrossBorderStatusEntity;
    private String type;
    private boolean isSmartList;

    public static final String USE_NEW_UI = "USE_NEW_UI";

    @Override
    protected int getLayoutId() {
        return R.layout.act_cross_border_remittance_result;
    }

    @Override
    protected CrossBorderRemittanceContract.Presenter createPresenter() {
        return new CrossBorderPresenter(this);
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.CP3_3a_1);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            isSmartList = extras.getBoolean(Constants.SMART_TYPE, false);
        }

        mCrossBorderListAdapter = new CrossBorderListAdapter(this, new CrossBorderListAdapter.OnItemShareListener() {
            @Override
            public void OnItemShare(int position) {
                PermissionInstance.getInstance().getStoreImagePermission(CrossBorderRemittanceResultActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        if (position == -1) {
                            ShareImageUtils.saveImageToGalley(CrossBorderRemittanceResultActivity.this, ShareImageUtils.generateShareParams(CrossBorderRemittanceResultActivity.this, transferCrossBorderStatusEntity));
                        } else {
                            ShareImageUtils.shareImageToOther(CrossBorderRemittanceResultActivity.this, position, ShareImageUtils.generateShareParams(CrossBorderRemittanceResultActivity.this, transferCrossBorderStatusEntity));
                        }
                    }

                    @Override
                    public void rejectPermission() {
                        ShareImageUtils.showLackOfPermissionDialog(CrossBorderRemittanceResultActivity.this);
                    }
                }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        });
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mCrossBorderListAdapter);
        if (isSmartList) {
            String outtrfrefno = extras.getString(Constants.OUTTRFREFNO);
            String txnType = extras.getString(Constants.TXNTYPE);
            mPresenter.getUIRecordListWithSmart(outtrfrefno, txnType);
        } else {
            tnxId = extras.getString(Constants.TNX_ID);
            type = extras.getString(Constants.ACCOUNT_TYPE);
            mPresenter.getUIRecordList(tnxId, type);
        }

        loadDetailBanners();

    }

    /**
     * initBanner 接口主要用于拉取各种banner接口
     */
    private void loadDetailBanners() {
        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {

            }
        });
    }


    @Override
    public void getUIRecordListSuccess(TransferCrossBorderStatusEntity response, ArrayList<ItemModel> list) {
        this.transferCrossBorderStatusEntity = response;
        mCrossBorderListAdapter.refresh(list);
    }

    @Override
    public void getUIRecordListError(String errorCode, String errorMsg) {

        showErrorMsgDialog(this, errorMsg);
    }
}
