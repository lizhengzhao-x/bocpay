package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class GetTnxIdEntity extends BaseEntity {


    public String getTransactionUniqueID() {
        return transactionUniqueID;
    }

    public void setTransactionUniqueID(String transactionUniqueID) {
        this.transactionUniqueID = transactionUniqueID;
    }

    private String transactionUniqueID;
}
