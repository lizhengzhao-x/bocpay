package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.RewardRegisterEventEntity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 信用卡登记查询失败
 */
public class RewardRegisterCheckFailedActivity extends BaseCompatActivity {
    @BindView(R.id.id_top_success_img)
    ImageView idTopSuccessImg;
    @BindView(R.id.id_reward_status)
    TextView idRewardStatus;
    @BindView(R.id.id_reward_now)
    TextView idRewardNow;
    @BindView(R.id.id_reward_back)
    TextView idRewardBack;

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.CR209_11_1);
        idRewardBack.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //登记其他活动
                EventBus.getDefault().postSticky(new RewardRegisterEventEntity(RewardRegisterEventEntity.EVENT_BACK_REWARD_LIST, ""));
                finish();
            }
        });
        idRewardNow.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_rewardregister_check_failed;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
