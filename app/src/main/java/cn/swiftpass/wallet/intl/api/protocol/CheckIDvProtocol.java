package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;


public class CheckIDvProtocol extends BaseProtocol {

    public CheckIDvProtocol(String type, NetWorkCallbackListener<RegisterIDEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        //不同流程 后台区分 ur不同
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/checkIDV";
        } else if (type.equals(RequestParams.REGISTER_STWO_ACCOUNT)) {
            mUrl = "api/smartReg/checkIDV";
        }
    }

}
