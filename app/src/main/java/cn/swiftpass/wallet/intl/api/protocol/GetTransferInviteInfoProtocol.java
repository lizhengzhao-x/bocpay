package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 注册成功后填写邀请码界面
 */
public class GetTransferInviteInfoProtocol extends BaseProtocol {

    public GetTransferInviteInfoProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/getTransferInviteInfo";
    }
}
