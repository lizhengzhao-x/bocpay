package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 信用卡确认注册接口，返回对象为
 */

public class UpdateSmartAccountInfoProtocol extends BaseProtocol {
    public static final String TAG = UpdateSmartAccountInfoProtocol.class.getSimpleName();

    /**
     * 修改主账号
     */
    String relevanceAccNo;
    /**
     * 修改自动充值限额
     */
    String autotopupamt;
    /**
     * 当日限额
     */
    String limitPay;
    /**
     * 自动充值方式
     */
    String addedmethod;
    /**
     * A:修改主账号，L:修改限额，M:增值方式
     * S:暂停我的账户 R:重新激活 C:注销我的账户
     */
    String action;

    public UpdateSmartAccountInfoProtocol(String action, String relevanceAccNo, String limitPay, String addedmethod, String autotopupamt, NetWorkCallbackListener dataCallback) {
        this.relevanceAccNo = getValue(relevanceAccNo);
        this.limitPay = getValue(limitPay);
        this.autotopupamt = getValue(autotopupamt);
        this.addedmethod = getValue(addedmethod);
        this.action = getValue(action);
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/updateSmartInfo";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.SMART_ACCOUNT_PRIMARY, relevanceAccNo);
        mBodyParams.put(RequestParams.AUTO_TOPUP_LIMIT, autotopupamt);
        mBodyParams.put(RequestParams.DAILY_PAY_LIMIT, limitPay);
        mBodyParams.put(RequestParams.TOPUP_METHOD, addedmethod);
        mBodyParams.put(RequestParams.ACTION, action);
    }

    private String getValue(String str) {
        return (null == str ? "" : str);
    }
}
