package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/10/9.
 * FPS他行绑定记录
 */

public class FpsOtherBankRecordDisplayBean extends BaseEntity {

    FpsOtherBankRecordBean recordBean;
    //是否隐藏勾选框
    boolean isHideCheck;

    //禁止可点，默认可以点
    boolean isBanClick;

    //是否选中
    boolean isStatusCheck;

    public boolean isStatusCheck() {
        return isStatusCheck;
    }

    public void setStatusCheck(boolean statusCheck) {
        isStatusCheck = statusCheck;
    }

    public boolean isBanClick() {
        return isBanClick;
    }

    public void setBanClick(boolean banClick) {
        isBanClick = banClick;
    }

    public FpsOtherBankRecordBean getRecordBean() {
        return recordBean;
    }

    public void setRecordBean(FpsOtherBankRecordBean recordBean) {
        this.recordBean = recordBean;
    }

    public boolean isHideCheck() {
        return isHideCheck;
    }

    public void setHideCheck(boolean hideCheck) {
        isHideCheck = hideCheck;
    }

}
