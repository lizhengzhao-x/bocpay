package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class TransferListEntity extends BaseEntity {


    private List<BankListBean> bankList;

    public List<BankListBean> getTopBankList() {
        return topBankList;
    }

    public void setTopBankList(List<BankListBean> topBankList) {
        this.topBankList = topBankList;
    }

    private List<BankListBean> topBankList;

    public List<BankListBean> getBankList() {
        return bankList;
    }

    public void setBankList(List<BankListBean> bankList) {
        this.bankList = bankList;
    }

    public static class BankListBean extends BaseEntity {
        /**
         * checked : false
         * currency : HKD
         * pageNumber : 1
         * pageSize : 10
         * participantCode : 013
         * participantName : Bhina Bank Hong Kong
         * participantNameSc : 中过银行
         * participantNameTc : 中国银行
         * total : 0
         */

        private boolean checked;
        private String currency;
        private int pageNumber;
        private int pageSize;
        private String participantCode;
        private String participantName;
        private String participantNameSc;
        private String participantNameTc;

        public String getTempNames() {
            return tempNames;
        }

        public void setTempNames(String tempNames) {
            this.tempNames = tempNames;
        }

        private String tempNames;
        private int total;

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public String getParticipantCode() {
            return participantCode;
        }

        public void setParticipantCode(String participantCode) {
            this.participantCode = participantCode;
        }

        public String getParticipantName() {
            return participantName;
        }

        public void setParticipantName(String participantName) {
            this.participantName = participantName;
        }

        public String getParticipantNameSc() {
            return participantNameSc;
        }

        public void setParticipantNameSc(String participantNameSc) {
            this.participantNameSc = participantNameSc;
        }

        public String getParticipantNameTc() {
            return participantNameTc;
        }

        public void setParticipantNameTc(String participantNameTc) {
            this.participantNameTc = participantNameTc;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }
}
