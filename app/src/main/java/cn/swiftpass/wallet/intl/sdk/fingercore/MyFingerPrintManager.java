package cn.swiftpass.wallet.intl.sdk.fingercore;

import android.app.Activity;

import com.tradelink.boc.authapp.utils.AuthCommonUtils;
import com.tradelink.boc.authapp.utils.FidoOperationUtils;
import com.tradelink.boc.sotp.utils.SoftTokenOperationUtils;

import cn.swiftpass.wallet.intl.module.setting.paymentsetting.FIOActivity;

/**
 * Created by ramon on 2018/8/30.
 */

public class MyFingerPrintManager {
    //设备是否设置指纹
    boolean mFingerPrintPolicy = false;
    //手机是否支持指纹
    boolean mFingerPrintSupport = false;
    private static MyFingerPrintManager sInstance = new MyFingerPrintManager();

    private MyFingerPrintManager() {

    }

    public static MyFingerPrintManager getInstance() {
        return sInstance;
    }

    public void setFingerPrintPolicy(boolean flag) {
        mFingerPrintPolicy = flag;
    }

    public void setFingerPrintSupport(boolean flag) {
        mFingerPrintSupport = flag;
    }

    public boolean getFingerPrintPolicy() {
        return mFingerPrintPolicy;
    }

    public boolean setFingerPrintSupport() {
        return mFingerPrintSupport;
    }

    public boolean isRegisterFIO(Activity activity) {
        return SoftTokenOperationUtils.isRegistered(activity);
    }

    public boolean isChangeFIO(Activity activity) {
        return FidoOperationUtils.isFingerPrintChanged(activity);
    }


    public String getRegisterFIO(Activity activity) {
        return AuthCommonUtils.getFioKey();
    }

    public void clearFIO() {
        AuthCommonUtils.setFioKey("");
    }

    public void deregisterFingerprint(Activity activity) {
        FIOActivity.startDegregisterFingerPrint(activity);
    }

    public void setLanguage(Activity activity, String langCode) {
        //AuthCommonUtils.setLocale(activity, langCode);
        //LocaleUtils.updateLocale(activity, langCode);
    }
}
