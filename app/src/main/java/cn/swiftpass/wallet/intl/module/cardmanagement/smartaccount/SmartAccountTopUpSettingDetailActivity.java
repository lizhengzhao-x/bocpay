package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

/**
 * 我的账户
 */

public class SmartAccountTopUpSettingDetailActivity extends BaseCompatActivity {
    @BindView(R.id.id_currency)
    TextView idCurrency;
    @BindView(R.id.id_money)
    TextView idMoney;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_C0_6);
        String moneyStr = getIntent().getExtras().getString(Constants.MONEY);
        String currency = getIntent().getExtras().getString(Constants.CURRENCY);

        idMoney.setText(AndroidUtils.formatPrice(Double.parseDouble(moneyStr), true));
        idCurrency.setText(currency);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_topupdetail;
    }


    @OnClick(R.id.tv_confirm)
    public void onViewClicked() {
        if (ButtonUtils.isFastDoubleClick()) return;
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_UPDATE_RECHARGETYPE);
        mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_M);
        mHashMapsLogin.put(Constants.DATA_ADD_METHOD, getIntent().getExtras().getString(Constants.DATA_ADD_METHOD));
        mHashMapsLogin.put(Constants.MONEY, getIntent().getExtras().getString(Constants.MONEY));
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
        ActivitySkipUtil.startAnotherActivity(SmartAccountTopUpSettingDetailActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }
}
