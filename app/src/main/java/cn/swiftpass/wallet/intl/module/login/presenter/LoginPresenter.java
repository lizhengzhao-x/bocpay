package cn.swiftpass.wallet.intl.module.login.presenter;

import android.os.Handler;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.BankLoginVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.LoginCheckNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SystemPageInitDataProtocol;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.LoginCheckEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.module.login.contract.LoginContract;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {


//    @Override
//    public void checkAccount(String mobile, String action) {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
//
//        new LoginCheckProtocol(mobile, action, new NetWorkCallbackListener<LoginCheckEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView() != null) {
//                    getView().dismissDialog();
//                    getView().OnCheckAccountError(errorCode, errorMsg);
//                }
//            }
//
//            @Override
//            public void onSuccess(LoginCheckEntity response) {
//                if (getView() != null) {
//                    getView().dismissDialog();
//                    getView().OnCheckAccountSuccess(response, mobile);
//                }
//            }
//        }).execute();
//    }

    @Override
    public void getInitData(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SystemPageInitDataProtocol(new NetWorkCallbackListener<SystemPagerDataEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInitDataFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SystemPagerDataEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInitDataSuccess(response, action);
                }
            }
        }).execute();
    }

    @Override
    public void getVerifyCode() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BankLoginVerifyCodeProtocol("L", new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getVerifyCodeFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getVerifyCodeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void verifyCode(String code) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckVerifyCodeProtocol(code, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyCodeFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyCodeSuccess();
                }
            }
        }).execute();
    }

    @Override
    public void verifyPwd(String passcode, String mobile) {

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getView().verifyPwdFail("xxxxx", "xxxxxxxxxxx");
//            }
//        },5000);

        new LoginCheckNewProtocol(passcode, mobile, new NetWorkCallbackListener<LoginCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().verifyPwdFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(LoginCheckEntity response) {
                if (getView() != null) {
//                    getView().dismissDialog();
                    getView().verifyPwdSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void forgetPwdDeviceStatus() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ForgetPwdProtocol(new NetWorkCallbackListener<ForgetPasswordEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().forgetPwdDeviceStatusFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ForgetPasswordEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().forgetPwdDeviceStatusSuccess(response);
                }
            }
        }).execute();
    }
}
