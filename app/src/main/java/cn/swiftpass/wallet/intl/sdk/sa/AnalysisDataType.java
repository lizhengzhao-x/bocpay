package cn.swiftpass.wallet.intl.sdk.sa;

/**
 * 埋点数据类型 ACTION_CLI_POINT 点击事件埋点；PA_REG_FLOW_POINT pa注册流程埋点；PA_REG_ERR_POINT pa注册错误码埋点
 */
public class AnalysisDataType {
    public static final String ACTION_CLI_POINT = "ACTION_CLI_POINT";
    public static final String PA_REG_FLOW_POINT = "PA_REG_FLOW_POINT";
    public static final String PA_REG_ERR_POINT = "PA_REG_ERR_POINT";
}
