package cn.swiftpass.wallet.intl.module.setting.about;

import android.content.Intent;
import android.net.Uri;
import android.view.View;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;


public class ServiceAgreementActivity extends BaseCompatActivity {
    @BindView(R.id.id_service_statement)
    ImageArrowView idServiceStatement;
    @BindView(R.id.id_service_notice)
    ImageArrowView idServiceNotice;
    @BindView(R.id.id_service_privacy)
    ImageArrowView idServicePrivacy;
    @BindView(R.id.id_service_wallet)
    ImageArrowView idServiceWallet;
    @BindView(R.id.id_service_qrcode)
    ImageArrowView idServiceQrcode;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.setting_about_service);
        //中银香港电子钱包服务之条款及细则
        idServiceStatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(ServiceAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_RULE_SIMP_URL, idServiceStatement.getTvLeft().getText().toString());
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_RULE_TRAD_URL, idServiceStatement.getTvLeft().getText().toString());
                } else {
                    startToOtherActivity(ApiConstant.WALLET_RULE_ENGLISH_URL, idServiceStatement.getTvLeft().getText().toString());
                }

            }
        });

        //中银二维条码支付服务的条款及细则
        idServiceNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(ServiceAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_DATA_SIMP_URL, idServiceNotice.getTvLeft().getText().toString());
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_DATA_TRAD_URL, idServiceNotice.getTvLeft().getText().toString());
                } else {
                    startToOtherActivity(ApiConstant.WALLET_DATA_ENGLISH_URL, idServiceNotice.getTvLeft().getText().toString());
                }
            }
        });

        //中银信用卡主要条款及细则摘要
        idServicePrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(ServiceAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_URL_SIMP_URL, idServicePrivacy.getTvLeft().getText().toString());
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_URL_TRAD_URL, idServicePrivacy.getTvLeft().getText().toString());
                } else {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_URL_ENGLISH_URL, idServicePrivacy.getTvLeft().getText().toString());
                }
            }
        });


        //隐私政策说明
        idServiceWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(ServiceAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_POLICY_SIMP_URL, idServiceWallet.getTvLeft().getText().toString());
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_POLICY_TRAD_URL, idServiceWallet.getTvLeft().getText().toString());
                } else {
                    startToOtherActivity(ApiConstant.WALLET_POLICY_ENGLISH_URL, idServiceWallet.getTvLeft().getText().toString());
                }
            }
        });

        //资料政策通告
        idServiceQrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(ServiceAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_QR_SIMP_URL, idServiceQrcode.getTvLeft().getText().toString());
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_QR_TRAD_URL, idServiceQrcode.getTvLeft().getText().toString());
                } else {
                    startToOtherActivity(ApiConstant.WALLET_SERVICE_QR_ENGLISH_URL, idServiceQrcode.getTvLeft().getText().toString());
                }
            }
        });

    }

    private void startToOtherActivity(String url, String title) {
        if (url.endsWith(".pdf")) {
            //解决pdf文件结尾 webview无法加载的问题
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
            return;
        }

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, url);
        mHashMaps.put(Constants.DETAIL_TITLE, title);
        ActivitySkipUtil.startAnotherActivity(ServiceAgreementActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_serviceagreement;
    }


}
