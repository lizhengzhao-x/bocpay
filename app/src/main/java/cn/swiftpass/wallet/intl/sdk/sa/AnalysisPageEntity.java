package cn.swiftpass.wallet.intl.sdk.sa;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

/**
 * 埋点:页面事件
 */
public class AnalysisPageEntity extends AnalysisBaseEntity {

    /**
     * 用户进入当前页面时间，格式yyyy-MM-dd HH:mm:ss
     */
    public String date_time;

    /**
     * 按钮所在页面上一页面id，参考表1-1 中指定页面id(可能为空)
     */
    public String last_address;

    /**
     * 当前页面id
     */
    public String current_address;

    /**
     * 在該頁面停留時間 (mm:ss)
     */
    public String time_duration;

    public AnalysisPageEntity() {
        super();
    }

    public void setTime_duration(long time_duration) {
        this.time_duration = longToSecond(time_duration);
    }

    public void setDate_time(long date_time) {
        this.date_time = longToStr(date_time, "yyyy-MM-dd HH:mm:ss");
    }

    public AnalysisPageEntity(long date_time, String last_address, String current_address, long time_duration) {
        super();
        this.date_time = longToStr(date_time, "yyyy-MM-dd HH:mm:ss");
        this.last_address = TextUtils.isEmpty(last_address) ? "" : last_address;
        this.current_address = current_address;
//        this.time_duration = longToStr(time_duration,"mm:ss");
        this.time_duration = longToSecond(time_duration);
    }

    @Override
    public String getDataType() {
        return AnalysisDataType.PA_REG_FLOW_POINT;
    }

    @Override
    public JSONObject getJsonObject() {
        JSONObject jsonObject = super.getJsonObject();
        jsonObject.put(AnalysisParams.DATE_TIME, date_time);
        jsonObject.put(AnalysisParams.LAST_ADDRESS, last_address);
        jsonObject.put(AnalysisParams.CURRENT_ADDRESS, current_address);
        jsonObject.put(AnalysisParams.TIME_DURATION, time_duration);
        return jsonObject;
    }
}
