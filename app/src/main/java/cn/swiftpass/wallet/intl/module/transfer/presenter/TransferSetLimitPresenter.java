package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSetLimitContract;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class TransferSetLimitPresenter extends BasePresenter<TransferSetLimitContract.View> implements TransferSetLimitContract.Presenter {


    @Override
    public void updateQRCode() {

    }
}
