package cn.swiftpass.wallet.intl.module.register;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 信用卡账户注册
 */
public class RegisterCreditCardSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.id_register_number)
    TextView idRegisterNumber;
    @BindView(R.id.tv_card_info_nextPage)
    TextView tvCardInfoNextPage;
    @BindView(R.id.id_invite_intro)
    TextView idInviteIntro;

    @BindView(R.id.id_credit_register_info_desc)
    TextView creditCardRegisterInfoDesc;
    @BindView(R.id.id_credit_register_info_title)
    TextView creditCardRegisterInfoTitle;


    String mAccountType = Constants.ACCOUNT_TYPE_SMART;
    int mTypeFlag = -1;
    @BindView(R.id.id_smart_account_title)
    TextView idSmartAccountTitle;
    @BindView(R.id.id_smart_account)
    TextView idSmartAccount;
    //注册成功的描述
    private String rewardtip;
    //是否需要跳转到输入邀请码的界面
    private Boolean showInvitePage;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        if (null == getIntent() || null == getIntent().getExtras()) {
            finish();
            return;
        }
        setToolBarTitle(R.string.IDV_2_1);
        hideBackIcon();
        String phoneStr = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        mAccountType = getIntent().getExtras().getString(Constants.ACCOUNT_TYPE);
        String smartAccount = getIntent().getExtras().getString(Constants.SMART_ACCOUNT, null);
        if (smartAccount != null) {
            //智能账户注册
            idSmartAccountTitle.setVisibility(View.VISIBLE);
            idSmartAccount.setVisibility(View.VISIBLE);
            idSmartAccount.setText(smartAccount);
        }
        mTypeFlag = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
        if (TextUtils.equals(mAccountType, Constants.ACCOUNT_TYPE_CREDIT) && mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
            creditCardRegisterInfoDesc.setVisibility(View.VISIBLE);
            creditCardRegisterInfoTitle.setVisibility(View.VISIBLE);
        }
        rewardtip = getIntent().getExtras().getString(Constants.REWARDTIP);
        showInvitePage = getIntent().getExtras().getBoolean(Constants.SHOWINVITEPAGE);
        if (!TextUtils.isEmpty(rewardtip)) {
            idInviteIntro.setText(rewardtip);
            idInviteIntro.setVisibility(View.VISIBLE);
        } else {
            idInviteIntro.setVisibility(View.GONE);
        }
        idRegisterNumber.setText(AndroidUtils.formatCenterPhoneNumberStr(phoneStr));
        tvCardInfoNextPage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                judgeToNextInvitePage();
            }
        });
    }

    /**
     * 判断是否跳转到输入邀请码界面
     */
    private void judgeToNextInvitePage() {
        //如果是信用卡 需要跳转到引导界面 智能账户 跳转到 输入邀请码界面
//        if (TextUtils.equals(mAccountType, Constants.ACCOUNT_TYPE_CREDIT) && mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
//            CacheManagerInstance.getInstance().setRegisterPopupBannerUrl(getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_CARD_TYPE_SHOW_INVITE);
            maps.put(Constants.SHOWINVITEPAGE, showInvitePage);
            maps.put(RegisterSmartAccountDescActivity.SHOW_MENU_BACK, true);
            maps.put(RegisterSmartAccountDescActivity.SHOW_MENU_MORE, true);
            maps.put(Constants.IS_NEED_JUMP_NOTIFICATION, getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false));
            RegisterSmartAccountDescActivity.startActivity(getActivity(), maps);
//        }


//        if (showInvitePage) {
//            //首次注册
//            //如果是信用卡 需要跳转到引导界面 智能账户 跳转到 输入邀请码界面
//            if (TextUtils.equals(mAccountType, Constants.ACCOUNT_TYPE_CREDIT) && mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
//                HashMap<String, Object> maps = new HashMap<>();
//                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_CARD_TYPE_SHOW_INVITE);
//                maps.put(RegisterSmartAccountDescActivity.SHOW_MENU_BACK, true);
//                maps.put(RegisterSmartAccountDescActivity.SHOW_MENU_MORE, true);
//                boolean isNeedJumpNotification = false;
//                if (getIntent() != null) {
//                    isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//                    maps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//                }
//                maps.put(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
//                RegisterSmartAccountDescActivity.startActivity(getActivity(), maps);
//            } else {
//                Intent intent = new Intent(RegisterCreditCardSuccessActivity.this, RegisterInviteActivity.class);
//                intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
//                startActivity(intent);
//            }
//        } else {
//            MyActivityManager.removeAllTaskExcludeMainStack();
//            //如果是在登录态 可以把bocpay变成智能账户
//            Intent intent = new Intent(RegisterCreditCardSuccessActivity.this, MainHomeActivity.class);
//            intent.putExtra(Constants.ISREGESTER, true);
//            boolean isNeedJumpNotification = false;
//            if (getIntent() != null) {
//                isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//                intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//            }
//            intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
//            if (TextUtils.equals(mAccountType, Constants.ACCOUNT_TYPE_CREDIT) && mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
//                intent.putExtra(SmartAccountConst.TO_BIND_SMART_ACCOUNT, true);
//            }
//            startActivity(intent);
//            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
//        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_success;
    }
}
