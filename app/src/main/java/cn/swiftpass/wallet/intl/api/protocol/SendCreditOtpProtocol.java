package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 发送信用卡OTP接口，返回对象为 SendOTPEntity
 */

public class SendCreditOtpProtocol extends BaseProtocol {
    public static final String TAG = SendCreditOtpProtocol.class.getSimpleName();
    /**
     * 用户id
     */
    String mWalletId;
    /**
     * 卡号
     */
    String mPan;
    /**
     * 过期时间
     */
    String mExpDate;
    /**
     * 标识取【L】：B：绑定检查；V：仅验证；F：忘记密码，L：登录；
     */
    String mAction;
    //账号
    String mAccount;

    public SendCreditOtpProtocol(String account, String walletId, String pan, String expDate, String action, NetWorkCallbackListener dataCallback) {
        this.mWalletId = walletId;
        this.mPan = pan;
        this.mExpDate = expDate;
        this.mAction = action;
        mAccount = account;
        this.mDataCallback = dataCallback;
        mUrl = "api/register/sendOtpCc";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        mBodyParams.put(RequestParams.PAN, mPan);
        mBodyParams.put(RequestParams.EXDATE, mExpDate);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.ACCOUNT, mAccount);

    }
}
