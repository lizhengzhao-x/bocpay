package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;

/**
 * @author Created by ramon on 2018/8/20.
 * 转账试算接口
 */

public class TransferPreCheckByBocProtocol extends BaseProtocol {
    TransferPreCheckReq mReq;

    public TransferPreCheckByBocProtocol(TransferPreCheckReq req, NetWorkCallbackListener callback) {
        this.mReq = req;
        mDataCallback = callback;
        mUrl = "api/transfer/bocPayTransferPreOrder";
    }

    @Override
    public void packData() {
        super.packData();
        if (null != mReq) {
            mBodyParams.put(RequestParams.PHONE, mReq.phone);
            mBodyParams.put(RequestParams.TRANSFER_POSTSCRIPT, mReq.postscript);
            mBodyParams.put(RequestParams.TRANSFER_AMOUNT, mReq.transferAmount);
            if (mReq.isLiShi) {
                mBodyParams.put(RequestParams.TRANSFERCATEGORY, "7");
                mBodyParams.put(RequestParams.TRANSFER_COVERID, mReq.coverId);

            }
        }
    }
}
