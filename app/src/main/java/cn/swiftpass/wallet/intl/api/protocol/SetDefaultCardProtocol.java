package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 设置默认支付卡，
 */

public class SetDefaultCardProtocol extends BaseProtocol {
    public static final String TAG = SetDefaultCardProtocol.class.getSimpleName();
    /**
     * 用户的卡号
     */
    String cardId;
    String defaultPayType;

    public SetDefaultCardProtocol(String cardId, String defaultPayType, NetWorkCallbackListener dataCallback) {
        this.defaultPayType = defaultPayType;
        this.cardId = cardId;
        this.mDataCallback = dataCallback;
        mUrl = "api/card/setDefaultCard";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARD_ID, cardId);
        mBodyParams.put(RequestParams.DEFAULTPAYTYPE, defaultPayType);
    }
}
