package cn.swiftpass.wallet.intl.module.deepLink;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferBaseDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetBusinessResultsProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.BusinessResultsEntity;

import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.getUrlParams;

public class DeepLinkPresenter<P extends DeepLinkContract.View> extends BasePresenter<P> implements DeepLinkContract.Presenter<P> {

    /**
     * 跳转至 UPlan 主页
     */
    @Override
    public void goTpUPlan() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetUplanurlProtocol(new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goTpUPlanFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goTpUPlanSuccess(response);
                }
            }
        }).execute();


    }

    /**
     * 跳转至 UPlan  指定门店
     */
    @Override
    public void goToUPlanStore(String deepLinkUrl) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        HashMap<String, String> map = getUrlParams(deepLinkUrl);
        String merchantInfo = map.get("merchantInfo");
        String parameter = "{\"action\":\"MERCHANT\",\"merchantInfo\":\"" + merchantInfo + "\"}";

        new GetUplanurlProtocol(parameter, new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToUPlanStoreFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToUPlanStoreSuccess(response);
                }
            }
        }).execute();

    }

    /**
     * 跳转至 跨境汇款
     */
    @Override
    public void goToRemittancePage() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetTransferBaseDataProtocol(new NetWorkCallbackListener<TransferCrossBorderBaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToRemittancePageFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderBaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToRemittancePageSuccess(response);
                }
            }
        }).execute();

    }

    /**
     * 跳转至 信用卡登记奖赏
     */
    @Override
    public void goToAppointRewardRegister(String deepLinkUrl) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        String campaignId = getUrlParams(deepLinkUrl).get("campaignId");
        String businessParam = "{\"campaignId\":\"" + campaignId + "\"}";

        new GetBusinessResultsProtocol("CREDIT_CARD_REWARD", businessParam, new NetWorkCallbackListener<BusinessResultsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToAppointRewardRegisterFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BusinessResultsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().goToAppointRewardRegisterSuccess(response);
                }
            }
        }).execute();

    }
}
