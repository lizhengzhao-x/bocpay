package cn.swiftpass.wallet.intl.module.rewardregister.view;


import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardPansListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 信用卡登记奖赏 选择卡列表
 */
public class CheckCardRewardListPop extends BasePopWindow {
    private OnGradeWindowClickListener onGradeWindowClickListener;

    private TextView mId_grade_title;
    private ImageView mId_close_dialog;
    private TextView mTv_confirm;
    private androidx.recyclerview.widget.RecyclerView mId_recyclerView;
    private List<RewardPansListEntity.RewardPan> rewardSelCardList;
    private int selPos;
    private int changePos;

    private void bindViews() {
        mTv_confirm = (TextView) mContentView.findViewById(R.id.tv_confirm);
        mId_grade_title = (TextView) mContentView.findViewById(R.id.id_grade_title);
        mId_close_dialog = (ImageView) mContentView.findViewById(R.id.id_close_dialog);
        mId_recyclerView = (androidx.recyclerview.widget.RecyclerView) mContentView.findViewById(R.id.id_recyclerView);
    }


    public CheckCardRewardListPop(Activity mActivity, List<RewardPansListEntity.RewardPan> gradeInfosIn, int selPosIn, OnGradeWindowClickListener onGradeWindowClickListenerIn) {
        super(mActivity);
        this.onGradeWindowClickListener = onGradeWindowClickListenerIn;
        this.rewardSelCardList = gradeInfosIn;
        this.selPos = selPosIn;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_rewardcard_sel_list);
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.9));
        setHeight((int) (AndroidUtils.getScreenHeight(mActivity) * 0.6));
        bindViews();
        mId_grade_title.setText(mActivity.getString(R.string.CR209_8_1));
        initSelPosition(selPos);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerView.setLayoutManager(layoutManager);
        mId_recyclerView.setAdapter(getAdapter(null));
        ((IAdapter<RewardPansListEntity.RewardPan>) mId_recyclerView.getAdapter()).setData(rewardSelCardList);
        mId_recyclerView.getAdapter().notifyDataSetChanged();
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mTv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGradeWindowClickListener.onGradeItemClickListener(changePos);
                dismiss();
            }
        });
    }

    private void initSelPosition(int selPos) {
        for (int i = 0; i < rewardSelCardList.size(); i++) {
            if (selPos == i) {
                rewardSelCardList.get(i).setSel(true);
            } else {
                rewardSelCardList.get(i).setSel(false);
            }
        }
    }

    private CommonRcvAdapter<RewardPansListEntity.RewardPan> getAdapter(final List<RewardPansListEntity.RewardPan> data) {
        return new CommonRcvAdapter<RewardPansListEntity.RewardPan>(data) {

            @Override
            public Object getItemType(RewardPansListEntity.RewardPan demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new RewardCardListSelAdapter(mActivity, new RewardCardListSelAdapter.OnItemSelListener() {
                    @Override
                    public void onItemChecked(int position) {
                        changePos = position;
                        initSelPosition(position);
                        mId_recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }, true);
            }
        };
    }

    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, Gravity.CENTER, Gravity.CENTER);
    }

    public interface OnGradeWindowClickListener {
        void onGradeItemClickListener(int position);
    }
}
