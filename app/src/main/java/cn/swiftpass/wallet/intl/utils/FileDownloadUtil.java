package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.wallet.intl.entity.Constants;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 文件下载
 */
public class FileDownloadUtil {
    private static FileDownloadUtil downloadUtil;
    private final OkHttpClient okHttpClient;
    private Context mContext;

    public static FileDownloadUtil get(Context mContext) {
        if (downloadUtil == null) {
            downloadUtil = new FileDownloadUtil(mContext);
        }
        return downloadUtil;
    }

    private FileDownloadUtil(Context mContext) {
        this.mContext = mContext;
        okHttpClient = OkHttpManager.getInstance().getOkHttpClientNoInterceptor();
    }

    /**
     * @param url      下载连接
     * @param saveDir  储存下载文件的SDCard目录
     * @param listener 下载监听
     */
    public void download(final String url, String type, String languageType) {
        //url 为null 不需要下载
        if (TextUtils.isEmpty(url)) {
            return;
        }

        Request request;
        try {
            request = new Request.Builder().url(url).build();
        } catch (Exception e) {
            request = null;
        }
        if (null == request) {
            return;
        }

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                try {
                    // 储存下载文件的目录
                    String savePath = getExistDir(mContext, type, languageType);
                    is = response.body().byteStream();
                    long total = response.body().contentLength();
                    File file = new File(savePath, getNameFromUrl(url, type));
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fos.flush();

                    //删除保存的其他文件，这个文件夹里面只能有一个文件
                    deleteDirWihtOutFile(mContext, savePath, type, url, languageType);
                } catch (Exception e) {
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                    }
                }
            }
        });
    }


    /**
     * @param type
     * @return
     * @throws IOException 创建目录
     */
    public static String getExistDir(Context mContext, String type, String languageType) {
        try {
            //如果文件不存在 创建文件
            File downloadFile = new File(getRedDefaultPath(mContext, languageType), type);
            if (!downloadFile.mkdirs()) {
                downloadFile.createNewFile();
            }
            String savePath = downloadFile.getAbsolutePath();
            return savePath;
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * @return
     */
    private static String getRedDefaultPath(Context mContext, String languageType) {
        String cacheDirectory = AndroidUtils.getAppExternalPrivateSpacePath(mContext) + "/" + Constants.RED_SAVE_FILE;
        cacheDirectory = cacheDirectory + "/" + languageType;
        return cacheDirectory;
    }

    public static String getLanguageType() {
        String lan = SpUtils.getInstance().getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            return Constants.RED_SAVE_CN;
        } else if (AndroidUtils.isHKLanguage(lan)) {
            return Constants.RED_SAVE_HK;
        } else {
            return Constants.RED_SAVE_EN;
        }
    }

    /**
     * 删除文件夹里的文件
     *
     * @return
     */
    public static void clearImgResource(Context mContext) {
        if (null == mContext) {
            return;
        }
        //使用和Glide一样的缓存地址
        String cacheDirectory = AndroidUtils.getAppExternalPrivateSpacePath(mContext);
        //得到文件
        File downloadFile = new File(cacheDirectory, Constants.RED_SAVE_FILE);

    }

    //除了当前文件其他文件都删除
    private static void deleteDirWihtOutFile(Context mContext, String path, String type, String url, String languageType) {
        if (null == mContext) {
            return;
        }
        String localPath = FileDownloadUtil.getExistDir(mContext, type, languageType);

        //进入文件夹
        File dir = new File(localPath);
        if (dir == null || !dir.exists() || !dir.isDirectory()) {
            return;
        }
        for (File fileTemp : dir.listFiles()) {
            if (!TextUtils.equals(fileTemp.getName(), getNameFromUrl(url,type)) && fileTemp.isFile()) {
                fileTemp.delete(); // 删除所有文件
            }
        }
    }


    /**
     * @param url
     * @return 从下载连接中解析出文件名
     */
    @NonNull
    public static String getNameFromUrl(String url, String type) {
        if (!TextUtils.isEmpty(url)) {
            //保存的文件不能有/
//            String gifType = Constants.STAFF_GIF;
//            String mp3Type = Constants.STAFF_MP3;
            if (TextUtils.equals(type, Constants.STAFF_MP3)) {
                LogUtils.i("FILEXXX","'URL:"+url + " md5 " + AndroidUtils.md5(url));
                return AndroidUtils.md5(url) + ".mp3";
            }
            return url.replaceAll("/", "");
        }
        return "";
    }

    public interface OnDownloadListener {
        /**
         * 下载成功
         */
        void onDownloadSuccess(String url, String path);

        /**
         * 下载失败
         */
        void onDownloadFailed(String url);
    }


}
