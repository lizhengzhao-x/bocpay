package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class GetUplanurlProtocol extends BaseProtocol {

    private String parameter;

    public GetUplanurlProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/unionpay/uPlan/getUPlanInfo";
    }


    public GetUplanurlProtocol(String parameterIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        parameter = parameterIn;
        mUrl = "api/unionpay/uPlan/getUPlanInfo";
    }


    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(parameter)) {
            mBodyParams.put(RequestParams.PARAMETER, parameter);
        }

    }


}
