package cn.swiftpass.wallet.intl.module.register;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ValueCallback;

import androidx.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CircleWebview;

import static cn.swiftpass.wallet.intl.module.register.CheckIdvProgressActivity.BLACK_LIST;

/**
 * Created by zhangxinchao on 2018/2/27.
 */

public class WebViewVideoActivity extends BaseWebViewActivity {
    @BindView(R.id.webView)
    CircleWebview webView;

    private String lan;
    private ValueCallback<Uri[]> mValueCallback;
    private boolean isAutoBack;
    private RegisterEventEntity mEventEntity;
    private int mPageFlow;


    @Override
    public void init() {
        EventBus.getDefault().register(this);
//        lan = SpUtils.getInstance(mContext).getAppLanguage();
//        switchLanguage(lan);
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        mOriginalUrl = getIntent().getExtras().getString(Constants.DETAIL_URL);
        String content = getIntent().getExtras().getString(Constants.DETAIL_TITLE);
        isAutoBack = getIntent().getExtras().getBoolean(Constants.IS_AUTO_BACK, false);
        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        boolean isVideoPlay = getIntent().getExtras().getBoolean(Constants.IS_VIDEO_PLAY, false);
        if (!isVideoPlay) {
            //视频播放 硬件加速问题
            webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        }
        final boolean isOpenErrorHint = getIntent().getExtras().getBoolean(Constants.WEB_VIEW_ERROR_IS_HINT, false);
        if (!TextUtils.isEmpty(content)) {
            setToolBarTitle(content);
        }
        if (TextUtils.isEmpty(mOriginalUrl)) {
            finish();
        }
        setWebViewClient(webView, isOpenErrorHint,true);
        initWebView(webView, mOriginalUrl);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_webview;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        Uri[] results = new Uri[]{intent.getData()};
        if (mValueCallback != null) {
            mValueCallback.onReceiveValue(results);
            mValueCallback = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.destroy();
            webView = null;
        }
        EventBus.getDefault().unregister(this);
    }

    private void showDialog(final RegisterEventEntity event) {
        String str_title = "   ";
        String str_msg = "";
        str_msg = getString(R.string.IDV_27_3);
        String str_ok = getActivity().getString(R.string.IDV_27_5);
        String str_cancel = getActivity().getString(R.string.IDV_27_4);
        AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_ok, str_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //确认
                Intent in = new Intent();
                in.putExtra(Constants.EVENT_TYPE, mEventEntity.getEventType());
                setResult(RESULT_OK, in);
                finish();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(final RegisterEventEntity event) {
        if (event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_SUCCESS
                || event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_FAILED
                || event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_ERROR
                || event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_ERROR_TWO) {
            if (event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_SUCCESS) {
                event.setEventType(RegisterEventEntity.EVENT_IDV_CHECK_SUCCESS);
            } else if (event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_FAILED) {
                event.setEventType(RegisterEventEntity.EVENT_IDV_CHECK_FAILED);
            } else if (event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_ERROR) {
                showErrorMsgDialog(WebViewVideoActivity.this, event.getEventMessage(), new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        boolean isBlackStatus = false;
                        for (int i = 0; i < BLACK_LIST.length; i++) {
                            if (TextUtils.equals(event.getErrorCode(), BLACK_LIST[i])) {
                                isBlackStatus = true;
                                break;
                            }
                        }
                        if (isBlackStatus) {
                            //中了黑名单 只可能注册/绑卡出现 需要跳转到首页
                            MyActivityManager.removeAllTaskExcludeMainStack();

                        } else {
                            //已经是中银客户 点击跳转到 s3页面注册
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                            } else {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                            }
                            MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                            ActivitySkipUtil.startAnotherActivity(WebViewVideoActivity.this, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    }
                });
                return;
            } else if (event.getEventType() == RegisterEventEntity.EVENT_IDV_SHOW_ERROR_TWO) {
                showErrorMsgDialog(WebViewVideoActivity.this, event.getEventMessage(), new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {


                        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                            //注册流程 不管是黑名单 还是已经注册bocpay 都跳转到登录界面
                            MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                            //绑卡 应该只存在黑名单情况 跳转到选择绑卡界面
                            MyActivityManager.removeAllTaskWithPaBindCard();
                        } else {
                            //异常 结束流程
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                            ActivitySkipUtil.startAnotherActivity(WebViewVideoActivity.this, CheckIdvFailedActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    }
                });
                return;
            }
            mEventEntity = event;
            showDialog(event);
        }
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {

        if (webView.canGoBack() && !isAutoBack) {
            webView.goBack();
        } else {
            if (mEventEntity != null) {
                Intent in = new Intent();
                in.putExtra(Constants.EVENT_TYPE, mEventEntity.getEventType());
                setResult(RESULT_OK, in);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

}
