package cn.swiftpass.wallet.intl.module.creditcardreward.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetMyCreditCardRewardListProtocol;
import cn.swiftpass.wallet.intl.entity.MyCreditCardRewardActionList;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.MyCreditCardRewardContact;

public class MyCreditCardRewardPresent extends BasePresenter<MyCreditCardRewardContact.View> implements MyCreditCardRewardContact.Presenter {
    public String testJson = "{\"currentPage\":\"5\",\"hasNext\":\"1\",\"rows\":[{\"account\":\"信用卡（1234）\",\"accumulatedSpendingAmt\":\"HKD 100,000.00\",\"bannerUrl\":\"xxxxx.jpg\",\"completionRate\":\"0.75\",\"content\":\"大湾区一卡通test test test test test test test test test test test test test test test test test test test test\",\"deadline\":\"2021-07-01\",\"details\":\"xxxxxxx.html\",\"earnPointsForReachingTarget\":\"可获得12万积分\",\"lastUpdateDate\":\"2021-07-01\",\"obtainReward\":\"60000分\",\"oweArrivalsSpendingAmt\":\"HKD 5,000.00\",\"rewardsType\":\"1\"},{\"account\":\"信用卡（1234）\",\"accumulatedSpendingAmt\":\"HKD 100,000.00\",\"bannerUrl\":\"xxxxx.jpg\",\"completionRate\":\"0.75\",\"content\":\"大湾区一卡通test test test test test test test test test test test test test test test test test test test test\",\"deadline\":\"2021-07-01\",\"details\":\"xxxxxxx.html\",\"lastUpdateDate\":\"2021-07-01\",\"obtainReward\":\"HKD 65(100)\",\"oweArrivalsSpendingAmt\":\"HKD 5,000.00\",\"rewardsType\":\"0\"}]}";
    int i = 0;

    @Override
    public void loadCreditCardReward(String dateType, String currentPage) {
   /*     i++;
        MyCreditCardRewardActionList cardRewardActionList = new Gson().fromJson(testJson, MyCreditCardRewardActionList.class);
        if (getView() != null) {
            if (i == 4) {
                cardRewardActionList.setHasNext("0");
            }
            getView().getCreditCardRewardListSuccess(cardRewardActionList);
        }*/
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new GetMyCreditCardRewardListProtocol(dateType, currentPage, new NetWorkCallbackListener<MyCreditCardRewardActionList>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCreditCardRewardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MyCreditCardRewardActionList response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCreditCardRewardListSuccess(response);
                }
            }
        }).execute();
    }
}
