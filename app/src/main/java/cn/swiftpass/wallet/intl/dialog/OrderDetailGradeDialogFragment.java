package cn.swiftpass.wallet.intl.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.alibaba.fastjson.JSONObject;
import com.bocpay.analysis.AnalysisManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * 收银台  积分
 */
public class OrderDetailGradeDialogFragment extends DialogFragment {

    private ImageView mId_close_dialog;
    private TextView mId_read_info;
    private ImageView mId_question;
    private TextView mId_pay_symbol;
    private TextView mId_paid_money;
    private TextView mId_paidTo;
    private TextView mId_sub_title;
    private TextView mId_term_rule;
    private ImageView mId_right_card;
    private TextView mId_paidvia;
    private TextView mId_grade_total;
    private TextView mId_grade_deduct;
    private TextView mId_grade_retain;
    private TextView mId_dikou_money;
    private TextView mId_title_str;
    private TextView mTv_order_ok;
    private TextView mTv_order_jifen;
    private TextView mTv_order_confirm;
    private LinearLayout mId_linear_grade;
    private LinearLayout mId_linear_grade_btn;
    private LinearLayout mId_nograde_order_confirm;
    private LinearLayout mIdir_right_view;

    //    private TextView mId_replease_text;
    private ImageView mId_read_info_img;
    private TextView mId_buttom_text;
    private ImageView mIv_gr_centerimage;
    private View mId_item_line;
    private View mId_grade_reduce_info_view;


    //支付方式
    private BankCardEntity mBankCardEntity;
    //积分收银台展示对象 主扫
    private ActionTrxGpInfoEntity actionTrxGpInfoEntity;
    //积分收银台展示对象 被扫
    private SweptCodePointEntity sweptCodePointEntity;
    private String defaultCardId;
    private OnDialogClickListener mOnPopClickListener;
    //收银台是否可以切换支付方式 被扫不允许更改
    private boolean isCanChangeCard = true;
    //是否是被扫
    private boolean isBackScan;
    private boolean isDissmissAuto = true;

    private long currentPageCreateTime;

    public void setInterceptClickEvent(boolean interceptClickEvent) {
        isInterceptClickEvent = interceptClickEvent;
    }

    /**
     * 主要是为了防止主扫下单dialog没及时显示 然后用户重复点击按钮
     */
    private boolean isInterceptClickEvent;

    private void bindViews(View view) {

        currentPageCreateTime = System.currentTimeMillis();
        mId_close_dialog = (ImageView) view.findViewById(R.id.id_close_dialog);
        mId_read_info = (TextView) view.findViewById(R.id.id_read_info);
        mId_question = (ImageView) view.findViewById(R.id.id_question);
        mId_pay_symbol = (TextView) view.findViewById(R.id.id_pay_symbol);
        mId_paid_money = (TextView) view.findViewById(R.id.id_paid_money);
        mId_paidTo = (TextView) view.findViewById(R.id.id_paidTo);
        mId_read_info_img = (ImageView) view.findViewById(R.id.id_read_info_img);
        mId_right_card = (ImageView) view.findViewById(R.id.id_right_card);
        mIv_gr_centerimage = (ImageView) view.findViewById(R.id.iv_gr_centerimage);
        mId_paidvia = (TextView) view.findViewById(R.id.id_paidvia);
        mId_grade_total = (TextView) view.findViewById(R.id.id_grade_total);
        mId_grade_deduct = (TextView) view.findViewById(R.id.id_grade_deduct);
        mId_grade_reduce_info_view = view.findViewById(R.id.id_grade_reduce_info_view);
        mId_grade_retain = (TextView) view.findViewById(R.id.id_grade_retain);
        mId_title_str = (TextView) view.findViewById(R.id.id_title_str);
        mId_dikou_money = (TextView) view.findViewById(R.id.id_dikou_money);
        mId_buttom_text = (TextView) view.findViewById(R.id.id_buttom_text);
        mTv_order_ok = (TextView) view.findViewById(R.id.tv_order_ok);
        mId_sub_title = (TextView) view.findViewById(R.id.id_sub_title);
        mId_term_rule = (TextView) view.findViewById(R.id.id_term_rule);
//        mId_replease_text = (TextView) view.findViewById(R.id.id_replease_text);
        mTv_order_jifen = (TextView) view.findViewById(R.id.tv_order_jifen);
        mTv_order_confirm = (TextView) view.findViewById(R.id.tv_order_confirm);
        mId_linear_grade = (LinearLayout) view.findViewById(R.id.id_linear_grade);
        mId_linear_grade_btn = (LinearLayout) view.findViewById(R.id.id_linear_grade_btn);
        mId_nograde_order_confirm = (LinearLayout) view.findViewById(R.id.tv_nograde_order_confirm);
        mIdir_right_view = (LinearLayout) view.findViewById(R.id.ir_right_view);
        mId_item_line = view.findViewById(R.id.id_item_line);
        mTv_order_confirm.setVisibility(View.GONE);

        // 服务条款 更换词条的时候 需要添加% 为了适配文字
        String totalStr = getResources().getString(R.string.CCP2105_3_11);
        String[] arrays = totalStr.split("##");
        final int startIndex = arrays[0].length();
        final int endIndex = arrays[0].length() + arrays[1].length();
        final String title = totalStr.replace("##", "");
        SpannableString spannableString = new SpannableString(title);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                String lan = SpUtils.getInstance(getContext()).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_SIMP_URL);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_TRAD_URL);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_ENGLISH_URL);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        mId_term_rule.setText(spannableString);
        mId_term_rule.setMovementMethod(LinkMovementMethod.getInstance());


        addListener();
    }

    private void addListener() {
        mTv_order_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInterceptClickEvent) return;
                if (!ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
                    mOnPopClickListener.onOkListener(false);
                }
            }
        });
        mTv_order_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInterceptClickEvent) return;
                if (!ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
                    mOnPopClickListener.onOkListener(false);
                }
            }
        });
        mTv_order_jifen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInterceptClickEvent) return;
                if (!ButtonUtils.isFastDoubleClick(view.getId(), 300)) {
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_SAC_PAY_POINT, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_SCAN_PAGE,
                                PagerConstant.ADDRESS_PAGE_SCAN_TO_PAY_PAGE, System.currentTimeMillis() - currentPageCreateTime);
                        JSONObject jsonObject = analysisButtonEntity.getJsonObject();
                        AnalysisManager.getInstance().sendAnalysisEvent(jsonObject);
                    }
                    mOnPopClickListener.onOkListener(true);
                }
            }
        });
        mIdir_right_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInterceptClickEvent) return;
                mOnPopClickListener.onSelCardListener();
            }
        });

        mId_right_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isInterceptClickEvent) return;
                mOnPopClickListener.onSelCardListener();
            }
        });
        if (!isCanChangeCard) {
            mIdir_right_view.setEnabled(false);
            mId_right_card.setVisibility(View.GONE);
        }
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onDisMissListener();
                if (isDissmissAuto) {
                    dismiss();
                }
            }
        });
        mId_read_info_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showErrorMsgDialog(getContext(), getResources().getString(R.string.LYP06_01_2), getResources().getString(R.string.CP1_4a_16));
            }
        });


        mId_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showErrorMsgDialog(getContext(), getResources().getString(R.string.A1_4_1));
            }
        });

        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface anInterface, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    public void setDefaultCardId(BankCardEntity bankCardEntity) {
        if (null != bankCardEntity && !TextUtils.isEmpty(bankCardEntity.getPanShowNumber())) {
            mBankCardEntity = bankCardEntity;
            this.defaultCardId = bankCardEntity.getPanShowNumber();
            this.actionTrxGpInfoEntity.setCardId(bankCardEntity.getCardId());
            mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(getContext(), defaultCardId, mBankCardEntity.getCardType()));
            if (!TextUtils.isEmpty(bankCardEntity.getCardType()) && bankCardEntity.getCardType().equals("2") && actionTrxGpInfoEntity.getIsCommonQR()) {
                //智能账户才需要显示
                mId_sub_title.setVisibility(View.VISIBLE);
                mId_sub_title.setText(bankCardEntity.getIsFPS().equals("1") ? getResources().getString(R.string.A1_3_1) : getResources().getString(R.string.A1_2_1));

            } else {
                mId_sub_title.setVisibility(View.GONE);
            }
            if (actionTrxGpInfoEntity.getIsCommonQR()) {
                mId_question.setVisibility(View.VISIBLE);
            } else {
                mId_question.setVisibility(View.INVISIBLE);
            }

        }
    }

    public boolean isFpsPaymentType() {
        if (mBankCardEntity != null) {
            if (TextUtils.isEmpty(mBankCardEntity.getIsFPS())) {
                return false;
            } else {
                return mBankCardEntity.getIsFPS().equals("1") && actionTrxGpInfoEntity.getIsCommonQR();
            }
        } else {
            if (actionTrxGpInfoEntity != null) {
                return actionTrxGpInfoEntity.getIsFPS().equals("1") && actionTrxGpInfoEntity.getIsCommonQR();
            } else {
                return false;
            }

        }
    }

    public void updateParams(ActionTrxGpInfoEntity actionTrxGpInfoEntity) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        updateMajorScanUI();
    }

    /**
     * 获取当前选中的支付方式
     *
     * @return
     */
    public String getCurrentSelCardId() {
        return actionTrxGpInfoEntity.getCardId();
    }

    /**
     * 主扫 拉起收银台
     *
     * @param actionTrxGpInfoEntity      对象不同
     * @param onPopWindowOkClickListener
     */
    public void initParams(ActionTrxGpInfoEntity actionTrxGpInfoEntity, OnDialogClickListener onPopWindowOkClickListener) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        this.mOnPopClickListener = onPopWindowOkClickListener;
    }


    public void initParams(ActionTrxGpInfoEntity actionTrxGpInfoEntity, OnDialogClickListener onPopWindowOkClickListener, boolean isDissmissAutoIn) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        this.mOnPopClickListener = onPopWindowOkClickListener;
        this.isDissmissAuto = isDissmissAutoIn;
    }


    /**
     * 主扫 拉起收银台
     *
     * @param actionTrxGpInfoEntity      对象不同
     * @param onPopWindowOkClickListener
     */
    public void initParams(ActionTrxGpInfoEntity actionTrxGpInfoEntity, boolean isCanChangeCardIn, OnDialogClickListener onPopWindowOkClickListener) {
        this.initParams(actionTrxGpInfoEntity, onPopWindowOkClickListener);
        isCanChangeCard = isCanChangeCardIn;

    }


    /**
     * 被扫拉起收银台
     *
     * @param sweptCodePointEntity
     * @param isCanChangeCard
     * @param onPopWindowOkClickListener
     */
    public void initParams(SweptCodePointEntity sweptCodePointEntity, boolean isCanChangeCard, boolean isBackScan, OnDialogClickListener onPopWindowOkClickListener) {
        this.sweptCodePointEntity = sweptCodePointEntity;
        this.mOnPopClickListener = onPopWindowOkClickListener;
        this.isCanChangeCard = isCanChangeCard;
        this.isBackScan = isBackScan;
    }


    /**
     * 主扫拉起收银台 信息更新
     */
    private void updateMajorScanUI() {
        mId_title_str.setText(getResources().getString(R.string.pop_order_detail));
        mId_grade_reduce_info_view.setVisibility(View.VISIBLE);
        try {
            mId_paid_money.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(actionTrxGpInfoEntity.getTranAmt()), true));
        } catch (NumberFormatException e) {
        }
        mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(getContext(), actionTrxGpInfoEntity.getPanFour(), actionTrxGpInfoEntity.getCardType()));
        mId_paidTo.setText(actionTrxGpInfoEntity.getMerchantName());
        mId_pay_symbol.setText(AndroidUtils.getTrxCurrency(actionTrxGpInfoEntity.getTranCur()));
        mId_question.setVisibility(View.INVISIBLE);
        if (mBankCardEntity == null) {
            //当前没有选择卡的时候 根据返回结果判断是否显示 fps/银联
            if (!TextUtils.isEmpty(actionTrxGpInfoEntity.getCardType()) && actionTrxGpInfoEntity.getCardType().equals("2") && actionTrxGpInfoEntity.getIsCommonQR()) {
                //智能账户才需要显示
                mId_sub_title.setVisibility(View.VISIBLE);
                mId_sub_title.setText(actionTrxGpInfoEntity.getIsFPS().equals("1") ? getResources().getString(R.string.A1_3_1) : getResources().getString(R.string.A1_2_1));
            } else {
                mId_sub_title.setVisibility(View.GONE);
            }
            if (actionTrxGpInfoEntity.getIsCommonQR()) {
                mId_question.setVisibility(View.VISIBLE);
            } else {
                mId_question.setVisibility(View.INVISIBLE);
            }
        } else {
            setDefaultCardId(mBankCardEntity);
        }
        TextPaint tp = mId_dikou_money.getPaint();
        tp.setFakeBoldText(false);
        mId_item_line.setVisibility(View.VISIBLE);
        if (TextUtils.isEmpty(actionTrxGpInfoEntity.getRedeemFlag()) || actionTrxGpInfoEntity.getRedeemFlag().equals("0")) {
            //不使用积分
            mId_linear_grade.setVisibility(View.GONE);
            mId_nograde_order_confirm.setVisibility(View.VISIBLE);
            mId_linear_grade_btn.setVisibility(View.GONE);
            mTv_order_confirm.setVisibility(View.VISIBLE);
            mId_item_line.setVisibility(View.GONE);
        } else {
            try {
                mId_grade_retain.setText(AndroidUtils.formatPrice(Double.valueOf(actionTrxGpInfoEntity.getGpBalance()), false));
                mId_grade_total.setText(AndroidUtils.formatPrice(Double.valueOf(actionTrxGpInfoEntity.getAvaGpCount()), false));
                mId_dikou_money.setText(AndroidUtils.getTrxCurrency(actionTrxGpInfoEntity.getTranCur()) + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(actionTrxGpInfoEntity.getRedeemGpTnxAmt()), false));
            } catch (NumberFormatException e) {
            }

            mId_dikou_money.setTextColor(actionTrxGpInfoEntity.getColorSign());
            mId_grade_deduct.setText(AndroidUtils.formatPrice(Double.valueOf(actionTrxGpInfoEntity.getRedeemGpCount()), false));
            //够积分换领
            mTv_order_jifen.setBackgroundResource(R.drawable.bg_btn_next_page);
            mTv_order_jifen.setEnabled(true);
            //使用积分
            mId_linear_grade.setVisibility(View.VISIBLE);
            mId_linear_grade_btn.setVisibility(View.VISIBLE);
            mId_nograde_order_confirm.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(actionTrxGpInfoEntity.getGpDateCmt())) {
                mId_buttom_text.setText(actionTrxGpInfoEntity.getGpDateCmt());
            } else {
                mId_buttom_text.setVisibility(View.GONE);
            }
        }

        if (!TextUtils.isEmpty(actionTrxGpInfoEntity.getGpMsgUrl())) {
            if (actionTrxGpInfoEntity.getGpMsgUrl().endsWith(".gif")) {
                GlideApp.with(this).load(actionTrxGpInfoEntity.getGpMsgUrl())
                        .skipMemoryCache(true)
                        .placeholder(R.mipmap.banner_white)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mIv_gr_centerimage);
            } else {
                GlideApp.with(this).load(actionTrxGpInfoEntity.getGpMsgUrl())
                        .skipMemoryCache(true)
                        .placeholder(R.mipmap.banner_white)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mIv_gr_centerimage);
            }
        }
    }

    /**
     * 被扫收银台更新
     */
    private void updateBackScanUI() {
        mId_grade_reduce_info_view.setVisibility(View.VISIBLE);
        mId_title_str.setText(getResources().getString(R.string.CP2_5a_1));
        mId_right_card.setVisibility(View.GONE);
        try {
            mId_paid_money.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(sweptCodePointEntity.getTrxAmt()), true));
        } catch (NumberFormatException e) {
        }
        mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(getContext(), sweptCodePointEntity.getPanFour(), sweptCodePointEntity.getCardType()));
        mId_paidTo.setText(sweptCodePointEntity.getMerchantName());
        mId_pay_symbol.setText(AndroidUtils.getTrxCurrency(sweptCodePointEntity.getTranCur()));

        mId_grade_total.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getAvaGpCount()), false));
        mId_grade_deduct.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getRedeemGpCount()), false));
        mId_grade_retain.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getGpBalance()), false));

        mId_dikou_money.setTextColor(actionTrxGpInfoEntity.getColorSign());

        try {
            mId_dikou_money.setText(AndroidUtils.getTrxCurrency(sweptCodePointEntity.getTranCur()) + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(sweptCodePointEntity.getRedeemGpTnxAmt()), false));
        } catch (NumberFormatException e) {
        }
        TextPaint tp = mId_dikou_money.getPaint();
        tp.setFakeBoldText(false);
        mId_dikou_money.setTextColor(sweptCodePointEntity.getColorSign());

        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpDateCmt())) {
            mId_buttom_text.setText(sweptCodePointEntity.getGpDateCmt());
        } else {
            mId_buttom_text.setVisibility(View.GONE);
        }
        mId_linear_grade_btn.setVisibility(View.GONE);
        mTv_order_confirm.setVisibility(View.VISIBLE);
        mTv_order_confirm.setText(getString(R.string.P3_A1_18b_13));
        mId_nograde_order_confirm.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpDateCmt())) {
            mId_buttom_text.setText(sweptCodePointEntity.getGpDateCmt());
        } else {
            mId_buttom_text.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(sweptCodePointEntity.getRedeemFlag()) && sweptCodePointEntity.getRedeemFlag().equals("0")) {
            mTv_order_confirm.setBackgroundResource(R.drawable.bg_btn_next_page_disable);
            mTv_order_confirm.setEnabled(false);
            mTv_order_confirm.setText(getString(R.string.CP1_4a_11));
        } else {
            mTv_order_confirm.setBackgroundResource(R.drawable.bg_btn_next_page);
            mTv_order_confirm.setTextColor(Color.WHITE);
            mTv_order_confirm.setEnabled(true);
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_order_jifen_detail, container, false);
        bindViews(view);
        isInterceptClickEvent = false;
        if (isBackScan) {
            updateBackScanUI();
        } else {
            updateMajorScanUI();
        }
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    public interface OnDialogClickListener {
        void onOkListener(boolean point);

        void onSelCardListener();

        void onDisMissListener();
    }
}
