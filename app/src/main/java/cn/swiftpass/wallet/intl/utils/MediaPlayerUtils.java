package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;

import cn.swiftpass.boc.commonui.base.utils.LogUtils;


public class MediaPlayerUtils {
    private static MediaPlayer mMediaPlayer;
    private static final String TAG = "MediaPlayerUtils";

    public static void startPlayMusic(Context mContext, Uri uri) {
        if (uri == null) return;
        try {
//            mMediaPlayer = MediaPlayer.create(mContext,uri);
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setLooping(true);
            ///storage/emulated/0/red_save_file/red_save_en/staff_mp3/https:whkbcuat.ftcwifi.comcreditcardBOCPAYsoundsred_packetsmpbackground_music_staff.mp3?version=202101072003
//            mMediaPlayer.setDataSource(uri.toString());
//            https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/sounds/red_packet/smp/background_music_staff.mp3?version=202101072003
//            File file = new File("/storage/emulated/0/red_save_file/red_save_cn/staff_mp3/https:whkbcuat.ftcwifi.comcreditcardBOCPAYsoundsred_packetsmpbackground_music_staff.mp3?version=202101072003");
            mMediaPlayer.setDataSource(mContext, uri);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mMediaPlayer.start();
                }
            });
        } catch (Exception e) {
            LogUtils.i(TAG, "" + e.getLocalizedMessage());
        }

    }

    public static void stopPlayMusic() {
        if (mMediaPlayer != null) {
            LogUtils.i(TAG, "stopPlayMusic");
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }
}

