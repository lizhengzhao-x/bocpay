package cn.swiftpass.wallet.intl.module.setting.presenter;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteListEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.InviteContactContract;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;

public class InviteContactPresenter extends BasePresenter<InviteContactContract.View> implements InviteContactContract.Presenter {

    @Override
    public void getInviteContact(List<InviteContactEntity> accountList) {
//        getView().showDialogNotCancel();

        InviteListEntity inviteListEntity = new InviteListEntity();
        inviteListEntity.result = accountList;
        getView().getInviteContactSuccess(inviteListEntity);

//        new InviteContactProtocol(accountList, new NetWorkCallbackListener<InviteListEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                getView().dismissDialog();
//                getView().getInviteContactFail(errorCode, errorMsg);
//            }
//
//            @Override
//            public void onSuccess(InviteListEntity response) {
//                getView().dismissDialog();
//                getView().getInviteContactSuccess(response);
//            }
//        }).execute();
    }

    @Override
    public void initPageData(final Context mContext, final int currentPage, final int pageSize, final List<InviteContactEntity> allContactList) {
        if (allContactList.size() == 0 || currentPage == 1) {
            allContactList.clear();
            ContactUtils.getInstance().getAllContactsAsyn(mContext, true, new ContactUtils.OnReadContactsSuccessCallBack() {
                @Override
                public void OnReadContactsSuccess(ArrayList<ContactEntity> contactEntities) {

                    //去除所有特殊符号与空格，只认可13位、11位、8位纯数字为香港\澳门\大陆电话号码；
                    //② 如果为13位数字，必须为86开头，且第三位数字为1，否则认定为非电话号码；
                    //③如果为11位数字，
                    //        i. 若为1开头，则为大陆电话号码；
                    //        ii. 若为852\853开头，检验第四位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
                    //④ 如果为8位数字； 检验第1位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；

                    for (ContactEntity contactEntity : contactEntities) {
                        InviteContactEntity inviteContactEntity = new InviteContactEntity();
                        String number = contactEntity.getNumber();
                        //去除所有特殊符号与空格 先只处理-/+/(/)/空格
                        number = number.
                                replace("-", "").
                                replace("+", "").
                                replace(" ", "").
                                replace("(", "").
                                replace(")", "");
                        if (!TextUtils.isEmpty(number) && isNumeric(number)) {
                            //只认可13位、11位、8位纯数字为香港\澳门\大陆电话号码；
                            if (number.length() == 8 || number.length() == 11 || number.length() == 13) {
                                if (number.length() == 13) {
                                    //如果为13位数字，必须为86开头，且第三位数字为1，否则认定为非电话号码；
                                    if (number.startsWith("861")) {
                                        isZhNumber(inviteContactEntity, contactEntity, allContactList);
                                    } else {
                                        LogUtils.i("DEAL:", "invailable " + number);
                                    }
                                } else if (number.length() == 11) {
                                    // ii. 若为852\853开头，检验第四位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
                                    if (number.startsWith("852") || number.startsWith("853")) {
                                        isHkPhoneNumber(number, inviteContactEntity, contactEntity, allContactList);
                                    } else if (number.startsWith("1")) {
                                        // i. 若为1开头，则为大陆电话号码；
                                        isZhNumber(inviteContactEntity, contactEntity, allContactList);
                                    } else {
                                        LogUtils.i("DEAL:", "invailable " + number);
                                    }
                                } else {
                                    //如果为8位数字； 检验第1位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
                                    isHkPhoneNumberFirst(number, inviteContactEntity, contactEntity, allContactList);
                                }
                            } else {
                                LogUtils.i("DEAL:", "invailable " + number);
                            }
                        } else {
                            LogUtils.i("DEAL:", "invailable " + number);
                        }
                    }
                    setData(pageSize, currentPage, allContactList);
                }
            });
        } else {
            setData(pageSize, currentPage, allContactList);
        }
    }

    private void isZhNumber(InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        inviteContactEntity.setAccount(contactEntity.getNumber());
        inviteContactEntity.setName(contactEntity.getUserName());
        allContactList.add(inviteContactEntity);
    }

    private void isHkPhoneNumberFirst(String number, InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        if (TextUtils.equals(number.charAt(0) + "", "0") ||
                TextUtils.equals(number.charAt(0) + "", "1") ||
                TextUtils.equals(number.charAt(0) + "", "2") ||
                TextUtils.equals(number.charAt(0) + "", "3")) {
            //非法手机号
            LogUtils.i("DEAL:", "invailable " + number);
        } else {
            //香港\澳门手机号码；
            inviteContactEntity.setAccount(contactEntity.getNumber());
            inviteContactEntity.setName(contactEntity.getUserName());
            allContactList.add(inviteContactEntity);
        }
    }

    private void isHkPhoneNumber(String number, InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        if (TextUtils.equals(number.charAt(3) + "", "0") ||
                TextUtils.equals(number.charAt(3) + "", "1") ||
                TextUtils.equals(number.charAt(3) + "", "2") ||
                TextUtils.equals(number.charAt(3) + "", "3")) {
            //非法手机号
            LogUtils.i("DEAL:", "invailable " + number);
        } else {
            //香港\澳门手机号码；
            inviteContactEntity.setAccount(contactEntity.getNumber());
            inviteContactEntity.setName(contactEntity.getUserName());
            allContactList.add(inviteContactEntity);
        }
    }


    /**
     * 是否是纯数字
     *
     * @param str
     * @return
     */
    public boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    private void setData(int pageSize, int currentPage, List<InviteContactEntity> allContactList) {
        int size = pageSize * currentPage;
        int startPosition = pageSize * (currentPage - 1);
        if (allContactList.size() < pageSize * currentPage) {
            size = allContactList.size();
        }

        /**请求接口数据*/
        List<InviteContactEntity> dataList = new ArrayList<>();
        for (int i = startPosition; i < size; i++) {
            InviteContactEntity inviteContactEntity = allContactList.get(i);
            InviteContactEntity data = new InviteContactEntity();
            data.setName(inviteContactEntity.getName());
            data.setAccount(inviteContactEntity.getAccount());
            dataList.add(data);
        }
        if (getView() != null) {
            getView().getDataSuccess(dataList,allContactList.size());
        }
    }
}
