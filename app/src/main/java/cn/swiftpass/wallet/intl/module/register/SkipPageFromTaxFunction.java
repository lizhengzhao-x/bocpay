package cn.swiftpass.wallet.intl.module.register;

@FunctionalInterface
public interface SkipPageFromTaxFunction {
    void skipNextPage();
}
