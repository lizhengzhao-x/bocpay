package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.entity.AccountPointDataEntity;


public class QueryAccountDataProtocol extends BaseProtocol {

    @Override
    public boolean isNeedLogin() {
        return true;
    }


    public QueryAccountDataProtocol(NetWorkCallbackListener<AccountPointDataEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/postLogin/querySmartAndGpInfo";
    }
}
