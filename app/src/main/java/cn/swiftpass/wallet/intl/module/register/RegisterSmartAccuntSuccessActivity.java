package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 注册我的账户成功界面
 */

public class RegisterSmartAccuntSuccessActivity extends BaseCompatActivity {
    public final static String DATA_REGISTER_SUCCESS = "DATA_REGISTER_SUCCESS";
    @BindView(R.id.tv_invite_intro)
    TextView tvInviteIntro;
    @BindView(R.id.tv_register_number)
    TextView tvRegisterNumber;
    @BindView(R.id.tv_smart_account)
    TextView tvSmartAccount;
    @BindView(R.id.ll_smart_account)
    LinearLayout llSmartAccount;
    @BindView(R.id.tv_bind_new_card)
    TextView tvBindNewCard;
    @BindView(R.id.tv_use_now)
    TextView tvUseNow;

    @BindView(R.id.id_buttom_hinttext)
    TextView idButtomHintText;

    private String rewardtip;//鉴赏提示
    private String phoneStr;//手机号码
    private String smartAccount;//智能账户
    private RegSucEntity successEntity;//成功model
    private boolean showInvitePage;//是否跳到邀请页面
    private int mTypeFlag;//标记注册或者绑定
    private boolean haveVCCard;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.string_regist_smart_account);
        setToolBarTitle(R.string.IDV_2_1);
        hideBackIcon();
        if (getIntent() != null) {
            phoneStr = getIntent().getStringExtra(Constants.DATA_PHONE_NUMBER);
            smartAccount = getIntent().getStringExtra(Constants.SMART_ACCOUNT);
            successEntity = (RegSucEntity) getIntent().getSerializableExtra(DATA_REGISTER_SUCCESS);
            rewardtip = getIntent().getStringExtra(Constants.REWARDTIP);
            mTypeFlag = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
            showInvitePage = getIntent().getBooleanExtra(Constants.SHOWINVITEPAGE, false);
            haveVCCard = getIntent().getBooleanExtra(Constants.HAVE_VC_CARD, false);
        }

        if (haveVCCard) {
            tvBindNewCard.setVisibility(View.VISIBLE);
        } else {
            tvBindNewCard.setVisibility(View.INVISIBLE);
            idButtomHintText.setVisibility(View.INVISIBLE);
        }

        tvSmartAccount.setText(smartAccount);

        if (!TextUtils.isEmpty(rewardtip)) {
            tvInviteIntro.setText(rewardtip);
            tvInviteIntro.setVisibility(View.VISIBLE);
        } else {
            tvInviteIntro.setVisibility(View.INVISIBLE);
        }
        tvRegisterNumber.setText(AndroidUtils.formatCenterPhoneNumberStr(phoneStr));


    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_smart_success;
    }


    /**
     * 判断是否跳转到输入邀请码界面
     */
    private void clickUseByNow() {
        if (showInvitePage) {
            //首次注册
            //智能账户 跳转到 输入邀请码界面
            Intent intent = new Intent(this, RegisterInviteActivity.class);
//            boolean isNeedJumpNotification = false;
//            if (getIntent() != null) {
//                intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//                isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//            }
            intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
//            intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
            startActivity(intent);
        } else {
            MyActivityManager.removeAllTaskExcludeMainStack();
            //如果是在登录态 可以把bocpay变成智能账户
            Intent intent = new Intent(this, MainHomeActivity.class);
            intent.putExtra(Constants.ISREGESTER, true);
//            boolean isNeedJumpNotification = false;
//            if (getIntent() != null) {
//                isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//                intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//            }
//            intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
            startActivity(intent);
            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @OnClick({R.id.tv_bind_new_card, R.id.tv_use_now})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_bind_new_card:
                //注册成功显示通知中心弹框 涉及到一键绑定流程太长就不往后边带
//                boolean isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//                CacheManagerInstance.getInstance().setShowNotificationCenter(isNeedJumpNotification);
//                因为注册成功会返回要不要显示迎新的banenr 其他注册方式都是通过intent携带参数到首页，这里涉及到一键绑定信用卡特别长的流程暂时通过单例保存
//                CacheManagerInstance.getInstance().setRegisterPopupBannerUrl(getIntent().getStringExtra(IS_NEED_SHOW_REGISTER_POPUP_BANNER));
                VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false, new OnPwdVerifyCallBack() {
                    @Override
                    public void onVerifySuccess() {
//                        CacheManagerInstance.getInstance().setRegisterPopupBannerUrl(getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
                        ActivitySkipUtil.startAnotherActivity(RegisterSmartAccuntSuccessActivity.this, BindNewCardActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    @Override
                    public void onVerifyFailed(String errorCode, String errorMsg) {
                        showErrorMsgDialog(RegisterSmartAccuntSuccessActivity.this, errorMsg);
                    }

                    @Override
                    public void onVerifyCanceled() {

                    }
                });
                break;
            case R.id.tv_use_now:
                clickUseByNow();
                break;
        }
    }
}
