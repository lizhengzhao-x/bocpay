package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedeemGiftContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.RedeemGiftPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcoupConvertDescAdapter;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * Created by ZhangXinchao on 2019/8/9.
 */
public class EcouponsConvertDescripActivity extends BaseCompatActivity<RedeemGiftContract.Presenter> implements RedeemGiftContract.View {
    @BindView(R.id.id_sel_ecoup_recycleview)
    RecyclerView idSelEcoupRecycleview;
    @BindView(R.id.id_use_card_title)
    TextView idUseCardTitle;
    @BindView(R.id.id_use_card_reduce_grade)
    TextView idUseCardReduceGrade;
    @BindView(R.id.id_use_card_retain_grade)
    TextView idUseCardRetainGrade;
    @BindView(R.id.id_terms_condition)
    TextView idTermCondition;
    @BindView(R.id.tv_next_step)
    TextView tvNextStep;
    private List<RedeemableGiftListEntity.EVoucherListBean> eVoucherListBeans;
    /**
     * 电子券唯一标识
     */
    private String redmId;

    /**
     * gift类型
     */
    private String giftTp;

    /**
     * panid
     */
    private String panId;

    /**
     * panid
     */
    private String panIdHide;

    /**
     * panType
     */
    private String panType;

    /**
     * 需要抵扣积分
     */
    private String totalGrade;
    /**
     * 剩余积分
     */
    private String retainGrade;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC06_1));
        eVoucherListBeans = (List<RedeemableGiftListEntity.EVoucherListBean>) getIntent().getExtras().getSerializable(Constants.ECOUPONCONVERTENTITY);
        giftTp = getIntent().getExtras().getString(Constants.GIFTTP);
        redmId = getIntent().getExtras().getString(Constants.REDMID);
        panId = getIntent().getExtras().getString(Constants.PAN_ID);
        panIdHide = getIntent().getExtras().getString(Constants.PAN_ID_HIDE);
        panType = getIntent().getExtras().getString(Constants.PAN_TYPE);

        totalGrade = getIntent().getExtras().getString(Constants.TOTAL_GRADE);
        retainGrade = getIntent().getExtras().getString(Constants.RETAIN_GRADE);


        idUseCardReduceGrade.setText(getString(R.string.EC05c_3) + AndroidUtils.formatPrice(Double.valueOf(totalGrade), false));
        idUseCardRetainGrade.setText(getString(R.string.EC05c_4) + AndroidUtils.formatPrice(Double.valueOf(retainGrade), false));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idSelEcoupRecycleview.setLayoutManager(layoutManager);
        idSelEcoupRecycleview.setAdapter(getAdapter(null));
        ((IAdapter<RedeemableGiftListEntity.EVoucherListBean>) idSelEcoupRecycleview.getAdapter()).setData(eVoucherListBeans);
        idSelEcoupRecycleview.getAdapter().notifyDataSetChanged();
        idUseCardTitle.setText(panType + "  " + panIdHide);
        tvNextStep.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                commitConvertRequest("C");
            }
        });
        initTextMsg();
    }

    private void initTextMsg() {
        //下划线
        idTermCondition.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        //抗锯齿
        idTermCondition.getPaint().setAntiAlias(true);
        idTermCondition.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.EC05c_7));
                String lan = SpUtils.getInstance(EcouponsConvertDescripActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, AppTestManager.getInstance().getBaseUrl() + Constants.ECOUP_MERCHANT_SC);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, AppTestManager.getInstance().getBaseUrl() + Constants.ECOUP_MERCHANT_TC);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, AppTestManager.getInstance().getBaseUrl() + Constants.ECOUP_MERCHANT_EN);
                }
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoupon_descrip;
    }

    private void commitConvertRequest(String action) {
        EcouponConvertEntity ecouponConvertEntityIn = new EcouponConvertEntity();
        ecouponConvertEntityIn.setRedmId(redmId);
        ecouponConvertEntityIn.setReqAct(action);
        ecouponConvertEntityIn.setGiftTp(giftTp);
        ecouponConvertEntityIn.setPan(panId);
        ecouponConvertEntityIn.setRedeemPoint(totalGrade);
        List<EcouponConvertEntity.RedeemItem> redeemItems = new ArrayList<>();
        for (int i = 0; i < eVoucherListBeans.size(); i++) {
            RedeemableGiftListEntity.EVoucherListBean eVoucherListBean = eVoucherListBeans.get(i);
            EcouponConvertEntity.RedeemItem redeemItem = new EcouponConvertEntity.RedeemItem();
            redeemItem.setGiftCode(eVoucherListBean.getGiftCode());
            redeemItem.setOrderTp(eVoucherListBean.getOrderTp());
            redeemItem.seteVoucherQty(eVoucherListBean.getCurrentSelCnt() + "");
            redeemItem.setAswItemCode(eVoucherListBean.getAswItemCode());
            redeemItem.setItemName(eVoucherListBean.getEVoucherDesc());
            redeemItems.add(redeemItem);
        }
        ecouponConvertEntityIn.setRedeemItems(redeemItems);
        mPresenter.redeemGift(ecouponConvertEntityIn);
    }

    @Override
    protected RedeemGiftContract.Presenter createPresenter() {
        return new RedeemGiftPresenter();
    }

    @Override
    public void redeemGiftSuccess(RedeemResponseEntity response) {
        if (response.getStockStatus().equals("1")) {
            showEmptyConvertView(response.geteVoucherNos());
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.REDEEMRESPONSEENTITY, response);
            mHashMaps.put(Constants.RETAIN_GRADE, retainGrade);
            mHashMaps.put(Constants.ECOUPONCONVERTENTITY, eVoucherListBeans);
            ActivitySkipUtil.startAnotherActivity(getActivity(), EcoupConvertSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    private void showEmptyConvertView(List<RedeemResponseEntity.ResultErrorRedmBean> resultRedmBeans) {
        String showInfo = "";
        for (int i = 0; i < resultRedmBeans.size(); i++) {
            if (TextUtils.isEmpty(showInfo)) {
                showInfo = resultRedmBeans.get(i).getItemName();
            } else {
                showInfo = showInfo + "\n" + resultRedmBeans.get(i).getItemName();
            }
        }
        //#444444
        EmptyEcouponDialog.Builder builder = new EmptyEcouponDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.EC05_6_1));
        builder.setMessage(showInfo);
        builder.setNegativeButton(getString(R.string.EC07_3), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_RETRY_COUNT, ""));
                finish();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = getActivity();
        if (mActivity != null && !mActivity.isFinishing()) {
            EmptyEcouponDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }

    }

    @Override
    public void redeemGiftError(String errorCode, String errorMsg) {
        //GPS
        if (errorCode.startsWith("GJ")) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.ERROR_MESSAGE, errorMsg);
            ActivitySkipUtil.startAnotherActivity(this, EcouponsConvertStatusActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        } else {
            showErrorMsgDialog(this, errorMsg);
        }
    }

    private CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean> getAdapter(final List<RedeemableGiftListEntity.EVoucherListBean> data) {
        return new CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean>(data) {

            @Override
            public Object getItemType(RedeemableGiftListEntity.EVoucherListBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new EcoupConvertDescAdapter(mContext);
            }
        };
    }


}
