package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 17:54
 * @change
 * @chang time
 * @class describe
 */
public class MenuItemEntity extends BaseEntity {
    private String menuKey;
    private String name;
    private int flag;
    private int flagSelect;
    private Class<?> cls;
    private boolean levelFirst=true;
    private boolean isChildItem=false;


    public ArrayList<MenuItemEntity> childMenus = new ArrayList<>();

    public MenuItemEntity() {
    }

    public MenuItemEntity(String key) {
        this.menuKey = key;
    }

    public MenuItemEntity(String menuKey, String name, int flag,int flagSelect, Class<?> cls, boolean levelFirst) {
        this.menuKey = menuKey;
        this.name = name;
        this.flag = flag;
        this.flagSelect = flagSelect;
        this.cls = cls;
        this.levelFirst=levelFirst;
    }

    public int getFlagSelect() {
        return flagSelect;
    }

    public void setFlagSelect(int flagSelect) {
        this.flagSelect = flagSelect;
    }

    public boolean isLevelFirst() {
        return levelFirst;
    }

    public void setLevelFirst(boolean levelFirst) {
        this.levelFirst = levelFirst;
    }

    public String getMenuKey() {
        return menuKey;
    }

    public String getName() {
        return name;
    }

    public int getFlag() {
        return flag;
    }

    public Class<?> getCls() {
        return cls;
    }

    public boolean isChildItem() {
        return isChildItem;
    }

    public void setChildItem(boolean childItem) {
        isChildItem = childItem;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof MenuItemEntity) {
            MenuItemEntity menu = (MenuItemEntity) obj;
            return (menuKey.equals(menu.menuKey));
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        MenuItemEntity name = (MenuItemEntity) this;
        return name.hashCode();
    }

}
