package cn.swiftpass.wallet.intl.entity;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 *
 */
public class RedPacketStaffEntity extends BaseEntity {
    /**
     * blessingWords : 电话的🤗🙄😮🤬
     * coverId : 1
     * coverUrl : images/en_US/bg_cny_style_default_envelope.png
     * name : CHEN DEREK
     * phone : 852-90296763
     * sendAmt : 1400
     * sendTime : 1606976318573
     * srcRefNo : T051418320000005
     * time : 2020-12-03 14:18:38
     * transferType : 8
     * turnOn : 1
     */

    private String blessingWords;
    private String coverId;
    private String coverUrl;
    private String name;
    private String phone;
    private String sendAmt;
    private long sendTime;
    private String srcRefNo;
    private String time;
    private String transferType;
    private String turnOn;
    private String redPacketImageUrl;
    private String redPacketMusicUrl;

    public String getRedPacketImageUrl() {
        return redPacketImageUrl;
    }

    public void setRedPacketImageUrl(String redPacketImageUrl) {
        this.redPacketImageUrl = redPacketImageUrl;
    }

    public String getRedPacketMusicUrl() {
        return redPacketMusicUrl;
    }

    public void setRedPacketMusicUrl(String redPacketMusicUrl) {
        this.redPacketMusicUrl = redPacketMusicUrl;
    }

    public String getBlessingWords() {
        return blessingWords;
    }

    public void setBlessingWords(String blessingWords) {
        this.blessingWords = blessingWords;
    }

    public String getCoverId() {
        return coverId;
    }

    public void setCoverId(String coverId) {
        this.coverId = coverId;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSendAmt() {
        return sendAmt;
    }

    public void setSendAmt(String sendAmt) {
        this.sendAmt = sendAmt;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    public String getSrcRefNo() {
        return srcRefNo;
    }

    public void setSrcRefNo(String srcRefNo) {
        this.srcRefNo = srcRefNo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public String getTurnOn() {
        return turnOn;
    }

    public void setTurnOn(String turnOn) {
        this.turnOn = turnOn;
    }
}
