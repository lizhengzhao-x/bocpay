package cn.swiftpass.wallet.intl.module.register.model;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.entity.RegisterTaxResultEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoStatusEntity;

public class RegisterDetailsData extends BaseEntity {
    public ArrayList<TaxInfoStatusEntity> data = new ArrayList<>();
    public boolean hasQuestionTwo;
    public boolean hasOtherTaxInfo;
    public boolean showBottomCheck;
    public int resultSelectQuestionOne;
    public int resultSelectQuestionTwo;
    public int currentLimit = -1;

    public RegisterTaxResultEntity registerTaxResult;
}
