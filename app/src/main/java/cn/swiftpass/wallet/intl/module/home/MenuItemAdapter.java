package cn.swiftpass.wallet.intl.module.home;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.recyclerview.ExpandableViewHoldersUtil;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.home
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 18:49
 * @change
 * @chang time
 * @class describe
 */
public class MenuItemAdapter extends RecyclerView.Adapter {
    private final BaseAbstractActivity context;
    private final OnMenuItemClickListener listener;


    private ArrayList<MenuItemEntity> list = new ArrayList<>();


    private final int TYPE_MENU_NORMAL = 1;
    //二级列表
    private final int TYPE_MENU_SECOND = 2;

    private int selectPosition=-1;


    public void setShouldVerifyPwd(boolean shouldVerifyPwd) {
        isShouldVerifyPwd = shouldVerifyPwd;
    }

    private boolean isShouldVerifyPwd = true;

    public MenuItemAdapter(BaseAbstractActivity context, OnMenuItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public void clearSelect(){
        selectPosition=-1;
    }

    public void setList(ArrayList<MenuItemEntity> list) {
        this.list = list;
        clearSelect();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
        if (type == TYPE_MENU_SECOND) {
            View secondView = LayoutInflater.from(context).inflate(R.layout.item_drawer_second, parent, false);
            return new MenuItemSecondHolder(secondView);
        } else {
            View normalView = LayoutInflater.from(context).inflate(R.layout.item_drawer_normal, parent, false);
            return new MenuItemNormalHolder(normalView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MenuItemNormalHolder) {
            MenuItemNormalHolder itemNormalHolder = (MenuItemNormalHolder) viewHolder;
            if (list != null && list.size() > 0 && list.get(position) != null) {
                bindItemData(itemNormalHolder, list.get(position));
            }
        } else if (viewHolder instanceof MenuItemSecondHolder) {
            MenuItemSecondHolder itemSecondHolder = (MenuItemSecondHolder) viewHolder;
            if (list != null && list.size() > 0 && list.get(position) != null) {
                bindSecondItemData(itemSecondHolder, list.get(position),position);
            }
        }
    }


    private void bindSecondItemData(final MenuItemSecondHolder itemSecondHolder, final MenuItemEntity item, int position) {


        itemSecondHolder.llSecondLevel.setVisibility(View.GONE);

        if (itemSecondHolder.llSecondLevel.getVisibility() == View.GONE) {
            setSecondMenuStatus(itemSecondHolder, View.INVISIBLE, R.color.app_white, R.color.font_black_high,
                    item.getFlag(), R.mipmap.icon_arrow_list_down);
        } else {
            setSecondMenuStatus(itemSecondHolder, View.VISIBLE, R.color.color_f9f8fa, R.color.color_b6002a,
                    item.getFlagSelect(), R.mipmap.icon_arrow_list_up);
        }

        try {
            if (!TextUtils.isEmpty(item.getName())) {
                itemSecondHolder.tvSecondTitle.setVisibility(View.VISIBLE);
                itemSecondHolder.tvSecondTitle.setText(item.getName());
            } else {
                itemSecondHolder.tvSecondTitle.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        itemSecondHolder.llSecondLevel.removeAllViews();
        ArrayList<MenuItemEntity> list = item.childMenus;
        if (list != null && list.size() > 0) {
            itemSecondHolder.ivSecondDown.setVisibility(View.VISIBLE);
            int size = list.size();
            for (int i = 0; i < size; i++) {
                MenuItemEntity menuSecondItemEntity = list.get(i);
                if (!TextUtils.isEmpty(menuSecondItemEntity.getName())) {
                    addSecondItemView(itemSecondHolder.llSecondLevel, menuSecondItemEntity);
                }
            }
        } else {
            itemSecondHolder.ivSecondDown.setVisibility(View.GONE);
        }
        itemSecondHolder.llSecondItem.setOnClickListener(new OnProhibitFastClickListener(300) {
            @Override
            public void onFilterClick(View v) {
                    if (itemSecondHolder.llSecondLevel.getVisibility() == View.VISIBLE) {
                        selectPosition=-1;
                        ExpandableViewHoldersUtil.closeHolder(itemSecondHolder, itemSecondHolder.getExpandView(), true);
                        setSecondMenuStatus(itemSecondHolder, View.INVISIBLE, R.color.app_white, R.color.font_black_high,
                                item.getFlag(), R.mipmap.icon_arrow_list_down);
                    } else {
                        //如果有条目选中关闭选中的条目
                        if (selectPosition>-1&&selectPosition<getItemCount()){
                           notifyItemChanged(selectPosition);
                        }
                        ExpandableViewHoldersUtil.openHolder(itemSecondHolder, itemSecondHolder.getExpandView(), true);
                        setSecondMenuStatus(itemSecondHolder, View.VISIBLE, R.color.color_f9f8fa, R.color.color_b6002a,
                                item.getFlagSelect(), R.mipmap.icon_arrow_list_up);
                        selectPosition=position;

                    }

            }
        });
    }

    private void setSecondMenuStatus(MenuItemSecondHolder itemSecondHolder, int RedVisible, int conColor, int titleColor, int ivImage, int arrowImage) {
        itemSecondHolder.redMenuLine.setVisibility(RedVisible);
        itemSecondHolder.llSecondItem.setBackgroundColor(context.getColor(conColor));
        itemSecondHolder.tvSecondTitle.setTextColor(context.getColor(titleColor));
        itemSecondHolder.ivSecond.setImageResource(ivImage);
        itemSecondHolder.ivSecondDown.setImageResource(arrowImage);
    }

    private void addSecondItemView(LinearLayout llSecond, final MenuItemEntity item) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_second_view, null);

        TextView tvSecondName = view.findViewById(R.id.tv_second_name);

        if (!TextUtils.isEmpty(item.getName())) {
            tvSecondName.setText(item.getName());
        }

        view.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                listener.onMenuItemClick(item);
            }
        });
        llSecond.addView(view, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, AndroidUtils.dip2px(context, 50)));
    }

    private void bindItemData(MenuItemNormalHolder itemNormalHolder, final MenuItemEntity item) {

        itemNormalHolder.ivItemImage.setImageResource(item.getFlag());
        try {
            if (!TextUtils.isEmpty(item.getName())) {
                itemNormalHolder.tvItemName.setVisibility(View.VISIBLE);
                itemNormalHolder.tvItemName.setText(item.getName());
            } else {
                itemNormalHolder.tvItemName.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        itemNormalHolder.itemView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                listener.onMenuItemClick(item);
            }
        });
    }



    @Override
    public int getItemViewType(int position) {
        if (list != null && list.size() > 0 && list.get(position) != null) {
            MenuItemEntity menuItemEntity = list.get(position);
            if (menuItemEntity != null && menuItemEntity.childMenus != null && menuItemEntity.childMenus.size() > 0) {
                return TYPE_MENU_SECOND;
            }
        }
        return TYPE_MENU_NORMAL;
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }
}
