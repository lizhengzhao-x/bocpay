package cn.swiftpass.wallet.intl.module.creditcardreward.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.MyCreditCardRewardActionList;

public class MyCreditCardRewardContact {
    public interface View extends IView {
        void getCreditCardRewardListError(String errorCode, String errorMsg);

        void getCreditCardRewardListSuccess(MyCreditCardRewardActionList response);
    }

    public interface Presenter extends IPresenter<View> {
        void loadCreditCardReward(String dateType, String currentPage);
    }
}
