package cn.swiftpass.wallet.intl.module.home;

import cn.swiftpass.wallet.intl.entity.HomeLifeMenuEntity;

public interface OnLifeMenuItemClickListener {

    boolean onHomeLifeMenuItemClick(HomeLifeMenuEntity item);


}