package cn.swiftpass.wallet.intl.module.cardmanagement.fps;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordDisplayBean;

/**
 * 绑定FPS的银行列表
 */

public class FpsBindBankListAdapter implements AdapterItem<FpsOtherBankRecordDisplayBean> {

    private View mRootView;
    private ImageView mSelectIV;
    private TextView mBankNameTV;
    private TextView mBankAccountTV;
    private TextView mDefaultTV;

    private Context mContext;

    private BankItemCallback mCallback;

    public FpsBindBankListAdapter(Context context, BankItemCallback callback) {
        mCallback = callback;
        mContext = context;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fps_bind_bank_item;
    }

    @Override
    public void bindViews(View root) {
        mRootView = root;
        mSelectIV = root.findViewById(R.id.iv_fps_bank_select);
        mBankNameTV = root.findViewById(R.id.tv_fps_bank_name);
        mBankAccountTV = root.findViewById(R.id.tv_fps_bank_account);
        mDefaultTV = root.findViewById(R.id.tv_fps_bank_default);
    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(final FpsOtherBankRecordDisplayBean displayBean, final int position) {
        //this.position = position;
        FpsOtherBankRecordBean bean = displayBean.getRecordBean();
        if (TextUtils.isEmpty(bean.getBankName())) {
            mBankNameTV.setText(bean.getBankCode());
        } else {
            mBankNameTV.setText(bean.getBankName());
        }
        if (displayBean.isHideCheck()) {
            mSelectIV.setVisibility(View.INVISIBLE);
        } else {
            mSelectIV.setVisibility(View.VISIBLE);
            mSelectIV.setSelected(displayBean.isStatusCheck());
        }
        mBankAccountTV.setText(bean.getDisplayEngName());
        if (TextUtils.equals(bean.getDefaultBank(), FpsConst.FPS_DEFAULT_YES)) {
            mDefaultTV.setVisibility(View.VISIBLE);
        } else {
            mDefaultTV.setVisibility(View.INVISIBLE);
        }

        mSelectIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!displayBean.isBanClick()) {
                    boolean isSelect = !displayBean.isStatusCheck();
                    mSelectIV.setSelected(isSelect);
                    displayBean.setStatusCheck(isSelect);
                    if (mCallback != null) {
                        mCallback.onSelectClick(position);
                    }
                }
            }
        });
    }

    public interface BankItemCallback {
        void onSelectClick(int position);
    }
}
