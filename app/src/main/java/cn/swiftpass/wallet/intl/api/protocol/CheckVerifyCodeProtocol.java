package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class CheckVerifyCodeProtocol extends BaseProtocol {
    String mVerifyCode;

    public CheckVerifyCodeProtocol(String verifyCodeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.mVerifyCode = verifyCodeIn;
        mUrl = "api/login/checkVerifyCode";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
        mBodyParams.put(RequestParams.ACTION, "L");
    }

}
