package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderAccountListEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 21:55
 * @change
 * @chang time
 * @class describe
 */
public class TransferAccountListAdapter extends RecyclerView.Adapter {
    private final Context context;
    private final ArrayList<TransferCrossBorderAccountListEntity> list;
    private final OnItemClickListener listener;
    private TransferCrossBorderAccountListEntity select;


    public TransferAccountListAdapter(Context context, ArrayList<TransferCrossBorderAccountListEntity> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public void setSelect(TransferCrossBorderAccountListEntity select) {
        this.select = select;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int posotion) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_transfer_account, parent, false);
        return new PurposeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        PurposeViewHolder purposeViewHolder = (PurposeViewHolder) viewHolder;
        String sb = ("1".equals(list.get(i).cardType)
                ? context.getString(R.string.LYP01_06_6) : context.getString(R.string.LYP01_06_4))
                + "(" + list.get(i).panFour + ")";
        purposeViewHolder.tvAccount.setText(sb);
        if (select != null && select.panFour.equals(list.get(i).panFour)) {
            purposeViewHolder.ivSelect.setVisibility(View.VISIBLE);
        } else {
            purposeViewHolder.ivSelect.setVisibility(View.INVISIBLE);
        }
        purposeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(i, list.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, TransferCrossBorderAccountListEntity select);
    }

    public class PurposeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_account)
        TextView tvAccount;
        @BindView(R.id.iv_select)
        ImageView ivSelect;

        public PurposeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
