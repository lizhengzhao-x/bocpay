package cn.swiftpass.wallet.intl.module.register.adapter;

import cn.swiftpass.wallet.intl.entity.TaxInfoReason;
import cn.swiftpass.wallet.intl.entity.TaxInfoStatusEntity;
import cn.swiftpass.wallet.intl.entity.TaxResidentEntity;

public interface OnTaxDataChangeListener {
    void OnChangeNextBtn(boolean status);

    void selectCountryData(int index, TaxResidentEntity residenceEntity);

    void selectTaxReasonData(int index, TaxInfoReason reasonEntity);

    void deleteTaxInfo(int index, TaxInfoStatusEntity taxInfoStatusEntity);

    void resetDataResidentList();
}
