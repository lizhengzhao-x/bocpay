package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.app.Activity;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 积分查询列表弹框 列表查询
 */
public class CheckEcouponsDetailPop extends BasePopWindow {

    private TextView mId_grade_title;
    private TextView id_grade_desc;
    private ImageView mId_close_dialog;
    private String title;
    private String name;

    private void bindViews() {
        mId_grade_title = (TextView) mContentView.findViewById(R.id.id_grade_title);
        mId_close_dialog = (ImageView) mContentView.findViewById(R.id.id_close_dialog);
        id_grade_desc = (TextView) mContentView.findViewById(R.id.id_grade_desc);
    }


    public CheckEcouponsDetailPop(Activity mActivity, String titleIn, String nameIn) {
        super(mActivity);
        this.title = titleIn;
        this.name = nameIn;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_ecoupon_detail);
        bindViews();
        mId_grade_title.setText(title);
        id_grade_desc.setText(name);
        id_grade_desc.setMovementMethod(ScrollingMovementMethod.getInstance());
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.9));
//        setHeight((int) (AndroidUtils.getScreenHeight(mActivity) * 0.6));
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, 0, 0);
    }
}
