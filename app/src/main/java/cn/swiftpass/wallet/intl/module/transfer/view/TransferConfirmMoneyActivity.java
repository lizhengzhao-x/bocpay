package cn.swiftpass.wallet.intl.module.transfer.view;

import static cn.swiftpass.wallet.intl.module.transfer.presenter.TransferConfirmPresenter.ACTION_PAY;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferFirstSendOtpActivity.EXTRA_TRANSFERCHECK;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.UserNameSetDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferConfirmContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferConfirmPresenter;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 转账确认页面
 */
public class TransferConfirmMoneyActivity extends BaseCompatActivity<TransferConfirmContract.Presenter> implements TransferConfirmContract.View {

    @BindView(R.id.tv_amount)
    TextView tvAmount;
    protected @BindView(R.id.transfer_next)
    TextView transferNext;
    @BindView(R.id.tv_pay_fee_type)
    TextView tvPayFeeType;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_confirm_edit)
    TextView tvConfirmEdit;
    @BindView(R.id.tv_nick_name)
    TextView tvNickName;
    @BindView(R.id.tv_user_account)
    TextView tvUserAccount;
    @BindView(R.id.title_account_info)
    TextView titleAccountInfo;


    /**
     * 转账附言标题
     */
    @BindView(R.id.lly_postscript)
    protected LinearLayout llyPostscript;
    /**
     * 转账附言具体内容
     */
    @BindView(R.id.detail_postscript)
    protected TextView tvDetailPostscript;
    @BindView(R.id.tv_confirm_bank_name)
    TextView tvConfirmBankName;
    @BindView(R.id.lly_confirm_bank_name)
    LinearLayout llyConfirmBankName;
    @BindView(R.id.tv_confirm_bank_code)
    TextView tvConfirmBankCode;
    @BindView(R.id.lly_confirm_bank_code)
    LinearLayout llyConfirmBankCode;
    @BindView(R.id.lly_confirm_nick_name)
    RelativeLayout llyConfirmNickName;
    @BindView(R.id.id_ll_reference)
    LinearLayout llReference;
    @BindView(R.id.title_referenceNo)
    TextView tvReferenceNo;


    /**
     * tvTransferInfomation 转账信息
     * tvFromAccount 付款账户
     * tvPayeePerson 收款人
     * tvReferenceTitle 参考编号
     * tvRemark 备注
     */
    TextView tvTransferInfomation;
    TextView tvFromAccount;
    TextView tvPayeePerson;
    TextView tvReferenceTitle;
    TextView tvRemark;


    private UserNameSetDialog mChangeUserNameDialog;

    /**
     * 确定金额前需预检查
     */
    protected TransferPreCheckEntity mPreCheckEntity;
    private TransferConfirmReq mTransferComplete;
    private String transferType;
    private String mCurrentEditName;
    private int currentPaymentType;

    String mUserNameStr;
    TransferConfirmReq req;
    TransferConfirmReq reqBocPay;
    public static final int PAYMENT_APPCALLAPP_PAYMENT = 200;
    public static final int TRANSFER_NORMAL = 200;
    public static final int TRANSFER_APPCALLAPP_PAYMENT = 100;

    @Override
    public void init() {
        if (null != getIntent()) {
            mPreCheckEntity = (TransferPreCheckEntity) getIntent().getSerializableExtra(TransferConst.TRANS_FPS_CHECK_DATA);
        } else {
            finish();
        }
        currentPaymentType = getIntent().getExtras().getInt(Constants.TYPE);
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            hideBackIcon();
            ImageView closeImg = setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
            closeImg.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    onBackPressed();
                }
            });

        }
        transferType = mPreCheckEntity.getTransferType();
        EventBus.getDefault().register(this);
        initView();

        //转账确认界面 如果是商户 显示付款 如果是个人，根据不同转账方式显示
        if (TextUtils.isEmpty(mPreCheckEntity.getPaymentCategory()) || TextUtils.equals(mPreCheckEntity.getPaymentCategory(), TransferConst.TRANS_TYPE_PERSON)) {
            //transferNext.setText(R.string.TF2101_5_7);
            if (TextUtils.equals(transferType, TransferConst.TRANS_FPS)) {
                setToolBarTitle(R.string.TF2101_26_1);
            } else if (TextUtils.equals(transferType, TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
                setToolBarTitle(R.string.TF2101_30_1);
            } else if (TextUtils.equals(transferType, TransferConst.TRANS_TYPE_PHONE)) {
                setToolBarTitle(R.string.TF2101_5_1);
            } else {
                setToolBarTitle(R.string.TF2101_5_1);
            }

            if (tvTransferInfomation != null && tvFromAccount != null && tvRemark != null && tvPayeePerson != null && tvReferenceTitle != null) {
                //利是界面没有这些控件，若为空则不执行
//                tvTransferInfomation.setText(R.string.CR2101_8_4);
                //tvFromAccount.setText(R.string.TF2101_21_4);
                tvPayeePerson.setText(R.string.TF2101_34_12);
                tvReferenceTitle.setText(R.string.FPSBP2101_4_5);
                tvRemark.setText(R.string.TF2101_33_7);
            }
            transferNext.setText(R.string.TF2101_5_7);

        } else { //商户
            setToolBarTitle(R.string.FPSBP2101_1_1);

            if (tvTransferInfomation != null && tvFromAccount != null && tvRemark != null && tvPayeePerson != null && tvReferenceTitle != null) {
                //利是界面没有这些控件，若为空则不执行
                //tvTransferInfomation.setText(R.string.FPSBP2101_4_3);
                // tvFromAccount.setText(R.string.TF2101_26_4);
                tvPayeePerson.setText(R.string.TF2101_7_3);
                tvReferenceTitle.setText(R.string.FPSBP2101_4_5);
                tvRemark.setText(R.string.TF2101_33_7);
            }
            transferNext.setText(R.string.TF2101_5_7);


        }

        if (null != tvTransferInfomation) {
            //如果是商家
            if (TextUtils.equals(mPreCheckEntity.getPaymentCategory(), "1")) {
                tvTransferInfomation.setText(R.string.FPSBP2101_4_3);
            }
            //如果是FPS id转账
//            if (TextUtils.equals(transferType, TransferConst.TRANS_FPS)) {
//                tvTransferInfomation.setText(R.string.FPSBP2101_4_3);
//            }
        }

        String str;
        //2为支付账户 3为智能账户
        if (TextUtils.equals(Constants.ACCOUNT_PAY_ACCOUNT, mPreCheckEntity.getSmartAcLevel())) {
            str = getString(R.string.DB2101_1_2);
        } else {
            str = getString(R.string.DB2101_1_1);
        }
        titleAccountInfo.setText(str + "(" + mPreCheckEntity.getAcNo() + ")");

    }

    private void initView() {

        tvTransferInfomation = findViewById(R.id.tv_transfer_infomation);
        tvFromAccount = findViewById(R.id.tv_from_account);
        tvRemark = findViewById(R.id.tv_remark);
        tvPayeePerson = findViewById(R.id.tv_payee_person);
        tvReferenceTitle = findViewById(R.id.tv_reference_title);

        String merName = mPreCheckEntity.getCrDisplayedEngNm();
        //转账昵称是否修改过
        String realRevUserName = mPreCheckEntity.getCrModifyName();
        if (!TextUtils.isEmpty(merName)) {
            tvUsername.setText(merName);
        }

        if (!TextUtils.isEmpty(realRevUserName)) {
            //最近转账回把用户的昵称带过来 如果和后台反馈的fpp查询昵称不一样 就显示 否则
            updateUserNameSpinnerStr(merName, realRevUserName);
        }
        String amount = mPreCheckEntity.getDrAmt();
        if (!TextUtils.isEmpty(amount)) {
            String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(amount), true);
            tvAmount.setText(balanceStr);
        }
        tvPayFeeType.setText(mPreCheckEntity.getSrvcChrgDrCur());

        //控制附言显示
        if (!TextUtils.isEmpty(mPreCheckEntity.getPostscript())) {
            llyPostscript.setVisibility(View.VISIBLE);
            tvDetailPostscript.setText(mPreCheckEntity.getPostscript());
        } else {
            tvDetailPostscript.setVisibility(View.GONE);
            llyPostscript.setVisibility(View.GONE);
        }

        String acc = mPreCheckEntity.getPreCheckReq().tansferToAccount;

        tvUserAccount.setText(acc);

        //FPS缴费 有TransferBank  银行账户转账 ui 单独显示 银行名称 账号
        if (!TextUtils.isEmpty(mPreCheckEntity.getTransferBank())) {
            llyConfirmBankName.setVisibility(View.VISIBLE);
            tvConfirmBankName.setText(mPreCheckEntity.getTransferBank());
        } else if (TextUtils.equals(transferType, TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
            llyConfirmBankName.setVisibility(View.VISIBLE);
            tvConfirmBankName.setText(mPreCheckEntity.getBankShowName());
        } else {
            llyConfirmBankName.setVisibility(View.GONE);
        }

        //控制修改昵称显示 只有邮箱转账 bocpay转账才可以修改昵称
        if (!TextUtils.isEmpty(mPreCheckEntity.getPaymentCategory()) && TextUtils.equals(mPreCheckEntity.getPaymentCategory(), "1")) {

            tvConfirmEdit.setVisibility(View.GONE);

        } else {
            //扫码转账带过来 0/1代表手机号 邮箱
            if (TextUtils.equals(transferType, TransferConst.TRANS_TYPE_PHONE) || TextUtils.equals(transferType, TransferConst.TRANS_TYPE_EMAIL)) {
                tvConfirmEdit.setVisibility(View.VISIBLE);
            } else {
                tvConfirmEdit.setVisibility(View.GONE);
            }
        }

        String billReference = mPreCheckEntity.getBillReference();
        if (TextUtils.isEmpty(billReference)) {
            llReference.setVisibility(View.GONE);
        } else {
            llReference.setVisibility(View.VISIBLE);
            tvReferenceNo.setText(billReference);
        }

        String str = tvNickName.getText().toString();
        //昵称为空 按钮文字为设定，不为空则为修改
        if (TextUtils.isEmpty(str)) {
            tvConfirmEdit.setText(getString(R.string.RP2101_13_5));
        } else {
            tvConfirmEdit.setText(getString(R.string.RP2101_13_6));
        }
        hideLlyNickName();
    }

    private void hideLlyNickName() {
        if (null == tvNickName) {
            return;
        }

        /**
         * 可以修改昵称不需要隐藏 昵称这一行
         */
        if (tvConfirmEdit.getVisibility() == View.VISIBLE) {
            return;
        }

        String str = tvNickName.getText().toString();

        /**
         * 昵称不为空 不需要隐藏 昵称这一行
         */
        if (!TextUtils.isEmpty(str)) {
            return;
        }


        llyConfirmNickName.setVisibility(View.GONE);


    }

    /**
     * 修改昵称的输入dialog
     */
    private void onClickAmountLimit() {
        String nickName = tvNickName.getText().toString();
        UserNameSetDialog.Builder builder = new UserNameSetDialog.Builder(this, nickName);
        mChangeUserNameDialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                String userName = mChangeUserNameDialog.getmAmount().trim();
                changeUserNameEvent(userName);
            }
        }).create();
        mChangeUserNameDialog.show();
        WindowManager.LayoutParams params = mChangeUserNameDialog.getWindow().getAttributes();
        params.width = (int) (this.getWindowManager().getDefaultDisplay().getWidth() * 0.8);
        mChangeUserNameDialog.getWindow().setAttributes(params);
    }

    /**
     * 修改用户昵称
     *
     * @param userName
     */
    private void changeUserNameEvent(String userName) {
        mUserNameStr = mPreCheckEntity.getCrDisplayedEngNm();
        String type = mPreCheckEntity.isBocPayTransfer() ? Constants.BOCPAY : Constants.FPS;
        mPresenter.transferChangeUserName(mPreCheckEntity.getScrRefNo(), userName, type);
    }

    private void updateUserNameSpinnerStr(String userNameStr, String mCurrentEditName) {
        tvUsername.setText(userNameStr);
        tvNickName.setText(mCurrentEditName);
        String str = tvNickName.getText().toString();
        //昵称为空 按钮文字为设定，不为空则为修改
        if (TextUtils.isEmpty(str)) {
            tvConfirmEdit.setText(getString(R.string.RP2101_13_5));
        } else {
            tvConfirmEdit.setText(getString(R.string.RP2101_13_6));
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_amount_confirm;
    }

    /**
     * EVENT_TRANSFER_SUCCESS 转账成功 关闭当前页面
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS
                || event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.transfer_next, R.id.tv_confirm_edit})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) return;
        switch (view.getId()) {
            case R.id.transfer_next:
                verifyPwdAction();
                break;
            case R.id.tv_confirm_edit:
                //转账 修改 添加备注
                onClickAmountLimit();
                break;
        }
    }

    private void verifyPwdAction() {
        if (ButtonUtils.isFastDoubleClick()) return;
        holdBackListener(false);
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (mPreCheckEntity.needSendOtp()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(EXTRA_TRANSFERCHECK, mPreCheckEntity);
                    if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
                        mHashMaps.put(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
                    }
                    ActivitySkipUtil.startAnotherActivity(TransferConfirmMoneyActivity.this, TransferFirstSendOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    resetClickStatus();
                } else {
                    verifyResultSuc();
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                resetClickStatus();
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                resetClickStatus();
            }
        });
    }

    private void verifyResultSuc() {
        if (mPreCheckEntity.isBocPayTransfer()) {
            confirmTransferBocPay();
        } else {
            confirmTransferFPS();
        }
    }

    /**
     * 防止页面切换 back点击事件触发
     */
    private void resetClickStatus() {
        if (mHandler == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                holdBackListener(true);
            }
        }, 500);
    }

    /**
     * 防止页面切换 back按键是否可点
     */
    private void holdBackListener(boolean isClick) {
        LogUtils.i(TAG, "holdBackListener" + isClick);
        if (isClick) {
            mToolBarBase.setNavigationOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    //finish();
                    onBackPressed();
                }
            });
        } else {
            mToolBarBase.setNavigationOnClickListener(null);
        }
    }


    /**
     * fps转账确认
     */
    private void confirmTransferFPS() {
        req = new TransferConfirmReq();
        req.tansferToAccount = TextUtils.isEmpty(mPreCheckEntity.getAccountNo()) ? mPreCheckEntity.getPreCheckReq().tansferToAccount : mPreCheckEntity.getAccountNo();
        req.transferAmount = mPreCheckEntity.getDrAmt();
        req.bankType = mPreCheckEntity.getPreCheckReq().bankType;
        req.scrRefNo = mPreCheckEntity.getScrRefNo();
        req.transferType = mPreCheckEntity.getPreCheckReq().transferType;
        req.isLiShi = mPreCheckEntity.getPreCheckReq().isLiShi;
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            mPresenter.confirmTransfer(req, AppCallAppUtils.FPS_FLOW_ACTION);
        } else {
            mPresenter.confirmTransfer(req, null);
        }
    }

    /**
     * bocPay转账确认
     */
    private void confirmTransferBocPay() {
        reqBocPay = new TransferConfirmReq();
        reqBocPay.tansferToAccount = mPreCheckEntity.getAccountNo();
        reqBocPay.transferAmount = mPreCheckEntity.getDrAmt();
        reqBocPay.bankType = mPreCheckEntity.getPreCheckReq().bankType;
        reqBocPay.scrRefNo = mPreCheckEntity.getScrRefNo();
        reqBocPay.transferType = mPreCheckEntity.getPreCheckReq().transferType;
        reqBocPay.isLiShi = mPreCheckEntity.getPreCheckReq().isLiShi;
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            mPresenter.transferConfirmBoc(mPreCheckEntity.getScrRefNo(), reqBocPay.isLiShi, ACTION_PAY);
        } else {
            mPresenter.transferConfirmBoc(mPreCheckEntity.getScrRefNo(), reqBocPay.isLiShi, null);
        }

    }

    @Override
    protected TransferConfirmContract.Presenter createPresenter() {
        return new TransferConfirmPresenter();
    }

    @Override
    public void transferChangeUserNameSuccess(ContentEntity response) {

        if (mChangeUserNameDialog != null) {
            mChangeUserNameDialog.dismiss();
        }
        mCurrentEditName = mChangeUserNameDialog.getmAmount().trim();

        //昵称为空不需要刷新
        if (!TextUtils.isEmpty(mCurrentEditName)) {
            //转账 最近转账列表需要刷新 更新昵称
            EventBus.getDefault().post(new TransferEventEntity(TransferEventEntity.UPDATE_TRANSFER_LIST, mCurrentEditName));

        }

        updateUserNameSpinnerStr(mUserNameStr, mCurrentEditName);
    }

    @Override
    public void transferChangeUserNameError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    @Override
    public void confirmTransferSuccess(TransferCompleteEntity response) {
        mTransferComplete = req;
        mTransferComplete.paymentCategory = mPreCheckEntity.getPaymentCategory();
        onTransferSuccess(response);
    }

    @Override
    public void confirmTransferError(String errorCode, String errorMsg) {
        resetClickStatus();
        if (errorCode.equals(ErrorCode.TASNSFER_ORDER_ERROR.code)) {
            TransferFailedActivity.startActivity(TransferConfirmMoneyActivity.this);
        } else {
            showErrorMsgDialog(TransferConfirmMoneyActivity.this, errorMsg);
        }
    }

    @Override
    public void transferConfirmBocSuccess(TransferCompleteEntity response) {
        mTransferComplete = reqBocPay;
        onTransferSuccess(response);
    }

    @Override
    public void transferConfirmBocError(String errorCode, String errorMsg) {
        resetClickStatus();
        if (errorCode.equals(ErrorCode.TASNSFER_ORDER_ERROR.code)) {
            TransferFailedActivity.startActivity(TransferConfirmMoneyActivity.this);
        } else {
            showErrorMsgDialog(TransferConfirmMoneyActivity.this, errorMsg);
        }
    }

    public static void startActivity(Activity activity, TransferPreCheckEntity entity) {
        Intent intent = new Intent(activity, TransferConfirmMoneyActivity.class);
        intent.putExtra(TransferConst.TRANS_FPS_CHECK_DATA, entity);
        activity.startActivity(intent);
    }

    /**
     * app call app 单独处理
     *
     * @param activity
     * @param entity
     */
    public static void startActivityForResult(Activity activity, TransferPreCheckEntity entity) {
        Intent intent = new Intent(activity, TransferConfirmMoneyActivity.class);
        intent.putExtra(TransferConst.TRANS_FPS_CHECK_DATA, entity);
        intent.putExtra(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
        activity.startActivity(intent);
//        activity.finish();
    }

    private void onTransferSuccess(TransferCompleteEntity response) {
        mTransferComplete.currency = mPreCheckEntity.getSrvcChrgDrCur();
        mTransferComplete.tansferAccount = mPreCheckEntity.getAcNo();
        mTransferComplete.tansferToAccount = response.getCrDisplayedEngNm();
        mTransferComplete.collectionAccount = response.getCollectionAccount();
        mTransferComplete.billReference = response.getBillReference();
        mTransferComplete.mSmartLevel = response.getSmartAcLevel();
        mTransferComplete.transferDate = response.getTransferDate();
        mTransferComplete.transferBank = response.getTransferBank();
        mTransferComplete.dbtrNm = response.getDbtrNm();
        mTransferComplete.txnId = response.getTxnId();
        mTransferComplete.extCmt = response.getExtCmt();
        mTransferComplete.preBank = response.getPreBank();
        mTransferComplete.bckImgsBean = mPreCheckEntity.getBckImgsBean();
        mTransferComplete.transferType = mPreCheckEntity.getTransferType();
        mTransferComplete.paymentCategory = mPreCheckEntity.getPaymentCategory();
        mTransferComplete.isBocPayTransfer = mPreCheckEntity.isBocPayTransfer();
        mTransferComplete.smartAcLevel = mPreCheckEntity.getSmartAcLevel();
        mTransferComplete.bocpayRedPackeNotifyTip = response.getBocpayRedPackeNotifyTip();
        if (response.getPostscript() != null) {
            mTransferComplete.postscript = response.getPostscript();
        }
        Intent intent = new Intent(TransferConfirmMoneyActivity.this, TransferSuccessActivity.class);
        intent.putExtra(TransferConst.TRANS_FPS_CHECK_DATA, mTransferComplete);
        intent.putExtra(TransferConst.TRANS_COLLECTION_TYPE, response.getPaymentCategory());
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            intent.putExtra(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
//            if (getIntent() != null || getIntent().getExtras() != null) {
//                intent.putExtra(Constants.FPS_CALLBACK_URL, getIntent().getExtras().getString(Constants.FPS_CALLBACK_URL));
//            }
        }
        startActivity(intent);
        resetClickStatus();
    }


    @Override
    public void onBackPressed() {
        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
            AndroidUtils.showConfrimExitDialog(TransferConfirmMoneyActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                @Override
                public void onConfirmBtnClick() {
                    AppCallAppUtils.goToMerchantAppForFps(TransferConfirmMoneyActivity.this, false);
                    finish();
                }
            });
        } else {
            super.onBackPressed();
        }
    }

}
