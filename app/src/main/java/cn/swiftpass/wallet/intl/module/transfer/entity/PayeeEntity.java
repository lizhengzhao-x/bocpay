package cn.swiftpass.wallet.intl.module.transfer.entity;

import com.google.gson.annotations.SerializedName;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:30
 * @change
 * @chang time
 * @class 收款人资料
 */
public class PayeeEntity extends BaseEntity {

    @SerializedName("receiverName")
    public String payeeName;

    @SerializedName("receiverMarkedCardNo")
    public String receiverMarkedCardNo;

    @SerializedName("receiverMarkedName")
    public String payeeHideName;

    @SerializedName("receiverCardNo")
    public String payeeCardId;

    @SerializedName("receiverCardType")
    public String payeeCardType;

    @SerializedName("receiverBankName")
    public String payeeBank;


    public PayeeEntity() {
    }

    public PayeeEntity(String payeeName, String payeeCardId, String payeeBank) {
        this.payeeName = payeeName;
        this.payeeCardId = payeeCardId;
        this.payeeBank = payeeBank;
    }


    public PayeeEntity(String payeeName, String payeeHideName, String payeeCardId, String payeeBank) {
        this.payeeName = payeeName;
        this.payeeHideName = payeeHideName;
        this.payeeCardId = payeeCardId;
        this.payeeBank = payeeBank;
    }

    public PayeeEntity(String payeeName, String payeeHideName, String payeeCardId, String payeeCardType, String payeeBank) {
        this.payeeName = payeeName;
        this.payeeHideName = payeeHideName;
        this.payeeCardId = payeeCardId;
        this.payeeCardType = payeeCardType;
        this.payeeBank = payeeBank;
    }
}
