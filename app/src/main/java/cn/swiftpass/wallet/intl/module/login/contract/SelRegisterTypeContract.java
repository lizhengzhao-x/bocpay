package cn.swiftpass.wallet.intl.module.login.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;

public class SelRegisterTypeContract {

    public interface View extends IView {
        void getSmartLevelForBindFailed(String errorCode, String errorMsg);

        void getSmartLevelForBindSuccess(SmartLevelEntity response);

        void checkCreditCardNumFailed(String errorCode, String errorMsg);

        void checkCreditCardNumSuccess(RegSucBySmartEntity response);
    }

    public interface Presenter extends IPresenter<View> {


        void getSmartLevelForBind();

        void checkCreditCardNum();
    }
}
