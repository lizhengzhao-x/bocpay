package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 首页活动列表数据URL
 */

public class ActivityEntity extends BaseEntity {
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    String value;

    public String getSystime() {
        return systime;
    }

    public void setSystime(String systime) {
        this.systime = systime;
    }

    private String systime;

    /**
     * en_US : {"data":{"title":"Cross-border Bill Payment Platform","subTitle":"Flexible payment of various types of bills online in the Greater Bay Area","type":"BOC PAY","introduction":"You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>\"Bill Payment Steps\"<\/Flow> and \"Accepting Merchants\" for further details (Available in Chinese only)","tips":"To borrow or not to borrow? Borrow only if you can repay!","flowUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (\u201cBOCHK\u201d) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}}
     */

    private EnUSBean en_US;

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public EnUSBean getEn_US() {
        return en_US;
    }

    public void setEn_US(EnUSBean en_US) {
        this.en_US = en_US;
    }

    public static class EnUSBean {
        /**
         * data : {"title":"Cross-border Bill Payment Platform","subTitle":"Flexible payment of various types of bills online in the Greater Bay Area","type":"BOC PAY","introduction":"You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>\"Bill Payment Steps\"<\/Flow> and \"Accepting Merchants\" for further details (Available in Chinese only)","tips":"To borrow or not to borrow? Borrow only if you can repay!","flowUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html","cooperationUrl":"https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html","tnc":"Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (\u201cBOCHK\u201d) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.","crossPaymentUrl":"https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT"}
         */

        private DataBean data;

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {
            /**
             * title : Cross-border Bill Payment Platform
             * subTitle : Flexible payment of various types of bills online in the Greater Bay Area
             * type : BOC PAY
             * introduction : You can simply access Cross-border Bill Payment Platform via BoC Pay mobile application to make payment of various types of public service bills such as water, electricity, fuel and television fees in the Greater Bay Area anytime anywhere with your BOC Dual Currency Card! Please click <Flow>"Bill Payment Steps"</Flow> and "Accepting Merchants" for further details (Available in Chinese only)
             * tips : To borrow or not to borrow? Borrow only if you can repay!
             * flowUrl : https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_paymentsteps.html
             * cooperationUrl : https://www.bochk.com/creditcard/eng/creditcard/boci_cc_ex_cbbp_acceptingmerchants.html
             * tnc : Cross-border Bill Payment Platform is provided by Guangzhou UnionPay Network Payment Company Limited. Bank of China (Hong Kong) Limited (“BOCHK”) and BOC Credit Card (International) Limited give no guarantee to the products and services of Guangzhou UnionPay Network Payment Company Limited or does not accept any liability arising in conjunction with the use of the products and services provided by Guangzhou UnionPay Network Payment Company Limited. The usage of the products and services is subject to the terms as specified by Guangzhou UnionPay Network Payment Company Limited.
             * crossPaymentUrl : https://m.gnete.com/GneteAutoFeeGrandBay/service.action?ModelCode=GrandBay&TranCode=GRAND_BAY_JUMP&Language=CHT
             */

            private String title;
            private String subTitle;
            private String type;
            private String introduction;
            private String tips;
            private String flowUrl;
            private String cooperationUrl;
            private String tnc;
            private String crossPaymentUrl;


            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubTitle() {
                return subTitle;
            }

            public void setSubTitle(String subTitle) {
                this.subTitle = subTitle;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getIntroduction() {
                return introduction;
            }

            public void setIntroduction(String introduction) {
                this.introduction = introduction;
            }

            public String getTips() {
                return tips;
            }

            public void setTips(String tips) {
                this.tips = tips;
            }

            public String getFlowUrl() {
                return flowUrl;
            }

            public void setFlowUrl(String flowUrl) {
                this.flowUrl = flowUrl;
            }

            public String getCooperationUrl() {
                return cooperationUrl;
            }

            public void setCooperationUrl(String cooperationUrl) {
                this.cooperationUrl = cooperationUrl;
            }

            public String getTnc() {
                return tnc;
            }

            public void setTnc(String tnc) {
                this.tnc = tnc;
            }

            public String getCrossPaymentUrl() {
                return crossPaymentUrl;
            }

            public void setCrossPaymentUrl(String crossPaymentUrl) {
                this.crossPaymentUrl = crossPaymentUrl;
            }
        }
    }
}
