package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 15:03
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderConst {
    public static final String DATA_TRANSFER_BASE = "DATA_TRANSFER_BASE";
    public static final String DATA_TRANSFER_CACHE = "DATA_TRANSFER_CACHE";
    public static final String DATA_TRANSFER_ORDER_ID = "DATA_TRANSFER_ORDER_ID";
    public static final String DATA_TRANSFER_PURPOSE = "DATA_TRANSFER_PURPOSE";
    public static final String DATA_TRANSFER_PURPOSE_CODE = "DATA_TRANSFER_PURPOSE_CODE";
    public static final String DATA_TRANSFER_SET_LIMIT_DATA = "DATA_TRANSFER_SET_LIMIT_DATA";
    public static final String DATA_TRANSFER_SELECT_ACCOUNT = "DATA_TRANSFER_SELECT_ACCOUNT";
    public static final String DATA_TRANSFER_DETAIL = "DATA_TRANSFER_DETAIL";
    public static final String DATA_PAYEE_NAME = "DATA_PAYEE_NAME";
    public static final String DATA_PAYEE_CARD_ID = "DATA_PAYEE_CARD_ID";
    public static final String DATA_PAYEE_CARD_TYPE = "DATA_PAYEE_CARD_TYPE";
    public static final String DATA_PAYEE_BANK = "DATA_PAYEE_BANK";
    public static final String ISTRANSFERTOSELF = "isTransferToSelf";
    public static final String DATA_FOR_USED_CODE = "DATA_FOR_USED_CODE";
    public static final String DATA_FOR_USED_VALUE = "DATA_FOR_USED_VALUE";
    public static final String DATA_TRANSFER_AMOUNT = "DATA_TRANSFER_AMOUNT";
    public static final String DATA_TRANSFER_ACCOUNT = "DATA_TRANSFER_ACCOUNT";
    public static final String DATA_TRANSFER_ITEM = "DATA_TRANSFER_ITEM";
    public static final String DATA_TRANSFER_LIMIT_AMOUNT = "DATA_TRANSFER_LIMIT_AMOUNT";
}
