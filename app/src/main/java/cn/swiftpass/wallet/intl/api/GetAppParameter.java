package cn.swiftpass.wallet.intl.api;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 开关配置
 */
public class GetAppParameter extends BaseProtocol {

    public GetAppParameter(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/parameter/getAppParameter";
    }
}

