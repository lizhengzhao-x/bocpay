package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/12/2.
 */
public class SmilingFaceProtocol extends BaseProtocol {
    public static final String TAG = SmilingFaceProtocol.class.getSimpleName();
    String action;
    String pageId;
    String desc;

    public SmilingFaceProtocol(String action, String pageId, String desc, NetWorkCallbackListener dataCallback) {
        this.action = action;
        this.pageId = pageId;
        this.desc = desc;
        this.mDataCallback = dataCallback;
        mUrl = "api/smilingFace/add";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
        mBodyParams.put(RequestParams.PAGEID, pageId);
        mBodyParams.put(RequestParams.DESC, desc);
    }
}
