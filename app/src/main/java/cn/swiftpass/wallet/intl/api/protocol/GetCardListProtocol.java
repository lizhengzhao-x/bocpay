package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


/**
 * 获取支付方式 进行默认支付方式设置
 */
public class GetCardListProtocol extends BaseProtocol {

    public GetCardListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/querycard/getCardList";
    }
}
