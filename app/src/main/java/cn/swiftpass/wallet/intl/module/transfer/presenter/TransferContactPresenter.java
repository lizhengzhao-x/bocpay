package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.TransferRecentlyProtocol;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferContactContract;


public class TransferContactPresenter extends BasePresenter<TransferContactContract.View> implements TransferContactContract.Presenter {
    @Override
    public void getContractList() {
        new TransferRecentlyProtocol(null,new NetWorkCallbackListener<ContractListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getContractListError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ContractListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getContractListSuccess(response);
                }
            }
        }).execute();
    }

    //    @Override
//    public void getRegEddaInfo() {

//    }
}
