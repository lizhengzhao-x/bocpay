package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/11/22.
 */
public class VirtualCardForgetPasswordSendOtpProtocol extends BaseProtocol {

    String action;
    String checkType;

    public VirtualCardForgetPasswordSendOtpProtocol(String actionIn, String checkTypeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.action = actionIn;
        this.checkType = checkTypeIn;
        mUrl = "api/virtual/forgetpwd/sendOtp";
    }


    @Override
    public void packData() {
        super.packData();
//        action	String	M	VIRPWDSET---虚拟卡忘记密码
//        checkType	String	M	1:信用卡 2：网银
        putIngoreNullParams(RequestParams.ACTION, action);
        putIngoreNullParams(RequestParams.CHECKTYPE, checkType);
    }
}
