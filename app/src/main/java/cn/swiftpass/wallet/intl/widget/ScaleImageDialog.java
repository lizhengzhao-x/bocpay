package cn.swiftpass.wallet.intl.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.bumptech.glide.request.RequestOptions;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.RotateTransformation;

/**
 * 沉浸式Dialog 隐藏状态栏 全面屏显示
 */
public class ScaleImageDialog extends Dialog {

    private View view;
    private ImageView idScaleImage;
    private Context context;

    public void setImage(Bitmap bitmap) {
        GlideApp.with(context)
                .load(bitmap)
                .apply(RequestOptions.bitmapTransform(new RotateTransformation(90f)))
                .into(idScaleImage);
    }


    public ScaleImageDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        initViews(context);
    }

    public static void fullScreen(Window window) {
        if (window == null) {
            return;
        }
        View decorView = window.getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        decorView.setSystemUiVisibility(option);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.TRANSPARENT);
        fitsNotchScreen(window);
    }

    /**
     * 适配刘海屏
     */
    private static void fitsNotchScreen(Window window) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            WindowManager.LayoutParams lp = window.getAttributes();
            lp.layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
            window.setAttributes(lp);
        }
    }

    private void initViews(Context context) {
        this.context = context;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        view = layoutInflater.inflate(R.layout.dialog_img_scale_view, null);
        idScaleImage = view.findViewById(R.id.id_scale_image);
        ImageView closeIv = view.findViewById(R.id.iv_close);
        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        if (getWindow() != null) {
            fullScreen(getWindow());
        }
        setContentView(view);
    }

    public ScaleImageDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        initViews(context);
    }
}
