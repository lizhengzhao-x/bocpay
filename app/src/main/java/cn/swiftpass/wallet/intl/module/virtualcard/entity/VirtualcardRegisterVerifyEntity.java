package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualcardRegisterVerifyEntity extends BaseEntity {

    private String phoneNo;
    private String nid;
    private String nidType;
    private String nidRgn;
    private String action;
    private String accNo;
    private String password;
    /**
     * 1：网银 2：ATM 3：信用卡
     */
    private String loginType;

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getEaiAction() {
        return eaiAction;
    }

    public void setEaiAction(String eaiAction) {
        this.eaiAction = eaiAction;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCheckAct() {
        return checkAct;
    }

    public void setCheckAct(String checkAct) {
        this.checkAct = checkAct;
    }

    public String getVirtualCardInd() {
        return virtualCardInd;
    }

    public void setVirtualCardInd(String virtualCardInd) {
        this.virtualCardInd = virtualCardInd;
    }

    private String verifyCode;
    private String pan;
    private String expiryDate;
    private String eaiAction;
    private String cvv;
    private String checkAct;
    private String virtualCardInd;

//    accNo	String	C	手机号
//    password	String	C	身份证号
//    loginType	String	C	1：网银 2：ATM 3：信用卡
//    verifyCode	String	C
//    pan	String	C
//    expiryDate	String	C
//    cvv	String	C
//    eaiAction	String	C	“B”
//    nid	String	C
//    nidType	String	C
//    nidRgn	String	C
//    phoneNo	String	C	852-11111111
//    checkAct	String	C	//R:register  F:forget
//    action	String	M	注册流程：‘VIRREG ‘ 忘记密码流程 ‘VIRPWDSET
//    virtualCardInd	String	C	是否是虚拟卡：Y/N

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getNidType() {
        return nidType;
    }

    public void setNidType(String nidType) {
        this.nidType = nidType;
    }

    public String getNidRgn() {
        return nidRgn;
    }

    public void setNidRgn(String nidRgn) {
        this.nidRgn = nidRgn;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


}
