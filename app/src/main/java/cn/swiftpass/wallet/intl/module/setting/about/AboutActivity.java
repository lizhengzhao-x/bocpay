package cn.swiftpass.wallet.intl.module.setting.about;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

/**
 *
 */

public class AboutActivity extends BaseCompatActivity {
    @BindView(R.id.id_version_code)
    TextView idVersionCode;
    @BindView(R.id.id_about_service)
    ImageArrowView idAboutService;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.setting_about);
        String version = (getString(R.string.version) + AndroidUtils.getCurrentAppVersionName(AboutActivity.this));
        if (!BuildConfig.FLAVOR.toLowerCase().contains(Constants.BUILD_FLAVOR_PRD)) {
            version = version + "_" + BuildConfig.BUILD_DATE;
        }
        idVersionCode.setText(version);
        idAboutService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(AboutActivity.this, ServerAgreementActivity.class);
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_about;
    }

}
