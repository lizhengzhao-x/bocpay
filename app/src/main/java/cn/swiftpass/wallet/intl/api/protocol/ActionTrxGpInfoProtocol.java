package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;

/**
 * 获取主扫验证积分换领信息接口
 */
public class ActionTrxGpInfoProtocol extends BaseProtocol {

    private MpQrUrlInfoResult mpQrUrlInfoResult;

    public ActionTrxGpInfoProtocol(MpQrUrlInfoResult mpQrUrlInfoResult, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.mpQrUrlInfoResult = mpQrUrlInfoResult;
        mUrl = "api/transaction/actGpInfo";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TRANAMT, mpQrUrlInfoResult.getTranAmt());
        mBodyParams.put(RequestParams.TRANCUR, mpQrUrlInfoResult.getTranCur());
        mBodyParams.put(RequestParams.QRCUSECASE, mpQrUrlInfoResult.getqRCUseCase());
        mBodyParams.put(RequestParams.MCC, mpQrUrlInfoResult.getmCC());
        mBodyParams.put(RequestParams.NAME, mpQrUrlInfoResult.getName());
        mBodyParams.put(RequestParams.CURRENCY, mpQrUrlInfoResult.getCountry());
        mBodyParams.put(RequestParams.CITY, mpQrUrlInfoResult.getCity());
        mBodyParams.put(RequestParams.NAMEAL, mpQrUrlInfoResult.getNameAL());
        mBodyParams.put(RequestParams.CITYAL, mpQrUrlInfoResult.getCityAL());
        mBodyParams.put(RequestParams.QRCTYPE, mpQrUrlInfoResult.getQrcType());
        mBodyParams.put(RequestParams.CARDNUMBER, mpQrUrlInfoResult.getCardNumber());
        mBodyParams.put(RequestParams.PANID, mpQrUrlInfoResult.getPandId());
        mBodyParams.put(RequestParams.CARD_ID, mpQrUrlInfoResult.getCardId());
        mBodyParams.put(RequestParams.MPQRCODE, mpQrUrlInfoResult.getQrcodeStr());
    }


}
