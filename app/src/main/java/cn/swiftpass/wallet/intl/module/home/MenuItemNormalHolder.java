package cn.swiftpass.wallet.intl.module.home;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.home
 * @class describe
 * @anthor zhangfan
 * @time 2019/10/30 18:57
 * @change
 * @chang time
 * @class describe
 */
class MenuItemNormalHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_item_image)
    ImageView ivItemImage;
    @BindView(R.id.tv_item_name)
    TextView tvItemName;

    public MenuItemNormalHolder(View normalView) {
        super(normalView);
        ButterKnife.bind(this, normalView);
    }

}
