package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;

/**
 * Created by ZhangXinchao on 2019/10/31.
 */
public class TransferItemTitleAdapter implements AdapterItem<ContractListEntity.RecentlyBean> {

    private View mId_contract_title_img;
    private TextView mId_contract_title_name;
    private TextView mId_contract_content_name;
    private OnItemContactClickCallback onItemContactClickCallback;

    public TransferItemTitleAdapter(Context context, OnItemContactClickCallback onItemContactClickCallbackIn) {
        onItemContactClickCallback = onItemContactClickCallbackIn;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.item_contract_title_view;
    }

    @Override
    public void bindViews(View root) {
        mId_contract_title_img = (View) root.findViewById(R.id.id_contract_title_img);
        mId_contract_title_name = (TextView) root.findViewById(R.id.id_contract_title_name);
        mId_contract_content_name = (TextView) root.findViewById(R.id.id_contract_content_name);
    }

    @Override
    public void setViews() {
        mId_contract_content_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemContactClickCallback.onItemClick();
            }
        });
    }

    @Override
    public void handleData(ContractListEntity.RecentlyBean addressBookItemEntity, int position) {
        mId_contract_title_name.setText(addressBookItemEntity.getUiTitle());
        if (!TextUtils.isEmpty(addressBookItemEntity.getUiContent())) {
            mId_contract_content_name.setVisibility(View.VISIBLE);
            mId_contract_content_name.setText(addressBookItemEntity.getUiContent());
        } else {
            mId_contract_content_name.setVisibility(View.GONE);
        }
    }

    public static class OnItemContactClickCallback {
        public void onItemClick() {
            // do nothing
        }
    }
}
