package cn.swiftpass.wallet.intl.utils;

public interface SoftKeyboardShowListener {
    void showSoftKeyboard();
    void hideSoftKeyboard();
}
