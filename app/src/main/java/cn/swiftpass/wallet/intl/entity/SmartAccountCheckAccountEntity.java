package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author create date on  on 2018/9/03 20:58
 * 我的账户下的账号
 */

public class SmartAccountCheckAccountEntity extends BaseEntity {
    /*
     *账号
     */
    public String accountNo;

    /*
     *名称
     */
    public String name;

    /*
     *名称
     */
    public String accType;


    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }


}
