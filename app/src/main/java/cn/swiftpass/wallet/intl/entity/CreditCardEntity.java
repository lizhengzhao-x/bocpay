package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class CreditCardEntity extends BaseEntity {

    private List<CardListBean> cardList;

    public List<CardListBean> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardListBean> cardList) {
        this.cardList = cardList;
    }

    public static class CardListBean {
        /**
         * cardName : 银联双币卡
         * panFour : 7896
         * panId : 650
         */

        private String cardName;
        private String panFour;
        private String panId;

        public boolean isExpand() {
            return isExpand;
        }

        public void setExpand(boolean expand) {
            isExpand = expand;
        }

        private boolean isExpand;

        public String getCardFaceId() {
            return cardFaceId;
        }

        public void setCardFaceId(String cardFaceId) {
            this.cardFaceId = cardFaceId;
        }

        private String cardFaceId;

        public String getUiTitle() {
            return uiTitle;
        }

        public void setUiTitle(String uiTitle) {
            this.uiTitle = uiTitle;
        }

        private String uiTitle;

        public ArrayList<CreditCardBillEntity.StmtTxnBean> getStmtTxnBeans() {
            return stmtTxnBeans;
        }

        public void setStmtTxnBeans(ArrayList<CreditCardBillEntity.StmtTxnBean> stmtTxnBeans) {
            this.stmtTxnBeans = stmtTxnBeans;
        }

        private ArrayList<CreditCardBillEntity.StmtTxnBean> stmtTxnBeans;

        public String getCardName() {
            return cardName;
        }

        public void setCardName(String cardName) {
            this.cardName = cardName;
        }

        public String getPanFour() {
            return panFour;
        }

        public void setPanFour(String panFour) {
            this.panFour = panFour;
        }

        public String getPanId() {
            return panId;
        }

        public void setPanId(String panId) {
            this.panId = panId;
        }
    }
}
