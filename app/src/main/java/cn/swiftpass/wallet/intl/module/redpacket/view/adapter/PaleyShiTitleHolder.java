package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.view.LiShiHistoryActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

public class PaleyShiTitleHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_title_paley_shi)
    TextView tvTitlePaleyShi;
    @BindView(R.id.tv_lishi_more)
    TextView tvLishiMore;

    public PaleyShiTitleHolder(Activity activity, View holder) {
        super(holder);
        ButterKnife.bind(this, itemView);
        tvTitlePaleyShi.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
        tvTitlePaleyShi.getPaint().setStrokeWidth(0.7f);
        tvLishiMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == activity) {
                    return;
                }
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.TRANSACTIONTYPE, Constants.SEND);
                ActivitySkipUtil.startAnotherActivity(activity, LiShiHistoryActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    public void setData(PaleyShiHomeEntity mPaleyShiHomeEntity) {

        //大于0显示更多
        if (mPaleyShiHomeEntity != null && mPaleyShiHomeEntity.getHasMoreRecords() > 0) {
            tvLishiMore.setVisibility(View.VISIBLE);
        } else {
            tvLishiMore.setVisibility(View.GONE);
        }

    }

}
