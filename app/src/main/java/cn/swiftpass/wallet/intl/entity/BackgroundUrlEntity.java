package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class BackgroundUrlEntity extends BaseEntity {

  private String  postLoginBgImageUrl;
  private String  postLoginBgImageVer;
  private String  preLoginBgImageUrl;
  private String  preLoginBgImageVer;

    public String getPostLoginBgImageUrl() {
        return postLoginBgImageUrl;
    }

    public void setPostLoginBgImageUrl(String postLoginBgImageUrl) {
        this.postLoginBgImageUrl = postLoginBgImageUrl;
    }

    /**
     * 为了保证图片能在版本或者地址变化时更新
     * @return
     */
    public String getPostLoginBgImageVer() {
        return postLoginBgImageUrl+postLoginBgImageVer;
    }

    public void setPostLoginBgImageVer(String postLoginBgImageVer) {
        this.postLoginBgImageVer = postLoginBgImageVer;
    }

    public String getPreLoginBgImageUrl() {
        return preLoginBgImageUrl;
    }

    public void setPreLoginBgImageUrl(String preLoginBgImageUrl) {
        this.preLoginBgImageUrl = preLoginBgImageUrl;
    }

    /**
     * 为了保证图片能在版本或者地址变化时更新
     * @return
     */
    public String getPreLoginBgImageVer() {
        return preLoginBgImageUrl+preLoginBgImageVer;
    }

    public void setPreLoginBgImageVer(String preLoginBgImageVer) {
        this.preLoginBgImageVer = preLoginBgImageVer;
    }
}
