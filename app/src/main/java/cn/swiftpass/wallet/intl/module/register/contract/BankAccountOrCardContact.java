package cn.swiftpass.wallet.intl.module.register.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;

public class BankAccountOrCardContact {
    public interface View extends IView {

        void getLoginVerifyCodeFailed(String errorCode, String errorMsg);

        void getLoginVerifyCodeSuccess(BankLoginVerifyCodeEntity response);

        void getUpdateAccountVerifyCodeFailed(String errorCode, String errorMsg);

        void getUpdateAccountVerifyCodeSuccess(BankLoginVerifyCodeEntity response);

        void virtualCardForgetPwdFailed(String errorCode, String errorMsg);

        void virtualCardForgetPwdSuccess(VirtualCardListEntity response);

        void virtualCardSmartAccountRegisterFailed(String errorCode, String errorMsg);

        void virtualCardSmartAccountRegisterSuccess(VirtualCardListEntity response);

        void bankLoginFailed(String errorCode, String errorMsg);

        void bankLoginSuccess(BankLoginResultEntity response);

        void sTwoUpdateAccountFailed(String errorCode, String errorMsg);

        void sTwoUpdateAccountSuccess(BankLoginResultEntity response);
    }

    public interface Presenter extends IPresenter<View> {


        void getLoginVerifyCode(String mLoginAction);

        void getUpdateAccountVerifyCode(String mLoginAction);

        void virtualCardForgetPwd(VirtualcardRegisterVerifyEntity parseExtraData);

        void virtualCardSmartAccountRegister(VirtualcardRegisterVerifyEntity parseExtraData);

        void bankLogin(String acc, String pass, String action, String type, String verifyCode);

        void sTwoUpdateAccount(String acc, String pass, String type, String loginAction, String verifyCode);
    }
}
