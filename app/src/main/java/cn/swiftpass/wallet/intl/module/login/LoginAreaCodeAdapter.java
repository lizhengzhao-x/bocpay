package cn.swiftpass.wallet.intl.module.login;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.AreaListEntity;
import cn.swiftpass.wallet.intl.entity.Constants;


/**
 * @author: xp
 * @date: 2017/7/19
 */

public class LoginAreaCodeAdapter extends RecyclerView.Adapter<LoginAreaCodeAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private List<AreaListEntity.TelListBean.ChildItemsBean> mData;
    private Context mContext;

    public LoginAreaCodeAdapter(Context context, List<AreaListEntity.TelListBean.ChildItemsBean> data) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
    }

    @Override
    public LoginAreaCodeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_sort, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvTag = (TextView) view.findViewById(R.id.tag);
        viewHolder.tvName = (TextView) view.findViewById(R.id.name);
        viewHolder.id_image_right = (ImageView) view.findViewById(R.id.id_image_right);
        viewHolder.id_root_view = (RelativeLayout) view.findViewById(R.id.id_root_view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final LoginAreaCodeAdapter.ViewHolder holder, final int position) {
        if (mData.get(position).getKey().equals("@")) {
            holder.tvTag.setVisibility(View.GONE);
        } else {
            int section = getSectionForPosition(position);
            //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
            holder.tvTag.setVisibility(View.VISIBLE);
            if (position == getPositionForSection(section)) {
                holder.tvTag.setVisibility(View.VISIBLE);
                holder.tvTag.setText(mData.get(position).getKey());
                if (mData.get(position).getKey().equals(Constants.TOP_HEADER_CHAR)) {
                    holder.tvTag.setVisibility(View.INVISIBLE);
                }
            } else {
                holder.tvTag.setVisibility(View.GONE);
            }
        }
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mOnItemClickListener.onItemClick(holder.itemView, position);
                }
            });

        }
        holder.tvName.setText(this.mData.get(position).getCountryCode() + " " + this.mData.get(position).getName());
        holder.id_image_right.setVisibility(this.mData.get(position).isSel() ? View.VISIBLE : View.INVISIBLE);
        holder.id_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(holder.itemView, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    //**********************itemClick************************
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
    //**************************************************************

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTag, tvName, id_text_right;
        ImageView id_image_right;
        RelativeLayout id_root_view;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * 提供给Activity刷新数据
     *
     * @param list
     */
    public void updateList(List<AreaListEntity.TelListBean.ChildItemsBean> list) {
        this.mData = list;
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        if (TextUtils.isEmpty(mData.get(position).getKey())) {
            return -1;
        } else {
            return mData.get(position).getKey().charAt(0);
        }

    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getKey();
            if (TextUtils.isEmpty(sortStr)) {
                return -1;
            } else {
                char firstChar = sortStr.toUpperCase().charAt(0);
                if (firstChar == section) {
                    return i;
                }
            }
        }
        return -1;
    }

}
