package cn.swiftpass.wallet.intl.module.login;

import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AreaListEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.SideBar;


/**
 * 支持用户登陆 多地区选择
 */
public class SelectLoginCodeActivity extends BaseCompatActivity {

    @BindView(R.id.id_edit_search)
    ClearEditText idEditSearch;
    @BindView(R.id.id_recyclerView)
    RecyclerView idRecyclerView;
    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    public static final String FROM_TYPE = "FROM_TYPE";
    public static final int FROM_TYPE_LOGIN = 1000;
    private LoginAreaCodeAdapter adapter;
    private ArrayList<AreaListEntity.TelListBean.ChildItemsBean> childItemsBeans;
    private String oldSelStr;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.title_region);
        idSideBar.setTextView(idDialog);
        oldSelStr = getIntent().getStringExtra(Constants.LAST_SELECT_COUNTRY);
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contains("\n")) {
                    return source.toString().replace("\n", "");
                } else {
                    return null;
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter};
        idEditSearch.setFilters(filters);
        //根据输入框输入值的改变来过滤搜索
        idEditSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        getCustomTelRegions();
        if (getIntent() != null) {
            //注册流程 输入框不用显示hint
            if (FROM_TYPE_LOGIN == getIntent().getIntExtra(FROM_TYPE, 0)) {
                idEditSearch.setHint("");
            }
        }

    }

    private void getCustomTelRegions() {
        ApiProtocolImplManager.getInstance().getCustomTelRegion(this, "1.0", new NetWorkCallbackListener<AreaListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(SelectLoginCodeActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(AreaListEntity response) {
                if (response != null && response.getTelList() != null && response.getTelList().size() > 0) {
                    updateUi(response.getTelList());
                }
            }
        });
    }


    private void updateUi(List<AreaListEntity.TelListBean> arrays) {

        if (arrays != null && arrays.size() > 0) {
            childItemsBeans = new ArrayList<>();
            AreaListEntity.TelListBean telListBeanTop = arrays.get(0);
            List<AreaListEntity.TelListBean.ChildItemsBean> childItemsTop = telListBeanTop.getChildItems();
            for (int j = 0; j < childItemsTop.size(); j++) {
                //设置A B C
                childItemsTop.get(j).setKey("@");
            }
            childItemsBeans.addAll(childItemsTop);

            for (int i = 1; i < arrays.size(); i++) {
                AreaListEntity.TelListBean telListBean = arrays.get(i);
                List<AreaListEntity.TelListBean.ChildItemsBean> childItems = telListBean.getChildItems();
                for (int j = 0; j < childItems.size(); j++) {
                    //设置A B C
                    childItems.get(j).setKey(telListBean.getKey());
                    if (!TextUtils.isEmpty(oldSelStr) && childItems.get(j).getCountryCode().equals(oldSelStr)) {
                        childItems.get(j).setSel(true);
                    }
                }
                childItemsBeans.addAll(childItems);
            }

            //默认选中的显示对号
            for (int i = 0; i < childItemsBeans.size(); i++) {
                AreaListEntity.TelListBean.ChildItemsBean telListBean = childItemsBeans.get(i);
                if (!TextUtils.isEmpty(oldSelStr) && telListBean.getCountryCode().equals(oldSelStr)) {
                    telListBean.setSel(true);
                }
            }
            LinearLayoutManager manager = new LinearLayoutManager(this);
            manager.setOrientation(LinearLayoutManager.VERTICAL);
            idRecyclerView.setLayoutManager(manager);
            adapter = new LoginAreaCodeAdapter(this, childItemsBeans);
            idRecyclerView.setAdapter(adapter);
            //item点击事件
            adapter.setOnItemClickListener(new LoginAreaCodeAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    AreaListEntity.TelListBean.ChildItemsBean itemsBean = ((AreaListEntity.TelListBean.ChildItemsBean) adapter.getItem(position));
                    Intent intent = new Intent();
                    intent.putExtra(Constants.DATA_COUNTRY_CODE, itemsBean.getCountryCode() + "");
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });

            //设置右侧SideBar触摸监听
            idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

                @Override
                public void onTouchingLetterChanged(String s) {
                    //该字母首次出现的位置
                    int position = adapter.getPositionForSection(s.charAt(0));
                    if (position != -1) {
                        ((LinearLayoutManager) idRecyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
                    }
                }
            });
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_select_countrycode;
    }


    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        ArrayList<AreaListEntity.TelListBean.ChildItemsBean> childItemsBeanChange = new ArrayList<>();
        if (childItemsBeans != null && childItemsBeans.size() > 0) {
            for (int i = 0; i < childItemsBeans.size(); i++) {
                AreaListEntity.TelListBean.ChildItemsBean telListBean = childItemsBeans.get(i);
                String name = telListBean.getName() + telListBean.getCountryCode();
                String firstSpell = Pinyin.toPinyin(name, "");
                if (name.indexOf(filterStr.toString()) != -1 ||
                        firstSpell.startsWith(filterStr.toString()) ||
                        //不区分大小写
                        firstSpell.toLowerCase().startsWith(filterStr.toString()) ||
                        firstSpell.toUpperCase().startsWith(filterStr.toString()) ||
                        name.contains(filterStr.toString()) ||
                        name.contains(filterStr.toString().toLowerCase()) ||
                        name.contains(filterStr.toString().toUpperCase())) {
                    childItemsBeanChange.add(telListBean);
                }
            }
            adapter.updateList(childItemsBeanChange);
        }
    }

}
