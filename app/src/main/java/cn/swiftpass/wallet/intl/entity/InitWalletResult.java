/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import java.io.Serializable;


/**
 * Class Name: InitWalletResult<br/>
 *
 * Class Description:
 *
 *
 * @author 86755221
 * @version 1   Date: 2018-1-19
 *
 */
public class InitWalletResult implements Serializable {
    private static final long serialVersionUID = 2135145936342829277L;
    private String panId;
    private String maskedToken;
    private String maskedCardNo;
    private String cardExpiryDate;
    private String cardArtId;
    private String isDefaultCard;


    //*********************ui添加字段*********************************//

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }

    private boolean isSel;

    //*********************ui添加字段*********************************//

    /**
     *
     */
    public InitWalletResult() {
        super();
    }

    /**
     * @param panId
     * @param maskedToken
     * @param maskedCardNo
     * @param cardExpiryDate
     * @param isDefaultCard
     */
    public InitWalletResult(String panId, String maskedToken, String maskedCardNo, String cardExpiryDate, String cardArtId, String isDefaultCard) {
        super();
        this.panId = panId;
        this.maskedToken = maskedToken;
        this.maskedCardNo = maskedCardNo;
        this.cardExpiryDate = cardExpiryDate;
        this.cardArtId = cardArtId;
        this.isDefaultCard = isDefaultCard;
    }

    /**
     * @return this.the panId
     */
    public String getPanId() {
        return this.panId;
    }

    /**
     * @param panId
     *            the panId to set
     */
    public void setPanId(String panId) {
        this.panId = panId;
    }

    /**
     * @return this.the maskedToken
     */
    public String getMaskedToken() {
        return this.maskedToken;
    }

    /**
     * @param maskedToken
     *            the maskedToken to set
     */
    public void setMaskedToken(String maskedToken) {
        this.maskedToken = maskedToken;
    }

    /**
     * @return this.the maskedCardNo
     */
    public String getMaskedCardNo() {
        return this.maskedCardNo;
    }

    /**
     * @param maskedCardNo
     *            the maskedCardNo to set
     */
    public void setMaskedCardNo(String maskedCardNo) {
        this.maskedCardNo = maskedCardNo;
    }

    /**
     * @return this.the cardExpiryDate
     */
    public String getCardExpiryDate() {
        return this.cardExpiryDate;
    }

    /**
     * @return the cardArtId
     */
    public String getCardArtId() {
        return this.cardArtId;
    }

    /**
     * @param cardArtId the cardArtId to set
     */
    public void setCardArtId(String cardArtId) {
        this.cardArtId = cardArtId;
    }

    /**
     * @param cardExpiryDate
     *            the cardExpiryDate to set
     */
    public void setCardExpiryDate(String cardExpiryDate) {
        this.cardExpiryDate = cardExpiryDate;
    }

    /**
     * @return this.the isDefaultCard
     */
    public String getIsDefaultCard() {
        return this.isDefaultCard;
    }

    /**
     * @param isDefaultCard
     *            the isDefaultCard to set
     */
    public void setIsDefaultCard(String isDefaultCard) {
        this.isDefaultCard = isDefaultCard;
    }

}
