package cn.swiftpass.wallet.intl.module.activitys;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.NewMsgEntity;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;
import cn.swiftpass.wallet.intl.module.home.NewMessageAdapter;
import cn.swiftpass.wallet.intl.module.home.contract.NewMsgContract;
import cn.swiftpass.wallet.intl.module.home.presenter.NewMsgPresenter;
import cn.swiftpass.wallet.intl.module.home.utils.BannerUtils;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

public class NewMsgFragment extends BaseFragment<NewMsgContract.Presenter> implements NewMsgContract.View {
    @BindView(R.id.ry_new_msg)
    RecyclerView ryNewMsg;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    private NewMessageAdapter newMessageAdapter;
    private List<NewMsgEntity> newMsgList;

    private View emptyView;

    MyItemDecoration myItemDecoration;

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {

        mActivity.setToolBarTitle(R.string.what_is_new);
    }

    @Override
    protected NewMsgContract.Presenter loadPresenter() {
        return new NewMsgPresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_new_msg;
    }

    @Override
    protected void initView(View v) {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 24f);

        myItemDecoration = MyItemDecoration.createVertical(getContext().getColor(R.color.app_white), ryLineSpace);

        ryNewMsg.addItemDecoration(myItemDecoration);
        ryNewMsg.setLayoutManager(mLayoutManager);
        newMsgList = new ArrayList<>();
        newMessageAdapter = new NewMessageAdapter(mContext, newMsgList);
        newMessageAdapter.bindToRecyclerView(ryNewMsg);


        newMessageAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) return;
                if (view.getId() == R.id.iv_new_msg) {
                    BannerUtils.clickBannerEvent(getActivity(), newMsgList.get(position));
                }
            }
        });
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mPresenter.getMsgList();
            }
        });
        mPresenter.getMsgList();
    }

    /**
     * 页面添加空布局  直接用该方法
     *
     * @param mContext
     * @param recyclerView
     * @param textId
     * @param imgId
     * @return
     */
    protected View getEmptyView(Context mContext, RecyclerView recyclerView, int textId, int imgId) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(textId));
        if (imgId == -1) {
            noDataImg.setVisibility(View.GONE);
        } else {
            noDataImg.setVisibility(View.VISIBLE);
            noDataImg.setImageResource(imgId);
        }
        return emptyView;
    }


    @Override
    public void getActListSuccess(NewMsgListEntity activityEntity) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        }

        emptyView = getEmptyView(mContext, ryNewMsg, R.string.string_activity_no, -1);
        newMessageAdapter.setEmptyView(emptyView);

        if (null == activityEntity) {
            return;
        }
        newMsgList.clear();
        newMsgList.addAll(activityEntity.getPromotionResults());
        if (newMsgList.size() == 0) {
            /**
             * 需要删除itemDecoration  保证emptyView的位置是居中的
             */
            ryNewMsg.removeItemDecoration(myItemDecoration);

            emptyView.setVisibility(View.VISIBLE);
        } else {
            /**
             * 判断itemDecor的个数来要不要添加itemDecoration
             */
            if (ryNewMsg.getItemDecorationCount() <= 0) {
                ryNewMsg.addItemDecoration(myItemDecoration);
            }

            emptyView.setVisibility(View.GONE);
            newMessageAdapter.setDataList(newMsgList);
        }
    }

    @Override
    public void getActListFail(String errorCode, String errorMsg) {
        //停止刷新
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        } else {
            return;
        }
        emptyView = getEmptyView(mContext, ryNewMsg, R.string.string_activity_no, -1);
        newMessageAdapter.setEmptyView(emptyView);
        emptyView.setVisibility(View.VISIBLE);
        newMessageAdapter.notifyDataSetChanged();
        showErrorMsgDialog(mContext, errorMsg);
    }
}
