package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SortEntity;


/**
 * @author: xp
 * @date: 2017/7/19
 */

public class SortAdapter extends RecyclerView.Adapter<SortAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    private List<SortEntity> mData;
    private Context mContext;

    public SortAdapter(Context context, List<SortEntity> data) {
        mInflater = LayoutInflater.from(context);
        mData = data;
        this.mContext = context;
    }

    @Override
    public SortAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_sort, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.tvTag = (TextView) view.findViewById(R.id.tag);
        viewHolder.tvName = (TextView) view.findViewById(R.id.name);
        viewHolder.id_root_view = (RelativeLayout) view.findViewById(R.id.id_root_view);
        viewHolder.id_text_right = (TextView) view.findViewById(R.id.id_text_right);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final SortAdapter.ViewHolder holder, final int position) {
        int section = getSectionForPosition(position);
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        holder.tvTag.setVisibility(View.VISIBLE);
        if (position == getPositionForSection(section)) {
            holder.tvTag.setVisibility(View.VISIBLE);
            holder.tvTag.setText(mData.get(position).getLetters());
            if (mData.get(position).getLetters().equals(Constants.TOP_HEADER_CHAR)) {
                holder.tvTag.setVisibility(View.INVISIBLE);
            }
        } else {
            holder.tvTag.setVisibility(View.GONE);
        }

        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    mOnItemClickListener.onItemClick(holder.itemView, position);
                }
            });

        }
        String titleText = this.mData.get(position).getName();
        if (titleText.contains(Constants.SPECIAL_LETTER_TEXT)) {
            String leftText = titleText.substring(0, titleText.indexOf(Constants.SPECIAL_LETTER_TEXT));
            holder.tvName.setText(leftText);
            String rightText = titleText.substring(titleText.indexOf(Constants.SPECIAL_LETTER_TEXT) + Constants.SPECIAL_LETTER_TEXT.length(), titleText.length());
            holder.id_text_right.setText(rightText);
        } else if (titleText.contains(Constants.TOP_HEADER_CHARALL)) {
            String leftBankText = titleText.substring(0, titleText.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT)).replace(Constants.TOP_HEADER_CHARALL, "");
            String leftCodeText = titleText.substring(titleText.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT) + Constants.SPECIAL_LETTER_BANK_TEXT.length(), titleText.length());
            holder.tvName.setText(leftCodeText + " " + leftBankText);
        } else if (titleText.contains(Constants.SPECIAL_LETTER_BANK_TEXT)) {
            String leftBankText = titleText.substring(0, titleText.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT));
            String leftCodeText = titleText.substring(titleText.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT) + Constants.SPECIAL_LETTER_BANK_TEXT.length(), titleText.length());
            holder.tvName.setText(leftCodeText + " " + leftBankText);
        } else if (titleText.contains(Constants.TOP_HEADER_CHAR_CUSTOM)) {
            String leftCodeText = titleText.substring(titleText.indexOf(Constants.TOP_HEADER_CHAR_CUSTOM) + 1, titleText.length());
            holder.tvName.setText(leftCodeText);
        } else {
            holder.tvName.setText(this.mData.get(position).getName());
        }

        holder.id_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(holder.itemView, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    //**********************itemClick************************
    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
    //**************************************************************

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTag, tvName, id_text_right;
        RelativeLayout id_root_view;

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * 提供给Activity刷新数据
     *
     * @param list
     */
    public void updateList(List<SortEntity> list) {
        this.mData = list;
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return mData.get(position);
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        return mData.get(position).getLetters().charAt(0);
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mData.get(i).getLetters();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }

}
