package cn.swiftpass.wallet.intl.base.password;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;

/**
 * 验证密码 通用
 * Created by ZhangXinchao on 2019/7/16.
 */
public interface VerifyPwdView<V extends IPresenter> {
    void verifyPwdFailed(String errorCode, String errorMsg);

    void verifyPwdSuccess();
}
