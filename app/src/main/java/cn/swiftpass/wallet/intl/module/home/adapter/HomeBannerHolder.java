package cn.swiftpass.wallet.intl.module.home.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cn.swiftpass.wallet.intl.R;

public class HomeBannerHolder extends RecyclerView.ViewHolder {
    public ImageView homeBannerIv;

    public HomeBannerHolder(@NonNull View itemView) {
        super(itemView);
        homeBannerIv = itemView.findViewById(R.id.iv_home_bottom_banner);
    }
}
