package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.io.IOException;
import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.QrcodeScanListener;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.widget.ViewfinderView;
import cn.swiftpass.boc.commonui.base.zxing.BeepManager;
import cn.swiftpass.boc.commonui.base.zxing.camera.CameraManager;
import cn.swiftpass.boc.commonui.base.zxing.decoding.CaptureActivityHandler;
import cn.swiftpass.boc.commonui.base.zxing.decoding.DecodeFormatManager;
import cn.swiftpass.boc.commonui.base.zxing.decoding.InactivityTimer;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.UpQrFpsEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.FileUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ParseQrCodeAysnTask;


/**
 * FPS缴费 应该只能识别商户码 其他二维码都应该报错弹框
 */
public class FPSPaymentScanFragment extends BaseFragment implements SurfaceHolder.Callback {
    private static final String TAG = FPSPaymentScanFragment.class.getSimpleName();
    private static final int REQUEST_PICK_FORM_ALBUM = 0x16;
    @BindView(R.id.preview_view)
    SurfaceView surfaceView;
    @BindView(R.id.viewfinder_view)
    ViewfinderView viewfinderView;
    @BindView(R.id.id_buttom_msg)
    TextView id_buttom_msg;

    @BindView(R.id.id_top_smarttitle)
    TextView fpsAcctountTitle;

    @BindView(R.id.id_buttom_fps_view)
    LinearLayout mButtomFpsView;
    @BindView(R.id.id_relative_tips)
    RelativeLayout mFpsTipsView;


    private boolean mSurfaceViewCreated;
    private String characterSet;

    private CaptureActivityHandler handler;
    private Collection<BarcodeFormat> decodeFormats;
    private Map<DecodeHintType, ?> decodeHints;
    private BeepManager beepManager;
    private InactivityTimer inactivityTimer;
    private View rootView;
    private QrcodeScanListener qrcodeScanListener;
    private boolean hasFlashLight = false;
    private Camera camera = null;
    ////是否处于支付模式/校验密码跳转模式
    private boolean isPayMode;
    //扫描到的二维码对象
    private UpQrFpsEntity mUpQrFpsEntity;
    private Handler mTransferScanHandler;
    private CameraManager cameraManager;

    //转账
    private int currentScanType;

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    private CustomDialog mDealDialog;

    //是否第一次选择拒绝且不再询问权限
    public boolean firstRejectCamera = true;

    /**
     * 扫码报错 点击recently回来不能预览
     */
    private boolean hasDialogShow;


    private void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.string_camera_use));
        builder.setMessage(getString(R.string.string_turn_on_camera_pay));

        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mActivity);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transfer_scan;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.FPSBP2101_2_1);
            mActivity.setToolBarRightViewToText(R.string.album);
            mActivity.getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    pickFromAlbum();
                }
            });
        }
    }

    private void initScanListener() {
        qrcodeScanListener = new QrcodeScanListener() {
            @Override
            public void scanResult(boolean isOk, String result) {
            }

            @Override
            public void submitData(String result, boolean status) {
            }

            @Override
            public void drawViewfinder() {
                viewfinderView.drawViewfinder();
            }

            @Override
            public void handleDecode(Result obj, Bitmap barcode) {
                handleScanResult(obj.getText());
            }

            @Override
            public void setResult(int resultOk, Intent obj) {

            }

            @Override
            public ViewfinderView getViewfinderView() {
                return viewfinderView;
            }

            @Override
            public Handler getHandler() {
                return handler;
            }

            @Override
            public CameraManager getCameraManager() {
                return cameraManager;
            }

            @Override
            public Context getContext() {
                return getContext();
            }
        };


    }


    private void handleScanResult(String text) {
        if (!TextUtils.isEmpty(text)) {
            beepManager.playBeepSoundAndVibrate();
            LogUtils.i(TAG, "message;" + text);
            stopPreview();
            parseTransferCode(text);
//            if (currentScanType == Constants.SCAN_TO_TRANSFER) {
//                parseTransferCode(text);
//            } else {
//                parseCodeStr(text);
//            }
        } else {
            handleScanError();
        }
    }

    /**
     * 停止预览并扫描 收银台/密码等弹出层出现 背景扫描应该停止
     */
    protected void stopPreview() {
        if (handler != null) {
            handler.stopPreviewAndDecode();
            viewfinderView.setStopDraw(true);
        }
    }


    private void parseTransferCode(String code) {
        ApiProtocolImplManager.getInstance().parserTransferCode(mActivity, code, new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                hasDialogShow = true;
                showErrorMsgDialog(mActivity, errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        hasDialogShow = false;
                        restartPreviewAfterDelay(500);
                    }
                });

            }

            @Override
            public void onSuccess(Object response) {
                ParserQrcodeEntity parserQrcodeEntity = (ParserQrcodeEntity) response;
                if (TextUtils.equals(parserQrcodeEntity.getPaymentCategory(), TransferConst.TRANS_TYPE_MERCHANT)) {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.TYPE, Constants.SCAN_TO_TRANSFER);
                    intent.putExtra(TransferConst.TRANS_FPS_DATA, parserQrcodeEntity);
                    intent.setClass(getActivity(), TransferInputMoneyActivity.class);
                    startActivity(intent);
                } else {
                    showErrorMsgDialog(getContext(), getString(R.string.FPSBP2101_10_1), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            restartPreviewAfterDelay(500);
                        }
                    });
                }
            }
        });
    }

//    /**
//     * 二维码解析
//     *
//     * @param content
//     */
//    private void parseCodeStr(final String content) {
//        if (dealWithEmvCode(content)) {
//            return;
//        } else {
//            handleScanError();
//        }
//    }


//    /**
//     * emv码的解析处理
//     * 区分银联商户二
//     *
//     * @param content
//     */
//    private boolean dealWithEmvCode(String content) {
//        HashMap<String, String> resultMap = ParseQRUtils.parseEMV(content);
//        if (resultMap == null || resultMap.isEmpty()) {
//            return false;
//        }
//        //15,16-Reserved for UnionPay  26-Reserved for the Faster Payment System for use in Hong Kong
//        if (!resultMap.containsKey("26")) {
//            return false;
//        }
//        mUpQrFpsEntity = new UpQrFpsEntity();
//        if (resultMap.containsKey("53")) {
//            mUpQrFpsEntity.setTranCur(resultMap.get("53"));
//        }
//        if (resultMap.containsKey("54")) {
//            mUpQrFpsEntity.setTranAmt(resultMap.get("54"));
//        }
//        if (resultMap.containsKey("58")) {
//            mUpQrFpsEntity.setCountry(resultMap.get("58"));
//        }
//        if (resultMap.containsKey("59")) {
//            mUpQrFpsEntity.setName(resultMap.get("59"));
//        }
//        if (resultMap.containsKey("60")) {
//            mUpQrFpsEntity.setCity(resultMap.get("60"));
//        }
//        if (resultMap.containsKey("63")) {
//            mUpQrFpsEntity.setPostal(String.valueOf(Base64.decode(resultMap.get("63"))));
//        }
//
//        //判断扫码出来是否带金额 不带金额跳转InputMoneyActivity
//        mUpQrFpsEntity.setQrcodeStr(content);
//        if (resultMap.containsKey("26")) {
//            toDealFPS(resultMap);
//            return true;
//        }
//        return false;
//    }

//    private void toDealFPS(HashMap<String, String> resultMap) {
//        String fspContent = resultMap.get("26");
//        HashMap<String, String> fpsMap = ParseQRUtils.parseEMV(fspContent);
//        if (fpsMap == null)
//            return;
//        if (fpsMap.containsKey("00")) {
//            mUpQrFpsEntity.setGlobalId(fpsMap.get("00"));
//        }
//        if (fpsMap.containsKey("01")) {
//            mUpQrFpsEntity.setClearCode(fpsMap.get("01"));
//        }
//        if (fpsMap.containsKey("02")) {
//            mUpQrFpsEntity.setFpsId(fpsMap.get("02"));
//        }
//        if (fpsMap.containsKey("03")) {
//            mUpQrFpsEntity.setPhoneNo(fpsMap.get("03"));
//        }
//        if (fpsMap.containsKey("04")) {
//            mUpQrFpsEntity.setEmailAddress(fpsMap.get("04"));
//        }
//        if (fpsMap.containsKey("05")) {
//            mUpQrFpsEntity.setTimeout(fpsMap.get("05"));
//        }
//        Intent intent = new Intent();
//        intent.putExtra(TransferConst.TRANS_FPS_DATA, mUpQrFpsEntity);
//        intent.setClass(getActivity(), TransferInputMoneyActivity.class);
//        startActivity(intent);
//    }


    @Override
    protected void initView(View v) {
        //检查是否有相机权限
        if (!isGranted_(WalletConstants.PERMISSIONS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS[0])) {
                //拒绝并勾选不再询问
                firstRejectCamera = false;
            } else {
                firstRejectCamera = true;
            }
        }

        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        rootView = v;

        boolean isSmallPixels = AndroidUtils.isSmallPixels(getActivity());
        if (isSmallPixels) {
            //适配小分辨率手机(小于16 : 9)，防止图标遮挡
            viewfinderView.setMarginTopHeight(AndroidUtils.dip2px(getContext(), 120));
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mFpsTipsView.getLayoutParams();
            layoutParams.topMargin = AndroidUtils.dip2px(getActivity(), 20);
            mFpsTipsView.setLayoutParams(layoutParams);
        } else {
            viewfinderView.setMarginTopHeight(AndroidUtils.dip2px(getContext(), 160));
        }
        //FPS缴费 UI不一样
        mButtomFpsView.setVisibility(View.VISIBLE);
        fpsAcctountTitle.setVisibility(View.VISIBLE);

        int topHeight = viewfinderView.getScanViewButtomPos();
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mButtomFpsView.getLayoutParams();
        if (isSmallPixels) {
            //适配小分辨率手机，防止图标遮挡
            lp.topMargin = topHeight + AndroidUtils.dip2px(getContext(), 5);
        } else {
            lp.topMargin = topHeight + AndroidUtils.dip2px(getContext(), 10);
        }
        mButtomFpsView.setLayoutParams(lp);


        inactivityTimer = new InactivityTimer(getActivity());
        beepManager = new BeepManager(getActivity());
        PackageManager pm = getActivity().getPackageManager();
        hasFlashLight = pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
        mTransferScanHandler = new Handler();
    }


    /*
     * 从相册选择二维码
     */
    private void pickFromAlbum() {
        Intent innerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        innerIntent.setType("image/*");
        Intent wrapperIntent = Intent.createChooser(innerIntent, getResources().getString(R.string.choose_qrcode_fps));
        this.startActivityForResult(wrapperIntent, REQUEST_PICK_FORM_ALBUM);
    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        try {
            LogUtils.i(TAG, "打开相机:" + isVisible());
            initScanListener();
            cameraManager.openDriver(surfaceHolder);
            if (handler == null) {
                handler = new CaptureActivityHandler(qrcodeScanListener, decodeFormats, characterSet, decodeHints, mTransferScanHandler, cameraManager, !hasDialogShow);
            }
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
        } catch (RuntimeException e) {
            Log.w(TAG, "Unexpected error initializing camera", e);
        }

    }


    /**
     * 重新开始扫描解析
     *
     * @param delayMS
     */
    public void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.startPreviewAndDecode();
            viewfinderView.setStopDraw(false);
            viewfinderView.reOnDraw();
            handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
        }
    }

    public void handleScanError() {
        try {
            handler.stopPreviewAndDecode();
            viewfinderView.setStopDraw(true);
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (getActivity() != null) {
            hasDialogShow = true;
            showErrorMsgDialog(getActivity(), getString(R.string.QRW209_1_1), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    hasDialogShow = false;
                    restartPreviewAfterDelay(500);
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // historyManager must be initialized here to update the history preference
        if (mNeedRequestPermissions && AndroidUtils.needRequestApplyPermissien()) {
            checkPermissions(WalletConstants.PERMISSIONS);
            mNeedRequestPermissions = false;
        }
        cameraManager = new CameraManager(getContext());
        decodeFormats = EnumSet.noneOf(BarcodeFormat.class);
        decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
        viewfinderView.setCameraManager(cameraManager);
        resumeCamera();
        inactivityTimer.onResume();
        if (viewfinderView != null) {
            viewfinderView.setOnFlashLightStateChangeListener(new ViewfinderView.onFlashLightStateChangeListener() {
                @Override
                public void openFlashLight(boolean open) {
                    turnOnFlashLight(open);
                    viewfinderView.reOnDraw();
                }
            });
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    //判断是否点击了 不再询问
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permissions[i])) {

                        if (isGranted(WalletConstants.PERMISSIONS[0])) {
                            if (mDealDialog != null) {
                                mDealDialog.dismiss();
                            }
                        } else {
                            if (!firstRejectCamera) {
                                showLackOfPermissionDialog();
                            } else {
                                //如果是第一次选择不再询问并拒绝  则不弹出自定义权限选择框
                                firstRejectCamera = false;
                            }
                        }
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    protected void turnOnFlashLight(boolean open) {
        if (cameraManager != null) {
            cameraManager.setTorch(open);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    private void releaseCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        if (inactivityTimer != null) {
            inactivityTimer.onPause();
        }
        if (mSurfaceViewCreated && surfaceView != null) {
            surfaceView = rootView.findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
            mSurfaceViewCreated = false;
            LogUtils.i(TAG, "releaseCamera--->mSurfaceViewCreated" + mSurfaceViewCreated);
        }
        turnOnFlashLight(false);
        //viewfinderView.setOpenFlashLight(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (cameraManager != null) {
            cameraManager.closeDriver();
        }
    }

    private void resumeCamera() {
        LogUtils.i(TAG, "isVisible--->" + isVisible() + " hasSurface:" + mSurfaceViewCreated);
        if (!isVisible()) {
            return;
        }
        surfaceView = rootView.findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        LogUtils.i(TAG, "addCallback:");
        surfaceHolder.addCallback(this);
        initCamera(surfaceHolder);
        viewfinderView.reOnDraw();
        beepManager.updatePrefs();
        inactivityTimer.onResume();
        viewfinderView.setOpenFlashLight(false);
        decodeFormats = null;
        characterSet = null;
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.i(TAG, "hidden:" + hidden);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (inactivityTimer != null) {
            inactivityTimer.shutdown();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        LogUtils.i(TAG, "surfaceCreated---> " + mSurfaceViewCreated);
        if (!mSurfaceViewCreated) {
            mSurfaceViewCreated = true;
            initCamera(surfaceHolder);
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        LogUtils.i(TAG, "surfaceChanged---> ");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        mSurfaceViewCreated = false;
        LogUtils.i(TAG, "surfaceDestroyed---> ");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PICK_FORM_ALBUM:
                    // 从相册选择图片识别二维码
                    Uri photoUri = data.getData();
                    scanPickPhoto(photoUri);
                    break;
                default:
                    break;

            }
        }
    }


    /**
     * 解析选择图片中的二维码
     */
    private void scanPickPhoto(Uri photoUri) {
        if (photoUri == null) {
            handleScanError();
        } else {
            ApiProtocolImplManager.getInstance().showDialog(getActivity());
            Bitmap bitmap = null;
            try {
                bitmap = FileUtils.getBitmapFormUri(getActivity(), photoUri);
            } catch (IOException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            if (bitmap == null) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                handleScanError();
            } else {
                ParseQrCodeAysnTask.onParseResultListener listener = new ParseQrCodeAysnTask.onParseResultListener() {
                    @Override
                    public void onResult(String res) {
                        ApiProtocolImplManager.getInstance().dismissDialog();
                        if (null != res) {
                            handleScanResult(res);
                        } else {
                            handleScanError();
                        }
                    }
                };
                new ParseQrCodeAysnTask(bitmap, listener).execute();
            }
        }
    }
}
