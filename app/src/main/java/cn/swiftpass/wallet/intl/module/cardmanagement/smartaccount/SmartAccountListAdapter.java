package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SmartAccountListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class SmartAccountListAdapter implements AdapterItem<SmartAccountListEntity.RowsBean> {
    private Context context;
    private TextView id_smart_account_info;
    private ImageView id_id_show_sel;
    private AccountItemCallback accountItemCallback;
    private View rootView;

    public SmartAccountListAdapter(Context context, AccountItemCallback accountItemCallback) {
        this.context = context;
        this.accountItemCallback = accountItemCallback;
    }


    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_smartaccount;
    }

    @Override
    public void bindViews(View root) {
        rootView = root.findViewById(R.id.id_root_view);
        id_smart_account_info = root.findViewById(R.id.id_smart_account_info);
        id_id_show_sel = root.findViewById(R.id.id_id_show_sel);
    }


    @Override
    public void setViews() {
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountItemCallback.onItemAccountClick(mPosition);
            }
        });
    }

    @Override
    public void handleData(SmartAccountListEntity.RowsBean text, int position) {
        mPosition = position;
        String account = AndroidUtils.getPrimaryAccountDisplay(text.getAccType(), text.getAccountNo());
        id_smart_account_info.setText(account);
        id_id_show_sel.setVisibility(text.isSel() ? View.VISIBLE : View.INVISIBLE);
    }


    public static class AccountItemCallback {
        public void onItemAccountClick(int position) {
            // do nothing
        }
    }
}
