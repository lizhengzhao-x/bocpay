package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.graphics.Color;
import android.view.View;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferContactContract;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.FrequentContactAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactLocalCacheUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * 常用联系人
 */
public class FrequentContactFragment extends BaseTransferSelFragment<ContactEntity, TransferContactContract.Presenter> {
    @BindView(R.id.ry_contact)
    RecyclerView ryContact;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    MyItemDecoration myItemDecoration;

    private FrequentContactAdapter frequentContactAdapter;

    private static final String TAG = "RecentlyCollectionContactFrament";
    private List<ContactEntity> itemCommonCollectionList = new ArrayList<>();

    public static FrequentContactFragment newInstance() {
        return new FrequentContactFragment();
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_UPDATE_LOCAL_CACHE_CONTACT) {
            itemCommonCollectionList.clear();
            filterWithLocalAddress(false);
            updateSelStatus();
            frequentContactAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getView() != null) {
            getView().setBackgroundColor(Color.TRANSPARENT);
        }
    }

    /**
     * 本地数据与通讯录匹配
     */
    private void filterWithLocalAddress(boolean refresh) {

        List<String> contactIDs = ContactLocalCacheUtils.getLocalCacheWithUserId(getContext(), HttpCoreKeyManager.getInstance().getUserId());
        ArrayList<ContactEntity> contactEntities = null;
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
        } else {
            contactEntities = ContactUtils.getInstance().getAllContacts(getContext(), refresh);
        }
        if (contactEntities == null || contactEntities.size() == 0) {
            return;
        }
        for (int i = 0; i < contactIDs.size(); i++) {
            String contactID = contactIDs.get(i);
            for (int j = 0; j < contactEntities.size(); j++) {
                ContactEntity contactEntitItem = contactEntities.get(j);
                if (contactEntitItem.getContactID().equals(contactID)) {
                    itemCommonCollectionList.add(contactEntitItem);
                    break;
                }
            }
        }
    }

    /**
     * 默认收藏列表中都是收藏状态
     */
    private void updateSelStatus() {
        for (int i = 0; i < itemCommonCollectionList.size(); i++) {
            itemCommonCollectionList.get(i).setCollect(true);
        }
    }

    @Override
    protected TransferContactContract.Presenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_common_collection_contact_view;
    }

    @Override
    protected void initView(View v) {
        LogUtils.i(TAG, "initView RecentlyCollectionContactFrament--->");

        frequentContactAdapter = new FrequentContactAdapter(itemCommonCollectionList);
        frequentContactAdapter.bindToRecyclerView(ryContact);

        initRecycleView(ryContact, swipeRefreshLayout, frequentContactAdapter);

        frequentContactAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.ll_contact_collect_imag:
                        //清除本地存储记录
                        ContactLocalCacheUtils.removeContractToLocalCacheWithUserId(getContext(), itemCommonCollectionList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                        EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_ADDRESS_COLLECTION_STATUS, ""));
                        itemCommonCollectionList.remove(position);
                        frequentContactAdapter.notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        });

        frequentContactAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                startAnotherActivity(getActivity(), isTransferNormalType, itemCommonCollectionList.get(position).getNumber(), itemCommonCollectionList.get(position).getUserName(), null, itemCommonCollectionList.get(position).isEmail());
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 24f);
        myItemDecoration = MyItemDecoration.createVertical(getContext().getColor(R.color.app_white), ryLineSpace);

        ryContact.addItemDecoration(myItemDecoration);


        //判断当前是否有通讯录权限
        if (isGranted(Manifest.permission.READ_CONTACTS)) {
            //如果有 跟本地缓存列表对比显示
            updateRecycleView(true);
        } else {
            //直接显示空
            emptyView.setVisibility(View.VISIBLE);
            /**
             * 需要删除itemDecoration  保证emptyView的位置是居中的
             */
            ryContact.removeItemDecoration(myItemDecoration);
        }
    }


    @Override
    protected void updateContactList(ArrayList<ContactEntity> contactEntities, boolean obtain) {
//        if (!obtain) {
//            itemCommonCollectionList.clear();
//            updateRecycleView();
//            idEmptyTvView.setText(R.string.TR1_4b_1);
//        } else {
//            updateRecycleView(false);
//        }
    }

    public void updateRecycleView(boolean refresh) {
        if (ryContact == null) {
            return;
        }
        itemCommonCollectionList.clear();
        filterWithLocalAddress(refresh);
        updateSelStatus();
        frequentContactAdapter.notifyDataSetChanged();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void updateItemList() {
        swipeRefreshLayout.setRefreshing(false);
    }

    public List<ContactEntity> getCollections() {
        return itemCommonCollectionList;
    }
}
