package cn.swiftpass.wallet.intl.entity;

import android.content.Intent;
import android.os.Handler;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.login.SelCountyAdapter;


public class SelCountyActivity extends BaseCompatActivity {


    private RecyclerView mRecyclerView;
    private SelCountyAdapter mAdapter;
    private List<SelCountyEntity> mList = new ArrayList<>();

    private String choiceAreaPosition;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.title_region);
        initData();
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_info);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new SelCountyAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new SelCountyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, final SelCountyEntity bean) {
                for (int i = 0; i < mList.size(); i++) {
                    if (i == position) {
                        mList.get(i).setSelection(true);
                    } else {
                        mList.get(i).setSelection(false);
                    }
                }
                mAdapter.notifyDataSetChanged();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.DATA_COUNTRY_CODE, bean.getCountyCode());
                        setResult(Constants.REQUEST_CODE_SEL_COUNTRY, intent);
                        SelCountyActivity.this.finish();
                    }
                }, 200);
            }
        });

    }

    private void initData() {

        mList.add(new SelCountyEntity(Constants.DATA_COUNTRY_CODES_SPACE[0], getString(R.string.P3_A1_23_b_7), false));
        mList.add(new SelCountyEntity(Constants.DATA_COUNTRY_CODES_SPACE[1], getString(R.string.P3_ewa_01_009), false));
        mList.add(new SelCountyEntity(Constants.DATA_COUNTRY_CODES_SPACE[2], getString(R.string.P3_ewa_01_010), false));
        choiceAreaPosition = getIntent().getStringExtra(Constants.CHOICE_AREA_POSITION);
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).getCountyCode().equals(choiceAreaPosition.replace(" ", ""))) {
                mList.get(i).setSelection(true);
            } else {
                mList.get(i).setSelection(false);
            }
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_sel_county;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
