package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;

/**
 * Created by ramon on 2018/8/20.
 * 转账试算返回结果
 */

public class TransferPreCheckEntity extends BaseEntity {

    //待添加
    private String accountNo;

    public boolean isBocPayTransfer() {
        return isBocPayTransfer;
    }

    public void setBocPayTransfer(boolean bocPayTransfer) {
        isBocPayTransfer = bocPayTransfer;
    }

    /**
     * 区分是bocpay转账还是fps转账
     */
    private boolean isBocPayTransfer;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    private String userName;

    public String getSrvcChrgDrCur() {
        return srvcChrgDrCur;
    }

    public void setSrvcChrgDrCur(String srvcChrgDrCur) {
        this.srvcChrgDrCur = srvcChrgDrCur;
    }

    private String srvcChrgDrCur;

    public String getBillReference() {
        return billReference;
    }

    public void setBillReference(String billReference) {
        this.billReference = billReference;
    }

    private String billReference;

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    private String transferType;

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    private String postscript;


    //金额,
    private String drAmt;
    //账户脱敏后英文名称
    private String crDisplayedEngNm;
    //内部订单号
    private String scrRefNo;

    private String crChinNm;

    public String getCrModifyName() {
        return crModifyName;
    }

    public void setCrModifyName(String crModifyName) {
        this.crModifyName = crModifyName;
    }

    private String crModifyName;

    //付款类别 0：个人，1：商户
    private String paymentCategory;

    private String dbtrNm;
    private String transferBank;

    public String getDbtrNm() {
        return dbtrNm;
    }

    public void setDbtrNm(String dbtrNm) {
        this.dbtrNm = dbtrNm;
    }

    public String getTransferBank() {
        return transferBank;
    }

    public void setTransferBank(String transferBank) {
        this.transferBank = transferBank;
    }

    public String getCrMembId() {
        return crMembId;
    }

    public void setCrMembId(String crMembId) {
        this.crMembId = crMembId;
    }

    private String crMembId;

    public String getSmartAcLevel() {
        return smartAcLevel;
    }

    public void setSmartAcLevel(String smartAcLevel) {
        this.smartAcLevel = smartAcLevel;
    }

    /**
     * 我的账户级别 1：S1  2:S2  3:S3
     */
    private String smartAcLevel;


    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }


    public String getAcNo() {
        return acNo;
    }

    public void setAcNo(String acNo) {
        this.acNo = acNo;
    }

    private String acNo;

    public String getRealCusName() {
        return realCusName;
    }

    public void setRealCusName(String realCusName) {
        this.realCusName = realCusName;
    }

    private String realCusName;

    private TransferPreCheckReq preCheckReq;

    public TransferPreCheckReq getPreCheckReq() {
        return preCheckReq;
    }

    public void setPreCheckReq(TransferPreCheckReq preCheckReq) {
        this.preCheckReq = preCheckReq;
    }

    public String getDrAmt() {
        return drAmt;
    }

    public void setDrAmt(String drAmt) {
        this.drAmt = drAmt;
    }

    public String getCrDisplayedEngNm() {
        return crDisplayedEngNm;
    }

    public void setCrDisplayedEngNm(String crDisplayedEngNm) {
        this.crDisplayedEngNm = crDisplayedEngNm;
    }

    public String getScrRefNo() {
        return scrRefNo;
    }

    public void setScrRefNo(String scrRefNo) {
        this.scrRefNo = scrRefNo;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getCrChinNm() {
        return crChinNm;
    }

    public void setCrChinNm(String crChinNm) {
        this.crChinNm = crChinNm;
    }

    public TransferResourcesEntity.DefBean.BckImgsBean getBckImgsBean() {
        return bckImgsBean;
    }

    public void setBckImgsBean(TransferResourcesEntity.DefBean.BckImgsBean bckImgsBean) {
        this.bckImgsBean = bckImgsBean;
    }

    private TransferResourcesEntity.DefBean.BckImgsBean bckImgsBean;


    public String getNeedOtpVerify() {
        return needOtpVerify;
    }

    public void setNeedOtpVerify(String needOtpVerify) {
        this.needOtpVerify = needOtpVerify;
    }

    private String needOtpVerify;

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;

    /**
     * 转账需要发送otp
     *
     * @return
     */
    public boolean needSendOtp() {

        if (TextUtils.isEmpty(needOtpVerify)) {
            return false;
        }
        return TextUtils.equals(needOtpVerify, "1");

    }

    /**
     *  银行名称 银行转账需要
     */
    private String bankShowName;

    public String getBankShowName() {
        return bankShowName;
    }

    public void setBankShowName(String bankShowName) {
        this.bankShowName = bankShowName;
    }
}
