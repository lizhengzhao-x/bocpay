package cn.swiftpass.wallet.intl.base.fio;

import android.content.Intent;

import static android.app.Activity.RESULT_OK;

/**
 * @name HKBocBill
 * @class name：cn.swiftpass.bocbill.support.utils
 * @class FIO 结果分发器
 * @anthor zhangfan
 * @time 2019/7/2 18:31
 * @change
 * @chang time
 * @class describe
 */
public class FIODispatcher {

    public static void requestCallBack(FIOResultView baseView, int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case FioConst.REQUEST_CODE_CHECK_POLICY:
                if (resultCode == RESULT_OK) {
                    boolean bCheckPolicy = data.getBooleanExtra(FioConst.ACTION_CHECK_POLICY, false);
                    boolean hasFingerPrintEnrolled = data.getBooleanExtra(FioConst.HAS_FINGERPRINT_ENROLLED, false);
                    boolean hasFingerPrint = data.getBooleanExtra(FioConst.HAS_FINGERPRINT, false);
                    if (bCheckPolicy && hasFingerPrintEnrolled && hasFingerPrint) {
                        baseView.onFioSuccess(requestCode, resultCode, data);
                    } else {
                        baseView.onFioFail(requestCode, resultCode, data);
                    }
                }
                break;
            case FioConst.REQUEST_CODE_REG_FIO:
                if (resultCode == RESULT_OK) {
                    boolean bRegFio = data.getBooleanExtra(FioConst.ACTION_REG_FIO, false);
                    if (bRegFio) {
                        baseView.onFioSuccess(requestCode, resultCode, data);
                    } else {
                        baseView.onFioFail(requestCode, resultCode, data);
                    }
                } else {
                    baseView.onFioFail(requestCode, resultCode, data);
                }
                break;
            case FioConst.REQUEST_CODE_AUTH_FIO:
                if (resultCode == RESULT_OK) {
                    boolean bRegFio = data.getBooleanExtra(FioConst.ACTION_AUTH_FIO, false);
                    if (bRegFio) {
                        baseView.onFioSuccess(requestCode, resultCode, data);
                    } else {
                        baseView.onFioFail(requestCode, resultCode, data);
                    }
                } else {
                    baseView.onFioFail(requestCode, resultCode, data);
                }
                break;
            case FioConst.REQUEST_CODE_DE_REG_FIO:
                baseView.onFioSuccess(requestCode, resultCode, data);
                break;
            case FioConst.REQUEST_CODE_CHECK_VALID_FIO:
                if (resultCode == RESULT_OK) {
                    boolean bRegFio = data.getBooleanExtra(FioConst.ACTION_CHECK_VALID_FIO, false);
                    if (bRegFio) {
                        baseView.onFioSuccess(requestCode, resultCode, data);
                    } else {
                        baseView.onFioFail(requestCode, resultCode, data);
                    }
                } else {
                    baseView.onFioFail(requestCode, resultCode, data);
                }
                break;
            default:
                break;
        }

    }
}
