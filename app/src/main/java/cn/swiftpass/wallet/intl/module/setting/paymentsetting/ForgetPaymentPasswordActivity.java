package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.entity.event.ForgetPwdEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelectLoginCodeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static android.view.View.IMPORTANT_FOR_AUTOFILL_NO;


/**
 * 未登录态忘记密码 首先获取账户级别
 */
public class ForgetPaymentPasswordActivity extends BaseCompatActivity {


    @BindView(R.id.id_verifycode_view)
    LinearLayout idVerifycodeView;
    @BindView(R.id.tv_account_flag)
    TextView tvAccountFlag;

    private TextView mTv_region_code_str;
    private ImageView mIv_sel_region_arrow;
    private EditTextWithDel mEtwd_transfer_phone_num;
    private CustomTvEditText mCet_verify_code;
    private ImageView mIv_verify_code;
    private ImageView mIv_refresh_code;
    private TextView mTv_login;
    /**
     * 理论上只有成功离开这个界面才可以刷新
     */
    private boolean isCanRefreshCode = true;

    private void bindViews() {
        mTv_region_code_str = (TextView) findViewById(R.id.tv_region_code_str);
        mIv_sel_region_arrow = (ImageView) findViewById(R.id.iv_sel_region_arrow);
        mEtwd_transfer_phone_num = (EditTextWithDel) findViewById(R.id.etwd_transfer_phone_num);
        mCet_verify_code = (CustomTvEditText) findViewById(R.id.cet_verify_code);
        mIv_verify_code = (ImageView) findViewById(R.id.iv_verify_code);
        mIv_refresh_code = (ImageView) findViewById(R.id.iv_refresh_code);
        mTv_login = (TextView) findViewById(R.id.tv_login);
        mEtwd_transfer_phone_num.setLineVisible(false);
        mEtwd_transfer_phone_num.hideErrorView();
        idVerifycodeView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isCanRefreshCode) {
            getVerifyCode();
        }
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        bindViews();

        setToolBarTitle(R.string.setting_payment_fgw);

        mTv_region_code_str.setText(Constants.DATA_COUNTRY_CODES[1]);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mCet_verify_code.getEditText().setImportantForAutofill(IMPORTANT_FOR_AUTOFILL_NO);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }

        mTv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSmartLevel();
            }
        });
        mIv_refresh_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getVerifyCode();
            }
        });

        mIv_sel_region_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selAreaCode();
            }
        });

        mTv_region_code_str.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selAreaCode();
            }
        });

        mEtwd_transfer_phone_num.getEditText().addTextChangedListener(onTextChangedListener);
        mCet_verify_code.getEditText().addTextChangedListener(onTextChangedListener);
        mCet_verify_code.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        updateOkBackground(mTv_login, false);
        EventBus.getDefault().register(this);
    }

    private void selAreaCode() {
        isCanRefreshCode = false;
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.LAST_SELECT_COUNTRY, mTv_region_code_str.getText().toString());
        ActivitySkipUtil.startAnotherActivityForResult(ForgetPaymentPasswordActivity.this, SelectLoginCodeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, Constants.REQUEST_CODE_SEL_COUNTRY);
    }


    private TextWatcher onTextChangedListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            updateEditFlag();
        }
    };

    private void getVerifyCode() {
        mIv_refresh_code.setEnabled(false);
        ApiProtocolImplManager.getInstance().getLoginVerifyCode(this, "F", new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                mIv_refresh_code.setEnabled(true);
                AndroidUtils.showTipDialog(ForgetPaymentPasswordActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                mIv_refresh_code.setEnabled(true);
                int codeLen = 8;
                try {
                    codeLen = Integer.valueOf(response.getVerifyCodeLength());
                } catch (Exception e) {
                }
                try {
                    String bmpCode = response.getImageCode();
                    Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
                    mIv_verify_code.setImageBitmap(bmp);
                    mCet_verify_code.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
                    mCet_verify_code.setContentText("");
                } catch (Exception e) {
                }
                //dialog 隐藏之后 软键盘需要弹出
            }
        });
    }

    private void updateEditFlag() {
        //对输入的数字，每四位加一个空格
        //获取输入框中的内容,不可以去空格
        String etContent = mEtwd_transfer_phone_num.getEditText().getText().toString();
        if (TextUtils.isEmpty(etContent)) {
            updateOkBackground(mTv_login, false);
            return;
        }
        String countryCode = mTv_region_code_str.getText().toString().trim();
        if (!AndroidUtils.validatePhone(countryCode, etContent)) {
            tvAccountFlag.setText(getString(R.string.LG2101_940_1));
            updateOkBackground(mTv_login, false);
        } else {
            if (AndroidUtils.validatePhoneRegister(countryCode, etContent)) {
                int codeLength = mCet_verify_code.getInputText().length();
                if (codeLength == 4) {
                    updateOkBackground(mTv_login, true);
                } else {
                    updateOkBackground(mTv_login, false);
                }
            } else {
                updateOkBackground(mTv_login, false);
            }
            tvAccountFlag.setText("");
        }
    }

    /**
     * 我的账户级别
     */
    private void getSmartLevel() {
        if (ButtonUtils.isFastDoubleClick()) return;
        String phone = mEtwd_transfer_phone_num.getText();
        String verifyCode = mCet_verify_code.getInputText();
        String mCountryCode = mTv_region_code_str.getText().toString().trim();
        mCountryCode = mCountryCode.replace("+", "");
        final String finalPhone = (mCountryCode + "-" + phone).replace(" ", "");
        ApiProtocolImplManager.getInstance().getSmartLevelForForget(this, finalPhone, verifyCode, "F", new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(ForgetPaymentPasswordActivity.this, errorMsg);
                //重新获取验证码
                if (NetworkUtil.isNetworkAvailable()) {
                    getVerifyCode();
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                //登出态  忘记密码
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.SMART_LEVEL_INFO, response);
                mHashMaps.put(Constants.MOBILE_PHONE, finalPhone);
                ActivitySkipUtil.startAnotherActivity(getActivity(), STwoSelForgetPasswordTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                //重新获取验证码
                isCanRefreshCode = true;
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ForgetPwdEventEntity event) {
        if (event.getEventType() == ForgetPwdEventEntity.EVENT_FORGET_PWD_LOGIN
        ) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_forgetpassword;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data != null && data.getExtras() != null) {
                String countryCode = data.getExtras().getString(Constants.DATA_COUNTRY_CODE);
                if (countryCode != null && !countryCode.equals(mTv_region_code_str.getText().toString().trim())) {
                    mTv_region_code_str.setText(countryCode);
                    updateEditFlag();
                }
                AndroidUtils.showKeyboard(this, mEtwd_transfer_phone_num.getEditText());
            }
        }
    }


}
