package cn.swiftpass.wallet.intl.module.register.adapter.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class BindeNewCardContentHolder extends RecyclerView.ViewHolder {
    private final Context context;
    @BindView(R.id.iv_check_bind_card)
    ImageView ivCheckBindCard;
    @BindView(R.id.tv_card_type)
    TextView tvCardType;
    @BindView(R.id.tv_card_number)
    TextView tvCardNumber;
    @BindView(R.id.iv_bind_new_card)
    ImageView ivBindNewCard;

    /**
     * cardTypeAvailableColor     可选信用卡的卡类型字体颜色
     * cardNumberAvailableColor   可选信用卡的卡号字体颜色
     * cardTypeNuAvailableColor   不可选信用卡的卡类型字体颜色
     * cardNumberNuAvailableColor 不可选信用卡的卡号字体颜色
     */
    public int cardTypeAvailableColor;
    public int cardNumberAvailableColor;
    public int cardTypeNuAvailableColor;
    public int cardNumberNuAvailableColor;


    public BindeNewCardContentHolder(Context context, View view) {
        super(view);
        this.context = context;
        cardTypeAvailableColor = context.getColor(R.color.color_FF232323);
        cardNumberAvailableColor = context.getColor(R.color.color_FF7E7E7E);
        cardTypeNuAvailableColor = context.getColor(R.color.font_gray_middle);
        cardNumberNuAvailableColor = context.getColor(R.color.font_gray_middle);
        ButterKnife.bind(this, view);
    }

    public void setData(BindNewCardEntity bindNewCardEntity, OnBindeNewCardListener listener) {
        if (bindNewCardEntity != null) {
            tvCardNumber.setText("************" + bindNewCardEntity.getPanShowNum());
            tvCardType.setText("");
            tvCardType.setText(bindNewCardEntity.getPanName());
            updateIvCheckBindCard(bindNewCardEntity);
            ivCheckBindCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                    bindNewCardEntity.toggle();
                    updateIvCheckBindCard(bindNewCardEntity);
                    listener.OnChangeCheck();
                }
            });

            int cardWidth = AndroidUtils.dip2px(context, 80);
            int cardHeight = (int) (cardWidth * AndroidUtils.cardPresent);

            GlideApp.with(context)
                    .load(AndroidUtils.getCardFaceUrl(bindNewCardEntity.getCardType()) + bindNewCardEntity.getCardBin() + ".png")
                    .placeholder(R.mipmap.img_card_default)
                    .override(cardWidth, cardHeight)
                    .error(R.mipmap.img_card_default)
                    .into(ivBindNewCard);
        }
    }

    private void updateIvCheckBindCard(BindNewCardEntity bindNewCardEntity) {
        //默认可以选中
        ivCheckBindCard.setEnabled(true);
        ivCheckBindCard.setImageResource(R.mipmap.list_icon_choose_rectangle_default);
        tvCardType.setTextColor(cardTypeAvailableColor);
        tvCardNumber.setTextColor(cardNumberAvailableColor);
        if (bindNewCardEntity != null) {
            if (bindNewCardEntity.isCanSelect()) {
                if (bindNewCardEntity.isCheck()) {
                    ivCheckBindCard.setImageResource(R.mipmap.list_icon_other_ticked_filled);
                }
            } else {
                ivCheckBindCard.setEnabled(false);
                ivCheckBindCard.setImageResource(R.mipmap.list_icon_choose_rectangle_unavailable);
                tvCardType.setTextColor(cardTypeNuAvailableColor);
                tvCardNumber.setTextColor(cardNumberNuAvailableColor);
            }
        }
    }
}
