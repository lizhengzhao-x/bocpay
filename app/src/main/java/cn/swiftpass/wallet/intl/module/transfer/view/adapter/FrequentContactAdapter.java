package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

/**
 * 常用联系人adapter
 */
public class FrequentContactAdapter extends BaseRecyclerAdapter<ContactEntity> {

    public FrequentContactAdapter(@Nullable List<ContactEntity> data) {
        super(R.layout.item_contact_view, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, ContactEntity contactEntity, int position) {
        if (TextUtils.isEmpty(contactEntity.getUserName())) {
            holder.setGone(R.id.id_contact_user_name, true);
        } else {
            holder.setGone(R.id.id_contact_user_name, false);
            holder.setText(R.id.id_contact_user_name, contactEntity.getUserName());
        }
        holder.setText(R.id.id_contact_user_number, contactEntity.getNumber());
        holder.setImageResource(R.id.id_contact_collect_imag, contactEntity.isCollect() ? R.mipmap.icon_contactlist_favorite : R.mipmap.icon_contactlist_favorite_empty);
        holder.setImageResource(R.id.id_contact_head_img, contactEntity.isEmail() ? R.mipmap.icon_transfer_profile_email : R.mipmap.icon_transfer_profile_mobile);

        holder.addOnClickListener(R.id.ll_contact_collect_imag);
    }
}
