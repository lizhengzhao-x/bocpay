package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class PreLoginBannerEntity extends BaseEntity {
    String imageUrl;
    String actionUrl;
    String title;
    String shareImageUrl;
    /**
     * 未登录preLogin 消息类型 点击跳转类型
     */
    String newsType;
    boolean isInitData;


    public boolean isInitData() {
        return isInitData;
    }

    public void setInitData(boolean initData) {
        isInitData = initData;
    }

    public int getLocalDefaultImageUrl() {
        return localDefaultImageUrl;
    }

    /**
     * 是否是视频类型
     *
     * @return
     */
    public boolean isVideoView() {
        if (TextUtils.isEmpty(newsType)) return false;
        return TextUtils.equals(newsType, "VIDEO");
    }

    /**
     * 是否是虚拟卡申请类型
     *
     * @return
     */
    public boolean isVirtualCardApplyView() {
        if (TextUtils.isEmpty(newsType)) return false;
        return TextUtils.equals(newsType, "VIRTUAL_CARD");
    }


    /**
     * 本地默认的资源文件
     */
    private int localDefaultImageUrl = -1;


    public PreLoginBannerEntity(int resId, boolean isInitData) {
        this.localDefaultImageUrl = resId;
        this.isInitData = isInitData;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getShareImageUrl() {
        return shareImageUrl;
    }

    public void setShareImageUrl(String shareImageUrl) {
        this.shareImageUrl = shareImageUrl;
    }
}
