package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;


public class TransferCrossBorderSetLimitContract {

    public interface View extends IView {

        void setTransferLimitSuccess(BaseEntity response);

        void setTransferLimitError(String errorCode, String errorMsg);

        void getTransferBaseDataSuccess(TransferCrossBorderBaseEntity entity);

        void getTransferBaseDataFail(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {
        void setTransferLimit(int mCurrentLimit);

        void getTransferBaseData();
    }

}
