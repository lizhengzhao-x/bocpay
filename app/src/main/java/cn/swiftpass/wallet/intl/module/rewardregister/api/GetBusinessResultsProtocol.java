package cn.swiftpass.wallet.intl.module.rewardregister.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.BusinessResultsEntity;

/**
 * @author lizheng.zhao
 */
public class GetBusinessResultsProtocol extends BaseProtocol {

    private String businessType;
    private String businessParam;


    public GetBusinessResultsProtocol(String businessType, String businessParam, NetWorkCallbackListener<BusinessResultsEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/preLogin/getBusinessResults";
        this.businessType = businessType;
        this.businessParam = businessParam;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.BUSINESSTYPE, businessType);
        mBodyParams.put(RequestParams.BUSINESSPARAM, businessParam);
    }
}
