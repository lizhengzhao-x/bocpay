package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 获取被扫积分信息接口
 */

public class GetSweptCodePointsProtocol extends BaseProtocol {

    private String tnxId;

    public static final String REDEEMTYPE_BACK_SCAN = "S";
    public static final String REDEEMTYPE_TRANSACTION = "O";
    /**
     * S、被扫积分抵消签账 O、交易记录积分抵消签账
     */
    private String redeemType;

    public GetSweptCodePointsProtocol(String tnxIdIn, String redeemTypeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/points/getSweptCodePoints";
        this.redeemType = redeemTypeIn;
        this.tnxId = tnxIdIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, tnxId);
        mBodyParams.put(RequestParams.REDEEMTYPE, redeemType);
    }

}
