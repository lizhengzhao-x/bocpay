package cn.swiftpass.wallet.intl.module.virtualcard.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/11/22.
 */
public class VirtualCardForgetPwdEntity extends BaseEntity {

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    private String account;

    private List<VirtualCardListEntity.VirtualCardListBean> virtualCardList;

    public List<VirtualCardListEntity.VirtualCardListBean> getVirtualCardList() {
        return virtualCardList;
    }

    public void setVirtualCardList(List<VirtualCardListEntity.VirtualCardListBean> virtualCardList) {
        this.virtualCardList = virtualCardList;
    }
}