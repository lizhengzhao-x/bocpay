package cn.swiftpass.wallet.intl.sdk.verification;

public interface OnPwdVerifyCallBack {
    void onVerifySuccess();

    void onVerifyFailed(String errorCode, String errorMsg);

    void onVerifyCanceled();
}
