package cn.swiftpass.wallet.intl.module.cardmanagement.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.entity.GradeEntity;
import cn.swiftpass.wallet.intl.entity.event.UpdateTotalGradeEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.TotalCreditCardGradeContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.TotalGradePresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.CreditCardGradeDetailAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.AnimatedExpandableListView;

/**
 * 总积分展示
 */
public class TotalGradeActivity extends BaseCompatActivity<TotalCreditCardGradeContract.Presenter> implements TotalCreditCardGradeContract.View {

    @BindView(R.id.id_card_grade_sub_title)
    TextView idCardGradeSubTitle;

    @BindView(R.id.id_message_info)
    TextView id_message_info;


    @BindView(R.id.id_recyclerview)
    AnimatedExpandableListView idRecyclerview;


    private CreditCardGradeDetailAdapter creditCardGradeDetailAdapter;

    // 涉及到隐藏资料 添加一个10分钟计时器  10分钟后自动关闭该页面


    @Override
    public void init() {
        setToolBarTitle(R.string.smart_account);
        EventBus.getDefault().register(this);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 10 * 60 * 1000);
        creditCardGradeDetailAdapter = new CreditCardGradeDetailAdapter(this);
        idRecyclerview.setAdapter(creditCardGradeDetailAdapter);
        mPresenter.getTotalGradeInfo();
    }

    protected IPresenter loadPresenter() {
        return new TotalGradePresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_totalgrade;
    }

    @Override
    protected TotalCreditCardGradeContract.Presenter createPresenter() {
        return new TotalGradePresenter();
    }

    @Override
    public void getTotalGradeInfoSuccess(CreditCardGradeEntity response) {

        idCardGradeSubTitle.setText(AndroidUtils.formatPrice(Double.valueOf(response.getSumGp()), false));
        creditCardGradeDetailAdapter.setData(changeData(response));
        creditCardGradeDetailAdapter.notifyDataSetChanged();

        id_message_info.setVisibility(View.VISIBLE);

    }

    private List<GradeEntity> changeData(CreditCardGradeEntity response) {
        List<GradeEntity> list = new ArrayList<>();
        if (response != null) {
            if (!TextUtils.isEmpty(response.getSmaGp()) && response.getSmaGpList() != null && response.getSmaGpList().size() > 0) {
                list.add(new GradeEntity(response.getSmaGp(), response.getSmaName(), response.getSmaGpList()));
                id_message_info.setText(getString(R.string.LYP04_04_3));
            }
            if (!TextUtils.isEmpty(response.getCreditCardGp()) && response.getCcGpInfo() != null && response.getCcGpInfo().size() > 0) {
                list.add(new GradeEntity(response.getCreditCardGp(), getString(R.string.LYP04_02_2), response.getCcGpInfo()));
                id_message_info.setText(getString(R.string.LYP04_04_2));
            }
        }
        return list;
    }

    @Override
    public void getTotalGradeInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UpdateTotalGradeEntity event) {
        if (event.getEventType() == UpdateTotalGradeEntity.UPDATE_TOTAL_GRADE) {
            mPresenter.getTotalGradeInfo();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
