package cn.swiftpass.wallet.intl.module.setting.help;

import android.content.Context;
import android.view.View;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PromotionEntity;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

/**
 * @author Created by  on 2018/9/12.
 */

public class HelpListAdapter implements AdapterItem<PromotionEntity.PromotionBean> {
    private Context context;

    public HelpListAdapter(Context context, MessageItemCallback messageItemCallback) {
        this.context = context;
        this.mCallback = messageItemCallback;
    }

    ImageArrowView mItemView;
    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_help_view;
    }

    @Override
    public void bindViews(View root) {
        mItemView = root.findViewById(R.id.iav_view);
    }

    private MessageItemCallback mCallback;

    @Override
    public void setViews() {
        mItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onMessageItemCallback(mPosition);
                }
            }
        });
    }

    @Override
    public void handleData(PromotionEntity.PromotionBean homeMessageEntity, int position) {
        mPosition = position;
        mItemView.setLeftTextTitle(homeMessageEntity.getTitle());
    }

    public static class MessageItemCallback {
        public void onMessageItemCallback(int position) {

        }

    }
}
