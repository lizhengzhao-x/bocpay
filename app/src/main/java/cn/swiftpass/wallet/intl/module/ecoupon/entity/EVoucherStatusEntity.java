package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/11.
 */
public class EVoucherStatusEntity extends BaseEntity {


    private List<RowsBean> rows;

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean extends BaseEntity {
        /**
         * referenceNo : 9000105179
         * status : 1
         */

        private String referenceNo;
        private String status;

        public String getReferenceNo() {
            return referenceNo;
        }

        public void setReferenceNo(String referenceNo) {
            this.referenceNo = referenceNo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
