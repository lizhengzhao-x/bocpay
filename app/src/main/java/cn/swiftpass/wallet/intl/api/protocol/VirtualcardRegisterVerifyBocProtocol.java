package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualcardRegisterVerifyBocProtocol extends BaseProtocol {
    VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity;

    public VirtualcardRegisterVerifyBocProtocol(VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntityIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        virtualcardRegisterVerifyEntity = virtualcardRegisterVerifyEntityIn;
        mUrl = "api/virtual/isBocCus";
    }

    @Override
    public void packData() {
        super.packData();
//        phoneNo	String	M	手机号
//        nid	String	M	身份证号
//        nidType	String	M
//        nidRgn	String	M
//        action	String	M	Regist:‘VIRREG ‘  forgetpwd:‘VIRPWDSET
        mBodyParams.put(RequestParams.PHONENO, virtualcardRegisterVerifyEntity.getPhoneNo());
        mBodyParams.put(RequestParams.NID, virtualcardRegisterVerifyEntity.getNid());
        mBodyParams.put(RequestParams.NIDTYPE, virtualcardRegisterVerifyEntity.getNidType());
        mBodyParams.put(RequestParams.NIDRGN, virtualcardRegisterVerifyEntity.getNidRgn());
        mBodyParams.put(RequestParams.ACTION, virtualcardRegisterVerifyEntity.getAction());
    }


}
