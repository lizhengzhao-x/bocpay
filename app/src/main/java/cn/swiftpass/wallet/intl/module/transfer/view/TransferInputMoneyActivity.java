package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountManageActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSmartAccountInfoContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferSmartAccountInfoPresenter;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

/**
 * Created by aijingya on 2018/7/26.
 *
 * @Package cn.swiftpass.wallet.intl.activity
 * @Description: 转账输入金额页面
 * @date 2018/7/26.17:12.
 */

public class TransferInputMoneyActivity extends BaseCompatActivity<TransferSmartAccountInfoContract.Presenter> implements TransferSmartAccountInfoContract.View {

    private static final String ACTION_SET_DAILY_LIMIT_BALANCE = "ACTION_SET_DAILY_LIMIT_BALANCE";
    private static final String ACTION_GET_SMART_INFO = "ACTION_GET_SMART_INFO";


    @BindView(R.id.et_input_amount)
    EditTextWithDel etInputAmount;
    @BindView(R.id.transfer_next)
    TextView transferNext;
    @BindView(R.id.tv_username)
    TextView tvUsername;
    @BindView(R.id.tv_user_account)
    TextView tvUserAccount;
    private ParserQrcodeEntity mFpsEntity;
    private MySmartAccountEntity mySmartAccountEntity;
    @BindView(R.id.id_transfer_remark)
    EditTextWithDel mId_transfer_remark;
    @BindView(R.id.id_remark_size)
    TextView mId_remark_size;

    /**
     * 账户资料区
     */
    @BindView(R.id.iv_toggle_see)
    ImageView ivToggleSee;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.ll_home_top_up_account)
    LinearLayout llHomeTopUpAccount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;

    @BindView(R.id.id_available_amount)
    TextView idAvailableAmount;

    private boolean isEyeOpen = false;


    private int currentPaymentType;

    @Override
    public void init() {
        //setToolBarTitle(R.string.transfer);
        EventBus.getDefault().register(this);
        if (null != getIntent()) {
            mFpsEntity = (ParserQrcodeEntity) getIntent().getSerializableExtra(TransferConst.TRANS_FPS_DATA);
        } else {
            finish();
        }
        if (null == mFpsEntity) {
            finish();
        }

        if (TextUtils.equals(mFpsEntity.getPaymentCategory(), TransferConst.TRANS_TYPE_PERSON)) {
            setToolBarTitle(R.string.TF2101_5_1);
        } else {
            setToolBarTitle(R.string.FPSBP2101_1_1);
        }
        currentPaymentType = getIntent().getExtras().getInt(Constants.TYPE);

        tvUsername.setCompoundDrawables(null, null, null, null);
        etInputAmount.getEditText().setTextSize(17);
        etInputAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                dealWithExceedMaxMoney(etInputAmount.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        //默认按钮为false
        updateOkBackground(transferNext, false);
        //防止软键盘换车换行
        mId_transfer_remark.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        AndroidUtils.setLimit(mId_transfer_remark.getEditText(), mId_remark_size);
        //两位小数过滤 8位长度限制
        etInputAmount.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(10)});
        etInputAmount.hideErrorView();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //每次到达这个界面 都需要更新一下限额，防止更新限额显示
        mPresenter.getSmartAccountInfo(ACTION_GET_SMART_INFO, true, true);
    }

    private void dealWithExceedMaxMoney(String s) {
        if (TextUtils.isEmpty(s)) {
            idAvailableAmount.setVisibility(View.INVISIBLE);
            updateOkBackground(transferNext, false);
            return;
        }
        double currentV = 0;
        try {
            currentV = Double.valueOf(s);
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (currentV == 0) {
            updateOkBackground(transferNext, false);
            return;
        }
        double maxValue = 0;
        if (mySmartAccountEntity != null && !TextUtils.isEmpty(mySmartAccountEntity.getOutavailbal())) {
            try {
                maxValue = Double.valueOf(mySmartAccountEntity.getOutavailbal());
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        } else {
            updateOkBackground(transferNext, false);
            return;
        }
        if (currentV <= maxValue) {
            idAvailableAmount.setVisibility(View.INVISIBLE);
            if (mySmartAccountEntity != null) {
                String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mySmartAccountEntity.getOutavailbal()), true);
                idAvailableAmount.setText(getString(R.string.RP1_4_5) + " " + mySmartAccountEntity.getCurrency() + " " + balanceStr);
            }
            idAvailableAmount.setTextColor(getResources().getColor(R.color.font_gray_four));
        } else {
            idAvailableAmount.setVisibility(View.VISIBLE);
            idAvailableAmount.setText(getResources().getString(R.string.TF2101_4_11));
            idAvailableAmount.setTextColor(ContextCompat.getColor(this, R.color.color_FFBF2F4F));
        }
        updateOkBackground(transferNext, currentV <= maxValue);
    }

    @Override
    protected void onStart() {
        super.onStart();
        hiddenAccountData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS) {
            finish();
        }
    }

    private void initView() {
        String dslpName = mFpsEntity.getDsplName();
        String merAccount = mFpsEntity.getTransferAccNo();
        if (!TextUtils.isEmpty(dslpName)) {
            tvUsername.setText(dslpName);
        }
        if (!TextUtils.isEmpty(merAccount)) {
            tvUserAccount.setText(merAccount);
        }
        hiddenAccountData();

        //初始化金额输入框的内容
        if (!TextUtils.isEmpty(mFpsEntity.getTransferAmount())) {
            etInputAmount.setContentText(BigDecimalFormatUtils.forMatWithDigs(mFpsEntity.getTransferAmount(), 2));
        }
    }

    /**
     * 更新扫码信息
     */
    private void updateAmountInfo() {
        //        mFpsEntity.setEditableTraAmountFalg("1");
        //FPS扫码，如果有金额，判断editableTraAmountFalg是否为1，为1表示金额可重新编辑；为其他值不可编辑
        //如果没有金额，不需要判断editableTraAmountFalg，都需要输入金额
        if (!TextUtils.isEmpty(mFpsEntity.getTransferAmount())) {

            String inputMoney = etInputAmount.getText().toString().trim();
            String qrMoney = mFpsEntity.getTransferAmount();

            etInputAmount.setSelectionLast();
            if (mFpsEntity.getEditableTraAmountFalg()) {
                //可重新编辑
                if (TextUtils.isEmpty(inputMoney)) {
                    etInputAmount.setContentText(BigDecimalFormatUtils.forMatWithDigs("0", 2));
                } else {
                    etInputAmount.setContentText(BigDecimalFormatUtils.forMatWithDigs(inputMoney, 2));
                }
            } else {
                etInputAmount.setContentText(BigDecimalFormatUtils.forMatWithDigs(qrMoney, 2));
                etInputAmount.setLineVisible(false);
                etInputAmount.getEditText().setFocusable(false);
                etInputAmount.setEditTextEnable(false);
                etInputAmount.hideDelView();
                etInputAmount.hideErrorView();
                //如果有金额 edittext 不允许 长按（会出现粘贴复制等）
                etInputAmount.getEditText().setLongClickable(false);
                etInputAmount.getEditText().setSelected(false);
                etInputAmount.getEditText().setEnabled(false);

            }
        } else {
            AndroidUtils.showKeyboard(this, etInputAmount.getEditText());
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.input_transfer_amount_activity;
    }

    @Override
    protected TransferSmartAccountInfoContract.Presenter createPresenter() {
        return new TransferSmartAccountInfoPresenter();
    }


    @OnClick({R.id.transfer_next, R.id.iv_toggle_see, R.id.ll_home_top_up_account, R.id.iv_set_daily_limit_balance})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.transfer_next:
                if (ButtonUtils.isFastDoubleClick()) return;
                final TransferPreCheckReq req = new TransferPreCheckReq();
                req.transferType = mFpsEntity.getTransferAccTp();
                req.bankType = TransferConst.TRANS_BANK_TYPE_FPS;
                req.tansferToAccount = mFpsEntity.getTransferAccNo();
                req.bankName = mFpsEntity.getTransferBankCode();
                req.postscript = mId_transfer_remark.getText().toString().trim();
                req.transferAmount = etInputAmount.getText();
                mFpsEntity.getPaymentCategory();
                req.isQrcode = true;
                ApiProtocolImplManager.getInstance().checkTransferPre(this, true, req, new NetWorkCallbackListener<TransferPreCheckEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        AndroidUtils.showTipDialog(TransferInputMoneyActivity.this, errorMsg);
                    }

                    @Override
                    public void onSuccess(TransferPreCheckEntity response) {
                        response.setPreCheckReq(req);
                        response.setTransferType(TextUtils.isEmpty(req.transferType) ? "1" : req.transferType);
                        TransferConfirmMoneyActivity.startActivity(TransferInputMoneyActivity.this, response);
                    }
                });
                break;
            case R.id.iv_toggle_see:
                toggleSee();
                break;
            case R.id.ll_home_top_up_account:
                topUpAccount();
                break;
            case R.id.iv_set_daily_limit_balance:
                mPresenter.getSmartAccountInfo(ACTION_SET_DAILY_LIMIT_BALANCE, true, false);
                break;
        }
    }

    /**
     * 账户余额眼睛显示隐藏
     */
    private void toggleSee() {
        if (isEyeOpen) {
            hiddenAccountData();
        } else {
            if (SystemInitManager.getInstance().isValidShowAccount()) {
                isEyeOpen = true;
                ivToggleSee.setImageResource(R.mipmap.icon_eye_open_purple);
                ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
                MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                if (mySmartAccountEntity != null) {
                    showAccountData(mySmartAccountEntity);
                } else {
                    mPresenter.getSmartAccountInfo(ACTION_GET_SMART_INFO, true, false);
                }
            } else {
                VerifyPasswordCommonActivity.startActivityForResult(this, new OnPwdVerifyCallBack() {
                    @Override
                    public void onVerifySuccess() {
                        SystemInitManager.getInstance().setValidShowAccount(true);
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                        isEyeOpen = true;
                        ivToggleSee.setImageResource(R.mipmap.icon_eye_open_purple);
                        ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
                        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                        if (mySmartAccountEntity != null) {
                            showAccountData(mySmartAccountEntity);
                        } else {
                            mPresenter.getSmartAccountInfo(ACTION_GET_SMART_INFO, true, false);
                        }
                    }

                    @Override
                    public void onVerifyFailed(String errorCode, String errorMsg) {
                    }

                    @Override
                    public void onVerifyCanceled() {

                    }
                });
            }
        }
    }

    /**
     * 显示用户资料
     *
     * @param mySmartAccountEntity
     */
    private void showAccountData(MySmartAccountEntity mySmartAccountEntity) {
        if (mySmartAccountEntity != null) {
            double price = -1;
            try {
                price = Double.parseDouble(mySmartAccountEntity.getBalance());
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            //账户冻结时，返回 -1 需要展示 *****
            if (price < 0) {
                tvAmount.setText("*****");
            } else {
                String newPrice = AndroidUtils.formatPriceWithPoint(price, true);
                tvAmount.setText(newPrice);
            }

            if (!TextUtils.isEmpty(mySmartAccountEntity.getOutavailbal())) {
                double dailyLimitBalance = -1;
                try {
                    dailyLimitBalance = Double.parseDouble(mySmartAccountEntity.getOutavailbal());
                } catch (Exception e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }
                }
                String dailyLimitBalanceStr = AndroidUtils.formatPriceWithPoint(dailyLimitBalance, true);
                //账户冻结时，返回 -1 需要展示 *****
                if (dailyLimitBalance < 0) {
                    tvAmountUsable.setText("*****");
                } else {
                    tvAmountUsable.setText(dailyLimitBalanceStr);
                }
            } else {
                tvAmountUsable.setText("*****");
            }
        }
    }

    /**
     * 隐藏账户资料
     */
    private void hiddenAccountData() {
        isEyeOpen = false;
        ivToggleSee.setImageResource(R.mipmap.icon_eye_close_purple);
        tvAmount.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.INVISIBLE);
        tvAmountUsable.setText("*****");
    }

    /**
     * 增值
     */
    private void topUpAccount() {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null && MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
            TransferUtils.checkTopUpEvent(mySmartAccountEntity, new TransferUtils.RegEddaEventListener() {
                @Override
                public void getRegEddaInfo() {
                    mPresenter.getRegEddaInfo();
                }

                @Override
                public Activity getContext() {
                    return getActivity();
                }

                @Override
                public void showErrorDialog(String errorCode, String message) {
                    showErrorMsgDialog(getContext(), message);
                }
            });
        } else {
            showActiveAccount();
        }
    }

    public void showActiveAccount() {
        AndroidUtils.showTipDialog(getActivity(), getString(R.string.AC2101_19_11), getString(R.string.AC2101_19_8),
                getString(R.string.AC2101_19_10), getString(R.string.AC2101_19_9), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            hiddenAccountData();
        }
    }


    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response, String action, boolean isOnResume) {
        if (response != null) {
            if (ACTION_SET_DAILY_LIMIT_BALANCE.equals(action)) {
                SmartAccountAdjustDailyLimitActivity.startActivity(getActivity(), response, response.getHideMobile());
            } else if (ACTION_GET_SMART_INFO.equals(action)) {
                if (isOnResume) {
                    mySmartAccountEntity = response;
                    updateAmountInfo();
                    dealWithExceedMaxMoney(etInputAmount.getEditText().getText().toString());
                } else {
                    showAccountData(response);
                }
            }
        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
        updateAmountInfo();
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(getActivity(), errorMsg);
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mySmartAccountEntity.getHideMobile());
            mHashMapsLogin.put(Constants.SMART_ACCOUNT, mySmartAccountEntity);
            mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
            ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        }
    }
}
