package cn.swiftpass.wallet.intl.entity;


public class RegisterInviteInfo {


    /**
     * rewardRule : 恭喜您成功注册BoC Pay，现在输入邀请码，您的亲朋好友亦可得到<money>$10元奖赏</money>
     * pageTitle : 输入推荐码赚奖赏
     */

    private String rewardRule;
    private String pageTitle;

    public String getActivityTncUrl() {
        return activityTncUrl;
    }

    public void setActivityTncUrl(String activityTncUrl) {
        this.activityTncUrl = activityTncUrl;
    }

    private String activityTncUrl;


    public String getRewardRule() {
        return rewardRule;
    }

    public void setRewardRule(String rewardRule) {
        this.rewardRule = rewardRule;
    }

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }
}
