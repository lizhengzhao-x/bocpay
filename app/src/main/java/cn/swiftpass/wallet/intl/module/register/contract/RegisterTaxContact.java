package cn.swiftpass.wallet.intl.module.register.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.entity.RegisterTaxResultEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoParamEntity;
import cn.swiftpass.wallet.intl.module.register.SkipPageFromTaxFunction;

public class RegisterTaxContact {
    public interface View extends IView {

        void onCheckTaxFail(String errorCode, String errorMsg);

        void onCheckTaxSuccess(RegisterIDEntity response, RegisterCustomerInfo registerCustomerInfo);

        void getTaxResidentRelationInfoFail(String errorCode, String errorMsg);

        void getTaxResidentRelationInfoSuccess(RegisterTaxResultEntity response);

        void checkTaxInfoListProtocolFail(String errorCode, String errorMsg);

        void checkTaxInfoListProtocolSuccess(ContentEntity response, ArrayList<TaxInfoParamEntity> data);
    }

    public interface Presenter extends IPresenter<View> {


        void checkIdvRequest(String type, RegisterCustomerInfo registerCustomerInfo);

        void getTaxResidentRelationInfo(String type, SkipPageFromTaxFunction skipPageFromTaxFunction);

        void checkTaxList(ArrayList<TaxInfoParamEntity> data, String ifHKPeople, String ifOtherPeople, String type);
    }
}
