package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;


public class EcouponEditCntDialog extends Dialog {

    private int mAmount;
    private int mMaxAmount;
    private Context context;
    private EditText amountET;
    private int amount;
    private int maxAmount;

    private OnValueChangedListener onValueChangedListener;

    public int getmAmount() {
        return mAmount;
    }

    public void setmAmount(int mAmount) {
        this.mAmount = mAmount;
    }

    public EcouponEditCntDialog(Context context) {
        super(context);
    }

    public EcouponEditCntDialog(Context context, int theme) {
        super(context, theme);
    }

    public EcouponEditCntDialog(Activity contextIn, int amountIn, int maxAmountIn, OnValueChangedListener onValueChangedListenerIn) {
        super(contextIn, R.style.Dialog);
        context = contextIn;
        amount = amountIn;
        maxAmount = maxAmountIn;
        onValueChangedListener = onValueChangedListenerIn;
        init();
        setCancelable(true);
    }

    public void init() {
        View rootView = View.inflate(context, R.layout.dialog_set_cnt_layout, null);
        setContentView(rootView);
        final Button posBtn = rootView.findViewById(R.id.positive_dl);
        Button negBtn = rootView.findViewById(R.id.negative_dl);
        amountET = rootView.findViewById(R.id.et_amount);
        posBtn.setTextColor(context.getColor(R.color.color_green));
        if (amount != -1) {
            amountET.setText(String.valueOf(amount));
            amountET.setSelection(String.valueOf(amount).length());
        }
        negBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        amountET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                String currentValueStr = amountET.getText().toString();
                if (!TextUtils.isEmpty(currentValueStr)) {
                    int currentValue = 0;
                    try {
                        currentValue = Integer.valueOf(currentValueStr).intValue();
                    } catch (NumberFormatException e) {
                        if (BuildConfig.DEBUG) {
                            e.printStackTrace();
                        }
                        currentValue = maxAmount + 1;
                    }
                    if (currentValue <= maxAmount) {
                        posBtn.setEnabled(true);
                        posBtn.setTextColor(context.getColor(R.color.color_green));
                    } else {
                        amountET.setText(String.valueOf(maxAmount));
                        amountET.setSelection(String.valueOf(maxAmount).length());
                    }
                }
            }
        });
        posBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(amountET.getText().toString())) {
                    onValueChangedListener.onValueChanged(Integer.valueOf(amountET.getText().toString()));
                    dismiss();
                } else {

                    onValueChangedListener.onValueChanged(0);
                    dismiss();
                }
            }
        });
    }

    public interface OnValueChangedListener {
        void onValueChanged(int value);
    }

    public void showKeyboard() {
        if (amountET != null) {
            //设置可获得焦点
            amountET.setFocusable(true);
            amountET.setFocusableInTouchMode(true);
            //请求获得焦点
            amountET.requestFocus();
            //调用系统输入法
            InputMethodManager inputManager = (InputMethodManager) amountET.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.showSoftInput(amountET, 0);
        }
    }
}

//
//    public static class Builder {
//        private Context context;
//        private EditText amountET;
//        private OnClickListener positiveButtonClickListener;
//
//        public Builder(Context context) {
//            this.context = context;
//        }
//
//        public Builder setPositiveListener(OnClickListener listener) {
//            this.positiveButtonClickListener = listener;
//            return this;
//        }
//
//        public EcouponEditCntDialog create(final int amount, final int maxAmount) {
//            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            final EcouponEditCntDialog dialog = new EcouponEditCntDialog(context, R.style.Dialog);
//            View layout = inflater.inflate(R.layout.dialog_set_cnt_layout, null);
//            final Button posBtn = layout.findViewById(R.id.positive_dl);
//            Button negBtn = layout.findViewById(R.id.negative_dl);
//            amountET = layout.findViewById(R.id.et_amount);
//            posBtn.setTextColor(context.getColor(R.color.color_green));
//            if (amount != -1) {
//                amountET.setText(String.valueOf(amount));
//                amountET.setSelection(String.valueOf(amount).length());
//            }
//            negBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    dialog.dismiss();
//
//                }
//            });
//            amountET.addTextChangedListener(new TextWatcher() {
//                @Override
//                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                }
//
//                @Override
//                public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                }
//
//                @Override
//                public void afterTextChanged(Editable arg0) {
//                    String currentValueStr = amountET.getText().toString();
//                    if (!TextUtils.isEmpty(currentValueStr)) {
//                        int currentValue = 0;
//                        try {
//                            currentValue = Integer.valueOf(currentValueStr).intValue();
//                        } catch (NumberFormatException e) {
//                            if (BuildConfig.DEBUG) {
//                                e.printStackTrace();
//                            }
//                            currentValue = maxAmount + 1;
//                        }
//                        if (currentValue <= maxAmount) {
//                            posBtn.setEnabled(true);
//                            posBtn.setTextColor(context.getColor(R.color.color_green));
//                        } else {
//                            amountET.setText(String.valueOf(maxAmount));
//                            amountET.setSelection(String.valueOf(maxAmount).length());
//                        }
//                    }
//                }
//            });
//            posBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (!TextUtils.isEmpty(amountET.getText().toString())) {
//                        dialog.setmAmount(Integer.valueOf(amountET.getText().toString()));
//                        if (positiveButtonClickListener != null) {
//                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
//                        }
//                        dialog.dismiss();
//                    } else {
//                        dialog.setmAmount(0);
//                        if (positiveButtonClickListener != null) {
//                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
//                        }
//                        dialog.dismiss();
//                    }
//                }
//            });
//            dialog.setContentView(layout);
//            return dialog;
//        }
//
//        public void showKeyboard() {
//            if (amountET != null) {
//                //设置可获得焦点
//                amountET.setFocusable(true);
//                amountET.setFocusableInTouchMode(true);
//                //请求获得焦点
//                amountET.requestFocus();
//                //调用系统输入法
//                InputMethodManager inputManager = (InputMethodManager) amountET.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputManager.showSoftInput(amountET, 0);
//            }
//        }
//    }



