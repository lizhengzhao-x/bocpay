package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetVitualCarditListProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.VirtualCardConfirmContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;

/**
 * 主界面管理
 */
public class VirtualCardConfirmPresenter extends BasePresenter<VirtualCardConfirmContract.View> implements VirtualCardConfirmContract.Presenter {
    @Override
    public void getVirtualCardList() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetVitualCarditListProtocol(new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getVirtualCardListError(errorCode, errorMsg);
                    getView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                if (getView() != null) {
                    getView().getVirtualCardListSuccess(response);
                    //屏幕跳转之间 按钮点击问题 后dismissDialog
                    getView().dismissDialog();
                }
            }
        }).execute();
    }
}
