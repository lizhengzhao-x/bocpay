package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 11:08
 * 账单查询返回对象
 */

public class MainScanCodeInfoEntity extends BaseEntity {
    /**
     * {
     * "city": "U2hhbmdoYWk=",
     * "cityAL": "w4nDj8K6wqM=",
     * "country": "CN",
     * "lang": "zh",
     * "mCC": "5411",
     * "name": "VW5pb25QYXkgSW50ZXJuYXRpb25hbA==",
     * "nameAL": "w5LDuMOBwqrCucO6wrzDig==",
     * "postal": "200120",
     * "qRCUseCase": "10",
     * "tID": "012345678912345",
     * "tranAmt": "101.10",
     * "tranCur": "156",
     * "tranID": "A399900052018051117000200005348"
     * }
     */

    //交易ID
    String tranID;
    //交易金额
    String tranAmt;
    //交易币种
    String tranCur;
    //卡司接口返回字段（QRC Use Case）
    String qRCUseCase;
    //商户名称
    String name;
    //商户类别码
    String mCC;
    //商户所属国家
    String country;
    //商户城市
    String city;
    //商户邮编
    String postal;
    //商户语言
    String lang;
    //商户别名
    String nameAL;
    //城市别名
    String cityAL;
    //终端ID(卡司接口返回，和deviceId不同)
    String tID;

    public void setTranID(String tranID) {
        this.tranID = tranID;
    }

    public void setTranAmt(String tranAmt) {
        this.tranAmt = tranAmt;
    }

    public void setTranCur(String tranCur) {
        this.tranCur = tranCur;
    }

    public void setqRCUseCase(String qRCUseCase) {
        this.qRCUseCase = qRCUseCase;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setmCC(String mCC) {
        this.mCC = mCC;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setNameAL(String nameAL) {
        this.nameAL = nameAL;
    }

    public void setCityAL(String cityAL) {
        this.cityAL = cityAL;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getTranID() {
        return tranID;
    }

    public String getTranAmt() {
        return tranAmt;
    }

    public String getTranCur() {
        return tranCur;
    }

    public String getqRCUseCase() {
        return qRCUseCase;
    }

    public String getName() {
        return name;
    }

    public String getmCC() {
        return mCC;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getPostal() {
        return postal;
    }

    public String getLang() {
        return lang;
    }

    public String getNameAL() {
        return nameAL;
    }

    public String getCityAL() {
        return cityAL;
    }

    public String gettID() {
        return tID;
    }
}
