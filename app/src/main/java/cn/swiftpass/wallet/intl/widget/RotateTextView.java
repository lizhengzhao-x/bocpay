package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 *
 */

public class RotateTextView extends TextView {

    public RotateTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.rotate(90);
        canvas.translate(0, -AndroidUtils.dip2px(getContext(), 60));
        super.onDraw(canvas);
    }
}
