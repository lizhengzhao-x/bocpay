package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.CreditCardBillEntity;
import cn.swiftpass.wallet.intl.entity.CreditCardEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.ViewHolderUtils;


public class CreditCardPaymentListExpandableAdapter extends BaseExpandableListAdapter {

    private ArrayList<CreditCardEntity.CardListBean> groups;
    private Context mContext;
    private View checkMoreView;
    private View itemChildView;

    public void setOnGroupItemClickListener(OnGroupItemClickListener onGroupItemClickListener) {
        this.onGroupItemClickListener = onGroupItemClickListener;
    }

    //    private int[] mSectionIndices;
//    private Character[] mSectionLetters;
    private OnGroupItemClickListener onGroupItemClickListener;

    public CreditCardPaymentListExpandableAdapter(ArrayList<CreditCardEntity.CardListBean> groupsIn, Context contextIn) {
        this.groups = groupsIn;
        this.mContext = contextIn;
        checkMoreView = LayoutInflater.from(contextIn).inflate(R.layout.item_check_more_view, null);
        itemChildView = LayoutInflater.from(contextIn).inflate(R.layout.item_bill_credit, null);
    }


    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return groups.get(groupPosition).getStmtTxnBeans().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if (groups.get(groupPosition).getStmtTxnBeans().size() > childPosition) {
            return groups.get(groupPosition).getStmtTxnBeans().get(childPosition);
        } else {
            return null;
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.position_header_view, null);
        }
        ImageView logo = (ImageView) ViewHolderUtils.get(convertView, R.id.id_left_image);
        TextView title = (TextView) ViewHolderUtils.get(convertView, R.id.id_tv_left);
        TextView expand = (TextView) ViewHolderUtils.get(convertView, R.id.id_image_right);
        CreditCardEntity.CardListBean cardListBean = groups.get(groupPosition);
        title.setText(cardListBean.getUiTitle());
        expand.setText(cardListBean.isExpand() ? mContext.getString(R.string.E1_1_1) : mContext.getString(R.string.E1_2_3));
        GlideApp.with(mContext)
                .load(AndroidUtils.getCardFaceUrl(SmartAccountConst.CARD_TYPE_CREDIT) + cardListBean.getCardFaceId() + ".png")
                .into(logo);
        return convertView;
    }

    private int getItemViewType(int postion, int groupPosition) {
        CreditCardBillEntity.StmtTxnBean cardItemEntity = groups.get(groupPosition).getStmtTxnBeans().get(postion);
        if (!TextUtils.isEmpty(cardItemEntity.getUiType()) && cardItemEntity.getUiType().equals(CreditCardBillEntity.StmtTxnBean.UITYPE_MORE)) {
            return 0;
        }
        return 1;
    }

    @Override
    public View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        CreditCardBillEntity.StmtTxnBean cardItemEntity = groups.get(groupPosition).getStmtTxnBeans().get(childPosition);
        MoreViewHolder holderMore = null;
        ItemViewHolder holderItem = null;
        if (convertView == null) {
            if (getItemViewType(childPosition, groupPosition) == 0) {
                holderMore = new MoreViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check_more_view, null);
                holderMore.checkMore = convertView.findViewById(R.id.transfer_ok);
                convertView.setTag(holderMore);
            } else if (getItemViewType(childPosition, groupPosition) == 1) {
                holderItem = new ItemViewHolder();
                convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill_credit, null);
                holderItem.mId_merchant_head = convertView.findViewById(R.id.id_merchant_head);
                holderItem.mId_merchant_name = convertView.findViewById(R.id.id_merchant_name);
                holderItem.mId_merchant_money = convertView.findViewById(R.id.id_merchant_money);
                holderItem.mId_bill_time = convertView.findViewById(R.id.id_bill_time);
                holderItem.mId_discount = convertView.findViewById(R.id.id_discount);
                holderItem.mId_pay_symbol = convertView.findViewById(R.id.id_pay_symbol);
                holderItem.mId_root_view = convertView.findViewById(R.id.id_root_view);
                convertView.setTag(holderItem);
            }
        } else {
            if (getItemViewType(childPosition, groupPosition) == 0) {
                if (convertView.getTag() instanceof MoreViewHolder) {
                    holderMore = (MoreViewHolder) convertView.getTag();
                } else {
                    //item type做了切换
                    holderMore = new MoreViewHolder();
                    convertView = setViewHolder(holderMore, parent);
                }

            } else {
                if (convertView.getTag() instanceof ItemViewHolder) {
                    holderItem = (ItemViewHolder) convertView.getTag();
                } else {
                    //item type做了切换
                    holderItem = new ItemViewHolder();
                    convertView = setViewHolder(holderItem, parent);
                }
            }
        }
        if (getItemViewType(childPosition, groupPosition) == 0) {
            holderMore.checkMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onGroupItemClickListener != null) {
                        onGroupItemClickListener.onGroupItemClick(groupPosition);
                    }
                }
            });
        } else {
            holderItem.mId_merchant_name.setText(cardItemEntity.getTxnDesc());
            holderItem.mId_bill_time.setText(cardItemEntity.getTxnDt());
            holderItem.mId_pay_symbol.setText(cardItemEntity.getCur());
            String trxAmt = cardItemEntity.getChrgPymtLab();
            if (TextUtils.isEmpty(trxAmt)) {
                trxAmt = "0";
            }
            holderItem.mId_merchant_money.setText(String.format(trxAmt) + cardItemEntity.getShowType());
            if (!TextUtils.isEmpty(cardItemEntity.getDiscount())) {
                //优惠信息
                if (Integer.valueOf(cardItemEntity.getDiscount()) > 0) {
                    holderItem.mId_discount.setVisibility(View.VISIBLE);
                    holderItem.mId_discount.setText(cardItemEntity.getDiscount());
                    holderItem.mId_discount.getPaint().setAntiAlias(true);
                    holderItem.mId_discount.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                }
            }
            holderItem.mId_merchant_head.setImageResource(R.mipmap.img_icon_store_list);
            if (!TextUtils.isEmpty(cardItemEntity.getGpDetail())) {
                if (cardItemEntity.isGpDetail()) {
                    //App判断是否展示积分换领详情页   1:是 0：否 积分显示金额
                    holderItem.mId_merchant_head.setImageResource(R.mipmap.icon_transactions_points_old);
                }
            }
        }

        return convertView;
    }

    private View setViewHolder(ItemViewHolder holderItem, View parent) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bill_credit, null);
        holderItem.mId_merchant_head = convertView.findViewById(R.id.id_merchant_head);
        holderItem.mId_merchant_name = convertView.findViewById(R.id.id_merchant_name);
        holderItem.mId_merchant_money = convertView.findViewById(R.id.id_merchant_money);
        holderItem.mId_bill_time = convertView.findViewById(R.id.id_bill_time);
        holderItem.mId_discount = convertView.findViewById(R.id.id_discount);
        holderItem.mId_pay_symbol = convertView.findViewById(R.id.id_pay_symbol);
        holderItem.mId_root_view = convertView.findViewById(R.id.id_root_view);
        convertView.setTag(holderItem);
        return convertView;
    }

    private View setViewHolder(MoreViewHolder holderMore, View parent) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check_more_view, null);
        holderMore.checkMore = convertView.findViewById(R.id.transfer_ok);
        convertView.setTag(holderMore);
        return convertView;
    }


    private static class MoreViewHolder {
        TextView checkMore;
    }

    private static class ItemViewHolder {

        ImageView mId_merchant_head;
        TextView mId_merchant_name;
        TextView mId_merchant_money;
        TextView mId_bill_time;
        TextView mId_discount;
        TextView mId_pay_symbol;
        LinearLayout mId_root_view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public interface OnGroupItemClickListener {
        void onGroupItemClick(int groupId);
    }
}
