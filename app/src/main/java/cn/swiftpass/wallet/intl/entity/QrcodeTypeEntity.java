package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class QrcodeTypeEntity extends BaseEntity {


    /**
     * EMV码
     */
    public static final String QR_EMV_PAY = "EMV_PAY";
    /**
     * 银联url
     */
    public static final String QR_UNION_PAY = "UNION_URL_PAY";
    /**
     * 银联商户码
     */
    public static final String QR_WEB_PAY = "UNION_MERCHANT_PAY";
    /**
     * 微信pay
     */
    public static final String QR_MER_PAY = "WX_PAY";
    /**
     * 银联UPLAN商户码
     */
    public static final String UPLAN_MERCHANT_URL = "UPLAN_MERCHANT_URL";


    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    private String couponPictureUrl;

    public String getCouponPictureUrl() {
        return couponPictureUrl;
    }

    public void setCouponPictureUrl(String couponPictureUrl) {
        this.couponPictureUrl = couponPictureUrl;
    }

    private String payUrl;

    public String getPayUrl() {
        return payUrl;
    }

    public void setPayUrl(String payUrl) {
        this.payUrl = payUrl;
    }

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    private String walletId;
    private String action;

    public String getQrcType() {
        return qrcType;
    }

    public void setQrcType(String qrcType) {
        this.qrcType = qrcType;
    }

    private String qrcType;


}
