package cn.swiftpass.wallet.intl.module.cardmanagement.view;


import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 电子券 列表查询 展示
 */
public class GradeListAdapter implements AdapterItem<SmaGpInfoBean> {
    private Context context;
    private boolean isShowLeft;

    public GradeListAdapter(Context context, boolean isShowLeftIn) {
        this.context = context;
        isShowLeft = isShowLeftIn;
    }

    private TextView mId_card_title;
    private TextView mId_card_grade;
    private TextView mId_card_number;
    private TextView mId_card_expiredata;
    private ImageView mId_left_sel_image;
    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.include_grade_item;
    }

    @Override
    public void bindViews(View root) {
        mId_card_title = (TextView) root.findViewById(R.id.id_card_title);
        mId_card_grade = (TextView) root.findViewById(R.id.id_card_grade);
        mId_card_number = (TextView) root.findViewById(R.id.id_card_number);
        mId_card_expiredata = (TextView) root.findViewById(R.id.id_card_expiredata);
        mId_left_sel_image = (ImageView) root.findViewById(R.id.id_left_sel_image);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(SmaGpInfoBean homeMessageEntity, int position) {
        mPosition = position;
        mId_left_sel_image.setVisibility(View.GONE);
        mId_card_title.setText(homeMessageEntity.getName());
        if (!TextUtils.isEmpty(homeMessageEntity.getBal())) {
            mId_card_grade.setText(AndroidUtils.formatPrice(Double.valueOf(homeMessageEntity.getBal()), false));
        }
        mId_card_number.setText(homeMessageEntity.getPan());
        mId_card_expiredata.setText(context.getString(R.string.EC04_1_a) + homeMessageEntity.getExpiryDate());
    }


}
