package cn.swiftpass.wallet.intl.utils.encry;

import android.content.Context;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.GreetingEntity;

/**
 * Created by admin on 2018/7/19.
 */

public class InitJsonContants {
    public static final String menuJson = "{\"menuData\":[{\"menuKey\":\"HOME\",\"name\":\"主页\"},{\"childMenus\":[{\"menuKey\":\"MY_ACCOUNT_GUIDE\",\"name\":\"我的账户\"},{\"menuKey\":\"TRANSACTION_MANAGEMENT\",\"name\":\"交易记录\"}],\"menuKey\":\"MY_ACCOUNT\",\"name\":\"我的账户\"},{\"childMenus\":[{\"menuKey\":\"VIRTUAL_CARD_APPLY\",\"name\":\"申请\"},{\"menuKey\":\"VIRTUAL_CARD_CONFIRM\",\"name\":\"确认\"}],\"menuKey\":\"VIRTUAL_CARD\",\"name\":\"虚拟信用卡\"},{\"childMenus\":[{\"menuKey\":\"BACK_SCAN_QR_CODE\",\"name\":\"付款码\"},{\"menuKey\":\"MAJOR_SCAN_QR_CODE\",\"name\":\"一扫即付\"},{\"menuKey\":\"TRANSFER_SELECT_LIST\",\"name\":\"转账/汇款\"},{\"menuKey\":\"NEW_YEAR_RED_POCKET\",\"name\":\"新年派利是\"},{\"menuKey\":\"TRANSFER_BY_STATIC_CODE\",\"name\":\"收款\"}],\"menuKey\":\"PAYMENT_OR_COLLECT\",\"name\":\"支付/收款\"},{\"childMenus\":[{\"menuKey\":\"TRANSFER_CROSS_BORDER\",\"name\":\"跨境汇款\"},{\"menuKey\":\"PAYMENT_CROSS_BORDER\",\"name\":\"跨境缴费\"}],\"menuKey\":\"CROSS_BORDER\",\"name\":\"跨境\"},{\"childMenus\":[{\"menuKey\":\"NEW_MESSAGE\",\"name\":\"至HIT优惠\"},{\"menuKey\":\"CREDIT_CARD_REGISTER_REWARD\",\"name\":\"登记推广\"},{\"menuKey\":\"INVITE_SHARE\",\"name\":\"推荐亲友\"}],\"menuKey\":\"PREFERENCES_LIST\",\"name\":\"推广优惠\"},{\"menuKey\":\"ECOUPON\",\"name\":\"电子礼券\"},{\"menuKey\":\"UPLAN\",\"name\":\"优计划\"}],\"menuEncryptionData\":\"84031b1aee567d92259eca287963afb5ed46ebc6e8558c51e5e3fadbf61aad89\"}";

    public static ArrayList<GreetingEntity> getDefaultGreeting(Context context) {
        ArrayList<GreetingEntity> greetings = new ArrayList<>();
        greetings.add(new GreetingEntity(" ", "00:00:00", "11:59:59", "HH:mm:ss"));
        greetings.add(new GreetingEntity(" ", "12:00:00", "18:59:59", "HH:mm:ss"));
        greetings.add(new GreetingEntity(" ", "19:00:00", "23:59:59", "HH:mm:ss"));
        return greetings;
    }


}
