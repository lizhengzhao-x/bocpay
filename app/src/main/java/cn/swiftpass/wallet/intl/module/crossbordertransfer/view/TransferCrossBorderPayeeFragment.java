package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountManageActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderPayeeContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderCacheEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderCardEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderRecordEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.TransferCrossBorderPayeePresenter;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.TransInformation;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.entity.PayeeEntity;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.module.transfer.view.SoftKeyboardTranslationView;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter.CHINESE_SPECIAL_BANK_NAME;

public class TransferCrossBorderPayeeFragment extends BaseFragment<TransferCrossBorderPayeeContract.Presenter> implements TransferCrossBorderPayeeContract.View, OnRecordItemClickListener {


    private static final String ACTION_SET_DAILY_LIMIT_BALANCE = "ACTION_SET_DAILY_LIMIT_BALANCE";
    private static final String ACTION_GET_SMART_INFO = "ACTION_GET_SMART_INFO";
    private final int TYPE_CHECK_EMPTY = 1001;
    private final int TYPE_CHECK_NEW_ADD = 1002;
    private final int TYPE_CHECK_SELF = 1003;
    private final int TYPE_CHECK_RECORD = 1004;

    @BindView(R.id.cb_self)
    CheckBox cbSelf;
    @BindView(R.id.cb_record)
    CheckBox cbRecord;
    @BindView(R.id.ry_record)
    RecyclerView ryRecord;
    @BindView(R.id.cb_add_new)
    CheckBox cbAddNew;
    @BindView(R.id.ed_payee_name)
    EditTextWithDel edPayeeName;
    @BindView(R.id.ed_payee_card)
    EditTextWithDel edPayeeCard;
    @BindView(R.id.ed_payee_bank)
    TextView edPayeeBank;
    @BindView(R.id.tv_next)
    TextView tvNext;
    @BindView(R.id.tv_wrong_bank)
    TextView tvWrongBank;
    @BindView(R.id.tv_clear)
    TextView tvClear;
    @BindView(R.id.id_checkmore_view)
    View checkMoreView;
    @BindView(R.id.id_checkmore_view_tv)
    TextView checkMoreTv;
    @BindView(R.id.id_checkmore_view_img)
    ImageView checkMoreImg;
    @BindView(R.id.id_linear_recently)
    LinearLayout linearRecentlyView;
    @BindView(R.id.ll_body)
    SoftKeyboardTranslationView bodyLayout;
    @BindView(R.id.iv_tip_card)
    ImageView ivTipCard;
    @BindView(R.id.iv_tip_payee)
    ImageView ivTipPayee;
    @BindView(R.id.iv_tip_bank)
    ImageView ivTipBank;
    @BindView(R.id.nsl_body)
    LinearLayout llContent;
    @BindView(R.id.iv_toggle_see)
    ImageView ivToggleSee;
    @BindView(R.id.ll_home_top_up_account)
    LinearLayout llHomeTopUpAccount;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.ll_account_balance)
    LinearLayout llAccountBalance;


    private int REQ_INPUT_AMOUNT = 2001;
    private TransferCrossBorderRecordEntity mCurrentSelrecordEntity;
    private TransferCrossBorderCacheEntity mCurrentCacheEntity;
    private boolean isEyeOpen = false;
    private String cardType;
    private TransferCrossBorderBaseEntity baseEntity;
    private TransferCrossBorderRecordAdapter adapter;
    private boolean isSelRecordList = false;

    private HashMap<String, TransferCrossBorderCardEntity> cardCacheList = new HashMap<>();
    private HashMap<String, Object> tmpCardCacheList = new HashMap<>();

    TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            //名字不能以空格开头
            if (edPayeeName.getEditText().getText().toString().startsWith(" ")) {
                edPayeeName.setContentText(edPayeeName.getEditText().getText().toString().replace(" ", ""));
            }
            checkNextBtn();
        }

        @Override
        public void afterTextChanged(Editable s) {
//            if (!cbSelf.isChecked() && edPayeeName.getEditText().hasFocus()) {
//                //当进入app输入卡号 应该选中新增收款人选项
//                cbAddNew.setChecked(true);
//            }
        }
    };


    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.SMB1_1_4);
            bodyLayout.initPopView(mActivity, llContent);
            mPresenter.getSmartAccountInfo("", false);
            hiddenAccountData();
        }
    }

    public void setBaseEntity(TransferCrossBorderBaseEntity baseEntity) {
        this.baseEntity = baseEntity;
    }

    @Override
    protected void initView(View v) {

        EventBus.getDefault().register(this);

        ryRecord.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        if (baseEntity.recent.size() == 0) {
            linearRecentlyView.setVisibility(View.GONE);
        } else {
            adapter = new TransferCrossBorderRecordAdapter(mContext, this);
            //默认只显示3条 点击查看更多
            int size = baseEntity.recent.size() > 3 ? 3 : baseEntity.recent.size();
            if (size > 3) {
                checkMoreView.setVisibility(View.VISIBLE);
            }
            adapter.setData(baseEntity.recent.subList(0, size));
            ryRecord.setAdapter(adapter);
        }

        initCheckListener();
        resetCheck();
        updatePayee(TYPE_CHECK_EMPTY, null);
        checkMoreView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUIShow();
            }
        });


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void updateUIShow() {

        if (adapter.getItemCount() == baseEntity.recent.size()) {
            int size = baseEntity.recent.size() > 3 ? 3 : baseEntity.recent.size();
            adapter.setData(baseEntity.recent.subList(0, size));
            checkMoreTv.setText(getString(R.string.CT1_2d_1a));
            checkMoreImg.setImageResource(R.mipmap.icon_text_select_right);
        } else {
            adapter.setData(baseEntity.recent);
            checkMoreTv.setText(getString(R.string.CT1_2d_2));
            checkMoreImg.setImageResource(R.mipmap.icon_text_select_up);
        }
        adapter.notifyDataSetChanged();
    }

    private void initCheckListener() {
        cbSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetCheck();
                cbSelf.setChecked(true);
                edPayeeCard.getEditText().setEnabled(true);
                updatePayee(TYPE_CHECK_SELF, baseEntity.self);
            }
        });
        cbRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetCheck();
                mCurrentSelrecordEntity = null;
                mCurrentCacheEntity = null;//选中最近联系人后清空，之前的缓存数据
                cbRecord.setChecked(true);
                edPayeeCard.getEditText().setEnabled(false);
                updatePayee(TYPE_CHECK_RECORD, null);
            }
        });
        cbAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetCheck();
                cbAddNew.setChecked(true);
                edPayeeCard.getEditText().setEnabled(true);
                updatePayee(TYPE_CHECK_NEW_ADD, null);
            }
        });


        edPayeeName.addTextChangedListener(watcher);


        edPayeeCard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.toString().length() < 6) {
                    tvWrongBank.setVisibility(View.GONE);
                    edPayeeBank.setText("");
                } else {
                    String cardStr = "";
                    if (editable.toString().length() == 6) {
                        cardStr = editable.toString();
                    } else if (editable.toString().length() == 7) {
                        cardStr = editable.toString();
                    } else {
                        cardStr = editable.toString().substring(0, 8);
                    }
                    if (!cardCacheList.containsKey(cardStr) && !tmpCardCacheList.containsKey(cardStr)) {
                        tmpCardCacheList.put(cardStr, "");
                        mPresenter.checkCardId(cardStr);
                    } else if (cardCacheList.containsKey(cardStr)) {
                        //银行卡号缓存中存在这个卡号
                        TransferCrossBorderCardEntity cardEntity = cardCacheList.get(cardStr);
                        tvWrongBank.setVisibility(View.GONE);
                        cardType = cardEntity.cardType;
                        edPayeeBank.setText(cardEntity.bankName + " - " + AndroidUtils.getCardType(cardEntity.cardType, mContext));
                    }
                }
                if (!cbSelf.isChecked() && !cbRecord.isChecked() && edPayeeCard.getEditText().hasFocus()) {
                    //当进入app输入卡号 应该选中新增收款人选项
                    cbAddNew.setChecked(true);
                }
                checkNextBtn();
            }
        });
        edPayeeCard.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(19)});

        edPayeeName.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(135), new NormalInputFilter(CHINESE_SPECIAL_BANK_NAME)});
        //小写自动转换为大写
        edPayeeName.getEditText().setTransformationMethod(new TransInformation());

        edPayeeCard.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(final boolean hasFocus) {
                if (!hasFocus) {
                    edPayeeCard.hideDelView();
                }

                if (hasFocus && !cbAddNew.isChecked() && !cbRecord.isChecked() && !cbSelf.isChecked()) {
                    //resetCheck();
                    cbAddNew.setChecked(true);
                }
                //这里头有个问题 用户点击x号删除的时候 edittext会焦点先变化 然后再出发 afterTextChanged 所以导致会 checkCardId dialog 显示 目前先做个延迟解决问题
                mFmgHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (edPayeeCard == null) return;
                        String cardText = edPayeeCard.getText();
                        if (!hasFocus && !TextUtils.isEmpty(cardText) && cardText.length() > 5) {
                            mPresenter.checkCardId(cardText);
                        }
                    }
                }, 200);

            }
        });

        edPayeeName.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (!hasFocus) {
                    edPayeeName.hideDelView();
                }
                if (hasFocus && !cbAddNew.isChecked() && !cbRecord.isChecked() && !cbSelf.isChecked()) {
                    //resetCheck();
                    cbAddNew.setChecked(true);
                }
            }
        });

        edPayeeBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cardText = edPayeeCard.getText();
                if (!TextUtils.isEmpty(cardText) && cardText.length() > 5) {
                    removeFocus();
                    mPresenter.checkCardId(cardText);
                }
            }
        });
    }

    private void removeFocus() {
//        //防止dialog隐藏 软键盘弹出
//        edPayeeName.getEditText().setEnabled(false);
//        edPayeeCard.getEditText().setEnabled(false);
    }

    private void addFocus() {
//        edPayeeName.getEditText().setEnabled(true);
//        edPayeeCard.getEditText().setEnabled(true);
    }


    public void checkNextBtn() {
        if (!TextUtils.isEmpty(edPayeeName.getText()) && !TextUtils.isEmpty(edPayeeCard.getText()) && edPayeeCard.getText().toString().length() >= 16 && !TextUtils.isEmpty(edPayeeBank.getText()) && edPayeeCard.getText().length() > 5) {
            tvNext.setEnabled(true);
        } else {
            tvNext.setEnabled(false);
        }
    }

    @Override
    public void OnRecordItemClick(TransferCrossBorderRecordEntity recordEntity) {
        mCurrentSelrecordEntity = recordEntity;
        isSelRecordList = true;
        tvWrongBank.setVisibility(View.GONE);
        updatePayee(TYPE_CHECK_RECORD, recordEntity);
        if (adapter.getItemCount() == baseEntity.recent.size()) {
            updateUIShow();
        }

    }

    private void updatePayee(int type, PayeeEntity payeeEntity) {

        switch (type) {
            case TYPE_CHECK_EMPTY:
                ryRecord.setVisibility(View.GONE);
                checkMoreView.setVisibility(View.GONE);
                break;
            case TYPE_CHECK_NEW_ADD:
                ryRecord.setVisibility(View.GONE);
                checkMoreView.setVisibility(View.GONE);
                edPayeeName.setEditTextEnable(true);
                edPayeeName.getEditText().setLongClickable(true);
                edPayeeName.getEditText().setSelected(true);
                edPayeeName.getEditText().setEnabled(true);
                edPayeeName.getEditText().setText("");
                edPayeeCard.getEditText().setText("");
                edPayeeBank.setText("");
                break;
            case TYPE_CHECK_SELF:
                ryRecord.setVisibility(View.GONE);
                checkMoreView.setVisibility(View.GONE);
                edPayeeName.setEditTextEnable(false);
                edPayeeName.getEditText().setLongClickable(false);
                edPayeeName.getEditText().setSelected(false);
                edPayeeName.getEditText().setEnabled(false);
                if (payeeEntity != null) {
                    edPayeeName.getEditText().setText(payeeEntity.payeeName);
                    edPayeeCard.getEditText().setText("");
                    edPayeeBank.setText("");
                }
                cbAddNew.setChecked(false);
                break;
            case TYPE_CHECK_RECORD:
                edPayeeName.setEditTextEnable(true);
                edPayeeName.getEditText().setLongClickable(true);
                edPayeeName.getEditText().setSelected(true);
                edPayeeName.getEditText().setEnabled(true);
                ryRecord.setVisibility(View.VISIBLE);
                //只有在选择了最近汇款记录里面的汇款人后,银行卡号才禁止修改
                if (payeeEntity != null) {
                    edPayeeCard.getEditText().setEnabled(false);
                } else {
                    edPayeeCard.getEditText().setEnabled(true);
                }
                if (baseEntity.recent.size() > 3) {
                    checkMoreView.setVisibility(View.VISIBLE);
                } else {
                    checkMoreView.setVisibility(View.GONE);
                }
                if (adapter.getItemCount() == baseEntity.recent.size()) {
                    checkMoreTv.setText(getString(R.string.CT1_2d_2));
                    checkMoreImg.setImageResource(R.mipmap.icon_text_select_up);
                } else {
                    checkMoreTv.setText(getString(R.string.CT1_2d_1a));
                    checkMoreImg.setImageResource(R.mipmap.icon_text_select_right);
                }
                edPayeeName.getEditText().setText("");
                if (payeeEntity != null) {
                    edPayeeCard.setContentText(payeeEntity.payeeCardId);
                    edPayeeBank.setText(payeeEntity.payeeBank + " - " + AndroidUtils.getCardType(payeeEntity.payeeCardType, mContext));
                    cardType = payeeEntity.payeeCardType;
                }
                //  edPayeeName.getEditText().setText(""); 会导致焦点变化
                cbAddNew.setChecked(false);
                break;
            default:
                break;
        }
        edPayeeName.hideDelView();
        edPayeeCard.hideDelView();
    }


    private void resetCheck() {
        ryRecord.setVisibility(View.GONE);
        cbSelf.setChecked(false);
        cbRecord.setChecked(false);
        cbAddNew.setChecked(false);
        isSelRecordList = false;
        edPayeeName.getEditText().clearFocus();
        edPayeeName.setContentText("");
        edPayeeCard.getEditText().clearFocus();
        edPayeeCard.setContentText("");
        edPayeeName.setEditTextEnable(true);
        tvWrongBank.setVisibility(View.GONE);

    }


    @Override
    protected TransferCrossBorderPayeeContract.Presenter loadPresenter() {
        return new TransferCrossBorderPayeePresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transfer_cross_border_payee;
    }


    @Override
    public void checkCardIdSuccess(TransferCrossBorderCardEntity response) {

        if (response != null && !TextUtils.isEmpty(response.bankName)) {
            //有几个焦点异常的问题
            if (edPayeeCard.getText().length() == 0) {
                return;
            }
            tmpCardCacheList.remove(response.cardNo);
            cardCacheList.put(response.cardNo, response);

            if (!TextUtils.isEmpty(response.cardNo)) {
                if (!TextUtils.isEmpty(edPayeeCard.getText()) && edPayeeCard.getText().length() > 5) {
                    if (edPayeeCard.getText().startsWith(response.cardNo)) {
                        tvWrongBank.setVisibility(View.GONE);
                        cardType = response.cardType;
                        edPayeeBank.setText(response.bankName + " - " + AndroidUtils.getCardType(response.cardType, mContext));
                    }
                }

            }


        } else {
            if (response != null && !TextUtils.isEmpty(response.cardNo)) {
                tmpCardCacheList.remove(response.cardNo);
            } else {
                tmpCardCacheList.clear();
            }
            cardType = "";
            edPayeeBank.setText("");
            if (edPayeeCard.getEditText().getText().toString().length() > 7) {
                tvWrongBank.setVisibility(View.VISIBLE);
            } else {
                tvWrongBank.setVisibility(View.GONE);
            }
        }

        checkNextBtn();
    }

    @Override
    public void checkCardIdError(String errorCode, String errorMsg) {
        tmpCardCacheList.clear();
        if (TextUtils.equals(errorCode, ErrorCode.CONTENT_TIME_OUT.code) || TextUtils.equals(errorCode, ErrorCode.NETWORK_ERROR.code)) {
            showErrorMsgDialog(mContext, getString(R.string.CT1_2a_12a));
        } else {
            showErrorMsgDialog(mContext, errorMsg);
        }

    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {

    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response, String action) {
        if (response != null) {
            if (ACTION_SET_DAILY_LIMIT_BALANCE.equals(action)) {
                SmartAccountAdjustDailyLimitActivity.startActivity(mActivity, response, response.getHideMobile());
            } else if (ACTION_GET_SMART_INFO.equals(action)) {
                showAccountData(response);
            }
        }
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mySmartAccountEntity.getHideMobile());
            mHashMapsLogin.put(Constants.SMART_ACCOUNT, mySmartAccountEntity);
            mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
            ActivitySkipUtil.startAnotherActivity(mActivity, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        if (requestCode == REQ_INPUT_AMOUNT && resultCode == Activity.RESULT_OK) {
            if (intent != null && !isSelRecordList) {
                //如果当前页面选中的是最近联系人，则不接收数据
                mCurrentCacheEntity = (TransferCrossBorderCacheEntity) intent.getSerializableExtra(TransferCrossBorderConst.DATA_TRANSFER_CACHE);
            }

        }

    }


    @OnClick({R.id.tv_amount_tip, R.id.tv_amount, R.id.tv_amount_unused_tip, R.id.tv_amount_usable,
            R.id.iv_set_daily_limit_balance, R.id.ll_home_top_up_account, R.id.iv_toggle_see, R.id.tv_clear, R.id.tv_next, R.id.ll_body, R.id.nsl_body, R.id.iv_tip_card, R.id.iv_tip_payee, R.id.iv_tip_bank})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_set_daily_limit_balance:
                mPresenter.getSmartAccountInfo(ACTION_SET_DAILY_LIMIT_BALANCE, true);
                break;
            case R.id.ll_home_top_up_account:
                topUpAccount();
                break;
            case R.id.iv_toggle_see:
            case R.id.tv_amount_tip:
            case R.id.tv_amount:
            case R.id.tv_amount_unused_tip:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_CROSSBORDER_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                toggleSee();
                break;
            case R.id.iv_tip_card:
                //收款银行卡号
                AndroidUtils.showTipDialogWithCusSize(mActivity, getString(R.string.CT1_2a_10b), getString(R.string.CT1_2a_10c), 16, getString(R.string.CT1_2a_10d));
                break;
            case R.id.iv_tip_payee:
                //收款人姓名
                AndroidUtils.showTipDialogWithCusSize(mActivity, getString(R.string.CT1_2a_8), getString(R.string.CT1_2a_8a), 16, getString(R.string.CT1_2a_10d));
                break;
            case R.id.iv_tip_bank:
                //收款银行
                AndroidUtils.showTipDialogWithCusSize(mActivity, getString(R.string.CT1_2a_11a), getString(R.string.SCB2101_1_7), 16, getString(R.string.CT1_2a_10d));
                break;
            case R.id.ll_body:
            case R.id.nsl_body:
                bodyLayout.setFocusable(true);
                bodyLayout.setFocusableInTouchMode(true);
                bodyLayout.requestFocus();
                edPayeeName.getEditText().clearFocus();
                edPayeeCard.getEditText().clearFocus();
                edPayeeBank.clearFocus();
                break;
            case R.id.tv_clear:
                edPayeeName.getEditText().setText("");
                edPayeeCard.getEditText().setText("");
                edPayeeBank.setText("");
                edPayeeCard.setEditTextEnable(true);
                edPayeeName.setEditTextEnable(true);
                resetCheck();
                checkMoreView.setVisibility(View.GONE);
                break;
            case R.id.tv_next:
                HashMap<String, Object> param = new HashMap<>();
                param.put(TransferCrossBorderConst.DATA_TRANSFER_BASE, baseEntity);
                param.put(TransferCrossBorderConst.DATA_PAYEE_CARD_TYPE, cardType);
                param.put(TransferCrossBorderConst.DATA_PAYEE_NAME, edPayeeName.getText().toString().trim());
                param.put(TransferCrossBorderConst.DATA_PAYEE_CARD_ID, edPayeeCard.getText());
                param.put(TransferCrossBorderConst.DATA_PAYEE_BANK, edPayeeBank.getText());
                param.put(TransferCrossBorderConst.ISTRANSFERTOSELF, cbSelf.isChecked() ? "1" : "0");
                if (cbRecord.isChecked() && mCurrentSelrecordEntity != null) {
                    param.put(TransferCrossBorderConst.DATA_FOR_USED_CODE, mCurrentSelrecordEntity.getForUsedObj().getCode());
                    param.put(TransferCrossBorderConst.DATA_FOR_USED_VALUE, mCurrentSelrecordEntity.getForUsedObj().getValue());
                    param.put(TransferCrossBorderConst.DATA_TRANSFER_ITEM, cbSelf.isChecked() ? "1" : "0");
                    param.put(TransferCrossBorderConst.DATA_TRANSFER_AMOUNT, mCurrentSelrecordEntity.amount);
                    param.put(TransferCrossBorderConst.DATA_TRANSFER_ACCOUNT, mCurrentSelrecordEntity.getAccountObj());
                } else if (mCurrentCacheEntity != null) {
                    param.put(TransferCrossBorderConst.DATA_FOR_USED_CODE, mCurrentCacheEntity.getForUsedObj().getCode());
                    param.put(TransferCrossBorderConst.DATA_FOR_USED_VALUE, mCurrentCacheEntity.getForUsedObj().getValue());
                    param.put(TransferCrossBorderConst.DATA_TRANSFER_ITEM, cbSelf.isChecked() ? "1" : "0");
                    if (!TextUtils.isEmpty(mCurrentCacheEntity.amount)) {
                        param.put(TransferCrossBorderConst.DATA_TRANSFER_AMOUNT, mCurrentCacheEntity.amount);
                    }
                    if (mCurrentCacheEntity.getAccountObj() != null) {
                        param.put(TransferCrossBorderConst.DATA_TRANSFER_ACCOUNT, mCurrentCacheEntity.getAccountObj());
                    }
                }
                ActivitySkipUtil.startAnotherActivityForResult(this, TransferCrossBorderAmountActivity.class, param, REQ_INPUT_AMOUNT);
                break;
            default:
                break;
        }


    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.d("home", "=====onHiddenChanged");
        if (hidden) {   // 不在最前端显示 相当于调用了onPause();
            return;
        } else {  // 在最前端显示 相当于调用了onResume();
            hiddenAccountData();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        hiddenAccountData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            hiddenAccountData();
        }
    }


    private void topUpAccount() {
         MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null && MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
            TransferUtils.checkTopUpEvent(mySmartAccountEntity, new TransferUtils.RegEddaEventListener() {
                @Override
                public void getRegEddaInfo() {
                    mPresenter.getRegEddaInfo();
                }

                @Override
                public Activity getContext() {
                    return getActivity();
                }

                @Override
                public void showErrorDialog(String errorCode, String message) {
                    showErrorMsgDialog(getContext(), message);
                }
            });
        } else {
            showActiveAccount();
        }
    }

    public void showActiveAccount() {
        AndroidUtils.showTipDialog(mActivity, getString(R.string.AC2101_19_11), getString(R.string.AC2101_19_8),
                getString(R.string.AC2101_19_10), getString(R.string.AC2101_19_9), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivitySkipUtil.startAnotherActivity(mActivity, SmartAccountManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                });
    }


    private void toggleSee() {
        if (isEyeOpen) {
            hiddenAccountData();
        } else {
            if (SystemInitManager.getInstance().isValidShowAccount()) {
                isEyeOpen = true;
                ivToggleSee.setImageResource(R.mipmap.icon_eye_open_purple);
                ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
                MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                if (mySmartAccountEntity != null) {
                    showAccountData(mySmartAccountEntity);
                } else {
                    mPresenter.getSmartAccountInfo(ACTION_GET_SMART_INFO, true);
                }
            } else {
                VerifyPasswordCommonActivity.startActivityForResult(mActivity, new OnPwdVerifyCallBack() {
                    @Override
                    public void onVerifySuccess() {
                        SystemInitManager.getInstance().setValidShowAccount(true);
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                        isEyeOpen = true;
                        ivToggleSee.setImageResource(R.mipmap.icon_eye_open_purple);
                        ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
                        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                        if (mySmartAccountEntity != null) {
                            showAccountData(mySmartAccountEntity);
                        } else {
                            mPresenter.getSmartAccountInfo(ACTION_GET_SMART_INFO, true);
                        }
                    }

                    @Override
                    public void onVerifyFailed(String errorCode, String errorMsg) {
                    }

                    @Override
                    public void onVerifyCanceled() {

                    }
                });
            }
        }
    }

    private void hiddenAccountData() {
        isEyeOpen = false;
        ivToggleSee.setImageResource(R.mipmap.icon_eye_close_purple);
        tvAmount.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.INVISIBLE);
        tvAmountUsable.setText("*****");
    }

    private void showAccountData(MySmartAccountEntity mySmartAccountEntity) {
        if (mySmartAccountEntity != null) {
            double price = Double.parseDouble(mySmartAccountEntity.getBalance());
            //账户冻结时，返回 -1 需要展示 *****
            if (price < 0) {
                tvAmount.setText("*****");
            } else {
                String newPrice = AndroidUtils.formatPriceWithPoint(price, true);
                tvAmount.setText(newPrice);
            }

            if (!TextUtils.isEmpty(mySmartAccountEntity.getOutavailbal())) {
                double dailyLimitBalance = Double.parseDouble(mySmartAccountEntity.getOutavailbal());
                String dailyLimitBalanceStr = AndroidUtils.formatPriceWithPoint(dailyLimitBalance, true);
                //账户冻结时，返回 -1 需要展示 *****
                if (dailyLimitBalance < 0) {
                    tvAmountUsable.setText("*****");
                } else {
                    tvAmountUsable.setText(dailyLimitBalanceStr);
                }
            } else {
                tvAmountUsable.setText("*****");
            }
        }
    }


}
