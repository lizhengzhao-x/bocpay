package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;

/**
 * @author create date on  on 2018/7/30 13:50
 * 校验我的账户OTP接口，返回对象为 BaseEntity
 */

public class CheckSmartAccountOtpProtocol extends BaseProtocol {
    public static final String TAG = CheckSmartAccountOtpProtocol.class.getSimpleName();

    /**
     * V：注册流程  B：绑定流程  F：忘记密码流程 L：登录流程
     */
    String mAction;
    /**
     * 短信验证码
     */
    String mVerifyCode;
    //用户ID
    String mWalletId;

    //如果在没有我的账户的前提下，需要传参
    BankLoginResultEntity mSmartAccountInfo;

    public CheckSmartAccountOtpProtocol(String walletId, String action, String verifyCode, BankLoginResultEntity smartInfo, NetWorkCallbackListener dataCallback) {
        mWalletId = walletId;
        this.mVerifyCode = verifyCode;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/checkOtp";
        mSmartAccountInfo = smartInfo;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        if (null != mSmartAccountInfo) {
            mBodyParams.put(RequestParams.TOPUP_METHOD, mSmartAccountInfo.getAddedmethod());
            mBodyParams.put(RequestParams.DAILY_PAY_LIMIT, mSmartAccountInfo.getPayLimit());
            mBodyParams.put(RequestParams.AUTO_TOPUP_LIMIT, mSmartAccountInfo.getAutotopupamt());
            mBodyParams.put(RequestParams.SMART_ACCOUNT_PRIMARY, mSmartAccountInfo.getRelevanceAccNo());
        }
    }
}
