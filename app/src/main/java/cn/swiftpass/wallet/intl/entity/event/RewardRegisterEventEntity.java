package cn.swiftpass.wallet.intl.entity.event;


public class RewardRegisterEventEntity extends BaseEventEntity{


    public static final int EVENT_BACK_REWARD_LIST = 400;


    public RewardRegisterEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public RewardRegisterEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
