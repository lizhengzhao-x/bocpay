package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;

public class SmartAccountDeleteContract {
    public interface View extends IView {


        void updateSmartAccountCancelFail(String errorCode, String errorMsg);

        void updateSmartAccountCancelSuccess(SmartAccountUpdateEntity response);
    }


    public interface Presenter extends IPresenter<SmartAccountDeleteContract.View> {


        void updateSmartAccountCancel();
    }
}
