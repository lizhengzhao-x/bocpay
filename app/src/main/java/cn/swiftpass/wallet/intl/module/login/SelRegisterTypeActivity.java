package cn.swiftpass.wallet.intl.module.login;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.entity.event.PWidEventEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.login.contract.SelRegisterTypeContract;
import cn.swiftpass.wallet.intl.module.login.presenter.SelRegisterPresenter;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.module.register.InputOrBindNewCardActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterPaymentAccountDescActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSmartAccountDescActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.LeftImageArrowView;


/**
 * 选择注册或绑定方式界面
 */

public class SelRegisterTypeActivity extends BaseCompatActivity<SelRegisterTypeContract.Presenter> implements SelRegisterTypeContract.View {
    /**
     * 信用卡
     */
    @BindView(R.id.id_link_credit_card)
    CardView cadCreditCard;
    /**
     * 智能账户
     */
    @BindView(R.id.id_link_bank_account)
    CardView cadBankAccount;


    @BindView(R.id.id_apply_bochk_bankaccount)
    CardView cadPaymentAccount;
    @BindView(R.id.tv_type_title)
    TextView mTypeTitleTV;
    @BindView(R.id.iav_credit_card)
    LeftImageArrowView mCreditCardIAV;
    @BindView(R.id.iav_bank_account)
    LeftImageArrowView mBankAccountIAV;
    @BindView(R.id.iav_bochk_bankaccount)
    LeftImageArrowView mBochkBankAccountIAV;
    @BindView(R.id.id_buttom_text)
    LinearLayout mId_buttom_text;
    @BindView(R.id.id_relative_layout_p_wid)
    RelativeLayout relativeLayoutPWid;
    @BindView(R.id.id_check_box_p_wid)
    CheckBox checkBoxPWid;

    /**
     * 跳转type 1.注册 2.绑卡 3.忘记密码
     */
    private int mCurrentPageFlow = Constants.PAGE_FLOW_REGISTER;


    @Override
    protected SelRegisterTypeContract.Presenter createPresenter() {
        return new SelRegisterPresenter();
    }


    @Override
    public void init() {
        //idv sdk 的相关errorCode errorMsg拉取
        CommonRequestUtils.getServerErrorCode(this);
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        mCurrentPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);

        initTitleByType();
        if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            //绑卡
            relativeLayoutPWid.setVisibility(View.VISIBLE);
            mId_buttom_text.setVisibility(View.VISIBLE);
            mCreditCardIAV.getTvLeft().setText(getString(R.string.BD1_1_1));
            mCreditCardIAV.setSubText(getString(R.string.CCA03_02_2));
            mBankAccountIAV.getTvLeft().setText(getString(R.string.BD1_1_3));
            mBankAccountIAV.setSubText(getString(R.string.CCA03_02_4));
            mBochkBankAccountIAV.getTvLeft().setText(getString(R.string.IDV_2_3_7));
            mBochkBankAccountIAV.setSubText(getString(R.string.CCA03_02_6));
            getSmartLevel();
        } else if (mCurrentPageFlow == Constants.PAGE_FLOW_REGISTER) {
            //注册
            relativeLayoutPWid.setVisibility(View.GONE);
            mId_buttom_text.setVisibility(View.VISIBLE);
            cadBankAccount.setVisibility(View.VISIBLE);
            cadCreditCard.setVisibility(View.VISIBLE);
            mCreditCardIAV.getTvLeft().setText(getString(R.string.P3_A1_2_b_2));
            mCreditCardIAV.setSubText(getString(R.string.CCA03_02_2));
            mBankAccountIAV.getTvLeft().setText(getString(R.string.P3_A1_2_b_1));
            mBankAccountIAV.setSubText(getString(R.string.CCA03_02_4));
            mBochkBankAccountIAV.getTvLeft().setText(getString(R.string.BD1_1_5));
            mBochkBankAccountIAV.setSubText(getString(R.string.CCA03_02_6));
        }
    }

    /**
     * 我的账户级别 根据不同的账户类型 显示支持不同的绑卡方式
     */
    private void getSmartLevel() {
        mPresenter.getSmartLevelForBind();
    }

    /**
     * 设置标题
     */
    private void initTitleByType() {
        int titleRes = R.string.string_sel_bind;
        if (mCurrentPageFlow == Constants.PAGE_FLOW_REGISTER) {
            setToolBarTitle(R.string.IDV_2_1);
        } else if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            setToolBarTitle(R.string.bind_title);
        }
        mTypeTitleTV.setText(titleRes);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_sel_register_type;
    }

    /**
     * piwd 智障人士 - 显示拒纳信息
     */
    private void showPWidRefuseMsg() {
        showErrorMsgDialog(getActivity(), getString(R.string.PWID2109_1_2), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                EventBus.getDefault().post(new PWidEventEntity(PWidEventEntity.EVENT_HOME_PAGE, "pwid拒纳信息返回主页"));
                finish();
            }
        });
    }


    @OnClick({R.id.id_link_credit_card, R.id.id_link_bank_account, R.id.iav_bochk_bankaccount})
    public void onViewClicked(View view) {
        if (!ButtonUtils.isFastDoubleClick()) {
            switch (view.getId()) {
                case R.id.id_link_credit_card:
                    if (checkBoxPWid.isChecked()) {
                        //选中pwid时，显示拒纳信息
                        showPWidRefuseMsg();
                        return;
                    }
                    //信用卡绑定
                    if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD && CacheManagerInstance.getInstance().isLogin()) {
                        mPresenter.checkCreditCardNum();
                    } else {
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentPageFlow);
                        ActivitySkipUtil.startAnotherActivity(this, InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }

                    break;
                case R.id.id_link_bank_account:
                    //智能账户注册绑卡
                    if (checkBoxPWid.isChecked()) {
                        //选中pwid时，显示拒纳信息
                        showPWidRefuseMsg();
                        return;
                    }
                    if (mCurrentPageFlow == Constants.PAGE_FLOW_REGISTER) {
                        HashMap<String, Object> maps = new HashMap<>();
                        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                        RegisterSmartAccountDescActivity.startActivity(getActivity(), maps);
                    } else if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
                        HashMap<String, Object> maps = new HashMap<>();
                        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                        RegisterSmartAccountDescActivity.startActivity(getActivity(), maps);
                    }
                    break;
                case R.id.iav_bochk_bankaccount:
                    if (checkBoxPWid.isChecked()) {
                        //选中pwid时，显示拒纳信息
                        showPWidRefuseMsg();
                        return;
                    }
                    if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
                        //s2账户绑卡
                        HashMap<String, Object> mHashMapsBind = new HashMap<>();
                        mHashMapsBind.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_PA);
                        //在这里 绑定pa需要手机号 来判断是否有税务编号 因为涉及到很多个地方都有绑卡的入口 所以这里但是所有的绑卡都会走到这个界面 所以统一在这里获取手机号
                        List<BankCardEntity> cardEntities = CacheManagerInstance.getInstance().getCardEntities();
                        if (cardEntities != null && !cardEntities.isEmpty()) {
                            //86-*******2541
                            String phoneNumber = cardEntities.get(0).getPhone();
                            if (!TextUtils.isEmpty(phoneNumber)) {
                                //绑卡操作 绑s2的时候需要手机号 税务编号的界面需要根据手机号来判断税务类型
                                String items[] = phoneNumber.split("-");
                                mHashMapsBind.put(Constants.EXTRA_COUNTRY_CODE, items[0]);
                                mHashMapsBind.put(Constants.DATA_PHONE_NUMBER, items[1]);
                            }
                        }
                        ActivitySkipUtil.startAnotherActivity(SelRegisterTypeActivity.this, RegisterPaymentAccountDescActivity.class, mHashMapsBind, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                default:
                    break;
            }
        }
    }


    @Override
    public void getSmartLevelForBindFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void getSmartLevelForBindSuccess(SmartLevelEntity response) {
        String currentAccountLevel = response.getSmartAccLevel();
        if (currentAccountLevel.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            //支付账户 s2
            cadPaymentAccount.setVisibility(View.GONE);
            cadBankAccount.setVisibility(View.GONE);
            cadCreditCard.setVisibility(View.VISIBLE);
        } else if (currentAccountLevel.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
            //我的账户 s3
            cadPaymentAccount.setVisibility(View.GONE);
            cadBankAccount.setVisibility(View.GONE);
            cadCreditCard.setVisibility(View.VISIBLE);
        } else if (currentAccountLevel.equals("0")) {
            //信用卡用户
            cadPaymentAccount.setVisibility(View.VISIBLE);
            cadBankAccount.setVisibility(View.VISIBLE);
            cadCreditCard.setVisibility(View.VISIBLE);
        } else {
            cadBankAccount.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void checkCreditCardNumFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void checkCreditCardNumSuccess(RegSucBySmartEntity response) {
        if ("Y".equals(response.getHaveVCCard())) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentPageFlow);
            ActivitySkipUtil.startAnotherActivity(this, InputOrBindNewCardActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentPageFlow);
            ActivitySkipUtil.startAnotherActivity(this, InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mCurrentPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            //退出需求清除相关app call app流程
            if (UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class) != null) {
                UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
            }
        }
    }
}
