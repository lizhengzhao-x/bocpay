package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.text.TextUtils;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountDrawDetailContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.SmartAccountDrawDetailPresenter;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpSuccessActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;


public class SmartAccountWithDrawDetailActivity extends BaseCompatActivity<SmartAccountDrawDetailContract.Presenter> implements SmartAccountDrawDetailContract.View {
    @BindView(R.id.id_withdraw_account)
    TextView idWithdrawAccount;
    @BindView(R.id.id_currency)
    TextView idCurrency;
    @BindView(R.id.id_money)
    TextView idMoney;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    private boolean isFirstWithDraw;
    private String currency, money, paymentAccount, smartLevel;


    @Override
    public void init() {
        setToolBarTitle(R.string.P3_ewa_01_039);
        currency = getIntent().getExtras().getString(Constants.CURRENCY);
        money = getIntent().getExtras().getString(Constants.MONEY,"");
        paymentAccount = getIntent().getExtras().getString(Constants.PAYMENT_ACCOUNT);
        isFirstWithDraw = getIntent().getExtras().getBoolean(Constants.IS_FIRST_WITHDRAW);
        smartLevel = getIntent().getExtras().getString(Constants.LAVEL);
        String accType = getIntent().getExtras().getString(Constants.ACCTYPE);
        idCurrency.setText(currency);

        String amount = AndroidUtils.formatPriceWithPoint(Double.parseDouble(money), true);
        idMoney.setText(amount);
        if (TextUtils.isEmpty(accType)) {
            idWithdrawAccount.setText("(" + AndroidUtils.subPaymentMethod(paymentAccount) + ")");
        } else {
            idWithdrawAccount.setText(accType + "(" + AndroidUtils.subPaymentMethod(paymentAccount) + ")");
        }
        EventBus.getDefault().register(this);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_withdraw_deatil;
    }

    @Override
    protected SmartAccountDrawDetailContract.Presenter createPresenter() {
        return new SmartAccountDrawDetailPresenter();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @OnClick(R.id.tv_confirm)
    public void onViewClicked() {
        if (ButtonUtils.isFastDoubleClick()) return;
        String action="";
        if (smartLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            action="";
        }else {
            //S3 验密需要传action="W"
            action="W";
        }
        VerifyPasswordCommonActivity.startActivityForActionResult(getActivity(),action, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (smartLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
                    mPresenter.preCheckTopUpWithSmartAccountTwo( money);
                } else {
                    mPresenter.preCheckTopUpWithSmartAccountThree(money);
                }

            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

    }


    @Override
    public void preCheckTopUpWithSmartAccountTwoFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountWithDrawDetailActivity.this,errorMsg);
    }

    @Override
    public void preCheckTopUpWithSmartAccountTwoSuccess( STwoTopUpEntity response) {
        LogUtils.d("TIME","11=="+System.currentTimeMillis());
        if (isFirstWithDraw) {
            dismissDialog();
            firstWithDrawEvent(response.getScrRefNo());
        } else {
            mPresenter.topUpWithSmartAccountTwo( money, response.getScrRefNo());
        }
    }

    @Override
    public void preCheckTopUpWithSmartAccountThreeFail( String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountWithDrawDetailActivity.this,errorMsg);
    }

    @Override
    public void preCheckTopUpWithSmartAccountThreeSuccess( SThreeTopUpEntity response) {
        mPresenter.topUpWithSmartAccountThree(money, response.getOrderNo());
    }

    @Override
    public void topUpWithSmartAccountTwoFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountWithDrawDetailActivity.this,errorMsg);
    }

    @Override
    public void topUpWithSmartAccountTwoSuccess(STwoTopUpEntity response) {
        LogUtils.d("TIME","222=="+System.currentTimeMillis());
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.PAYMENT_METHOD, AndroidUtils.subPaymentMethod(paymentAccount));
        mHashMaps.put(Constants.TYPE, Constants.TYPE_WITHDREA);
        mHashMaps.put(Constants.CURRENCY, idCurrency.getText().toString());
        mHashMaps.put(Constants.MONEY, BigDecimalFormatUtils.forMatWithDigs(money, 2));
        mHashMaps.put(Constants.RECORD_NUMBER, response.getScrRefNo());
        mHashMaps.put(Constants.ACCTYPE, getIntent().getExtras().getString(Constants.ACCTYPE));
        ActivitySkipUtil.startAnotherActivity(SmartAccountWithDrawDetailActivity.this, TopUpSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        LogUtils.d("TIME","333=="+System.currentTimeMillis());
        finish();
    }

    @Override
    public void topUpWithSmartAccountThreeFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountWithDrawDetailActivity.this,errorMsg);
    }

    @Override
    public void topUpWithSmartAccountThreeSuccess(TopUpResponseEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.PAYMENT_METHOD, AndroidUtils.subPaymentMethod(paymentAccount));
        mHashMaps.put(Constants.TYPE, Constants.TYPE_WITHDREA);
        mHashMaps.put(Constants.CURRENCY, idCurrency.getText().toString());
        mHashMaps.put(Constants.MONEY, BigDecimalFormatUtils.forMatWithDigs(money, 2));
        mHashMaps.put(Constants.RECORD_NUMBER, response.getTrxNo());
        mHashMaps.put(Constants.ACCTYPE, getIntent().getExtras().getString(Constants.ACCTYPE));
        ActivitySkipUtil.startAnotherActivity(SmartAccountWithDrawDetailActivity.this, TopUpSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    private void firstWithDrawEvent(String referenceNo) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_D);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_WITHDRAW);
        mHashMapsLogin.put(Constants.PAYMENT_METHOD, AndroidUtils.subPaymentMethod(paymentAccount));
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
        mHashMapsLogin.put(Constants.ACCTYPE, getIntent().getExtras().getString(Constants.ACCTYPE));
        mHashMapsLogin.put(Constants.MONEY, money);
        mHashMapsLogin.put(Constants.REFERENCENO, referenceNo);
        ActivitySkipUtil.startAnotherActivity(SmartAccountWithDrawDetailActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

}
