package cn.swiftpass.wallet.intl.module.register;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.ATMTipDialog;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountInfoConfirmActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountInfoSetActivity;
import cn.swiftpass.wallet.intl.module.register.contract.BankAccountOrCardContact;
import cn.swiftpass.wallet.intl.module.register.presenter.BankAccountOrCardPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardBindTermsActivity;
import cn.swiftpass.wallet.intl.sdk.safekeyboard.SafeKeyboard;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;

import static cn.swiftpass.wallet.intl.module.virtualcard.utils.NormalInputFilter.CHARSEQUENCE_BANK_SPECIAL;

/**
 * 银行账号登录界面
 */

public class RegisterBankActivity extends BaseCompatActivity<BankAccountOrCardContact.Presenter> implements BankAccountOrCardContact.View {
    public static final String TAG = RegisterBankActivity.class.getSimpleName();
    public static final String LOGIN_TYPE_ONLINE = "1";
    public static final String LOGIN_TYPE_OFFLINE = "2";

    public static final int BANK_NUM_LEN_MAX = 19;
    public static final int BANK_NUM_LEN_MIN = 16;
    public static final int OFF_LINE_PASS_LEN = 6;

    public static String TAG_OFFLINE_ACCOUNT = "offline_account";
    public static String TAG_OFFLINE_PASS = "offline_pass";
    public static String TAG_ONLINE_ACCOUNT = "online_account";
    public static String TAG_ONLINE_PASS = "offline_account";
    public static String TAG_VERIFY_CODE = "verify_code";
    /**
     * 未开通我的账户 (走开通我的账户流程)
     */
    public static final String REG_TYPE_NO_REG = "nosmartreg";
    /**
     * 存在我的账户并已绑定（退出绑定，走登陆流程）
     */
    public static final String REG_TYPE_EXIST_BIND = "hpaysmartreg";
    /**
     * 存在我的账户未绑定BOCPay（走绑定我的账户流程）
     */
    public static final String REG_TYPE_EXIST_UNBIND = "nobocpayreg";

    public static final String REG_TYPE_NO_SAME = "saccnosameman";


    /**
     * 未开通我的账户 (走开通我的账户流程)
     */
    public static final String BIND_TYPE_NO_REG = "nosmartbind";
    /**
     * 存在我的账户并已绑定（退出绑定，走登陆流程）
     */
    public static final String BIND_TYPE_EXIST_BIND = "hpaysmartbind";
    /**
     * 存在我的账户未绑定BOCPay（走绑定我的账户流程）
     */
    public static final String BIND_TYPE_EXIST_UNBIND = "nobocpaybind";
    //账户拥有人与注册钱包的用户不是同一个人
    public static final String BIND_TYPE_NO_SAME = "saccnosameman";


    @BindView(R.id.id_online_account)
    TextView idOnlineAccount;
    @BindView(R.id.id_offline_account)
    TextView idOfflineAccount;
    @BindView(R.id.id_switch_view)
    LinearLayout idSwitchView;
    @BindView(R.id.id_et_online_account)
    CustomTvEditText idEtOnlineAccount;
    @BindView(R.id.id_et_online_password)
    CustomTvEditText idEtOnlinePassword;
    @BindView(R.id.id_et_offline_account)
    CustomTvEditText idEtOfflineAccount;
    @BindView(R.id.id_et_offline_password)
    CustomTvEditText idEtOfflinePassword;
    @BindView(R.id.cet_verify_code)
    CustomTvEditText mVerifyCodeET;
    @BindView(R.id.id_virtualcard_register_view)
    View mVirtualCardTitle;
    @BindView(R.id.id_bankaccount_topimage)
    ImageView mSmartAccountTitle;
    @BindView(R.id.tv_login)
    TextView tvLongin;
    @BindView(R.id.iv_tip_mark)
    ImageView mTipMarkIV;
    @BindView(R.id.id_root_view)
    LinearLayout mRootView;
    @BindView(R.id.iv_verify_code)
    ImageView mVerifyCodeIV;

    @BindView(R.id.iv_refresh_code)
    ImageView mRefreshCodeIV;


    private boolean mOnlineViewShow = true;

    private boolean mOnlineEyeShow = false;

    private boolean mOfflineEyeShow = false;

    private SafeKeyboard safeKeyboardLeft;
    //V：注册流程  B：绑定流程  F：忘记密码流程 L:登陆流程(登录不会跳转这个页面)
    private String mLoginAction = ApiConstant.FLOW_REGISTER;
    //业务类型
    private int mTypeFlag;

    private CustomTvEditText.OnEditTextChangeListener mEditTextChangeListener = new CustomTvEditText.OnEditTextChangeListener() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            updateLoginBtn();
        }
    };
    private boolean isVerifyCodeUse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_bank;
    }

    @Override
    protected BankAccountOrCardContact.Presenter createPresenter() {
        return new BankAccountOrCardPresenter();
    }

    @Override
    public void init() {

        Intent intent = getIntent();
        if (null != intent && null != getIntent().getExtras()) {
            mTypeFlag = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
            mLoginAction = AndroidUtils.getFlowFromType(mTypeFlag);
        }


        if (mTypeFlag == Constants.PAGE_FLOW_FORGETPASSWORD) {
            //重置密码
            setToolBarTitle(R.string.setting_payment_fgw);
        } else if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
            //升级
            setToolBarTitle(R.string.P3_ewa_01_028);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册
            setToolBarTitle(getString(R.string.VC02_01a_1));
            mVirtualCardTitle.setVisibility(View.VISIBLE);
            mSmartAccountTitle.setVisibility(View.GONE);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡忘记密码
            setToolBarTitle(getString(R.string.VC07_03_1));
            mVirtualCardTitle.setVisibility(View.VISIBLE);
            mSmartAccountTitle.setVisibility(View.GONE);
        } else {
            //PAGE_FLOW_REGISTER
            setToolBarTitle(R.string.bank_of_china);
        }

        idEtOnlinePassword.hidePasswordText();
        idEtOfflinePassword.hidePasswordText();
        idEtOnlineAccount.setOnEditTextChangeListener(mEditTextChangeListener);
        idEtOnlinePassword.setOnEditTextChangeListener(mEditTextChangeListener);
        idEtOfflineAccount.setOnEditTextChangeListener(mEditTextChangeListener);
        idEtOfflinePassword.setOnEditTextChangeListener(mEditTextChangeListener);
        mVerifyCodeET.setOnEditTextChangeListener(mEditTextChangeListener);
//        idEtOnlineAccount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AndroidUtils.showKeyboard(RegisterBankActivity.this,idEtOnlineAccount.getEditText());
//            }
//        });
//        idEtOfflineAccount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AndroidUtils.showKeyboard(RegisterBankActivity.this,idEtOfflineAccount.getEditText());
//            }
//        });

        idEtOfflineAccount.setInputType(InputType.TYPE_CLASS_NUMBER);
        idEtOfflinePassword.setInputType(InputType.TYPE_CLASS_NUMBER);
        idEtOfflinePassword.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
        idEtOnlinePassword.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
        //网上银行设置限制
        idEtOnlineAccount.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(16), new NormalInputFilter(CHARSEQUENCE_BANK_SPECIAL)});
        mVerifyCodeET.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        mVerifyCodeET.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
        initEditorActionListener();
        updateLoginBtn();
        getVerifyCode();
        initKeyBoard();
        idEtOfflineAccount.getEditText().setFocusableInTouchMode(true);
        idEtOnlineAccount.getEditText().setFocusableInTouchMode(true);
        idEtOnlineAccount.setFocusable(true);
        idEtOnlineAccount.requestFocus();
//        AndroidUtils.showKeyboard(this,idEtOnlineAccount.getEditText());
        AndroidUtils.showKeyboardDelay(idEtOnlineAccount.getEditText());

    }


    @Override
    protected void onStart() {
        super.onStart();
        if (isVerifyCodeUse) {
            getVerifyCode();
            isVerifyCodeUse = false;
        }
    }

    @Override
    protected boolean isAutoUseHideSoftInput() {
        return false;
    }

    private void initKeyBoard() {
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_keyboard_containor, null);
        safeKeyboardLeft = new SafeKeyboard(getApplicationContext(), keyboardContainer, idEtOnlinePassword.getEditText(), R.layout.layout_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId());
        safeKeyboardLeft.setDelDrawable(this.getResources().getDrawable(R.drawable.icon_del));
        safeKeyboardLeft.setLowDrawable(this.getResources().getDrawable(R.drawable.icon_capital_default));
        safeKeyboardLeft.setUpDrawable(this.getResources().getDrawable(R.drawable.icon_capital_selected));
        safeKeyboardLeft.setOnEditTextFocusChangeListener(new SafeKeyboard.OnEditTextFocusChangeListener() {
            @Override
            public void onFocusChanged(boolean focus) {
                //需要特殊处理 焦点事件
                if (mOnlineViewShow) {
                    idEtOnlinePassword.changeFocusUiStatus(focus);
                } else {
                    idEtOfflinePassword.changeFocusUiStatus(focus);
                }
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //软键盘弹出 用户手势滑动的时候 隐藏软键盘
                if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()
                        && !AndroidUtils.inRangeOfView(safeKeyboardLeft.getKeyboardView(), ev)
                        && !AndroidUtils.inRangeOfView(idEtOnlinePassword, ev)) {
                    safeKeyboardLeft.hideKeyboard();
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
            safeKeyboardLeft.hideKeyboard();
            safeKeyboardLeft = null;
        }
    }

    private void initEditorActionListener() {
        TextView.OnEditorActionListener listener = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                ////让获取输入焦点
                if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_DONE || (event != null && event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    String tag = (String) v.getTag();
                    boolean flag = false;
                    if (TextUtils.equals(tag, TAG_ONLINE_ACCOUNT)) {
                        idEtOnlineAccount.clearFocus();
                        idEtOnlinePassword.getEditText().requestFocus();
                        flag = true;
                    } else if (TextUtils.equals(tag, TAG_ONLINE_PASS)) {
                        idEtOnlinePassword.clearFocus();
                        mVerifyCodeET.getEditText().requestFocus();
                        flag = true;
                    } else if (TextUtils.equals(tag, TAG_OFFLINE_ACCOUNT)) {
                        idEtOfflineAccount.clearFocus();
                        idEtOfflinePassword.getEditText().requestFocus();
                        flag = true;
                    } else if (TextUtils.equals(tag, TAG_OFFLINE_PASS)) {
                        idEtOfflinePassword.clearFocus();
                        mVerifyCodeET.getEditText().requestFocus();
                        flag = true;
                    } else if (TextUtils.equals(tag, TAG_VERIFY_CODE)) {
                        AndroidUtils.hideKeyboard(mVerifyCodeET.getEditText());
                        flag = true;
                    }
                    return flag;
                }
                return false;
            }
        };
        idEtOnlineAccount.getEditText().setOnEditorActionListener(listener);
        idEtOnlineAccount.getEditText().setTag(TAG_ONLINE_ACCOUNT);

        idEtOnlinePassword.getEditText().setOnEditorActionListener(listener);
        idEtOnlinePassword.getEditText().setTag(TAG_ONLINE_PASS);

        idEtOfflineAccount.getEditText().setOnEditorActionListener(listener);
        idEtOfflineAccount.getEditText().setTag(TAG_OFFLINE_ACCOUNT);

        idEtOfflinePassword.getEditText().setOnEditorActionListener(listener);
        idEtOfflinePassword.getEditText().setTag(TAG_OFFLINE_PASS);

        mVerifyCodeET.getEditText().setOnEditorActionListener(listener);
        mVerifyCodeET.getEditText().setTag(TAG_VERIFY_CODE);

//        mRootView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
//                    safeKeyboardLeft.hideKeyboard();
//                }
//                AndroidUtils.hideKeyboard(mRootView);
//            }
//        });
    }

    private void getVerifyCode() {

        if (mTypeFlag == Constants.PAGE_FLOW_FORGETPASSWORD || mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            getVerifyCodeForLogin();
        } else if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
            getVerifyCodeForUpdate();
        } else {
            getVerifyCodeForLogin();
        }
    }

    private void getVerifyCodeForLogin() {
        mRefreshCodeIV.setEnabled(false);
        mPresenter.getLoginVerifyCode(mLoginAction);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //自定义安全键盘跳动
        if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
            safeKeyboardLeft.hideKeyboard();
            if (mOnlineViewShow) {
                idEtOnlineAccount.getEditText().requestFocus();
                if (idEtOnlineAccount.getInputText().length() > 0) {
                    idEtOnlineAccount.getEditText().setSelection(idEtOnlineAccount.getInputText().length());
                }
            } else {
                idEtOfflineAccount.getEditText().requestFocus();
                if (idEtOfflineAccount.getInputText().length() > 0) {
                    idEtOfflineAccount.getEditText().setSelection(idEtOfflineAccount.getInputText().length());
                }
            }
        }
        tvLongin.setEnabled(true);
    }

    private void getVerifyCodeForUpdate() {
        mRefreshCodeIV.setEnabled(false);
        mPresenter.getUpdateAccountVerifyCode(mLoginAction);
    }


    @OnClick({R.id.id_online_account, R.id.id_offline_account, R.id.tv_login, R.id.iv_tip_mark, R.id.iv_refresh_code})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) return;
        switch (view.getId()) {
            case R.id.id_online_account:
                switchTab(true);
                safeKeyboardLeft.changeEditText(idEtOnlinePassword.getEditText(), 1);
                break;
            case R.id.id_offline_account:
                switchTab(false);
                safeKeyboardLeft.changeEditText(idEtOfflinePassword.getEditText(), 3);
                break;
            case R.id.tv_login:
                doLogin();
                break;
            case R.id.iv_tip_mark:
                onClickBankTip();
                break;
            case R.id.iv_refresh_code:
                getVerifyCode();
                break;
            default:
                break;
        }
    }

    private void onClickBankTip() {
        new ATMTipDialog.Builder(RegisterBankActivity.this).create().show();
    }

    public void doLogin() {
        String acc;
        String pass;
        String type;
        String verifyCode;
        if (mOnlineViewShow) {
            acc = idEtOnlineAccount.getInputText();
            pass = idEtOnlinePassword.getInputText();
            type = LOGIN_TYPE_ONLINE;
        } else {
            acc = idEtOfflineAccount.getInputText();
            pass = idEtOfflinePassword.getInputText();
            type = LOGIN_TYPE_OFFLINE;
            if (acc.length() != BANK_NUM_LEN_MAX && acc.length() != BANK_NUM_LEN_MIN) {
                String msg = getString(R.string.incorrect_card_num);
                AndroidUtils.showTipDialog(this, msg);
                return;
            }
        }

        verifyCode = mVerifyCodeET.getInputText();
        //防止多个接口调用dialog show diss 之间 用户多次点击
        tvLongin.setEnabled(false);
        if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
            //升级
            updateSmartAccount(acc, pass, mLoginAction, type, verifyCode);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册
            registerVirtualAccount(acc, pass, mLoginAction, type, verifyCode);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡忘记密码
            forgetPwdVirtualAccount(acc, pass, mLoginAction, type, verifyCode);
        } else {
            registerSmartAccount(acc, pass, mLoginAction, type, verifyCode);
        }
    }

    private VirtualcardRegisterVerifyEntity parseExtraData(String acc, String pass, String mLoginAction, String type, String verifyCode) {
        VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntityIn = (VirtualcardRegisterVerifyEntity) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDREGISTERVERIFINVO);
        virtualcardRegisterVerifyEntityIn.setAccNo(acc);
        virtualcardRegisterVerifyEntityIn.setPassword(pass);
        virtualcardRegisterVerifyEntityIn.setAction(mLoginAction);
        virtualcardRegisterVerifyEntityIn.setLoginType(type.equals(LOGIN_TYPE_ONLINE) ? "1" : "2");
        virtualcardRegisterVerifyEntityIn.setEaiAction("B");
        virtualcardRegisterVerifyEntityIn.setVerifyCode(verifyCode);
        return virtualcardRegisterVerifyEntityIn;
    }

    private void forgetPwdVirtualAccount(String acc, String pass, String mLoginAction, String type, String verifyCode) {
        mPresenter.virtualCardForgetPwd(parseExtraData(acc, pass, mLoginAction, type, verifyCode));
    }

    private void registerVirtualAccount(String acc, String pass, String mLoginAction, String type, String verifyCode) {
        mPresenter.virtualCardSmartAccountRegister(parseExtraData(acc, pass, mLoginAction, type, verifyCode));
    }

    private void registerSmartAccount(String acc, String pass, String mLoginAction, String type, String verifyCode) {
        mPresenter.bankLogin(acc, pass, mLoginAction, type, verifyCode);
    }

    /**
     * 只能账户升级
     */
    private void updateSmartAccount(String acc, String pass, String mLoginAction, String type, String verifyCode) {
        mPresenter.sTwoUpdateAccount(acc, pass, type, mLoginAction, verifyCode);
    }

    private void parseVirtualCardForgetPasswordResult(VirtualCardListEntity virtualCardListEntity) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        mHashMaps.put(Constants.VIRTUALCARDLIST, virtualCardListEntity);
        mHashMaps.put(Constants.FORGET_PASSWORD_TYPE, "2");
        ActivitySkipUtil.startAnotherActivity(this, VirtualCardBindTermsActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        tvLongin.setEnabled(true);
    }

    private void parseVirtualCardRegisterResult(VirtualCardListEntity response) {
        Intent intent = new Intent(RegisterBankActivity.this, VirtualCardBindTermsActivity.class);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        intent.putExtra(Constants.VIRTUALCARDLIST, response);
        startActivity(intent);
        tvLongin.setEnabled(true);
    }


    private void parseLoginResult(BankLoginResultEntity response) {
        if (mTypeFlag == Constants.PAGE_FLOW_BIND_CARD) {
            doBinding(response);
        } else if (mTypeFlag == Constants.PAGE_FLOW_FORGETPASSWORD) {
            doFotgotPasscode(response);
        } else if (mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
            doRegister(response);
        } else if (mTypeFlag == Constants.SMART_ACCOUNT_UPDATE) {
            doUpdateAccount(response);
        }
    }

    private void doUpdateAccount(BankLoginResultEntity response) {
        if (null == response) {
            return;
        }
        Intent intent = new Intent(RegisterBankActivity.this, SmartAccountInfoSetActivity.class);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        intent.putExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, response);
        startActivity(intent);
        tvLongin.setEnabled(true);
    }

    //走注册流程
    private void doRegister(BankLoginResultEntity response) {
        if (null == response) {
            return;
        }
        if (REG_TYPE_NO_REG.equalsIgnoreCase(response.getType())) {
            doRegisterWithRegSmart(response);
        } else if (REG_TYPE_EXIST_BIND.equalsIgnoreCase(response.getType())) {
            doRegisterExistBoc(response);
        } else if (REG_TYPE_EXIST_UNBIND.equalsIgnoreCase(response.getType())) {
            doRegisterExistSmart(response);
        }
        tvLongin.setEnabled(true);
    }


    //走绑定流程
    private void doBinding(BankLoginResultEntity response) {
        if (null == response) {
            return;
        }
        if (BIND_TYPE_NO_REG.equalsIgnoreCase(response.getType())) {
            doRegisterWithRegSmart(response);
        } else if (BIND_TYPE_EXIST_BIND.equalsIgnoreCase(response.getType())) {
            AndroidUtils.showTipDialog(this, R.string.tip_already_link);
        } else if (BIND_TYPE_EXIST_UNBIND.equalsIgnoreCase(response.getType())) {
            doRegisterExistSmart(response);
        } else if (BIND_TYPE_NO_SAME.equalsIgnoreCase(response.getType())) {
            AndroidUtils.showTipDialog(this, R.string.need_same_account);
        }
        tvLongin.setEnabled(true);
    }

    //走忘记密码流程
    private void doFotgotPasscode(BankLoginResultEntity response) {
        if (null == response) {
            return;
        }
        if (REG_TYPE_NO_REG.equalsIgnoreCase(response.getType())) {
            doRegisterWithRegSmart(response);
        } else if (REG_TYPE_EXIST_BIND.equalsIgnoreCase(response.getType())) {
            doRegisterExistSmart(response);
        } else if (REG_TYPE_EXIST_UNBIND.equalsIgnoreCase(response.getType())) {
            doRegisterExistSmart(response);
        } else if (REG_TYPE_NO_SAME.equalsIgnoreCase(response.getType())) {
            //只有登录时才会返回这种类型
            AndroidUtils.showTipDialog(this, R.string.need_same_account);
            tvLongin.setEnabled(false);
        }
    }

    private void switchTab(boolean onlineViewShow) {
        if (mOnlineViewShow != onlineViewShow) {
            mOnlineViewShow = onlineViewShow;
            if (mOnlineViewShow) {
                showOnlineLoginView();
            } else {
                showOfflineLoginView();
            }
            getVerifyCode();
        }
    }

    /**
     * 显示左边登录页面
     */
    private void showOnlineLoginView() {
        idEtOnlineAccount.getEditText().requestFocus();
        AndroidUtils.showKeyboard(this,idEtOnlineAccount.getEditText());
        idEtOnlineAccount.setVisibility(View.VISIBLE);
        idEtOnlinePassword.setVisibility(View.VISIBLE);
        idEtOfflineAccount.setVisibility(View.GONE);
        idEtOfflinePassword.setVisibility(View.GONE);
        mTipMarkIV.setVisibility(View.GONE);
        idOnlineAccount.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
        idOfflineAccount.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        idOnlineAccount.setTextColor(getResources().getColor(R.color.app_white));
        idOfflineAccount.setTextColor(getResources().getColor(R.color.font_gray_three));
        idEtOnlineAccount.setContentText("");
        idEtOnlinePassword.setContentText("");
        mVerifyCodeET.setContentText("");
        updateLoginBtn();
    }

    /**
     * 显示右边登录页面
     */
    private void showOfflineLoginView() {
        idEtOfflineAccount.requestFocus();
        AndroidUtils.showKeyboard(this,idEtOfflineAccount.getEditText());
        idEtOnlineAccount.setVisibility(View.GONE);
        idEtOnlinePassword.setVisibility(View.GONE);
        idEtOfflineAccount.setVisibility(View.VISIBLE);
        idEtOfflinePassword.setVisibility(View.VISIBLE);
        mTipMarkIV.setVisibility(View.VISIBLE);
        idOfflineAccount.setBackgroundResource(R.drawable.right_china_btn_bac);
        idOnlineAccount.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        idOfflineAccount.setTextColor(getResources().getColor(R.color.app_white));
        idOnlineAccount.setTextColor(getResources().getColor(R.color.font_gray_three));
        idEtOfflineAccount.setContentText("");
        idEtOfflinePassword.setContentText("");
        mVerifyCodeET.setContentText("");
        updateLoginBtn();
    }

    private void updateLoginBtn() {
        boolean flag = false;
        String acc;
        String pass;
        String verifyCode;
        if (mOnlineViewShow) {
            acc = idEtOnlineAccount.getInputText();
            pass = idEtOnlinePassword.getInputText();
        } else {
            acc = idEtOfflineAccount.getInputText();
            pass = idEtOfflinePassword.getInputText();
        }
        verifyCode = mVerifyCodeET.getInputText();

        if (!TextUtils.isEmpty(acc) && !TextUtils.isEmpty(pass) && !TextUtils.isEmpty(verifyCode)) {
            flag = true;
            if (!mOnlineViewShow) {
                if (acc.length() < BANK_NUM_LEN_MIN || pass.length() != OFF_LINE_PASS_LEN) {
                    flag = false;
                }
            }
        }
        tvLongin.setEnabled(flag);
        tvLongin.setBackgroundResource(flag ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    //已经存在bocpay
    private void doRegisterExistBoc(BankLoginResultEntity response) {
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        //builder.setTitle(R.string.tip_signed_up);
        builder.setMessage(R.string.tip_already_link);
        builder.setPositiveButton(R.string.button_login, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                MyActivityManager.removeAllTaskExcludeMainStackForRegister();
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        if (!isFinishing()) {
            CustomDialog dialog = builder.create();
            dialog.show();
        }
    }

    //需要注册SmartAccount
    private void doRegisterWithRegSmart(BankLoginResultEntity response) {
        Intent intent = new Intent(RegisterBankActivity.this, SmartAccountInfoSetActivity.class);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        intent.putExtra(Constants.USE_BIOMETRICAUTH, response.isOpenBiometricAuth());
        intent.putExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, response);
        intent.putExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, response);
        startActivity(intent);
        tvLongin.setEnabled(true);
    }

    //已经存在SmartAccount
    private void doRegisterExistSmart(BankLoginResultEntity response) {
        SmartAccountInfoConfirmActivity.startActivity(this, mTypeFlag, response);
        tvLongin.setEnabled(true);
    }

    @Override
    public void getLoginVerifyCodeFailed(String errorCode, String errorMsg) {
        mRefreshCodeIV.setEnabled(true);
        AndroidUtils.showTipDialog(RegisterBankActivity.this, errorMsg);
    }

    @Override
    public void getLoginVerifyCodeSuccess(BankLoginVerifyCodeEntity response) {
        mRefreshCodeIV.setEnabled(true);
        int codeLen = 8;
        try {
            codeLen = Integer.parseInt(response.getVerifyCodeLength());
            String bmpCode = response.getImageCode();
            Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
            mVerifyCodeIV.setImageBitmap(bmp);
            mVerifyCodeET.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
        } catch (Exception e) {

        }
    }

    @Override
    public void getUpdateAccountVerifyCodeFailed(String errorCode, String errorMsg) {
        mRefreshCodeIV.setEnabled(true);
        AndroidUtils.showTipDialog(RegisterBankActivity.this, errorMsg);
    }

    @Override
    public void getUpdateAccountVerifyCodeSuccess(BankLoginVerifyCodeEntity response) {
        mRefreshCodeIV.setEnabled(true);
        int codeLen = 8;
        try {
            codeLen = Integer.parseInt(response.getVerifyCodeLength());
            String bmpCode = response.getImageCode();
            Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
            mVerifyCodeIV.setImageBitmap(bmp);
            mVerifyCodeET.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
        } catch (Exception e) {

        }
    }

    @Override
    public void virtualCardForgetPwdFailed(String errorCode, String errorMsg) {
        tvLongin.setEnabled(true);
        AndroidUtils.showTipDialog(RegisterBankActivity.this, errorMsg);
        LogUtils.i(TAG, errorMsg);
        mVerifyCodeET.setContentText("");
        if (!NetworkUtil.isNetworkAvailable()) return;
        getVerifyCode();
    }

    @Override
    public void virtualCardForgetPwdSuccess(VirtualCardListEntity response) {
        mVerifyCodeET.setContentText("");
        isVerifyCodeUse = true;
        parseVirtualCardForgetPasswordResult(response);
    }

    @Override
    public void virtualCardSmartAccountRegisterFailed(String errorCode, String errorMsg) {
        tvLongin.setEnabled(true);
        AndroidUtils.showTipDialog(RegisterBankActivity.this, errorMsg);
        LogUtils.i(TAG, errorMsg);
        mVerifyCodeET.setContentText("");
        if (!NetworkUtil.isNetworkAvailable()) return;
        getVerifyCode();
    }

    @Override
    public void virtualCardSmartAccountRegisterSuccess(VirtualCardListEntity response) {
        mVerifyCodeET.setContentText("");
        isVerifyCodeUse = true;
        parseVirtualCardRegisterResult(response);
    }

    @Override
    public void bankLoginFailed(String errorCode, String errorMsg) {
        tvLongin.setEnabled(true);
        showErrorMsgDialog(mContext, errorMsg);
        LogUtils.i(TAG, errorMsg);
        mVerifyCodeET.setContentText("");
        if (!NetworkUtil.isNetworkAvailable()) return;
        getVerifyCode();
    }

    @Override
    public void bankLoginSuccess(BankLoginResultEntity response) {
        mVerifyCodeET.setContentText("");
        isVerifyCodeUse = true;
        parseLoginResult(response);
    }

    @Override
    public void sTwoUpdateAccountFailed(String errorCode, String errorMsg) {
        tvLongin.setEnabled(true);
        showErrorMsgDialog(RegisterBankActivity.this, errorMsg);
        mVerifyCodeET.setContentText("");
        if (!NetworkUtil.isNetworkAvailable()) return;
        getVerifyCode();
    }

    @Override
    public void sTwoUpdateAccountSuccess(BankLoginResultEntity response) {
        mVerifyCodeET.setContentText("");
        isVerifyCodeUse = true;
        parseLoginResult(response);
    }
}
