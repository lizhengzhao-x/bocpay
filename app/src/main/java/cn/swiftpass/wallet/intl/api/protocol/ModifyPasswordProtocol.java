package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 修改支付密码，返回对象为 CheckPasswordEntity
 */

public class ModifyPasswordProtocol extends E2eeProtocol {
    public static final String TAG = ModifyPasswordProtocol.class.getSimpleName();

    /**
     * 用户id
     */
    public ModifyPasswordProtocol(String payPwd, String old, NetWorkCallbackListener dataCallback) {
        mPasscodeOld = old;
        mPasscode = payPwd;
        this.mDataCallback = dataCallback;
        mUrl = "api/settingpwd/setting";
        isModify = true;
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAY_PASSCODE, mPasscode);
        mBodyParams.put(RequestParams.PAY_PASSCODE_OLD, mPasscodeOld);
    }
}
