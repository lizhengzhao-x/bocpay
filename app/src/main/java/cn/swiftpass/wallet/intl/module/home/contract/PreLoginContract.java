package cn.swiftpass.wallet.intl.module.home.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;

public class PreLoginContract {

    public interface View extends IView {
        void appCallAppLinkSuccess(AppCallAppLinkEntity response, String url);

        void appCallAppLinkError(String errorCode, String errorMsg);

        void getMenuListSuccess(MenuListEntity response);

        void getMenuListFail(String errorCode, String errorMsg);


        void getInitDataSuccess(SystemPagerDataEntity response,String action);

        void getInitDataFail(String errorCode, String errorMsg);

    }

    public interface Presenter extends IPresenter<View> {
        void getAppCallAppLink(String token, String url, String checkSum, String digest);

        void getMenuList();

        void getInitData(String action);
        
        void getBackgroundUrl(GlideImageDownLoadListener listener);
        
    }
}
