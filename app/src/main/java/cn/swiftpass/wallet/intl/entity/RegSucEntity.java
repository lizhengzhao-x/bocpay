package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 信用卡注册成功返回
 */

public class RegSucEntity extends BaseEntity {


    //会话id
    String sId;
    String phone;
    String logout;
    //0不显示
    private String showInvite;
    /**
     * 是否需要修改密码 Y/N
     */
    private String needEditPwd;

    /**
     * 注册迎新popup banner
     */
    private String newUserPopUpBanner;

    public RegisterAccountEntity.CardsBean getCards() {
        return cards;
    }

    public void setCards(RegisterAccountEntity.CardsBean cards) {
        this.cards = cards;
    }

    String haveVCCard; //是否有可以绑定的信用卡

    private RegisterAccountEntity.CardsBean cards;

    /**
     * 通讯中心  (Y：跳 N：不跳)
     */
    private String jumpToMessageCenter;

    /**
     * Y 的时候是打开， N的时候是关闭
     */
    private String messageCenterIsOpen;

    /**
     * 当app为升级的时候，要判断 messageCenterIsOpen = N 的时候才显示推送通知弹框
     *
     * @return
     */
    public boolean isJumpToMessageCenterWhenFirstInstallOrUpdate() {
        if (TextUtils.isEmpty(messageCenterIsOpen)) {
            return false;
        } else {
            return TextUtils.equals(messageCenterIsOpen, "N");
        }
    }

    /**
     * 手动登录 确认是新设备 跳转到通知中心
     *
     * @return
     */
    public boolean isJumpToMessageCenter() {
        if (TextUtils.isEmpty(jumpToMessageCenter)) {
            return false;
        } else {
            return TextUtils.equals(jumpToMessageCenter, "Y");
        }
    }

    public boolean isOpenBiometricAuth() {
        if (TextUtils.isEmpty(openBiometricAuth)) {
            return true;
        }
        return TextUtils.equals(openBiometricAuth, "Y");
    }

    public String getHaveVCCard() {
        return haveVCCard;
    }

    public void setHaveVCCard(String haveVCCard) {
        this.haveVCCard = haveVCCard;
    }

    public void setOpenBiometricAuth(String openBiometricAuth) {
        this.openBiometricAuth = openBiometricAuth;
    }

    /**
     * 是否开启生物认证 Y开启 N 不开启
     */
    private String openBiometricAuth;


    public boolean showInvitePage() {
        if (TextUtils.isEmpty(showInvite)) {
            return false;
        } else {
            return showInvite.equals("1");
        }
    }

    public String getShowInvite() {
        return showInvite;
    }

    public void setShowInvite(String showInvite) {
        this.showInvite = showInvite;
    }

    public String getRewardTip() {
        return newop;
    }

    public void setRewardTip(String rewardTip) {
        this.newop = rewardTip;
    }

    private String newop;

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
    }

    String serverPubKey;


    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isNeedEditPwd() {
        if (TextUtils.isEmpty(needEditPwd)) {
            return false;
        } else {
            return TextUtils.equals(needEditPwd, "Y");
        }
    }

    public String getNewUserPopUpBanner() {
//        return "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/openrice/tc/";

        return newUserPopUpBanner;
    }

    public void setNewUserPopUpBanner(String newUserPopUpBanner) {
        this.newUserPopUpBanner = newUserPopUpBanner;
    }

    public String getJumpToMessageCenter() {
        return jumpToMessageCenter;
    }

    public void setJumpToMessageCenter(String jumpToMessageCenter) {
        this.jumpToMessageCenter = jumpToMessageCenter;
    }

    public String getMessageCenterIsOpen() {
        return messageCenterIsOpen;
    }

    public void setMessageCenterIsOpen(String messageCenterIsOpen) {
        this.messageCenterIsOpen = messageCenterIsOpen;
    }

    public String getOpenBiometricAuth() {
        return openBiometricAuth;
    }

    public String getNewop() {
        return newop;
    }

    public void setNewop(String newop) {
        this.newop = newop;
    }

    public String getNeedEditPwd() {
        return needEditPwd;
    }

    public void setNeedEditPwd(String needEditPwd) {
        this.needEditPwd = needEditPwd;
    }


}
