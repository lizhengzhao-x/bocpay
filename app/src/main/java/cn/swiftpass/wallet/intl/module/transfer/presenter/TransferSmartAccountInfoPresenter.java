package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSmartAccountInfoContract;

public class TransferSmartAccountInfoPresenter<P extends TransferSmartAccountInfoContract.View> extends BasePresenter<P> implements TransferSmartAccountInfoContract.Presenter<P> {
    @Override
    public void getSmartAccountInfo(String action, boolean iShowLoading, boolean isOnResume) {
        if (getView() != null && iShowLoading) {
            getView().showDialogNotCancel();
        }

        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (iShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    if (iShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoSuccess(response, action, isOnResume);
                }
            }
        })).execute();
    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }
}
