package cn.swiftpass.wallet.intl.module.cardmanagement;


import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.h6ah4i.android.widget.advrecyclerview.animator.GeneralItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.animator.RefactoredDefaultItemAnimator;
import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.card.CardContentViewHolder;
import cn.swiftpass.wallet.intl.module.cardmanagement.card.CardLinearLayoutManager;
import cn.swiftpass.wallet.intl.module.cardmanagement.card.CardStackAdapter;
import cn.swiftpass.wallet.intl.module.cardmanagement.card.CardStackClickListener;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.CardManagerContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.CardManagerPresenter;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.CardDetailActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountManageActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpgradeAccountActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.view.TotalGradeActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSmartAccountDescActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardBindSendOtpActivity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

import static cn.swiftpass.wallet.intl.entity.ApiConstant.VIRTUALCARD_QUERY;
import static cn.swiftpass.wallet.intl.module.cardmanagement.card.CardStackAdapter.cardItemOffset;


/**
 * 卡管理界面
 */


public class NewCardManagerFragment extends BaseFragment<CardManagerContract.Presenter> implements CardManagerContract.View, CardStackClickListener {
    @BindView(R.id.ry_card_stack)
    RecyclerView recyclerView;
    @BindView(R.id.id_check_totalgrade)
    RelativeLayout idCheckTotalGrade;
    public static final String ACTION_TOGGLE_ACCOUNT = "ACTION_TOGGLE_ACCOUNT";
    public static final String ACTION_ADD_CARD = "ACTION_ADD_CARD";
    public static final String ACTION_SEE_POINT = "ACTION_SEE_POINT";
    public static final String ACTION_UPDATE_ACCOUNT = "ACTION_UPDATE_ACCOUNT";
    public static final String ACTION_VIRTUAL_CARD_MANAGER = "ACTION_VIRTUAL_CARD_MANAGER";
    /**
     * 卡列表
     */
    private ArrayList<BankCardEntity> cardEntities;

    private CardStackAdapter adapter;

    private RecyclerViewExpandableItemManager expandableItemManager;
    private BankCardEntity selectVerifyBankCard;


    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.smart_account);
            mActivity.setToolBarRightViewToText(R.string.TEMP_1);
            mActivity.getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    //添加卡的时候需要密码验证
                    initPayPwdVerify(ACTION_ADD_CARD, false);
                }
            });
            if (CacheManagerInstance.getInstance().getCardAllEntities() != null) {
                cardEntities.addAll(CacheManagerInstance.getInstance().getCardAllEntities());
            } else if (CacheManagerInstance.getInstance().getCardEntities() != null) {
                cardEntities.addAll(CacheManagerInstance.getInstance().getCardEntities());
            }
            initRecyclerView();

        }

        idCheckTotalGrade.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_ACCOUNT_TOTALPOINT, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                expandableItemManager.collapseAll();
                initPayPwdVerify(ACTION_SEE_POINT, true);
            }
        });

        mPresenter.queryCardList("", true);
        mPresenter.getSmartAccountInfo(false);
    }

    private void initRecyclerView() {
        if (expandableItemManager != null) {
            expandableItemManager.release();
            expandableItemManager = null;
        }
        expandableItemManager = new RecyclerViewExpandableItemManager(null);
        expandableItemManager.setOnGroupExpandListener(new RecyclerViewExpandableItemManager.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition, boolean fromUser, Object payload) {
                if (fromUser) {
                    for (int i = 0; i < expandableItemManager.getGroupCount(); i++) {
                        if (groupPosition != i) {
                            expandableItemManager.collapseGroup(i);
                        }
                    }
                }

            }
        });
        recyclerView.setLayoutManager(new CardLinearLayoutManager(mActivity));
        adapter = new CardStackAdapter(mActivity, this);
        adapter.setRecyclerView(recyclerView);
        adapter.setExpandItemManager(expandableItemManager);
        adapter.updateData(cardEntities);
        recyclerView.setAdapter(expandableItemManager.createWrappedAdapter(adapter));
        DividerItemDecoration itemDecoration = new DividerItemDecoration(mActivity, DividerItemDecoration.VERTICAL) {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int adapterPosition = parent.getChildAdapterPosition(view);
                if (parent.getAdapter() != null && parent.getAdapter().getItemCount() > 0) {
                    if (adapterPosition != (parent.getAdapter().getItemCount() - 1)) {
                        outRect.bottom = -1 * cardItemOffset;
                    }
                }
            }

        };

        recyclerView.addItemDecoration(itemDecoration);
        final GeneralItemAnimator animator = new RefactoredDefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);
        recyclerView.setItemAnimator(animator);
        expandableItemManager.attachRecyclerView(recyclerView);

    }

    private void initPayPwdVerify(String action, boolean isSupportFinger) {
        VerifyPasswordCommonActivity.startActivityForResult(mActivity, isSupportFinger, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc(action);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mActivity, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    @Override
    public void onStart() {
        super.onStart();
        //主界面重新可见时，需要重新静默拉取账户余额和积分数据 因为是fragment所以onHiddenChanged也可能被触发
        LogUtils.d("home", "=====onStart");

        if (CacheManagerInstance.getInstance().isLogin() && CacheManagerInstance.getInstance().isHasSessionId()) {
            adapter.hideAccount();
            mPresenter.queryCardList("", true);
        }
    }

    private void hideAccount() {
        adapter.hideAccount();
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.d("home", "=====onHiddenChanged");
        if (hidden) {   // 不在最前端显示 相当于调用了onPause();
            return;
        } else {  // 在最前端显示 相当于调用了onResume();
            hideAccount();
            mPresenter.queryCardList("", false);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD
                || event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            mPresenter.queryCardList("", true);
            expandableItemManager.collapseAll();
        } else if (event.getEventType() == CardEventEntity.EVENT_REFRESH_SMART_INFO) {
            mPresenter.getSmartAccountInfo(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PagerEventEntity event) {
        if (event.getEventType() == PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_ACCOUNT_NOTIFICATION) {
            AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_ACCOUNT_TOTALPOINT, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
            sendAnalysisEvent(analysisButtonEntity);
            expandableItemManager.collapseAll();
            initPayPwdVerify(ACTION_SEE_POINT, true);
        }
    }

    private void verifyResultSuc(String action) {
        if (ACTION_ADD_CARD.equals(action)) {
            if (CacheManagerInstance.getInstance().checkLoginStatus()) {
                linkBankCard(ACTION_ADD_CARD);
            }
        } else if (ACTION_SEE_POINT.equals(action)) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), TotalGradeActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (ACTION_UPDATE_ACCOUNT.equals(action)) {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
            ActivitySkipUtil.startAnotherActivity(mActivity, UpgradeAccountActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (ACTION_TOGGLE_ACCOUNT.equals(action)) {
            SystemInitManager.getInstance().setValidShowAccount(true);
            if (selectVerifyBankCard != null) {
                selectVerifyBankCard.setSel(true);
            }
            adapter.notifyDataSetChanged();
            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
        } else if (ACTION_VIRTUAL_CARD_MANAGER.equals(action)) {
            VirtualCardListEntity.VirtualCardListBean vitualCardListEntity = new VirtualCardListEntity.VirtualCardListBean();
            vitualCardListEntity.setCardId(selectVerifyBankCard.getCardId());
            vitualCardListEntity.setAccount(selectVerifyBankCard.getPhone());
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.VITUALCARD_CHECK_DETAIL);
            mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, vitualCardListEntity);
            ActivitySkipUtil.startAnotherActivity(mActivity, VirtualCardBindSendOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void linkBankCard(String action) {
        if (cardEntities != null && !cardEntities.isEmpty()) {
            //网络异常状态操作 需要拉取一次卡列表
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
            ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            mPresenter.queryCardList(action, true);
        }
    }


    @Override
    protected CardManagerContract.Presenter loadPresenter() {
        return new CardManagerPresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_compat_card_manager;
    }

    @Override
    protected void initView(View v) {
        EventBus.getDefault().register(this);
        cardEntities = new ArrayList<>();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void onClickTopUp(BankCardEntity bankCardEntity) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null && !TextUtils.isEmpty(mySmartAccountEntity.getSmartAcLevel())) {
            if (MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
                TransferUtils.checkTopUpEvent(mySmartAccountEntity, new TransferUtils.RegEddaEventListener() {
                    @Override
                    public void getRegEddaInfo() {
                        mPresenter.getRegEddaInfo();
                    }

                    @Override
                    public Activity getContext() {
                        return getActivity();
                    }

                    @Override
                    public void showErrorDialog(String errorCode, String message) {
                        showErrorMsgDialog(getContext(), message);
                    }
                });
            } else {
                showActiveAccount(bankCardEntity);
            }
        } else {
            mPresenter.getSmartAccountInfo(false);
        }
    }

    public void showActiveAccount(BankCardEntity bankCardEntity) {
        AndroidUtils.showTipDialog(mActivity, getString(R.string.AC2101_19_11), getString(R.string.AC2101_19_8),
                getString(R.string.AC2101_19_10), getString(R.string.AC2101_19_9), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                        mHashMapsLogin.put(Constants.CURRENT_BANK_CARD, bankCardEntity);
                        mHashMapsLogin.put(Constants.CARD_ENTITY, CacheManagerInstance.getInstance().getCardEntities());
                        ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountManageActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                });
    }

    @Override
    public void onClickWithdraw(BankCardEntity bankCardEntity) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null && !TextUtils.isEmpty(mySmartAccountEntity.getSmartAcLevel())) {
            if (MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
                TransferUtils.onWithdrawEvent(mySmartAccountEntity, new TransferUtils.RegEddaEventListener() {
                    @Override
                    public void getRegEddaInfo() {
                        mPresenter.getRegEddaInfo();
                    }

                    @Override
                    public Activity getContext() {
                        return mActivity;
                    }

                    @Override
                    public void showErrorDialog(String errorCode, String message) {
                        showErrorMsgDialog(getContext(), message);
                    }
                });
            } else {
                showActiveAccount(bankCardEntity);
            }

        } else {
            mPresenter.getSmartAccountInfo(false);
        }
    }

    @Override
    public void onClickChangeLimitBalance(BankCardEntity bankCardEntity) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            if (MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
                SmartAccountAdjustDailyLimitActivity.startActivity(mActivity, mySmartAccountEntity, mySmartAccountEntity.getHideMobile());
            } else {
                showActiveAccount(bankCardEntity);
            }
        }
    }

    @Override
    public void verifyPwdAction(String verifyPwdAction, CardContentViewHolder holder, BankCardEntity bankCardEntity) {

        this.selectVerifyBankCard = bankCardEntity;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc(verifyPwdAction);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    public void onClickUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc(ACTION_UPDATE_ACCOUNT);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    public void onClickVirtualCardManager(BankCardEntity bankCardEntity) {
        this.selectVerifyBankCard = bankCardEntity;
        VerifyPasswordCommonActivity.startActivityForActionResult(getActivity(), VIRTUALCARD_QUERY, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc(ACTION_VIRTUAL_CARD_MANAGER);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    public void onClickAccountManager(BankCardEntity bankCardEntity) {
        // 需要检查是否绑定只能账户
        if (TextUtils.equals(bankCardEntity.getCardType(), SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.CURRENT_BANK_CARD, bankCardEntity);
            mHashMapsLogin.put(Constants.CARD_ENTITY, CacheManagerInstance.getInstance().getCardEntities());
            ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountManageActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            creditCardInfo(bankCardEntity);
        }

    }


    /**
     * 信用卡详情页
     *
     * @param bankCardEntity
     */
    private void creditCardInfo(BankCardEntity bankCardEntity) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.CARD_ENTITY, bankCardEntity);
        ActivitySkipUtil.startAnotherActivity(getActivity(), CardDetailActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void onClickRegisterFps(BankCardEntity bankCardEntity) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            if (MySmartAccountEntity.ACCOUNT_STATUS_NORMAL.equals(mySmartAccountEntity.getCardState())) {
                VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
                    @Override
                    public void onVerifySuccess() {

                        mPresenter.checkSmartAccountBind(true);

                    }

                    @Override
                    public void onVerifyFailed(String errorCode, String errorMsg) {
                    }

                    @Override
                    public void onVerifyCanceled() {

                    }
                });
            } else {
                showActiveAccount(bankCardEntity);
            }
        }

    }


    @Override
    public void queryCardListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(getActivity(), errorMsg);
        //如果拉取失败 加载内存中的卡列表
        if (cardEntities == null) {
            cardEntities = new ArrayList<>();
        }

        cardEntities.clear();
        if (CacheManagerInstance.getInstance().getCardAllEntities() != null) {
            cardEntities.addAll(CacheManagerInstance.getInstance().getCardAllEntities());
        } else if (CacheManagerInstance.getInstance().getCardEntities() != null) {
            cardEntities.addAll(CacheManagerInstance.getInstance().getCardEntities());
        }
        adapter.updateData(cardEntities);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void queryCardListSuccess(CardsEntity response, String action) {
        cardEntities.clear();
        ArrayList<BankCardEntity> rows = response.getRows();
        if (rows != null && rows.size() > 0) {
            cardEntities.addAll(rows);
            CacheManagerInstance.getInstance().setCardEntities(rows);
            CacheManagerInstance.getInstance().setCardAllEntities(rows);
        }
        adapter.updateData(rows);
        adapter.notifyDataSetChanged();
        if (rows != null) {
            if (ACTION_ADD_CARD.equals(action)) {
                //网络异常状态操作 需要拉取一次卡列表
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        if (mySmartAccountEntity != null) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);

            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mySmartAccountEntity.getHideMobile());
            mHashMapsLogin.put(Constants.SMART_ACCOUNT, mySmartAccountEntity);
            mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
            ActivitySkipUtil.startAnotherActivity(mActivity, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_CLOSE_LEFT_MENU, ""));

        }

    }

    @Override
    public void checkSmartAccountBindError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            AndroidUtils.showBindSmartAccountDialog(mActivity);
        } else {
            showErrorMsgDialog(getActivity(), errorMsg);
        }
    }

    @Override
    public void checkSmartAccountBindSuccess(ContentEntity response) {
        if (TextUtils.equals(response.getResult_code(), SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
            RegisterSmartAccountDescActivity.startActivity(getActivity(), maps);
        } else if (TextUtils.equals(response.getResult_code(), SmartAccountConst.SMART_ACCOUNT_EXIST)) {
            ActivitySkipUtil.startAnotherActivity(mActivity, FpsManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            AndroidUtils.showTipDialog(getActivity(), response.getResult_msg());
        }
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {

    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        //接口底层已经刷新了缓存中的MySmartAccountEntity 数据
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (cardEntities != null) {
                for (BankCardEntity cardEntity : cardEntities) {
                    cardEntity.setSel(false);
                }
                adapter.updateData(cardEntities);
                adapter.notifyDataSetChanged();
            }
        }

    }
}
