package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckRequest;

/**
 * Created by ramon on 2018/10/10.
 */

public class FpsPreCheckProtocol extends BaseProtocol {

    FpsPreCheckRequest mPreReq;
    //删除银行记录的类型
    String mDelType;

    public FpsPreCheckProtocol(FpsPreCheckRequest request, NetWorkCallbackListener dataCallback) {
        mPreReq = request;
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/operationFPSPreCheck";
    }

    public FpsPreCheckProtocol(FpsPreCheckRequest request, String delType, NetWorkCallbackListener dataCallback) {
        this(request, dataCallback);
        mDelType = delType;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID, mPreReq.accountId);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID_TYPE, mPreReq.accountIdType);
        mBodyParams.put(FpsConst.FPS_DEFAULT_BANK, mPreReq.defaultBank);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_NO, mPreReq.accountNo);
        mBodyParams.put(FpsConst.FPS_BANK_CODE, mPreReq.bankCode);
        mBodyParams.put(FpsConst.FPS_ACTION, mPreReq.action);
        if (!TextUtils.isEmpty(mDelType)) {
            mBodyParams.put(FpsConst.FPS_RECORD_DEL_TYPE, mDelType);
        }
    }

}
