package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class TopUpResponseEntity extends BaseEntity {

    private String openfld;

    public String getOpenfld() {
        return openfld;
    }

    public void setOpenfld(String openfld) {
        this.openfld = openfld;
    }

    public String getTrxNo() {
        return trxNo;
    }

    public void setTrxNo(String trxNo) {
        this.trxNo = trxNo;
    }

    public String getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(String trxDate) {
        this.trxDate = trxDate;
    }

    private String trxNo;
    private String trxDate;
}
