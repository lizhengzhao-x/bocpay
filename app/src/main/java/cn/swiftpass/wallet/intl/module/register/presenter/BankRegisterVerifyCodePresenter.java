package cn.swiftpass.wallet.intl.module.register.presenter;

import android.content.Context;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.ErrorEntity;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeEntity;
import cn.swiftpass.wallet.intl.module.register.contract.BankRegisterVerifyCodeContract;
import cn.swiftpass.wallet.intl.module.register.model.BankRegisterVerifyCodeModel;

public class BankRegisterVerifyCodePresenter extends BasePresenter<BankRegisterVerifyCodeContract.View> implements BankRegisterVerifyCodeContract.Presenter {
    /**
     * 获取信用卡注册界面验证码
     *
     * @param context
     */
    @Override
    public void getBankRegisterVerifyCode(final Context context) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        } else {
            return;
        }

        BankRegisterVerifyCodeModel.getBankRegisterVerifyCode("CC_V", new NetWorkCallbackListener<BankRegisterVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getBankRegisterVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankRegisterVerifyCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getBankRegisterVerifyCodeSuccess(response);
                }
            }
        });

    }

    /**
     * 检查信用卡注册界面验证码
     *
     * @param context
     * @param verifyCode
     */
    @Override
    public void checkBankRegisterVerifyCode(final Context context, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        } else {
            return;
        }

        BankRegisterVerifyCodeModel.checkBankRegisterVerifyCode(verifyCode, "CC_V", new NetWorkCallbackListener<BankRegisterVerifyCodeCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkBankRegisterVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankRegisterVerifyCodeCheckEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkBankRegisterVerifyCodeSuccess(response);
                }
            }
        });


    }

}
