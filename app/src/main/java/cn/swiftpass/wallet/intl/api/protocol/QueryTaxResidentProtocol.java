package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.RegisterTaxResultEntity;


public class QueryTaxResidentProtocol extends BaseProtocol {

    private final String type;

    public QueryTaxResidentProtocol(String type, NetWorkCallbackListener<RegisterTaxResultEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartReg/queryTaxResidentRelationInfo";
        this.type = type;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ENTRANCE, type);
    }

}
