package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.setting.presenter.InviteSharePresenter;

/**
 * 推荐亲友
 *
 * @author lizheng.zhao
 */
public class InviteShareActivity extends BaseCompatActivity {

    private FragmentManager mFragmentManager;
    private InviteShareFragment inviteShareFragment;


    @Override
    protected void init() {
        setToolBarTitle(R.string.MGM2103_1_1);

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        inviteShareFragment = new InviteShareFragment();
        fragmentTransaction.add(R.id.fl_main, inviteShareFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_invite_share;
    }

    @Override
    protected InviteSharePresenter createPresenter() {
        return new InviteSharePresenter();
    }
}