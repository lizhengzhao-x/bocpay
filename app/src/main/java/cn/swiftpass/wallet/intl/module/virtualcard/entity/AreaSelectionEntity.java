package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/12/5.
 */
public class AreaSelectionEntity extends BaseEntity {


    private String area;
    private boolean isSelection;

    public AreaSelectionEntity(String area, boolean isSelection) {
        this.area = area;
        this.isSelection = isSelection;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public boolean isSelection() {
        return isSelection;
    }

    public void setSelection(boolean selection) {
        isSelection = selection;
    }
}
