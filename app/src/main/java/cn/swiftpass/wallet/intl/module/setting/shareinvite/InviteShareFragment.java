package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.entity.InviteShareEntity;
import cn.swiftpass.wallet.intl.entity.ShowInviteCodeEntity;
import cn.swiftpass.wallet.intl.module.register.RegisterInviteActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.contract.InviteShareContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.InviteSharePresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.dialog.BottomDialog;

import static cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity.ISSUPPORTZOOMIN;

public class InviteShareFragment extends BaseFragment<InviteShareContract.Presenter> implements InviteShareContract.View {

    /**
     * 分享
     */
    public static final String SHARE_INFO = "share_info";
    public static final String HAS_DATA = "has_data";

    @BindView(R.id.id_listview)
    ListView idListview;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;


    LinearLayout inviteCodeLayout;
    RelativeLayout successListLayout;
    ImageView idShareImage;
    TextView idInviteShareTitle;
    TextView idInviteShareDate;
    TextView idShareSubtitle;
    LinearLayout idInviteShareCode;
    TextView idInviteShareCopy;
    TextView idInviteShareRule;
    TextView tvInvite;
    View lineNoActivityLine;
    TextView idShowAll;
    TextView idHintText;


    private List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeans;
    private InviteListHeaderAdapter inviteListHeaderAdapter;
    private InviteShareEntity mInviteShareEntity;
    private InviteRecordListEntity inviteRecordListEntity;
    private View headerView;
    /**
     * 有没有检测过权限
     */
    public boolean mAlreadyCheckPermission = false;
    private Handler mShareHandler;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;
    private CustomDialog mDealDialog;

    private BottomDialog bottomDialog;
//    /**
//     * 是否为弹出bottomDialog而触发的刷新页面
//     */
//    boolean isNeedUpdate = true;

    /**
     * 页面创建时间
     */
    private long currentPageCreateTime;

    /**
     * 初始化数据
     */
    private final String INIT_DATA = "init_data";

    /**
     * 输入邀请码的点击
     */
    private final String RIGHT_TEXT_CLICK = "right_Text_Click";


    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {

            mActivity.setToolBarRightViewToText(R.string.MGM1_2_2);
            mActivity.getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (mInviteShareEntity != null) {
                        rightTextClick();
                    } else {
                        mPresenter.getInviteShareDetail(RIGHT_TEXT_CLICK);
                    }
                }
            });
        }
    }

    @Override
    protected InviteShareContract.Presenter loadPresenter() {
        return new InviteSharePresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_invite_share;
    }


    @Override
    protected void initView(View v) {
        currentPageCreateTime = System.currentTimeMillis();
        mShareHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.obj != null) {
                    try {
                        List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeans = (List<InviteRecordListEntity.InviteRecordsBean>) msg.obj;
                        notifyUI(inviteRecordsBeans);
                    } catch (Exception e) {
                        if (BuildConfig.isLogDebug) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        headerView = LayoutInflater.from(getContext()).inflate(R.layout.include_share_header, null);
        idShareImage = (ImageView) headerView.findViewById(R.id.id_share_image);
        idInviteShareTitle = (TextView) headerView.findViewById(R.id.id_invite_share_title);
        idShareSubtitle = (TextView) headerView.findViewById(R.id.id_share_subtitle);
        idInviteShareDate = (TextView) headerView.findViewById(R.id.id_invite_share_date);
        idInviteShareCode = (LinearLayout) headerView.findViewById(R.id.id_invite_share_code);
        idInviteShareCopy = (TextView) headerView.findViewById(R.id.id_invite_share_copy);
        idInviteShareRule = (TextView) headerView.findViewById(R.id.id_invite_share_rule);
        idShowAll = (TextView) headerView.findViewById(R.id.id_show_all);
        idHintText = (TextView) headerView.findViewById(R.id.id_hint_text);
        tvInvite = (TextView) headerView.findViewById(R.id.tv_invite);
        lineNoActivityLine = headerView.findViewById(R.id.id_no_activity_status);
        inviteCodeLayout = (LinearLayout) headerView.findViewById(R.id.layout_invite_share_code);
        successListLayout = (RelativeLayout) headerView.findViewById(R.id.layout_success_list);

        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idShareImage.getLayoutParams();
        int width = AndroidUtils.getScreenWidth(mContext);
        // 375 85
        int height = width * 35 / 75;
        lp.width = width;
        lp.height = height;
        idShareImage.setLayoutParams(lp);

        idListview.addHeaderView(headerView);
        View footView = LayoutInflater.from(getContext()).inflate(R.layout.item_footview, null);
        idListview.addFooterView(footView);
        headerView.setVisibility(View.INVISIBLE);
        inviteRecordsBeans = new ArrayList<>();
        inviteListHeaderAdapter = new cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteListHeaderAdapter(inviteRecordsBeans, getContext());
        idListview.setAdapter(inviteListHeaderAdapter);
        idShowAll.setEnabled(false);
        idShowAll.setVisibility(View.INVISIBLE);
        successListLayout.setVisibility(View.VISIBLE);
        BasicUtils.initSpannableStrWithTv(getString(R.string.MGM1_2_8_1), "%%", idInviteShareRule, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (mInviteShareEntity != null && !TextUtils.isEmpty(mInviteShareEntity.getActivityTncUrl())) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.DETAIL_URL, mInviteShareEntity.getActivityTncUrl());
                    mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                    mHashMaps.put(ISSUPPORTZOOMIN, true);
                    mHashMaps.put(WebViewActivity.NEW_MSG, true);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }else{

                }
            }
        });

        addListener();


        idListview.post(new Runnable() {
            @Override
            public void run() {
                idListview.smoothScrollToPosition(0);
            }
        });

        idListview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                View firstView = view.getChildAt(firstVisibleItem);
                if (firstVisibleItem == 0 && (firstView == null || firstView.getTop() == 0)) {
                    /*上滑到listView的顶部时，下拉刷新组件可见*/
                    smartRefreshLayout.setEnabled(true);
                } else {
                    /*不是listView的顶部时，下拉刷新组件不可见*/
                    smartRefreshLayout.setEnabled(false);
                }
            }
        });

        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                reloadView();
            }
        });

        mPresenter.getInviteShareDetail(INIT_DATA);
    }

    private void addListener() {
        idShowAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //显示所有的邀请人
                if (!ButtonUtils.isFastDoubleClick()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    ActivitySkipUtil.startAnotherActivity(getActivity(), InviteListActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
        tvInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //分享
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (mInviteShareEntity != null) {
                        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_REFER_FRIEND_INVI, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_REFER_FRIENDS_PAGE, System.currentTimeMillis() - currentPageCreateTime);
                        sendAnalysisEvent(analysisButtonEntity);
                        showInviteDialog();
                    }
                }
            }
        });

        idInviteShareCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //复制到剪贴板
                if (!ButtonUtils.isFastDoubleClick()) {
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_REFER_FRIEND_CP, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_REFER_FRIENDS_PAGE, System.currentTimeMillis() - currentPageCreateTime);
                    sendAnalysisEvent(analysisButtonEntity);
                    if (mInviteShareEntity != null) {
                        if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("0")) {
                            showErrorMsgDialog(getContext(), getString(R.string.EMSG1_1_8));
                        } else if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("1")) {
                            showErrorMsgDialog(getContext(), getString(R.string.EMSG1_1_9));
                        } else {
                            AndroidUtils.copyToClipboard(getContext(), mInviteShareEntity.getInCode());
                            Toast.makeText(getContext(), getString(R.string.MGM1_2_7_a), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        });
    }

    private void showInviteDialog() {
        bottomDialog = new BottomDialog(mActivity);
        View contentView = LayoutInflater.from(mActivity).inflate(R.layout.dialog_bottom_invite, null);
        ImageView ivClose = contentView.findViewById(R.id.iv_close);
        TextView tvFindInvite = contentView.findViewById(R.id.tv_find_invite);
        TextView tvInvite = contentView.findViewById(R.id.tv_invite);

        tvFindInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
//                    showDialogNotCancel();
                    bottomDialog.dismiss();
                    //检查是否有通讯录权限
                    if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                            //拒绝并勾选不再询问
                            firstRejectContacts = false;
                        } else {
                            firstRejectContacts = true;
                        }

                        PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
                            @Override
                            public void acceptPermission() {
//                                dismissDialog();
                                if (mInviteShareEntity != null) {
                                    HashMap<String, Object> shareMap = new HashMap<>();
                                    shareMap.put(SHARE_INFO, mInviteShareEntity);
                                    ActivitySkipUtil.startAnotherActivity(mActivity, InviteContactActivity.class, shareMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }
                            }

                            @Override
                            public void rejectPermission() {
//                                dismissDialog();
                                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                                    if (!firstRejectContacts) {
                                        showLackOfPermissionDialog();
                                    } else {
                                        firstRejectContacts = false;
                                    }
                                }
                            }
                        }, WalletConstants.PERMISSIONS_READ_CONTACTS[0]);
                    } else {
                        if (mInviteShareEntity != null) {
                            HashMap<String, Object> shareMap = new HashMap<>();
                            shareMap.put(SHARE_INFO, mInviteShareEntity);
                            ActivitySkipUtil.startAnotherActivity(mActivity, InviteContactActivity.class, shareMap, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    }
                }
            }
        });

        tvInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    bottomDialog.dismiss();
                    shareEvent(mInviteShareEntity.getShareInfo());
                }
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    bottomDialog.dismiss();
                }
            }
        });

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
//                isNeedUpdate = false;
            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            layoutParams.height = AndroidUtils.dip2px(mActivity, 270);
            dialogWindow.setAttributes(layoutParams);
        }
        bottomDialog.showWithBottomAnim();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (bottomDialog != null && bottomDialog.isShowing()) {
            bottomDialog.dismiss();
        }
    }

    public void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.TR1_4b_2));
        builder.setMessage(getString(R.string.TR1_4b_3));

        builder.setPositiveButton(getString(R.string.M10_1_25), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mActivity);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        if (!mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    public void reloadView() {
        if (mPresenter != null) {
            mPresenter.getInviteShareDetail(INIT_DATA);
        }
    }

    private void shareEvent(String content) {
        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
        textIntent.putExtra(Intent.EXTRA_TEXT, content);
        startActivity(Intent.createChooser(textIntent, ""));
    }


    /**
     * 分享的好友列表
     */
    public void getTransferInviteRecord() {
        mPresenter.getInviteShareList();
    }

    /**
     * @param inviteRecordsBeansIn
     */
    private void notifyUI(List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeansIn) {
        if (inviteRecordsBeansIn == null) {
            return;
        }
        if (inviteRecordsBeansIn.size() > 5) {
            idShowAll.setVisibility(View.VISIBLE);
            List<InviteRecordListEntity.InviteRecordsBean> beansTemp = new ArrayList<>();
            List<InviteRecordListEntity.InviteRecordsBean> beans = inviteRecordsBeansIn.subList(0, 5);
            beansTemp.addAll(beans);
            inviteRecordsBeansIn.clear();
            inviteRecordsBeansIn.addAll(beansTemp);
        } else {
            idShowAll.setVisibility(View.INVISIBLE);
        }
        inviteListHeaderAdapter.updateList(inviteRecordsBeansIn);
    }


    /**
     * 根据server 返回 更新ui
     *
     * @param response
     */
    private void judgeActivityValidity(InviteShareEntity response) {
        mInviteShareEntity = response;
        updateUiStatus(!response.isStatusEnd());
    }

    private void rightTextClick() {
        if (null == mInviteShareEntity) {
            return;
        }

        if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("0")) {
            showErrorMsgDialog(getContext(), getString(R.string.EMSG1_1_8));
        } else if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("1")) {
            showErrorMsgDialog(getContext(), getString(R.string.EMSG1_1_9));
        } else {
            AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_REG_ENTER_CODE, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_REFER_FRIENDS_PAGE, System.currentTimeMillis() - currentPageCreateTime);
            sendAnalysisEvent(analysisButtonEntity);
            judjeUserAlreadyInputInviteCode();
        }
    }


    private void updateUiStatus(boolean isValidity) {
        int length = mInviteShareEntity.getInCode().length();
        int totalWith = idInviteShareCode.getWidth();
        int totalHeight = idInviteShareCode.getHeight();
        int itemWidth = totalWith / length;
        idInviteShareCode.removeAllViews();
        for (int i = 0; i < length; i++) {
            TextView tv = new TextView(getContext());
            tv.setLayoutParams(new LinearLayout.LayoutParams(itemWidth, totalHeight));
            tv.setText(mInviteShareEntity.getInCode().charAt(i) + "");
            tv.setTextSize(25);
            tv.setGravity(Gravity.CENTER);
            idInviteShareCode.addView(tv);
            tv.setTextColor(isValidity ? Color.BLACK : Color.parseColor("#9B9B9B"));
        }
        idInviteShareDate.setText(getString(R.string.MGM1_2_5) + " " + mInviteShareEntity.getInvitationTime());
        if (!TextUtils.isEmpty(mInviteShareEntity.getRewardesc())) {
            idInviteShareTitle.setText(mInviteShareEntity.getRewardesc());
        }
        if (!TextUtils.isEmpty(mInviteShareEntity.getSubContent())) {
            idShareSubtitle.setText(mInviteShareEntity.getSubContent());
        }

        //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
        RequestOptions options = new RequestOptions()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);


        GlideApp.with(this).load(mInviteShareEntity.getPicUrl())
                .apply(options)
                .skipMemoryCache(true)
                .placeholder(R.mipmap.ic_bac_alpha)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(idShareImage);

        headerView.setVisibility(View.VISIBLE);
        idInviteShareCopy.setTextColor(isValidity ? Color.BLACK : Color.parseColor("#9B9B9B"));
        tvInvite.setEnabled(isValidity);
        mActivity.getToolBarRightView().setEnabled(isValidity);
        idInviteShareCopy.setEnabled(isValidity);
        tvInvite.setBackgroundResource(isValidity ? R.drawable.bg_btn_next_page : R.drawable.bg_btn_next_gray_disable);
        if (mInviteShareEntity != null) {
            //活动未开始和活动结束都不显示推广期
            if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("0")) {
                idInviteShareDate.setVisibility(View.GONE);
                //未开始
                inviteCodeLayout.setVisibility(View.GONE);
                //条款细则不显示
                idInviteShareRule.setVisibility(View.GONE);
                lineNoActivityLine.setVisibility(View.VISIBLE);
                //右上角 输入邀请码不显示
                mActivity.getToolBarRightView().setVisibility(View.INVISIBLE);
                tvInvite.setText(getString(R.string.MGM2101_1_2));
                mActivity.setToolBarTitle(getString(R.string.MGM2101_1_1));
                getInviteRecordList();
            } else if (!TextUtils.isEmpty(mInviteShareEntity.getInviteDateStatus()) && mInviteShareEntity.getInviteDateStatus().equals("1")) {
                idInviteShareDate.setVisibility(View.GONE);
                inviteCodeLayout.setVisibility(View.GONE);
                idInviteShareRule.setVisibility(View.GONE);
                mActivity.getToolBarRightView().setVisibility(View.INVISIBLE);
                lineNoActivityLine.setVisibility(View.VISIBLE);
                tvInvite.setText(getString(R.string.MGM2101_1_2));
                mActivity.setToolBarTitle(getString(R.string.MGM2101_1_1));
                //已经结束
                getInviteRecordList();
            } else {
                mActivity.setToolBarTitle(R.string.MGM2103_1_1);
                mActivity.getToolBarRightView().setVisibility(View.VISIBLE);
                idInviteShareDate.setVisibility(View.VISIBLE);
                inviteCodeLayout.setVisibility(View.VISIBLE);
                idInviteShareRule.setVisibility(View.VISIBLE);
                //正在进行
                getInviteRecordList();
            }
        }

    }

    private void getInviteRecordList() {
        if (!mAlreadyCheckPermission) {
            checkHasContactsPermission();
        } else {
            getTransferInviteRecord();
        }
    }

    private void checkHasContactsPermission() {
        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
        }

        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                getTransferInviteRecord();
            }

            @Override
            public void rejectPermission() {
//                dismissDialog();
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                    if (!firstRejectContacts) {
                        showLackOfPermissionDialog();
                    } else {
                        firstRejectContacts = false;
                    }
                }
            }
        }, WalletConstants.PERMISSIONS_READ_CONTACTS[0]);

    }

    private void judjeUserAlreadyInputInviteCode() {
        ApiProtocolImplManager.getInstance().isShowInviteUI(mActivity, new NetWorkCallbackListener<ShowInviteCodeEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                showErrorMsgDialog(getContext(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                    }
                });
            }

            @Override
            public void onSuccess(ShowInviteCodeEntity response) {
                if (response.showInviteUIStatus()) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_SETTING);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), RegisterInviteActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }


    @Override
    public void getInviteShareDetailSuccess(InviteShareEntity response, String type) {
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        }
        judgeActivityValidity(response);

        /**
         * 只有按钮显示 而且从按钮点击事件传递过来才需要处理
         */
        if (TextUtils.equals(type, RIGHT_TEXT_CLICK) &&
                View.VISIBLE == mActivity.getToolBarRightView().getVisibility()) {
            rightTextClick();
        }

    }

    @Override
    public void getInviteShareDetailError(String errorCode, String errorMsg) {
        if (smartRefreshLayout != null) {
            smartRefreshLayout.finishRefresh();
        }
        showErrorMsgDialog(getContext(), errorMsg);
    }

    @Override
    public void getInviteShareListSuccess(InviteRecordListEntity response) {

        idShowAll.setEnabled(true);
        inviteRecordListEntity = response;
        if (!TextUtils.isEmpty(response.getUpToLimit())) {
            idHintText.setText(response.getUpToLimit());
            idHintText.setVisibility(View.VISIBLE);
        } else {
            idHintText.setVisibility(View.GONE);
        }
        if (response.getInviteRecords() != null) {
            inviteRecordsBeans.clear();
            inviteRecordsBeans.addAll(response.getInviteRecords());
            if (inviteRecordsBeans.size() == 0) {
                return;
            }
            if (isGranted(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //如果有读取本地通讯录的权限
                        //本地通讯录跟server返回对比 如果本地通讯录中有这个号码 就显示明文 否则显示密文
                        ArrayList<ContactEntity> contactEntities = null;
                        if (ContactUtils.getInstance().getContacts() != null) {
                            contactEntities = ContactUtils.getInstance().getContacts();
                        } else {
                            contactEntities = ContactUtils.getInstance().getAllContacts(getContext(), true);
                        }
                        final int size = inviteRecordsBeans.size() > 5 ? 5 : inviteRecordsBeans.size();
                        //只显示5条 点击显示更多 才查看更多
                        if (contactEntities != null && contactEntities.size() > 0) {
                            for (int i = 0; i < size; i++) {
                                InviteRecordListEntity.InviteRecordsBean inviteRecordsBean = inviteRecordsBeans.get(i);
                                for (int j = 0; j < contactEntities.size(); j++) {
                                    ContactEntity contactEntity = contactEntities.get(j);
                                    inviteRecordsBean.setMatcher(true);
                                    if (AndroidUtils.isSameNumber(inviteRecordsBean.getReMobile(), contactEntity.getNumber())) {
                                        inviteRecordsBean.setShowNumber(true);
                                        inviteRecordsBean.setReCustName(contactEntity.getUserName());
                                    }
                                }
                            }
                            if (mShareHandler != null) {
                                updateUiData(inviteRecordsBeans);
                            }
                        } else {
                            updateUiData(inviteRecordsBeans);
                        }
                    }
                }).start();
            } else {
                updateUiData(inviteRecordsBeans);
            }

        }
    }

    /**
     * 防止权限弹出 onResume掉用两次 然后数据刷新过程中数据被篡改 所以这里用多个集合 转换
     * 用handler 主要是 有个读取通讯录 耗时 需要线程切换
     *
     * @param inviteRecordsBeansIn
     */
    public void updateUiData(List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeansIn) {
        Message message = Message.obtain();
        List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeanTmp = new ArrayList<>();
        inviteRecordsBeanTmp.addAll(inviteRecordsBeansIn);
        message.obj = inviteRecordsBeanTmp;
        mShareHandler.sendMessageDelayed(message, 0);
    }


    @Override
    public void getInviteShareListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(getContext(), errorMsg);
        idShowAll.setEnabled(true);
    }

}
