package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class GlobalRoamingListEntity extends BaseEntity {


    private List<GlobalRoamingListBean> globalRoamingList;

    public List<GlobalRoamingListBean> getGlobalRoamingList() {
        return globalRoamingList;
    }

    public void setGlobalRoamingList(List<GlobalRoamingListBean> globalRoamingList) {
        this.globalRoamingList = globalRoamingList;
    }

    public static class GlobalRoamingListBean extends BaseEntity {
        /**
         * areaCode : 93
         * checked : false
         * chineseName : 阿富汗
         * chineseSimplifiedName : 阿富汗
         * englishName : Afghanistan
         * pageNumber : 1
         * pageSize : 10
         * total : 0
         */

        private String areaCode;
        private boolean checked;
        private String chineseName;
        private String chineseSimplifiedName;
        private String englishName;
        private int pageNumber;
        private int pageSize;
        private int total;

        public String getAreaCode() {
            return areaCode;
        }

        public void setAreaCode(String areaCode) {
            this.areaCode = areaCode;
        }

        public boolean isChecked() {
            return checked;
        }

        public void setChecked(boolean checked) {
            this.checked = checked;
        }

        public String getChineseName() {
            return chineseName;
        }

        public void setChineseName(String chineseName) {
            this.chineseName = chineseName;
        }

        public String getChineseSimplifiedName() {
            return chineseSimplifiedName;
        }

        public void setChineseSimplifiedName(String chineseSimplifiedName) {
            this.chineseSimplifiedName = chineseSimplifiedName;
        }

        public String getEnglishName() {
            return englishName;
        }

        public void setEnglishName(String englishName) {
            this.englishName = englishName;
        }

        public int getPageNumber() {
            return pageNumber;
        }

        public void setPageNumber(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }
    }
}
