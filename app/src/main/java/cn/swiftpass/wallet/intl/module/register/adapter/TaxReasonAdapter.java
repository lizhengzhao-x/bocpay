package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TaxInfoReason;

public class TaxReasonAdapter extends RecyclerView.Adapter {
    private final ArrayList<TaxInfoReason> data;
    private final Context context;
    private final OnClickTaxReasonListener listener;
    private TaxInfoReason currentReasonData;
    private int selectIndex = -1;

    public TaxReasonAdapter(Context context, ArrayList<TaxInfoReason> taxCountryList, OnClickTaxReasonListener listener) {
        this.context = context;
        this.data = taxCountryList;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tax_reason, parent, false);
        return new TaxReasonHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        TaxReasonHolder taxReasonHolder = (TaxReasonHolder) holder;
        TaxInfoReason residenceEntity = data.get(position);
        if (residenceEntity != null) {
            taxReasonHolder.tvTaxReason.setText(residenceEntity.getNoResidentReason());
            if (residenceEntity.isSelect()) {
                selectIndex = position;
                taxReasonHolder.ivSelectTax.setVisibility(View.VISIBLE);
            } else {
                taxReasonHolder.ivSelectTax.setVisibility(View.INVISIBLE);
            }
        }
        taxReasonHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    if (selectIndex > -1) {
                        TaxInfoReason lastEntity = data.get(selectIndex);
                        lastEntity.setSelect(false);
                    }
                    if (residenceEntity != null) {
                        selectIndex = position;
                        residenceEntity.setSelect(true);
                        listener.OnClickTaxReason(residenceEntity);
                    }
                    notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    public void setCurrentReason(TaxInfoReason currentReasonData) {
        this.currentReasonData = currentReasonData;
        if (data != null && currentReasonData != null) {
            for (TaxInfoReason taxInfoReason : data) {
                if (taxInfoReason.getNoResidentReasonCode().equals(currentReasonData.getNoResidentReasonCode())) {
                    taxInfoReason.setSelect(true);
                }
            }
        }
    }

}
