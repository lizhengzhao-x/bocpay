package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.CrossBorderRemittanceResultActivity;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.SmartAccountBillListAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.SmartAccountBillListCheckMoreAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.SmartAccountBillListTitleAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

import static cn.swiftpass.wallet.intl.entity.Constants.ACCOUNT_TYPE_SMART;
import static cn.swiftpass.wallet.intl.module.transactionrecords.view.OrderDetailActivity.ISFROMBOCPAY;
import static cn.swiftpass.wallet.intl.module.transactionrecords.view.OrderDetailActivity.SMARTENTITY;


public class TransacSmartAccountFragment extends BaseFragment {
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    @BindView(R.id.id_empty_view)
    View id_empty_view;


    private static final String TYPE_TITLE = "TYPE_TITLE";
    private static final String TYPE_CONTENT = "TYPE_CONTENT";

    private SmartAccountBillListEntity smartAccountBillListEntity;

    @BindView(R.id.id_nodata_image)
    ImageView idNodataImage;
    private List<SmartAccountBillListEntity.OutgroupBean> paymentEnquiryResults;
    private HashMap<String, List<SmartAccountBillListEntity.OutgroupBean>> paymentResults;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    private boolean isShowMoreMode = false;
//    /**
//     * 交易记录开关控制 显示新的UI 还是旧的ui
//     */
//    private boolean isUseNewUi;

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transac;
    }

    @Override
    public void initTitle() {
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    protected void initView(View v) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        idRecyclerview.addItemDecoration(itemDecoration);
        idRecyclerview.setAdapter(getAdapter(null));
        idRecyclerview.setEmptyView(id_empty_view);
        paymentEnquiryResults = new ArrayList<>();
        ((IAdapter<SmartAccountBillListEntity.OutgroupBean>) idRecyclerview.getAdapter()).setData(paymentEnquiryResults);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        paymentResults = new HashMap<>();
        getnTxnHistEnquirySmartCard();
//        id_empty_view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                getnTxnHistEnquirySmartCard();
//            }
//        });
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getnTxnHistEnquirySmartCard();
            }
        });
    }

    public void clearData() {
        if (paymentEnquiryResults != null && idRecyclerview != null && idRecyclerview.getAdapter() != null) {
            paymentEnquiryResults.clear();
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }


    private void getnTxnHistEnquirySmartCard() {
        showDialogNotCancel();
        ApiProtocolImplManager.getInstance().getnTxnHistEnquirySmartCard(getActivity(), "", new NetWorkCallbackListener<SmartAccountBillListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (getActivity() == null) return;
                showErrorMsgDialog(getActivity(), errorMsg);
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                }
                if (idRecyclerview != null && idRecyclerview.getAdapter() != null) {
                    idRecyclerview.getAdapter().notifyDataSetChanged();
                }
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
            }

            @Override
            public void onSuccess(SmartAccountBillListEntity response) {
                dismissDialog();
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                }
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                } else {
                    return;
                }
                if (response != null) {
                    smartAccountBillListEntity = response;
                    int size = smartAccountBillListEntity.getOrderList().getOutgroup().size();
                    if (isShowMoreMode) {
                        dealWithData(size, false);
                    } else {
                        //所有交易列表客户端根据日期进行分组处理
                        dealWithData(size > Constants.BILL_ITEM_PAGE_SIZE ? Constants.BILL_ITEM_PAGE_SIZE : size, size > Constants.BILL_ITEM_PAGE_SIZE ? true : false);
                    }

                }

            }
        });

    }


    private CommonRcvAdapter<SmartAccountBillListEntity.OutgroupBean> getAdapter(List<SmartAccountBillListEntity.OutgroupBean> data) {
        return new CommonRcvAdapter<SmartAccountBillListEntity.OutgroupBean>(data) {

            @Override
            public Object getItemType(SmartAccountBillListEntity.OutgroupBean demoModel) {
                return demoModel.getUiType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                if (type.equals(TYPE_TITLE)) {
                    return new SmartAccountBillListTitleAdapter(getContext());
                } else if (type.equals(Constants.TYPE_CHECK_MORE)) {
                    return new SmartAccountBillListCheckMoreAdapter(getContext(), new SmartAccountBillListCheckMoreAdapter.CheckMoreItemCallback() {
                        @Override
                        public void checkMoreClick() {
                            super.checkMoreClick();
                            TransactionManagementFragment transactionManagementFragment = (TransactionManagementFragment) getParentFragment();
                            transactionManagementFragment.checkMoreEvent();
                        }
                    });
                } else {
                    return new SmartAccountBillListAdapter(getContext(), new CardItemCallback() {

                        @Override
                        public void onItemCardClick(int position) {
                            super.onItemCardClick(position);
                            if (!ButtonUtils.isFastDoubleClick()) {
                                String recordType = paymentEnquiryResults.get(position).getRecordType();
                                if (!TextUtils.isEmpty(recordType)) {
                                    if (recordType.equals("1")) {
                                        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                                        mHashMapsLogin.put(Constants.ACCOUNT_TYPE, ACCOUNT_TYPE_SMART);
                                        mHashMapsLogin.put(Constants.OUTTRFREFNO, paymentEnquiryResults.get(position).getOuttrfrefno());
                                        mHashMapsLogin.put(Constants.TXNTYPE, paymentEnquiryResults.get(position).getTxnType());
                                        mHashMapsLogin.put(Constants.SMART_TYPE, true);
                                        ActivitySkipUtil.startAnotherActivity(getActivity(), CrossBorderRemittanceResultActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                    } else {
                                        jumpToOrderDetail(paymentEnquiryResults.get(position));
                                    }
                                } else {
                                    jumpToOrderDetail(paymentEnquiryResults.get(position));
                                }
                            }
                        }
                    });
                }

            }
        };
    }


    private void jumpToOrderDetail(SmartAccountBillListEntity.OutgroupBean transferInfosBean) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(SMARTENTITY, transferInfosBean);
        mHashMapsLogin.put(ISFROMBOCPAY, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), OrderDetailActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    /**
     * @param sizeIndex
     * @param addCheckMore
     */
    private void dealWithData(int sizeIndex, boolean addCheckMore) {
        paymentEnquiryResults.clear();
        paymentResults.clear();
        for (int i = 0; i < sizeIndex; i++) {
            SmartAccountBillListEntity.OutgroupBean paymentEnquiryResult = null;
            paymentEnquiryResult = smartAccountBillListEntity.getOrderList().getOutgroup().get(i);


            paymentEnquiryResult.setUiType(TYPE_CONTENT);
            String transDate = paymentEnquiryResult.getOuttxdate().trim();
            if (!TextUtils.isEmpty(transDate) && transDate.length() > 4) {
                String items[] = transDate.split("-");
                String monthTtitle = items[1] + "/" + items[0];
                if (paymentResults.containsKey(monthTtitle)) {
                    List<SmartAccountBillListEntity.OutgroupBean> paymentEnquiryResults = paymentResults.get(monthTtitle);
                    paymentEnquiryResults.add(paymentEnquiryResult);
                } else {
                    SmartAccountBillListEntity.OutgroupBean paymentEnquiryTitle = new SmartAccountBillListEntity.OutgroupBean();
                    paymentEnquiryTitle.setUiTitle(monthTtitle);
                    paymentEnquiryTitle.setUiType(TYPE_TITLE);
                    paymentEnquiryResults.add(paymentEnquiryTitle);

                    List<SmartAccountBillListEntity.OutgroupBean> paymentEnquiryResultItems = new ArrayList<>();
                    paymentEnquiryResultItems.add(paymentEnquiryResult);
                    paymentResults.put(monthTtitle, paymentEnquiryResultItems);
                }
                paymentEnquiryResults.add(paymentEnquiryResult);
            } else {
                paymentEnquiryResults.add(paymentEnquiryResult);
            }
        }
        if (addCheckMore) {
            SmartAccountBillListEntity.OutgroupBean orderQueryEntity = new SmartAccountBillListEntity.OutgroupBean();
            orderQueryEntity.setUiType(Constants.TYPE_CHECK_MORE);
            paymentEnquiryResults.add(orderQueryEntity);
        }
        if (idRecyclerview.getAdapter() != null) {
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }

    }


    public void updateCheckMoreUI() {
        if (smartAccountBillListEntity != null) {
            if (smartAccountBillListEntity.getOrderList().getOutgroup().size() > Constants.BILL_ITEM_PAGE_SIZE) {
                dealWithData(smartAccountBillListEntity.getOrderList().getOutgroup().size(), false);
            }
        }
        isShowMoreMode = true;
    }

//    public void updateList() {
//        if (smartRefreshLayout != null && paymentEnquiryResults != null && paymentEnquiryResults.isEmpty()) {
//            smartRefreshLayout.post(new Runnable() {
//                @Override
//                public void run() {
//                    smartRefreshLayout.autoRefresh();
//                    getnTxnHistEnquirySmartCard();
//                }
//            });
//        }
//    }
}
