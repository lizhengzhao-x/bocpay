package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 判断派利是按钮是否显示
 */
public class JudgeLishiShowProtocol extends BaseProtocol {

    @Override
    public boolean isNeedLogin() {
        return false;
    }


    public JudgeLishiShowProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferSign";
    }


}
