package cn.swiftpass.wallet.intl.module.transfer.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import java.util.HashMap;
import java.util.List;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountTopUpActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountWithdrawActivity;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferFillInfoActivity;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferWithLiShiActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * Created by ZhangXinchao on 2019/12/25.
 */
public class TransferUtils {


    /**
     * 点击提现的统一处理，因为有可能是Pa账户 不能直接增值需要绑定EDDA
     *
     * @param mSmartAccountInfo
     * @param regEddaEventListener
     */
    public static void onWithdrawEvent(MySmartAccountEntity mSmartAccountInfo, RegEddaEventListener regEddaEventListener) {
        if (mSmartAccountInfo == null || TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel())) {
            return;
        }
        if (!TextUtils.isEmpty(mSmartAccountInfo.getRelevanceAccNo()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DATA_SMART_INFO_ACTION, mSmartAccountInfo);
            mHashMaps.put(Constants.IS_FIRST_WITHDRAW, false);
            ActivitySkipUtil.startAnotherActivity(regEddaEventListener.getContext(), SmartAccountWithdrawActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            sTwoWithDrawEvent(mSmartAccountInfo, regEddaEventListener);
        }
    }


    /**
     * 点击增值的统一处理，因为有可能是Pa账户 不能直接增值需要绑定EDDA
     *
     * @param mSmartAccountInfo
     * @param regEddaEventListener
     */
    public static void checkTopUpEvent(MySmartAccountEntity mSmartAccountInfo, RegEddaEventListener regEddaEventListener) {
        if (mSmartAccountInfo == null) return;
        if (TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel())) return;
        if (!TextUtils.isEmpty(mSmartAccountInfo.getRelevanceAccNo()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
            mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
            mHashMaps.put(Constants.ACCTYPE, mSmartAccountInfo.getAccType());
            mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(regEddaEventListener.getContext(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            topUpAccountEvent(mSmartAccountInfo, regEddaEventListener);
        }
    }


    private static void sTwoWithDrawEvent(MySmartAccountEntity mSmartAccountInfo, RegEddaEventListener regEddaEventListener) {
        List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
        if (bankCardsBeans != null && bankCardsBeans.size() > 0 && !TextUtils.isEmpty(bankCardsBeans.get(0).getStatus())) {
            //Edda状态，N：没有登记过，A：登记成功 R：登记失败 	1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
            if (bankCardsBeans.get(0).getEddaStatus().equals("A") || bankCardsBeans.get(0).getEddaStatus().equals("3") || bankCardsBeans.get(0).getEddaStatus().equals("E")) {
                if (bankCardsBeans.get(0).getStatus().equals("2") || bankCardsBeans.get(0).getStatus().equals("3") || bankCardsBeans.get(0).getStatus().equals("4")) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.DATA_SMART_INFO_ACTION, mSmartAccountInfo);
                    mHashMaps.put(Constants.IS_FIRST_WITHDRAW, !bankCardsBeans.get(0).getStatus().equals("4"));
                    mHashMaps.put(Constants.ACCTYPE, bankCardsBeans.get(0).getBankName());
                    ActivitySkipUtil.startAnotherActivity(regEddaEventListener.getContext(), SmartAccountWithdrawActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (bankCardsBeans.get(0).getStatus().equals("1")) {
                    onWarningWithDrawEventTopUp(mSmartAccountInfo, bankCardsBeans.get(0).getBankName(), regEddaEventListener);
                } else {
                    onWarningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.string_edda_fail), regEddaEventListener);
                }
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("0") || bankCardsBeans.get(0).getEddaStatus().equals("1")) {
                regEddaEventListener.showErrorDialog("", regEddaEventListener.getContext().getString(R.string.string_edda_loading));
            } else {
                onWarningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.string_edda_fail), regEddaEventListener);
            }
        } else {
            onWarningWithDrawEvent(regEddaEventListener);
        }
    }

    private static void onWarningWithDrawEvent(RegEddaEventListener regEddaEventListener) {
        AndroidUtils.showTipDialog(regEddaEventListener.getContext(), regEddaEventListener.getContext().getString(R.string.P3_D1_1_1), regEddaEventListener.getContext().getString(R.string.P3_ewa_01_041), regEddaEventListener.getContext().getString(R.string.P3_D1_2_1), regEddaEventListener.getContext().getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                regEddaEventListener.getRegEddaInfo();
            }
        });
    }

    /**
     * s2用户绑定银行卡
     */
    private static void onWarningTopUpWithBindAccount(String message, RegEddaEventListener regEddaEventListener) {
        AndroidUtils.showTipDialog(regEddaEventListener.getContext(), regEddaEventListener.getContext().getString(R.string.P3_D1_1_1), message, regEddaEventListener.getContext().getString(R.string.P3_D1_1_3), regEddaEventListener.getContext().getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                regEddaEventListener.getRegEddaInfo();
            }
        });
    }


    private static void onWarningWithDrawEventTopUp(final MySmartAccountEntity mSmartAccountInfo, final String bankName, RegEddaEventListener regEddaEventListener) {
        AndroidUtils.showTipDialog(regEddaEventListener.getContext(), regEddaEventListener.getContext().getString(R.string.P3_D1_1_1), regEddaEventListener.getContext().getString(R.string.P3_ewa_01_041), regEddaEventListener.getContext().getString(R.string.P3_D1_2_1), regEddaEventListener.getContext().getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
                mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
                mHashMaps.put(Constants.ACCTYPE, bankName);
                mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
                ActivitySkipUtil.startAnotherActivity(regEddaEventListener.getContext(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    private static void topUpAccountEvent(MySmartAccountEntity mSmartAccountInfo, final RegEddaEventListener regEddaEventListener) {
        List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
        if (bankCardsBeans != null && bankCardsBeans.size() > 0 && !TextUtils.isEmpty(bankCardsBeans.get(0).getStatus())) {
            //Edda状态，N：没有登记过，A：登记成功 R：登记失败 1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
            // E:删除失败 C:删除成功 4:删除申请失败 9删除申请处理中 3提交删除申请成功
            if (bankCardsBeans.get(0).getEddaStatus().equals("A") || bankCardsBeans.get(0).getEddaStatus().equals("E") || bankCardsBeans.get(0).getEddaStatus().equals("3")) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
                mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
                mHashMaps.put(Constants.ACCTYPE, bankCardsBeans.get(0).getBankName());
                mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
                ActivitySkipUtil.startAnotherActivity(regEddaEventListener.getContext(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("N")) {
                onWainningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.P3_D1_1_2), regEddaEventListener);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("R")) {
                onWainningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.string_edda_fail), regEddaEventListener);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("0") || bankCardsBeans.get(0).getEddaStatus().equals("1")) {
                regEddaEventListener.showErrorDialog("", regEddaEventListener.getContext().getString(R.string.string_edda_loading));
            } else {
                onWainningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.string_edda_fail), regEddaEventListener);
            }
        } else {
            onWainningTopUpWithBindAccount(regEddaEventListener.getContext().getString(R.string.P3_D1_1_2), regEddaEventListener);
        }

    }

    /**
     * s2用户绑定银行卡
     */
    private static void onWainningTopUpWithBindAccount(String message, RegEddaEventListener regEddaEventListener) {
        AndroidUtils.showTipDialog(regEddaEventListener.getContext(),
                regEddaEventListener.getContext().getString(R.string.P3_D1_1_1),
                message,
                regEddaEventListener.getContext().getString(R.string.P3_D1_1_3),
                regEddaEventListener.getContext().getString(R.string.string_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        regEddaEventListener.getRegEddaInfo();
                    }
                });
    }


    /**
     * @param activity
     * @param isTransferNormalType
     * @param phone
     * @param name
     * @param userRelName          最近转账 需要将用户的昵称传过去
     * @param isEmail
     */
    public static void transferWithPhoneNumber(Activity activity, boolean isTransferNormalType, String phone, String name, String userRelName, boolean isEmail) {
        phone = phone.replace("-", "");
        if (isEmail) {
            if (isTransferNormalType) {
                TransferFillInfoActivity.startActivityWithPhoneNumber(activity, "", phone, name, userRelName, isEmail);
            } else {
                TransferWithLiShiActivity.startActivityWithPhoneNumber(activity, "", phone, name, userRelName, isEmail);
            }
        } else {
            String phoneNumberStr = phone;
            String phoneAreaCodeStr = "";
            String items[] = {"+86", "86", "852", "+852", "853", "+853"};
            for (int i = 0; i < items.length; i++) {
                if (phone.startsWith(items[i])) {
                    phoneAreaCodeStr = items[i];
                    phoneNumberStr = phone.substring(items[i].length());
                    break;
                }
            }
            if (!TextUtils.isEmpty(phoneAreaCodeStr)) {
                if (!phoneAreaCodeStr.contains("+")) {
                    phoneAreaCodeStr = "+" + phoneAreaCodeStr;
                }
            }
            if (isTransferNormalType) {
                TransferFillInfoActivity.startActivityWithPhoneNumber(activity, phoneAreaCodeStr, phoneNumberStr, name, userRelName);
            } else {
                TransferWithLiShiActivity.startActivityWithPhoneNumber(activity, phoneAreaCodeStr, phoneNumberStr, name, userRelName);
            }
        }
    }

    public interface RegEddaEventListener {
        void getRegEddaInfo();

        Activity getContext();

        void showErrorDialog(String errorCode, String message);
    }


}
