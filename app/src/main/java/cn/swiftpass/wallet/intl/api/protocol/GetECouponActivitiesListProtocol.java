package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class GetECouponActivitiesListProtocol extends BaseProtocol {


    public GetECouponActivitiesListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/promotions/getActivities";
    }


}
