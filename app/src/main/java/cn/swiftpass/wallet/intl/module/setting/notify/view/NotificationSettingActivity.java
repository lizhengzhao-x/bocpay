package cn.swiftpass.wallet.intl.module.setting.notify.view;

import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.SettingContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.SettingPresenter;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

public class NotificationSettingActivity extends BaseCompatActivity<SettingContract.Presenter> implements SettingContract.View {
    @BindView(R.id.layout_open_notification)
    ImageArrowView idOpenNotification;

    @BindView(R.id.id_sub_text)
    TextView subTextTv;


    private boolean idCurrentLocalStatusOpen;

    private boolean isLocalNotificationStatus;

//    /**
//     * 点击设定去通知中打开通知开关
//     */
//    private boolean isGoSettingOpen;

    @Override
    protected void init() {
        setToolBarTitle(R.string.NT2101_6_6);
        idOpenNotification.getImageRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.getNotificationStatus(true, idCurrentLocalStatusOpen ? NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE : NotificationStatusEntity.NOTIFICATIONSTATUSOPEN, NotificationStatusEntity.NOTIFICATIONACTIONUPDATE);
//                isLocalNotificationStatus = CheckNotifySwitchUtils.checkNotifySetting(NotificationSettingActivity.this);
//                if (isLocalNotificationStatus) {
//                    mPresenter.getNotificationStatus(true, idCurrentLocalStatusOpen ? NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE : NotificationStatusEntity.NOTIFICATIONSTATUSOPEN, NotificationStatusEntity.NOTIFICATIONACTIONUPDATE);
//                } else {
//                    showOpenNotificationStatusDialog();
//                }
            }
        });
        idOpenNotification.getImageRight().setImageResource(R.mipmap.button_switch_off_s);

        subTextTv.setText(getString(R.string.NT2101_6_4) + "/n" + getString(R.string.NT2101_6_5));
    }


    /**
     * 页面每次可见需要更新状态 有可能从系统里头打开关闭开关返回界面
     */
    private void updateNotificationStatus() {
        mPresenter.getNotificationStatus(true, isLocalNotificationStatus ? NotificationStatusEntity.NOTIFICATIONSTATUSOPEN : NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE, NotificationStatusEntity.NOTIFICATIONACTIONQUERY);

//        boolean isLocalNotificationStatus = CheckNotifySwitchUtils.checkNotifySetting(this);
//        if (isGoSettingOpen && isLocalNotificationStatus) {
//            //点击设定去系统设定里头打开开关回来到界面时候需要更新按钮状态
//            mPresenter.getNotificationStatus(false, NotificationStatusEntity.NOTIFICATIONSTATUSOPEN, NotificationStatusEntity.NOTIFICATIONACTIONUPDATE);
//        } else {
//            if (isLocalNotificationStatus) {
//                //如果系统是开的 就走查询状态流程
//                mPresenter.getNotificationStatus(false, isLocalNotificationStatus ? NotificationStatusEntity.NOTIFICATIONSTATUSOPEN : NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE, NotificationStatusEntity.NOTIFICATIONACTIONQUERY);
//            } else {
//                //如果系统是关的的 就走更新状态流程 默认app一定要关掉
//                mPresenter.getNotificationStatus(false, NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE, NotificationStatusEntity.NOTIFICATIONACTIONUPDATE);
//            }
//        }
//        isGoSettingOpen = false;
    }

//    private void showOpenNotificationStatusDialog() {
//        AndroidUtils.showTipDialog(NotificationSettingActivity.this, getString(R.string.NT2101_1_1), getString(R.string.NT2101_1_2),
//                getString(R.string.LBS209_1_4), getString(R.string.LBS209_8_4), new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        isGoSettingOpen = true;
//                        CheckNotifySwitchUtils.tryJumpNotifyPage(NotificationSettingActivity.this);
//
//                    }
//                }, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//
//                    }
//                });
//
//    }

    @Override
    protected void onResume() {
        super.onResume();
        updateNotificationStatus();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_notificationsetting;
    }

    @Override
    protected SettingContract.Presenter createPresenter() {
        return new SettingPresenter();
    }

    @Override
    public void getNotificationStatusSuccess(NotificationStatusEntity notificationStatusEntity) {
        boolean isOpenStatus = notificationStatusEntity.isOpenStatus();
        idCurrentLocalStatusOpen = isOpenStatus;
        idOpenNotification.getImageRight().setImageResource(isOpenStatus ? R.mipmap.button_switch_on_s : R.mipmap.button_switch_off_s);
    }

    @Override
    public void getNotificationStatusFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);

    }
}
