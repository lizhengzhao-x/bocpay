package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 转账查看银行列表
 */

public class GetBankListProtocol extends BaseProtocol {
    public static final String TAG = GetBankListProtocol.class.getSimpleName();

    private String currency;
    private String name;
    private String code;

    public GetBankListProtocol(String currency, String name, String code, NetWorkCallbackListener dataCallback) {
        this.currency = currency;
        this.name = name;
        this.code = code;
        this.mDataCallback = dataCallback;
        mUrl = "api/bankList/getBankList";
    }

    @Override
    public void packData() {
        super.packData();
//        mBodyParams.put(RequestParams.CURRENCY, currency);
//        mBodyParams.put(RequestParams.NAME, name);
//        mBodyParams.put(RequestParams.CODE, code);
    }
}
