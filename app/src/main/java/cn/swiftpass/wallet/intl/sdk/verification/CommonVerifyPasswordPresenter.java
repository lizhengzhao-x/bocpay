package cn.swiftpass.wallet.intl.sdk.verification;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckPasswordProtocol;

public class CommonVerifyPasswordPresenter extends BasePresenter<CommonVerifyPasswordContract.View> implements  CommonVerifyPasswordContract.Presenter {
    @Override
    public void getCheckPasswordWithAction(String payPwd, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckPasswordProtocol(payPwd, action, -1, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCheckPasswordWithActionError(errorCode, errorMsg);
                }


            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCheckPasswordWithActionSuccess(response);
                }
            }
        }).execute();
    }
}
