package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferContactContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferContactPresenter;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.RecentlyContactAdapter;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * 最近联系人
 */
public class RecentlyContactFragment extends BaseTransferSelFragment<ContractListEntity.RecentlyBean, TransferContactContract.Presenter> implements TransferContactContract.View {
    private static final String TAG = "CommonCollectionContactFrament";
    @BindView(R.id.ry_contact)
    RecyclerView ryContact;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    private final int TOTAL_SIZE = 10;
    private RecentlyContactAdapter recentlyContactAdapter;

    private View footView;

    private ContactListRefresh contactListRefresh;


    public void setOnContactListRefresh(ContactListRefresh contactListRefresh) {
        this.contactListRefresh = contactListRefresh;
    }

    public List<ContractListEntity.RecentlyBean> getItemCommonCollectionList() {
        return itemCommonCollectionList;
    }

    private List<ContractListEntity.RecentlyBean> itemCommonCollectionList = new ArrayList<>();

    public static RecentlyContactFragment newInstance() {
        return new RecentlyContactFragment();
    }


    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {

    }

    @Override
    public void onStart() {
        super.onStart();
        if (getView() != null) {
            getView().setBackgroundColor(Color.TRANSPARENT);
        }
    }

    /**
     * 页面添加空布局  直接用该方法
     *
     * @param mContext
     * @param recyclerView
     * @param textId
     * @param imgId
     * @return
     */
    protected View getEmptyView(Context mContext, RecyclerView recyclerView, int textId, int imgId) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_top_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(textId));
        if (imgId == -1) {
            noDataImg.setVisibility(View.GONE);
        } else {
            noDataImg.setVisibility(View.VISIBLE);
            noDataImg.setImageResource(imgId);
        }
        return emptyView;
    }


    @Override
    protected TransferContactContract.Presenter loadPresenter() {
        return new TransferContactPresenter();
    }

    @Override
    public void getContractListSuccess(ContractListEntity contractListEntity) {
        itemCommonCollectionList.clear();
        emptyView.setVisibility(View.GONE);
        if (contractListEntity != null) {
            List<ContractListEntity.RecentlyBean> list = contractListEntity.getRecently();
            if (list != null) {
                int totalSize = Math.min(list.size(), TOTAL_SIZE);
                if (totalSize == 0) {
                    if (emptyView != null) {
                        emptyView.setVisibility(View.VISIBLE);
                    }
                } else {
                    itemCommonCollectionList.addAll(list.subList(0, totalSize));
                    for (int i = 0; i < totalSize; i++) {
                        ContractListEntity.RecentlyBean recentlyBean = itemCommonCollectionList.get(i);
                        recentlyBean.setEmail("1".equals(recentlyBean.getType()));
                    }
                    if (totalSize >= TOTAL_SIZE) {
                        footView.setVisibility(View.VISIBLE);
                    } else {
                        footView.setVisibility(View.GONE);
                    }
                }
            }
        }
        recentlyContactAdapter.setDataList(itemCommonCollectionList);
        swipeRefreshLayout.setRefreshing(false);
        if (null != contactListRefresh) {
            contactListRefresh.onSuccess();
        }
    }

    @Override
    public void getContractListError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            if (isTransferNormalType) {
                AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifyPwdToUpdateAccount();
                    }
                });
            } else {
                AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_13), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        verifyPwdToUpdateAccount();
                    }
                });
            }
        } else {
            showErrorMsgDialog(getActivity(), errorMsg);
        }
        swipeRefreshLayout.setRefreshing(false);

        itemCommonCollectionList.clear();
        recentlyContactAdapter.setDataList(itemCommonCollectionList);

        if (null != contactListRefresh) {
            contactListRefresh.onFail();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_common_collection_contact_view;
    }

    @Override
    protected void initView(View v) {

        LogUtils.i(TAG, "initView CommonCollectionContactFrament--->");

        recentlyContactAdapter = new RecentlyContactAdapter(itemCommonCollectionList);
        recentlyContactAdapter.bindToRecyclerView(ryContact);
        initRecycleView(ryContact, swipeRefreshLayout, recentlyContactAdapter);
        if (getEmptyView() != null) {
            getEmptyView().setVisibility(View.GONE);
        }
        footView = LayoutInflater.from(getContext()).inflate(R.layout.item_common_collection_title_view, (ViewGroup) ryContact.getParent(), false);
        recentlyContactAdapter.addFooterView(footView);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                //swipeRefreshLayout.setRefreshing(true);
                getContractList();
            }
        });


        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        ryContact.addItemDecoration(itemDecoration);

        recentlyContactAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                if (ButtonUtils.isFastDoubleClick()) return;
                if (itemCommonCollectionList.get(position).isEmail()) {
                    itemCommonCollectionList.get(position).setPhoneNo(itemCommonCollectionList.get(position).getEmail());
                }
                startAnotherActivity(getActivity(), isTransferNormalType, itemCommonCollectionList.get(position).getPhoneNo(), itemCommonCollectionList.get(position).getDisCustName(), itemCommonCollectionList.get(position).isHasChanged() ? itemCommonCollectionList.get(position).getDisCustName() : null, itemCommonCollectionList.get(position).isEmail());
            }
        });
    }

    public void getContractList() {
        mPresenter.getContractList();
    }


    @Override
    public void updateItemList() {
        getContractList();
    }

    @Override
    protected void updateContactList(ArrayList<ContactEntity> contactEntities, boolean obtain) {

    }

    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(),false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (getActivity() == null) {
                    return;
                }
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                    }
                }, 300);

            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {


                    }
                });
            }

            @Override
            public void onVerifyCanceled() {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
            }
        });
    }

    public interface ContactListRefresh {
        void onSuccess();

        void onFail();
    }

}

