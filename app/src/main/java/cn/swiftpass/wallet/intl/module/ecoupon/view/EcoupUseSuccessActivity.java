package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/8/12.
 */
public class EcoupUseSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.tv_payment)
    TextView tvPayment;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC13_1));

        setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
        hideBackIcon();
        tvPayment.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                inoutPswPopWindow();
            }
        });
        getToolBarRightView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                finish();
            }
        });
    }


    /**
     * 密码弹出框
     */
    public void inoutPswPopWindow() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    private void verifyResultSuc() {
//        HashMap<String, Object> mHashMaps = new HashMap<>();
////        mHashMaps.put(Constants.SCAN_CARD_TYPE, Constants.SCAN_CARD_TYPE_BACK);
////        ActivitySkipUtil.startAnotherActivity(this, ScanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        EventBus.getDefault().postSticky(new UplanEntity(UplanEntity.EVENT_UPLAN_BACK_SCAN, ""));
        finish();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_ecou_use;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
}
