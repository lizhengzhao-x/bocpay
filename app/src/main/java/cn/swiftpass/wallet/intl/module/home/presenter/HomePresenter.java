package cn.swiftpass.wallet.intl.module.home.presenter;

import androidx.annotation.Nullable;
import androidx.core.util.Consumer;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.AccountPointDataEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetLifeSceneAreaCountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetNewMsgDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.InitBannerProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryAccountDataProtocol;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.HomeLifeSceneAreaCountEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;
import cn.swiftpass.wallet.intl.module.home.contract.HomeContract;

public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {


    private boolean isQueryAccountPointDataRunning = false;
    private boolean isQuerySmartAccountDataRunning = false;


    @Override
    public void queryAccountPointData(boolean iShow) {
        //先判断判断是否需要loading，再是否需要退出请求

        if (getView() != null && iShow) {
            getView().showDialogNotCancel();
        }
        if (isQueryAccountPointDataRunning)
            return;
        isQueryAccountPointDataRunning = true;
        new QueryAccountDataProtocol(new NetWorkCallbackListener<AccountPointDataEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                isQueryAccountPointDataRunning = false;
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryAccountPointDataError(errorCode, errorMsg,iShow);
                }

            }

            @Override
            public void onSuccess(AccountPointDataEntity response) {
                isQueryAccountPointDataRunning = false;
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryAccountPointDataSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void checkSetLimitBalance() {
        if (getView() != null ) {
            getView().showDialogNotCancel();
        }

        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkSetLimitBalanceError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {

                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkSetLimitBalanceSuccess(response);
                }
            }
        })).execute();
    }

    @Override
    public void getMySmartAccountInfo(boolean iShow, @Nullable Consumer<MySmartAccountEntity> consumerSuccess,@Nullable Consumer<String> consumerFail) {
        if (getView() != null && iShow) {
            getView().showDialogNotCancel();
        }
        if (isQuerySmartAccountDataRunning)
            return;
        isQuerySmartAccountDataRunning = true;
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                isQuerySmartAccountDataRunning=false;
                if (getView() != null) {
                    getView().dismissDialog();
                    if (consumerFail!=null){
                        consumerFail.accept(errorCode);
                    }
                    getView().getMySmartAccountInfoError(errorCode, errorMsg,iShow);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                isQuerySmartAccountDataRunning=false;
                if (getView() != null) {
                    getView().dismissDialog();
                    if (consumerSuccess!=null){
                        consumerSuccess.accept(response);
                    }
                    getView().getMySmartAccountInfoSuccess(response);
                }
            }
        })).execute();
    }

    @Override
    public void getNewMsgList(boolean action) {
        new GetNewMsgDataProtocol(true, new NetWorkCallbackListener<NewMsgListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getNewMsgListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NewMsgListEntity response) {
                if (getView() != null) {
                    getView().getNewMsgListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getLifeSceneAreaCount() {
        new GetLifeSceneAreaCountProtocol(new NetWorkCallbackListener<HomeLifeSceneAreaCountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getLifeSceneAreaCountError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(HomeLifeSceneAreaCountEntity response) {
                if (getView() != null) {
                    getView().getLifeSceneAreaCountSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void refreshLifeMenu() {

        new InitBannerProtocol(new NetWorkCallbackListener<InitBannerEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().refreshLifeMenuError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(InitBannerEntity response) {
                if (getView() != null) {
                    getView().refreshLifeMenuSuccess(response);
                }
            }
        }).execute();
    }


}
