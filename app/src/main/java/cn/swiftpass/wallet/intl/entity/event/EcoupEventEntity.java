package cn.swiftpass.wallet.intl.entity.event;


public class EcoupEventEntity extends BaseEventEntity{
    public static final int EVENT_FINISH_VONVERT_ECOUP = 1001;
    public static final int EVENT_CAME_MY_ECOUP = 1002;
    public static final int EVENT_RETRY_COUNT = 19;
    public static final int EVENT_RETRY_REFRESH_CNT = 3002;


    public EcoupEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public EcoupEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
