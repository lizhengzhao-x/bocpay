package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartAccountListEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ChangeDefaultAccountActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


public class SmartAccountListActivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    private List<SmartAccountListEntity.RowsBean> rows;
    private SmartAccountListEntity smartAccountListEntity;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.primary_account);
        rows = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        ((IAdapter<SmartAccountListEntity.RowsBean>) idRecyclerview.getAdapter()).setData(rows);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        EventBus.getDefault().register(this);
        getAccountList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smartaccount_list;
    }


    private void getAccountList() {
        ApiProtocolImplManager.getInstance().getSmartAccount(mContext, new NetWorkCallbackListener<SmartAccountListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onSuccess(SmartAccountListEntity response) {
                if (response != null) {
                    String accountNo = getIntent().getExtras().getString(Constants.CURRENT_SEL_ACCOUNT_NO);
                    accountNo = accountNo.replace("-", "");
                    smartAccountListEntity = response;
                    for (int i = 0; i < smartAccountListEntity.getRows().size(); i++) {
                        String smartAccountNo = AndroidUtils.subPaymentMethod(smartAccountListEntity.getRows().get(i).getAccountNo().replace("-", ""));
                        if (accountNo.contains(smartAccountNo)) {
                            smartAccountListEntity.getRows().get(i).setSel(true);
                            break;
                        }
                    }
                    rows.clear();
                    rows.addAll(smartAccountListEntity.getRows());
                    idRecyclerview.getAdapter().notifyDataSetChanged();
                }
            }
        });

    }


    private CommonRcvAdapter<SmartAccountListEntity.RowsBean> getAdapter(final List<SmartAccountListEntity.RowsBean> data) {
        return new CommonRcvAdapter<SmartAccountListEntity.RowsBean>(data) {

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new SmartAccountListAdapter(mContext, new SmartAccountListAdapter.AccountItemCallback() {
                    @Override
                    public void onItemAccountClick(int position) {
                        super.onItemAccountClick(position);
                        if (ButtonUtils.isFastDoubleClick()) return;
                        SmartAccountListEntity.RowsBean rowsBean = null;
                        if (smartAccountListEntity != null && smartAccountListEntity.getRows().size() > 0) {
                            if (position < smartAccountListEntity.getRows().size()) {
                                rowsBean = smartAccountListEntity.getRows().get(position);
                            }
                        }
                        if (rowsBean != null && !rowsBean.isSel()) {
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.EXTRA_ACCOUNT_INFO, rowsBean);
                            mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
                            ActivitySkipUtil.startAnotherActivity(SmartAccountListActivity.this, ChangeDefaultAccountActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }

                    }
                });
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
