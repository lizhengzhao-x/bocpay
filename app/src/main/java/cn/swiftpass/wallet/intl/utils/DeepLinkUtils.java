package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferBaseDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkSkipEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpgradeAccountActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSmartAccountDescActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetBusinessResultsProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.BusinessResultsEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity;
import cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteShareActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CAMPAIGNID;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.IMAGEURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.TERMINFOURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.BANKCARDTYPE;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.URL_REWARD;
import static cn.swiftpass.wallet.intl.utils.DialogUtils.showErrorMsgDialog;

/**
 * @author lizheng.zhao
 */
public class DeepLinkUtils {

    public static final int SKIP_TYPE_WEB_VIEW = 100;
    public static final int SKIP_TYPE_SPLASH = 200;
    public static final int SKIP_TYPE_MAIN_HOME = 300;

    public static final int TYPE_REMITTANCE_UPDATE = 2001;
    public static final int TYPE_REMITTANCE_BIND = 2002;

    public static final int TYPE_REMITTANCE_CLOSE_MSG_PAGE = 3001;

    /**
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=mgm
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=uplan
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=evoucher
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=remittance
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=ccRegisterPromotion&campaignId=SPENDANDWIN2021APR
     * https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=uplan&merchantInfo=259803442892541
     *
     * <p>
     * DEEP_LINK_TYPE_MGM                推荐亲友页面（二级界面） --- pageId = mgm
     * DEEP_LINK_TYPE_U_PLAN             优计划首页（二级界面） --- pageId = pageId = uplan
     * DEEP_LINK_TYPE_E_COUPON           电子礼券页面 --- pageId = evoucher
     * DEEP_LINK_TYPE_CROSS_BORDER       跨境汇款页面 --- pageId = pageId = remittance
     * DEEP_LINK_TYPE_REWARD_REGISTER    信用卡奖赏登记详情页（二级界面） --- pageId = ccRegisterPromotion
     * DEEP_LINK_TYPE_U_PLAN_STORE       优计划指定商户页面 --- pageId = uplan 携带参数 merchantInfo
     */
    public static final int DEEP_LINK_TYPE_MGM = 1001;
    public static final int DEEP_LINK_TYPE_E_COUPON = 1002;
    public static final int DEEP_LINK_TYPE_CROSS_BORDER = 1003;
    public static final int DEEP_LINK_TYPE_REWARD_REGISTER = 1004;
    public static final int DEEP_LINK_TYPE_U_PLAN = 1005;
    public static final int DEEP_LINK_TYPE_U_PLAN_STORE = 1006;

    /**
     * 跳转类型做映射
     *
     * @param url 获取的H5的url
     * @return 跳转类型
     */
    public static int getLinkType(String url) {

        HashMap<String, String> map = getUrlParams(url);

        String pageId = map.get("pageId");

        if (TextUtils.equals(pageId, "mgm")) {
            return DEEP_LINK_TYPE_MGM;
        } else if (TextUtils.equals(pageId, "uplan")) {
            if (!TextUtils.isEmpty(map.get("merchantInfo"))) {
                return DEEP_LINK_TYPE_U_PLAN_STORE;
            }
            return DEEP_LINK_TYPE_U_PLAN;
        } else if (TextUtils.equals(pageId, "evoucher")) {
            return DEEP_LINK_TYPE_E_COUPON;
        } else if (TextUtils.equals(pageId, "remittance")) {
            return DEEP_LINK_TYPE_CROSS_BORDER;
        } else if (TextUtils.equals(pageId, "ccRegisterPromotion")) {
            return DEEP_LINK_TYPE_REWARD_REGISTER;
        } else {
            return -1;
        }
    }

    /**
     * 获取 URL中的参数 - (key, value)
     *
     * @param url 获取的H5的url
     * @return url中携带的参数的键值对
     */
    public static HashMap<String, String> getUrlParams(String url) {
        HashMap<String, String> resultMap = new HashMap<String, String>();
        if (!TextUtils.isEmpty(url) && url.contains("?") && url.contains("&") && url.contains("=")) {
            int index = url.indexOf("?");
            try {
                String params = url.substring(index + 1);
                String[] argAry = params.split("&");
                for (String arg : argAry) {
                    String[] keyValue = arg.split("=");
                    resultMap.put(keyValue[0], keyValue[1]);
                }
                return resultMap;
            } catch (ArrayIndexOutOfBoundsException e) {
                //处理数组越界
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }

        return resultMap;
    }


    /**
     * 判断是否是新需求中的 Deep Link
     * 新需求中的 url 才有ewaBoCPayNativeType字段，且值为JumpPage
     *
     * @param url 获取的H5的url
     * @return
     */
    public static boolean isNewDeepLink(String url) {
        HashMap<String, String> map = getUrlParams(url);
        if (TextUtils.equals(map.get("type"), "JumpPage")) {
            return true;
        }
        return false;
    }


    /**
     * 通过 link type 跳转到相应页面
     */
    public static void skipByDeepLinkType(String deepLinkUrl, Activity fromActivity, long startTime, int skipType) {
        if (skipType == SKIP_TYPE_WEB_VIEW) {
            //webView 界面跳转处理
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(AppCallAppLinkEntity.APP_LINK_URL, deepLinkUrl);
            int linkType = getLinkType(deepLinkUrl);
            switch (linkType) {
                case DEEP_LINK_TYPE_U_PLAN_STORE://UPlan指定门店 二级界面
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态
                        goToUPlanStore(deepLinkUrl, fromActivity);
                    } else {
                        //登出态
                        mHashMapsLogin.put(Constants.DETAIL_URL, deepLinkUrl);
                        ActivitySkipUtil.startAnotherActivity(fromActivity, LoginActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                case DEEP_LINK_TYPE_MGM://MGM二级界面
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态
                        ActivitySkipUtil.startAnotherActivity(fromActivity, InviteShareActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        //登出态
                        mHashMapsLogin.put(Constants.DETAIL_URL, deepLinkUrl);
                        ActivitySkipUtil.startAnotherActivity(fromActivity, LoginActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                case DEEP_LINK_TYPE_U_PLAN://U_PLAN二级界面
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态
                        getUPlanUrl(fromActivity);
                    } else {
                        //登出态
                        mHashMapsLogin.put(Constants.DETAIL_URL, deepLinkUrl);
                        ActivitySkipUtil.startAnotherActivity(fromActivity, LoginActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                case DEEP_LINK_TYPE_REWARD_REGISTER://REWARD_REGISTER二级界面
                    String campaignId = getUrlParams(deepLinkUrl).get("campaignId");
                    if (!TextUtils.isEmpty(campaignId)) {
                        String businessParam = "{\"campaignId\":\"" + campaignId + "\"}";
                        goToAppointRewardRegisterActivity(fromActivity, businessParam);
                    }
                    break;
                case DEEP_LINK_TYPE_E_COUPON:
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态
                        ActivitySkipUtil.startAnotherActivity(fromActivity, MainHomeActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    } else {
                        //登出态
                        mHashMapsLogin.put(Constants.DETAIL_URL, deepLinkUrl);
                        ActivitySkipUtil.startAnotherActivity(fromActivity, LoginActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                case DEEP_LINK_TYPE_CROSS_BORDER:
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态
                        goToRemittancePage(fromActivity, deepLinkUrl);
                    } else {
                        //登出态
                        mHashMapsLogin.put(Constants.DETAIL_URL, deepLinkUrl);
                        ActivitySkipUtil.startAnotherActivity(fromActivity, LoginActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                    break;
                case -1:
                    //报错
                    break;
            }
        } else if (skipType == SKIP_TYPE_SPLASH) {
            //SplashActivity界面跳转
            Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
            int linkType = getLinkType(deepLinkUrl);
            switch (linkType) {
                case DEEP_LINK_TYPE_U_PLAN_STORE://UPlan指定门店 二级界面
                    goToUPlanStore(deepLinkUrl, currentActivity);
                    break;
                case DEEP_LINK_TYPE_MGM://MGM二级界面
                    ActivitySkipUtil.startAnotherActivity(currentActivity, InviteShareActivity.class);
                    break;
                case DEEP_LINK_TYPE_U_PLAN://U_PLAN二级界面
                    getUPlanUrl(currentActivity);
                    break;
                case DEEP_LINK_TYPE_REWARD_REGISTER://REWARD_REGISTER二级界面
                    String campaignId = getUrlParams(deepLinkUrl).get("campaignId");
                    if (!TextUtils.isEmpty(campaignId)) {
                        String businessParam = "{\"campaignId\":\"" + campaignId + "\"}";
                        goToAppointRewardRegisterActivity(currentActivity, businessParam);
                    }
                    break;
                case DEEP_LINK_TYPE_E_COUPON:
                case DEEP_LINK_TYPE_CROSS_BORDER:
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(MainHomeActivity.IS_AUTO_LOGIN, !CacheManagerInstance.getInstance().isHasSessionId());
                    map.put(AppCallAppLinkEntity.APP_LINK_URL, deepLinkUrl);
                    ActivitySkipUtil.startAnotherActivity(currentActivity, MainHomeActivity.class, map, ActivitySkipUtil.ANIM_TYPE.NONE);
                    break;
                case -1:
                    //报错
                    break;
            }
        } else if (skipType == SKIP_TYPE_MAIN_HOME) {
            //MainHome页面跳转
            int linkType = getLinkType(deepLinkUrl);
            switch (linkType) {
                case DEEP_LINK_TYPE_U_PLAN_STORE://UPlan指定门店 二级界面
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_U_PLAN_STORE, deepLinkUrl));
                    break;
                case DEEP_LINK_TYPE_MGM://MGM二级界面
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_MGM, "MGM"));
                    break;
                case DEEP_LINK_TYPE_U_PLAN://U_PLAN二级界面
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_U_PLAN, "U_PLAN"));
                    break;
                case DEEP_LINK_TYPE_REWARD_REGISTER://REWARD_REGISTER二级界面
                    String campaignId = getUrlParams(deepLinkUrl).get("campaignId");
                    if (!TextUtils.isEmpty(campaignId)) {
                        String businessParam = "{\"campaignId\":\"" + campaignId + "\"}";
                        EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_REWARD_REGISTER, businessParam));
                    }
                    break;
                case DEEP_LINK_TYPE_E_COUPON:
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_E_COUPON, "E_COUPON"));
                    break;
                case DEEP_LINK_TYPE_CROSS_BORDER:
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_CROSS_BORDER, "CROSS_BORDER"));
                    break;
                case -1:
                    //报错
                    break;
            }
        }


    }


    /**
     * 跳转至UPlan页面时，需拉接口
     *
     * @param currentActivity
     */
    private static void getUPlanUrl(Activity currentActivity) {
        ((BaseCompatActivity) currentActivity).showDialogNotCancel();
        new GetUplanurlProtocol(new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ((BaseCompatActivity) currentActivity).dismissDialog();
                ((BaseCompatActivity) currentActivity).showErrorMsgDialog(currentActivity, errorMsg);
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                ((BaseCompatActivity) currentActivity).dismissDialog();
                if (response != null) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
                    mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
                    mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
                    mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
                    mHashMaps.put(UplanActivity.IS_FROM_DEEP_LINK, true);
                    ActivitySkipUtil.startAnotherActivity(currentActivity, UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        }).execute();
    }


    /**
     * 跨境货款
     *
     * @param fromActivity
     * @param deepLinkUrl
     */
    private static void goToRemittancePage(Activity fromActivity, String deepLinkUrl) {
        ((BaseCompatActivity) fromActivity).showDialogNotCancel();
        new GetTransferBaseDataProtocol(new NetWorkCallbackListener<TransferCrossBorderBaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ((BaseCompatActivity) fromActivity).dismissDialog();

                if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS)) {
                    //支付账户升级
                    String message = fromActivity.getString(R.string.CT1_1c_4) + " (" + errorCode + ")";
                    AndroidUtils.showBindSmartAccountAndCancelDialogTwo(fromActivity, message, fromActivity.getString(R.string.upgrade_account), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            inputPswPopWindow(fromActivity, TYPE_REMITTANCE_UPDATE, true);
                        }
                    });
                } else if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT_TWO) || TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS_TWO)) {
                    //信用卡绑定智能账户
                    AndroidUtils.showBindSmartAccountAndCancelDialogTwo(fromActivity, fromActivity.getString(R.string.CT1_1c_1) + " (" + errorCode + ")", fromActivity.getString(R.string.PU2107_1_3), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            inputPswPopWindow(fromActivity, TYPE_REMITTANCE_BIND, false);
                        }
                    });
                } else {
                    showErrorMsgDialog(fromActivity, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderBaseEntity response) {
                ((BaseCompatActivity) fromActivity).dismissDialog();
                if (response != null) {
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_CROSS_BORDER, "CROSS_BORDER", response));
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(TYPE_REMITTANCE_CLOSE_MSG_PAGE, "CLOSE_MSG_PAGE"));
                    fromActivity.finish();
                }
            }
        }).execute();
    }

    /**
     * 跳到指定活动介绍页面
     *
     * @param fromActivity
     * @param businessParam
     */
    public static void goToAppointRewardRegisterActivity(Activity fromActivity, String businessParam) {
        ((BaseCompatActivity) fromActivity).showDialogNotCancel();
        new GetBusinessResultsProtocol("CREDIT_CARD_REWARD", businessParam, new NetWorkCallbackListener<BusinessResultsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ((BaseCompatActivity) fromActivity).dismissDialog();
                showErrorMsgDialog(fromActivity, errorMsg);
            }

            @Override
            public void onSuccess(BusinessResultsEntity response) {
                ((BaseCompatActivity) fromActivity).dismissDialog();
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(CAMPAIGNID, response.getCreditCardRewardInfo().getCampaignId());
                maps.put(URL_REWARD, response.getCreditCardRewardInfo().getActivityUrl());
                maps.put(IMAGEURL, response.getCreditCardRewardInfo().getImagesUrl());
                maps.put(TERMINFOURL, response.getCreditCardRewardInfo().getTermsAndConditionsUrl());
                maps.put(BANKCARDTYPE, response.getCreditCardRewardInfo().getBankCardType());
                maps.put(REMARK, response.getCreditCardRewardInfo().getExplain());
                ActivitySkipUtil.startAnotherActivity(fromActivity, RewardRegisterWebviewActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }).execute();
    }


    /**
     * 密码弹出框
     */
    public static void inputPswPopWindow(Activity fromActivity, int type, boolean isSupportFinger) {

        VerifyPasswordCommonActivity.startActivityForResult(fromActivity, isSupportFinger, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {

                if (fromActivity == null) {
                    return;
                }

                if (type == TYPE_REMITTANCE_BIND) {
                    //绑卡
                    binderSmartAccount(fromActivity);
                } else {
                    //升级
                    updateAccount(fromActivity);
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (fromActivity == null) {
                    return;
                }
                if (type == TYPE_REMITTANCE_BIND) {
                    showErrorMsgDialog(fromActivity, errorMsg);
                }
            }

            @Override
            public void onVerifyCanceled() {
                if (type == TYPE_REMITTANCE_BIND) {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                }
            }
        });
    }


    /**
     * 账户升级
     */
    private static void updateAccount(Activity fromActivity) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
        ActivitySkipUtil.startAnotherActivity(fromActivity, UpgradeAccountActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    /**
     * 绑定智能账户
     * 跳转到RegisterSmartAccountDescActivity
     * 绑定SA账户流程
     */
    private static void binderSmartAccount(Activity fromActivity) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        RegisterSmartAccountDescActivity.startActivity(fromActivity, maps);
    }


    /**
     * 跳转至UPlan指定门店
     *
     * @param deepLinkUrl
     * @param fromActivity
     */
    public static void goToUPlanStore(String deepLinkUrl, Activity fromActivity) {
        HashMap<String, String> map = getUrlParams(deepLinkUrl);
        String merchantInfo = map.get("merchantInfo");
        String parameter = "{\"action\":\"MERCHANT\",\"merchantInfo\":\"" + merchantInfo + "\"}";


        ((BaseCompatActivity) fromActivity).showDialogNotCancel();
        new GetUplanurlProtocol(parameter, new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ((BaseCompatActivity) fromActivity).dismissDialog();
                showErrorMsgDialog(fromActivity, errorMsg);
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                ((BaseCompatActivity) fromActivity).dismissDialog();
                if (response != null) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
                    mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
                    mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
                    mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
                    mHashMaps.put(UplanActivity.IS_FROM_DEEP_LINK, true);
                    ActivitySkipUtil.startAnotherActivity(fromActivity, UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }


            }
        }).execute();
    }

}
