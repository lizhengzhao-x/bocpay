package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOCheckValidPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 17:31
 * @change
 * @chang time
 * @class 检查FIO是否为有效的FIO
 */
public interface FIOCheckValidPresenter<V extends IView> {
    void onFioCheckValid(String fioId);
}
