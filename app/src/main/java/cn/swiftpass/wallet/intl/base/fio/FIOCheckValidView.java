package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.FioCheckValidEntity;


public interface FIOCheckValidView<V extends IPresenter> {
    void onFioCheckValidSuccess(FioCheckValidEntity response);

    void onFioCheckValidFail(String errorCode, String errorMsg);
}
