package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.MyCreditCardRewardActionList;

/**
 * 获取信用卡奖赏活动-我的任务
 */
public class GetMyCreditCardRewardListProtocol extends BaseProtocol {


    private final String currentPage;
    private final String dateType;

    public GetMyCreditCardRewardListProtocol(String dateType, String currentPage, NetWorkCallbackListener<MyCreditCardRewardActionList> dataCallback) {
        this.mDataCallback = dataCallback;
        this.dateType = dateType;
        this.currentPage = currentPage;
        mUrl = "api/creditCardReward/getRewardTasks";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAGENUM, currentPage);
        mBodyParams.put(RequestParams.DATE_TYPE, dateType);

    }
}
