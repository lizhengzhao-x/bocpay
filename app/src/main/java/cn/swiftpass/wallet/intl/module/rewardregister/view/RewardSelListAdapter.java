package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardActivityListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

/**
 * 常用联系人adapter
 */
public class RewardSelListAdapter extends BaseRecyclerAdapter<RewardActivityListEntity.RewardActivity> {

    public RewardSelListAdapter(@Nullable List<RewardActivityListEntity.RewardActivity> data) {
        super(R.layout.item_reward_sel_activity, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, RewardActivityListEntity.RewardActivity contactEntity, int position) {
        ImageView img = holder.getView(R.id.id_reward_img);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) img.getLayoutParams();
        int width = AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 40);
        lp.width = width;
        lp.height = (int) (width / 2);
        img.setLayoutParams(lp);
        GlideApp.with(mContext).load(contactEntity.getImagesUrl()).transform(new RoundedCorners(AndroidUtils.dip2px(mContext, 4f))).placeholder(R.mipmap.img_cardpromotion_skeleton).into(img);
    }
}
