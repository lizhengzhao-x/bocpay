package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;


public class OrderDetailPopWindow extends BasePopWindow implements View.OnClickListener {
    private TextView mId_paid_money;
    private TextView mId_paidTo;
    private TextView mId_paidvia;
    private TextView mTv_order_ok;
    private TextView mId_pay_symbol;
    private ImageView mId_right_card, id_close_dialog;
    private String money;
    private String paidTO;
    private String trxNumber;

    private BankCardEntity mBankCardEntity;

    public void setDefaultCardId(BankCardEntity bankCardEntity) {
        if (null != bankCardEntity && !TextUtils.isEmpty(bankCardEntity.getPanShowNumber())) {
            mBankCardEntity = bankCardEntity;
            this.defaultCardId = bankCardEntity.getPanShowNumber();
            mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(mActivity, defaultCardId, mBankCardEntity.getCardType()));
        }
    }


    private String defaultCardId;
    private OnPopWindowOkClickListener mOnPopClickListener;

    public OrderDetailPopWindow(Activity mActivity, String money, String paidTO, String trxNumber, BankCardEntity bankCardEntity, OnPopWindowOkClickListener onPopWindowOkClickListener) {
        super(mActivity);
        mBankCardEntity = bankCardEntity;
        this.money = money;
        this.paidTO = paidTO;
        this.defaultCardId = bankCardEntity.getPanShowNumber();
        this.mOnPopClickListener = onPopWindowOkClickListener;
        this.trxNumber = trxNumber;
        initView();
    }


    @Override
    protected void initView() {
        setContentView(R.layout.pop_order_detail);
        this.setAnimationStyle(R.style.dialog_style);
        mId_paid_money = mContentView.findViewById(R.id.id_paid_money);
        mId_paidTo = mContentView.findViewById(R.id.id_paidTo);
        mId_paidvia = mContentView.findViewById(R.id.id_paidvia);
        mTv_order_ok = mContentView.findViewById(R.id.tv_order_ok);
        id_close_dialog = mContentView.findViewById(R.id.id_close_dialog);
        mId_right_card = mContentView.findViewById(R.id.id_right_card);
        mId_pay_symbol = mContentView.findViewById(R.id.id_pay_symbol);
        mTv_order_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onPopWindowOkClickListener(true);
            }
        });
        mId_right_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onSelCardClickListener();
            }
        });
        mId_paidvia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onSelCardClickListener();
            }
        });
        id_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onDisMissClickListener();
                dismiss();
            }
        });
        mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(mActivity, defaultCardId, mBankCardEntity.getCardType()));

        mId_paid_money.setText(BigDecimalFormatUtils.forMatWithDigs(money, 2));
        mId_paidTo.setText(paidTO);
        mId_pay_symbol.setText(AndroidUtils.getTrxCurrency(trxNumber));
        setDisMissView(mContentView);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        mOnPopClickListener.onDisMissClickListener();
    }


    /**
     * 切换卡列表需要更换
     *
     * @param defaultCardId
     */
    public void changeCardSelNumber(String defaultCardId) {
        mId_paidvia.setText(AndroidUtils.getSubMasCardNumberTitle(defaultCardId, mActivity));
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnPopWindowOkClickListener {
        void onPopWindowOkClickListener(boolean complete);

        void onSelCardClickListener();

        void onDisMissClickListener();
    }
}
