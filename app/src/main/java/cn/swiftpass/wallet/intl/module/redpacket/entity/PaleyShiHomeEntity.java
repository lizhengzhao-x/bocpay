package cn.swiftpass.wallet.intl.module.redpacket.entity;


import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by jamy on 2018/5/22.
 */

public class PaleyShiHomeEntity extends BaseEntity {

    private String redPacketBanner;  //派利是首页banner图
    private int receivedRedPacketNum;  //未拆红包数量
    private int hasMoreRecords;  //1 表示有更多交易记录，0 表示没有更多
    private int recordTotalNum;  //交易记录总数
    private List<RedPacketBeanEntity> sendRecords;  //首页派利是记录，最多5条
    private String srcRefNo;  //交易编号
    private Long sendTime;  //派利是时间戳
    private String sendTimeStr;  //派利是时间yyyy-MM-dd HH24:mi:ss
    private String sendAmt;  //派利是金额 1.00
    private String receiverName;  //收款人名称
    private String receiverAcNo;  //收款人账号 （电话、邮箱）
    private String currency;  //币种（默认HKD）
    private String blessingWords;  //祝福语
    private String transferType;  //交易类型 7:fps派利是  8:bocpay派利是

    private String redPacketIntroduce;//派利是活动说明文案
    private String redPacketIntroduceTitle;// 派利是活动说明文案 标题

    public String getRedPacketIntroduce() {
        return redPacketIntroduce;
    }

    public void setRedPacketIntroduce(String redPacketIntroduce) {
        this.redPacketIntroduce = redPacketIntroduce;
    }

    public String getRedPacketIntroduceTitle() {
        return redPacketIntroduceTitle;
    }

    public void setRedPacketIntroduceTitle(String redPacketIntroduceTitle) {
        this.redPacketIntroduceTitle = redPacketIntroduceTitle;
    }

    public String getRedPacketBanner() {
        return redPacketBanner;
    }

    public void setRedPacketBanner(String redPacketBanner) {
        this.redPacketBanner = redPacketBanner;
    }


    public int getReceivedRedPacketNum() {
        return receivedRedPacketNum;
    }

    public void setReceivedRedPacketNum(int receivedRedPacketNum) {
        this.receivedRedPacketNum = receivedRedPacketNum;
    }

    public int getHasMoreRecords() {
        return hasMoreRecords;
    }

    public void setHasMoreRecords(int hasMoreRecords) {
        this.hasMoreRecords = hasMoreRecords;
    }

    public int getRecordTotalNum() {
        return recordTotalNum;
    }

    public void setRecordTotalNum(int recordTotalNum) {
        this.recordTotalNum = recordTotalNum;
    }

    public List<RedPacketBeanEntity> getSendRecords() {
        return sendRecords;
    }

    public void setSendRecords(List<RedPacketBeanEntity> sendRecords) {
        this.sendRecords = sendRecords;
    }

    public String getSrcRefNo() {
        return srcRefNo;
    }

    public void setSrcRefNo(String srcRefNo) {
        this.srcRefNo = srcRefNo;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public String getSendTimeStr() {
        return sendTimeStr;
    }

    public void setSendTimeStr(String sendTimeStr) {
        this.sendTimeStr = sendTimeStr;
    }

    public String getSendAmt() {
        return sendAmt;
    }

    public void setSendAmt(String sendAmt) {
        this.sendAmt = sendAmt;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverAcNo() {
        return receiverAcNo;
    }

    public void setReceiverAcNo(String receiverAcNo) {
        this.receiverAcNo = receiverAcNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBlessingWords() {
        return blessingWords;
    }

    public void setBlessingWords(String blessingWords) {
        this.blessingWords = blessingWords;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }
}
