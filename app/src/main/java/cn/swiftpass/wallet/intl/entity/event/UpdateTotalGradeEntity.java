package cn.swiftpass.wallet.intl.entity.event;

public class UpdateTotalGradeEntity extends BaseEventEntity {

    public static final int UPDATE_TOTAL_GRADE = 1423;

    public UpdateTotalGradeEntity(int eventType, String message) {
        super(eventType, message);
    }
}
