package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;

public class GetMenuListProtocol extends BaseProtocol {

    @Override
    public boolean isNeedLogin() {
        return false;
    }

    public GetMenuListProtocol(NetWorkCallbackListener<MenuListEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/preLogin/queryLeftMenuInfoList";

    }
}
