package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class AuthTokenEntity extends BaseEntity {


    public String getUserToken() {
        return userToken;
    }

    private String userToken;

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    private String respCode;

}
