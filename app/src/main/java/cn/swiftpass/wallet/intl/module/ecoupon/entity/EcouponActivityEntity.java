package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class EcouponActivityEntity extends BaseEntity {
    /**
     * businessType : EVOUCHER
     * itemDesc : 电子券
     * itemName : eVoucher
     * itemPictureUrl : http://mycarson.wicp.vip/images/zh_CN/BOC_Pay_Banner.jpg?v=1
     * itemType : button
     * redirectUrl :
     */

    private String statementContent;
    private String statementUrl;
    private String buttonDesc;
    private String businessType;
    private String itemDesc;
    private String itemName;
    private String itemPictureUrl;
    private String itemType;
    private String redirectUrl;

    public String getStatementContent() {
        return statementContent;
    }

    public void setStatementContent(String statementContent) {
        this.statementContent = statementContent;
    }

    public String getStatementUrl() {
        return statementUrl;
    }

    public void setStatementUrl(String statementUrl) {
        this.statementUrl = statementUrl;
    }

    public String getButtonDesc() {
        return buttonDesc;
    }

    public void setButtonDesc(String buttonDesc) {
        this.buttonDesc = buttonDesc;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemPictureUrl() {
        return itemPictureUrl;
    }

    public void setItemPictureUrl(String itemPictureUrl) {
        this.itemPictureUrl = itemPictureUrl;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
