/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

/**
 * Class Name: MpQrUrlInfo<br/>
 *
 * Class Description:
 *
 *
 * @author 86755221
 * @version 1 Date: 2018-1-19
 * 银联FPS码
 *
 */

public class UpQrFpsEntity extends MpQrUrlInfoResult {
    //00 Globally Unique Identifier
    private String globalId;
    //01 Clearing Code
    private String clearCode;
    //02 FPS Identifier
    private String fpsId;
    //03 Mobile Phone
    private String phoneNo;
    //04 Email Address
    private String emailAddress;

    //05 Merchant time-out time
    private String timeout;

    public String getTimeout() {
        return timeout;
    }

    public void setTimeout(String timeout) {
        this.timeout = timeout;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getClearCode() {
        return clearCode;
    }

    public void setClearCode(String clearCode) {
        this.clearCode = clearCode;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getGlobalId() {
        return globalId;
    }

    public void setGlobalId(String globalId) {
        this.globalId = globalId;
    }

    public String getFpsId() {
        return fpsId;
    }

    public void setFpsId(String fpsId) {
        this.fpsId = fpsId;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    private String outTime;
}
