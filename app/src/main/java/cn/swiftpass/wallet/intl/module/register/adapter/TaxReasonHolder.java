package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class TaxReasonHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_tax_reason)
    TextView tvTaxReason;
    @BindView(R.id.iv_select_tax_reason)
    ImageView ivSelectTax;

    private final Context context;

    public TaxReasonHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }
}
