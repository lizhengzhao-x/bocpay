/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import java.io.Serializable;


/**
 * Class Name: GetQrCodeResult<br/>
 *
 * Class Description:
 *
 *
 * @author 86755221
 * @version 1   Date: 2018-1-19
 *
 */
public class GetQrCodeResult implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 3998935958647362909L;
    private String emvCpQrc;
    private String barCodeCpQrc;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    private String txnId;

//	eMVQR
//			String
//	C
//			二维码信息
//	barcode
//			String
//	C
//			条形码信息
//	txnId
//			String
//	C
//			交易单号


    /**
     *
     */
    public GetQrCodeResult() {
        super();
    }

    /**
     * @param emvCpQrc
     * @param barCodeCpQrc
     */
    public GetQrCodeResult(String emvCpQrc, String barCodeCpQrc) {
        super();
        this.emvCpQrc = emvCpQrc;
        this.barCodeCpQrc = barCodeCpQrc;
    }

    /**
     * @return the emvCpQrc
     */
    public String getEmvCpQrc() {
        return this.emvCpQrc;
    }

    /**
     * @param emvCpQrc
     *            the emvCpQrc to set
     */
    public void setEmvCpQrc(String emvCpQrc) {
        this.emvCpQrc = emvCpQrc;
    }

    /**
     * @return the barCodeCpQrc
     */
    public String getBarCodeCpQrc() {
        return this.barCodeCpQrc;
    }

    /**
     * @param barCodeCpQrc
     *            the barCodeCpQrc to set
     */
    public void setBarCodeCpQrc(String barCodeCpQrc) {
        this.barCodeCpQrc = barCodeCpQrc;
    }

}
