package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import com.google.gson.annotations.SerializedName;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.PayeeEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:31
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderRecordEntity extends PayeeEntity {


    @SerializedName("txTime")
    public String date;

    @SerializedName("amount")
    public String amount;
    @SerializedName("cur")
    public String currency;

    public TransferCrossBorderAccountListEntity getAccountObj() {
        return accountObj;
    }

    private TransferCrossBorderAccountListEntity accountObj;

    public UsedObjBean getForUsedObj() {
        return forUsedObj;
    }

    public void setForUsedObj(UsedObjBean forUsedObj) {
        this.forUsedObj = forUsedObj;
    }

    private UsedObjBean forUsedObj;


    public TransferCrossBorderRecordEntity() {
        super();
    }

    public TransferCrossBorderRecordEntity(String date, String amount) {
        this.date = date;
        this.amount = amount;
    }

    public TransferCrossBorderRecordEntity(String date, String amount, String currency) {
        this.date = date;
        this.amount = amount;
        this.currency = currency;
    }

    public TransferCrossBorderRecordEntity(String payeeName, String payeeHideName, String payeeCardId, String payeeBank, String date, String amount, String currency) {
        super(payeeName, payeeHideName, payeeCardId, payeeBank);
        this.date = date;
        this.amount = amount;
        this.currency = currency;
    }


    public static class UsedObjBean extends BaseEntity {


        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        private String code;

    }
}
