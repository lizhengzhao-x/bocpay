package cn.swiftpass.wallet.intl.module.setting.contract;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.base.fio.FIOCheckPresenter;
import cn.swiftpass.wallet.intl.base.fio.FIOCheckValidPresenter;
import cn.swiftpass.wallet.intl.base.fio.FIOCheckValidView;
import cn.swiftpass.wallet.intl.base.fio.FIOCheckView;
import cn.swiftpass.wallet.intl.base.fio.FIOResultPresenter;
import cn.swiftpass.wallet.intl.base.fio.FIOResultView;


/**
 * @name cn.swiftpass.bocbill.model.login.contract
 * @class name：LoginCheckContract
 * @class describe
 * @anthor zhangfan
 * @time 2019/6/27 21:59
 * @change
 * @chang time
 * @class describe
 */
public class TouchIDContract {

    public interface View extends FIOResultView<Presenter>, FIOCheckView<Presenter>, FIOCheckValidView<Presenter>, IView {

        void verifyPwdFailed(String errorCode, String errorMsg);

        void verifyPwdSuccess(BaseEntity response);
    }


    public interface Presenter extends FIOResultPresenter<View>, FIOCheckPresenter<View>, FIOCheckValidPresenter<View>, IPresenter<TouchIDContract.View> {
        void onFioDelete(String fioId);

        void verifyPwd(String action, String payPwd);

    }
}
