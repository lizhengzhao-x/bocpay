package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardEntity;

public class CreditCardRewardBaseAdapter extends RecyclerView.Adapter {

    private static final int TYPE_MORE_HOLDER = 102;
    private static final int TYPE_NORMAL_HOLDER = 101;
    private final Context context;
    private final OnRewadDetailListener listener;
    List<CreditCardRewardEntity> data;


    public CreditCardRewardBaseAdapter(Context context, OnRewadDetailListener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_MORE_HOLDER) {
            View inflate = LayoutInflater.from(context).inflate(R.layout.item_base_card_reward_more, parent, false);
            return new CreditCardRewardBaseMoreHolder(inflate);
        } else {
            View inflate = LayoutInflater.from(context).inflate(R.layout.item_credit_card_reward, parent, false);
            return new CreditCardRewardHolder(inflate);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CreditCardRewardHolder) {
            CreditCardRewardHolder cardRewardHolder = (CreditCardRewardHolder) holder;
            cardRewardHolder.bindData(context, data.get(position), listener);
        }
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() + 1 : 0;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == getItemCount() - 1) {
            return TYPE_MORE_HOLDER;
        } else {
            return TYPE_NORMAL_HOLDER;
        }

    }

    public void addData(List<CreditCardRewardEntity> rows) {
        if (data == null) {
            data = new ArrayList<>();
        }
        data.addAll(rows);

    }

    public void setData(List<CreditCardRewardEntity> rows) {
        data = rows;
    }
}
