package cn.swiftpass.wallet.intl.module.scan.majorscan.view;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.app.ActivityCompat;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.util.Collection;
import java.util.EnumSet;
import java.util.Map;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.QrcodeScanListener;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.widget.ViewfinderView;
import cn.swiftpass.boc.commonui.base.zxing.BeepManager;
import cn.swiftpass.boc.commonui.base.zxing.camera.CameraManager;
import cn.swiftpass.boc.commonui.base.zxing.decoding.CaptureActivityHandler;
import cn.swiftpass.boc.commonui.base.zxing.decoding.DecodeFormatManager;
import cn.swiftpass.boc.commonui.base.zxing.decoding.InactivityTimer;
import cn.swiftpass.httpcore.manager.PermissionAskManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

/**
 * 扫码的逻辑独立存在
 */
public class ScanFragment extends BaseFragment implements SurfaceHolder.Callback {

    private static final String TAG = "ScanFragment";

    @BindView(R.id.preview_view)
    SurfaceView surfaceView;
    @BindView(R.id.viewfinder_view)
    ViewfinderView viewfinderView;


    /**
     * 判断srufaceview是否已经创建
     */
    private boolean mSurfaceViewCreated;


    /**
     * 是否第一次选择拒绝且不再询问权限
     */
    public boolean firstRejectCamera = true;

    /**
     * 弹出相机权限选择框
     */
    private CustomDialog mPermissionDialog;

    private CameraManager cameraManager;

    /**
     * zxing 二维码解析类型
     */
    private Collection<BarcodeFormat> mDecodeFormats;

    private Handler mScanHandler;

    private CaptureActivityHandler handler;

    private QrcodeScanListener qrcodeScanListener;

    /**
     * 解析二维码成功 控制震动
     */
    private BeepManager beepManager;

    private View rootView;

    private InactivityTimer inactivityTimer;

    private Map<DecodeHintType, ?> decodeHints;


    /**
     * 扫码报错 点击recently回来不能预览
     */
    protected boolean hasDialogShow;

    /**
     * MBK 传递过来的qrcode信息
     */
    protected String qrCodeInfo;

    @Override
    public void initTitle() {

    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_major_scan;
    }

    @Override
    protected void initView(View v) {
        mScanHandler = new Handler();
        if (!isGranted_(WalletConstants.PERMISSIONS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS[0])) {
                //拒绝并勾选不再询问
                firstRejectCamera = false;
            } else {
                firstRejectCamera = true;
            }
        }
        Window window = getActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        rootView = v;
        inactivityTimer = new InactivityTimer(getActivity());
        beepManager = new BeepManager(getActivity());


//        surfaceView.getViewTreeObserver().addOnPreDrawListener(
//                new ViewTreeObserver.OnPreDrawListener() {
//
//                    @Override
//                    public boolean onPreDraw() {
//                        surfaceView.getViewTreeObserver().removeOnPreDrawListener(this);
//                        scaleSurfaceView(surfaceView.getWidth(), surfaceView.getHeight());
//                        return true;
//                    }
//                });


    }


    public SurfaceView getSurfaceView() {
        return surfaceView;
    }


    @Override
    public void onResume() {
        super.onResume();
        LogUtils.i(TAG, "onResume---> ");
        if (mNeedRequestPermissions && AndroidUtils.needRequestApplyPermissien()) {
            checkPermissions();
            mNeedRequestPermissions = false;
        }
        if (cameraManager == null) {
            cameraManager = new CameraManager(getContext());
            mDecodeFormats = EnumSet.noneOf(BarcodeFormat.class);
            mDecodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
            viewfinderView.setCameraManager(cameraManager);
        }
        if (viewfinderView != null) {
            viewfinderView.setOnFlashLightStateChangeListener(new ViewfinderView.onFlashLightStateChangeListener() {
                @Override
                public void openFlashLight(boolean open) {
                    turnOnFlashLight(open);
                    viewfinderView.setStopDraw(false);
                    viewfinderView.reOnDraw();
                }
            });
        }
    }

    private void checkPermissions() {
        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS[0])) {
                //拒绝并勾选不再询问
                PermissionAskManager.getInstance().setmCameraPermissionAskClicked(true);
            } else {
                PermissionAskManager.getInstance().setmCameraPermissionAskClicked(false);
            }
        }
        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                showFlashLight();
            }

            @Override
            public void rejectPermission() {
                //需求是只可能弹一次弹框 要不是系统层面 要不是自定义层面
                hideFlashLight();
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS[0])) {
                    if (PermissionAskManager.getInstance().ismCameraPermissionAskClicked()) {
                        showLackOfPermissionDialog();
                    } else {
                        PermissionAskManager.getInstance().setmCameraPermissionAskClicked(false);
                    }
                }
            }
        }, WalletConstants.PERMISSIONS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        LogUtils.i(TAG, "surfaceCreated---> " + mSurfaceViewCreated + surfaceView.getWidth() + " height:" + surfaceView.getHeight());

        if (!mSurfaceViewCreated) {
            mSurfaceViewCreated = true;
            initCamera(surfaceHolder, false);
        }
    }

    protected void showFlashLight() {

    }

    protected void hideFlashLight() {

    }


    /**
     * 打开关闭闪光灯
     *
     * @param open
     */
    protected void turnOnFlashLight(boolean open) {
        if (cameraManager != null) {
            if (open) {
                //需要相机权限是否获取，然后可否打开
                if (!isGranted_(WalletConstants.PERMISSIONS[0])) {
                    return;
                }
            }
            viewfinderView.setOpenFlashLight(open);
            cameraManager.setTorch(open);
        }
    }

    private void initScanListener() {
        qrcodeScanListener = new QrcodeScanListener() {
            @Override
            public void scanResult(boolean isOk, String result) {

            }

            @Override
            public void submitData(String result, boolean status) {

            }

            @Override
            public void drawViewfinder() {
                viewfinderView.drawViewfinder();
            }

            @Override
            public void handleDecode(Result obj, Bitmap barcode) {
                if (getActivity() != null) {
                    LogUtils.i(TAG, "message;" + obj.getText());
                    beepManager.playBeepSoundAndVibrate();
                    parseCodeStr(obj.getText());
                }
            }

            @Override
            public void setResult(int resultOk, Intent obj) {

            }

            @Override
            public ViewfinderView getViewfinderView() {
                return viewfinderView;
            }

            @Override
            public Handler getHandler() {
                return handler;
            }

            @Override
            public CameraManager getCameraManager() {
                return cameraManager;
            }

            @Override
            public Context getContext() {
                return getContext();
            }
        };
    }

    protected void parseCodeStr(String content) {

    }


    /**
     * 打开摄像头
     *
     * @param surfaceHolder
     */
    private void initCamera(SurfaceHolder surfaceHolder, boolean isSpecialPage) {
        try {
            LogUtils.i(TAG, "open camera:" + isVisible());
            if (cameraManager == null) return;
            cameraManager.openDriver(surfaceHolder);
            if (handler == null) {
                initScanListener();
                handler = new CaptureActivityHandler(qrcodeScanListener, mDecodeFormats, null, decodeHints, mScanHandler, cameraManager, !hasDialogShow && !isSpecialPage);
            } else {
                if (!hasDialogShow && isSpecialPage && TextUtils.isEmpty(qrCodeInfo)) {
                    //重新自动聚焦 华为 三星某些手机适配
                    restartPreviewAfterDelay(500);
                }
            }
        } catch (Exception ioe) {
            Log.w(TAG, ioe);
        }
    }


    /**
     * 重新开始扫描解析
     *
     * @param delayMS
     */
    public void restartPreviewAfterDelay(boolean isSpecialEvent, long delayMS) {
        resumeCamera(isSpecialEvent);
        viewfinderView.setStopDraw(false);
        viewfinderView.reOnDraw();
        handler.restartPreview();
    }

    /**
     * 重新开始扫描解析
     *
     * @param delayMS
     */
    public void restartPreviewAfterDelay(long delayMS) {
        if (!TextUtils.isEmpty(qrCodeInfo)) {
            qrCodeInfo = "";
        }
        if (handler != null) {
            viewfinderView.setStopDraw(false);
            viewfinderView.reOnDraw();
            handler.restartPreview();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        LogUtils.i(TAG, "surfaceChanged---> ");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mSurfaceViewCreated = false;

    }

    @Override
    public void onPause() {
        super.onPause();
        LogUtils.i(TAG, "onPause--->");
        releaseCamera();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (inactivityTimer != null) {
            inactivityTimer.shutdown();
        }
        releaseCamera();
    }

    /**
     * camera 释放
     */
    protected void resumeCamera(boolean isSpecialPage) {
        if (!isGranted_(WalletConstants.PERMISSIONS[0])) {
            //权限未获取 相机不用初始化
            return;
        }
        LogUtils.i(TAG, "isVisible--->" + isVisible() + " hasSurface:" + mSurfaceViewCreated);
        surfaceView = rootView.findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        surfaceHolder.addCallback(this);
        initCamera(surfaceHolder, isSpecialPage);
        if (hasDialogShow) {
            viewfinderView.setStopDraw(true);
        } else {
            viewfinderView.setStopDraw(false);
        }
        viewfinderView.reOnDraw();
        beepManager.updatePrefs();
        inactivityTimer.onResume();
        mDecodeFormats = null;
    }


    public void releaseCamera() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        if (mSurfaceViewCreated && surfaceView != null) {
            surfaceView = rootView.findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
            mSurfaceViewCreated = false;
            LogUtils.i(TAG, "releaseCamera--->mSurfaceViewCreated" + mSurfaceViewCreated);
        }
        viewfinderView.setOpenFlashLight(false);
        viewfinderView.setStopDraw(true);
        if (cameraManager != null) {
            cameraManager.stopPreview();
            cameraManager.closeDriver();
            cameraManager = null;
        }

    }


    /**
     * 停止预览并扫描 收银台/密码等弹出层出现 背景扫描应该停止
     */
    public void stopPreview() {
        if (handler != null) {
            handler.stopPreviewAndDecode();
            viewfinderView.setStopDraw(true);
        }
    }

    private void showLackOfPermissionDialog() {
        if (mPermissionDialog != null && mPermissionDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.string_camera_use));
        builder.setMessage(getString(R.string.string_turn_on_camera_pay));
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AndroidUtils.startAppSetting(mActivity);
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = getActivity();
        if (mActivity != null && !mActivity.isFinishing()) {
            mPermissionDialog = builder.create();
            mPermissionDialog.setCancelable(false);
            mPermissionDialog.show();
        }
    }

}
