package cn.swiftpass.wallet.intl.base.password;

import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * 验证支付密码
 * Created by ZhangXinchao on 2019/7/16.
 */
public interface VerifyPwdPresenter<V extends IView> {

    void verifyPwd(String pwd, String action);
}
