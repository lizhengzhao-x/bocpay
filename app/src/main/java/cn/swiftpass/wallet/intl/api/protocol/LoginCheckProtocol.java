package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class LoginCheckProtocol extends BaseProtocol {
    public static final String TAG = LoginCheckProtocol.class.getSimpleName();
    /**
     * 用户id
     */
    String mMobile;
    /**
     * 取值：L：登录
     */
    String mAction;

    public LoginCheckProtocol(String mobile, String action, NetWorkCallbackListener dataCallback) {
        this.mMobile = mobile;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/login/check";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
    }
}
