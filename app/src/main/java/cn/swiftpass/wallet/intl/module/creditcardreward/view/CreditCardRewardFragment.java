package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.CreditCardRewardAdapter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;

public class CreditCardRewardFragment extends BaseFragment {

    /**
     * 信用卡奖赏记录点击push通知切换至“我的任务”
     * MSG_ID_CREDIT_CARD_REWARD
     * creditCardRewardHandler
     */
    public static final int MSG_ID_CREDIT_CARD_REWARD = 9945;
    public Handler creditCardRewardHandler = new Handler() {
        @SuppressLint("HandlerLeak")
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == MSG_ID_CREDIT_CARD_REWARD) {
                vpCreditCardRewardFrg.setCurrentItem(0);
            }
        }
    };

    @BindView(R.id.tab_credit_card_reward_frg)
    TabLayout tabCreditCardRewardFrg;
    @BindView(R.id.vp_credit_card_reward_frg)
    ViewPager2 vpCreditCardRewardFrg;


    @Override
    protected void initView(View v) {

    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(getString(R.string.CRQ2107_1_1));
            vpCreditCardRewardFrg.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
            tabCreditCardRewardFrg.setSelectedTabIndicatorColor(mActivity.getColor(R.color.color_FFBF2F4F));

            List<Fragment> fragments = new ArrayList<>();
            fragments.add(new MyCreditCardRewardFragment());
            fragments.add(new CreditCardRewardHistoryListFragment());

            CreditCardRewardAdapter adapter = new CreditCardRewardAdapter(getChildFragmentManager(), this.getLifecycle(), fragments);
            vpCreditCardRewardFrg.setAdapter(adapter);
            vpCreditCardRewardFrg.setCurrentItem(0);

            vpCreditCardRewardFrg.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
                @Override
                public void onPageSelected(int position) {
                    super.onPageSelected(position);
                    String actionId = "";
                    if (position == 0) {
                        actionId = ActionIdConstant.BUT_CREDIT_REWARD_TASK;
                    } else {
                        actionId = ActionIdConstant.BUT_CREDIT_REWARD_RECORD;
                    }
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(actionId, startTime, null, PagerConstant.ADDRESS_PAGE_FRONT, System.currentTimeMillis() - startTime);
                    sendAnalysisEvent(analysisButtonEntity);
                }
            });

            new TabLayoutMediator(tabCreditCardRewardFrg, vpCreditCardRewardFrg, true, new TabLayoutMediator.TabConfigurationStrategy() {
                @Override
                public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                    if (position == 0) {
                        tab.setText(getString(R.string.CRQ2107_1_2));
                    } else {
                        tab.setText(getString(R.string.CRQ2107_1_3));
                    }
                }
            }).attach();

        }


    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_credit_card_reward;
    }


}