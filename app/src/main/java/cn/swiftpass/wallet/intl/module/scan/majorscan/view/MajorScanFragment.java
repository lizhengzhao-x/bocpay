package cn.swiftpass.wallet.intl.module.scan.majorscan.view;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.zxing.ZxingConst;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.dialog.OrderDetailGradeDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentListDialogFragment;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.QrcodeTypeEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.scan.majorscan.InputMoneyActivity;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.MajorScanContract;
import cn.swiftpass.wallet.intl.module.scan.majorscan.presenter.MajorScanPresenter;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferInputMoneyActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyWithMajorScanCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;


public class MajorScanFragment extends ScanFragment implements MajorScanContract.View {

    private static final String TAG = "MajorScanFragment";
    public static final String UPI_QRCODE_PREFIX = "https://qr.95516.com";
    public static final String ACTION = "ACTION";
    /**
     * 是否处于支付模式/校验密码跳转模式
     */
    private boolean isPayMode;
    /**
     * 扫描到的二维码对象
     */
    private MpQrUrlInfoResult mpQrUrlInfoResult;
    private String queryCardId, qrcodeStr;
    /**
     * 当前二维码类型 由后台返回
     */
    private String currentQRcType;
    private String mCurrentQrcode;
    private boolean isResumeFromFioActivity = false;
    private static final int REQUEST_PICK_FORM_ALBUM = 0x16;
    /**
     * 订单详情 收银台
     */
    private OrderDetailGradeDialogFragment orderDetailDialogFragment;
    /**
     * 支付里欸包
     */
    private PaymentListDialogFragment paymentListDialogFragment;
    /**
     * 标识当前订单是否使用积分
     */
    private boolean currencyUsePoint;
    /**
     * 支付bean
     */

    private Handler mQueryHandler;
    private ActionTrxGpInfoEntity actionTrxGpInfoEntity;


    private MajorScanPresenter mMainPresenter;

    /**
     * 默认的cardId 当前支付的cardId
     */
    private String originalCardId;

    /**
     * 特殊场景控制onResume的时候不能重新扫描
     */
    private boolean isSpecialPageEvent = false;

    private LinearLayout buttomView;


    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarRightViewToText(R.string.album);
            mActivity.setToolBarTitle(getString(R.string.M10_1_8));
            mActivity.getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    pickFromAlbum();
                }
            });
        }
    }

    @Override
    protected void showFlashLight() {
        super.showFlashLight();
        ZxingConst.isWeakLight = true;
        if (viewfinderView != null) {
            viewfinderView.reOnDraw();
        }
    }

    @Override
    protected void hideFlashLight() {
        super.hideFlashLight();
        ZxingConst.isWeakLight = false;
        viewfinderView.setShowFlashLight(false);
        viewfinderView.setStopDraw(false);
        viewfinderView.reOnDraw();
        if (viewfinderView != null) {
            viewfinderView.reOnDraw();
        }
    }

    @Override
    protected MajorScanContract.Presenter loadPresenter() {
        return new MajorScanPresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_major_scan;
    }


    @Override
    protected void initView(View v) {
        if (mPresenter instanceof MajorScanContract.Presenter) {
            mMainPresenter = (MajorScanPresenter) mPresenter;
        }
        super.initView(v);
        buttomView = v.findViewById(R.id.id_buttom_hint_view);
        mPresenter = new MajorScanPresenter();
        mQueryHandler = new Handler();
        int topHeight = viewfinderView.getScanViewButtomPos();
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) buttomView.getLayoutParams();
        lp.topMargin = topHeight + AndroidUtils.dip2px(getContext(), 10);
        buttomView.setLayoutParams(lp);
    }


    @Override
    public void onResume() {
        super.onResume();
        if (isResumeFromFioActivity) return;
        //如果当前onResume调用的时候 验证密码弹框没有消失 这个时候不能进行扫码逻辑
        if ((orderDetailDialogFragment != null && orderDetailDialogFragment.isVisible()) || (MyActivityManager.getInstance().getCurrentActivity() instanceof VerifyPasswordCommonActivity)) {
            mQueryHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isSpecialPageEvent = true;
                    stopPreview();
                }
            }, 200);
        } else {
            resumeCamera(isSpecialPageEvent);
            isSpecialPageEvent = false;
        }
        if (!TextUtils.isEmpty(qrCodeInfo)) {
            parserLocalQrcodeSuccess(qrCodeInfo);
        }
    }

    /**
     * 二维码解析
     *
     * @param content
     */
    @Override
    protected void parseCodeStr(final String content) {
        //停止预览
        stopPreview();
        mCurrentQrcode = content;
        mMainPresenter.scanQrCodeType(content);
    }

    /**
     * url model
     * 银联商户二维码
     *
     * @param content
     */
    private boolean dealWithUrlModeCode(final String content) {
        if (TextUtils.isEmpty(content) || !content.toLowerCase().startsWith(UPI_QRCODE_PREFIX)) {
            return false;
        }
        mMainPresenter.getUpMerchantURLQRInfo(content);

        return true;
    }


    /**
     * emv码的解析处理
     * 区分银联商户二
     *
     * @param content
     */
    private boolean dealWithEmvCode(String content) {
        mMainPresenter.dealWithEmvCode(content);
        return true;
    }

    public void setQrCodeInfo(String qrCodeInfo) {
        this.qrCodeInfo = qrCodeInfo;
    }

    /**
     * 吊起收银台 后期要全部换成这种方式
     */
    private void showCheckStand(final ActionTrxGpInfoEntity actionTrxGpInfoEntity) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        originalCardId = actionTrxGpInfoEntity.getCardId();
        mpQrUrlInfoResult.setCardId(actionTrxGpInfoEntity.getCardId());
        if (orderDetailDialogFragment == null) {
            orderDetailDialogFragment = new OrderDetailGradeDialogFragment();
            OrderDetailGradeDialogFragment.OnDialogClickListener onDialogClickListener = new OrderDetailGradeDialogFragment.OnDialogClickListener() {
                @Override
                public void onOkListener(boolean isPoint) {
                    if (!ButtonUtils.isFastDoubleClick()) {
                        if (orderDetailDialogFragment.isFpsPaymentType()) {
                            //走fps转账流程
                            mMainPresenter.parseTransferCode(mpQrUrlInfoResult.getQrcodeStr(), true);
                        } else {
                            currencyUsePoint = isPoint;
                            isPayMode = true;
                            inoutPswPopWindow();
                        }
                    }
                }

                @Override
                public void onSelCardListener() {
                    mMainPresenter.getPaymentTypeList(mpQrUrlInfoResult.getQrcodeStr(), actionTrxGpInfoEntity.isUrlCode());
                }

                @Override
                public void onDisMissListener() {
                    orderDetailDialogFragment = null;
                    restartPreviewAfterDelay(isSpecialPageEvent, 500);
                }
            };
            orderDetailDialogFragment.initParams(actionTrxGpInfoEntity, onDialogClickListener);
            orderDetailDialogFragment.show(getChildFragmentManager(), "OrderDetailGradeDialogFragment");
            //停止扫描
            stopPreview();
        } else {
            orderDetailDialogFragment.updateParams(actionTrxGpInfoEntity);
        }
    }

    /**
     * 支付列表
     *
     * @param bankCardEntities
     * @param isUrlMode
     */
    private void showPaymentListDialog(final ArrayList<BankCardEntity> bankCardEntities, final boolean isUrlMode) {
        paymentListDialogFragment = new PaymentListDialogFragment();
        PaymentListDialogFragment.OnPaymentListClickListener onPaymentListClickListener = new PaymentListDialogFragment.OnPaymentListClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onMoreBtnClickListener() {
            }

            @Override
            public void onCardSelListener(int positon) {
                //切换卡号   //卡列表选择
                mpQrUrlInfoResult.setCardNumber(bankCardEntities.get(positon).getPanShowNumber());
                mpQrUrlInfoResult.setCardId(bankCardEntities.get(positon).getCardId());
                if (!isUrlMode) {
                    //不是url mode 才需要 重新拉取积分
                    mMainPresenter.getActionTrxGpInfo(mpQrUrlInfoResult);
                } else {
                    orderDetailDialogFragment.setDefaultCardId(bankCardEntities.get(positon));
                }
            }
        };
        paymentListDialogFragment.initParams(onPaymentListClickListener, bankCardEntities, orderDetailDialogFragment.getCurrentSelCardId(), orderDetailDialogFragment.isFpsPaymentType());
        paymentListDialogFragment.show(getChildFragmentManager(), "paymentListDialogFragment");
    }

    /**
     * 吊起订单详情界面
     *
     * @param result
     */
    public void initOrderPopWindow(MpQrUrlInfoResult result) {
        this.mpQrUrlInfoResult = result;
        String moneyStr = result.getTranAmt();
        if (moneyStr.startsWith(".")) {
            showErrorMsgDialog(getActivity(), getString(R.string.error_msg_input_payment_amount));
            return;
        }
        double moneyValue = 0;
        try {
            moneyValue = Double.parseDouble(moneyStr);
        } catch (NumberFormatException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        if (moneyValue <= 0) {
            showErrorMsgDialog(getActivity(), getString(R.string.error_msg_input_payment_amount));
            return;
        }
        isPayMode = true;
        mMainPresenter.getActionTrxGpInfo(mpQrUrlInfoResult);
    }


    /**
     * 严密逻辑
     */
    public void inoutPswPopWindow() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (isPayMode) {
                    submitOrderInfoRequest();
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        }, new OnPwdVerifyWithMajorScanCallBack() {
            @Override
            public void onActivityResume() {
                if (mQueryHandler == null) return;
                mQueryHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        stopPreview();
                    }
                }, 200);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (orderDetailDialogFragment != null) {
            orderDetailDialogFragment.dismissAllowingStateLoss();
        }
        if (paymentListDialogFragment != null) {
            paymentListDialogFragment.dismissAllowingStateLoss();
        }
        EventBus.getDefault().unregister(this);
        mQueryHandler.removeCallbacksAndMessages(null);
        if (orderDetailDialogFragment != null) {
            orderDetailDialogFragment = null;
        }
    }

    /**
     * 提交订单信息
     */
    private void submitOrderInfoRequest() {
        MainScanOrderRequestEntity orderRequest = new MainScanOrderRequestEntity();
        orderRequest.setTrxAmt(actionTrxGpInfoEntity.getTranAmt());
        orderRequest.setQrcType(currentQRcType);
        orderRequest.setTrxFeeAmt("0");
        orderRequest.setMpQrCode(mpQrUrlInfoResult.getQrcodeStr());
        orderRequest.setCardId(mpQrUrlInfoResult.getCardId());
        orderRequest.setMerchantName(mpQrUrlInfoResult.getName());
        orderRequest.setTxnCurr(actionTrxGpInfoEntity.getTranCur());
        orderRequest.setTxnId(actionTrxGpInfoEntity.getTranID());
        if (!actionTrxGpInfoEntity.isUrlCode() && currencyUsePoint && actionTrxGpInfoEntity.getRedeemFlag().equals("1")) {
            orderRequest.setRedeemGpAmt(actionTrxGpInfoEntity.getRedeemGpTnxAmt());
            orderRequest.setMerchantName(actionTrxGpInfoEntity.getName());
            orderRequest.setGpRequired(actionTrxGpInfoEntity.getGpRequired());
            orderRequest.setRedeemFlag(actionTrxGpInfoEntity.getRedeemFlag());
            orderRequest.setGpCode(actionTrxGpInfoEntity.getGpCode());
            orderRequest.setYearHolding(actionTrxGpInfoEntity.getYearHolding());
            orderRequest.setPayAmt(actionTrxGpInfoEntity.getPayAmt());
            orderRequest.setRedeemGpCount(actionTrxGpInfoEntity.getRedeemGpCount());
            orderRequest.setAvaGpCount(actionTrxGpInfoEntity.getAvaGpCount());
            ApiProtocolImplManager.getInstance().actionMpqrPaymentWithPoint(getActivity(), orderRequest, new NetWorkCallbackListener<ContentEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    showErrorMsgDialog(getActivity(), errorMsg);
                }

                @Override
                public void onSuccess(ContentEntity response) {
                    String tansId = response.getResult_msg();
                    queryOrderStatus(tansId);
                }
            });
        } else {
            mMainPresenter.getMainScanOrder(orderRequest);
        }

    }


    /**
     * 轮询订单状态
     *
     * @param tansId
     */
    private void queryOrderStatus(String tansId) {
        queryCardId = mpQrUrlInfoResult.getCardId();
        qrcodeStr = mpQrUrlInfoResult.getQrcodeStr();
        showDialogNotCancel();
        queryOrderStatus(tansId, queryCardId, qrcodeStr);
    }


    /**
     * 扫描订单结果
     *
     * @param tansId
     * @param cardId
     * @param qrcodeStr
     */

    private void queryOrderStatus(final String tansId, final String cardId, String qrcodeStr) {
        mMainPresenter.getPayMentResult(cardId, qrcodeStr, tansId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void parserUrlCodeError(String errorCode, String errorMsg) {

        if (!TextUtils.isEmpty(errorCode)) {
            if (!TextUtils.isEmpty(errorMsg)) {
                hasDialogShow = true;
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        hasDialogShow = false;
                        restartPreviewAfterDelay(500);
                    }
                });
            }
        }
    }

    /**
     * URL二维码解析成功
     *
     * @param mpQrUrlInfoResultResponse
     */
    @Override
    public void parserUrlCodeSuccess(MpQrUrlInfoResult mpQrUrlInfoResultResponse) {
        //ur码 不需要调用积分抵扣的相关接口
        mpQrUrlInfoResult = mpQrUrlInfoResultResponse;
        if (!TextUtils.isEmpty(mpQrUrlInfoResultResponse.getTranAmt())) {
            showCheckStand(AndroidUtils.convertActionTrxGpInfoEntity(mpQrUrlInfoResultResponse));
            return;
        }
        turnOnFlashLight(false);
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.URLQRINFO_ENTITY, mpQrUrlInfoResultResponse);
        mHashMaps.put(Constants.SCAN_CARD_TYPE, currentQRcType);
        ActivitySkipUtil.startAnotherActivity(getActivity(), InputMoneyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.NONE);
        qrCodeInfo = "";
    }

    @Override
    public void parseTransferCodeError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            hasDialogShow = true;
            stopPreview();
            AndroidUtils.showBindSmartAccount(mActivity, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            verifyPwdToUpdateAccount();
                            qrCodeInfo = "";
                        }
                    }, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            hasDialogShow = false;
                            restartPreviewAfterDelay(500);
                        }
                    }
            );
        } else {
            hasDialogShow = true;
            showErrorMsgDialog(mActivity, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    hasDialogShow = false;
                    restartPreviewAfterDelay(500);
                }
            });
        }
    }


    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(mActivity, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                hasDialogShow = false;
                restartPreviewAfterDelay(500);
            }
        });
    }

    /**
     * 转账二维码解析成功
     *
     * @param responseQrcode
     */
    @Override
    public void parseTransferCodeSuccess(ParserQrcodeEntity responseQrcode) {
        if (getActivity() != null) {
            turnOnFlashLight(false);
            if (responseQrcode != null && !TextUtils.isEmpty(responseQrcode.getTransferAmount()) && Double.valueOf(responseQrcode.getTransferAmount()) > 10000000) {
                hasDialogShow = true;
                showErrorMsgDialog(mActivity, getString(R.string.MACC1_1_10), new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        hasDialogShow = false;
                        restartPreviewAfterDelay(500);
                    }
                });
                return;
            }
            ParserQrcodeEntity parserQrcodeEntity = responseQrcode;
            Intent intent = new Intent();
            intent.putExtra(Constants.TYPE, Constants.SCAN_TO_TRANSFER);
            intent.putExtra(TransferConst.TRANS_FPS_DATA, parserQrcodeEntity);
            intent.setClass(getActivity(), TransferInputMoneyActivity.class);
            startActivity(intent);
        }
    }

    /**
     * EMV二维码解析成功
     *
     * @param mpQrUrlInfoResult
     */
    @Override
    public void dealWithEmvCodeSuccess(MpQrUrlInfoResult mpQrUrlInfoResult) {

        toDealUpEMV(mpQrUrlInfoResult.getQrcodeStr(), mpQrUrlInfoResult);

    }

    @Override
    public void dealWithEmvCodeError() {
        hasDialogShow = true;
        showErrorMsgDialog(getActivity(), getString(R.string.QRW209_1_1), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                hasDialogShow = false;
                restartPreviewAfterDelay(500);
            }
        });
    }

    @Override
    public void getActionTrxGpInfoError(String errorCode, String errorMsg) {
        hasDialogShow = true;
        mpQrUrlInfoResult.setCardId(originalCardId);
        showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                hasDialogShow = false;
                restartPreviewAfterDelay(500);
            }
        });
    }

    @Override
    public void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity parserQrcodeEntity) {
        showCheckStand(parserQrcodeEntity);
    }

    @Override
    public void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards,
                                          boolean isUrlMode) {
        showPaymentListDialog(bankCards, isUrlMode);
    }

    @Override
    public void getPaymentTypeListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
    }

    /**
     * 下单轮训接口成功
     *
     * @param response
     */
    @Override
    public void getPayMentResultSuccess(PaymentEnquiryResult response) {
        if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_SUCCESS)) {
            dismissDialog();
            GradeReduceSuccessActivity.startActivityFromMajorScan(getActivity(), response);
        }
    }

    @Override
    public void getPayMentResultError(String errorCode, String errorMsg) {
        dismissDialog();
        showErrorMsgDialog(getActivity(), errorMsg);
    }


    @Override
    public void getMainScanOrderSuccess(ContentEntity response) {
        String tansId = response.getResult_msg();
        queryOrderStatus(tansId);
    }

    /**
     * 主扫下单报错
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getMainScanOrderError(final String errorCode, final String errorMsg) {
        hasDialogShow = true;
        showErrorMsgDialog(getContext(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                hasDialogShow = false;
                boolean isFlag = actionTrxGpInfoEntity.isUrlCode() && (errorCode.equals("EWA3015") || errorCode.equals("EWA3004") || errorCode.startsWith("EWA80"));
                if (isFlag) {
                    //停止当前流程
                    if (orderDetailDialogFragment != null) {
                        orderDetailDialogFragment.dismiss();
                    }
                    restartPreviewAfterDelay(500);
                }
            }
        });
    }

    /**
     * 本地二维码bitMap解析
     *
     * @param content
     */
    @Override
    public void parserLocalQrcodeSuccess(String content) {
//        LogUtils.i("TESTCLICK","parserLocalQrcodeSuccess1---->");
        isSpecialPageEvent = false;
        if (TextUtils.isEmpty(content)) {
            handleScanError();
        } else {
            parseCodeStr(content);
        }
    }

    @Override
    public void scanQrCodeTypeSuccess(QrcodeTypeEntity qrcodeTypeEntity) {
        //跳转时，禁止点击
        currentQRcType = qrcodeTypeEntity.getQrcType();
        if (qrcodeTypeEntity.getAction().equals(QrcodeTypeEntity.QR_EMV_PAY)) {
            dealWithEmvCode(mCurrentQrcode);
        } else if (qrcodeTypeEntity.getAction().equals(QrcodeTypeEntity.QR_UNION_PAY)) {
            dealWithUrlModeCode(mCurrentQrcode);
        } else if (qrcodeTypeEntity.getAction().equals(QrcodeTypeEntity.QR_WEB_PAY) || qrcodeTypeEntity.getAction().equals(QrcodeTypeEntity.QR_MER_PAY)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, qrcodeTypeEntity.getPayUrl());
            mHashMaps.put(Constants.WALLETID, qrcodeTypeEntity.getWalletId());
            //卡列表中默认的卡cardid
            mHashMaps.put(Constants.CARDID, CacheManagerInstance.getInstance().getDefaultCardEntitity().getCardId());
            mHashMaps.put(Constants.QRCTYPE, currentQRcType);
            ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewPayActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            //跳转后需要清空
            qrCodeInfo = "";
        } else if (qrcodeTypeEntity.getAction().equals(QrcodeTypeEntity.UPLAN_MERCHANT_URL)) {
            //銀聯Uplan優惠券場景 – 相同優惠
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, qrcodeTypeEntity.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, qrcodeTypeEntity.getRedirectUrl());
            ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void scanQrCodeTypeFailed(String errorCode, String errorMsg) {
        hasDialogShow = true;
        showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                hasDialogShow = false;
                restartPreviewAfterDelay(500);
            }
        });
    }

    /**
     * 是否是聚合码
     */
    private void toDealUpEMV(String qrcode, MpQrUrlInfoResult mpQrUrlInfoResult) {
        if (!TextUtils.isEmpty(mpQrUrlInfoResult.getTranAmt())) {
            initOrderPopWindow(mpQrUrlInfoResult);
        } else {
            turnOnFlashLight(false);
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.URLQRINFO_ENTITY, mpQrUrlInfoResult);
            mHashMaps.put(Constants.SCAN_CARD_TYPE, currentQRcType);
            mHashMaps.put(Constants.QRCODE_CONTENT, qrcode);
            ActivitySkipUtil.startAnotherActivity(getActivity(), InputMoneyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            qrCodeInfo = "";
        }
    }


    /**
     * 从相册选择二维码
     */
    private void pickFromAlbum() {
        isSpecialPageEvent = true;
        Intent innerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        //如果修改为 image/png P30 分享会报错 分享到备忘录 报图片太大 只能image/*
        innerIntent.setType("image/*");
        Intent wrapperIntent = Intent.createChooser(innerIntent, getResources().getString(R.string.choose_qrcode_fps));
        this.startActivityForResult(wrapperIntent, REQUEST_PICK_FORM_ALBUM);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_PICK_FORM_ALBUM:
                    mMainPresenter.parserLocalQrcode(getActivity(), data.getData());
                    break;

            }
        } else {
            isSpecialPageEvent = false;
        }

    }

    public void handleScanError() {
        if (getActivity() != null) {
            stopPreview();
            hasDialogShow = true;
            showErrorMsgDialog(getActivity(), getString(R.string.QRW209_1_1), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    hasDialogShow = false;
                    restartPreviewAfterDelay(500);
                }
            });
        }
    }


}
