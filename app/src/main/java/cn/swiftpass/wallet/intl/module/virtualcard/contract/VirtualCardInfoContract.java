package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.CardsEntity;


public class VirtualCardInfoContract {

    public interface View extends IView {

        void getCardListSuccess(CardsEntity response);

        void getCardListError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {
        /**
         * 拉取虚拟卡列表
         */
        void getCardList();
    }

}
