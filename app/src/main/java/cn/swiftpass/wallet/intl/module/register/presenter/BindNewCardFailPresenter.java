package cn.swiftpass.wallet.intl.module.register.presenter;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.BindNewCardListProtocol;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardFailContract;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;


public class BindNewCardFailPresenter extends BasePresenter<BindNewCardFailContract.View> implements BindNewCardFailContract.Presenter {


    @Override
    public void bindNewCardList(ArrayList<BindNewCardEntity> selectCardList, String action) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BindNewCardListProtocol(selectCardList, action, new NetWorkCallbackListener<BindNewCardResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BindNewCardResultEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardListSuccess(response);
                }
            }
        }).execute();

    }


}
