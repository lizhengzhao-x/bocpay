package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * 主动发起的自动登录接口  异步处理
 */
public class AutoLoginAsynProtocol extends BaseProtocol {
    NetWorkCallbackListener mLoginCallback;

    public AutoLoginAsynProtocol(NetWorkCallbackListener dataCallback) {
        mDataCallback = new AutoLoginCallback();
        mLoginCallback = dataCallback;
        mUrl = "api/login/auto";
    }

    class AutoLoginCallback extends NetWorkCallbackListener<AutoLoginSucEntity> {

        @Override
        public void onFailed(String errorCode, String errorMsg) {
            LogUtils.i(TAG, "AutoLoginCallback onFailed");
            //todo fix 是否这里的清除也需要清卡数据
            if (ErrorCode.FPS_SESSION_INVALID.code.equalsIgnoreCase(errorCode)) {
                HttpCoreKeyManager.getInstance().saveSessionId("");
                HttpCoreKeyManager.getInstance().setUserId("");
                CacheManagerInstance.getInstance().saveLogoutStatus();
                SystemInitManager.getInstance().setValidShowAccount(false);
            }
            if (null != mLoginCallback) {
                mLoginCallback.onFailed(errorCode, errorMsg);
            }
        }

        @Override
        public void onSuccess(AutoLoginSucEntity response) {
            LogUtils.i(TAG, "AutoLoginCallback onSuccess");
            HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
            HttpCoreKeyManager.getInstance().setUserId(response.getWalletId());
            //自动登录成功保存卡列表
            CacheManagerInstance.getInstance().setCardEntities(response.getCards().getRows());
            CacheManagerInstance.getInstance().saveOpenBiometricAuth(response.isOpenBiometricAuth());
            CacheManagerInstance.getInstance().saveLoginStatus();
            if (null != mLoginCallback) {
                mLoginCallback.onSuccess(response);
            }
        }
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }
}
