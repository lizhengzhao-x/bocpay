package cn.swiftpass.wallet.intl.module.home.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.NewMsgEntity;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;

import static cn.swiftpass.wallet.intl.module.home.utils.BannerUtils.BANNER_IMAGE_PROPORTION;

public class HomeBannerAdapter extends RecyclerView.Adapter {
    private ArrayList<NewMsgEntity> mCurrentData = new ArrayList<>();
    private Context context;


    private OnItemClickListener onItemClickListener;

    public HomeBannerAdapter(Context context, OnItemClickListener onItemClickListenerIn) {
        this.context = context;
        this.onItemClickListener = onItemClickListenerIn;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_home_bottom_bannner, parent, false);
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.height = (int) (ScreenUtils.getDisplayHeight() * 0.234f);
        layoutParams.width = (int) (ScreenUtils.getDisplayHeight() * 0.234f /BANNER_IMAGE_PROPORTION);
        return new HomeBannerHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HomeBannerHolder) {
            HomeBannerHolder bannerHolder = (HomeBannerHolder) holder;
            NewMsgEntity newMsgEntity = mCurrentData.get(position);

            if (TextUtils.isEmpty(newMsgEntity.getBottomImageUrl())) {
                if (newMsgEntity.getLocalDefaultImageUrl() != -1) {
                    //防止后台返回这个url为空
                    GlideApp.with(context)
                            .load(newMsgEntity.getLocalDefaultImageUrl())
                            .placeholder(R.mipmap.img_banner_skeleton)
                            .into(bannerHolder.homeBannerIv);
                }
            } else {
                GlideApp.with(context)
                        .load(newMsgEntity.getBottomImageUrl())
                        .placeholder(R.mipmap.img_banner_skeleton)
                        .into(bannerHolder.homeBannerIv);
            }


            bannerHolder.homeBannerIv.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return mCurrentData == null ? 0 : mCurrentData.size();
    }

    public void setmCurrentData(ArrayList<NewMsgEntity> mCurrentData) {
        this.mCurrentData = mCurrentData;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {

        void onItemClick(int position);
    }
}
