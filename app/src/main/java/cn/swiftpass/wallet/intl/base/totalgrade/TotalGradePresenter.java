package cn.swiftpass.wallet.intl.base.totalgrade;

import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * 获取用户总积分
 * Created by ZhangXinchao on 2019/7/16.
 */
public interface TotalGradePresenter<V extends IView> {

    void getUserTotalGrade();
}
