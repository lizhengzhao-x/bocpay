package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.PaymentListEntity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class SettingCardDefaultAcitivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    //支付列表
    private ArrayList<PaymentListEntity.RowsBean> cardEntities;
    private final int REQUEST_CODE = 11111;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.title_default_account);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        cardEntities = new ArrayList<>();
        getPaymentList();
    }

    /**
     * 获取支付方式
     */
    private void getPaymentList() {
        ApiProtocolImplManager.getInstance().getCardList(this, new NetWorkCallbackListener<PaymentListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(SettingCardDefaultAcitivity.this, errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        finish();
                    }
                });
            }

            @Override
            public void onSuccess(PaymentListEntity response) {
                cardEntities.clear();
                refreshViewWithData(response.getRows());
            }
        });

    }

    private void refreshViewWithData(List<PaymentListEntity.RowsBean> rows) {

        if (rows != null && rows.size() > 0) {
            cardEntities.addAll(rows);
        }
        for (int i = 0; i < rows.size(); i++) {
            PaymentListEntity.RowsBean rowsBean = rows.get(i);
            if (rowsBean.getTypes() != null && rowsBean.getTypes().size() > 0) {
                for (int j = 0; j < rowsBean.getTypes().size(); j++) {
                    PaymentListEntity.RowsBean.TypesBean typesBean = rowsBean.getTypes().get(j);
           /*         {
                        "isDefault":0,：是否默认：1-是|0-否
                        "payType":1：支付方式：1：银联2：fps
                    }*/
                    if (typesBean.getIsDefault().equals("1")) {
                        rowsBean.setSubTitle(typesBean.getPayType().equals("1") ? getResources().getString(R.string.A1_2_1) : getResources().getString(R.string.A1_3_1));
                    }
                }
            }
        }

        ((IAdapter<PaymentListEntity.RowsBean>) idRecyclerview.getAdapter()).setData(cardEntities);
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }


    private void setDefaultCardRequest(String newPanId, final String defaultPayType) {
        ApiProtocolImplManager.getInstance().setDefaultCard(mContext, newPanId, defaultPayType, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(SettingCardDefaultAcitivity.this, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                refreshLocalCardList();

            }
        });
    }

    /**
     * 更改默认支付方式之后 重新拉去一次卡列表
     */
    private void refreshLocalCardList() {
        CommonRequestUtils.getCardsListWithNoDialog(getActivity(), HttpCoreKeyManager.getInstance().getUserId(), true, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                finish();
            }

            @Override
            public void onSuccess(CardsEntity response) {
                ArrayList<BankCardEntity> rows = response.getRows();
                if (rows != null && rows.size() > 0) {
                    CacheManagerInstance.getInstance().setCardEntities(rows);
                }
                finish();
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_setdefaultcard;
    }


    private CommonRcvAdapter<PaymentListEntity.RowsBean> getAdapter(final List<PaymentListEntity.RowsBean> data) {
        return new CommonRcvAdapter<PaymentListEntity.RowsBean>(data) {

            @Override
            public Object getItemType(PaymentListEntity.RowsBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new SetDefaultCardListAdapter(mContext, new SetDefaultCardListAdapter.CardItemSelCallback() {
                    @Override
                    public void onItemCardSelClick(int position) {
                        if (cardEntities.size() == 0) {
                            finish();
                            return;
                        }
                        if (cardEntities.get(position).getTypes() != null && cardEntities.get(position).getTypes().size() > 0) {
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.EXTRA_TYPE, cardEntities.get(position).getTypes());
                            mHashMaps.put(Constants.CARD_ID, cardEntities.get(position).getCardId());
                            mHashMaps.put(Constants.DEFAULTPAYTYPE, cardEntities.get(position).getDefaultPayType());
                            ActivitySkipUtil.startAnotherActivityForResult(getActivity(), SettingPaymentDefaultActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_CODE);
                            return;
                        }
                        if (cardEntities.get(position).getIsDefault().equals("1")) {
                            return;
                        }
                        setDefaultCardRequest(cardEntities.get(position).getCardId() + "", cardEntities.get(position).getDefaultPayType());
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                    }
                });
            }
        };
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && RESULT_OK == resultCode) {
            refreshLocalCardList();
            finish();
        }
    }
}
