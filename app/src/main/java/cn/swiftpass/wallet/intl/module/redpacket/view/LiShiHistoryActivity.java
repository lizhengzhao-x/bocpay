package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.redpacket.contract.LiShiHistoryContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.LishiHistoryEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.module.redpacket.presenter.LiShiHistoryPresenter;
import cn.swiftpass.wallet.intl.module.redpacket.view.adapter.LishiHistoryAdapter;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

/**
 * 红包 收利是和派利是 公用 历史记录
 */
public class LiShiHistoryActivity extends BaseCompatActivity<LiShiHistoryContract.Presenter> implements LiShiHistoryContract.View {
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    @BindView(R.id.ry_lisi_history)
    RecyclerView ryLisiHistory;
    private LishiHistoryAdapter lishiHistoryAdapter;
    private List<RedPacketBeanEntity> items;

    private View emptyView;

    /**
     * 当前页面  默认为第一页
     */
    private int mCurrentPage = 1;
    /**
     * 每一页数据  默认为20条
     */
    private int mPageSize = 20;
    private String action;


    public static final String ACTION_TYPE = "action_type";

    @Override
    protected int getLayoutId() {
        return R.layout.act_lisi_history;
    }


    @Override
    protected void init() {
        if (null != getIntent() && null != getIntent().getExtras()) {
            action = getIntent().getStringExtra(Constants.TRANSACTIONTYPE);
        }
        String titleStr = getString(R.string.RP2101_23_7);
        if (TextUtils.equals(Constants.SEND, action)) {
            titleStr = getString(R.string.RP2101_10_7);
        }
        setToolBarTitle(titleStr);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        ryLisiHistory.setLayoutManager(mLayoutManager);
        items = new ArrayList<>();
        lishiHistoryAdapter = new LishiHistoryAdapter(items, action, mContext);
        lishiHistoryAdapter.bindToRecyclerView(ryLisiHistory);

        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                //清除通讯录
                ContactCompareUtils.clearAllContactList();
                mCurrentPage = 1;
                mPresenter.getLiShiHistory(mCurrentPage, mPageSize, action);
            }
        });

        lishiHistoryAdapter.setOnLoadMoreListener(new BaseRecyclerAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                mPresenter.getLiShiHistory(mCurrentPage, mPageSize, action);
            }
        }, ryLisiHistory);
        mPresenter.getLiShiHistory(mCurrentPage, mPageSize, action);
    }

    @Override
    public void getLiShiHistorySuccess(LishiHistoryEntity response) {
        smartRefreshLayout.finishRefresh();

        if (mCurrentPage == 1) {
            items.clear();

        }
        ContactCompareUtils.compareHisotryLocalRecord(this, response.getRows(), new ContactCompareUtils.CompareRedPacketsListResult() {

            @Override
            public void getRedPackets(List<RedPacketBeanEntity> redPacketBeanEntities) {
                if (null != items && null != lishiHistoryAdapter) {
                    items.addAll(redPacketBeanEntities);
                    lishiHistoryAdapter.setDataList(items);
                }
            }
        });


        /**
         * 刷新到了最后一页
         */
        if (items.size() >= response.getTotal()) {
            lishiHistoryAdapter.loadMoreEnd(true);
        } else {
            lishiHistoryAdapter.loadMoreComplete();
        }

        emptyView = getEmptyView(mContext, ryLisiHistory, "", R.mipmap.img_defaultpage_redpacket_empty);
        lishiHistoryAdapter.setEmptyView(emptyView);

        if (items.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            emptyView.setVisibility(View.GONE);
        }

    }


    @Override
    public void getLiShiHistoryError(String errorCode, String errorMsg) {
        smartRefreshLayout.finishRefresh();
        showErrorMsgDialog(this, errorMsg);
    }


    @Override
    protected LiShiHistoryContract.Presenter createPresenter() {
        return new LiShiHistoryPresenter();
    }

    /**
     * 页面添加空布局  直接用该方法
     *
     * @param mContext
     * @param recyclerView
     * @param textId
     * @param imgId
     * @return
     */
    protected View getEmptyView(Context mContext, RecyclerView recyclerView, String str, int imgId) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(str);
        if (imgId == -1) {
            noDataImg.setVisibility(View.GONE);
        } else {
            noDataImg.setVisibility(View.VISIBLE);
            noDataImg.setImageResource(imgId);
        }
        return emptyView;
    }


}
