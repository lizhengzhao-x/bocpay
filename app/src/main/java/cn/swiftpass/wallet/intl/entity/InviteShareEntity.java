package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class InviteShareEntity extends BaseEntity {


    /**
     * rewardRule : 每成功邀请一个亲朋好友可得到<money>$10奖赏</money> \n成功注册的亲朋好友亦可得到<money>$50元奖赏</money>
     * startDate : 2019/04/22
     * picUrl : http://newpic.jxnews.com.cn/0/13/50/84/13508441_543152.png
     * invitationTime : 2019-04-22 13:11:23 至 2019-06-10 20:10:23
     * rewardesc : 推廣期内推荐亲朋好友注册BoC Pay，即可齐齐获赏！每成功推荐一人可获HK$10奖赏，推荐越多，奖赏越丰富，绝无上限！被推荐人成功注册/綁定亦可获HK$50奖赏，赶快邀请您身边的人下载，有赏齐齐享?
     * inCode : 87654321
     * endDate : 2019/06/10
     * shareInfo : 有好消息告诉您！现成功注册BoC Pay更可获取$50奖赏！紧记输入我的邀请码｛code｝呀！立即浏览https://www.bochk.com/tc/more/ebanking/bocpay.html下载BoC Pay！
     * subContent : 紧记提示亲朋好友于注册/綁定BoC Pay时输入您的邀请码，齐齐拿奖赏！
     */

    private String rewardRule;


    private String activityTncUrl;
    private String startDate;
    private String picUrl;
    private String invitationTime;
    private String rewardesc;
    private String inCode;
    private String endDate;
    private String shareInfo;
    private String subContent;
    /**
     * MGM邀请通讯录  每页查询条目数量
     */
    private String mgmMobilePageSize;
    /**
     * 单独控制显示邀请码 1显示 其他不显示
     */
    private String showInvitationCode;
    //inviteDateStatus   活动时间状态  0：活动前  1： 活动后  2：活动间
    private String inviteDateStatus;

    public String getActivityTncUrl() {
        return activityTncUrl;
    }

    public void setActivityTncUrl(String activityTncUrl) {
        this.activityTncUrl = activityTncUrl;
    }

    public int getMgmMobilePageSize() {
        try {
            if ("0".equals(mgmMobilePageSize)) {
                return 20;
            } else {
                return Integer.parseInt(mgmMobilePageSize);
            }
        } catch (Exception e) {
            return 20;
        }
    }

    public void setMgmMobilePageSize(String mgmMobilePageSize) {
        this.mgmMobilePageSize = mgmMobilePageSize;
    }

    public boolean showInviteCode() {

        if (TextUtils.isEmpty(showInvitationCode)) {
            return false;
        }
        return showInvitationCode.equals("1");
    }


    public String getInviteDateStatus() {
        return inviteDateStatus;
    }

    public void setInviteDateStatus(String inviteDateStatus) {
        this.inviteDateStatus = inviteDateStatus;
    }

    public String getOfflineStatus() {
        return offlineStatus;
    }

    public void setOfflineStatus(String offlineStatus) {
        this.offlineStatus = offlineStatus;
    }

    //offlineStatus： 0：是offline  1：不是offline
    private String offlineStatus;

    public boolean isStatusEnd() {
        if (TextUtils.isEmpty(offlineStatus)) {
            return true;
        }
        return offlineStatus.equals("0");
    }


    public String getRewardRule() {
        return rewardRule;
    }

    public void setRewardRule(String rewardRule) {
        this.rewardRule = rewardRule;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getInvitationTime() {
        return invitationTime;
    }

    public void setInvitationTime(String invitationTime) {
        this.invitationTime = invitationTime;
    }

    public String getRewardesc() {
        return rewardesc;
    }

    public void setRewardesc(String rewardesc) {
        this.rewardesc = rewardesc;
    }

    public String getInCode() {
        return inCode;
    }

    public void setInCode(String inCode) {
        this.inCode = inCode;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getShareInfo() {
        return shareInfo;
    }

    public void setShareInfo(String shareInfo) {
        this.shareInfo = shareInfo;
    }

    public String getSubContent() {
        return subContent;
    }

    public void setSubContent(String subContent) {
        this.subContent = subContent;
    }
}
