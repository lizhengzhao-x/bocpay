package cn.swiftpass.wallet.intl.entity.event;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class DeepLinkEvent extends BaseEntity {

    public static final int DEEP_LINK = 9001;


    private int eventType;
    private String eventMessage;


    public DeepLinkEvent(int eventType, String message) {
        this.eventType = eventType;
        this.eventMessage = message;
    }


    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }
}
