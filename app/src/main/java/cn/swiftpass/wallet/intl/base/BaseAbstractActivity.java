package cn.swiftpass.wallet.intl.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.alibaba.fastjson.JSONObject;
import com.bocpay.analysis.AnalysisManager;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.intl.module.guide.SplashActivity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisBaseEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisImplManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LanguageUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.CustomProgressDialog;
import cn.swiftpass.wallet.intl.widget.PasswordInputView;
import me.jessyan.autosize.AutoSize;

public abstract class BaseAbstractActivity<P extends IPresenter> extends AppCompatActivity implements IView {
    protected Context mContext;
    protected LayoutInflater mInflater = null;
    protected View contentView;

    protected static final int PERMISSION_REQUESTCODE = 100;

    protected static final String TAG = "BaseActivity";
    protected KProgressHUD mKProgressHUD;
    protected P mPresenter;


    protected PermissionListener mListener;
    protected CustomProgressDialog progressDialog;
    protected Handler mHandler;

    protected static final int PERMISSON_REQUESTCODE = 0;

    protected int mTouchSlop;
    public long startTime = 0L;

    public static final int SYSTEM_UI_MODE_NONE = 0;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS = 1;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS = 2;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION = 3;
    public static final int SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION = 4;
    public static final int SYSTEM_UI_MODE_FULLSCREEN = 5;
    public static final int SYSTEM_UI_MODE_LIGHT_BAR = 6;
    private int progressDialogAlpha = 0;

    public HashMap<String, CustomMsgDialog> getDialogShowMsg() {
        return dialogShowMsg;
    }

    private final HashMap<String, CustomMsgDialog> dialogShowMsg = new HashMap<>();
    private boolean isFinishing;


    @IntDef({SYSTEM_UI_MODE_NONE, SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS, SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS, SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION, SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION, SYSTEM_UI_MODE_FULLSCREEN, SYSTEM_UI_MODE_LIGHT_BAR})
    @Retention(RetentionPolicy.SOURCE)
    private @interface SystemUiMode {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtils.i(TAG, "onCreate>" + this + " savedInstanceState:" + savedInstanceState);
        if (savedInstanceState != null) {
            //解决华为手机权限变化 app重启问题
            LogUtils.i(TAG, "savedInstanceState------>");
            Intent intent = new Intent(this, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            overridePendingTransition(0, 0);
            return;
        }

        mHandler = new Handler();
        mTouchSlop = ViewConfiguration.get(this).getScaledTouchSlop();
        mContext = this;
        setScreenOrientation();
        //根据上次的语言设置，重新设置语言

//        switchLanguage(SpUtils.getInstance(this).getAppLanguage());
        mInflater = LayoutInflater.from(this);
        //将activity进行统一管理
        ProjectApp.addTastStack(this);

        //MVP P层引用
        mPresenter = createPresenter();
        if (mPresenter == null) {
            LogUtils.i(TAG, "Presenter is null! Do you return null in createPresenter()?");
        } else {
            mPresenter.onAttachView(this);
        }

    }


    @Override
    protected void attachBaseContext(Context newBase) {
        Locale locale = LanguageUtils.getCurrentLocale(newBase.getApplicationContext());
        LanguageUtils.updatelocalSpLanguage(locale);
        LogUtils.i(TAG, "attachBaseContext------>" + newBase + "locale:" + locale.getLanguage());
        super.attachBaseContext(LanguageUtils.wrapContextCompat(newBase, locale));
    }


    protected boolean useScreenOrientation() {
        return false;
    }

    /**
     * app统一设置背景图片 默认都是 back_layer_clear_small 如果自定义设置 直接在布局中设置即可
     * back_layer_clear_small 这种方案会造成圆角适配问题 很难解决
     * 电子券版本 优化此方案 改为 toolbar一个背景图 剩余下边一个背景图 这种效果需要在布局文件中指定
     * toolbar include_top_view_noline_withbg
     * 为了整体实现顶部背景图 底部圆角 另 底部跟顶部衔接效果 现在分为两块
     */
    private void setScreenOrientation() {
        if (!useScreenOrientation()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }


    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        AutoSize.autoConvertDensityOfGlobal(this);
        return super.onCreateView(parent, name, context, attrs);
    }


    protected boolean notUsedToolbar() {
        return false;
    }

    protected boolean notUsedCustomBackground() {
        return false;
    }


    public void setWindowBrightness(float brightness) {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = brightness;
        window.setAttributes(lp);
    }


    public void setSystemUiMode(@SystemUiMode int mode) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        switch (mode) {
            case SYSTEM_UI_MODE_NONE:
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    TypedValue value = new TypedValue();
                    getTheme().resolveAttribute(R.attr.colorPrimary, value, true);
                    window.setStatusBarColor(value.data);
                }
                break;
            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS:
            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS:
                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
                break;
            case SYSTEM_UI_MODE_TRANSPARENT_BAR_STATUS_AND_NAVIGATION:
            case SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION:
                if (mode == SYSTEM_UI_MODE_TRANSPARENT_LIGHT_BAR_STATUS_AND_NAVIGATION && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR | View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR);
                    } else {
                        window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    }
                } else {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setNavigationBarColor(Color.TRANSPARENT);
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
                break;
            case SYSTEM_UI_MODE_FULLSCREEN:
                window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                break;
            case SYSTEM_UI_MODE_LIGHT_BAR:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    window.setStatusBarColor(Color.TRANSPARENT);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 禁止屏幕截屏
     */
    public void forbidScreen() {
        //禁止app截屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
    }

    @Override
    protected void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isAnalysisPage()) {
            if (getAnalysisPageEntity() != null) {
                long timeDuration = System.currentTimeMillis() - startTime;
                AnalysisPageEntity analysisPageEntity = getAnalysisPageEntity();
                analysisPageEntity.setDate_time(startTime);
                analysisPageEntity.setTime_duration(timeDuration);
                JSONObject jsonObject = analysisPageEntity.getJsonObject();
                AnalysisManager.getInstance().sendAnalysisEvent(jsonObject);
            }
        }

        if (ProjectApp.getFrontActivityNumber() == 0) {
            // app回到后台 做出提示
            LogUtils.i(TAG, "app回到后台");
            //app退到后台 强制上传数据
            AnalysisManager.getInstance().uploadDataWhenAppGoBackGround();
        }
    }

    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//


    public void showErrorMsgDialog(Context mContext, String title, String msg) {
        showErrorMsgDialog(mContext, title, msg, R.string.POPUP1_1, null);
    }

    public void showErrorMsgDialog(Context mContext, String msg) {
        showErrorMsgDialog(mContext, null, msg, R.string.POPUP1_1, null);
    }

    public void showErrorMsgDialog(Context mContext, String msg, int positiveResId, OnMsgClickCallBack onMsgClickCallBack) {
        showErrorMsgDialog(mContext, null, msg, positiveResId, onMsgClickCallBack);
    }

    public void showErrorMsgDialog(Context mContext, String msg, int positiveResId) {
        showErrorMsgDialog(mContext, null, msg, positiveResId, null);
    }

    public void showErrorMsgDialog(Context mContext, String title, String msg, OnMsgClickCallBack onMsgClickCallBack) {
        showErrorMsgDialog(mContext, title, msg, R.string.POPUP1_1, onMsgClickCallBack);
    }

    public void showErrorMsgDialog(Context mContext, String msg, OnMsgClickCallBack onMsgClickCallBack) {
        showErrorMsgDialog(mContext, null, msg, R.string.POPUP1_1, onMsgClickCallBack);
    }

    public void showErrorMsgDialogWithButtomMsg(Context mContext, String msg, int btnMsg, OnMsgClickCallBack onMsgClickCallBack) {
        showErrorMsgDialog(mContext, null, msg, btnMsg, onMsgClickCallBack);
    }


    /**
     * 相同内容的错误弹出窗口不会被弹出，所以可能存在相同内容的弹出窗口，部分点击事件丢失
     *
     * @param mContext
     * @param title
     * @param msg
     * @param positiveResId
     * @param onMsgClickCallBack
     */
    public void showErrorMsgDialog(Context mContext, String title, String msg, int positiveResId, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        if (title != null) {
            builder.setTitle(title);
        }
        builder.setMessage(msg);
        String finalMsg = msg;
        builder.setPositiveButton(mContext.getString(positiveResId), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //如果activity在ondestory则不再进行dialog dismiss操作等待ondestory中进行dismiss
                if (!isFinishing) {
                    dialog.dismiss();
                    try {
                        dialogShowMsg.remove(finalMsg);
                    } catch (Exception e) {

                    }
                }
                if (onMsgClickCallBack != null) {
                    onMsgClickCallBack.onBtnClickListener();
                }
            }
        });
        Activity mActivity = (Activity) mContext;
        //如果activity在ondestory则不再弹出dialog
        if (!mActivity.isFinishing() && (!isFinishing)) {
            if (!dialogShowMsg.containsKey(msg)) {
                CustomMsgDialog msgDialog = builder.create();
                msgDialog.show();
                try {
                    dialogShowMsg.put(msg, msgDialog);
                } catch (Exception e) {

                }
            }
        }
    }


    public void setProgressDialogAlpha(int alpha) {
        this.progressDialogAlpha = alpha;
    }


    private CustomDialog mPermissionDealDialog;

    protected void showLackOfPermissionDialog(final Activity mActivityIn) {
        if (mPermissionDealDialog != null && mPermissionDealDialog.isShowing()) {
            return;
        }

        CustomDialog.Builder builder = new CustomDialog.Builder(mActivityIn);
        builder.setTitle(getString(R.string.SH2_4_1a));
        builder.setMessage(getString(R.string.SH2_4_1b));

        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mPermissionDealDialog = null;
                AndroidUtils.startAppSetting(mActivityIn);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                mPermissionDealDialog = null;
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);


        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mPermissionDealDialog = builder.create();
            mPermissionDealDialog.setCancelable(false);
            mPermissionDealDialog.show();
        }
    }

    @Override
    public void showDialogNotCancel() {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) this;
                LogUtils.i(TAG, "current activity" + activity);
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.dialog_message), progressDialogAlpha);
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!progressDialog.isShowing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            if (progressDialog != null) {
                try {
                    if (!isFinishing()) {
                        progressDialog.setCancel(false);
                        if (!progressDialog.isShowing()) {
                            progressDialog.show();
                        }
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, e.getMessage());
                }

            }
        }

    }


    public void showDialogNotCancel(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.dialog_message));
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            if (progressDialog != null) {
                try {
                    if (!isFinishing()) {
                        progressDialog.setCancel(false);
                        progressDialog.show();
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, e.getMessage());
                }

            }
        }

    }

    @Override
    public void dismissDialog() {
        if (progressDialog == null) {
            return;
        }
        try {
            if (mHandler != null) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null && getActivity() != null) {
                            progressDialog.dismiss();
                            progressDialog = null;
                        }
                    }
                }, 200);
            }

        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }

    }

    /**
     * 网络请求前显示dialog
     *
     * @param canDismiss
     */
    public void showLoading(Context mContext, boolean canDismiss) {
        mKProgressHUD = KProgressHUD.create(mContext);
        mKProgressHUD.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE).setCancellable(canDismiss).setAnimationSpeed(2).setDimAmount(0.5f).show();
    }

    /**
     * 网络请求之后取消显示加载中dialog
     */
    public void hideLoading() {
        if (mKProgressHUD != null) {
            mKProgressHUD.dismiss();
        }
    }


    //--------------------------- -------------------------------------- 显示错误dialog -------------------------------------- --------------------------------------//

    //--------------------------- -------------------------------------- 权限相关 -------------------------------------- --------------------------------------//

    /**
     * @since 2.5.0
     * requestPermissions方法是请求某一权限，
     */
    public void checkPermissions(String... permissions) {
        List<String> needRequestPermissonList = findDeniedPermissions(permissions);
        if (null != needRequestPermissonList && needRequestPermissonList.size() > 0) {
            ActivityCompat.requestPermissions(this, needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]), PERMISSON_REQUESTCODE);
        }
    }

    protected List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                needRequestPermissonList.add(perm);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                    needRequestPermissonList.add(perm);
                }
            }
        }
        return needRequestPermissonList;
    }


    public boolean isGranted(String permission) {
        return !isMarshmallow() || isGranted_(permission);
    }

    private boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public boolean isGranted_(String permission) {
        int checkSelfPermission = ActivityCompat.checkSelfPermission(this, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * 检测是否所有的权限都已经授权
     *
     * @param grantResults
     * @return
     * @since 2.5.0
     */
    protected boolean verifyPermissions(int[] grantResults) {
        for (int result : grantResults) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    /**
     * 权限申请回调
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUESTCODE:
                if (grantResults.length > 0) {
                    //存放没授权的权限
                    List<String> deniedPermissions = new ArrayList<>();
                    for (int i = 0; i < grantResults.length; i++) {
                        int grantResult = grantResults[i];
                        String permission = permissions[i];
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            deniedPermissions.add(permission);
                        }
                    }
                    if (deniedPermissions.isEmpty()) {
                        //说明都授权了
                        mListener.onGranted();
                    } else {
                        mListener.onDenied(deniedPermissions);
                    }
                }
                break;
            default:
                break;
        }
    }

    public void sendAnalysisEvent(AnalysisBaseEntity analysisBaseEntity) {
        AnalysisImplManager.sendAnalysisEvent(analysisBaseEntity);
    }


    //--------------------------- -------------------------------------- Toolbar相关 -------------------------------------- --------------------------------------//

    /**
     * 更新按钮的状态
     *
     * @param tv
     * @param isSel
     */
    public void updateOkBackground(TextView tv, boolean isSel) {
        tv.setEnabled(isSel);
        tv.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_transfer_normal : R.drawable.bg_btn_next_page_disable);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            //code........
            onBackPressed();
            ((Activity) mContext).overridePendingTransition(R.anim.slide_left_in, R.anim.slide_right_out);
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.onDetachView();
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
        isFinishing = true;
        //判断有多少弹出的dialog 弹出窗口并隐藏
        try {
            if (dialogShowMsg != null) {
                Iterator<Map.Entry<String, CustomMsgDialog>> iterator = dialogShowMsg.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, CustomMsgDialog> entry = iterator.next();
                    CustomMsgDialog dialog = entry.getValue();
                    if (dialog != null) {
                        dialog.dismiss();
                    }
                    iterator.remove();
                }
            }
        } catch (Exception e) {

        }
        ProjectApp.removeTaskStack(this);

    }


    /**
     * abstract方法，在子类实现，用来获取要显示的view的ID
     *
     * @return
     */
    protected abstract int getLayoutId();

    protected BaseAbstractActivity<P> getActivity() {
        return this;
    }


    public void switchLanguage(String language) {
        //设置应用语言类型
        Resources resources = mContext.getApplicationContext().getResources();
        Configuration config = resources.getConfiguration();
        DisplayMetrics dm = resources.getDisplayMetrics();

        if (AndroidUtils.isHKLanguage(language)) {
            // 繁体
            config.locale = Locale.TRADITIONAL_CHINESE;
        } else if (AndroidUtils.isEGLanguage(language)) {
            config.locale = Locale.ENGLISH;
        } else if (AndroidUtils.isZHLanguage(language)) {
            config.locale = Locale.SIMPLIFIED_CHINESE;
        } else {
            // 跟随系统//默认英文
            config.locale = Locale.getDefault();
        }
        if (!AndroidUtils.isEmptyOrNull(language)) {
            HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, language);
        } else {
            Locale locale = getResources().getConfiguration().locale;
            String lan = locale.getLanguage() + "-" + locale.getCountry();
            if (AndroidUtils.isZHLanguage(lan)) {
                config.locale = Locale.SIMPLIFIED_CHINESE;
            } else if (AndroidUtils.isHKLanguage(lan)) {
                config.locale = Locale.TRADITIONAL_CHINESE;
            } else {
                config.locale = Locale.ENGLISH;
                lan = HttpCoreConstants.LANG_CODE_EN_US;
            }
            HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, lan);
        }
        resources.updateConfiguration(config, dm);
    }

    /**
     * 判断页面是否需要统计埋点(用来判断部分共用页面流程,如忘记密码 注册 绑卡等) 默认为false
     */
    public boolean isAnalysisPage() {
        return false;
    }

    /**
     * 埋点对象  由需要埋点的页面子类实现
     */
    public AnalysisPageEntity getAnalysisPageEntity() {
        return null;
    }

    protected abstract P createPresenter();

    protected void switchDiffFragmentContent(FragmentManager mFragmentManager, Fragment toFragment, Fragment mCurrentFragment, int resId, int index) {
        if (null == mCurrentFragment || null == toFragment) {
            return;
        }
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        if (!toFragment.isAdded()) {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.add(resId, toFragment, String.valueOf(index));
            fragmentTrasaction.commitAllowingStateLoss();
        } else {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.show(toFragment);
            fragmentTrasaction.commitAllowingStateLoss();
        }
    }


    //--------------------------- -------------------------------------- 处理隐藏键盘相关 -------------------------------------- -

    /**
     * 点击空白区域收起软键盘因为这个功能比较特殊 有些界面可能有冲突 如果哪些界面不需要这个功能 复写这个方法return false 即可
     *
     * @return
     */
    protected boolean isAutoUseHideSoftInput() {
        return true;
    }

    //记录第一点击的位置
    private float lastX, lastY;

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (isAutoUseHideSoftInput()) {
            switch (ev.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    lastX = ev.getX();
                    lastY = ev.getY();
                    //全局点击其他位置 隐藏软键盘 光标隐藏 点击的时候要重新显示光标
                    View currentFocusView = getCurrentFocus();
                    if (currentFocusView instanceof EditText && !(currentFocusView instanceof PasswordInputView)) {
                        //软键盘隐藏 需要隐藏光标
                        EditText et = (EditText) currentFocusView;
                        et.setCursorVisible(true);
                    }
                    break;


                case MotionEvent.ACTION_UP:
                    if (Math.abs(ev.getX() - lastX) < mTouchSlop && Math.abs(ev.getY() - lastY) < mTouchSlop) {
                        // 获得当前得到焦点的View，一般情况下就是EditText（特殊情况就是轨迹求或者实体案件会移动焦点）
                        View v = getCurrentFocus();
                        LogUtils.i(TAG, "V:" + v);
                        //重设密码场景不能隐藏软键盘
                        if (isShouldHideInput(v, ev) && !(v instanceof PasswordInputView)) {
                            if (v instanceof EditText) {
                                //软键盘隐藏 需要隐藏光标
                                EditText et = (EditText) v;
                                et.clearFocus();
                                et.setCursorVisible(false);
                            }
                            hideSoftInput(v.getWindowToken());
                        }
                    }
                    break;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时没必要隐藏
     */
    private boolean isShouldHideInput(View v, MotionEvent event) {
        if (v instanceof EditText) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0], top = l[1];
            int bottom = top + v.getMeasuredHeight(), right = left + v.getMeasuredWidth();
            return !(event.getX() > left) || !(event.getX() < right) || !(event.getY() > top) || !(event.getY() < bottom);
        }
        // 如果焦点不是EditText则忽略，这个发生在视图刚绘制完，第一个焦点不在EditView上，和用户用轨迹球选择其他的焦点
        return false;
    }

    /**
     * 多种隐藏软件盘方法的其中一种
     */
    private void hideSoftInput(IBinder token) {
        if (token != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(token, InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


}
