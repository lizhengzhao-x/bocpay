package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 分享
 */
public class TransferShareUiInfoProtocol extends BaseProtocol {

    public TransferShareUiInfoProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/getTransferShareUiInfo";
    }

}
