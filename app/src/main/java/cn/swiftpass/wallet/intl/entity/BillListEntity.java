package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 11:08
 * 账单查询返回账单List对象
 */

public class BillListEntity extends BaseEntity {
    ArrayList<OrderQueryEntity> orderList;


    public boolean useNewUI() {
        if (TextUtils.isEmpty(uiuxFunction)) return false;
        return TextUtils.equals(uiuxFunction, "1");
    }

    public String getUiuxFunction() {
        return uiuxFunction;
    }

    public void setUiuxFunction(String uiuxFunction) {
        this.uiuxFunction = uiuxFunction;
    }

    private String uiuxFunction;
    //uiuxFunction  0：关闭uiux优化  1：打开uiux优化

    public void setOrderList(ArrayList<OrderQueryEntity> orderList) {
        this.orderList = orderList;
    }

    public ArrayList<OrderQueryEntity> getOrderList() {
        return orderList;
    }
}
