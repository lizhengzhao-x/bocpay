package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountTopUpActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferSelTypeContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferSelTypePresenter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.CustomImageButton;


/**
 * Created by ZhangXinchao on 2019/10/30.
 * 转账优化 选择转账方式界面
 * 1.派利是选择转账/一般转账界面共用
 * 2.派利是底部没有tab
 */
public class TransferRedPacketFragment extends BaseFragment<TransferSelTypeContract.Presenter> implements TransferSelTypeContract.View, GetTransferSelTypeListener {
    private static final String TAG = "TransferSelTypeActivity";
    @BindView(R.id.id_transfer_search_et)
    ClearEditText idTransferSearchEt;

    @BindView(R.id.id_transfer_search_view)
    RelativeLayout idTransferSearchView;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.tab_transfer_redpackage)
    CustomImageButton tabTransferRedpackage;
    @BindView(R.id.tab_transfer_payment)
    CustomImageButton tabTransferPayment;
    @BindView(R.id.tab_transfer_fpsid)
    CustomImageButton tabTransferFpsid;
    @BindView(R.id.tab_transfer_bank)
    CustomImageButton tabTransferBank;
    @BindView(R.id.id_buttom_view)
    LinearLayout idButtomView;
    @BindView(R.id.id_buttom_tab)
    CardView idButtomTab;
    @BindView(R.id.tv_transfer_way_title)
    TextView tvTransferWayTitle;
    @BindView(R.id.rly_transfer_type_bocpay)
    RelativeLayout rlyTransferTypeBocpay;
    @BindView(R.id.coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;
    @BindView(R.id.apl_appBarLayout)
    AppBarLayout aplAppBarLayout;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.rly_transfer_eye)
    RelativeLayout rlyTransferEye;
    @BindView(R.id.tv_amount)
    TextView tvAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.lly_transfer_content)
    LinearLayout lly_transfer_content;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;

    private ArrayList<Fragment> mFragments;
    private List<String> tabNames;
    /**
     * 当前选择转账方式是否是派利是
     */
    private boolean isLiShi = true;
    private RecentlyContactFragment recentlyContactFragment;
    private FrequentContactFragment frequentContactFragment;
    private AddressBookFragment addressBookFragment;


    private boolean isAllowClickEvent = true;
    MySmartAccountEntity mSmartAccountInfo;
    private int dialogPull = 0;
    //需不要隐藏眼睛
    private boolean isHideEye = true;

    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(getString(R.string.RP1_2_6));
            idButtomTab.setVisibility(View.GONE);
            idButtomView.setBackgroundResource(R.drawable.bg_red_pocket);
        }
        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));
    }

    @Override
    protected void initView(View v) {
        EventBus.getDefault().register(this);
        recentlyContactFragment = RecentlyContactFragment.newInstance();
        frequentContactFragment = FrequentContactFragment.newInstance();

        addressBookFragment = AddressBookFragment.newInstance();
        mFragments = new ArrayList<>();
        mFragments.add(recentlyContactFragment);
        mFragments.add(frequentContactFragment);
        mFragments.add(addressBookFragment);
        bindData();

        idTransferSearchEt.setCursorVisible(false);
        idTransferSearchEt.setFocusable(false);
        idTransferSearchEt.setFocusableInTouchMode(false);
        idTransferSearchEt.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                List<ContractListEntity.RecentlyBean> recentlyBeans = mPresenter.dealWithAllList(recentlyContactFragment.getItemCommonCollectionList(), frequentContactFragment.getCollections(), addressBookFragment.getItemCommonCollectionList());
                TransferItemSearchActivity.setRecentlyInfo(recentlyBeans);
                HashMap<String, Object> mHashMapTransferInfo = new HashMap<>();
                //  mHashMapTransferInfo.put(Constants.TRANSFER_ITEM_LIST, recentlyBeans);
                mHashMapTransferInfo.put(Constants.TRANSFER_IS_LISHI, true);
                ActivitySkipUtil.startAnotherActivity(mActivity, TransferItemSearchActivity.class, mHashMapTransferInfo, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });


        lly_transfer_content.setVisibility(View.GONE);

        initSmartFresh();
        getSmartAccountInfo();
        coordinatorLayout.setBackgroundResource(R.drawable.bg_red_pocket);

        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });


        recentlyContactFragment.setOnContactListRefresh(new RecentlyContactFragment.ContactListRefresh() {
            @Override
            public void onSuccess() {
                freshEnd();
            }

            @Override
            public void onFail() {
                freshEnd();
            }
        });

        cbToggleSee.setChecked(false);
        hideAccount();
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }

    private void initSmartFresh() {
        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
//                getSmartAccountInfo();
//                dialogPull++;
//                //刷新最近转账列表
//                if (recentlyContactFragment != null) {
//                    recentlyContactFragment.getContractList();
//                    dialogPull++;
//                }
                getSmartAccountInfo();
                dialogPull++;

                if (null == viewPager) {
                    return;
                }

                //刷新最近转账列表
                if (0 == viewPager.getCurrentItem()) {
                    if (recentlyContactFragment != null) {
                        recentlyContactFragment.getContractList();
                        dialogPull++;
                    }
                }

                //刷新常用联络人
                if (1 == viewPager.getCurrentItem()) {
                    EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_LOCAL_CACHE_CONTACT, ""));
                }

                //刷新通讯录
                if (2 == viewPager.getCurrentItem()) {
                    EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_ADDRESS_COLLECTION_STATUS, ""));
                }
            }
        });
    }

    private void freshEnd() {
        dialogPull--;
        if (dialogPull <= 0) {
            dialogPull = 0;
        }
        //弹框也要关闭
        dismissDialog();
        mainRefreshLayout.finishRefresh();
    }

    /**
     * 获取我的账户信息
     */
    private void getSmartAccountInfo() {
        mPresenter.getSmartAccountInfo();
    }

    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }

        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();
        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);

            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }

    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @OnClick({R.id.tv_transfer_top_up, R.id.tv_balance_title,
            R.id.tv_amount,
            R.id.tv_balance_tody_title,
            R.id.tv_amount_usable, R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(),300)) return;
        switch (view.getId()) {
            case R.id.tv_transfer_top_up:
                onClickTopUp();
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_REDPACKET_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    String str = new Gson().toJson(mSmartAccountInfo);
                    try {
                        SmartAccountAdjustDailyLimitActivity.startActivity(mActivity, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                    } catch (Exception e) {

                    }
                }
                break;
        }
    }

    //充值
    private void onClickTopUp() {
        if (mSmartAccountInfo == null) return;
        if (TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel())) return;
        if (!TextUtils.isEmpty(mSmartAccountInfo.getRelevanceAccNo()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
            mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
            mHashMaps.put(Constants.ACCTYPE, mSmartAccountInfo.getAccType());
            mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            topUpAccountEvent();
        }

    }

    private void topUpAccountEvent() {
        List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
        if (bankCardsBeans != null && bankCardsBeans.size() > 0 && !TextUtils.isEmpty(bankCardsBeans.get(0).getStatus())) {
            //Edda状态，N：没有登记过，A：登记成功 R：登记失败 1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
            // E:删除失败 C:删除成功 4:删除申请失败 9删除申请处理中 3提交删除申请成功
            if (bankCardsBeans.get(0).getEddaStatus().equals("A") || bankCardsBeans.get(0).getEddaStatus().equals("E") || bankCardsBeans.get(0).getEddaStatus().equals("3")) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
                mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
                mHashMaps.put(Constants.ACCTYPE, bankCardsBeans.get(0).getBankName());
                mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
                ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("N")) {
                onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("R")) {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("0") || bankCardsBeans.get(0).getEddaStatus().equals("1")) {
                showErrorMsgDialog(getActivity(), getString(R.string.string_edda_loading));
            } else {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            }
        } else {
            onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
        }

    }

    /**
     * s2用户绑定银行卡
     */
    private void onWainningTopUpWithBindAccount(String message) {
        String str_title = getString(R.string.P3_D1_1_1);
        String str_msg = message;
        String str_pos = getString(R.string.P3_D1_1_3);
        String str_neg = getString(R.string.string_cancel);
        AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_pos, str_neg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getRegEddaInfo(mSmartAccountInfo);
            }
        });
    }

    private void getRegEddaInfo(final MySmartAccountEntity mSmartAccountInfo) {
        mPresenter.getRegEddaInfo();
    }

    private void bindData() {
        tabNames = new ArrayList<>();
        tabNames.add(getString(R.string.TR1_2_2));
        tabNames.add(getString(R.string.TR1_2_3));
        tabNames.add(getString(R.string.TR1_2_4));

        tabLayout.addTab(tabLayout.newTab().setText(tabNames.get(0)));
        tabLayout.addTab(tabLayout.newTab().setText(tabNames.get(1)));
        tabLayout.addTab(tabLayout.newTab().setText(tabNames.get(2)));


        viewPager.setOffscreenPageLimit(mFragments.size());
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabSelected: ");
                viewPager.setCurrentItem(tabLayout.getSelectedTabPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabUnselected: ");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                LogUtils.i(TAG, "onTabReselected: ");
            }
        });

        viewPager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return tabNames.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setText(tabNames.get(0));
        tabLayout.getTabAt(1).setText(tabNames.get(1));
        tabLayout.getTabAt(2).setText(tabNames.get(2));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.UPDATE_TRANSFER_LIST) {
            //更改昵称 需要刷新最近转账列表
            if (recentlyContactFragment != null) {
                recentlyContactFragment.getContractList();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        //搜索界面 如果收藏/取消收藏联系人 需要刷新 AddressBookFrament
        if (mFmgHandler != null && recentlyContactFragment != null) {
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (recentlyContactFragment != null) {
                        frequentContactFragment.updateRecycleView(true);
                        addressBookFragment.updateItemList();
                    }
                }
            }, 200);
            if (null != cbToggleSee) {
                if (isHideEye) {
                    cbToggleSee.setChecked(false);
                } else {
                    isHideEye = true;
                }

            }

        }

    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        mSmartAccountInfo = response;
        CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);

        if (null != cbToggleSee && cbToggleSee.isChecked()) {
            openAccount();
        } else {
            hideAccount();
        }

        if (isEyeDateNull) {
            cbToggleSee.setChecked(true);
            isEyeDateNull = false;
        }
        freshEnd();
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_red_packet_type;
    }




    /**
     * Activity 中 Fragment 需要知道当前操作跳转是到转账界面 还是 派利是界面
     *
     * @return
     */
    @Override
    public boolean isNormalTransferType() {
        return !isLiShi;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected TransferSelTypeContract.Presenter loadPresenter() {
        return new TransferSelTypePresenter();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    /**
     * 很多 例如绑卡/解绑完成之后要退到首页 刷新卡列表等
     *
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }
        }

    }


}
