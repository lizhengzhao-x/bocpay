package cn.swiftpass.wallet.intl.module.scan.backscan;

import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;


public class ShowQrcodeActivity extends BaseCompatActivity {
    @BindView(R.id.id_id_qr_code_str)
    TextView idIdQrCodeStr;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        String qrcode = getIntent().getExtras().getString(Constants.DATA_QRCODE_STR);
        idIdQrCodeStr.setText(qrcode);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_other_qrcode;
    }


}
