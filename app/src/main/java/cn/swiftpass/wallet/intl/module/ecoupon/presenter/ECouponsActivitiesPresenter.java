package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetECouponActivitiesListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.ECouponsActivitiesContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivitityListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;

/**
 * 电子券活动列表
 */
public class ECouponsActivitiesPresenter extends BasePresenter<ECouponsActivitiesContract.View> implements ECouponsActivitiesContract.Presenter {

    @Override
    public void getActivitiesList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetECouponActivitiesListProtocol(new NetWorkCallbackListener<EcouponActivitityListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActivitiesListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(EcouponActivitityListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActivitiesListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getUplanUrl() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetUplanurlProtocol(new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUplanUrlError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUplanUrlSuccess(response);
                }
            }
        }).execute();
    }
}
