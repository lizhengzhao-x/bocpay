package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 充值绑定银行卡
 */
public class RegEddaInfoProtocol extends BaseProtocol {

    String mBankCode;
    String mBankNo;
    String mCurrrency;

    public RegEddaInfoProtocol(String bankCode, String bankNo, String currency, NetWorkCallbackListener dataCallback) {
        this.mBankCode = bankCode;
        this.mBankNo = bankNo;
        this.mCurrrency = currency;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/regEddaInfo";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.BANKCODE, mBankCode);
        mBodyParams.put(RequestParams.BANKNO, mBankNo);
        mBodyParams.put(RequestParams.CURRENCY, mCurrrency);
    }

}
