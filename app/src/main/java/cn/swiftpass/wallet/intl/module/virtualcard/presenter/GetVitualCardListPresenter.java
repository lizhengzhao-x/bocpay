package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckVirCardProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.GetVitualCardListContract;

/**
 * Created by ZhangXinchao on 2019/11/15.
 */
public class GetVitualCardListPresenter extends BasePresenter<GetVitualCardListContract.View> implements GetVitualCardListContract.Presenter {
//    @Override
//    public void getVitualCardList() {
//
//
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
//        new GetVitualCarditListProtocol(new NetWorkCallbackListener<VitualCardListEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView() != null) {
//                    getView().dismissDialog();
//                    getView().getVitualCardListError(errorCode, errorMsg);
//                }
//            }
//
//            @Override
//            public void onSuccess(VitualCardListEntity response) {
//                if (getView() != null) {
//                    getView().dismissDialog();
//                     getView().getVitualCardListSuccess(response);
//                }
//            }
//        }).execute();
//    }

    @Override
    public void vitualCardCheck(String pan, String expiryDate, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckVirCardProtocol(pan, expiryDate, action, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardCheckError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardCheckSuccess(response);
                }
            }
        }).execute();
    }
}
