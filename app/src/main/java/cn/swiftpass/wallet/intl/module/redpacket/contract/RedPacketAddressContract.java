package cn.swiftpass.wallet.intl.module.redpacket.contract;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;

/**
 * 红包选择
 */
public class RedPacketAddressContract {

    public interface View extends IView {

        void checkSmartAccountNewBindSuccess(ContentEntity response, final String phone, final String name, final String relName, final boolean isEmail);

        void checkSmartAccountNewBindError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<RedPacketAddressContract.View> {

        void checkSmartAccountNewBind(final String phone, final String name, final String relName, final boolean isEmail);


        /**
         * 数据处理
         *
         * @param frequentCcontacts
         */
        List<ContractListEntity.RecentlyBean> dealWithAllList(List<ContactEntity> frequentCcontacts);


    }

}
