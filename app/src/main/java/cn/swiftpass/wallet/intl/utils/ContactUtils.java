package cn.swiftpass.wallet.intl.utils;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.SparseArray;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.entity.ContactEntity;

/**
 * 本地通讯录的读取 https://blog.csdn.net/zartzwj/article/details/8169548
 */
public class ContactUtils {


    private static final String ATOZSTR = "[A-Z]";
    private static final String SPECIAL_CHAR_ONE = "#";
    private static final String SPECIAL_CHAR_TWO = "@";
    /**
     * 要实现读取出来的列表顺序是a-z排序
     */
    private CharacterParser characterParser;

    public ContactUtils() {
        characterParser = new CharacterParser();
    }


    public static ContactUtils getInstance() {
        return ContactUtils.ContactUtilsolder.instance;
    }

    private static class ContactUtilsolder {
        private static ContactUtils instance = new ContactUtils();
    }

    public ArrayList<ContactEntity> getContacts() {
        return contacts;
    }

    /**
     * 作为缓存来处理
     */
    private ArrayList<ContactEntity> contacts;
    private static final String TAG = "ContactUtils";


    /**
     * @param context
     * @param isRefresh true 需要从本地重新读取
     * @return
     */
    public ArrayList<ContactEntity> getAllContacts(Context context, boolean isRefresh) {
        if (isRefresh) {
            return getAllContacts(context);
        } else {
            return contacts;
        }
    }


    /**
     * 外部不需要维护线程 内部已经处理
     *
     * @param context
     * @param isRefresh
     * @param onReadContactsSuccessCallBack
     */
    public void getAllContactsAsyn(Context context, boolean isRefresh, OnReadContactsSuccessCallBack onReadContactsSuccessCallBack) {
        if (isRefresh) {
            new ReadContactsTask(context, onReadContactsSuccessCallBack).execute();
        } else {
            if (onReadContactsSuccessCallBack != null) {
                onReadContactsSuccessCallBack.OnReadContactsSuccess(contacts);
            }
        }
    }

    public ArrayList<ContactEntity> getInviteContact(Context context, boolean isRefresh) {
        if (isRefresh) {
            return getAllContact(context, false);
        } else {
            return contacts;
        }
    }

    private synchronized ArrayList<ContactEntity> getAllContacts(Context context) {
        return getAllContact(context, true);
    }

    private synchronized ArrayList<ContactEntity> getAllContact(Context context, boolean needEmail) {

        Set<String> filterPhoneUserNames = new HashSet<String>();
        ArrayList<ContactEntity> result = null;
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = null;
        try {
            Uri contactUri = ContactsContract.RawContactsEntity.CONTENT_URI;

            String[] fields = new String[]{ContactsContract.RawContactsEntity.CONTACT_ID,
                    ContactsContract.RawContactsEntity.MIMETYPE,
                    ContactsContract.RawContactsEntity.DATA1};

            String selection = ContactsContract.RawContactsEntity.DELETED + "=? AND " + ContactsContract.RawContactsEntity.MIMETYPE + " IN (?,?,?)";


            //只要名字，邮箱，手机号，其他数据不要
            String[] selectionArgs = null;
            //判断是否需要邮箱  默认需要
            if (needEmail) {
                selectionArgs = new String[]{"0",
                        //0表示数据没有被删除
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE,};
            } else {
                selectionArgs = new String[]{"0",
                        //0表示数据没有被删除
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE,
                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
                };
            }
            /**
             * 1.在Android中怎么区分各个Provider？有提供联系人的，有提供图片的等等。所以就需要有一个唯一的标识来标识这个Provider，Uri就是这个标识
             * 2.查询要返回的列（Column）
             * 3.查询where字句
             * 4.查询条件属性值
             * 5.结果排序规则
             */
            cursor = cr.query(contactUri, fields, selection, selectionArgs, null);

            int itemCount = 0;
            if (cursor != null) {
                SparseArray<List<ContactEntity>> mapEntity = new SparseArray<>(cursor.getCount());
                SparseArray<String> mapName = new SparseArray<>(cursor.getCount() / 2);
                int contactIDIndex = cursor.getColumnIndex(fields[0]);
                int typeIndex = cursor.getColumnIndex(fields[1]);
                int dataIndex = cursor.getColumnIndex(fields[2]);
                while (cursor.moveToNext()) {
                    int contactID = cursor.getInt(contactIDIndex);
                    String type = cursor.getString(typeIndex);
                    //三星部分手机手机号中间格式化有空格 要去掉
                    String data = cursor.getString(dataIndex);
                    List<ContactEntity> entityList = mapEntity.get(contactID);
                    if (entityList == null) {
                        entityList = new LinkedList<>();
                        //contactID是递增的，所以用append增加性能
                        mapEntity.append(contactID, entityList);
                    }
                    ContactEntity entity = null;
                    switch (type) {
                        //data是手机号
                        case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                            data = data.replace(" ", "");
                            entity = new ContactEntity();
                            entity.setNumber(data);
                            entity.setContactID(contactID + "_" + entity.getNumber());
                            break;
                        //data是邮箱
                        case ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE:
                            entity = new ContactEntity();
                            entity.setNumber(data);
                            entity.setEmail(true);
                            entity.setContactID(contactID + "_" + data);
                            break;
                        //data是名字
                        case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                            mapName.append(contactID, data);
                            break;
                    }
                    if (entity != null) {
                        entityList.add(entity);
                        itemCount++;
                    }
                }
                cursor.close();//提前close释放内存，不用等到finally
                cursor = null;
                result = new ArrayList<>(itemCount);
                for (int i = 0, size = mapEntity.size(); i < size; i++) {
                    List<ContactEntity> contactList = mapEntity.valueAt(i);
                    for (ContactEntity entity : contactList) {
                        addSortLetter(result, mapEntity, mapName, i, entity);
                    }
                }
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        ArrayList<ContactEntity> filterContacts = removalDuplicateAndSort(filterPhoneUserNames, result);
        return filterContacts;
    }

    private void addSortLetter(ArrayList<ContactEntity> result, SparseArray<List<ContactEntity>> mapEntity, SparseArray<String> mapName, int i, ContactEntity entity) {
        String userName = mapName.get(mapEntity.keyAt(i));
        // 汉字转换成拼音
        if (!TextUtils.isEmpty(userName)) {
//            String pinyin = converterToFirstSpell(userName);
            String pinyin = Pinyin.toPinyin(userName, "");
//            LogUtils.i(TAG, "userName：--->" + userName + " pinyin:" + pinyin);
            if (!TextUtils.isEmpty(pinyin)) {
                String sortString = pinyin.substring(0, 1).toUpperCase();
                // 正则表达式，判断首字母是否是英文字母
                if (sortString.matches(ATOZSTR)) {
                    entity.setSortLetter(sortString.toUpperCase());
                } else {
                    entity.setSortLetter(SPECIAL_CHAR_ONE);
                }
            } else {
                entity.setSortLetter(SPECIAL_CHAR_ONE);
            }
        } else {
            entity.setSortLetter(SPECIAL_CHAR_ONE);
        }
        entity.setUserName(userName);
        result.add(entity);
    }

    private ArrayList<ContactEntity> removalDuplicateAndSort(Set<String> filterPhoneUserNames, ArrayList<ContactEntity> result) {
        ArrayList<ContactEntity> filterContacts = new ArrayList<>();
        //重复通讯录去重
        if (result != null) {
            for (Iterator iter = result.iterator(); iter.hasNext(); ) {
                ContactEntity contractItem = (ContactEntity) iter.next();
                //用户名手机号拼接 来判断去重
                String filterItem = "";
                if (!TextUtils.isEmpty(contractItem.getUserName())) {
                    filterItem += contractItem.getUserName();
                }
                if (!TextUtils.isEmpty(contractItem.getNumber())) {
                    filterItem += contractItem.getNumber();
                }
//                LogUtils.i(TAG, "filterItem：--->" + filterItem);
                if (filterPhoneUserNames.add(filterItem))
                    filterContacts.add(contractItem);
            }
        }
        //a-z排序
        Collections.sort(filterContacts, new PinyinComparator());
        this.contacts = filterContacts;
        return filterContacts;
    }


    public class PinyinComparator implements Comparator<ContactEntity> {

        public int compare(ContactEntity o1, ContactEntity o2) {
            if (o1.getSortLetter().equals(SPECIAL_CHAR_TWO) || o2.getSortLetter().equals(SPECIAL_CHAR_ONE)) {
                return -1;
            } else if (o1.getSortLetter().equals(SPECIAL_CHAR_ONE) || o2.getSortLetter().equals(SPECIAL_CHAR_TWO)) {
                return 1;
            } else {
                return o1.getSortLetter().compareTo(o2.getSortLetter());
            }
        }

    }

    /**
     * 耗时任务读取通讯录
     */
    private class ReadContactsTask extends AsyncTask<Void, Void, ArrayList<ContactEntity>> {
        private Context context;
        private OnReadContactsSuccessCallBack onReadContactsSuccessCallBack;

        public ReadContactsTask(Context contextIn, OnReadContactsSuccessCallBack onReadContactsSuccessCallBackIn) {
            this.context = contextIn;
            this.onReadContactsSuccessCallBack = onReadContactsSuccessCallBackIn;
        }

        @Override
        protected ArrayList<ContactEntity> doInBackground(Void... voids) {
            LogUtils.i(TAG, "doInBackground->");
            return getAllContact(context, true);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtils.i(TAG, "onPreExecute->");
        }

        @Override
        protected void onPostExecute(ArrayList<ContactEntity> contactEntities) {
            super.onPostExecute(contactEntities);
            LogUtils.i(TAG, "onPostExecute->");
            if (onReadContactsSuccessCallBack != null) {
                onReadContactsSuccessCallBack.OnReadContactsSuccess(contactEntities);
            }
        }
    }

    public interface OnReadContactsSuccessCallBack {
        void OnReadContactsSuccess(ArrayList<ContactEntity> contactEntities);
    }
}
