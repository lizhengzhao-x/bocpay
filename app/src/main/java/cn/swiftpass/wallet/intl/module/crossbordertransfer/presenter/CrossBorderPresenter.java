package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import android.content.Context;
import android.text.TextUtils;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferByTnxIdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferByTnxIdWithSmartProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.CrossBorderRemittanceContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemType;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

import static cn.swiftpass.wallet.intl.entity.Constants.ACCOUNT_TYPE_SMART;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_FAILED;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_PROCESS;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_SUCCESS;

public class CrossBorderPresenter extends BasePresenter<CrossBorderRemittanceContract.View> implements CrossBorderRemittanceContract.Presenter {


    private Context mContext;

    public CrossBorderPresenter(Context mContextIn) {
        super();
        this.mContext = mContextIn;
    }

    @Override
    public void getUIRecordList(String data, String type) {
        String tnxId = "";
        String outtrfrefno = "";
        if (ACCOUNT_TYPE_SMART.equals(type)) {
            outtrfrefno = data;
        } else {
            tnxId = data;
        }
        new GetTransferByTnxIdProtocol(tnxId, outtrfrefno, new NetWorkCallbackListener<TransferCrossBorderStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUIRecordListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderStatusEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    if (getView() != null) {
                        getView().getUIRecordListSuccess(response, dealWithData(response));
                    }
                }
            }
        }).execute();
    }

    /**
     * 智能账户特殊类型
     *
     * @param outtrfrefno
     * @param txnType
     */
    @Override
    public void getUIRecordListWithSmart(String outtrfrefno, String txnType) {
        new GetTransferByTnxIdWithSmartProtocol(outtrfrefno, txnType, new NetWorkCallbackListener<TransferCrossBorderStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUIRecordListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderStatusEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    if (getView() != null) {
                        getView().getUIRecordListSuccess(response, dealWithData(response));
                    }
                }
            }
        }).execute();
    }

    private ArrayList<ItemModel> dealWithData(TransferCrossBorderStatusEntity response ) {

        ArrayList<ItemModel> itemModels = new ArrayList<>();
        //4成功 3 失败 2 处理中
        String tnxStatus = response.status;
        if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_SUCCESS)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(response.receiverName).itemImage( R.mipmap.icon_transactions_crossborder_remittance ).build());
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_FAILED)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(response.receiverName).itemImage( R.mipmap.icon_transactions_crossborder_remittance ).build());
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_PROCESS)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.HEAD).itemTitle(response.receiverName).itemImage( R.mipmap.icon_transactions_crossborder_remittance ).build());
        } else {

        }


        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_8)).itemContent(mContext.getString(R.string.CT2_1_8a)+" " + AndroidUtils.formatPriceWithPoint(Double.valueOf(response.amountCNY), false)).build());


        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_3a_10)).itemContent(response.forUsed).itemVisibleLine(true).build());

        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_9)).itemContent(mContext.getString(R.string.CT2_1_9a) + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(response.amountHKD), false)).build());
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_5_7)).itemContent(mContext.getString(R.string.CT2_1_9a) + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(response.fee), false)).build());
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_4_8)).itemContent(mContext.getString(R.string.CT2_1_9a) + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(response.totalAmount), false)).itemVisibleLine(true).build());
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_4_2)).itemContent(response.receiverName).build());
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_4_3)).itemContent(response.receiverCardNo).build());

        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_4_4)).itemContent(response.receiverBankName + "-" + AndroidUtils.getCardType(response.receiverCardType, mContext)).build());

        String sb = ("1".equals(response.accountType) ? mContext.getString(R.string.LYP01_06_6) : mContext.getString(R.string.LYP01_06_4)) + "(" + response.panFour + ")";
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_3a_20)).itemContent(sb).itemVisibleLine(true).build());

        if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_SUCCESS)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_1)).itemContent(mContext.getString(R.string.CT2_1_5)).build());
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_FAILED)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_1)).itemContent(mContext.getString(R.string.CT2_1_6)).build());
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_PROCESS)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_1)).itemContent(mContext.getString(R.string.CT2_1_7)).build());
        } else {
        }

        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_2)).itemContent(response.txnOptionStr).build());


        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_3)).itemContent(response.txTime).build());
        itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT2_1_4)).itemContent(response.referenceNo).build());

        //失败类型
        if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_SUCCESS)) {
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_FAILED)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(mContext.getString(R.string.CT1_7_2)).itemContent(response.rejectReason).build());
        } else if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_PROCESS)) {
        } else {
        }

//        if (TextUtils.equals(tnxStatus, CROSSBORDERTRANSFER_SUCCESS)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.TAIL).build());
//        }
        return itemModels;
    }
}
