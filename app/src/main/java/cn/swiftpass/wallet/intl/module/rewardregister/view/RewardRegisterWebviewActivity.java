package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.event.RewardRegisterEventEntity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetPansForRewardProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardPansListEntity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.CircleWebview;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CAMPAIGNID;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CARDLIST;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.IMAGEURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.TERMINFOURL;

/**
 * 信用卡奖赏登记Web页面
 */
public class RewardRegisterWebviewActivity extends BaseWebViewActivity {
    @BindView(R.id.webView)
    CircleWebview webView;
    @BindView(R.id.id_apply_now)
    TextView idApplyNow;
    @BindView(R.id.id_bottom_view)
    LinearLayout idBottomView;
    private String url;
    private String banckCardType;
    public static final String URL_REWARD = "URL_REWARD";
    public static final String BANKCARDTYPE = "BANKCARDTYPE";
    public static final String REMARK = "REMARK";

    private Intent rewardRegisterIntent = null;


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        rewardRegisterIntent = intent;
        if (intent == null || intent.getExtras() == null) {
            finish();
            return;
        }
        url = intent.getExtras().getString(URL_REWARD);
        banckCardType = intent.getExtras().getString(BANKCARDTYPE);
        initWebView(webView, url);
    }

    @Override
    public void init() {
        super.init();
        setToolBarTitle(R.string.CR209_11_1);
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        rewardRegisterIntent = getIntent();
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        EventBus.getDefault().register(this);
        url = rewardRegisterIntent.getExtras().getString(URL_REWARD);
        banckCardType = rewardRegisterIntent.getExtras().getString(BANKCARDTYPE);
        setWebViewClient(webView, true, true);
        initWebView(webView, url);
        idApplyNow.setText(getString(R.string.CR209_3_2));
        idApplyNow.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    verifyPwd();
                } else {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(LoginActivity.TYPE_BACK_IN_LOGIN, true);
                    ActivitySkipUtil.startAnotherActivity(RewardRegisterWebviewActivity.this, LoginActivity.class, map, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }


    private void verifyPwd() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                getCardList();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    private void getCardList() {
        showDialogNotCancel();
        new GetPansForRewardProtocol(banckCardType, new NetWorkCallbackListener<RewardPansListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                showErrorMsgDialog(RewardRegisterWebviewActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(RewardPansListEntity responseIn) {
                dismissDialog();
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(CAMPAIGNID, rewardRegisterIntent.getExtras().getString(CAMPAIGNID));
                maps.put(IMAGEURL, rewardRegisterIntent.getExtras().getString(IMAGEURL));
                maps.put(TERMINFOURL, rewardRegisterIntent.getExtras().getString(TERMINFOURL));
                maps.put(CARDLIST, responseIn);
                maps.put(BANKCARDTYPE, banckCardType);
                maps.put(REMARK, rewardRegisterIntent.getExtras().getString(REMARK));
                ActivitySkipUtil.startAnotherActivity(RewardRegisterWebviewActivity.this, RewardRegisterDetailActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }).execute();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RewardRegisterEventEntity event) {
        if (event.getEventType() == RewardRegisterEventEntity.EVENT_BACK_REWARD_LIST) {
            finish();
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_reward_webview;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            finish();
        }
    }

}
