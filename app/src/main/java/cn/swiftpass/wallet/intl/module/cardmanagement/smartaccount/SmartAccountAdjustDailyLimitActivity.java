package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.AmountSetDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView;


public class SmartAccountAdjustDailyLimitActivity extends BaseCompatActivity {


    @BindView(R.id.id_edit_daily_amount)
    ImageArrowNoPaddingView mDailyAmountView;
    @BindView(R.id.id_seekbar)
    SeekBar mSeekbar;
    @BindView(R.id.id_money_min)
    TextView mMoneyMinTV;
    @BindView(R.id.id_money_max)
    TextView mMoneyMaxTV;
    @BindView(R.id.id_tip_limit)
    TextView mTipLimit;
    @BindView(R.id.tv_submit)
    TextView mSubmitTv;
    @BindView(R.id.iav_topup_setting)
    ImageArrowNoPaddingView iavTopupSetting;
    @BindView(R.id.id_right_label)
    TextView idRightLabel;
    private MySmartAccountEntity smartAccountInfo;
    private int currentLimit;
    private int mOldLimit;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.CR2101_16_1);
        EventBus.getDefault().register(this);
        smartAccountInfo = (MySmartAccountEntity) getIntent().getExtras().getSerializable(Constants.SMART_ACCOUNT);
        iavTopupSetting.setRightText(smartAccountInfo.getCurrency() + smartAccountInfo.getMaxPay());
        mMoneyMinTV.setText(smartAccountInfo.getCurrency() + " " + smartAccountInfo.getMinPay());
        mMoneyMaxTV.setText(smartAccountInfo.getCurrency() + " " + AndroidUtils.formatPrice(Double.valueOf(smartAccountInfo.getMaxPay()), false));
        idRightLabel.setText(smartAccountInfo.getCurrency() + " " + AndroidUtils.formatPrice(Double.valueOf(smartAccountInfo.getMaxPay()), false));
        currentLimit = Double.valueOf(smartAccountInfo.getPayLimit()).intValue();
        mOldLimit = currentLimit;
        mDailyAmountView.setLeftTextTitle(smartAccountInfo.getCurrency() + " " + AndroidUtils.formatPrice(Double.valueOf(smartAccountInfo.getPayLimit()), false));
        String tip = getResources().getString(R.string.STF2101_1_8).replace("10,000",  AndroidUtils.formatPrice(Double.valueOf(smartAccountInfo.getMaxPay()), false));
        mTipLimit.setText(tip);
        mSeekbar.setMax(100);
        mSeekbar.setProgress((int) ((Double.valueOf(smartAccountInfo.getPayLimit()) * 100 / Double.valueOf(smartAccountInfo.getMaxPay()))));
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentLimit = (int) ((progress * 1.0 / 100.0) * Float.valueOf(smartAccountInfo.getMaxPay()));
                currentLimit = currentLimit / 100 * 100;
                mDailyAmountView.setLeftTextTitle(smartAccountInfo.getCurrency() + " " + AndroidUtils.formatPrice(Double.valueOf(currentLimit), false));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smartaccount_adjust_daily_limit;
    }


    @OnClick({R.id.id_edit_daily_amount, R.id.id_seekbar, R.id.id_money_min, R.id.id_money_max, R.id.tv_submit})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_edit_daily_amount:
                onClickAmountLimit();
                break;
            case R.id.tv_submit:
                if (ButtonUtils.isFastDoubleClick()) return;
                if (mOldLimit == currentLimit) {
                    AndroidUtils.showTipDialog(getActivity(), getString(R.string.unchange));
                    return;
                }
                if (currentLimit > Double.valueOf(smartAccountInfo.getPayLimit()).intValue()) {
                    //发送otp
                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                    mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_UPDATE_DAILY_LIMIT);
                    mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_LMT_SET);
                    mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
                    mHashMapsLogin.put(Constants.DATA_LIMIT, currentLimit + "");
                    ActivitySkipUtil.startAnotherActivity(SmartAccountAdjustDailyLimitActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    if (currentLimit == 0) {
                        showErrorMsgDialog(mContext, getString(R.string.title_string_daily_litmit_error));
                        return;
                    }
                    //密码校验 直接更新
                    initPasswordDialog();
                }
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    private void initPasswordDialog() {
        VerifyPasswordCommonActivity.startActivityForResult(SmartAccountAdjustDailyLimitActivity.this, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                updateDailyLimitInfo();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

    }


    private void onClickAmountLimit() {
        AmountSetDialog.Builder builder = new AmountSetDialog.Builder(this);
        AmountSetDialog dialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                int limitAmount = ((AmountSetDialog) dlg).getmAmount();
                if (limitAmount == -1) return;
                mSeekbar.setProgress((int) ((limitAmount / Float.valueOf(smartAccountInfo.getMaxPay())) * 100));
                mDailyAmountView.setLeftTextTitle(smartAccountInfo.getCurrency() + " " + AndroidUtils.formatPrice(Double.valueOf(limitAmount), false));
                currentLimit = limitAmount;
            }
        }).create(-1, Integer.valueOf(smartAccountInfo.getMaxPay()));
        dialog.show();
    }

    public static void startActivity(Activity activity, MySmartAccountEntity smartAccountInfo, String phoneNumber) {
        Intent intent = new Intent(activity, SmartAccountAdjustDailyLimitActivity.class);
        intent.putExtra(Constants.SMART_ACCOUNT, smartAccountInfo);
        intent.putExtra(Constants.DATA_PHONE_NUMBER, phoneNumber);
        activity.startActivity(intent);
    }


    private void updateDailyLimitInfo() {
        ApiProtocolImplManager.getInstance().updateSmartAccountDailyLimit(mContext, currentLimit + "", new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                //先修改内存中的限额金额，同时发eventbus触发接口请求，静默刷新智能账户数据
                MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                if (mySmartAccountEntity!=null){
                    mySmartAccountEntity.setPayLimit(currentLimit + "");
                }
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT, ""));
                EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                finish();
            }
        });
    }


}
