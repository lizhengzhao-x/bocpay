package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ProgressWebView;


public class ChangeDefaultAccountActivity extends BaseWebViewActivity {


    @BindView(R.id.webView)
    ProgressWebView webView;
    @BindView(R.id.tv_not_agree)
    TextView tvNotAgree;
    @BindView(R.id.tv_agree)
    TextView tvAgree;

    private String lan;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        super.init();
        lan = SpUtils.getInstance(mContext).getAppLanguage();
//        switchLanguage(lan);
        setToolBarTitle(R.string.primary_account);
        tvNotAgree.setText(R.string.string_cancel);
        tvAgree.setText(R.string.title_btn_agree);
//        WebSettings settings = webView.getSettings();
//        settings.setJavaScriptEnabled(true);
//        settings.setAllowFileAccessFromFileURLs(false);
//        settings.setAllowUniversalAccessFromFileURLs(false);

        String url = null;
        if (AndroidUtils.isZHLanguage(lan)) {
            url = AppTestManager.getInstance().getBaseUrl() + "eAccountTermsConditions_CN.html";
        } else if (AndroidUtils.isHKLanguage(lan)) {
            url = AppTestManager.getInstance().getBaseUrl() + "eAccountTermsConditions_TW.html";
        } else {
            url = AppTestManager.getInstance().getBaseUrl() + "eAccountTermsConditions_EN.html";
        }
        setWebViewClient(webView, false);
        initWebView(webView, url);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_changedefault;
    }


    @OnClick({R.id.tv_not_agree, R.id.tv_agree})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick()) return;
        switch (view.getId()) {
            case R.id.tv_not_agree:
                finish();
                break;
            case R.id.tv_agree:
                //发送otp
                HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_CHANGE_DEFAULT_ACCOUNT);
                mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_A);
                mHashMapsLogin.put(Constants.EXTRA_ACCOUNT_INFO, getIntent().getExtras().getSerializable(Constants.EXTRA_ACCOUNT_INFO));
                mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
                ActivitySkipUtil.startAnotherActivity(ChangeDefaultAccountActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
                break;
            default:
                break;
        }
    }
}
