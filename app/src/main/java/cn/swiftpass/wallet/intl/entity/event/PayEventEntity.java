package cn.swiftpass.wallet.intl.entity.event;


public class PayEventEntity extends BaseEventEntity{

    public static final int EVENT_PAY_SUCCESS = 1001;


    public PayEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public PayEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
