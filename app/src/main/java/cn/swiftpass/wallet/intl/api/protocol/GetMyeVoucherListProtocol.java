package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class GetMyeVoucherListProtocol extends BaseProtocol {
    private String bu;
    private String reload;

    public GetMyeVoucherListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/geteVoucherList";
    }

    public GetMyeVoucherListProtocol(String bu, String reloadIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/geteVoucherList";
        this.reload = reloadIn;
        this.bu = bu;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(bu)) {
            List<String> bus = new ArrayList<>();
            bus.add(bu);
            mBodyParams.put(RequestParams.BUSINESSUNITS, bus);
            if (!TextUtils.isEmpty(reload)) {
                mBodyParams.put(RequestParams.RELOAD, reload);
            }
        }
    }
}
