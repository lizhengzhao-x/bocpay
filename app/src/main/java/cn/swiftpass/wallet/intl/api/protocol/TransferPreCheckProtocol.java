package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;

/**
 * @author Created by ramon on 2018/8/20.
 * 转账试算接口
 */

public class TransferPreCheckProtocol extends BaseProtocol {
    //    private String transferType;
    TransferPreCheckReq mReq;
    String action;

    public TransferPreCheckProtocol(TransferPreCheckReq req, NetWorkCallbackListener callback) {
        this.mReq = req;
        mDataCallback = callback;
        mUrl = "api/transfer/transferPreCheck";
    }


    public TransferPreCheckProtocol(TransferPreCheckReq req,String action, NetWorkCallbackListener callback) {
        this.mReq = req;
        mDataCallback = callback;
        this.action = action;
        mUrl = "api/transfer/transferPreCheck";
    }


    @Override
    public void packData() {
        super.packData();
        if (null != mReq) {
            mBodyParams.put(RequestParams.TRANSFER_REGION, mReq.region);
            mBodyParams.put(RequestParams.TRANSFER_ACCOUNT, mReq.tansferToAccount);
            mBodyParams.put(RequestParams.TRANSFER_AMOUNT, mReq.transferAmount);
            mBodyParams.put(RequestParams.TRANSFER_BANK_TYPE, mReq.bankType);
            mBodyParams.put(RequestParams.TRANSFER_TYPE, mReq.transferType);
            if (!TextUtils.isEmpty(mReq.postscript)) {
                mBodyParams.put(RequestParams.TRANSFER_POSTSCRIPT, mReq.postscript);
            }
            if (!TextUtils.isEmpty(mReq.billReference)) {
                mBodyParams.put(RequestParams.BILLREFERENCE, mReq.billReference);
            }
            mBodyParams.put(RequestParams.TRANSFER_BANK_NAME, mReq.bankName);
            if (!TextUtils.isEmpty(mReq.transferToName)) {
                mBodyParams.put(RequestParams.TRANSFERTONAME, mReq.transferToName);
            }
            mBodyParams.put(RequestParams.TRANSFER_ISQRCODE, mReq.isQrcode ? "Y" : "N");
            if (mReq.isLiShi) {
                mBodyParams.put(RequestParams.TRANSFERCATEGORY, "7");
                mBodyParams.put(RequestParams.TRANSFER_COVERID, mReq.coverId);
            }
            if (TextUtils.isEmpty(action)){
                mBodyParams.put(RequestParams.ACTION, action);
            }
        }
    }
}
