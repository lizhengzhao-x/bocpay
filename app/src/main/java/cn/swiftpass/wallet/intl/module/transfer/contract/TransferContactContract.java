package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;

/**
 * 转账联系人 获取数据
 */
public class TransferContactContract {

    public interface View extends IView {
        void getContractListSuccess(ContractListEntity response);

        void getContractListError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<TransferContactContract.View> {

        void getContractList();
    }

}
