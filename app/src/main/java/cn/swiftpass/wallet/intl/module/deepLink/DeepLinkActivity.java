package cn.swiftpass.wallet.intl.module.deepLink;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkSkipEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpgradeAccountActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSmartAccountDescActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.BusinessResultsEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity;
import cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteShareActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CAMPAIGNID;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.IMAGEURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.TERMINFOURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.BANKCARDTYPE;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.URL_REWARD;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_CROSS_BORDER;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_E_COUPON;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_MGM;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_REWARD_REGISTER;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_U_PLAN;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.DEEP_LINK_TYPE_U_PLAN_STORE;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.SKIP_TYPE_MAIN_HOME;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.SKIP_TYPE_SPLASH;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.SKIP_TYPE_WEB_VIEW;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.TYPE_REMITTANCE_BIND;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.TYPE_REMITTANCE_CLOSE_MSG_PAGE;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.TYPE_REMITTANCE_UPDATE;
import static cn.swiftpass.wallet.intl.utils.DeepLinkUtils.getLinkType;

public class DeepLinkActivity extends BaseCompatActivity<DeepLinkContract.Presenter> implements DeepLinkContract.View {
    private String actionUrl = "";


    @Override
    protected void init() {

        if (getIntent() != null) {
            actionUrl = getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
            if (!TextUtils.isEmpty(actionUrl)) {
                dealWithUrl(actionUrl);
            }
        }

    }

    /**
     * 处理 url
     *
     * @param actionUrl
     */
    private void dealWithUrl(String actionUrl) {

        if (DeepLinkUtils.isNewDeepLink(actionUrl)) {
            //新需求中的deep link
            skipByDeepLinkType(actionUrl, SKIP_TYPE_SPLASH);
            UserLoginEventManager.getInstance().clearAllEvent();
        } else {
            //旧需求中的app link
            Intent intentNext = new Intent(this, MainHomeActivity.class);
            intentNext.putExtra(MainHomeActivity.IS_AUTO_LOGIN, false);
            startActivity(intentNext);
            finish();
        }
    }


    /**
     * 通过 link type 跳转到相应页面
     *
     * @param deepLinkUrl
     * @param skipType
     */
    public void skipByDeepLinkType(String deepLinkUrl, int skipType) {
        if (skipType == SKIP_TYPE_WEB_VIEW) {

        } else if (skipType == SKIP_TYPE_SPLASH) {
            //SplashActivity界面跳转
            int linkType = getLinkType(deepLinkUrl);
            switch (linkType) {
                case DEEP_LINK_TYPE_U_PLAN_STORE://UPlan指定门店 二级界面
                    mPresenter.goToUPlanStore(deepLinkUrl);
                    break;
                case DEEP_LINK_TYPE_MGM://MGM二级界面
                    ActivitySkipUtil.startAnotherActivity(this, InviteShareActivity.class);
                    finish();
                    break;
                case DEEP_LINK_TYPE_U_PLAN://U_PLAN二级界面
                    mPresenter.goTpUPlan();
                    break;
                case DEEP_LINK_TYPE_REWARD_REGISTER://REWARD_REGISTER二级界面
                    mPresenter.goToAppointRewardRegister(deepLinkUrl);
                    break;
                case DEEP_LINK_TYPE_E_COUPON:
                    ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
                    EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_E_COUPON, "E_COUPON"));
                    finish();
                    break;
                case DEEP_LINK_TYPE_CROSS_BORDER:
                    mPresenter.goToRemittancePage();
                    break;
                case -1:
                    //报错
                    break;
            }

        } else if (skipType == SKIP_TYPE_MAIN_HOME) {

        }
    }


    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected DeepLinkContract.Presenter createPresenter() {
        return new DeepLinkPresenter();
    }


    @Override
    public void goTpUPlanSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            mHashMaps.put(UplanActivity.IS_FROM_DEEP_LINK, true);
            ActivitySkipUtil.startAnotherActivity(this, UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void goTpUPlanFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    public void goToUPlanStoreSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            mHashMaps.put(UplanActivity.IS_FROM_DEEP_LINK, true);
            ActivitySkipUtil.startAnotherActivity(this, UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void goToUPlanStoreFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    public void goToRemittancePageSuccess(TransferCrossBorderBaseEntity response) {
        if (response != null) {
            ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
            EventBus.getDefault().post(new DeepLinkSkipEventEntity(DEEP_LINK_TYPE_CROSS_BORDER, "CROSS_BORDER", response));
            EventBus.getDefault().post(new DeepLinkSkipEventEntity(TYPE_REMITTANCE_CLOSE_MSG_PAGE, "CLOSE_MSG_PAGE"));
            finish();
        }
    }

    @Override
    public void goToRemittancePageFailed(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS)) {
            //支付账户升级
            String message = getString(R.string.CT1_1c_4) + " (" + errorCode + ")";
            AndroidUtils.showBindSmartAccountAndCancelDialogTwo(this, message, getString(R.string.upgrade_account), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    inputPswPopWindow(getActivity(), TYPE_REMITTANCE_UPDATE, true);
                }
            });
        } else if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT_TWO) || TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UPDATE_STATUS_TWO)) {
            //信用卡绑定智能账户
            AndroidUtils.showBindSmartAccountAndCancelDialogTwo(this, getString(R.string.CT1_1c_1) + " (" + errorCode + ")", getString(R.string.PU2107_1_3), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    inputPswPopWindow(getActivity(), TYPE_REMITTANCE_BIND, false);
                }
            });
        } else {
            showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        }
    }

    @Override
    public void goToAppointRewardRegisterSuccess(BusinessResultsEntity response) {
        if (response != null) {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(CAMPAIGNID, response.getCreditCardRewardInfo().getCampaignId());
            maps.put(URL_REWARD, response.getCreditCardRewardInfo().getActivityUrl());
            maps.put(IMAGEURL, response.getCreditCardRewardInfo().getImagesUrl());
            maps.put(TERMINFOURL, response.getCreditCardRewardInfo().getTermsAndConditionsUrl());
            maps.put(BANKCARDTYPE, response.getCreditCardRewardInfo().getBankCardType());
            maps.put(REMARK, response.getCreditCardRewardInfo().getExplain());
            ActivitySkipUtil.startAnotherActivity(this, RewardRegisterWebviewActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void goToAppointRewardRegisterFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }


    /**
     * 密码弹出框
     */
    private void inputPswPopWindow(Activity fromActivity, int type, boolean isSupportFinger) {

        VerifyPasswordCommonActivity.startActivityForResult(fromActivity, isSupportFinger, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {

                if (fromActivity == null) {
                    return;
                }

                if (type == TYPE_REMITTANCE_BIND) {
                    //绑卡
                    binderSmartAccount(fromActivity);
                } else {
                    //升级
                    updateAccount(fromActivity);
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                if (fromActivity == null) {
                    return;
                }
                if (type == TYPE_REMITTANCE_BIND) {
                    showErrorMsgDialog(fromActivity, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            finish();
                        }
                    });
                }
            }

            @Override
            public void onVerifyCanceled() {
                if (type == TYPE_REMITTANCE_BIND) {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                    finish();
                }
            }
        });
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        UserLoginEventManager.getInstance().clearAllEvent();
//
//    }

    /**
     * 绑定智能账户
     * 跳转到RegisterSmartAccountDescActivity
     * 绑定SA账户流程
     */
    private void binderSmartAccount(Activity fromActivity) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        RegisterSmartAccountDescActivity.startActivity(fromActivity, maps);
        finish();
    }

    /**
     * 账户升级
     */
    private void updateAccount(Activity fromActivity) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
        ActivitySkipUtil.startAnotherActivity(fromActivity, UpgradeAccountActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    /**
     * 设置结束动画
     */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}
