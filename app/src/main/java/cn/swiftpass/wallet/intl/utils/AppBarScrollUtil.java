package cn.swiftpass.wallet.intl.utils;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.appbar.AppBarLayout;

/**
 * Created by XiaoJianjun on 2017/1/16.
 * 监听appBar滑动
 */
public class AppBarScrollUtil {

    public static void setAppBarScroll(AppBarLayout abl){
        if(null == abl){
            return;
        }

        abl.addOnOffsetChangedListener(new AppBarStateChangeListener(){
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                /**
                 * 悬浮状态  悬浮状态 TabLayout 也需要可以滑动
                 */
                if (state == State.COLLAPSED) {
                    setAppBarLayoutDisableScroll(appBarLayout,true);
                }
            }
        });
    }

    /***
     * 设置APPBar是否可以滑动
     * @param abl
     * @param disable
     */
    private static void setAppBarLayoutDisableScroll(AppBarLayout abl, boolean disable){
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) abl.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();

        if(null == behavior){
            return;
        }
        if(!disable){
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return false;
                }
            });
        }else {
            behavior.setDragCallback(new AppBarLayout.Behavior.DragCallback() {
                @Override
                public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
                    return true;
                }
            });
        }


    }


}
