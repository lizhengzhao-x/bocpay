package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.KeyBoardEntity;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ForgetPaymentPasswordActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.STwoSelForgetPasswordTypeActivity;
import cn.swiftpass.wallet.intl.module.topup.PwdKeyBoardAdapter;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.widget.CustomPswView;


public class PaymentPasswordDialogWithFioFragment extends DialogFragment {
    private RelativeLayout mId_top_navigation;
    private ImageView mId_back_image;
    private View mId_lineview;
    private CustomPswView mPswView;
    private TextView mId_forget_pwd;
    private GridView mGridView;
    private ImageView mLoadImageView;
    private View mId_root_dialog_progress;
    private int KEY_LEFT_SPACE = 9;
    private int KEY_RIGHT_DEL = 11;
    private String mCurrPsw = "";//当前密码
    private int mPswCount = 6; //默认输入的密码长度是6位
    private OnPwdDialogClickListener listener;
    /**
     * 默认点击 忘记密码 严密弹框要取消
     */
    private boolean isDissDialog = false;
    /**
     * 验证密码加载dialog 导致透明层重叠
     */
    private boolean isUseInnerDialog;


    private void bindViews(View view) {
        mId_top_navigation = (RelativeLayout) view.findViewById(R.id.id_top_navigation);
        mId_back_image = (ImageView) view.findViewById(R.id.id_back_image);
        mLoadImageView = (ImageView) view.findViewById(R.id.loadingImageView);
        mId_lineview = (View) view.findViewById(R.id.id_lineview);
        mId_root_dialog_progress = (View) view.findViewById(R.id.id_root_dialog_progress);
        mPswView = (CustomPswView) view.findViewById(R.id.pswView);
        mId_forget_pwd = (TextView) view.findViewById(R.id.id_forget_pwd);
        mGridView = (GridView) view.findViewById(R.id.gridView);
        mId_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                listener.onPwdBackClickListener();
            }
        });
        String[] keys = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "", "0", "delete"};
        String[] key_engs = new String[]{"", "ABC", "DEF", "GHI", "JKL", "MNO", "PQRS", "TUV", "WXYZ", "", "", "delete"};
        //构造数据
        List<KeyBoardEntity> list = new ArrayList<>();
        for (int i = 0; i < key_engs.length; i++) {
            KeyBoardEntity m = new KeyBoardEntity();
            m.setKey(keys[i]);
            m.setKeyEng(key_engs[i]);
            list.add(m);
        }
        mGridView.setAdapter(getAdapter(list, getActivity()));
        mId_forget_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CacheManagerInstance.getInstance().isLogin()) {
                    getSmartLevelRequest();
                } else {
                    ApiProtocolImplManager.getInstance().forgetPwdDeviceStatus(getActivity(), new NetWorkCallbackListener<ForgetPasswordEntity>() {
                        @Override
                        public void onFailed(String errorCode, String errorMsg) {
                            showErrorMsgDialog(errorMsg);
                        }

                        @Override
                        public void onSuccess(ForgetPasswordEntity response) {
                            ActivitySkipUtil.startAnotherActivity(getActivity(), ForgetPaymentPasswordActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                }
            }
        });
        initEvent(mGridView, list, mPswView);
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface anInterface, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }


    public void initParams(OnPwdDialogClickListener onPwdDialogClickListener, boolean isDissDialog) {

        this.isDissDialog = isDissDialog;
        this.listener = onPwdDialogClickListener;
    }

    public void initParams(OnPwdDialogClickListener onPwdDialogClickListener, boolean isDissDialog, boolean isUseInnerDialogIn) {

        this.isDissDialog = isDissDialog;
        this.listener = onPwdDialogClickListener;
        this.isUseInnerDialog = isUseInnerDialogIn;
    }


    public void initParams(OnPwdDialogClickListener onPwdDialogClickListener) {

        this.listener = onPwdDialogClickListener;
    }

    @Override
    public void dismiss() {
        try {
            dismissProgressDialog();
            super.dismiss();
        } catch (final Exception e) {

        }

    }


    public void showProgressDialog() {
        mId_root_dialog_progress.setVisibility(View.VISIBLE);
        AnimationDrawable animationDrawable = (AnimationDrawable) mLoadImageView.getBackground();
        animationDrawable.start();
    }

    public void dismissProgressDialog() {
        AnimationDrawable animationDrawable = (AnimationDrawable) mLoadImageView.getBackground();
        animationDrawable.stop();
        mId_root_dialog_progress.setVisibility(View.GONE);
    }

    //给键盘设置事件监听
    private void initEvent(GridView gridView, final List<KeyBoardEntity> list, final CustomPswView pswView) {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != KEY_LEFT_SPACE) {
                    if (i == KEY_RIGHT_DEL) {
                        if (mCurrPsw.length() > 0) {
                            mCurrPsw = mCurrPsw.substring(0, mCurrPsw.length() - 1);
                        }
                        pswView.setDatas(mCurrPsw);
                        onClickListenerCallback(mCurrPsw, false);
                    } else {
                        mCurrPsw += list.get(i).getKey();
                        pswView.setDatas(mCurrPsw);
                        if (mCurrPsw.length() == mPswCount) {
                            //输入密码达到6位后  禁止点击忘记密码  保证顺利弹出loading框
                            mId_forget_pwd.setEnabled(false);
                            mGridView.setEnabled(false);
                            onClickListenerCallback(mCurrPsw, true);
                        } else {
                            onClickListenerCallback(mCurrPsw, false);
                        }
                    }
                }
            }
        });
    }

    public void showErrorMsgDialog(String msg) {
        if (getActivity() == null) {
            return;
        }
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(getActivity());
        builder.setMessage(msg);
        builder.setPositiveButton(getActivity().getString(R.string.POPUP1_1), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        if (!getActivity().isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }

    /**
     * 忘记密码前 查询我的账户级别
     */
    private void getSmartLevelRequest() {
        ApiProtocolImplManager.getInstance().getSmartLevel(getActivity(), "", "", "", new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (null != listener) {
                    listener.onPwdFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.SMART_LEVEL_INFO, response);
                ActivitySkipUtil.startAnotherActivity(getActivity(), STwoSelForgetPasswordTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                //是否点击忘记密码跳转的时候 严密弹框要取消
                if (isDissDialog) {
                    if (getActivity() != null) {
                        dismiss();
                        if (getActivity() instanceof VerifyPasswordCommonActivity) {
                            VerifyPasswordCommonActivity vey = (VerifyPasswordCommonActivity) getActivity();
                            vey.verifyCanceled();
                        }
                        getActivity().finish();
                    }
                }
            }
        });
    }


    private void onClickListenerCallback(String cur, boolean complete) {
        if (null != listener) {
            listener.onPwdCompleteListener(cur, complete);
        }
    }


    private CommonAdapter<KeyBoardEntity> getAdapter(List<KeyBoardEntity> data, final Context context) {
        return new CommonAdapter<KeyBoardEntity>(data, 1) {

            @Override
            public Object getItemType(KeyBoardEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new PwdKeyBoardAdapter(context);
            }
        };
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_popwindow_dialog_input_psw, container, false);
        bindViews(view);
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void clearPwd() {
        if (mPswView != null) {
            mCurrPsw = "";
            mGridView.setEnabled(true);
            mId_forget_pwd.setEnabled(true);
            mPswView.setDatas(mCurrPsw);
        }
    }



    public interface OnPwdDialogClickListener {
        void onPwdCompleteListener(String psw, boolean complete);

        void onPwdBackClickListener();

        void onPwdFailed(String errorCode, String errorMsg);
    }
}
