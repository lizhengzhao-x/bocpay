package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

/**
 * 我的电子券 横向布局 两个并排
 */
public class MyEcouponsItemHorizontalAdapter implements AdapterItem<MyEVoucherEntity.EvoucherItem> {
    private Context context;
    private ImageView mId_ecoupon_url;
    private TextView mId_ecoupon_title;
    private TextView mId_jifen_discount;
    private TextView mId_ecoupon_use;
    private TextView leftTitleTv;
    private OnItemEcouponItemUseClickListener onItemEcouponItemUseClickListener;
    private View mId_check_detail, id_buttom_view;

    public MyEcouponsItemHorizontalAdapter(Context context, OnItemEcouponItemUseClickListener onItemEcouponItemUseClickListener) {
        this.context = context;
        this.onItemEcouponItemUseClickListener = onItemEcouponItemUseClickListener;
    }

    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_my_ecoupon_horizontal;
    }

    @Override
    public void bindViews(View root) {
        mId_ecoupon_url = (ImageView) root.findViewById(R.id.id_ecoupon_url);
        mId_ecoupon_title = (TextView) root.findViewById(R.id.id_ecoupon_title);
        mId_jifen_discount = (TextView) root.findViewById(R.id.id_jifen_discount);
        mId_ecoupon_use = (TextView) root.findViewById(R.id.id_use_now);
        leftTitleTv = (TextView) root.findViewById(R.id.id_left_title);
        mId_check_detail = root.findViewById(R.id.id_check_detail);
        id_buttom_view = root.findViewById(R.id.id_buttom_view);
    }


    @Override
    public void setViews() {
        //cardview 左右阴影效果 边距是 5dp
        int width = (AndroidUtils.getScreenWidth(context) - AndroidUtils.dip2px(context, 20) * 3 - AndroidUtils.dip2px(context, 5) * 4) / 2;
        mId_ecoupon_url.setLayoutParams(new FrameLayout.LayoutParams(width, (int) (width / 1.5)));
        mId_check_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponItemUseClickListener.onItemShowMore(mPosition);
            }
        });
        id_buttom_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemEcouponItemUseClickListener.onItemEcouponClick(mPosition);
            }
        });
    }

    @Override
    public void handleData(MyEVoucherEntity.EvoucherItem eVoucherListBean, final int position) {
        mPosition = position;

        mId_ecoupon_title.setText(eVoucherListBean.getItemName() + context.getString(R.string.EC05c_1_1_a) + eVoucherListBean.getEVoucherInfos().size() + context.getString(R.string.EC05c_1));
        GlideApp.with(context).load(AppTestManager.getInstance().getBaseUrl() + eVoucherListBean.getCouponImgSmall()).placeholder(R.mipmap.banner_ecoupon_short).into(mId_ecoupon_url);
        mId_jifen_discount.setText(eVoucherListBean.getExpireDate());
        if (eVoucherListBean.isRed()) {
            mId_jifen_discount.setTextColor(Color.RED);
            leftTitleTv.setTextColor(Color.RED);
        } else {
            mId_jifen_discount.setTextColor(Color.parseColor("#6F7681"));
            leftTitleTv.setTextColor(Color.parseColor("#6F7681"));
        }

    }

    public interface OnItemEcouponItemUseClickListener {
        void onItemEcouponClick(int position);

        void onItemShowMore(int position);
    }
}
