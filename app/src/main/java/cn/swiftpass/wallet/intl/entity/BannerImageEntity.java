package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class BannerImageEntity extends BaseEntity {
    /**
     * imagesUrl : 图片地址
     * type : 0
     */

    private String imagesUrl;
    /**
     * type 类 0 是不显示 1 利利是 2 邀请
     */
    private String type;

    public String getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(String imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
