package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderRecordEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.view
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:26
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderRecordAdapter extends RecyclerView.Adapter {


    private final OnRecordItemClickListener listener;
    private List<TransferCrossBorderRecordEntity> data = new ArrayList<>();
    private Context context;


    public TransferCrossBorderRecordAdapter(Context context, OnRecordItemClickListener listener) {
        this.context = context;
        this.listener = listener;
    }

    public List<TransferCrossBorderRecordEntity> getData() {
        return data;
    }

    public void setData(List<TransferCrossBorderRecordEntity> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int position) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_transfer_cross_border_record, parent, false);
        return new RecordViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        RecordViewHolder holder = (RecordViewHolder) viewHolder;
        final TransferCrossBorderRecordEntity recordEntity = data.get(i);
        if (recordEntity != null) {
            holder.tvPayeeName.setText(recordEntity.payeeHideName);
            holder.tvRecordAmount.setText(recordEntity.currency + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(recordEntity.amount), true));
            holder.tvTransferDate.setText(recordEntity.date);
            holder.tvRecordCardNumber.setText(recordEntity.receiverMarkedCardNo);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) listener.OnRecordItemClick(recordEntity);
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


}
