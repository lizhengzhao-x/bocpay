package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterDetailsEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoParamEntity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.register.adapter.RegisterDetailsAdapter;
import cn.swiftpass.wallet.intl.module.register.model.RegisterDetailsData;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisParams;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

public class RegisterDetailsActivity extends BaseCompatActivity {


    public static final String DATA_REGISTER_DETAIL_DATA = "DATA_REGISTER_DETAIL_DATA";
    private RecyclerView mRecyclerView;
    private RegisterDetailsAdapter mAdapter;
    private List<RegisterDetailsEntity> mList = new ArrayList<>();
    private RegisterCustomerInfo registerCustomerInfo;
    private S2RegisterInfoEntity registerInfoEntity;
    private int mCurrentType;
    private boolean isSelection;//是否选中
    private TextView id_second_tv, id_forth_tv;
    private RegisterDetailsData registerDetailsData;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {


        id_second_tv = (TextView) findViewById(R.id.id_second_tv);
        id_forth_tv = (TextView) findViewById(R.id.id_forth_tv);

        mCurrentType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        Serializable detailSerData = getIntent().getSerializableExtra(RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA);
        if (detailSerData != null) {
            registerDetailsData = (RegisterDetailsData) detailSerData;
        }

        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        }
        setToolBarTitle(R.string.P3_A1_18b_1);
        registerCustomerInfo = (RegisterCustomerInfo) getIntent().getExtras().getSerializable(Constants.CUSTOMER_INFO);
        registerInfoEntity = (S2RegisterInfoEntity) getIntent().getExtras().getSerializable(Constants.OCR_INFO);
        initData();
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_info);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerView.setFocusableInTouchMode(false);
        mAdapter = new RegisterDetailsAdapter(mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setData(mList);
        DataCollectManager.getInstance().sendConfirmInformation();
        itemFoot();
        id_second_tv.setText(new SpannableString(getString(R.string.P3_A1_18b_12_2)));
        id_forth_tv.setText(new SpannableString(getString(R.string.P3_A1_18b_12_3)));
    }

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA || mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.ALL_INFORMATION_CONFIRM);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA || mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.DAILY_LIMIT_SETTING;
        analysisPageEntity.current_address = PagerConstant.ALL_INFORMATION_CONFIRM;
        return analysisPageEntity;
    }

    public void initSDKListener() {

        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);

        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_CONFIRMREGISTERDATA);
    }

    private void submitRegisterInfo() {
        String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;
        ApiProtocolImplManager.getInstance().sTwoRegisterCustomerInfo(this, type, registerCustomerInfo, new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);
                if (errorCode.equals(ErrorCode.CHECK_IDV_FAILED.code)) {
                    //退出流程
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                    ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, CheckIdvFailedActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else if (errorCode.equals(ErrorCode.CHECK_ID_ZHONGYIN_CURTOMER.code) || errorCode.equals(ErrorCode.CHECK_ID_ZHONGYIN_CUS.code)) {
                    //是否是中银客户 点击跳转到
                    showErrorMsgDialog(RegisterDetailsActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            //已经是中银客户 点击跳转到 信用卡+智能账户注册界面 并要保证 点击back的时候要退到登录界面
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            if (mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                            } else {
                                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                            }
                            MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                            ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                } else if (errorCode.equals(ErrorCode.CHECK_ID_RETRY.code)) {
                    //idv认证对比需要轮训
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                    ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, CheckIdvProgressActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    showErrorMsgDialog(RegisterDetailsActivity.this, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            if (TextUtils.equals(errorCode, ErrorCode.CONTENT_TIME_OUT.code) || TextUtils.equals(errorCode, ErrorCode.NETWORK_ERROR.code)) {
                                //网络异常的错误码不处理
                            } else {
                                if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                                    //注册流程 不管是黑名单 还是已经注册bocpay 都跳转到登录界面
                                    MyActivityManager.removeAllTaskWithRegisterIsHkCust();
                                } else if (mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
                                    //绑卡 应该只存在黑名单情况 跳转到选择绑卡界面
                                    MyActivityManager.removeAllTaskWithPaBindCard();
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {

                if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                    mHashMaps.put(Constants.MOBILE_PHONE, response.getMobile());
                    ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    //绑卡流程
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(AnalysisParams.CURRENT_ADDRESS, PagerConstant.ALL_INFORMATION_CONFIRM);
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                    mHashMaps.put(Constants.DATA_PHONE_NUMBER, response.getMobile());
                    ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }


    private void initData() {

        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_8_2), registerInfoEntity.getChinessName()));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_8_3), registerInfoEntity.getEnglishName()));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_4), registerInfoEntity.getCertNo()));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_8_5), registerInfoEntity.getBirthday()));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_5), registerInfoEntity.getSex().equals("0") ? getString(R.string.P3_A1_18b_5_2) : getString(R.string.P3_A1_18b_5_1)));
        if (!TextUtils.isEmpty(registerInfoEntity.getIssueDate())) {
            mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_15_7), registerInfoEntity.getIssueDate()));
        }
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_6), registerInfoEntity.getNationalName()));
        String address = registerCustomerInfo.mRegion + "," + registerCustomerInfo.mStreetName + "," + registerCustomerInfo.mBuildingName + "," + registerCustomerInfo.mRoom;
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_7), address));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_8), registerCustomerInfo.mEmail));
//        mList.add(new RegisterDetailsEntity(getString(R.string.daily_limit_amount), "HKD " + registerCustomerInfo.mCurrentLimit));
        String money = AndroidUtils.formatPriceWithPoint(Double.parseDouble(registerCustomerInfo.mCurrentLimit), true);
        mList.add(new RegisterDetailsEntity(getString(R.string.daily_limit_amount), "HKD " + money.substring(0, money.length() - 3)));
        ArrayList<TaxInfoParamEntity> mTaxInfos = registerCustomerInfo.mTaxInfos;
        for (int i = 0; i < mTaxInfos.size(); i++) {
            if (!TextUtils.isEmpty(mTaxInfos.get(i).getTaxNumber())) {
                mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_16_4_1) + "(" + mTaxInfos.get(i).getTaxResidentRegionName() + ")", mTaxInfos.get(i).getTaxNumber()));
            } else {
                mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_16_4_1) + "(" + mTaxInfos.get(i).getTaxResidentRegionName() + ")", mTaxInfos.get(i).getNoResidentReasonInfo()));

            }
        }
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_10), getString(R.string.P3_A1_18b_10_1)));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_11), getString(R.string.P3_A1_18b_11_1)));
        mList.add(new RegisterDetailsEntity(getString(R.string.P3_A1_18b_12), getString(R.string.P3_ewa_01_009)));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_register_details;
    }


    private void itemFoot() {
        final ImageView ck_selected = (ImageView) findViewById(R.id.ck_selected);
        final TextView tv_next = (TextView) findViewById(R.id.tv_next);
        final TextView tv_edit = (TextView) findViewById(R.id.tv_edit);
        TextView tv_tips = (TextView) findViewById(R.id.tv_tips);
        String totalStr = getResources().getString(R.string.P3_A1_18b_12_1_a);
        //本人己阅悉、明白及接受BoC Pay相关服务条款及细则(如适用)、中国银行(香港)有限公司的《服务条款》(如适用)、《零售银行服务一般说明》(如适用)、《资料政策通告》及《私隐政策声明》。
        //本人己阅悉、明白及接受BoC Pay%%相关服务条款及细则(如适用)%%、%%中国银行(香港)有限公司的《服务条款》(如适用)%%、%%《零售银行服务一般说明》(如适用)%%、%%《资料政策通告》%%及%%《私隐政策声明》%%。
        final String items[] = totalStr.split("%%");
        int firstSart = totalStr.indexOf(items[1]) - 2;
        int firstEnd = firstSart + items[1].length();

        int secondSart = totalStr.indexOf(items[3]) - 6;
        int secondEnd = secondSart + items[3].length();

        int thirdSart = totalStr.indexOf(items[5]) - 10;
        int thirdEnd = thirdSart + items[5].length();

        int fouthSart = totalStr.indexOf(items[7]) - 14;
        int forthEnd = fouthSart + items[7].length();

        int fifthSart = totalStr.indexOf(items[9]) - 18;
        int fifthEnd = fifthSart + items[9].length();
        SpannableString spannableString = new SpannableString(totalStr.replace("%%", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, ServerAgreementActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, firstSart, firstEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_TITLE, items[3]);
                String lan = SpUtils.getInstance(RegisterDetailsActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.CONDITIONSFORSERVICES_SC);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.CONDITIONSFORSERVICES_TC);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.CONDITIONSFORSERVICES_EN);
                }
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, secondSart, secondEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                AndroidUtils.openUrl(mContext, Constants.GEN_INFO);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, thirdSart, thirdEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                String lan = SpUtils.getInstance(RegisterDetailsActivity.this).getAppLanguage();
                mHashMaps.put(Constants.DETAIL_TITLE, items[7]);
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.IMPORTANTNOTICE_SC);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.IMPORTANTNOTICE_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.IMPORTANTNOTICE_EN);
                }
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, fouthSart, forthEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                String lan = SpUtils.getInstance(RegisterDetailsActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();

                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_SC);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_TC);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_EN);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, items[9]);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, fifthSart, fifthEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        spannableString.setSpan(new UnderlineSpan(), firstSart, firstEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), secondSart, secondEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), thirdSart, thirdEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), fouthSart, forthEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), fifthSart, fifthEnd, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        tv_tips.setText(spannableString);
        tv_next.setEnabled(false);
        ck_selected.setImageResource(R.mipmap.icon_check_choose_circle_default);

        tv_tips.setMovementMethod(LinkMovementMethod.getInstance());
        ck_selected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isSelection) {
                    //选中
                    tv_next.setEnabled(true);
                    isSelection = false;
                    ck_selected.setImageResource(R.mipmap.icon_check_choose_circle);
                } else {
                    tv_next.setEnabled(false);
                    isSelection = true;
                    ck_selected.setImageResource(R.mipmap.icon_check_choose_circle_default);
                }
            }
        });

        tv_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    submitRegisterInfo();
                }
            }
        });

        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //退出到地址输入界面
                Intent intent = new Intent(RegisterDetailsActivity.this, STwoRegisterIdAuthInfoActivity.class);
                intent.addFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_CLEAR_TOP);

                intent.putExtra(DATA_REGISTER_DETAIL_DATA, registerDetailsData);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.INCLUDE_ONLINE_CHAT && mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT && mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            ChatSDKManager.hide();
        }
    }
}
