package cn.swiftpass.wallet.intl.module.setting;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.http.SslError;
import android.text.TextUtils;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.IOException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.CertsDownloadManager;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.ProgressWebView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static cn.swiftpass.wallet.intl.entity.Constants.BUILD_FLAVOR_PRD;
import static cn.swiftpass.wallet.intl.entity.Constants.FILENAME;

public class BaseWebViewActivity extends BaseCompatActivity {


    private ValueCallback<Uri[]> mValueCallback;
    protected static final String TAG = "WebViewActivity";
    private CustomDialog mDealDialog;
    private String tel;
    /**
     * 当前进入webiew的根url
     */
    protected String mOriginalUrl;
    private ArrayList<Certificate> currentCertificates;

    /**
     * 是否支持缩放功能
     */
    public static String ISSUPPORTZOOMIN = "ISSUPPORTZOOMIN";

    /**
     * 记录url 判断重定向的问题
     */
    private String startUrl;

    private boolean isDialogAlreadyShow;
    private ImageView closeIv;
    private WebView webView;

    @Override
    public void init() {
        //android webview 会影响到app国际化
//        String lan = SpUtils.getInstance(mContext).getAppLanguage();
//        switchLanguage(lan);
        closeIv = setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
        closeIv.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                finish();
            }
        });

//        //本身webview重定向的时候如果网络异常 会调用onReceivedError 然后会弹框提示 自从加上证书校验的功能之后，原始url https证书校验会失败 不会重定向 断网不会提示 现在的做法是进入当前页面要检测网络
//        if (!NetworkUtil.isNetworkAvailable()) {
//            showErrorMsgDialog(this, getString(R.string.string_network_available), new OnMsgClickCallBack() {
//                @Override
//                public void onBtnClickListener() {
//                    if (Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
//                        finish();
//                    }
//                }
//            });
//        }
    }


    protected View getCloseView() {
        return closeIv;
    }

    /**
     * 根据url下载图片，然后分享
     *
     * @param shareContent
     */
    public void shareImageUrl(final String shareContent) {
        PermissionInstance.getInstance().getStoreImagePermission(BaseWebViewActivity.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                showDialogNotCancel();
                GlideApp.with(BaseWebViewActivity.this).asBitmap().load(shareContent).into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, resource, FILENAME);
                        AndroidUtils.shareImage(BaseWebViewActivity.this, "", uri);
                        dismissDialog();
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        dismissDialog();
                        showErrorMsgDialog(BaseWebViewActivity.this, getString(R.string.NO1_4_1));
                    }
                });
            }

            @Override
            public void rejectPermission() {
                showLackOfPermissionDialog(BaseWebViewActivity.this);
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    protected void initWebViewSettings(ProgressWebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setDomStorageEnabled(true);
        boolean isSupportZoomIn = getIntent().getBooleanExtra(ISSUPPORTZOOMIN, false);
        if (isSupportZoomIn) {
            // 设置WebView可触摸放大缩小
            settings.setSupportZoom(true);
            settings.setBuiltInZoomControls(true);
            settings.setUseWideViewPort(true);
            //隐藏掉缩放工具
            settings.setDisplayZoomControls(false);
        }
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setAllowUniversalAccessFromFileURLs(false);

        //webView.loadUrl(url);
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });

        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                webView.upDateProgress(newProgress);
            }
        });
    }

    protected void supportZoom(ProgressWebView webView, boolean isSupport) {
        WebSettings settings = webView.getSettings();
        settings.setSupportZoom(isSupport);
        settings.setBuiltInZoomControls(isSupport);
        settings.setUseWideViewPort(isSupport);
        //隐藏掉缩放工具
        settings.setDisplayZoomControls(isSupport);
    }

    /**
     * @param webView
     * @param url
     * @param onUrlVerifyCallBack 有些场景支持校验成功之后需要把url返回
     */
    protected void initWebView(final ProgressWebView webView, final String url, boolean isFirstShowDlalog, final OnUrlVerifyCallBack onUrlVerifyCallBack) {
        LogUtils.i(TAG, "original url :" + url);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mValueCallback = filePathCallback;
                startActivityForResult(fileChooserParams.createIntent(), 1);
                return true;
            }

        });

        initWebViewSettings(webView);
        if (BuildConfig.DOWNLOAD_CER_SERVER) {
            CertsDownloadManager.getInstance().loadCertificates(this, true, new CertsDownloadManager.OnCertDownLoadListener() {
                @Override
                public void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates) {
                    if (isSuccess) {
                        //webview证书加载不能loadurl 需要校验
                        List<Certificate> certificatesLocal = HttpsUtils.getLocalCertificates();
                        currentCertificates = new ArrayList<>();
                        currentCertificates.addAll(certificatesLocal);
                        for (int i = 0; i < certificates.length; i++) {
                            currentCertificates.add(certificates[i]);
                        }
                        dealWithUrl(webView, url, false, isFirstShowDlalog, onUrlVerifyCallBack);
                    } else {
                        showErrorMsgDialog(BaseWebViewActivity.this, getString(R.string.string_network_available), new OnMsgClickCallBack() {
                            @Override
                            public void onBtnClickListener() {
                                finish();
                            }
                        });
                    }
                }
            });
        } else {
            //webview证书加载不能loadurl 需要校验
            currentCertificates = getCertificatesFromLocal();
            verifyCertifacate(webView, url, false, isFirstShowDlalog, onUrlVerifyCallBack);
        }
    }

    protected void initWebView(final ProgressWebView webView, final String url) {
        initWebView(webView, url, true, null);
    }

    protected void initWebView(final ProgressWebView webView, boolean isFirstShowDlalog, final String url) {
        initWebView(webView, url, isFirstShowDlalog, null);
    }

    /**
     * @param webView
     * @param isOpenErrorHint
     * @param onUrlVerifyCallBack url 证书校验成功之后需要会掉给url 有些场景是url需要拦截单独处理
     */
    protected void setWebViewClient(final WebView webView, final boolean isOpenErrorHint, final boolean isAutoLoadUrl, final OnUrlVerifyCallBack onUrlVerifyCallBack) {

        this.webView = webView;
        webView.setWebViewClient(new WebViewClient() {


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                startUrl = url;
            }


            @Nullable
            @Override
            public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
                LogUtils.i(TAG, "shouldInterceptRequest:" + request.getUrl());
                return super.shouldInterceptRequest(view, request);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogUtils.i(TAG, "shouldOverrideUrlLoading:" + url);

//                if (TextUtils.equals(url, "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/offer_bocpay/smp/more_hk.html")) {
//                    url = "https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=ccRegisterPromotion&campaignId=SPENDANDWIN2021MAR";
//                } else if (TextUtils.equals(url, "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/offer_bocpay/smp/more_mainland.html")) {
//                    url = "https://shawn-dev.wepayez.com/bocpay?type=JumpPage&pageId=ccRegisterPromotion&campaignId=SOGOAPR2021";
//                } else if (TextUtils.equals(url, "https://www.bochk.com/creditcard/BOCPAY/offer_bocpay/chi/more_hk.html")) {
//                    url = "https://shawn-dev.wepayez.com/bocpay/bocpay?ewaBoCPayNativeType=JumpPage&ewaBoCPayNativePageId=BoCPay_RemittancePage";
//                } else if (TextUtils.equals(url, "https://www.bochk.com/creditcard/BOCPAY/offer_bocpay/chi/more_mainland.html")) {
//                    url = "https://shawn-dev.wepayez.com/bocpay/bocpay?ewaBoCPayNativeType=JumpPage&ewaBoCPayNativePageId=BoCPay_CCRgisterPromotion";
//                }

                if (DeepLinkUtils.isNewDeepLink(url)) {
                    DeepLinkUtils.skipByDeepLinkType(url, getActivity(), startTime, DeepLinkUtils.SKIP_TYPE_WEB_VIEW);
                } else {
                    if (startUrl != null && startUrl.equals(url)) {
                        dealWithUrl(view, url, isAutoLoadUrl, onUrlVerifyCallBack);
                    } else {
                        dealWithUrl(view, url, isAutoLoadUrl, onUrlVerifyCallBack);
                        //return super.shouldOverrideUrlLoading(view, url);
                    }
                }
                return true;
            }


            @Override
            public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                super.onReceivedClientCertRequest(view, request);
                LogUtils.i(TAG, "onReceivedClientCertRequest:" + view.getUrl());
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                LogUtils.i(TAG, "onReceivedError:" + error.toString());
                if (isOpenErrorHint) {
                    // 断网或者网络连接超时
                    int errorCode = error.getErrorCode();
                    if (errorCode == ERROR_HOST_LOOKUP || errorCode == ERROR_CONNECT || errorCode == ERROR_TIMEOUT) {
                        // 避免出现默认的错误界面
                        view.loadUrl("about:blank");
                        showErrorMsgDialog(view.getContext(), getString(R.string.string_network_available), new OnMsgClickCallBack() {
                            @Override
                            public void onBtnClickListener() {
                                if (Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
                                    finish();
                                }
                            }
                        });
                    }
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.i(TAG, "onReceivedHttpError:" + errorResponse.toString());
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                LogUtils.i(TAG, "onReceivedSslError:" + error.toString());
                if (BuildConfig.FLAVOR.equals("uat")) {
                    handler.proceed();
                    initSafeOkhttpClient(view, handler);
                } else {
                    //super.onReceivedSslError(view, handler, error);
                }
            }
        });
    }

    protected void setWebViewClient(final WebView webView, final boolean isOpenErrorHint) {
        setWebViewClient(webView, isOpenErrorHint, true, null);
    }

    protected void setWebViewClient(final WebView webView, final boolean isOpenErrorHint, final boolean isAutoLoadUrl) {
        setWebViewClient(webView, isOpenErrorHint, isAutoLoadUrl, null);
    }

    private void dealWithUrl(WebView view, String url, final boolean isAutoLoadUrl, OnUrlVerifyCallBack onUrlVerifyCallBack) {

        dealWithUrl(view, url, isAutoLoadUrl, true, onUrlVerifyCallBack);

    }


    /**
     * @param view
     * @param url
     * @param isAutoLoadUrl       证书校验完成之后 是否自动loadurl
     * @param isFirstShowDlalog   第一次webview重定向 证书校验是否需要弹框
     * @param onUrlVerifyCallBack
     */
    private void dealWithUrl(WebView view, String url, final boolean isAutoLoadUrl, final boolean isFirstShowDlalog, OnUrlVerifyCallBack onUrlVerifyCallBack) {
        if (TextUtils.isEmpty(url)) {
            return;
        }
        if (url.endsWith(".pdf") || url.contains("bigbigchannel")) {
            Uri uri = Uri.parse(url);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
            //防止进入webview页面的rootUrl就是pdf 那么整个界面会出现白屏
            if (!TextUtils.isEmpty(mOriginalUrl) && TextUtils.equals(mOriginalUrl, url)) {
                finish();
            }
        } else if (url.startsWith("tel:")) {
            showCallPhoneDialog(url);
        } else {
            verifyCertifacate(view, url, isAutoLoadUrl, isFirstShowDlalog, onUrlVerifyCallBack);
        }
    }

    protected OkHttpClient.Builder setCertificates(OkHttpClient.Builder client) {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            int index = 0;
            for (Certificate certificate : currentCertificates) {
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, certificate);
            }
            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            final X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            client.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return client;
    }

    private ArrayList<Certificate> getCertificatesFromLocal() {
        ArrayList<Certificate> certificates = null;
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            certificates = new ArrayList<>();
            String path = "boc_certs_prd";
            if (BuildConfig.FLAVOR.equals(BUILD_FLAVOR_PRD)) {
            } else {
                path = "boc_certs_uat";
            }
            try {
                AssetManager am = getAssets();
                String[] list = am.list(path);
                for (int i = 0; i < list.length; i++) {
                    certificates.add(certificateFactory.generateCertificate(am.open(path + "/" + list[i])));
                    LogUtils.i(TAG, "cer name:" + path + "/" + list[i]);
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        } catch (CertificateException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return certificates;
    }


    /**
     * 强制证书校验 通过才可以打开url 否则需要弹框提示
     *
     * @param view
     * @param url
     */
    protected void verifyCertifacate(final WebView view, final String url, final boolean isAutoLoadUrl, final boolean isFirstShowDlalog, final OnUrlVerifyCallBack onUrlVerifyCallBack) {
        if (!TextUtils.isEmpty(url)) {
            if (url.startsWith("http") || url.startsWith("HTTP") || url.startsWith("WWW") || url.startsWith("www")) {
                OkHttpClient.Builder builder = null;
                try {
                    builder = setCertificates(new OkHttpClient.Builder());
                } catch (Exception e) {
                    builder = new OkHttpClient.Builder();
                }
                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };
                builder.hostnameVerifier(hostnameVerifier);
                Request request = new Request.Builder().url(url).build();
                if (isFirstShowDlalog) {
                    showDialogNotCancel(this);
                }
                builder.build().newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        LogUtils.i(TAG, "E:" + e.getLocalizedMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
//                                Toast.makeText(getActivity(),"error:"+e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
                                if (e instanceof SSLHandshakeException) {
                                    if (BuildConfig.FLAVOR.equals(Constants.BUILD_FLAVOR_UAT)) {
                                        //uat 环境下 特殊处理 有些uat自签名证书校验不过 需要域名匹配
                                        List<String> whiteList = Constants.WHITELISTDOMAINLIST;
                                        boolean contain = false;
                                        for (int i = 0; i < whiteList.size(); i++) {
                                            if (!TextUtils.isEmpty(url) && url.startsWith(whiteList.get(i))) {
                                                contain = true;
                                                break;
                                            }
                                        }
                                        if (contain) {
                                            if (onUrlVerifyCallBack != null) {
                                                if (isAutoLoadUrl) {
                                                    view.loadUrl(url);
                                                }
                                                onUrlVerifyCallBack.OnUrlVerifyFinish(url, true);
                                            } else {
                                                view.loadUrl(url);
                                            }

                                        } else {
                                            showOpenUrlWithBrowser(url);
                                        }

                                    } else {
                                        //弹框提示
                                        //Toast本质是通过window显示和绘制的（操作的是window），
                                        // 而主线程不能更新UI 是因为ViewRootImpl的checkThread方法在Activity维护的View树的行为。
                                        // Toast中TN类使用Handler是为了用队列和时间控制排队显示Toast，
                                        // 所以为了防止在创建TN时抛出异常，需要在子线程中使用Looper.prepare();和Looper.loop();
                                        // （但是不建议这么做，因为它会使线程无法执行结束，导致内存泄露）
                                        //Only the original thread that created a view hierarchy can touch its views.
                                        //ViewRootImpl在哪个线程创建的，你后续的UI更新就需要在哪个线程执行，跟是不是UI线程无关
//                                        Looper.prepare();
                                        showOpenUrlWithBrowser(url);
//                                        Looper.loop();
                                    }
                                } else {
                                    //证书校验失败 应该弹框网络异常
                                    showErrorMsgDialog(view.getContext(), getString(R.string.string_network_available), new OnMsgClickCallBack() {
                                        @Override
                                        public void onBtnClickListener() {
                                            if (Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
                                                finish();
                                            }
                                        }
                                    });
                                }

                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        LogUtils.i(TAG, "E:response" + response.toString());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dismissDialog();
                                if (onUrlVerifyCallBack != null) {
                                    if (isAutoLoadUrl) {
                                        view.loadUrl(url);
                                    }
                                    onUrlVerifyCallBack.OnUrlVerifyFinish(url, true);
                                } else {
                                    view.loadUrl(url);
                                }
                            }
                        });
                    }
                });
            } else {
                view.loadUrl(url);
            }
        } else {
            return;
        }

    }

    /**
     * 通过外部浏览器打开
     *
     * @param url
     */
    private void showOpenUrlWithBrowser(final String url) {
        //防止多次弹框  触发场景为用户多次点击某个连接 重定向进行证书校验，但是又不能对网页重定向做时间差处理 智能控制弹框
        if (isDialogAlreadyShow) {
            return;
        }
        isDialogAlreadyShow = true;
        String str_title = "   ";
        String str_msg = getString(R.string.WEBV1_1_1);
        String str_ok = getActivity().getString(R.string.WEBV1_1_3);
        String str_cancel = getActivity().getString(R.string.WEBV1_1_2);
        AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_ok, str_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogAlreadyShow = false;
                AndroidUtils.openUrl(getActivity(), url);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                isDialogAlreadyShow = false;
            }
        });
    }

    protected void initSafeOkhttpClient(WebView view, final SslErrorHandler handler) {
        OkHttpClient.Builder builder;
        try {
            builder = setCertificates(new OkHttpClient.Builder());
        } catch (Exception e) {
            builder = new OkHttpClient.Builder();
        }
        Request request = new Request.Builder().url(view.getUrl()).build();
        builder.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                handler.proceed();
            }
        });
    }

    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!TextUtils.isEmpty(tel) && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + tel));
                startActivity(intent);
            }
        }

    }


    //拨打电话
    protected void callPhone(String tel) {
        //检查拨打电话权限
        if (!TextUtils.isEmpty(tel) && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + tel));
            startActivity(intent);
        } else {
            checkPermissions(Manifest.permission.CALL_PHONE);
        }
    }


    protected void showCallPhoneDialog(String url) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        tel = url.replace("tel:", "");

        if (TextUtils.isEmpty(tel)) return;
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(tel);
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.POPUP1_2), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callPhone(tel);
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    @Override
    public void finish() {
        super.finish();
        recycleWebView(webView);
    }


    protected void recycleWebView(WebView webView) {
        if (webView != null) {
            webView.clearHistory();
            webView.clearCache(true);
            webView.loadUrl("about:blank"); // clearView() should be changed to loadUrl("about:blank"), since clearView() is deprecated now
            webView.freeMemory();
            webView.pauseTimers();
            webView.clearView();
            webView.destroy();
        }
    }

    public interface OnUrlVerifyCallBack {

        void OnUrlVerifyFinish(String url, boolean isSuccess);
    }
}
