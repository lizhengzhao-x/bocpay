package cn.swiftpass.wallet.intl.base;

import android.Manifest;


public class WalletConstants {

    public static final String[] PERMISSIONS = new String[]{Manifest.permission.CAMERA};
    public static final String[] PERMISSIONS_READ_CONTACTS = new String[]{Manifest.permission.READ_CONTACTS};
    public static final String[] PERMISSIONS_READ_SD = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final String[] PERMISSIONS_READ_PHONE_STATUS = new String[]{Manifest.permission.READ_PHONE_STATE};
    public static final String[] PERMISSIONS_ALL = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA};
    public static final String[] PERMISSIONS_READ_WRITE = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final String[] PERMISSIONS_WRITE_WRITE = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

}
