package cn.swiftpass.wallet.intl.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.security.cert.Certificate;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.ErrorEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.ActionCreditListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ActionQueryGpInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ActionTrxGpInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ActionTxnHistEnquiryCreditBillProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ActivityProtocol;
import cn.swiftpass.wallet.intl.api.protocol.AddTransferInviteRecordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ApplyVirCardProtocol;
import cn.swiftpass.wallet.intl.api.protocol.BankLoginVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CardListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckBankRegisterVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckIDvForBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckIDvForForgetNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckIDvNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckIDvProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckPAAcountVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckPasswordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckStaticCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ConfirmMbkAccessTokenProtocol;
import cn.swiftpass.wallet.intl.api.protocol.DatacollectProtocol;
import cn.swiftpass.wallet.intl.api.protocol.DeleteEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioAuthIdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioAuthOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioInitCinProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioRegRequestProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioRegResponseProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsConfirmProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsPreCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FpsQueryBankOtherRecords;
import cn.swiftpass.wallet.intl.api.protocol.FpsQueryBankRecords;
import cn.swiftpass.wallet.intl.api.protocol.GetBankListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetCardListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetCustomTelRegionProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetMbkAccessTokenProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetMbkLgnInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetPayTypeListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSDKErrMsgProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSmartLevelForBindProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSmartLevelForForgetProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSmartLevelProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSweptCodePointsProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTnxIdForForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTnxIdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferInviteInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferInviteRecordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetUplanurlProtocol;
import cn.swiftpass.wallet.intl.api.protocol.InitBannerProtocol;
import cn.swiftpass.wallet.intl.api.protocol.LogoutProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderWithPointProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ModifyPasswordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ParserTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QrcodeWithPointsProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryBillProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryEnquirySmartCardBillProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTradeStatusProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegisterByCreditCardProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegisterByCreditProtocol;
import cn.swiftpass.wallet.intl.api.protocol.RegisterBySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.S2RegisterCustomerInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.S2RegisterSetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.STwoTopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ScavengingProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SetDefaultCardProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ShowInviteUIProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SmartAccountCheckListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SmartForgotPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SmilingFaceProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpPreCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferPreCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferQueryBalanceProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferSummaryProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TurnOnRedPacketProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UnlinkCardProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountInfoSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountSubmitProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VerifySmartAccountInfoSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardCreditCardRegisterProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardRegisterSetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardSmartAccountForgetPwdProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.ApplyVirUrlEntity;
import cn.swiftpass.wallet.intl.entity.AreaListEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.BankRegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.BillListEntity;
import cn.swiftpass.wallet.intl.entity.ChatEntity;
import cn.swiftpass.wallet.intl.entity.CreditCardBillEntity;
import cn.swiftpass.wallet.intl.entity.CreditCardEntity;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.entity.FioCheckValidEntity;
import cn.swiftpass.wallet.intl.entity.FioInitEntity;
import cn.swiftpass.wallet.intl.entity.FioRegRequestEntity;
import cn.swiftpass.wallet.intl.entity.FioRegResponseEntity;
import cn.swiftpass.wallet.intl.entity.FioVerifyOtpEntity;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.entity.FpsConfirmRequest;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckRequest;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.GetTnxIdEntity;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.NewErrorCodeEntity;
import cn.swiftpass.wallet.intl.entity.PAAccountVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.PaymentListEntity;
import cn.swiftpass.wallet.intl.entity.QrIsSweptEntity;
import cn.swiftpass.wallet.intl.entity.RegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.RegisterCreditEntity;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.entity.RegisterInviteInfo;
import cn.swiftpass.wallet.intl.entity.RegisterInviteStatusEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterMobileEntity;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.ShowInviteCodeEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountListEntity;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;
import cn.swiftpass.wallet.intl.entity.TransferBillEntity;
import cn.swiftpass.wallet.intl.entity.TransferListEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.entity.TurnOnRedPackEtntity;
import cn.swiftpass.wallet.intl.entity.UnlinkCardEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSetPwdEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.sdk.utils.IdvSdkManager;
import cn.swiftpass.wallet.intl.utils.CertsDownloadManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.CustomProgressDialog;

/**
 * 所有接口的实现
 */

public class ApiProtocolImplManager<T> implements ApiProtocol {


    private CustomProgressDialog progressDialog;


    private static class ApiManagerHolder {
        private static ApiProtocolImplManager instance = new ApiProtocolImplManager();
    }

    private final String TAG = ApiProtocolImplManager.class.getSimpleName();

    public static ApiProtocolImplManager getInstance() {
        return ApiManagerHolder.instance;
    }

    private ApiProtocolImplManager() {

    }


    public void dismissDialog() {
        try {
            if (progressDialog == null) {
                return;
            } else {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    progressDialog = null;
                }
            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }


    /**
     * 网络是否可用
     *
     * @return
     */
    private boolean checkNetAvailable() {
        return NetworkUtil.isNetworkAvailable();
    }

    public void setDefaultCard(final Context context, String cardId, String defaultPayType, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new SetDefaultCardProtocol(cardId, defaultPayType, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 被扫二维码信息获取
     *
     * @param context
     * @param panId
     * @param couponNo
     * @param callback
     */

    public void getCPQRCode(final Context context, final String panId, final String couponNo, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            callback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
            return;
        }
        new ScavengingProtocol(panId, couponNo, action, new NetWorkCallbackListener<QrIsSweptEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(QrIsSweptEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    public void getMbkLgnInfo(String s, final NetWorkCallbackListener<ChatEntity> callback) {

        new GetMbkLgnInfoProtocol(s, new NetWorkCallbackListener<ChatEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ChatEntity response) {
                if (null != callback) {
                    callback.onSuccess(response);
                }
            }
        }).execute();
    }

    public void getUplanInfo(String actionInfo, final NetWorkCallbackListener<UplanUrlEntity> callback) {

        new GetUplanurlProtocol(actionInfo, new NetWorkCallbackListener<UplanUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (null != callback) {
                    callback.onFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UplanUrlEntity response) {
                if (null != callback) {
                    callback.onSuccess(response);
                }
            }
        }).execute();
    }

    public void getMbkAccessToken(String channel, String requesterAccessToken, final NetWorkCallbackListener<ChatEntity> callback) {

        new GetMbkAccessTokenProtocol(channel, requesterAccessToken, new NetWorkCallbackListener<ChatEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ChatEntity response) {
                if (null != callback) {
                    callback.onSuccess(response);
                }
            }
        }).execute();
    }

    public void confirmMbkAccessToken(String ts, String channel, String nonceStr, String signature, final NetWorkCallbackListener<ChatEntity> callback) {

        new ConfirmMbkAccessTokenProtocol(ts, channel, nonceStr, signature, new NetWorkCallbackListener<ChatEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ChatEntity response) {
                if (null != callback) {
                    callback.onSuccess(response);
                }
            }
        }).execute();
    }


    /**
     * dialog不可以取消
     *
     * @param context
     */
    public void showDialogNotCancel(Context context) {
        showDialogNotCancel(context, 0);
    }

    /**
     * dialog不可以取消
     *
     * @param context
     */
    public void showDialogNotCancel(Context context, float alpha) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                LogUtils.i(TAG, "current activity2" + activity);
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.dialog_message));
                    progressDialog.setCancelable(false);
                    if (progressDialog.getWindow() != null) {
                        progressDialog.getWindow().setDimAmount(alpha);
                    }
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, Log.getStackTraceString(e));
            }
        } else {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog.show();
                }
            } catch (Exception e) {
                LogUtils.e(TAG, Log.getStackTraceString(e));
            }
        }
    }


    public void showDialog(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.dialog_message));
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing() && !progressDialog.isShowing()) {
                    progressDialog.show();
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        }
    }

    /**
     * 网络异常直接返回
     *
     * @param callback
     */
    private void sendNotNetCallback(final NetWorkCallbackListener callback, Context context) {
        ErrorEntity errorEntity = new ErrorEntity();
        errorEntity.setErrorcode(ResponseCode.CONTENT_NO_INTERNET);
        errorEntity.setErrormsg(context.getString(R.string.string_network_available));
        callback.onFailed(errorEntity.getErrorcode(), errorEntity.getErrormsg());
        //showErrorPwdDialog(context, errorEntity.getErrormsg());
    }

    /*************************新增接口请求*********************************/

    /**
     * 获取活动列表获取
     */
    @Override
    public void getActivitiesList(String name, Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new ActivityProtocol(name, callback).execute();
    }

    /**
     * 信用卡注册
     * 提交信用卡信息接口
     *
     * @param context
     * @param cardNo
     * @param cvv
     * @param expiryDate
     * @param action
     * @param callback
     */
    public void getRegisterByCreditCard(final Context context, String cardNo, String phone, String cvv, String expiryDate, String action, String isContinue, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new RegisterByCreditCardProtocol(cardNo, phone, cvv, expiryDate, action, isContinue, new NetWorkCallbackListener<RegisterCreditEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegisterCreditEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 忘记密码时
     *
     * @param context
     * @param callback
     */
    public void forgetPwdDeviceStatus(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ForgetPwdProtocol(new NetWorkCallbackListener<ForgetPasswordEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ForgetPasswordEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
        ;
    }


    /**
     * 查询账单列表接口
     */
    @Override
    public void getQueryBill(final Context context, boolean show, String queryType, String pageNo, String pageSize, String panId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new QueryBillProtocol(queryType, pageNo, pageSize, panId, new NetWorkCallbackListener<BillListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BillListEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void virtualCardCreditRegister(final Context context, VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new VirtualcardCreditCardRegisterProtocol(virtualcardRegisterVerifyEntity, new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    /**
     * 虚拟卡忘记密码
     *
     * @param context
     * @param virtualcardRegisterVerifyEntity
     * @param callback
     */
    public void virtualCardCreditForgetPwd(final Context context, VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new VirtualcardForgetPwdProtocol(virtualcardRegisterVerifyEntity, new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    /**
     * 虚拟卡忘记密码
     *
     * @param context
     * @param virtualcardRegisterVerifyEntity
     * @param callback
     */
    public void virtualCardForgetPwd(final Context context, VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new VirtualcardSmartAccountForgetPwdProtocol(virtualcardRegisterVerifyEntity, new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 获取主扫下单接口
     */
    @Override
    public void getMainScanOrder(final Context context, MainScanOrderRequestEntity requestEntity, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        //重要业务 暂时禁止手动取消
        showDialogNotCancel(context);
        new MainScanOrderProtocol(requestEntity, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 查询交易状态
     *
     * @param cardId
     * @param cpQrCode
     * @param txnId
     * @param callback
     */
    public void getQueryTradeStatus(final Context context, final String cardId, final String cpQrCode, final String txnId, boolean show, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
//        if (show) {
//            showDialog(context);
//        }
        new QueryTradeStatusProtocol(cardId, cpQrCode, txnId, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
//                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
//                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 检验账户支付密码
     *
     * @param payPwd
     * @param callback
     */
    @Override
    public void getCheckPassword(final Context context, String payPwd, NetWorkCallbackListener callback) {
        getCheckPassword(context, payPwd, "", callback);
    }


    public void getCheckPasswordWithEcoup(final Context context, String payPwd, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new CheckPasswordProtocol(payPwd, action, -1, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //方法调用顺序不能更换 造成activity泄露
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);


            }

            @Override
            public void onSuccess(BaseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);

            }
        }).execute();
    }

    public void getCheckPassword(final Context context, String payPwd, String account, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new CheckPasswordProtocol(payPwd, account, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    /**
     * 信用卡注册确认接口,也用于信用卡忘记密码
     *
     * @param context
     * @param passCode
     * @param walletId
     * @param cardId
     * @param action
     * @param callback
     */

    @Override
    public void getConfirmRegister(final Context context, String passCode, String walletId, String cardId, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new RegisterByCreditProtocol(passCode, walletId, cardId, action, new NetWorkCallbackListener<RegSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegSucEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //用于我的账户忘记密码
    public void sendSmartForgotPwd(final Context context, String passCode, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new SmartForgotPwdProtocol(passCode, action, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    /**
     * 通过我的账户注册绑定确认接口
     *
     * @param context
     * @param passcode
     * @param action
     * @param callback
     */
    public void registerBySmartAccount(final Context context, String passcode, String fioId, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new RegisterBySmartAccountProtocol(passcode, action, new NetWorkCallbackListener<RegSucBySmartEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegSucBySmartEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 修改账号支付密码
     *
     * @param context
     * @param payPwd
     * @param callback
     */

    public void getModifyPassword(final Context context, String payPwd, String oldword, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ModifyPasswordProtocol(payPwd, oldword, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 获取银行卡列表
     *
     * @param context
     * @param pageNum
     * @param pageSize
     * @param logonUserId
     * @param callback
     */
    @Override
    public void getCardList(final Context context, String pageNum, String pageSize, String logonUserId, final NetWorkCallbackListener callback) {
        getCardList(context, false, pageNum, pageSize, logonUserId, callback);
    }

    public void getCardList(final Context context, final boolean isShow, String pageNum, String pageSize, String logonUserId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        if (isShow) {
            showDialogNotCancel(context);
        }
        new CardListProtocol(logonUserId, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void getCardListWithNoDialog(final Context context, final boolean isShow, String pageNum, String pageSize, String logonUserId, final NetWorkCallbackListener callback) {
        new CardListProtocol(logonUserId, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onSuccess(CardsEntity response) {
                callback.onSuccess(response);
            }
        }).execute();
    }

    /**
     * 退出登录
     */
    @Override
    public void logout() {
        if (!checkNetAvailable()) {
            return;
        }
        new LogoutProtocol().execute();
    }

    @Override
    public void topUpEvent(final Context context, String action, String trxAmount, String orderNo, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new TopUpAndWithDrawProtocol(action, trxAmount, orderNo, new NetWorkCallbackListener<TopUpResponseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(TopUpResponseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    @Override
    public void getSmartAccount(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new SmartAccountCheckListProtocol(new NetWorkCallbackListener<SmartAccountListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(SmartAccountListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void updateSmartAccountInfoSendOtp(final Context context, String action, final NetWorkCallbackListener dataCallback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(dataCallback, context);
            return;
        }
        showDialog(context);
        new UpdateSmartAccountInfoSendOtpProtocol(action, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, dataCallback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, dataCallback, response);
            }
        }).execute();
    }

    @Override
    public void verifySmartAccountInfoOtp(final Context context, String action, String verifyCode, final NetWorkCallbackListener dataCallback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(dataCallback, context);
            return;
        }
        showDialogNotCancel(context);
        new VerifySmartAccountInfoSendOtpProtocol(action, verifyCode, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, dataCallback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                dismissDialog();
                onSuccessCallBack(context, dataCallback, response);
            }
        }).execute();
    }

    /**
     * 转账的时候显示的账户余额
     *
     * @param context
     * @param dataCallback
     */
    @Override
    public void getAccountBalanceInfo(final Context context, final NetWorkCallbackListener dataCallback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(dataCallback, context);
            return;
        }
        showDialogNotCancel(context);
        new TransferQueryBalanceProtocol(new NetWorkCallbackListener<AccountBalanceEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, dataCallback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(AccountBalanceEntity response) {
                dismissDialog();
                onSuccessCallBack(context, dataCallback, response);
            }
        }).execute();
    }


    private void onFailedCallBack(Context context, NetWorkCallbackListener dataCallback, String errorCode, String errorMsg) {
        try {
            Activity activity = (Activity) context;
            if (!activity.isFinishing()) {
                dataCallback.onFailed(errorCode, errorMsg);
            }
        } catch (Exception e) {
            LogUtils.e(TAG, e.getMessage());
        }
    }

    private void onSuccessCallBack(Context context, NetWorkCallbackListener dataCallback, Object response) {
        if (context == null) {
            dataCallback.onSuccess(response);
            return;
        }
        try {
            Activity activity = (Activity) context;
            if (!activity.isFinishing()) {
                dataCallback.onSuccess(response);
            }
        } catch (Exception e) {
            LogUtils.e(TAG, e.getMessage());
        }
    }

    @Override
    public void getBankList(final Context context, String currency, String name, String code, final NetWorkCallbackListener dataCallback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(dataCallback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetBankListProtocol(currency, name, code, new NetWorkCallbackListener<TransferListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, dataCallback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(TransferListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, dataCallback, response);
            }
        }).execute();
    }

    /**
     * 解绑银行卡
     *
     * @param context
     * @param cardId
     * @param newDefaultCardId
     * @param callback
     */

    public void getUnbindCard(final Context context, String cardId, String newDefaultCardId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new UnlinkCardProtocol(cardId, newDefaultCardId, new NetWorkCallbackListener<UnlinkCardEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(UnlinkCardEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //查询我的账户是否绑定
    public void checkSmartAccountBind(final Context context, final boolean show, final NetWorkCallbackListener callback) {
        if (show && !checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
//        if (show) {
//            showDialogNotCancel(context);
//        }
        new CheckSmartAccountBindProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
//                if (show) {
//                    dismissDialog();
//                }
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
//                if (show) {
//                    dismissDialog();
//                }
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    //查询我的账户是否绑定
    @Override
    public void checkSmartAccountNewBind(final Context context, final boolean show, final NetWorkCallbackListener callback) {
        if (show && !checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        if (show) {
            showDialogNotCancel(context);
        }
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (show) {
                    dismissDialog();
                }
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
                if (show) {
                    dismissDialog();
                }
            }
        }).execute();
    }

    public void checkStaticCodeStatus(final Context context, final boolean show, final NetWorkCallbackListener callback) {
        if (show && !checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        if (show) {
            showDialogNotCancel(context);
        }
        new CheckStaticCodeProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (show) {
                    dismissDialog();
                }
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
                if (show) {
                    dismissDialog();
                }
            }
        }).execute();
    }


    //转账前置检查
    public void checkTransferPre(final Context context, boolean show, TransferPreCheckReq req, final NetWorkCallbackListener callback) {
        if (show && !checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        if (show) {
            showDialogNotCancel(context);
        }
        new TransferPreCheckProtocol(req, new NetWorkCallbackListener<TransferPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferPreCheckEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    //打开红包
    public void openRedPacket(final Context context, String srcRefNo, final NetWorkCallbackListener callback,boolean isShowDialog) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        if(isShowDialog){
            showDialogNotCancel(context);
        }

        new TurnOnRedPacketProtocol(srcRefNo, new NetWorkCallbackListener<TurnOnRedPackEtntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if(isShowDialog) {
                    dismissDialog();
                }
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TurnOnRedPackEtntity response) {
                if(isShowDialog) {
                    dismissDialog();
                }
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    //获取登陆验证码
    public void getLoginVerifyCode(final Context context, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new BankLoginVerifyCodeProtocol(action, new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    //获取我的账户信息
    public void getSmartAccountInfo(final Context context, final NetWorkCallbackListener<MySmartAccountEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        })).execute();
    }

    //修改我的账户信息
    public void updateSmartAccountInfo(final Context context, String action, String relevanceAccNo, String limitPay, String addedmethod, String autotopupamt, final NetWorkCallbackListener<BaseEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        //重要操作不允許用戶取消加載框
        showDialogNotCancel(context);
        new UpdateSmartAccountInfoProtocol(action, relevanceAccNo, limitPay, addedmethod, autotopupamt, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    //修改我的账户信息
    public void updateSmartAccountPrimaryAccount(Context context, String relevanceAccNo, final NetWorkCallbackListener<BaseEntity> callback) {
        updateSmartAccountInfo(context, SmartAccountConst.UPDATE_SMARTACCOUNT_PRIMARY_ACCOUNT, relevanceAccNo, null, null, null, callback);
    }

    //修改每日限额
    public void updateSmartAccountDailyLimit(Context context, String dailyLimit, final NetWorkCallbackListener<BaseEntity> callback) {
        updateSmartAccountInfo(context, SmartAccountConst.UPDATE_SMARTACCOUNT_TOP_UP, null, dailyLimit, null, null, callback);
    }

    //修改我的账户充值方式
    public void updateSmartAccountTopUp(Context context, String topUpMethod, String topUpAmount, final NetWorkCallbackListener<BaseEntity> callback) {
        updateSmartAccountInfo(context, SmartAccountConst.UPDATE_SMARTACCOUNT_TOP_UP, null, null, topUpMethod, topUpAmount, callback);
    }


    public void updateSmartAccountActive(Context context, final NetWorkCallbackListener<BaseEntity> callback) {
        updateSmartAccountInfo(context, SmartAccountConst.UPDATE_SMARTACCOUNT_ACTIVATE_ACCOUNT, null, null, null, null, callback);
    }


    //FIO获取asKey
    public void fioInitCin(final Context context, String walletId, String regType, final NetWorkCallbackListener<FioInitEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new FioInitCinProtocol(walletId, regType, new NetWorkCallbackListener<FioInitEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioInitEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //FIO 登记发起请求
    public void fioRegistrationReq(final Context context, String walletId, String param, String regType, final NetWorkCallbackListener<FioRegRequestEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new FioRegRequestProtocol(walletId, param, regType, new NetWorkCallbackListener<FioRegRequestEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioRegRequestEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //FIO 登记SDK返回结果处理

    public void fioRegistrationResp(final Context context, String walletId, String param, String regType, final NetWorkCallbackListener<FioRegResponseEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new FioRegResponseProtocol(walletId, param, regType, new NetWorkCallbackListener<FioRegResponseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioRegResponseEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    //校验客户端fioid
    public void fioCheckValidId(final Context context, String fioId, final NetWorkCallbackListener<FioCheckValidEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new FioAuthIdProtocol(fioId, new NetWorkCallbackListener<FioCheckValidEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioCheckValidEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //客户端调用Authtication 接口验证用户指纹，之后SDK返回的base64字符串
    public void fioVerifyOtp(final Context context, String fioParam, boolean isEcouponAction, final NetWorkCallbackListener<FioVerifyOtpEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new FioAuthOtpProtocol(fioParam, isEcouponAction, new NetWorkCallbackListener<FioVerifyOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioVerifyOtpEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void parserTransferCode(final Context context, String qrCode, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ParserTransferProtocol(qrCode, new NetWorkCallbackListener<ParserQrcodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ParserQrcodeEntity response) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }


    @Override
    public void transferSummary(final Context context, String pgSz, String nxtRecKey, String frTxnDt, String toTxnDt, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new TransferSummaryProtocol(pgSz, nxtRecKey, frTxnDt, toTxnDt, new NetWorkCallbackListener<TransferBillEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(TransferBillEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    @Override
    public void getnTxnHistEnquirySmartCard(final Context context, String innexttstmp, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
//        showDialogNotCancel(context);
        // showDialog(context);
        new QueryEnquirySmartCardBillProtocol(innexttstmp, new NetWorkCallbackListener<SmartAccountBillListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
//                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(SmartAccountBillListEntity response) {
//                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void getTnTxnHistEnquiryCredit(final Context context, String mPanId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ActionTxnHistEnquiryCreditBillProtocol(mPanId, new NetWorkCallbackListener<CreditCardBillEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CreditCardBillEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    //fps 行内绑定记录
    public void fpsQueryBankRecords(final Context context, final NetWorkCallbackListener<FpsBankRecordsEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new FpsQueryBankRecords(new NetWorkCallbackListener<FpsBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsBankRecordsEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //fps 行外绑定记录
    public void fpsQueryBankOtherRecords(final Context context, String accountId, String accountIdType, final NetWorkCallbackListener<FpsOtherBankRecordsEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new FpsQueryBankOtherRecords(accountId, accountIdType, new NetWorkCallbackListener<FpsOtherBankRecordsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsOtherBankRecordsEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //fps 预检查
    public void fpsPreCheck(final Context context, FpsPreCheckRequest request, final NetWorkCallbackListener<FpsPreCheckEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new FpsPreCheckProtocol(request, new NetWorkCallbackListener<FpsPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsPreCheckEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    //fps 预检查
    public void fpsPreCheckDel(final Context context, FpsPreCheckRequest request, String delType, final NetWorkCallbackListener<FpsPreCheckEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new FpsPreCheckProtocol(request, delType, new NetWorkCallbackListener<FpsPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsPreCheckEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void fpsConfirmAdd(Context context, FpsBankRecordBean bean, String fppRefNo, final NetWorkCallbackListener<FpsConfirmEntity> callback) {
        FpsConfirmRequest request = new FpsConfirmRequest();
        request.accountId = bean.getAccountId();
        request.accountIdType = bean.getAccountIdType();
        request.accountNo = bean.getAccountNo();
        request.action = FpsConst.FPS_ACTION_ADD;
        request.bankCode = "";
        request.defaultBank = bean.getDefaultBank();
        request.fppRefNo = fppRefNo;
        fpsConfirm(context, request, callback);
    }

    public void fpsConfirmDel(final Context context, FpsBankRecordBean bean, String fppRefNo, String bankCodes, String delType, final NetWorkCallbackListener<FpsConfirmEntity> callback) {
        FpsConfirmRequest request = new FpsConfirmRequest();
        request.accountId = bean.getAccountId();
        request.accountIdType = bean.getAccountIdType();
        request.accountNo = "";
        request.action = FpsConst.FPS_ACTION_DEL;
        request.bankCode = bankCodes;
        request.defaultBank = "";
        request.fppRefNo = fppRefNo;
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new FpsConfirmProtocol(request, delType, new NetWorkCallbackListener<FpsConfirmEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsConfirmEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }

    //fps 确认
    public void fpsConfirm(final Context context, FpsConfirmRequest request, final NetWorkCallbackListener<FpsConfirmEntity> callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new FpsConfirmProtocol(request, new NetWorkCallbackListener<FpsConfirmEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FpsConfirmEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * s2用户活体检测之后信息上传
     *
     * @param context
     */
    @Override
    public void sTwoRegisterCustomerInfo(final Context context, String type, RegisterCustomerInfo registerCustomerInfo, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new S2RegisterCustomerInfoProtocol(type, registerCustomerInfo, new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void sTwoRegisterSetPwd(final Context context, String pwdStr, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new S2RegisterSetPwdProtocol(pwdStr, new NetWorkCallbackListener<S2RegisterMobileEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);

            }

            @Override
            public void onSuccess(S2RegisterMobileEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void virtualCardRegisterSetPwd(final Context context, VirtualCardListEntity.VirtualCardListBean virtualCardListBeanIn, String walletId, String pwdStr, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new VirtualcardRegisterSetPwdProtocol(virtualCardListBeanIn, walletId, pwdStr, new NetWorkCallbackListener<VirtualCardSetPwdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(VirtualCardSetPwdEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    @Override
    public void getTnxIdForForgetPwd(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetTnxIdForForgetPwdProtocol(new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    @Override
    public void getTnxId(final Context context, String phoneNumber, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetTnxIdProtocol(RequestParams.REGISTER_STWO_ACCOUNT, phoneNumber, new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    @Override
    public void getTnxIdForBindAccount(final Context context, String phoneNumber, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetTnxIdProtocol(RequestParams.BIND_SMART_ACCOUNT, phoneNumber, new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    @Override
    public void getSmartLevel(final Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetSmartLevelProtocol(mobile, verifyCode, action, new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartLevelForForget(final Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetSmartLevelForForgetProtocol(mobile, verifyCode, action, new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    //验证信用卡注册验证码
    @Override
    public void checkBankRegisterVerifyCode(final Context context, String verifyCode, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new CheckBankRegisterVerifyCodeProtocol(verifyCode, action, new NetWorkCallbackListener<BankRegisterVerifyCodeCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankRegisterVerifyCodeCheckEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    //验证PA用户注册验证码
    @Override
    public void checkPAAccountVerifyCode(final Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new CheckPAAcountVerifyCodeProtocol(mobile, verifyCode, action, new NetWorkCallbackListener<PAAccountVerifyCodeCheckEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(PAAccountVerifyCodeCheckEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }


    @Override
    public void getSmartLevelForBind(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetSmartLevelForBindProtocol(new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (null != callback) {
                    onFailedCallBack(context, callback, errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                dismissDialog();
                if (null != callback) {
                    onSuccessCallBack(context, callback, response);
                }
            }
        }).execute();
    }

    public void deleteEddaInfo(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new DeleteEddaInfoProtocol(new NetWorkCallbackListener<RegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegEddaInfoEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();


    }

    @Override
    public void regEddaInfo(final Context context, String bankCode, String bankNo, String currency, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new RegEddaInfoProtocol(bankCode, bankNo, currency, new NetWorkCallbackListener<RegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegEddaInfoEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void getRegEddaInfo(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void sTwoTopUp(final Context context, String action, String txnAmt, String referenceNo, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new STwoTopUpAndWithDrawProtocol(action, txnAmt, referenceNo, new NetWorkCallbackListener<STwoTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(STwoTopUpEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void sTwoUpdateAccount(final Context context, String accNo, String password, String loginType, String action, String verifyCode, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new UpdateSmartAccountProtocol(accNo, password, loginType, action, verifyCode, new NetWorkCallbackListener<BankLoginResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BankLoginResultEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    @Override
    public void sTwoUpdateAccountInfo(final Context context, String primaryAc, String payLimit, String topupmethod, String autotopupamt, final NetWorkCallbackListener callback) {


        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new UpdateSmartAccountSubmitProtocol(primaryAc, payLimit, topupmethod, autotopupamt, new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(Object response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();


    }


    public void getInitBannerUrls(final NetWorkCallbackListener callback) {
        new InitBannerProtocol(new NetWorkCallbackListener<InitBannerEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(InitBannerEntity response) {
                dismissDialog();
                onSuccessCallBack(null, callback, response);
            }
        }).execute();
    }

    @Override
    public void checkIdvRequest(final Context context, String type, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new CheckIDvProtocol(type, new NetWorkCallbackListener<RegisterIDEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegisterIDEntity response) {
                dismissDialog();
                onSuccessCallBack(null, callback, response);
            }
        }).execute();
    }

    public void checkIdvNewRequest(final Context context, final NetWorkCallbackListener callback) {
        new CheckIDvNewProtocol(new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    public void checkIdForBnindRequest(final Context context, final NetWorkCallbackListener callback) {
        new CheckIDvForBindNewProtocol(new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                onSuccessCallBack(null, callback, response);
            }
        }).execute();
    }


    public void checkIdForForgetRequest(final Context context, final NetWorkCallbackListener callback) {
        new CheckIDvForForgetNewProtocol(new NetWorkCallbackListener<IdAuthIinfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(IdAuthIinfoEntity response) {
                onSuccessCallBack(null, callback, response);
            }
        }).execute();
    }


    @Override
    public void sThreeTopUpPrecheck(final Context context, String action, String trxAmount, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new TopUpPreCheckProtocol(action, trxAmount, new NetWorkCallbackListener<SThreeTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(SThreeTopUpEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }


    public void getPaymentTypeList(final Context context, final String qrcode, final String pageNum, final String pageSize, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialog(context);
        new GetPayTypeListProtocol(qrcode, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void getCustomTelRegion(final Context context, String version, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetCustomTelRegionProtocol(version, new NetWorkCallbackListener<AreaListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(AreaListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();


    }


    /**
     * 获取主扫验证积分换领信息接口
     *
     * @param context
     * @param mpQrUrlInfoResult
     * @param callback
     */
    public void getActionTrxGpInfo(final Context context, MpQrUrlInfoResult mpQrUrlInfoResult, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ActionTrxGpInfoProtocol(mpQrUrlInfoResult, new NetWorkCallbackListener<ActionTrxGpInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ActionTrxGpInfoEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    /**
     * 主扫支付下单使用积分接口
     *
     * @param context
     * @param requestEntity
     * @param callback
     */
    public void actionMpqrPaymentWithPoint(final Context context, MainScanOrderRequestEntity requestEntity, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new MainScanOrderWithPointProtocol(requestEntity, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 被扫查询积分信息
     *
     * @param context
     * @param callback
     */
    public void getSweptCodePoints(final Context context, String tnxId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetSweptCodePointsProtocol(tnxId,GetSweptCodePointsProtocol.REDEEMTYPE_BACK_SCAN, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();


    }


    /**
     * 被扫进行积分抵扣
     *
     * @param context
     * @param callback
     */
    public void qrcodeWithPoints(final Context context, String tnxId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new QrcodeWithPointsProtocol(tnxId, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }


    /**
     * 数据统计
     */
    public void getDatacollectProtocol(String action) {
        new DatacollectProtocol(action, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onSuccess(ContentEntity response) {
            }
        }).execute();
    }

    public void getCardList(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetCardListProtocol(new NetWorkCallbackListener<PaymentListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PaymentListEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }

    /**
     * 查询某张信用卡所剩积分
     *
     * @param context
     * @param cardId
     * @param callback
     */
    public void getGradeForCreditCard(final Context context, String cardId, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ActionQueryGpInfoProtocol(cardId, new NetWorkCallbackListener<CreditCardGradeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CreditCardGradeEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }


    /**
     * 获取用户的信用卡列表
     *
     * @param context
     * @param callback
     */
    public void getCreditCardList(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
//        showDialogNotCancel(context);
        new ActionCreditListProtocol(new NetWorkCallbackListener<CreditCardEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
//                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CreditCardEntity response) {
//                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();

    }

    public void getTransferInviteRecord(final Context context, boolean isShow, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        new GetTransferInviteRecordProtocol(new NetWorkCallbackListener<InviteRecordListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(InviteRecordListEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void getTransferInviteInfo(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new GetTransferInviteInfoProtocol(new NetWorkCallbackListener<RegisterInviteInfo>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegisterInviteInfo response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    public void addTransferInviteRecord(final Context context, String incode, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new AddTransferInviteRecordProtocol(incode, new NetWorkCallbackListener<RegisterInviteStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(RegisterInviteStatusEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * 获取sdk errorcode
     *
     * @param context
     * @param callback
     */
    public void getSDKErrMsgNew(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            //sendNotNetCallback(callback, context);
            return;
        }
        new GetSDKErrMsgProtocol(new NetWorkCallbackListener<NewErrorCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(NewErrorCodeEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    public void isShowInviteUI(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ShowInviteUIProtocol(new NetWorkCallbackListener<ShowInviteCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ShowInviteCodeEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    public void smilingFaceSubmint(final Context context, String action, String pageId, String desc, final NetWorkCallbackListener callback) {
        new SmilingFaceProtocol(action, pageId, desc, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }

    public void getVirtualCardUrl(final Context context, final NetWorkCallbackListener callback) {
        if (!checkNetAvailable()) {
            sendNotNetCallback(callback, context);
            return;
        }
        showDialogNotCancel(context);
        new ApplyVirCardProtocol(new NetWorkCallbackListener<ApplyVirUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                onFailedCallBack(context, callback, errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ApplyVirUrlEntity response) {
                dismissDialog();
                onSuccessCallBack(context, callback, response);
            }
        }).execute();
    }


    /**
     * idv 跳转之前 校验证书拉取
     *
     * @param context
     * @param callback
     */
    public void loadIdvCerts(final Activity context, final NetWorkCallbackListener callback) {
        if (BuildConfig.DOWNLOAD_CER_SERVER) {
            CertsDownloadManager.getInstance().loadCertificates(context, true, new CertsDownloadManager.OnCertDownLoadListener() {
                @Override
                public void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates) {
                    if (callback != null) {
                        if (isSuccess) {
                            if (certificates != null && certificates.length > 0) {
                                //证书配置成功之后需要配置给Idv sdk
                                IdvSdkManager.getInstance().init(certificates);
                                callback.onSuccess(certificates);
                            } else {
                                callback.onFailed("", context.getString(R.string.string_network_available));
                            }
                        } else {
                            callback.onFailed("", context.getString(R.string.string_network_available));
                        }
                    }
                }
            });
//            if (ProjectApp.getCertificates() != null) {
//                //如果内存中已经保存 不需要再次拉取
//                LogUtils.i(TAG, "Certificates EXIT");
//                if (callback != null) {
//                    callback.onSuccess(null);
//                }
//                return;
//            }
//            if (!checkNetAvailable()) {
//                LogUtils.i(TAG, "Certificates NOT EXIT NETWORK ERROR");
//                if (callback != null) {
//                    callback.onSuccess(null);
//                }
//                return;
//            }
//            showDialogNotCancel(context);
//            AndroidUtils.loadIdvCerts(context, new NetWorkCallbackListener<Object>() {
//                @Override
//                public void onFailed(String errorCode, String errorMsg) {
//                    dismissDialog();
//                    if (callback != null) {
//                        callback.onSuccess(null);
//                    }
//                }
//
//                @Override
//                public void onSuccess(Object response) {
//                    dismissDialog();
//                    if (callback != null) {
//                        callback.onSuccess(null);
//                    }
//                }
//            });
        } else {
            if (callback != null) {
                callback.onSuccess(null);
            }
        }

    }

}