package cn.swiftpass.wallet.intl.utils;


import java.util.Comparator;

import cn.swiftpass.wallet.intl.entity.SortEntity;


public class PinyinComparator implements Comparator<SortEntity> {

    public int compare(SortEntity o1, SortEntity o2) {
        if (o1.getLetters().equals("@") || o2.getLetters().equals("#")) {
            return -1;
        }
//        else if (o1.getLetters().equals(Constants.TOP_HEADER_CHAR) || o2.getLetters().equals(Constants.TOP_HEADER_CHAR)) {
//            return 1;
//        }
        else if (o1.getLetters().equals("#") || o2.getLetters().equals("@")) {
            return 1;
        } else {
            return o1.getLetters().compareTo(o2.getLetters());
        }
    }

}
