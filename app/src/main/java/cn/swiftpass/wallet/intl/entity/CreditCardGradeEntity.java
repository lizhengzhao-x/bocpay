package cn.swiftpass.wallet.intl.entity;


import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardGradeEntity extends BaseEntity {


    private String constantLandGp;
    private String creditCardGp;
    //智能名称
    private String smaName;
    //总积分
    private String sumGp;
    //积分数据
    private String smaGp;
    private String yearHolding;
    private List<SmaGpInfoBean> ccGpInfo;
    private List<SmaGpInfoBean> smaGpList;

    public String getConstantLandGp() {
        return constantLandGp;
    }

    public void setConstantLandGp(String constantLandGp) {
        this.constantLandGp = constantLandGp;
    }

    public String getCreditCardGp() {
        return creditCardGp;
    }

    public void setCreditCardGp(String creditCardGp) {
        this.creditCardGp = creditCardGp;
    }

    public String getSmaName() {
        return smaName;
    }

    public void setSmaName(String smaName) {
        this.smaName = smaName;
    }

    public String getSmaGp() {
        return smaGp;
    }

    public void setSmaGp(String smaGp) {
        this.smaGp = smaGp;
    }

    public List<SmaGpInfoBean> getSmaGpList() {
        return smaGpList;
    }

    public void setSmaGpList(List<SmaGpInfoBean> smaGpList) {
        this.smaGpList = smaGpList;
    }

    public String getSumGp() {
        return sumGp;
    }

    public void setSumGp(String sumGp) {
        this.sumGp = sumGp;
    }

    public String getYearHolding() {
        return yearHolding;
    }

    public void setYearHolding(String yearHolding) {
        this.yearHolding = yearHolding;
    }

    public List<SmaGpInfoBean> getCcGpInfo() {
        return ccGpInfo;
    }

    public void setCcGpInfo(List<SmaGpInfoBean> ccGpInfo) {
        this.ccGpInfo = ccGpInfo;
    }


}
