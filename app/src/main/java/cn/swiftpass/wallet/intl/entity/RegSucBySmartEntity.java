package cn.swiftpass.wallet.intl.entity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 确认登录账号返回对象
 */

public class RegSucBySmartEntity extends RegSucEntity {

    String smartAccount; //通过我的账户


    public String getSmartAccount() {
        return smartAccount;
    }

    public void setSmartAccount(String smartAccount) {
        this.smartAccount = smartAccount;
    }


}
