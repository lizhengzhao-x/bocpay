package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.view.UnOpenLishiListActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

public class UnGetLishiTitleHolder extends RecyclerView.ViewHolder {
    private final Activity activity;
    @BindView(R.id.tv_get_lishi_un_open_more)
    LinearLayout tvGetLishiUnOpenMore;
    @BindView(R.id.tv_title_un_get_lishi)
    TextView tvTitleUnGetLishi;

    public UnGetLishiTitleHolder(Activity activity, @NonNull View itemView) {
        super(itemView);
        this.activity = activity;
        ButterKnife.bind(this, itemView);
        tvTitleUnGetLishi.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
        tvTitleUnGetLishi.getPaint().setStrokeWidth(0.7f);
    }

    public void setData(GetLiShiHomeEntity homeEntity) {

        if (homeEntity != null && null != homeEntity.getReceivedRecordsTurnOff()
                && homeEntity.getReceivedRecordsTurnOff().size() > 0) {
            tvGetLishiUnOpenMore.setVisibility(View.VISIBLE);
        } else {
            tvGetLishiUnOpenMore.setVisibility(View.GONE);
        }

        if (homeEntity != null && 0 == homeEntity.getReceivedRecordsTurnOffHasMore()) {
            tvGetLishiUnOpenMore.setVisibility(View.GONE);
        } else {
            tvGetLishiUnOpenMore.setVisibility(View.VISIBLE);
        }

        tvGetLishiUnOpenMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                ActivitySkipUtil.startAnotherActivity(activity, UnOpenLishiListActivity.class);
            }
        });


    }
}
