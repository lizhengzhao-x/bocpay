package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import cn.swiftpass.wallet.intl.R;

/**
 * Created by ZhangXinchao on 2019/7/16.
 * 代金券不同金额
 */
public class VouchersChildView extends LinearLayout {

    private Context mContext;
    private TextView ecoupon_money;
    private TextView ecoupon_count;
    private CardView rootLinView;


    public VouchersChildView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }


    public VouchersChildView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    public VouchersChildView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, defStyle);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        LayoutInflater.from(mContext).inflate(R.layout.voucher_child_item, this);
        ecoupon_money = (TextView) findViewById(R.id.id_voucher_money);
        ecoupon_count = (TextView) findViewById(R.id.id_voucher_count);
        rootLinView = findViewById(R.id.id_linear_view);
    }


    public void initText(String moneyTxt, String countTxt) {
        ecoupon_money.setText(moneyTxt);
        ecoupon_count.setText(countTxt);
    }


    public void setSel(boolean sel) {
        if (sel) {
            rootLinView.setCardBackgroundColor(getResources().getColor(R.color.app_yellow_light));
            ecoupon_count.setBackgroundResource(R.drawable.evoucher_bottom_bac_sel);
            rootLinView.setUseCompatPadding(false);
            rootLinView.setCardElevation(0);
            //#EDC269
        } else {
            rootLinView.setCardBackgroundColor(getResources().getColor(R.color.app_white));
            ecoupon_count.setBackgroundResource(R.drawable.evoucher_bottom_bac);
            rootLinView.setUseCompatPadding(true);
            rootLinView.setCardElevation(5);
        }
    }
}
