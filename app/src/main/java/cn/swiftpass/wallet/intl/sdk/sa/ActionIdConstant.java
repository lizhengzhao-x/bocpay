package cn.swiftpass.wallet.intl.sdk.sa;

/**
 * Created by ZhangXinchao on 2019/12/6.
 */
public class ActionIdConstant {


    /**
     * 最新消息 -分享
     */
    public final static String BUT_NEWS_SHARE = "but_news_share";


    /**
     * 转回余额-查詢餘額(眼睛)
     */
    public final static String BUT_REVERSAL_BALANCEEYE = "but_reversal_balanceEye";


    /**
     * 转账-账户转账-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_ACCOUNTNO_BALANCEEYE = "transfer_acNo_balanceEye";


    /**
     * 跨境转账-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_CROSSBORDER_BALANCEEYE = "but_transfer_cb_balanceEye";


    /**
     * 派利是-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_REDPACKET_BALANCEEYE = "but_transfer_rP_balanceEye";


    /**
     * 转账首页-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_BALANCEEYE = "but_transfer_balanceEye";


    /**
     * 电邮转账-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_BOCPAY_BALANCEEYE = "but_transfer_bocpay_balanceEye";


    /**
     * FPS转账-查詢餘額(眼睛)
     */
    public final static String BUT_TRANSFER_FPSID_BALANCEEYE = "but_transfer_fpsId_balanceEye";


    /**
     * 我的账户-賬戶總積分
     */
    public final static String BUT_ACCOUNT_TOTALPOINT = "but_account_totalPoint";

    /**
     * 最新消息-信用卡奖赏
     */
    public final static String BUT_NEWS_CREDIT_REWARD = "but_news_creditReward";

    /**
     * 侧边栏-信用卡奖赏
     */
    public final static String BUT_MENU_CREDIT_REWARD = "but_menu_creditReward";


    /**
     * 我的账户-查詢餘額(眼睛)
     */
    public final static String BUT_ACCOUNT_BALANCEEYE = "but_account_balanceEye";


    /**
     * 首页 查詢餘額/積分(眼睛）
     */
    public final static String BUT_HOME_EYE = "but_home_eye";

    /**
     * 菜单栏 交易记录
     */
    public final static String BUT_MENU_RECORD = "but_menu_record";
    /**
     * 信用卡奖赏-我的任务
     */
    public final static String BUT_CREDIT_REWARD_TASK = "but_creditReward_task";
    /**
     * 信用卡奖赏-奖赏记录
     */
    public final static String BUT_CREDIT_REWARD_RECORD = "but_creditReward_record";
    /**
     * 首页最新消息更多
     */
    public final static String BUT_FRONT_PAGE_REP = "but_front_page_rep";
    /**
     * tab栏位 mgm邀请
     */
    public final static String BUT_REFER_FRIEND = "but_refer_friend";
    /**
     * mgm 复制到粘贴板
     */
    public final static String BUT_REFER_FRIEND_CP = "but_refer_friend_cp";
    /**
     * MGM分享
     */
    public final static String BUT_REFER_FRIEND_INVI = "but_refer_friend_invi";
    public final static String BUT_E_VOU_PAGE_TAL_GPT = "but_e_vou_page_tal_gpt";
    /**
     * 电子券右上角记录
     */
    public final static String BUT_E_VOU_PAGE_RECORD = "but_e_vou_page_record";
    /**
     * 主扫积分抵扣按钮
     */
    public final static String BUT_SAC_PAY_POINT = "but_sac_pay_point";
    /**
     * 被扫积分抵扣
     */
    public final static String BUT_QR_PAY_POINT = "but_qr_pay_point";
    /**
     * mgm右上角输入code
     */
    public final static String BUT_REG_ENTER_CODE = "but_reg_enter_code";
    /**
     * mgm输入code 同意条款以及细则
     */
    public final static String BUT_REGISTRATION_CNF = "but_registration_cnf";
    /**
     * 电子券入口
     */
    public final static String BUT_FRONT_PAGE_E_VOU = "but_front_page_e_vou";


    /**
     * 主頁
     */
    public final static String BUT_TAB_HOME = "but_tab_home";

    /**
     * 一掃即付
     */
    public final static String BUT_TAB_SCAN = "but_tab_scan";

    /**
     * 付款碼
     */
    public final static String BUT_TAB_PAYMENTCODE = "but_tab_paymentCode";

    /**
     * 收款
     */
    public final static String BUT_TAB_COLLECT = "but_tab_collect";

    /**
     * 轉賬
     */
    public final static String BUT_TAB_TRANSFER = "but_tab_transfer";

    /**
     * 優計劃
     */
    public final static String BUT_HOME_PAGE_UPLAN = "but_home_page_uplan";

    /**
     * 優計劃  下次不再顯示
     */
    public final static String BUT_UPLAN_NONEXTSHOW = "but_uplan_noNextShow";

    /**
     * 電子優惠券 使用說明
     */
    public final static String BUT_UPLAN_HELP = "but_uplan_help";

    /**
     * 電子優惠券使用優惠券及付款碼支付
     */
    public final static String BUT_UPLAN_USECODE = "but_uplan_useCode";

}
