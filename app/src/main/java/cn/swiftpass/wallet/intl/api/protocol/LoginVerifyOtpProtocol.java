package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.SignUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.NormalLoginSucEntity;

public class LoginVerifyOtpProtocol extends BaseProtocol {
    public static final String TAG = LoginVerifyOtpProtocol.class.getSimpleName();
    String mMobile;
    String mAction;
    String mCardId;
    String mOtpCode;
    boolean mIsContinue;

    public LoginVerifyOtpProtocol(String mobile, String cardId, String otpCode, String action, boolean isContinue, NetWorkCallbackListener dataCallback) {
        this.mMobile = mobile;
        this.mAction = action;
        this.mCardId = cardId;
        this.mIsContinue = isContinue;
        this.mOtpCode = otpCode;
        this.mDataCallback = dataCallback;
        mUrl = "api/login/verifyOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.CARD_ID, mCardId);
        mBodyParams.put(RequestParams.ISCONTINUE, mIsContinue ? "Y" : "");
        mBodyParams.put(RequestParams.OTPCODE, mOtpCode);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        try {
            NormalLoginSucEntity entity = mGson.fromJson(data, mDataCallback.mType);
            if (null != entity && !TextUtils.isEmpty(entity.getServerPubKey())) {
                HttpCoreKeyManager.getInstance().saveServerPublicKey(entity.getServerPubKey());
                return SignUtils.verify(sign, data, entity.getServerPubKey());
            } else {
                return super.verifySign(sign, data);
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
