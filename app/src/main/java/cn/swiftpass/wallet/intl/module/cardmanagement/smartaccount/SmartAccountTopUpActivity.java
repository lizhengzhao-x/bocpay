package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.TopUpOrderDetailEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountTopUpContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.SmartAccountTopUpPresenter;
import cn.swiftpass.wallet.intl.module.topup.TopUpOrderDetailDialogFragment;
import cn.swiftpass.wallet.intl.module.topup.TopUpSuccessActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

/**
 * @author Created  on 2018/9/3.
 */

public class SmartAccountTopUpActivity extends BaseCompatActivity<SmartAccountTopUpContract.Presenter> implements SmartAccountTopUpContract.View {

    @BindView(R.id.etwd_otp_code)
    EditTextWithDel idEditMoney;
    @BindView(R.id.id_bottom_line)
    View idBottomLine;
    @BindView(R.id.tv_nextPage)
    TextView tvNextPage;
    @BindView(R.id.id_currency)
    TextView idCurrency;
    private String currency;

    private String mUserLevel;
    private STwoTopUpEntity mStwoTopUpEntity;
    private SThreeTopUpEntity mSthreeTopUpEntity;
    private TopUpOrderDetailDialogFragment orderDetailDialogFragment;
    private String paymentMethod;
    private String accType;


    @Override
    protected SmartAccountTopUpContract.Presenter createPresenter() {
        return new SmartAccountTopUpPresenter();
    }

    @Override
    public void init() {
        EventBus.getDefault().register(this);
        setToolBarTitle(R.string.SMB1_2_5);
        if (getIntent()!=null){
            paymentMethod = getIntent().getStringExtra(Constants.PAYMENT_METHOD);
            accType= getIntent().getStringExtra(Constants.ACCTYPE);
        }
        mUserLevel = getIntent().getExtras().getString(Constants.LAVEL);
        idEditMoney.setLineVisible(false);
        updateOkBackground(tvNextPage, false);
        currency = getIntent().getExtras().getString(Constants.CURRENCY);
        idCurrency.setText(currency);
        idEditMoney.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                dealWithExceedMaxMoney(idEditMoney.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        idEditMoney.getEditText().setTextSize(20);
        //两位小数过滤 8位长度限制
        idEditMoney.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(9)});
        AndroidUtils.showKeyboard(this, idEditMoney.getEditText());
    }

    private void dealWithExceedMaxMoney(String s) {
        if (s.endsWith(".")) {
            s = s.substring(0, s.length() - 1);
        }
        double maxValue = 0;
        try {
            maxValue = Double.valueOf(s);
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        updateOkBackground(tvNextPage, maxValue > 0);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_smart_account_topup;
    }



    @OnClick(R.id.tv_nextPage)
    public void onViewClicked() {
        String money = idEditMoney.getText().toString().trim();
        setProgressDialogAlpha(0);
        if (mUserLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            /**
             *s2用户充值 校验金额
             */
            mPresenter.preCheckTopUpWithSmartAccountTwo(money);
        } else if (mUserLevel.equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            /**
             * s3用户充值 预下单 生成订单号
             */
            mPresenter.preCheckTopUpWithSmartAccountThree(money);
        }
    }

    private void showCheckStand(String money) {
        //拉起收银台
        AndroidUtils.hideKeyboard(idEditMoney.getTextView());
        TopUpOrderDetailEntity topUpOrderDetailEntity = new TopUpOrderDetailEntity();
        topUpOrderDetailEntity.setMoney(money);
        topUpOrderDetailEntity.setAccType(accType);
        topUpOrderDetailEntity.setPaymentMethod(paymentMethod);
        topUpOrderDetailEntity.setCurrency(currency);
        showCheckStandDialog(topUpOrderDetailEntity);
    }



    /**
     * 根据订单结果显示收银台
     *
     * @param topUpOrderDetailEntity
     */
    private void showCheckStandDialog(TopUpOrderDetailEntity topUpOrderDetailEntity) {
        orderDetailDialogFragment = new TopUpOrderDetailDialogFragment();
        TopUpOrderDetailDialogFragment.OnDialogClickListener onDialogClickListener = new TopUpOrderDetailDialogFragment.OnDialogClickListener() {
            @Override
            public void onConfirmListener() {
                verifyPwdAction(topUpOrderDetailEntity.getMoney());
            }



            @Override
            public void onDisMissListener() {

            }
        };
        orderDetailDialogFragment.initParams(topUpOrderDetailEntity,  onDialogClickListener);
        orderDetailDialogFragment.show(getSupportFragmentManager(), "OrderDetailGradeDialogFragment");
    }


    /**
     * 充值支付
     */
    private void onTopUpEvent(String money) {
        //显示加载框
        setProgressDialogAlpha(80);
        if (mUserLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            mPresenter.topUpWithSmartAccountTwo(money,mStwoTopUpEntity.getScrRefNo());
        } else if (mUserLevel.equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            mPresenter.topUpWithSmartAccountThree(money, mSthreeTopUpEntity.getOrderNo());
        }
    }


    private void goTopUpSuccessPage(String moneyStr, String no) {
        if (orderDetailDialogFragment!=null){
            orderDetailDialogFragment.dismiss();
        }

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.PAYMENT_METHOD, paymentMethod);
        mHashMaps.put(Constants.TYPE, Constants.TYPE_TOPUP);
        mHashMaps.put(Constants.CURRENCY, currency);
        mHashMaps.put(Constants.MONEY, BigDecimalFormatUtils.forMatWithDigs(moneyStr, 2));
        mHashMaps.put(Constants.RECORD_NUMBER, no);
        mHashMaps.put(Constants.ACCTYPE, accType);
        ActivitySkipUtil.startAnotherActivity(SmartAccountTopUpActivity.this, TopUpSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }



    private void verifyPwdAction(String money) {

        VerifyPasswordCommonActivity.startActivityForActionResult(getActivity(), "D", new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                onTopUpEvent(money);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });


//        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new VerifyPasswordCommonActivity.OnPwdVerifyCallBack() {
//            @Override
//            public void onVerifySuccess() {
//                onTopUpEvent(money);
//            }
//
//            @Override
//            public void onVerifyFailed(String errorCode, String errorMsg) {
//            }
//
//            @Override
//            public void onVerifyCanceled() {
//
//            }
//
//        });

    }

    @Override
    public void preCheckTopUpWithSmartAccountTwoFail(String errorCode, String errorMsg) {
        if (orderDetailDialogFragment!=null){
            orderDetailDialogFragment.dismiss();
        }
        showErrorMsgDialog(SmartAccountTopUpActivity.this,errorMsg);
    }

    @Override
    public void preCheckTopUpWithSmartAccountTwoSuccess(STwoTopUpEntity response,String money) {
        mStwoTopUpEntity = response;
        showCheckStand(money);
    }

    @Override
    public void preCheckTopUpWithSmartAccountThreeFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountTopUpActivity.this,errorMsg);
    }

    @Override
    public void preCheckTopUpWithSmartAccountThreeSuccess(SThreeTopUpEntity response,String money) {
        mSthreeTopUpEntity = response;
        showCheckStand(money);
    }

    @Override
    public void topUpWithSmartAccountTwoFail(String errorCode, String errorMsg) {
        if (orderDetailDialogFragment!=null){
            orderDetailDialogFragment.dismiss();
        }
        showErrorMsgDialog(SmartAccountTopUpActivity.this,errorMsg);
    }

    @Override
    public void topUpWithSmartAccountTwoSuccess(STwoTopUpEntity response,String moneyStr) {
        goTopUpSuccessPage(moneyStr, response.getScrRefNo());
    }

    @Override
    public void topUpWithSmartAccountThreeFail(String errorCode, String errorMsg) {
        if (orderDetailDialogFragment!=null){
            orderDetailDialogFragment.dismiss();
        }
        showErrorMsgDialog(SmartAccountTopUpActivity.this,errorMsg);
    }

    @Override
    public void topUpWithSmartAccountThreeSuccess(TopUpResponseEntity response,String moneyStr) {
        goTopUpSuccessPage(moneyStr, response.getTrxNo());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
                finish();
        }
    }
}
