package cn.swiftpass.wallet.intl.module.register.model;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class BindNewCardEntity extends BaseEntity {
    /**
     * pan string
     * 卡号
     * panType
     * string
     * 卡类型 （1:creditCard 2:virtualCard）
     * panName
     * string
     * 卡名称
     * cardBin
     * string
     * 卡面id
     */
    public static final String TYPE_CARD_CREDITCARD = "1";
    public static final String TYPE_CARD_VIRTUALCARD = "2";

    private String pan;
    private String panType;
    private String panName;
    private String cardBin;
    private String panShowNum;
    private String cardType;
    private boolean isCheck;
    private boolean canSelect = true;//默认为可以选中
    private boolean bindStatus;

    public String getPanShowNum() {
        return panShowNum;
    }

    public void setPanShowNum(String panShowNum) {
        this.panShowNum = panShowNum;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getPanType() {
        return panType;
    }

    public void setPanType(String panType) {
        this.panType = panType;
    }

    public String getPanName() {
        return panName;
    }

    public void setPanName(String panName) {
        this.panName = panName;
    }

    public String getCardBin() {
        return cardBin;
    }

    public void setCardBin(String cardBin) {
        this.cardBin = cardBin;
    }

    public void toggle() {
        this.isCheck = !this.isCheck;
    }

    public boolean isCheck() {
        return isCheck;
    }

    public boolean isCanSelect() {
        return canSelect;
    }

    public void setCanSelect(boolean canSelect) {
        this.canSelect = canSelect;
    }

    public void setBindStatus(boolean bindStatus) {
        this.bindStatus = bindStatus;
    }

    public boolean isBindStatus() {
        return bindStatus;
    }
}
