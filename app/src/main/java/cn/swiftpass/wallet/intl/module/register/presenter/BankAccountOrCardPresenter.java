package cn.swiftpass.wallet.intl.module.register.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeLoginBankProtocol;
import cn.swiftpass.wallet.intl.api.protocol.BankLoginVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAcccountVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardSmartAccountForgetPwdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardSmartAccountRegisterProtocol;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.module.register.contract.BankAccountOrCardContact;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;

public class BankAccountOrCardPresenter extends BasePresenter<BankAccountOrCardContact.View> implements BankAccountOrCardContact.Presenter {
    @Override
    public void getLoginVerifyCode(String action) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BankLoginVerifyCodeProtocol(action, new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getLoginVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getLoginVerifyCodeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getUpdateAccountVerifyCode(String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new UpdateSmartAcccountVerifyCodeProtocol(action, new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUpdateAccountVerifyCodeFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUpdateAccountVerifyCodeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void virtualCardForgetPwd(VirtualcardRegisterVerifyEntity data) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualcardSmartAccountForgetPwdProtocol(data, new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardForgetPwdFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardForgetPwdSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void virtualCardSmartAccountRegister(VirtualcardRegisterVerifyEntity data) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualcardSmartAccountRegisterProtocol(data, new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardSmartAccountRegisterFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardSmartAccountRegisterSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void bankLogin(String accNo, String password, String action, String loginType, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new E2eeLoginBankProtocol(accNo, password, action, loginType, verifyCode, new NetWorkCallbackListener<BankLoginResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bankLoginFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginResultEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bankLoginSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void sTwoUpdateAccount(String accNo, String password, String loginType, String action, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new UpdateSmartAccountProtocol(accNo, password, loginType, action, verifyCode, new NetWorkCallbackListener<BankLoginResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sTwoUpdateAccountFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BankLoginResultEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().sTwoUpdateAccountSuccess(response);
                }
            }
        }).execute();
    }
}
