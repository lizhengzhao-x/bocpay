package cn.swiftpass.wallet.intl.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.module.transfer.entity.LocalContactCacheEntity;

/**
 * Created by ZhangXinchao on 2019/11/6.
 */
public class ContactLocalCacheUtils {


    private static final String RECENTLY_LIST_CACHE_NAME = "recently_cache";
    private static final String TRANSFER_CONTRACT_COLLECTION_LIST = "TRANSFER_CONTRACT_COLLECTION_LIST";

    /**
     * 获取本地通讯录缓存列表
     *
     * @param context
     * @return
     */
    public static List<String> getLocalCacheWithUserId(Context context, String userId) {
        if (context == null) return new ArrayList<>();
        Object cacheObj = ACache.get(context, RECENTLY_LIST_CACHE_NAME).getAsObject(TRANSFER_CONTRACT_COLLECTION_LIST);
        LocalContactCacheEntity localContactCacheEntity = null;
        if (cacheObj != null) {
            localContactCacheEntity = (LocalContactCacheEntity) cacheObj;
        } else {
            localContactCacheEntity = new LocalContactCacheEntity();
        }
        if (localContactCacheEntity.getmContacts().containsKey(userId)) {
            return localContactCacheEntity.getmContacts().get(userId);
        }
        return new ArrayList<>();
    }

    /**
     * 获取本地通讯录缓存 根据手机号来区分
     *
     * @param context
     * @return
     */
    public static boolean addContractToLocalCacheWithUserId(Context context, String contactID, String userId) {
        boolean isSuccess = true;
        try {
            Object cacheObj = ACache.get(context, RECENTLY_LIST_CACHE_NAME).getAsObject(TRANSFER_CONTRACT_COLLECTION_LIST);
            LocalContactCacheEntity localContactCacheEntity = null;
            if (cacheObj != null) {
                localContactCacheEntity = (LocalContactCacheEntity) cacheObj;
            } else {
                localContactCacheEntity = new LocalContactCacheEntity();
            }
            if (localContactCacheEntity.getmContacts().containsKey(userId)) {
                localContactCacheEntity.getmContacts().get(userId).add(0, contactID);
            } else {
                ArrayList<String> contacts = new ArrayList<>();
                contacts.add(contactID);
                localContactCacheEntity.getmContacts().put(userId, contacts);
            }
            ACache.get(context, RECENTLY_LIST_CACHE_NAME).put(TRANSFER_CONTRACT_COLLECTION_LIST, localContactCacheEntity);
            isSuccess = true;
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }

    /**
     * 删除本地通讯录中某一条记录
     *
     * @param context
     * @return
     */
    public static boolean removeContractToLocalCacheWithUserId(Context context, String contactID, String userId) {

        boolean isSuccess = true;
        try {
            Object cacheObj = ACache.get(context, RECENTLY_LIST_CACHE_NAME).getAsObject(TRANSFER_CONTRACT_COLLECTION_LIST);
            LocalContactCacheEntity localContactCacheEntity = null;
            if (cacheObj != null) {
                localContactCacheEntity = (LocalContactCacheEntity) cacheObj;
                List<String> contactEntities = localContactCacheEntity.getmContacts().get(userId);
                int containPosition = -1;
                for (int i = 0; i < contactEntities.size(); i++) {
                    if (contactEntities.get(i).equals(contactID)) {
                        containPosition = i;
                        break;
                    }
                }
                if (containPosition != -1) {
                    contactEntities.remove(containPosition);
                }
                ACache.get(context, RECENTLY_LIST_CACHE_NAME).put(TRANSFER_CONTRACT_COLLECTION_LIST, localContactCacheEntity);
                isSuccess = true;
            } else {
                isSuccess = false;
            }

        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return isSuccess;
    }
}
