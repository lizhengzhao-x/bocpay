package cn.swiftpass.wallet.intl.module.home.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.NewMsgEntity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterSelectListActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.notify.view.NotificationSettingTransparentActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardApplyActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

/**
 * 最新消息未登录 登录调用点击事件一样
 */
public class BannerUtils {

    public static final float BANNER_IMAGE_PROPORTION = 1.53225f;


    public static void clickBannerEvent(Activity activity, NewMsgEntity promotionBean) {
        if (promotionBean == null) {
            return;
        }
        if (activity != null) {
            if (promotionBean.isOpenExtBrowser()) {
                String str_title = "   ";
                String str_msg = "";
                if (!TextUtils.isEmpty(promotionBean.getOpenExtBrowserMsg())) {
                    str_msg = promotionBean.getOpenExtBrowserMsg();
                }
                String str_ok = activity.getString(R.string.dialog_right_btn_ok);
                String str_cancel = activity.getString(R.string.cancel);

                DialogUtils.showTipDialog(activity, str_title, str_msg, str_ok, str_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, int which) {
                        AndroidUtils.openUrl(activity, promotionBean.getUrl());
                        //用户要求 吊起外部浏览器之间 其他按钮不能响应 这里暂时做一个延迟关闭dialog的代码
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (dialog != null) {
                                    dialog.dismiss();
                                }
                            }
                        }, 500);
                    }
                }, null);
            } else if (promotionBean.isVirtualCard()) {
                getVirtualCardUrl(activity, promotionBean.getUrl(), promotionBean.getShareImageUrl());
            } else if (promotionBean.isCreditCardReward()) {
                DeepLinkUtils.goToAppointRewardRegisterActivity(activity, promotionBean.getBusinessParam());
            } else if (promotionBean.isCreditCardRewardList()) {
                ActivitySkipUtil.startAnotherActivity(activity, RewardRegisterSelectListActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (promotionBean.isNotification()) {
                //9月版 - 推广通知
                ActivitySkipUtil.startAnotherActivity(activity, NotificationSettingTransparentActivity.class);
            } else if (promotionBean.isCreditCardRewards()) {
                //9月版 - 信用卡奖赏
                if (CacheManagerInstance.getInstance().isLogin()) {
                    EventBus.getDefault().postSticky(new PagerEventEntity(PagerEventEntity.EVENT_KEY_SWITCH_PAGES_TO_CREDIT_CARD_REWARD, ""));
                    Intent intent = new Intent(activity, MainHomeActivity.class);
                    intent.addFlags(FLAG_ACTIVITY_SINGLE_TOP | FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(intent);
                } else {
                    ActivitySkipUtil.startAnotherActivity(activity, LoginActivity.class);
                }
            } else {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, promotionBean.getUrl());
                mHashMaps.put(Constants.DETAIL_TITLE, activity.getString(R.string.what_is_new));
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                mHashMaps.put(WebViewActivity.SHARE_CONTENT, promotionBean.getShareImageUrl());
                ActivitySkipUtil.startAnotherActivity(activity, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }

    private static void getVirtualCardUrl(Activity activity, String url, String shareImageUrl) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, url);
        mHashMaps.put(WebViewActivity.NEW_MSG, true);
        mHashMaps.put(WebViewActivity.SHARE_CONTENT, shareImageUrl);
        ActivitySkipUtil.startAnotherActivity(activity, VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }
}



