package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.EcouponsCallBack;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivityEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.EcouponsActivityItemClickListener;
import cn.swiftpass.wallet.intl.module.ecoupon.view.EcouponsActivityItemHolder;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.GlideApp;

import static cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponItemType.TYPE_EVOUCHER;
import static cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponItemType.TYPE_STATIC;
import static cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponItemType.TYPE_UPLAN;
import static cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponItemType.TYPE_WEBVIEW;

public class EcouponsActivitiesAdapter extends RecyclerView.Adapter {
    ArrayList<EcouponActivityEntity> data = new ArrayList<>();
    Activity context;

    EcouponsActivityItemClickListener onClickListener = new EcouponsActivityItemClickListener() {
        @Override
        public void onClick(EcouponActivityEntity entity) {

            if (ecouponsCallBack != null) {
                ecouponsCallBack.onItemClick(entity);
            }

        }
    };
    private EcouponsCallBack ecouponsCallBack;

    public EcouponsActivitiesAdapter(Activity context) {
        this.context = context;
    }

    public void setData(ArrayList<EcouponActivityEntity> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_ecoupon_activities, parent, false);
        return new EcouponsActivityItemHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof EcouponsActivityItemHolder) {
            if (data.get(position) != null) {

                EcouponActivityEntity activityEntity = data.get(position);
                EcouponsActivityItemHolder itemHolder = (EcouponsActivityItemHolder) holder;
                RequestOptions options = new RequestOptions()
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);

                String type = activityEntity.getBusinessType();
                int placeholderImage = R.mipmap.img_promotion_skeleton_ecoupon;
                itemHolder.textItemBtn.setVisibility(View.VISIBLE);
                itemHolder.textItemBtn.setTextColor(Color.parseColor("#FFFFFF"));
                if (TYPE_EVOUCHER.equals(type)) {
                    itemHolder.textItemBtn.setBackgroundResource(R.drawable.bg_btn_ecoupon_item);
                } else if (TYPE_UPLAN.equals(type)) {
                    itemHolder.textItemBtn.setBackgroundResource(R.drawable.bg_btn_uplan_item);
                    //需求 uplan字体颜色单独设置
                    itemHolder.textItemBtn.setTextColor(Color.parseColor("#F0512A"));
                } else if (TYPE_WEBVIEW.equals(type)) {
                    itemHolder.textItemBtn.setBackgroundResource(R.drawable.bg_btn_ecoupon_item);
                } else if (TYPE_STATIC.equals(type)) {
                    itemHolder.textItemBtn.setVisibility(View.INVISIBLE);
                }

                GlideApp.with(context)
                        .load(activityEntity.getItemPictureUrl())
                        .placeholder(placeholderImage)
                        .apply(options)
                        .into(itemHolder.imageItem);

                if (!TextUtils.isEmpty(activityEntity.getStatementContent())) {
                    itemHolder.statementItemBtn.setText(activityEntity.getStatementContent());
                    itemHolder.statementItemBtn.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
                    itemHolder.statementItemBtn.getPaint().setAntiAlias(true);//抗锯齿
                    itemHolder.statementItemBtn.setVisibility(View.VISIBLE);
                    itemHolder.statementItemBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.DETAIL_URL, activityEntity.getStatementUrl());
                            mHashMaps.put(Constants.DETAIL_TITLE, activityEntity.getStatementContent());
                            mHashMaps.put(WebViewActivity.NEW_MSG, false);
                            ActivitySkipUtil.startAnotherActivity(context, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                } else {
                    itemHolder.statementItemBtn.setVisibility(View.GONE);
                }

                itemHolder.textItemBtn.setText(activityEntity.getButtonDesc());
                if (!TYPE_STATIC.equals(type)) {
                    itemHolder.setOnItemClickListener(activityEntity, onClickListener);
                }
            }

        }


    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public void setEcouponsCallBack(EcouponsCallBack ecouponsCallBack) {
        this.ecouponsCallBack = ecouponsCallBack;
    }
}
