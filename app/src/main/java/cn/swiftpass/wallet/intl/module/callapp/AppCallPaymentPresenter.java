package cn.swiftpass.wallet.intl.module.callapp;

import static cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils.FPS_FLOW_ACTION;

import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.wallet.intl.api.protocol.ActionTrxGpInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ExternalSysParameterProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetPayTypeListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanApppCallAppOrderProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderWithPointAppCallAppProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ParserTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTradeStatusProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferPreCheckProtocol;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.AppCallAppOrderEntity;
import cn.swiftpass.wallet.intl.entity.ExternalSysParameterEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OCSPTool;
import okhttp3.OkHttpClient;

public class AppCallPaymentPresenter extends BasePresenter<AppCallAppPaymentContract.View> implements AppCallAppPaymentContract.Presenter {

    private static final String ACTION_PAY = "EXTPAY";
    private static final String TAG = "AppCallPaymentPresenter";
    private ArrayList<String> organizationList;

    @Override
    public void parserQrcode(String qrcode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ExternalSysParameterProtocol(qrcode, new NetWorkCallbackListener<ExternalSysParameterEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parserQrcodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ExternalSysParameterEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parserQrcodeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void parserFpsQrcode(String qrCode) {
        new ParserTransferProtocol(qrCode, FPS_FLOW_ACTION, organizationList, new NetWorkCallbackListener<ParserQrcodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parseTransferCodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ParserQrcodeEntity response) {
                if (getView() != null) {
                    transferPreCheck(response);
                }
            }
        }).execute();
    }

    private void transferPreCheck(ParserQrcodeEntity mFpsEntity) {
        final TransferPreCheckReq req = new TransferPreCheckReq();
        req.isQrcode = true;
        req.transferType = mFpsEntity.getTransferAccTp();
        req.bankType = TransferConst.TRANS_BANK_TYPE_FPS;
        req.tansferToAccount = mFpsEntity.getTransferAccNo();
        req.bankName = mFpsEntity.getTransferBankCode();
        req.transferAmount = mFpsEntity.getTransferAmount();
        new TransferPreCheckProtocol(req, FPS_FLOW_ACTION, new NetWorkCallbackListener<TransferPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parseTransferCodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferPreCheckEntity response) {

                if (getView() != null) {
                    getView().dismissDialog();
                    response.setPreCheckReq(req);
                    response.setTransferType(req.transferType);
                    getView().parseTransferCodeSuccess(response);
                }
            }
        }).execute();
    }


    /**
     * FPS OCSP认证流程 通过一个Get请求 进行证书验证
     *
     * @param fpsUrl
     */
    @Override
    public void verifyOCSPUrl(String fpsUrl) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
//        FpsParamsEntity fpsParamsEntity = new FpsParamsEntity();
//        fpsParamsEntity.setURL("https://fps.oepay.octopus-cards.com/ow_owallet_ws/rest/fps/topup/present?ref=7ce158c638464978b8ee9644f");
//        fpsParamsEntity.setCallback("https://app.octopus.com.hk/qrpayment");
//        OkHttpClient okHttpClient = getBuilder().
//                connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS).
//                readTimeout(READ_TIME_OUT, TimeUnit.SECONDS).
//                writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS).
//                build();
//        Request request = new Request.Builder()
//                .url(fpsUrl)
//                .addHeader("Connection", "close")
//                .build();
//        try {
//            okHttpClient.newCall(request).enqueue(new Callback() {
//                @Override
//                public void onFailure(Call call, IOException e) {
//                    getView().dismissDialog();
//                    getView().verifyOCSPUrlError(null, null, null);
//                }
//
//                @Override
//                public void onResponse(Call call, Response response) throws IOException {
//                    if (response.isSuccessful() && response.body() != null) {
//                        String valueStr = response.body().string();
        FpsParamsResponseEntity fpsParamsResponseEntity = new FpsParamsResponseEntity();
//        FpsParamsResponseEntity fpsParamsResponseEntity = new Gson().fromJson(valueStr, FpsParamsResponseEntity.class);
        LogUtils.i(TAG, "valueStr:" + fpsParamsResponseEntity.getPayload());
        //成功 直接解析二维码
        fpsParamsResponseEntity.setPayload("00020101021126440012hk.com.hkicl0419blueskyco@gmail.com0601152040000530334454031.25802HK5902NA6002HK6216011201-1234567896304CB6F");
//        parserFpsQrcode(fpsParamsResponseEntity.getPayload(), fpsParamsEntity.getCallback());
        parserFpsQrcode(fpsParamsResponseEntity.getPayload());
//                    }
//                }
//            });
//        } catch (Exception e) {
//            if (BuildConfig.isLogDebug) {
//                e.printStackTrace();
//            }
//            getView().dismissDialog();
//            getView().verifyOCSPUrlError(null, null, null);
//        }

    }

    public OkHttpClient.Builder getBuilder() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                               String authType) throws CertificateException {
                    //该方法在服务端使用，用来校验客户端的安全性
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    //该方法在客户端使用，用来校验服务端的安全性
//                    try {
//                        chain[0].checkValidity();
//                    } catch (CertificateExpiredException e1) {
//                        //给用户弹出安全性提示
//                    } catch (CertificateNotYetValidException e2) {
//                        //给用户弹出安全性提示
//                    }

                    organizationList = new ArrayList<>();
                    for (int i = 0; i < chain.length; i++) {
                        String temp = chain[i].getIssuerDN().toString();
                        organizationList.add(temp);
                        LogUtils.i(TAG, "Principal name " + temp);
                    }
                    Integer result = OCSPTool.checkCertStatus(chain[0], chain[1], 1);
                    if (result != 0) {
                        //异常，需要报错
                        LogUtils.i(TAG, "checkServerTrusted FALSE ");
                    }
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }};
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustAllCerts, new SecureRandom());
            builder.sslSocketFactory(sslContext.getSocketFactory());
        } catch (Exception e) {
            HttpLogUtils.d(TAG, e.getMessage());
        }
        return builder;
    }

    @Override
    public void getActionTrxGpInfo(ActionTrxGpInfoEntity mActionUpiInfo) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        MpQrUrlInfoResult mpQrUrlInfoResult = mActionUpiInfo.parseMqrCode();
        new ActionTrxGpInfoProtocol(mpQrUrlInfoResult, new NetWorkCallbackListener<ActionTrxGpInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ActionTrxGpInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getPaymentTypeList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetPayTypeListProtocol(null, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListSuccess(response.getRows());
                }
            }
        }).execute();
    }

    @Override
    public void getPayMentResult(String txnId) {
        new QueryTradeStatusProtocol(txnId, ACTION_PAY, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPayMentResultError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPayMentResultSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getMainScanOrder(MainScanOrderRequestEntity orderRequest) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MainScanApppCallAppOrderProtocol(orderRequest, ACTION_PAY, new NetWorkCallbackListener<AppCallAppOrderEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AppCallAppOrderEntity response) {
                if (getView() != null) {
                    if (response.isSuccess()) {
                        getPayMentResult(response.getTxnId());
                    } else {
                        getView().dismissDialog();
                        TempSaveHelper.updateMerchantLink(response.getAppLink(), response.isAppCallAppFromH5());
                        getView().getMainScanOrderParamsError(response.getResultCode(), response.getMessage());
                    }
                }
            }
        }).execute();
    }

    @Override
    public void actionMpqrPaymentWithPoint(MainScanOrderRequestEntity orderRequest) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MainScanOrderWithPointAppCallAppProtocol(orderRequest, ACTION_PAY, new NetWorkCallbackListener<AppCallAppOrderEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(AppCallAppOrderEntity response) {
                if (getView() != null) {
                    if (response.isSuccess()) {
                        getPayMentResult(response.getTxnId());
                    } else {
                        getView().dismissDialog();
                        TempSaveHelper.updateMerchantLink(response.getAppLink(), response.isAppCallAppFromH5());
                        getView().getMainScanOrderParamsError(response.getResultCode(), response.getMessage());
                    }
                }
            }
        }).execute();
    }

}
