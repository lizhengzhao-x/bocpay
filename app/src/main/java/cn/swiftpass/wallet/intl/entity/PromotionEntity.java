package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class PromotionEntity extends BaseEntity {


    /**
     * zh_HK : {"promotion":[{"id":"1","type":"BOC PAY","title":"中銀信用卡客戶專享HK$800簽賬回贈","description":"推廣期內憑中銀信用卡作本地消費可享高達HK$800簽賬回贈","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion1.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"},{"id":"2","type":"BOC PAY","title":"SOGO 15倍積分獎賞","description":"推廣期內憑中銀信用卡於SOGO消費可享15倍積分獎賞","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion2.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"}]}
     * en_US : {"promotion":[{"id":"1","type":"BOC PAY","title":"Up to HK$800 Spending Credit Reward for BOC Credit Cardholder","description":"During the promotion period, simply make local spending upon designated amount, you will be entitle up to 800 Spending credit reward!","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion1.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"},{"id":"2","type":"BOC PAY","title":"Up to 15x reward gift point with spending at SOGO","description":"During the promotion period, retail spending at SOGO, you will be entitle up to 15x gift point reward.","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion2.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"}]}
     * zh_CN : {"promotion":[{"id":"1","type":"BOC PAY","title":"中银信用卡客户专享HK$800签账回赠","description":"推广期内凭中银信用卡作本地消费可享高达HK$800签账回赠","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion1.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"},{"id":"2","type":"BOC PAY","title":"SOGO 15倍积分奖赏","description":"推广期内凭中银信用卡于SOGO消费可享15倍积分奖赏","images":"https://mobileuat.boccc.com.hk/bocpay/promotion/promotion2.jpg","lastmodifydate":"20180109","expire":"20180330","url":"http://www.boci.com.hk/chi/index.html"}]}
     */

    private ZhHKBean zh_HK;
    private EnUSBean en_US;
    private ZhCNBean zh_CN;

    public ZhHKBean getZh_HK() {
        return zh_HK;
    }

    public void setZh_HK(ZhHKBean zh_HK) {
        this.zh_HK = zh_HK;
    }

    public EnUSBean getEn_US() {
        return en_US;
    }

    public void setEn_US(EnUSBean en_US) {
        this.en_US = en_US;
    }

    public ZhCNBean getZh_CN() {
        return zh_CN;
    }

    public void setZh_CN(ZhCNBean zh_CN) {
        this.zh_CN = zh_CN;
    }

    public static class ZhHKBean {
        private List<PromotionBean> promotion;

        public List<PromotionBean> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<PromotionBean> promotion) {
            this.promotion = promotion;
        }


    }

    public static class PromotionBean {
        /**
         * id : 1
         * type : BOC PAY
         * title : 中銀信用卡客戶專享HK$800簽賬回贈
         * description : 推廣期內憑中銀信用卡作本地消費可享高達HK$800簽賬回贈
         * images : https://mobileuat.boccc.com.hk/bocpay/promotion/promotion1.jpg
         * lastmodifydate : 20180109
         * expire : 20180330
         * url : http://www.boci.com.hk/chi/index.html
         */

        private String id;
        private String type;
        private String title;
        private String description;
        private String images;
        private String lastmodifydate;
        private String expire;
        private String url;
        private String fullBannerUrl;

        public String getNewsType() {
            return newsType;
        }

        public void setNewsType(String newsType) {
            this.newsType = newsType;
        }

        /**
         * newsType : 2 为信用卡登记奖赏。
         */
        private String newsType;


        public boolean isCreditCardReward() {
            if (TextUtils.isEmpty(newsType)) {
                return false;
            }
            return TextUtils.equals(newsType, "2");
        }

        public String getShareImageUrl() {
            return shareImageUrl;
        }

        public void setShareImageUrl(String shareImageUrl) {
            this.shareImageUrl = shareImageUrl;
        }

        /**
         * 分享图片的url
         */
        private String shareImageUrl;

        public String getFullBannerUrl() {
            return fullBannerUrl;
        }

        public void setFullBannerUrl(String fullBannerUrl) {
            this.fullBannerUrl = fullBannerUrl;
        }

        public String getIsJsonConnect() {
            return isJsonConnect;
        }

        public void setIsJsonConnect(String isJsonConnect) {
            this.isJsonConnect = isJsonConnect;
        }

        private String isJsonConnect;

        public String getOpenExtBrowserMsg() {
            return openExtBrowserMsg;
        }

        public void setOpenExtBrowserMsg(String openExtBrowserMsg) {
            this.openExtBrowserMsg = openExtBrowserMsg;
        }

        private String openExtBrowserMsg;

        public String getIsRandomDisplay() {
            return isRandomDisplay;
        }

        public void setIsRandomDisplay(String isRandomDisplay) {
            this.isRandomDisplay = isRandomDisplay;
        }

        private String isRandomDisplay;

        public String getIshidden() {
            return ishidden;
        }

        public boolean isHidden() {
            if (TextUtils.isEmpty(ishidden)) return true;
            return ishidden.equals("Y");
        }

        public void setIshidden(String ishidden) {
            this.ishidden = ishidden;
        }

        private String ishidden;

        public boolean isOpenExtBrowser() {
            return openExtBrowser;
        }

        public void setOpenExtBrowser(boolean openExtBrowser) {
            this.openExtBrowser = openExtBrowser;
        }

        private boolean openExtBrowser;

        public boolean isDirBrower() {
            if (TextUtils.isEmpty(dirBrower)) {
                return false;
            }
            return dirBrower.equals("N");
        }

        public String getDirBrower() {
            return dirBrower;
        }

        public void setDirBrower(String dirBrower) {
            this.dirBrower = dirBrower;
        }

        private String dirBrower;

        public String getEffective() {
            return effective;
        }

        public void setEffective(String effective) {
            this.effective = effective;
        }

        private String effective;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTitle() {
            return title;
        }

        public String getShareContent() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public String getLastmodifydate() {
            return lastmodifydate;
        }

        public void setLastmodifydate(String lastmodifydate) {
            this.lastmodifydate = lastmodifydate;
        }

        public String getExpire() {
            return expire;
        }

        public void setExpire(String expire) {
            this.expire = expire;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }

    public static class EnUSBean {
        private List<PromotionBean> promotion;

        public List<PromotionBean> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<PromotionBean> promotion) {
            this.promotion = promotion;
        }

    }

    public static class ZhCNBean {
        private List<PromotionBean> promotion;

        public List<PromotionBean> getPromotion() {
            return promotion;
        }

        public void setPromotion(List<PromotionBean> promotion) {
            this.promotion = promotion;
        }

    }
}
