package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 11:08
 * 账单查询返回账单List对象
 */

public class MenuListEntity extends BaseEntity {
    ArrayList<MenuItemEntity> menuData;
    private String menuEncryptionData;

    public ArrayList<MenuItemEntity> getMenuData() {
        return menuData;
    }

    public void setMenuData(ArrayList<MenuItemEntity> menuData) {
        this.menuData = menuData;
    }

    public String getMenuEncryptionData() {
        return menuEncryptionData;
    }

    public void setMenuEncryptionData(String menuEncryptionData) {
        this.menuEncryptionData = menuEncryptionData;
    }
}
