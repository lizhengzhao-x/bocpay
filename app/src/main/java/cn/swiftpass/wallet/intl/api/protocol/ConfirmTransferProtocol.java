package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/12/12.
 */
public class ConfirmTransferProtocol extends BaseProtocol {

    public static final String TAG = CreditBindSmartAccountOtpProtocol.class.getSimpleName();
    String txnId;

    public ConfirmTransferProtocol(String txnId, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.txnId = txnId;
        mUrl = "api/crossTransfer/confirmTransfer";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);
    }
}
