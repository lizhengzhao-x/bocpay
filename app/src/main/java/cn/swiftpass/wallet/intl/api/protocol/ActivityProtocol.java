package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/23 15:07
 * 获取首页活动列表文件地址
 * 对应的返回对象为 ActivityEntity
 */

public class ActivityProtocol extends BaseProtocol {
    String mName;

    public ActivityProtocol(String name, NetWorkCallbackListener dataCallback) {
        this.mName = name;
        this.mDataCallback = dataCallback;
        mUrl = "parameter/getValue";
        mEncryptFlag = false;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTIVITIES_NAME, mName);
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }
}
