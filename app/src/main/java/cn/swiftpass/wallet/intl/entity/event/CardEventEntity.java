package cn.swiftpass.wallet.intl.entity.event;


public class CardEventEntity extends BaseEventEntity{

    public static final int EVENT_BIND_CARD = 1001;
    public static final int EVENT_CLOSE_VIRTUALCADR_PAGE = 1002;
    public static final int EVENT_UNBIND_CARD_SUCCESS = 1003;
    public static final int EVENT_REFRESH_SMART_INFO = 1014;//刷新卡列表以及账户资料

    public CardEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public CardEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
