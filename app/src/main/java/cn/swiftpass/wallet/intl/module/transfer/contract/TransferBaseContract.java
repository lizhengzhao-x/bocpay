package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.module.transfer.entity.FpsCheckEntity;

/**
 * 转账输入界面 红包转账和普通转账
 */
public class TransferBaseContract {

    public interface View extends IView {

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);

        void getRegEddaInfoError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(MySmartAccountEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getAccountBalanceInfoSuccess(AccountBalanceEntity response);

        void getAccountBalanceInfoError(String errorCode, String errorMsg);

        void checkTransferPreSuccess(TransferPreCheckEntity response,boolean isLishi, TransferPreCheckReq req);

        void checkTransferPreError(String errorCode, String errorMsg);

        void fpsCheckActionSuccess(FpsCheckEntity response);

        void fpsCheckActionError(String errorCode, String errorMsg);

        void transferPreByBocSuccess(boolean isLiShi,TransferPreCheckReq req,String tansferAccount,TransferPreCheckEntity response);

        void transferPreByBocError(String errorCode, String errorMsg);
    }


    public interface Presenter<P extends TransferBaseContract.View> extends IPresenter<P> {

        void getRegEddaInfo();
        void getSmartAccountInfo();
        void getAccountBalanceInfo();

        void checkTransferPre(boolean show,boolean isLishi, TransferPreCheckReq req);

        void fpsCheckAction(String transferType, String tansferToAccount,boolean showDialog,boolean isPaiLiShi);

        void transferPreByBoc(boolean isLiShi,TransferPreCheckReq req,String tansferAccount);
    }

}
