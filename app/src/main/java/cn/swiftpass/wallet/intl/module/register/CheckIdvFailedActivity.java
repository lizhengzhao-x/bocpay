package cn.swiftpass.wallet.intl.module.register;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 新版checkIdv失败界面
 */
public class CheckIdvFailedActivity extends BaseCompatActivity {
    @BindView(R.id.tv_recognition_failed)
    TextView tvRecognitionRetry;
    @BindView(R.id.tv_recognition_success)
    TextView tvRecognitionLeave;
    @BindView(R.id.id_failed_view)
    LinearLayout idFailedView;
    private int mPageFlow;

    @Override
    public void init() {
        hideBackIcon();

        if (getIntent() != null && getIntent().getExtras() != null) {
            mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        } else {
            finish();
            return;
        }

        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_26_1);
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
            setToolBarTitle(R.string.IDV_3_1a);

            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            setToolBarTitle(R.string.IDV_3a_19_1);
        }


        //重试
        tvRecognitionRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态忘记密码
                        MyActivityManager.removeAllTaskForForgetPwdWithLoginStatusMainStack();
                    } else {
                        //登出态忘记密码
                        MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                    }

                } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                    //注册
                    MyActivityManager.removeAllTaskWithRegister();
                } else {
                    //绑卡 推到选择绑卡页面
                    MyActivityManager.removeAllTaskForBindCardStack();
                }
            }
        });

        //离开
        tvRecognitionLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        ActivitySkipUtil.startAnotherActivity(CheckIdvFailedActivity.this, MainHomeActivity.class);
                        MyActivityManager.removeAllTaskExcludeMainStack();
                    } else {
                        ActivitySkipUtil.startAnotherActivity(CheckIdvFailedActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    }
                } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                    //注册
                    MyActivityManager.removeAllTaskTologinPage();
                } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
                    //绑卡 离开退回到卡列表
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        MyActivityManager.removeAllTaskExcludeMainStack();
                        ActivitySkipUtil.startAnotherActivity(CheckIdvFailedActivity.this, MainHomeActivity.class);
                        EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
                    } else {
                        MyActivityManager.removeAllTaskExcludePreLoginStack();
                        ActivitySkipUtil.startAnotherActivity(CheckIdvFailedActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }


                }
            }
        });


    }

    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);
        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_COMPAREPEOPLEANDCARDS);
    }

    @Override
    public boolean isAnalysisPage() {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.ALL_INFORMATION_CONFIRM;
        analysisPageEntity.current_address = PagerConstant.REGISTRATION_FAILED;
        return analysisPageEntity;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_checkidv_failed;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.hide();
            }
        }
    }

}
