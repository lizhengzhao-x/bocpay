package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSetPaymentPswActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 我的账户已存在，确认已注册我的账户信息界面
 */

public class SmartAccountInfoConfirmActivity extends BaseCompatActivity {
    @BindView(R.id.tv_smart_account)
    TextView mSmartAccountTV;
    @BindView(R.id.tv_primary_account)
    TextView mPrimaryAccountTV;
    @BindView(R.id.tv_limit_amount)
    TextView mLimitAmountTV;
    @BindView(R.id.tv_topup_method)
    TextView mAddMethodTV;
    @BindView(R.id.tv_link_to_bocpay)
    TextView mLinkBocpayTV;
    @BindView(R.id.iv_confirm_mark)
    ImageView ivConfirmMark;
    @BindView(R.id.id_confirm_conditions)
    TextView idConfirmConditions;


    int mTypeFlag;
    public BankLoginResultEntity mResultEntity;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        if (null != getIntent()) {
            mTypeFlag = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
            mResultEntity = (BankLoginResultEntity) getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO);
            initAgreement();
            updateNextStatus(false);
            initView();
            if (mTypeFlag == Constants.PAGE_FLOW_FORGETPASSWORD) {
                //重设密码成功
                setToolBarTitle(R.string.setting_payment_fgw);
                mLinkBocpayTV.setText(R.string.next_page);
            } else {
                setToolBarTitle(R.string.link_to_bocpay);
            }

        } else {
            finish();
        }
    }

    private void initView() {
        mSmartAccountTV.setText(mResultEntity.getSmartNo());
        mPrimaryAccountTV.setText(AndroidUtils.getPrimaryAccountDisplay(mResultEntity.getAccType(), mResultEntity.getRelevanceAccNo()));
//        mLimitAmountTV.setText(AndroidUtils.getCurrencyAmount(mResultEntity.getPayLimit()));
        //千分位，不含小数
        String money = AndroidUtils.formatPrice(Double.parseDouble(mResultEntity.getPayLimit()), true);
        mLimitAmountTV.setText(AndroidUtils.getCurrencyAmount(money));
        mAddMethodTV.setText(AndroidUtils.getTopUpMethodValue(this, mResultEntity.getAddedmethod()));
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smart_info_confirm;
    }


    @OnClick({R.id.tv_link_to_bocpay, R.id.iv_confirm_mark})
    public void onViewClicked(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.tv_link_to_bocpay:
                next();
                break;
            case R.id.iv_confirm_mark:
                onClickCheck();
                break;
            default:
                break;
        }
    }

    private void next() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
        mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_SMART);
        mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, mResultEntity);
        mHashMaps.put(Constants.WALLET_ID, mResultEntity.getUserId());
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, mResultEntity.getPhone());
        if (mTypeFlag == Constants.PAGE_FLOW_REGISTER) {
            mHashMaps.put(Constants.USE_BIOMETRICAUTH, getIntent().getBooleanExtra(Constants.USE_BIOMETRICAUTH, true));
            ActivitySkipUtil.startAnotherActivity(SmartAccountInfoConfirmActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            ActivitySkipUtil.startAnotherActivity(SmartAccountInfoConfirmActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    private void onClickCheck() {
        ivConfirmMark.setSelected(!ivConfirmMark.isSelected());
        updateNextStatus(ivConfirmMark.isSelected());
    }

    private void updateNextStatus(boolean check) {
        if (check && !ivConfirmMark.isSelected()) {
            return;
        }
        mLinkBocpayTV.setEnabled(check);
        mLinkBocpayTV.setBackgroundResource(check ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    private void initAgreement() {
        String totalStr = getResources().getString(R.string.card_info_notice);
        int startIndex = totalStr.indexOf("%");
        SpannableString spannableString = new SpannableString(totalStr.replace("%", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(SmartAccountInfoConfirmActivity.this, ServerAgreementActivity.class);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        idConfirmConditions.setText(spannableString);
        idConfirmConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }

    public static void startActivity(Context context, int type, BankLoginResultEntity entity) {
        Intent intent = new Intent(context, SmartAccountInfoConfirmActivity.class);
        intent.putExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, entity);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, type);
        intent.putExtra(Constants.USE_BIOMETRICAUTH, entity.isOpenBiometricAuth());
        context.startActivity(intent);
    }
}
