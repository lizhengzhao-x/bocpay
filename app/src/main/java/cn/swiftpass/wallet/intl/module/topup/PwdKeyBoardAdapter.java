package cn.swiftpass.wallet.intl.module.topup;


import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.KeyBoardEntity;


public class PwdKeyBoardAdapter implements AdapterItem<KeyBoardEntity> {

    private TextView mTextKey, mTextKeyEng;
    private ImageView mImageDelete;
    private LinearLayout mLinKeys;
    private Context context;
    private RelativeLayout mRelaItem;

    public PwdKeyBoardAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.item_gridview_keyboard;
    }

    @Override
    public void bindViews(View root) {
        mTextKey = root.findViewById(R.id.tv_key);
        mTextKeyEng = root.findViewById(R.id.tv_key_eng);
        mImageDelete = root.findViewById(R.id.iv_delete);
        mLinKeys = root.findViewById(R.id.ll_keys);
        mRelaItem = root.findViewById(R.id.rela_item);
    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(KeyBoardEntity keyBoardEntity, int position) {
        if (keyBoardEntity.getKey().equals("delete")) {
            mImageDelete.setVisibility(View.VISIBLE);
            mRelaItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_bg_selector_gray));
            mLinKeys.setVisibility(View.INVISIBLE);
        } else if (TextUtils.isEmpty(keyBoardEntity.getKey())) {
            mImageDelete.setVisibility(View.GONE);
            mRelaItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_bg_selector_gray));
            mLinKeys.setVisibility(View.INVISIBLE);
        } else {
            mRelaItem.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.item_bg_selector));
            mImageDelete.setVisibility(View.GONE);
            mLinKeys.setVisibility(View.VISIBLE);
            mTextKey.setText(keyBoardEntity.getKey());
            mTextKeyEng.setText(keyBoardEntity.getKeyEng());
        }
    }
}
