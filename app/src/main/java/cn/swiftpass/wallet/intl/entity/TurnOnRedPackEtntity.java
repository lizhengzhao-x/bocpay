package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/8/20.
 * 拆利是
 */

public class TurnOnRedPackEtntity extends BaseEntity {


    /**
     * nextRedPacketRefNo : T121116410000006
     * receivedRecordsTurnOff : [{"blessingWords":"这是zhe'shi这是这是","coverId":"1","coverUrl":"images/en_US/bg_cny_style_default_envelope.png","currency":"HKD","name":"CHEN DEREK","phone":"852-90296763","sendAmt":"12.23","sendTime":1606734234148,"srcRefNo":"T121116410000006","time":"2020-11-30 19:03:54","transferType":"8","turnOn":"0"}]
     * receivedRecordsTurnOffHasMore : 0
     * receivedRecordsTurnOffNum : 1
     * receivedRecordsTurnOn : [{"blessingWords":"电话的🤗🙄😮🤬","coverId":"1","coverUrl":"images/en_US/bg_cny_style_default_envelope.png","currency":"HKD","name":"CHEN DEREK","phone":"852-90296763","sendAmt":"14.00","sendTime":1606976521664,"srcRefNo":"T051418320000005","time":"2020-12-03 14:22:01","transferType":"8","turnOn":"1","turnOnTimeStr":"2020-12-03 14:22:01"}]
     * receivedRecordsTurnOnHasMore : 0
     * receivedRecordsTurnOnNum : 1
     * receivedRedPacketDetail : {"blessingWords":"电话的🤗🙄😮🤬","coverId":"1","coverUrl":"images/en_US/bg_cny_style_default_envelope.png","name":"CHEN DEREK","phone":"852-90296763","sendAmt":"1400","sendTime":1606976318573,"srcRefNo":"T051418320000005","time":"2020-12-03 14:18:38","transferType":"8","turnOn":"1"}
     * turnRedPacketCount : 1
     */

    private String nextRedPacketRefNo;
    private int receivedRecordsTurnOffHasMore;
    private int receivedRecordsTurnOffNum;
    private int receivedRecordsTurnOnHasMore;
    private int receivedRecordsTurnOnNum;
    private ReceivedRedPacketDetailBean receivedRedPacketDetail;
    private int turnRedPacketCount;
    private List<ReceivedRecordsTurnOffBean> receivedRecordsTurnOff;
    private List<ReceivedRecordsTurnOnBean> receivedRecordsTurnOn;

    public String getNextRedPacketRefNo() {
        return nextRedPacketRefNo;
    }

    public void setNextRedPacketRefNo(String nextRedPacketRefNo) {
        this.nextRedPacketRefNo = nextRedPacketRefNo;
    }

    public int getReceivedRecordsTurnOffHasMore() {
        return receivedRecordsTurnOffHasMore;
    }

    public void setReceivedRecordsTurnOffHasMore(int receivedRecordsTurnOffHasMore) {
        this.receivedRecordsTurnOffHasMore = receivedRecordsTurnOffHasMore;
    }

    public int getReceivedRecordsTurnOffNum() {
        return receivedRecordsTurnOffNum;
    }

    public void setReceivedRecordsTurnOffNum(int receivedRecordsTurnOffNum) {
        this.receivedRecordsTurnOffNum = receivedRecordsTurnOffNum;
    }

    public int getReceivedRecordsTurnOnHasMore() {
        return receivedRecordsTurnOnHasMore;
    }

    public void setReceivedRecordsTurnOnHasMore(int receivedRecordsTurnOnHasMore) {
        this.receivedRecordsTurnOnHasMore = receivedRecordsTurnOnHasMore;
    }

    public int getReceivedRecordsTurnOnNum() {
        return receivedRecordsTurnOnNum;
    }

    public void setReceivedRecordsTurnOnNum(int receivedRecordsTurnOnNum) {
        this.receivedRecordsTurnOnNum = receivedRecordsTurnOnNum;
    }

    public ReceivedRedPacketDetailBean getReceivedRedPacketDetail() {
        return receivedRedPacketDetail;
    }

    public void setReceivedRedPacketDetail(ReceivedRedPacketDetailBean receivedRedPacketDetail) {
        this.receivedRedPacketDetail = receivedRedPacketDetail;
    }

    public int getTurnRedPacketCount() {
        return turnRedPacketCount;
    }

    public void setTurnRedPacketCount(int turnRedPacketCount) {
        this.turnRedPacketCount = turnRedPacketCount;
    }

    public List<ReceivedRecordsTurnOffBean> getReceivedRecordsTurnOff() {
        return receivedRecordsTurnOff;
    }

    public void setReceivedRecordsTurnOff(List<ReceivedRecordsTurnOffBean> receivedRecordsTurnOff) {
        this.receivedRecordsTurnOff = receivedRecordsTurnOff;
    }

    public List<ReceivedRecordsTurnOnBean> getReceivedRecordsTurnOn() {
        return receivedRecordsTurnOn;
    }

    public void setReceivedRecordsTurnOn(List<ReceivedRecordsTurnOnBean> receivedRecordsTurnOn) {
        this.receivedRecordsTurnOn = receivedRecordsTurnOn;
    }

    public static class ReceivedRedPacketDetailBean extends BaseEntity{
        /**
         * blessingWords : 电话的🤗🙄😮🤬
         * coverId : 1
         * coverUrl : images/en_US/bg_cny_style_default_envelope.png
         * name : CHEN DEREK
         * phone : 852-90296763
         * sendAmt : 1400
         * sendTime : 1606976318573
         * srcRefNo : T051418320000005
         * time : 2020-12-03 14:18:38
         * transferType : 8
         * turnOn : 1
         */

        private String blessingWords;
        private String coverId;
        private String coverUrl;
        private String name;
        private String phone;
        private String sendAmt;
        private long sendTime;
        private String srcRefNo;
        private String time;
        private String transferType;
        private String turnOn;
        private String redPacketImageUrl;
        private String redPacketMusicUrl;
        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getRedPacketImageUrl() {
            return redPacketImageUrl;
        }

        public void setRedPacketImageUrl(String redPacketImageUrl) {
            this.redPacketImageUrl = redPacketImageUrl;
        }

        public String getRedPacketMusicUrl() {
            return redPacketMusicUrl;
        }

        public void setRedPacketMusicUrl(String redPacketMusicUrl) {
            this.redPacketMusicUrl = redPacketMusicUrl;
        }

        public String getBlessingWords() {
            return blessingWords;
        }

        public void setBlessingWords(String blessingWords) {
            this.blessingWords = blessingWords;
        }

        public String getCoverId() {
            return coverId;
        }

        public void setCoverId(String coverId) {
            this.coverId = coverId;
        }

        public String getCoverUrl() {
            return coverUrl;
        }

        public void setCoverUrl(String coverUrl) {
            this.coverUrl = coverUrl;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSendAmt() {
            return sendAmt;
        }

        public void setSendAmt(String sendAmt) {
            this.sendAmt = sendAmt;
        }

        public long getSendTime() {
            return sendTime;
        }

        public void setSendTime(long sendTime) {
            this.sendTime = sendTime;
        }

        public String getSrcRefNo() {
            return srcRefNo;
        }

        public void setSrcRefNo(String srcRefNo) {
            this.srcRefNo = srcRefNo;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTransferType() {
            return transferType;
        }

        public void setTransferType(String transferType) {
            this.transferType = transferType;
        }

        public String getTurnOn() {
            return turnOn;
        }

        public void setTurnOn(String turnOn) {
            this.turnOn = turnOn;
        }
    }

    public static class ReceivedRecordsTurnOffBean extends BaseEntity{
        /**
         * blessingWords : 这是zhe'shi这是这是
         * coverId : 1
         * coverUrl : images/en_US/bg_cny_style_default_envelope.png
         * currency : HKD
         * name : CHEN DEREK
         * phone : 852-90296763
         * sendAmt : 12.23
         * sendTime : 1606734234148
         * srcRefNo : T121116410000006
         * time : 2020-11-30 19:03:54
         * transferType : 8
         * turnOn : 0
         */

        private String blessingWords;
        private String coverId;
        private String coverUrl;
        private String currency;
        private String name;
        private String phone;
        private String sendAmt;
        private long sendTime;
        private String srcRefNo;
        private String time;
        private String transferType;
        private String turnOn;

        public String getBlessingWords() {
            return blessingWords;
        }

        public void setBlessingWords(String blessingWords) {
            this.blessingWords = blessingWords;
        }

        public String getCoverId() {
            return coverId;
        }

        public void setCoverId(String coverId) {
            this.coverId = coverId;
        }

        public String getCoverUrl() {
            return coverUrl;
        }

        public void setCoverUrl(String coverUrl) {
            this.coverUrl = coverUrl;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSendAmt() {
            return sendAmt;
        }

        public void setSendAmt(String sendAmt) {
            this.sendAmt = sendAmt;
        }

        public long getSendTime() {
            return sendTime;
        }

        public void setSendTime(long sendTime) {
            this.sendTime = sendTime;
        }

        public String getSrcRefNo() {
            return srcRefNo;
        }

        public void setSrcRefNo(String srcRefNo) {
            this.srcRefNo = srcRefNo;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTransferType() {
            return transferType;
        }

        public void setTransferType(String transferType) {
            this.transferType = transferType;
        }

        public String getTurnOn() {
            return turnOn;
        }

        public void setTurnOn(String turnOn) {
            this.turnOn = turnOn;
        }
    }

    public static class ReceivedRecordsTurnOnBean extends BaseEntity{
        /**
         * blessingWords : 电话的🤗🙄😮🤬
         * coverId : 1
         * coverUrl : images/en_US/bg_cny_style_default_envelope.png
         * currency : HKD
         * name : CHEN DEREK
         * phone : 852-90296763
         * sendAmt : 14.00
         * sendTime : 1606976521664
         * srcRefNo : T051418320000005
         * time : 2020-12-03 14:22:01
         * transferType : 8
         * turnOn : 1
         * turnOnTimeStr : 2020-12-03 14:22:01
         */

        private String blessingWords;
        private String coverId;
        private String coverUrl;
        private String currency;
        private String name;
        private String phone;
        private String sendAmt;
        private long sendTime;
        private String srcRefNo;
        private String time;
        private String transferType;
        private String turnOn;
        private String turnOnTimeStr;

        public String getBlessingWords() {
            return blessingWords;
        }

        public void setBlessingWords(String blessingWords) {
            this.blessingWords = blessingWords;
        }

        public String getCoverId() {
            return coverId;
        }

        public void setCoverId(String coverId) {
            this.coverId = coverId;
        }

        public String getCoverUrl() {
            return coverUrl;
        }

        public void setCoverUrl(String coverUrl) {
            this.coverUrl = coverUrl;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSendAmt() {
            return sendAmt;
        }

        public void setSendAmt(String sendAmt) {
            this.sendAmt = sendAmt;
        }

        public long getSendTime() {
            return sendTime;
        }

        public void setSendTime(long sendTime) {
            this.sendTime = sendTime;
        }

        public String getSrcRefNo() {
            return srcRefNo;
        }

        public void setSrcRefNo(String srcRefNo) {
            this.srcRefNo = srcRefNo;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTransferType() {
            return transferType;
        }

        public void setTransferType(String transferType) {
            this.transferType = transferType;
        }

        public String getTurnOn() {
            return turnOn;
        }

        public void setTurnOn(String turnOn) {
            this.turnOn = turnOn;
        }

        public String getTurnOnTimeStr() {
            return turnOnTimeStr;
        }

        public void setTurnOnTimeStr(String turnOnTimeStr) {
            this.turnOnTimeStr = turnOnTimeStr;
        }
    }
}
