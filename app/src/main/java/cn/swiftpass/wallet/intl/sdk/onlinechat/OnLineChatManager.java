package cn.swiftpass.wallet.intl.sdk.onlinechat;

import android.app.Activity;

import com.egoo.chat.listener.EGXChatProtocol;
import com.google.gson.Gson;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.entity.ChatEntity;
import cn.swiftpass.wallet.intl.entity.OCSLanguageEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.sdk.onlinechat
 * @class describe
 * @anthor zhangfan
 * @time 2020/3/20 14:41
 * @change
 * @chang time
 * @class describe
 */
public class OnLineChatManager {

    private static OnLineChatManager sInstance = new OnLineChatManager();

    private OnLineChatManager() {

    }

    public static OnLineChatManager getInstance() {
        return sInstance;
    }

    public EGXChatProtocol.AppWatcher getLoginAppWatcher(final BaseAbstractActivity activity) {
        return new EGXChatProtocol.AppWatcher() {
            @Override
            public void getMbkLgnInfo(String s, final EGXChatProtocol.AppCallback appCallback) {
///api/onlineChat/getLgnInfo
                if (!NetworkUtil.isNetworkAvailable()) {
                    return;
                }
                activity.showDialogNotCancel();
                ApiProtocolImplManager.getInstance().getMbkLgnInfo(s, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        activity.dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        if (!response.status.equals("login")) {
                            activity.dismissDialog();
                        }
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });


            }

            @Override
            public void redirectToMbkFunc(String s, String s1, String s2) {

            }

            @Override
            public void notifyLoginMbk(String s, String s1, String s2) {
                LogUtils.d("centerbank", s + "======" + s1 + "=====" + s2);
                activity.dismissDialog();
            }

            @Override
            public void getMbaInfo(String s, final EGXChatProtocol.AppCallback appCallback) {
                OCSLanguageEntity ocsLanguageEntity = new OCSLanguageEntity();
//                C = trad. chinese  S= simp. chinese  E= English
                String lan = SpUtils.getInstance(activity).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    ocsLanguageEntity.setLanguage("S");
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    ocsLanguageEntity.setLanguage("C");
                } else {
                    ocsLanguageEntity.setLanguage("E");
                }
                String jsonString = new Gson().toJson(ocsLanguageEntity);
                appCallback.returnValue(jsonString);

            }

            @Override
            public void notifyMbaAction(Activity activity, String s, String s1, String s2, EGXChatProtocol.AppCallback appCallback) {

            }

            @Override
            public void getMbkAccessToken(String channel, String requesterAccessToken, final EGXChatProtocol.AppCallback appCallback) {
///api/onlineChat/getAccessToken
                ApiProtocolImplManager.getInstance().getMbkAccessToken(channel, requesterAccessToken, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        activity.dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });
            }

            @Override
            public void confirmMbkAccessToken(String ts, String channel, String nonceStr, String signature, final EGXChatProtocol.AppCallback appCallback) {
///api/onlineChat/confirmAccessToken
                ApiProtocolImplManager.getInstance().confirmMbkAccessToken(ts, channel, nonceStr, signature, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        activity.dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        activity.dismissDialog();
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });
            }

            @Override
            public void getCertFromMbaSrv(String s, String s1, EGXChatProtocol.AppCallback appCallback) {

            }
        };
    }

}
