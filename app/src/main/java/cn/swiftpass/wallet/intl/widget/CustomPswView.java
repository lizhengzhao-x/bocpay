package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import cn.swiftpass.wallet.intl.R;


public class CustomPswView extends View {
    private int mWidth;
    private int mHeight;
    private Paint mPaint;
    private int mPointNums;//记录密码个数
    private int mPsw_size;
    private int mPsw_color;
    private int mPsw_count;
    private int mBorder_color;
    private String mCurrPsw = "";//当前密码
    private OnPswChangedListener onPswChanged;
    private static final int TOTAL_COUNT = 6;

    public CustomPswView(Context context) {
        this(context, null, 0);
    }

    public CustomPswView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomPswView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray tArray = context.obtainStyledAttributes(attrs, R.styleable.PwdTextView);//获取配置属性
        mPsw_size = tArray.getDimensionPixelSize(R.styleable.PwdTextView_psw_size, 20);
        mPsw_color = tArray.getColor(R.styleable.PwdTextView_psw_color, Color.BLACK);
        mPsw_count = tArray.getInt(R.styleable.PwdTextView_psw_count, 6);
        mBorder_color = tArray.getColor(R.styleable.PwdTextView_border_color, Color.parseColor("#9b9b9b"));
        tArray.recycle();
        init(context);
    }

    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 200;
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int width;
        if (widthMode == MeasureSpec.EXACTLY) {
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            width = Math.min(desiredWidth, widthSize);
        } else {
            width = desiredWidth;
        }
        mWidth = width;
        mHeight = width / mPsw_count;
        setMeasuredDimension(mWidth, mHeight);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mPaint.setColor(mBorder_color);
        mPaint.setStrokeWidth(4);
        mPaint.setStyle(Paint.Style.FILL);

        //画实心圆 绘制密码
        int margin = (int) (mWidth * 0.2);
        int w = (int) (mWidth * 0.6 / (mPsw_count * 2));
        mPaint.setColor(Color.parseColor("#F2F5F7"));
        for (int i = 0; i < TOTAL_COUNT; i++) {
            canvas.drawCircle((float) (w * (2 * i + 1) + margin), w, mPsw_size, mPaint);
        }

        mPaint.setColor(mPsw_color);
        for (int i = 0; i < mPointNums; i++) {
            canvas.drawCircle((float) (w * (2 * i + 1) + margin), w, mPsw_size, mPaint);
        }
    }

    //设置数据
    public void setDatas(String psw) {
        mCurrPsw = psw;
        int piontNums = psw.length();
        if (piontNums < 0) {
            piontNums = 0;
        }
        if (piontNums > mPsw_count) piontNums = mPsw_count;
        mPointNums = piontNums;
        invalidate();
        if (onPswChanged != null) if (psw.length() == mPsw_count) {
            onPswChanged.onPswChanged(psw, true);
        } else {
            onPswChanged.onPswChanged(psw, false);
        }
    }

    public String getmCurrPsw() {
        return mCurrPsw;
    }

    public OnPswChangedListener getOnPswChanged() {
        return onPswChanged;
    }

    public void setOnPswChanged(OnPswChangedListener onPswChanged) {
        this.onPswChanged = onPswChanged;
    }

    interface OnPswChangedListener {
        void onPswChanged(String psw, boolean complete);
    }

    public int getmPsw_count() {
        return mPsw_count;
    }

    public void setmPsw_count(int mPsw_count) {
        this.mPsw_count = mPsw_count;
    }
}