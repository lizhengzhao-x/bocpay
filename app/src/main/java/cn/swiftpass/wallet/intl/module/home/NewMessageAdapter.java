package cn.swiftpass.wallet.intl.module.home;

import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.NewMsgEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

public class NewMessageAdapter extends BaseRecyclerAdapter<NewMsgEntity> {

    private int bannerHeight, bannerWidth;

    public NewMessageAdapter(Context mContext, List<NewMsgEntity> promotionList) {
        super(R.layout.item_new_message, promotionList);
        screenWidth = AndroidUtils.getScreenWidth(mContext);
        screenHeight = AndroidUtils.getScreenHeight(mContext);
        bannerWidth = screenWidth - AndroidUtils.dip2px(mContext, 32f);
        bannerHeight = bannerWidth / 4;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, NewMsgEntity promotionBean, int position) {
        String imageUrl = promotionBean.getFullBannerUrl();
        ImageView ivMsg = baseViewHolder.getView(R.id.iv_new_msg);

        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) ivMsg.getLayoutParams();
        lp.width = bannerWidth;
        lp.height = bannerHeight;
        ivMsg.setLayoutParams(lp);

        GlideApp.with(mContext).load(imageUrl).transform(new RoundedCorners(AndroidUtils.dip2px(mContext, 4f))).placeholder(R.mipmap.banner_bg_preview).into(ivMsg);
        baseViewHolder.addOnClickListener(R.id.iv_new_msg);
    }
}
