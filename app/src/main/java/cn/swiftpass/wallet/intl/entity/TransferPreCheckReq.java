package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/8/20.
 * 转账试算返回结果
 */

public class TransferPreCheckReq extends BaseEntity {

    //债务方账号，电话或者邮箱
    public String tansferToAccount;

    // 转账金额
    public String transferAmount;
    //收款人姓名，transferType为3时，必填[三期服务分级需求新增]
    public String transferToName;

    //如果账号为电话号码,区号
    public String region;
    public String postscript;
    public String phone;

    //银行标识行内转账，行外转账标识 0：FPS-Transfer to other banks     1: Bank of China (HK)
    public String bankType;

    //债务方账户类型 0：电话，1：邮箱
    public String transferType;

    //银行名称, 在bankType=0的情况下必传
    public String bankName;

    //是不是扫码转账
    public boolean isQrcode;

    //是不是派利是
    public boolean isLiShi;

    //派利是封面ID
    public String coverId;

    public String billReference;

}
