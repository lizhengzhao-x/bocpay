package cn.swiftpass.wallet.intl.module.redpacket.entity;


import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by jamy on 2018/5/22.
 */

public class GetLiShiHomeEntity extends BaseEntity {


    /**
     * receivedRecordsTrunOff : []
     * receivedRecordsTrunOffHasMore : 0
     * receivedRecordsTrunOffNum : 0
     * receivedRecordsTrunOn : [{"blessingWords":"电话的🤗🙄😮🤬","coverId":"1","currency":"HKD","name":"CHEN DEREK","phone":"852-90296763","sendAmt":"14.00","srcRefNo":"T051418320000005","time":"2020-12-01 00:02:18","transferType":"8","turnOn":"1","turnOnTimeStr":"2020-12-01 00:02:18"},{"blessingWords":"这是zhe'shi这是这是","currency":"HKD","name":"CHEN DEREK","phone":"852-90296763","sendAmt":"12.23","srcRefNo":"T121116410000006","time":"2020-11-30 19:03:54","transferType":"8","turnOn":"1","turnOnTimeStr":"2020-11-30 19:03:54"}]
     * receivedRecordsTrunOnHasMore : 0
     * receivedRecordsTrunOnNum : 2
     * trunRedPacketCount : 0
     */

    private int receivedRecordsTurnOffHasMore;
    private int receivedRecordsTurnOffNum;
    private int receivedRecordsTurnOnHasMore;
    private int receivedRecordsTurnOnNum;
    private int trunRedPacketCount;
    private List<ReceivedRecordsTurnOffEntity> receivedRecordsTurnOff;
    private List<RedPacketBeanEntity> receivedRecordsTurnOn;

    public int getReceivedRecordsTurnOffHasMore() {
        return receivedRecordsTurnOffHasMore;
    }

    public void setReceivedRecordsTurnOffHasMore(int receivedRecordsTurnOffHasMore) {
        this.receivedRecordsTurnOffHasMore = receivedRecordsTurnOffHasMore;
    }

    public int getReceivedRecordsTurnOffNum() {
        return receivedRecordsTurnOffNum;
    }

    public void setReceivedRecordsTurnOffNum(int receivedRecordsTurnOffNum) {
        this.receivedRecordsTurnOffNum = receivedRecordsTurnOffNum;
    }

    public int getReceivedRecordsTurnOnHasMore() {
        return receivedRecordsTurnOnHasMore;
    }

    public void setReceivedRecordsTurnOnHasMore(int receivedRecordsTurnOnHasMore) {
        this.receivedRecordsTurnOnHasMore = receivedRecordsTurnOnHasMore;
    }

    public int getReceivedRecordsTurnOnNum() {
        return receivedRecordsTurnOnNum;
    }

    public void setReceivedRecordsTurnOnNum(int receivedRecordsTurnOnNum) {
        this.receivedRecordsTurnOnNum = receivedRecordsTurnOnNum;
    }

    public int getTrunRedPacketCount() {
        return trunRedPacketCount;
    }

    public void setTrunRedPacketCount(int trunRedPacketCount) {
        this.trunRedPacketCount = trunRedPacketCount;
    }

    public List<ReceivedRecordsTurnOffEntity> getReceivedRecordsTurnOff() {
        return receivedRecordsTurnOff;
    }

    public void setReceivedRecordsTurnOff(List<ReceivedRecordsTurnOffEntity> receivedRecordsTurnOff) {
        this.receivedRecordsTurnOff = receivedRecordsTurnOff;
    }

    public List<RedPacketBeanEntity> getReceivedRecordsTurnOn() {
        return receivedRecordsTurnOn;
    }

    public void setReceivedRecordsTurnOn(List<RedPacketBeanEntity> receivedRecordsTurnOn) {
        this.receivedRecordsTurnOn = receivedRecordsTurnOn;
    }

    public class ReceivedRecordsTurnOffEntity extends BaseEntity {

        /**
         * bgColor : https://ewasit3.ftcwifi.com//#920783
         * bocPayRedPacket : true
         * bottomImageUrl : https://ewasit3.ftcwifi.com//images/en_US/1.3.1/layout4/bottom.png
         * centerImageUrl : https://ewasit3.ftcwifi.com//images/en_US/1.3.1/layout4/center.png
         * coverId : 4
         * coverUrl : https://ewasit3.ftcwifi.com//images/en_US/1.3.1/layout4/redPacketCoverImage.png
         * currency : HKD
         * fPSRedPacket : false
         * name : SZE K*** M***
         * phone : 852-64861407
         * redPacketImageUrl : https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/images/red_packet/smp/background_normal.png?version=1
         * redPacketMusicUrl : https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/sounds/red_packet/smp/background_music_normal.mp3?version=1
         * sendAmt : 1.00
         * sendTime : 1608544585287
         * sendTimeStr : 2020-12-21 17:56:25
         * srcRefNo : T122117560000396
         * staffRedPacket : false
         * transferType : 8
         * turnOn : 0
         */

        private String bgColor;
        private boolean bocPayRedPacket;
        private String bottomImageUrl;
        private String centerImageUrl;
        private String coverId;
        private String coverUrl;
        private String currency;
        private boolean fPSRedPacket;
        private String name;
        private String phone;
        private String redPacketImageUrl;
        private String redPacketMusicUrl;
        private String sendAmt;
        private long sendTime;
        private String sendTimeStr;
        private String srcRefNo;
        private boolean staffRedPacket;
        private String transferType;
        private String turnOn;
        private String email;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBgColor() {
            return bgColor;
        }

        public void setBgColor(String bgColor) {
            this.bgColor = bgColor;
        }

        public boolean isBocPayRedPacket() {
            return bocPayRedPacket;
        }

        public void setBocPayRedPacket(boolean bocPayRedPacket) {
            this.bocPayRedPacket = bocPayRedPacket;
        }

        public String getBottomImageUrl() {
            return bottomImageUrl;
        }

        public void setBottomImageUrl(String bottomImageUrl) {
            this.bottomImageUrl = bottomImageUrl;
        }

        public String getCenterImageUrl() {
            return centerImageUrl;
        }

        public void setCenterImageUrl(String centerImageUrl) {
            this.centerImageUrl = centerImageUrl;
        }

        public String getCoverId() {
            return coverId;
        }

        public void setCoverId(String coverId) {
            this.coverId = coverId;
        }

        public String getCoverUrl() {
            return coverUrl;
        }

        public void setCoverUrl(String coverUrl) {
            this.coverUrl = coverUrl;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public boolean isFPSRedPacket() {
            return fPSRedPacket;
        }

        public void setFPSRedPacket(boolean fPSRedPacket) {
            this.fPSRedPacket = fPSRedPacket;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getRedPacketImageUrl() {
            return redPacketImageUrl;
        }

        public void setRedPacketImageUrl(String redPacketImageUrl) {
            this.redPacketImageUrl = redPacketImageUrl;
        }

        public String getRedPacketMusicUrl() {
            return redPacketMusicUrl;
        }

        public void setRedPacketMusicUrl(String redPacketMusicUrl) {
            this.redPacketMusicUrl = redPacketMusicUrl;
        }

        public String getSendAmt() {
            return sendAmt;
        }

        public void setSendAmt(String sendAmt) {
            this.sendAmt = sendAmt;
        }

        public long getSendTime() {
            return sendTime;
        }

        public void setSendTime(long sendTime) {
            this.sendTime = sendTime;
        }

        public String getSendTimeStr() {
            return sendTimeStr;
        }

        public void setSendTimeStr(String sendTimeStr) {
            this.sendTimeStr = sendTimeStr;
        }

        public String getSrcRefNo() {
            return srcRefNo;
        }

        public void setSrcRefNo(String srcRefNo) {
            this.srcRefNo = srcRefNo;
        }

        public boolean isStaffRedPacket() {
            return staffRedPacket;
        }

        public void setStaffRedPacket(boolean staffRedPacket) {
            this.staffRedPacket = staffRedPacket;
        }

        public String getTransferType() {
            return transferType;
        }

        public void setTransferType(String transferType) {
            this.transferType = transferType;
        }

        public String getTurnOn() {
            return turnOn;
        }

        public void setTurnOn(String turnOn) {
            this.turnOn = turnOn;
        }
    }
}
