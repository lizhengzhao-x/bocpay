package cn.swiftpass.wallet.intl.module.ecoupon.view;

/**
 * Created by ZhangXinchao on 2019/8/17.
 */
public interface EcouponSelChangedListener {
    void onItemEcouponCountChanged(int count, int positon);

    void onItemCheckMore(int position);

    void onReachMaxValue();

    void onShowGradeInFo();

    void onEditTextValue(int positon);
}
