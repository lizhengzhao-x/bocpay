package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/8/11.
 */

public class BankLoginResultEntity extends BaseEntity {

    //智能账号
    private String smartNo;

    //用户唯一id
    private String userId;


    //电话号码
    private String phone;

    //支付限额
    private String payLimit;

    //最大支付限额
    private String maxPay;

    //最小支付限额
    private String minPay;

    //我的账户关联的扣款账号
    private String relevanceAccNo;

    //邮箱
    private String email;

    //证件类型
    private String certType;

    //证件号码
    private String certNo;

    //充值方式SmartAccountConst
    private String addedmethod;


    //自动充值时最大限额
    private String autotopupamt;

    //自动充值时的限额
    String methodMaxPay;
    //自动充值时的限额
    String methodMinPay;

    String accType;

    public boolean isOpenBiometricAuth() {
        if (TextUtils.isEmpty(openBiometricAuth)) {
            return true;
        }
        return TextUtils.equals(openBiometricAuth, "Y");
    }

    public void setOpenBiometricAuth(String openBiometricAuth) {
        this.openBiometricAuth = openBiometricAuth;
    }

    /**
     * 是否开启生物认证 Y开启 N 不开启
     */
    private String openBiometricAuth;



    //nosmartbind:未开通我的账户 (先开通我的账户流程)
    //hpaysmartbind:存在我的账户并已绑定（退出绑定，走登陆流程）
    //nobocpaybind:存在我的账户未绑定BOCPay（注册BOCPay流程）
    private String type;
    //账号列表
    private List<SmartAccountCheckAccountEntity> accountRows;


    //账户的状态 只有2是正常
    private String cardState;

    public String getMethodMaxPay() {
        return methodMaxPay;
    }

    public void setMethodMaxPay(String methodMaxPay) {
        this.methodMaxPay = methodMaxPay;
    }

    public String getMethodMinPay() {
        return methodMinPay;
    }

    public void setMethodMinPay(String methodMinPay) {
        this.methodMinPay = methodMinPay;
    }

    public String getAccType() {
        return accType;
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getCardState() {
        return cardState;
    }

    public void setCardState(String cardState) {
        this.cardState = cardState;
    }


    public String getAutotopupamt() {
        return autotopupamt;
    }

    public void setAutotopupamt(String autotopupamt) {
        this.autotopupamt = autotopupamt;
    }


    public String getAddedmethod() {
        return addedmethod;
    }

    public void setAddedmethod(String addedmethod) {
        this.addedmethod = addedmethod;
    }


    public List<SmartAccountCheckAccountEntity> getAccountRows() {
        return accountRows;
    }

    public void setAccountRows(List<SmartAccountCheckAccountEntity> accountRows) {
        this.accountRows = accountRows;
    }


    public String getSmartNo() {
        return smartNo;
    }

    public void setSmartNo(String smartNo) {
        this.smartNo = smartNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPayLimit() {
        return payLimit;
    }

    public void setPayLimit(String payLimit) {
        this.payLimit = payLimit;
    }

    public String getMaxPay() {
        return maxPay;
    }

    public void setMaxPay(String maxPay) {
        this.maxPay = maxPay;
    }

    public String getMinPay() {
        return minPay;
    }

    public void setMinPay(String minPay) {
        this.minPay = minPay;
    }

    public String getRelevanceAccNo() {
        return relevanceAccNo;
    }

    public void setRelevanceAccNo(String relevanceAccNo) {
        this.relevanceAccNo = relevanceAccNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
