package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;


import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

public class MenuFilterAdapter implements AdapterItem<RedeemableGiftListEntity.BuMapsBean> {
    private ImageView mId_menu_leftimage;
    private TextView mId_menu_text;
    private Context mContext;
    private View id_rootview;
    private OnItemSelClicklistener onItemSelClicklistener;

    public MenuFilterAdapter(Context context, OnItemSelClicklistener onItemSelClicklistenerIn) {
        mContext = context;
        this.onItemSelClicklistener = onItemSelClicklistenerIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_menu_filter;
    }

    @Override
    public void bindViews(View root) {
        mId_menu_leftimage = (ImageView) root.findViewById(R.id.id_menu_leftimage);
        mId_menu_text = (TextView) root.findViewById(R.id.id_menu_text);
        id_rootview = (View) root.findViewById(R.id.id_rootview);
    }

    @Override
    public void setViews() {

    }

    @Override
    public void handleData(RedeemableGiftListEntity.BuMapsBean menuFilterEntity, final int position) {
        mId_menu_text.setText(menuFilterEntity.getBuName());
        mId_menu_leftimage.setVisibility(menuFilterEntity.isSel() ? View.VISIBLE : View.INVISIBLE);
        id_rootview.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onItemSelClicklistener.onItemClick(position);
            }
        });
    }

    public interface OnItemSelClicklistener {

        void onItemClick(int position);
    }
}
