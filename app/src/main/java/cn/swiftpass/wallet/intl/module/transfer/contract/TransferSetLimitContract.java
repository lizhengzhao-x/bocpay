package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;


public class TransferSetLimitContract {

    public interface View extends IView {

        void updateQRCodeSuccess(BaseEntity response);

        void updateQRCodeError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<TransferSetLimitContract.View> {

        void updateQRCode();
    }

}
