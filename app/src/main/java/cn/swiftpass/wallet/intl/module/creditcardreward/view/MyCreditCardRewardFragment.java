package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.MyCreditCardRewardContact;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.MyCreditCardRewardAdapter;

public class MyCreditCardRewardFragment extends BaseFragment {


    @BindView(R.id.tv_all_reward)
    TextView tvAllReward;
    @BindView(R.id.ll_all_reward)
    LinearLayout llAllReward;
    @BindView(R.id.tv_every_month_reward)
    TextView tvEveryMonthReward;
    @BindView(R.id.ll_every_month_reward)
    LinearLayout llEveryMonthReward;
    @BindView(R.id.tv_every_year_reward)
    TextView tvEveryYearReward;
    @BindView(R.id.ll_every_year_reward)
    LinearLayout llEveryYearReward;
    @BindView(R.id.vp_my_credit_card_reward_frg)
    ViewPager2 vpMyCreditCardRewardFrg;

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {

            vpMyCreditCardRewardFrg.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);

            List<Fragment> fragments = new ArrayList<>();
            fragments.add(new AllCreditCardRewardFragment());
            fragments.add(new EveryMonthCreditCardRewardFragment());
            fragments.add(new EveryYearCreditCardRewardFragment());

            MyCreditCardRewardAdapter adapter = new MyCreditCardRewardAdapter(getChildFragmentManager(), this.getLifecycle(), fragments);
            vpMyCreditCardRewardFrg.setOffscreenPageLimit(3);
            vpMyCreditCardRewardFrg.setAdapter(adapter);
            vpMyCreditCardRewardFrg.setUserInputEnabled(false);
            vpMyCreditCardRewardFrg.setCurrentItem(0);


        }
    }

    @Override
    protected MyCreditCardRewardContact.Presenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_my_credit_card_reward;
    }

    @Override
    protected void initView(View v) {

    }

    @OnClick({R.id.ll_all_reward, R.id.ll_every_month_reward, R.id.ll_every_year_reward})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_all_reward:
                updateSelectTab(0);
                vpMyCreditCardRewardFrg.setCurrentItem(0);
                break;
            case R.id.ll_every_month_reward:
                updateSelectTab(1);
                vpMyCreditCardRewardFrg.setCurrentItem(1);
                break;
            case R.id.ll_every_year_reward:
                updateSelectTab(2);
                vpMyCreditCardRewardFrg.setCurrentItem(2);
                break;
            default:
                break;
        }
    }

    private void updateSelectTab(int position) {
        tvAllReward.setTextColor(mActivity.getColor(R.color.font_gray_two));
        tvEveryMonthReward.setTextColor(mActivity.getColor(R.color.font_gray_two));
        tvEveryYearReward.setTextColor(mActivity.getColor(R.color.font_gray_two));
        llAllReward.setBackgroundResource(R.drawable.bg_all_reward_un_select);
        llEveryMonthReward.setBackgroundResource(R.drawable.bg_every_month_reward_un_select);
        llEveryYearReward.setBackgroundResource(R.drawable.bg_every_year_reward_un_select);
        if (position == 0) {
            tvAllReward.setTextColor(mActivity.getColor(R.color.app_white));
            llAllReward.setBackgroundResource(R.drawable.bg_all_reward_select);
        } else if (position == 1) {
            tvEveryMonthReward.setTextColor(mActivity.getColor(R.color.app_white));
            llEveryMonthReward.setBackgroundResource(R.drawable.bg_every_month_reward_select);
        } else if (position == 2) {
            tvEveryYearReward.setTextColor(mActivity.getColor(R.color.app_white));
            llEveryYearReward.setBackgroundResource(R.drawable.bg_every_year_reward_select);
        }
    }
}
