package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.BindNewCardResultHolder;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class BindNewCardResultAdapter extends RecyclerView.Adapter {


    private final Context context;
    private final ArrayList<BindNewCardEntity> selectCardList;


    public BindNewCardResultAdapter(Context context, ArrayList<BindNewCardEntity> selectCardList) {
        this.context = context;
        this.selectCardList = selectCardList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_bind_new_card_result, parent, false);
        return new BindNewCardResultHolder(context, inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        BindNewCardResultHolder confirmBindNewCardHolder = (BindNewCardResultHolder) holder;
        confirmBindNewCardHolder.setData(selectCardList.get(position));
    }

    @Override
    public int getItemCount() {
        return selectCardList != null ? selectCardList.size() : 0;
    }
}
