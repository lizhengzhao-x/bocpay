package cn.swiftpass.wallet.intl.module.register.adapter.holder;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

public class BindeNewCardHeaderHolder extends RecyclerView.ViewHolder {
    private final Context context;
    @BindView(R.id.iv_tip_bind_new_card)
    ImageView ivTipBindNewCard;

    public BindeNewCardHeaderHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;

    }

    private void showBindNewCardTipDialog(int flowType) {
        CustomDialog.Builder builder = new CustomDialog.Builder(context);
        builder.setTitle(context.getString(R.string.KBCC2105_2_4));
        String msg = "";
        if (flowType == Constants.PAGE_FLOW_REGISTER) {
            msg = context.getString(R.string.KBCC2105_2_5);
        } else {
            msg = context.getString(R.string.KBCC2105_8_6);
        }
        builder.setMessage(msg);

        builder.setPositiveButton(R.string.KBCC2105_2_6, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green);
        CustomDialog mDealDialog = builder.create();
        mDealDialog.show();
    }

    public void setData(int flowType) {
        ivTipBindNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                showBindNewCardTipDialog(flowType);

            }
        });
    }
}
