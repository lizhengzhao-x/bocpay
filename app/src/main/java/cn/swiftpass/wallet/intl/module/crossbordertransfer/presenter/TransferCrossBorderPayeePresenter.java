package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckCardIdProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderPayeeContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderCardEntity;


public class TransferCrossBorderPayeePresenter extends BasePresenter<TransferCrossBorderPayeeContract.View> implements TransferCrossBorderPayeeContract.Presenter {


    @Override
    public void checkCardId(String cardText) {


        new CheckCardIdProtocol(cardText, new NetWorkCallbackListener<TransferCrossBorderCardEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().checkCardIdError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderCardEntity response) {
                if (getView() != null) {
                    getView().checkCardIdSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void getSmartAccountInfo(String action, boolean iShowLoading) {
        if (getView() != null && iShowLoading) {
            getView().showDialogNotCancel();
        }

        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (iShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    if (iShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoSuccess(response,action);
                }
            }
        })).execute();
    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }



}
