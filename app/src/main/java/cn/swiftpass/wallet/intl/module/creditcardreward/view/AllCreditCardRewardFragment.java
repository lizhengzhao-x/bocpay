package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;

public class AllCreditCardRewardFragment extends MyCreditCardRewardBaseFragment {

    public AllCreditCardRewardFragment() {
        super(CreditRewardType.REWARD_DATE_TYPE_ALL);
    }
}
