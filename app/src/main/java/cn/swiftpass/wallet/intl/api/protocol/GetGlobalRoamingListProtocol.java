package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class GetGlobalRoamingListProtocol extends BaseProtocol {

    public GetGlobalRoamingListProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/globalRoamingList/getGlobalRoamingList";
    }
}
