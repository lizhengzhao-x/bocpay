package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetOCRInfoForForgetPwdProtocol extends BaseProtocol {

    public static final String TAG = LoginCheckProtocol.class.getSimpleName();

    String seqNumber;
    String fileId;

    public GetOCRInfoForForgetPwdProtocol(String seqNumber, String fileId, NetWorkCallbackListener dataCallback) {
        this.seqNumber = seqNumber;
        this.fileId = fileId;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/getOCRInfo";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.SEQNUMBER, seqNumber);
        mBodyParams.put(RequestParams.FILEID, fileId);
    }
}
