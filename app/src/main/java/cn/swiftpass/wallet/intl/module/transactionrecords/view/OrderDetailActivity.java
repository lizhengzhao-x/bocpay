package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.Manifest;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.ShareImageUtils;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.OrderRecordListAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.contract.OrderDetailContract;
import cn.swiftpass.wallet.intl.module.transactionrecords.presenter.OrderDetailPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.GlideRoundTransform;

import static cn.swiftpass.wallet.intl.module.transactionrecords.view.GradeReduceActivity.GRADEREDUCEINFO;

/**
 * 订单详情页 因为需要设计统一bocpay/智能账户的详情页 但是每一个记录item对应的实体都不一样 所以需要做一层处理
 */
public class OrderDetailActivity extends BaseCompatActivity<OrderDetailContract.Presenter> implements OrderDetailContract.View {
    @BindView(R.id.list)
    RecyclerView mRecycleView;
    private OrderQueryEntity bocPayTransDetailEntity;
    private SmartAccountBillListEntity.OutgroupBean smartAccountTransDetailEntity;
    public static final String ISFROMBOCPAY = "ISFROMBOCPAY";
    public static final String BOCPAYENTITY = "BOCPAYENTITY";
    public static final String SMARTENTITY = "SMARTENTITY";
    private OrderRecordListAdapter orderRecordShareListAdapter;


    /**
     * 保存到本地图片的banner文字
     */
    private String txnDetailText = null;

    /**
     * 交易记录 分为三栏 bocpay/智能账户/信用卡
     */
    private boolean isFromBocPay = false;

    @Override
    protected OrderDetailContract.Presenter createPresenter() {
        return new OrderDetailPresenter(OrderDetailActivity.this);
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.CP3_3a_1);
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        EventBus.getDefault().register(this);
        isFromBocPay = getIntent().getExtras().getBoolean(ISFROMBOCPAY);
        mRecycleView.setLayoutManager(new LinearLayoutManager(this));
        orderRecordShareListAdapter = new OrderRecordListAdapter(this, new OrderRecordListAdapter.OnItemShareListener() {
            @Override
            public void OnItemShare(int position) {
                PermissionInstance.getInstance().getStoreImagePermission(OrderDetailActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        showDialogNotCancel();
                        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
                        if (initBannerEntity != null) {
                            BannerImageEntity pTwoPBean = initBannerEntity.getTransferRecord(OrderDetailActivity.this);
                            if (TempSaveHelper.getInitBannerUrlParams() != null && !TextUtils.isEmpty(TempSaveHelper.getInitBannerUrlParams().getTxnDetailShareText(mContext))) {
                                txnDetailText = TempSaveHelper.getInitBannerUrlParams().getTxnDetailShareText(mContext);
                            }
                            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                                //显示图片
                                GlideApp.with(OrderDetailActivity.this).load(pTwoPBean.getImagesUrl()).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).transform(new GlideRoundTransform(mContext, 5)).into(new SimpleTarget<Drawable>() {
                                    @Override
                                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                        initBitmapImg(position, pTwoPBean.getImagesUrl(), txnDetailText);
                                    }

                                    @Override
                                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                        super.onLoadFailed(errorDrawable);
                                        initBitmapImg(position, null, txnDetailText);
                                    }
                                });
                            } else {
                                initBitmapImg(position, null, txnDetailText);
                            }
                        }
                        dismissDialog();
                    }

                    @Override
                    public void rejectPermission() {
                        ShareImageUtils.showLackOfPermissionDialog(OrderDetailActivity.this);
                    }
                }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            @Override
            public void OnItemGradeReduce(int position) {

                if (mPresenter != null) {
                    mPresenter.queryGradeInfo(bocPayTransDetailEntity.getOuttrfrefno());
                }

            }

            @Override
            public void onBarCodeClick() {
                if (bocPayTransDetailEntity != null) {
                    String refundExplain = bocPayTransDetailEntity.getRefundExplain();
                    showErrorMsgDialog(OrderDetailActivity.this, refundExplain);
                }
            }
        });
        mRecycleView.setAdapter(orderRecordShareListAdapter);
        if (isFromBocPay) {
            bocPayTransDetailEntity = (OrderQueryEntity) getIntent().getExtras().getSerializable(BOCPAYENTITY);
            mPresenter.getOrderDetailByBocPay(bocPayTransDetailEntity, "0", bocPayTransDetailEntity.getOuttrfrefno(), bocPayTransDetailEntity.getOrderType());
        } else {
            smartAccountTransDetailEntity = (SmartAccountBillListEntity.OutgroupBean) getIntent().getExtras().getSerializable(SMARTENTITY);
            mPresenter.getOrderDetailBySmartAccount(smartAccountTransDetailEntity, "1", smartAccountTransDetailEntity.getOuttxrefno(), smartAccountTransDetailEntity.getOrderType());
        }

        initBanner();
    }

    private void initBitmapImg(int position, String imageUrl, String shareText) {
        if (isFromBocPay) {
            if (position == -1) {
                ShareImageUtils.saveImageToGalleyForOrderDetail(OrderDetailActivity.this, mPresenter.dealDateWithBocPay(bocPayTransDetailEntity, true), mPresenter.transferIsLiShi(), imageUrl, shareText);
            } else {
                ShareImageUtils.shareOrderImageToOther(OrderDetailActivity.this, position, mPresenter.dealDateWithBocPay(bocPayTransDetailEntity, true), mPresenter.transferIsLiShi(), imageUrl, shareText);
            }
        } else {
            if (position == -1) {
                ShareImageUtils.saveImageToGalleyForOrderDetail(OrderDetailActivity.this, mPresenter.dealDateWithSmartAccount(smartAccountTransDetailEntity, true), mPresenter.transferIsLiShi(), imageUrl, shareText);
            } else {
                ShareImageUtils.shareOrderImageToOther(OrderDetailActivity.this, position, mPresenter.dealDateWithSmartAccount(smartAccountTransDetailEntity, true), mPresenter.transferIsLiShi(), imageUrl, shareText);
            }
        }
    }

    private void initBanner() {

        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {

            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_order_detail_new;
    }


    @Override
    public void getOrderDetailSuccess(ArrayList<ItemModel> response) {
        orderRecordShareListAdapter.refresh(response);
    }

    @Override
    public void getOrderDetailError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }


    @Override
    public void queryGradeInfoSuccess(SweptCodePointEntity sweptCodePointEntity) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        sweptCodePointEntity.setTxnId(bocPayTransDetailEntity.getOuttrfrefno());
        mHashMaps.put(GRADEREDUCEINFO, sweptCodePointEntity);
        ActivitySkipUtil.startAnotherActivity(getActivity(), GradeReduceActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    public void queryGradeInfoError(String errorCode, String errorMsg) {

        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
