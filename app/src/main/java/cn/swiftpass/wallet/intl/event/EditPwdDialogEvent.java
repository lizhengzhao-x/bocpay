package cn.swiftpass.wallet.intl.event;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import java.util.HashMap;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.MainContract;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.OldPaymentPswActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_MODIFYPASSWORD;

public class EditPwdDialogEvent extends BaseEventType {
    public static final String EDITPWDDIALOGEVENTNAME = "EditPwdDialogEvent";

    public EditPwdDialogEvent() {
        super(EVENT_MODIFYPASSWORD.eventPriority, EVENT_MODIFYPASSWORD.eventName);
    }

    @Override
    protected String getCurrentEventType() {
        return EDITPWDDIALOGEVENTNAME;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof EditPwdDialogDealOwner) {
            EditPwdDialogDealOwner lbsDialogDealOwner = (EditPwdDialogDealOwner) currentActivity;
            showEditPwdDialog(lbsDialogDealOwner);
            return true;
        }else {
            return false;
        }
    }

    public interface EditPwdDialogDealOwner extends EventDealOwner {

        MainContract.Presenter getCurrentPresenter();


    }

    /**
     * 修改支付密码弹框提示
     */
    private void showEditPwdDialog(EditPwdDialogDealOwner lbsDialogDealOwner) {
        MainContract.Presenter presenter = lbsDialogDealOwner.getCurrentPresenter();
        Activity activity = lbsDialogDealOwner.getCurrentActivity();
        Context context = lbsDialogDealOwner.getCurrentContext();
        if (presenter == null || activity == null || context == null) return;
        AndroidUtils.showTipDialog(activity, context.getString(R.string.TME1_1_0), context.getString(R.string.TME1_1_1),
                context.getString(R.string.TME1_1_3), context.getString(R.string.TME1_1_2), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.pwdTipCallServer();
                        HashMap<String, Object> mHashMaps = new HashMap<>();
                        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_RESET_PASSWORD);
                        ActivitySkipUtil.startAnotherActivity(activity, OldPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        presenter.pwdTipCallServer();
                    }
                });
    }
}
