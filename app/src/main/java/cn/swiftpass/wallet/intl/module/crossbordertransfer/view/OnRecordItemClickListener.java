package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderRecordEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.view
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 14:26
 * @change
 * @chang time
 * @class describe
 */
public interface OnRecordItemClickListener {
    void OnRecordItemClick(TransferCrossBorderRecordEntity recordEntity);
}
