package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;


public class TransferCrossBorderStatusContract {

    public interface View extends IView {

        void getTransferStatusSuccess(TransferCrossBorderStatusEntity response);

        void getTransferStatusError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {

        void getTransferStatus(String tnxid, String orderId);
    }

}
