package cn.swiftpass.wallet.intl.module.setting;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;

public class SettingActivity extends BaseCompatActivity {
    private FragmentManager mFragmentManager;
    private SettingFragment settingFragment;



    @Override
    public void init() {

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        settingFragment = new SettingFragment();

        fragmentTransaction.add(R.id.fl_main, settingFragment);
        fragmentTransaction.commitAllowingStateLoss();

    }



    @Override
    protected int getLayoutId() {
        return R.layout.act_setting;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }
}
