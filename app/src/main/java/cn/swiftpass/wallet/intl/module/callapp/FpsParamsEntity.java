package cn.swiftpass.wallet.intl.module.callapp;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class FpsParamsEntity extends BaseEntity {


    private String URL;
    private String callback;

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }


}
