package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.api.protocol.CardListProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.VirtualCardInfoContract;

/**
 * 主界面管理
 */
public class VirtualCardInfoPresenter extends BasePresenter<VirtualCardInfoContract.View> implements VirtualCardInfoContract.Presenter {
    @Override
    public void getCardList() {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CardListProtocol( HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getCardListSuccess(response);
                }
            }
        }).execute();
    }
}
