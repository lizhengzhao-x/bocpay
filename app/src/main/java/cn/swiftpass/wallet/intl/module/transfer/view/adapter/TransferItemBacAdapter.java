package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.utils.GlideApp;

/**
 * 派利是 横向背景图片
 */
public class TransferItemBacAdapter implements AdapterItem<TransferResourcesEntity.DefBean.BckImgsBean> {

    private ImageView id_sel_text_image;
    private ImageView id_sel_image;

    private Context mContext;
    private OnIemBacClickCallback onIemBacClickCallback;
    private int positon;
    private ImageView iv_transfer_item_select_contain;


    public TransferItemBacAdapter(Context context, OnIemBacClickCallback onIemBacClickCallbackIn) {
        this.mContext = context;
        this.onIemBacClickCallback = onIemBacClickCallbackIn;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.transfer_item_sel_img;
    }

    @Override
    public void bindViews(View root) {
        id_sel_text_image = root.findViewById(R.id.id_sel_img_bac);
        id_sel_image = root.findViewById(R.id.id_sel_image);
        iv_transfer_item_select_contain = root.findViewById(R.id.iv_transfer_item_select_contain);
    }

    @Override
    public void setViews() {
        id_sel_text_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onIemBacClickCallback != null) {
                    onIemBacClickCallback.onItemClick(positon);
                }
            }
        });
    }

    @Override
    public void handleData(TransferResourcesEntity.DefBean.BckImgsBean itemMoney, int positonIn) {
        positon = positonIn;
        id_sel_image.setImageResource(itemMoney.isSel() ? R.mipmap.icon_redpacketbg_selected : R.mipmap.icon_redpacketbg_default);
        GlideApp.with(mContext).load(AppTestManager.getInstance().getBaseUrl() + itemMoney.getBacImageUrl()).diskCacheStrategy(DiskCacheStrategy.NONE).into(id_sel_text_image);

        iv_transfer_item_select_contain.setVisibility(View.GONE);
        if(itemMoney.isSel()){
            iv_transfer_item_select_contain.setVisibility(View.VISIBLE);
        }

    }

    public static class OnIemBacClickCallback {
        public void onItemClick(int position) {
            // do nothing
        }
    }
}
