package cn.swiftpass.wallet.intl.module.setting.location;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.view.View;

import androidx.core.app.ActivityCompat;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LocationUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.LOCATION_TIP_ALREADY_SHOW;

public class LocationSettingActivity extends BaseCompatActivity {

    public static final String LOCATION_SP = "LOCATION_SP";
    private ImageArrowView layoutOpenLocation;

    /**
     * 是否第一次选择拒绝通讯录权限且不再询问权限
     */
    public boolean firstRejectContacts = true;


    /**
     * 用于记录页面lbs开关
     */
    private boolean isLocationServiceOpen = false;


    /**
     * 弹出相机权限选择框
     */
    private CustomDialog mPermissionDialog;

    /**
     * 是否是手动到设定里头打开lbs开关
     */
    private boolean isManuallyOpen = false;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.LBS209_5_1));
        layoutOpenLocation = (ImageArrowView) findViewById(R.id.layout_open_location);
        try {
            isLocationServiceOpen = (Boolean) HttpCoreSpUtils.get(LOCATION_SP, false);
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        initTouchView(isLocationServiceOpen);
        layoutOpenLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLocationServiceOpen) {
                    checkLocationService();
                } else {
                    initTouchView(false);
                    HttpCoreSpUtils.put(LOCATION_SP, false);
                    isLocationServiceOpen = false;
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isGranted(Manifest.permission.ACCESS_COARSE_LOCATION) && isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (isManuallyOpen && LocationUtils.isLocationEnabled(this)) {
                //从设置里头打开定位服务返回app
                isLocationServiceOpen = true;
                initTouchView(isLocationServiceOpen);
                addGpsServiceListener();
            } else {
                //进入界面如果开启功能 需要改更新最新位置
                if (isLocationServiceOpen && LocationUtils.isLocationEnabled(this)) {
                    addGpsServiceListener();
                } else {
                    closeOpen();
                }
            }
        } else {
            //权限关闭 要同步关闭
            closeOpen();
        }
    }

    /**
     * app使用过程中 用户关闭GPS开关
     */
    private void addGpsServiceListener() {
        LocationUtils.registerLocationWifi(this, 0, 0, new LocationUtils.OnLocationChangeListener() {
            @Override
            public void getLastKnownLocation(Location location) {

            }

            @Override
            public void onLocationChanged(Location location) {

            }

            @Override
            public void onLocationClose() {
                isLocationServiceOpen = false;
                initTouchView(false);
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }
        });
    }

    /**
     * 需求要求，如果用户在当前页面 app处于前台，用户在设定里头关闭权限或者关闭定位服务 app开关要同步关掉
     */
    private void closeOpen() {
        isLocationServiceOpen = false;
        initTouchView(false);
        HttpCoreSpUtils.put(LOCATION_SP, false);
    }


    /**
     * 打开LBS开关
     * 1.检测定位权限 权限管理
     * 2.检测定位服务 通过gps可用不可用
     */
    private void checkLocationService() {
        PermissionInstance.getInstance().getPermission(LocationSettingActivity.this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                getLocation();
            }

            @Override
            public void rejectPermission() {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(LocationSettingActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    if (!firstRejectContacts) {
                        showLackOfPermissionDialog();
                    } else {
                        firstRejectContacts = false;
                    }
                }
            }
        }, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    private void getLocation() {
        boolean isProviderEnabled = LocationUtils.isLocationEnabled(this);
        if (!isProviderEnabled) {
            showOpenLocationServiceDialog();
        } else {
            isLocationServiceOpen = true;
            initTouchView(isLocationServiceOpen);
        }
    }


    private void initTouchView(boolean open) {
        if (layoutOpenLocation == null) return;
        if (open) {
            layoutOpenLocation.setRightImage(R.mipmap.button_switch_on_s);
        } else {
            layoutOpenLocation.setRightImage(R.mipmap.button_switch_off_s);
        }
        HttpCoreSpUtils.put(LOCATION_SP, open);
        if (open){
            //如果存在打开开关 就不用在显示LBS开关
            HttpCoreSpUtils.put(LOCATION_TIP_ALREADY_SHOW, true);
        }
    }

    /**
     * 定位服务未开启 弹框提示 点击跳转系统打开配置
     */
    private void showOpenLocationServiceDialog() {
        LocationUIUtils.showOpenLocationServiceDialog(LocationSettingActivity.this, new LocationUIUtils.DialogSelListener() {
            @Override
            public void onSelOkListener() {
                isManuallyOpen = true;
                LocationUtils.openGpsSettings(LocationSettingActivity.this);
            }

            @Override
            public void onSelCancelListener() {

            }
        });
    }


    /**
     * 定位权限
     */
    private void showLackOfPermissionDialog() {
        if (mPermissionDialog != null && mPermissionDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(getString(R.string.LBS209_8_1));
        builder.setMessage(getString(R.string.LBS209_8_2));
        builder.setPositiveButton(getString(R.string.LBS209_8_4), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(getString(R.string.LBS209_8_3), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //弹框提示打开定位权限
                isManuallyOpen = true;
                AndroidUtils.startAppSetting(LocationSettingActivity.this);
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = getActivity();
        if (mActivity != null && !mActivity.isFinishing()) {
            mPermissionDialog = builder.create();
            mPermissionDialog.setCancelable(false);
            mPermissionDialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocationUtils.unregister();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_location;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }
}
