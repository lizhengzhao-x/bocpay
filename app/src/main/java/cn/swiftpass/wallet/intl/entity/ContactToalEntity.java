package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class ContactToalEntity extends BaseEntity {

    private List<String> phoneNumbers;

    public ContactToalEntity() {
        this.phoneNumbers = new ArrayList<>();
        this.emailAddress = new ArrayList<>();
    }

    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(List<String> emailAddress) {
        this.emailAddress = emailAddress;
    }

    private String userName;
    private List<String> emailAddress;

    @Override
    public String toString() {
        return userName + " " + phoneNumbers.toString() + " " + emailAddress.toString();
    }
}
