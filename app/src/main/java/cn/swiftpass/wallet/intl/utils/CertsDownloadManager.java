package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.widget.CustomProgressDialog;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 证书下载
 * 1.7月版证书下载功能 只涉及到Idv/FRP/其他webview证书下载的证书
 * 2.Pa注册/绑卡/忘记密码 需要涉及到idv/frp证书
 * 3.webview页面需要一些域名检测的证书列表(银联互通 最新消息里头的一下外部重定向网页校验)
 * 4.需要用到下载证书的地方调用 有两个方法 1）异步返回 2）同步返回
 * 5.tnc 参与证书校验 不参与到网络异常的问题
 * 6.证书md5校验
 */
public class CertsDownloadManager {
    private static CertsDownloadManager instance;
    private static final Object INSTANCE_LOCK = new Object();
    private static final String TAG = "CertsDownloadManager";
    private Certificate[] mCertificates;
    private CustomProgressDialog progressDialog;
    /**
     * 下载压缩证书的连接
     */
    private String DOWNLOADCERTURL = AppTestManager.getInstance().getBaseUrl() + "api/sys/certs";

    public static CertsDownloadManager getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new CertsDownloadManager();
                }
            }
        }
        return instance;
    }

    /**
     * 只有当证书下载成功了 才可以直接调用这个方法  主要是为了解决activity传参无法序列化问题 需要内存存放
     *
     * @return
     */
    public Certificate[] getDownloadCerts() {

        return mCertificates;
    }


    /**
     * 证书初始化，提前下载证书配置
     */
    public void initCertificate() {
        loadCertificates(null, false, null);
    }

    /**
     * 异步拉取证书
     *
     * @param mContext
     * @param showDialog             是否显示dialog阻塞
     * @param onCertDownLoadListener
     */
    public void loadCertificates(Context mContext, boolean showDialog, OnCertDownLoadListener onCertDownLoadListener) {

        if (mCertificates != null && mCertificates.length > 0) {
            LogUtils.i(TAG, "Certificates ALREADY EXIT SUCCESS BACK");
            onCertDownLoadListener.onCertsDownLoaFinish(true, mCertificates);
        } else {
            if (!NetworkUtil.isNetworkAvailable()) {
                LogUtils.i(TAG, "Certificates NOT EXIT NETWORK ERROR");
                if (onCertDownLoadListener != null) {
                    onCertDownLoadListener.onCertsDownLoaFinish(false, null);
                }
                return;
            }
            LogUtils.i(TAG, "Certificates NOT EXIT START DOWN");
            new DownLoadCertsTask(onCertDownLoadListener, mContext, showDialog).execute();
        }
    }

    /**
     * 耗时任务读取通讯录
     */
    private class DownLoadCertsTask extends AsyncTask<Void, Void, Certificate[]> {
        private OnCertDownLoadListener onCertDownLoadListener;

        private boolean showDialog;
        private Context mContext;

        public DownLoadCertsTask(OnCertDownLoadListener onCertDownLoadListener, Context mContext, boolean showDialogIn) {
            this.onCertDownLoadListener = onCertDownLoadListener;
            this.showDialog = showDialogIn;
            this.mContext = mContext;
        }

        @Override
        protected Certificate[] doInBackground(Void... voids) {
            LogUtils.i(TAG, " down load doInBackground ->" + Thread.currentThread().getName());
            return downLoadCertificates();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LogUtils.i(TAG, "down load onPreExecute->");
            //看需不需要加载框显示
            if (showDialog && mContext != null) {
                showDialogNotCancel(mContext);
            }

        }

        @Override
        protected void onPostExecute(Certificate[] contactEntities) {
            super.onPostExecute(contactEntities);
            LogUtils.i(TAG, "down load onPostExecute->");
            if (onCertDownLoadListener != null) {
                //证书下载成功 缓存到内存中
                mCertificates = contactEntities;
                onCertDownLoadListener.onCertsDownLoaFinish(contactEntities != null, contactEntities);
                if (showDialog && mContext != null) {
                    dismissDialog();
                }
            }
        }
    }

    /**
     * 加锁 阻塞 防止并发请求
     *
     * @return
     */
    private synchronized Certificate[] downLoadCertificates() {
        if (mCertificates != null && mCertificates.length > 0) {
            return mCertificates;
        }
        LogUtils.i(TAG, "downLoadCertificates start ->");
/*        //1.首先通过第一个getValue接口 拿到压缩证书zip的下载地址
        JSONObject mBodyParams = new JSONObject();
        mBodyParams.put(RequestParams.ACTIVITIES_NAME, DOWNLOADCERTURLPARAMS);
        HashMap<String, Object> headMap = new HashMap<>();
        headMap.put(NetworkConstants.IGNORE_INTERCEPTOR, NetworkConstants.STATUS_TRUE);
        headMap.put(NetworkConstants.NEED_LOGIN, NetworkConstants.STATUS_FALSE);
        //忽略拦截器的操作
        NetWorkEntity netWorkEntity = OkHttpManager.getInstance().doPostSyn(GET_DOWNLOAD_URL, headMap, makePostData(mBodyParams).toJSONString());
        if (!netWorkEntity.success) {
            //网络异常等原因接口请求失败
            return null;
        }

        String responseBodyStr = null;
        try {
            if (netWorkEntity.response.body() != null) {
                responseBodyStr = netWorkEntity.response.body().string();
            }
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(responseBodyStr)) {
            return null;
        }
        CommonEntity originalEntity = new Gson().fromJson(responseBodyStr, CommonEntity.class);
        if (originalEntity == null || TextUtils.isEmpty(originalEntity.getContent())) {
            return null;
        }
        ContentEntity downLoadCertUrlEntity = new Gson().fromJson(originalEntity.getContent(), ContentEntity.class);
        if (downLoadCertUrlEntity == null || TextUtils.isEmpty(downLoadCertUrlEntity.getData())) {
            //数据解析异常 理论上不应该存在
            return null;
        }

        UrlData urlData = new Gson().fromJson(downLoadCertUrlEntity.getData(), UrlData.class);
        if (urlData == null || TextUtils.isEmpty(urlData.getValue())) {
            //数据解析异常 理论上不应该存在
            return null;
        }*/
//        DownLoadCertUrlEntity downLoadCertUrlEntity = new DownLoadCertUrlEntity();
//        downLoadCertUrlEntity.setDownLoadCertUrl(DOWNLOADCERTURL);
        //1.通过证书下载地址进行证书下载
        OkHttpClient okHttpClient = OkHttpManager.getInstance().getOkHttpClientNoInterceptor();
        Request request = new Request.Builder()
                .url(DOWNLOADCERTURL)
                .addHeader("Connection", "close")
                .build();
        List<Certificate> certificates = new ArrayList<>();
        ZipInputStream zin = null;
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful() && response.body() != null) {
                zin = new ZipInputStream(response.body().byteStream());
                ZipEntry entry;
                //如果entry不为空，并不在同一个目录下
                while (((entry = zin.getNextEntry()) != null)) {
                    //解压出的文件路径
                    if (!entry.isDirectory()) {
                        String fileName = entry.getName();
                        //byte[] fileContent = entry.getExtra();
                        LogUtils.i(TAG, "fileName: " + fileName);
                        try {
                            // 获取当前条目的字节数组
                            byte[] data = getByte(zin);
                            //这里要做异常捕获 防止多张证书 其中有些证书格式不对 直接异常 期待的是 格式正确的证书都应该读取到内存中
                            InputStream is = new ByteArrayInputStream(data);
                            X509Certificate certificate = (X509Certificate) CertificateFactory.getInstance("X509").generateCertificate(is);
                            certificates.add(certificate);
                        } catch (CertificateException e) {
                            if (BuildConfig.isLogDebug) {
                                e.printStackTrace();
                            }
                        }
                        zin.closeEntry();
                    }
                }
                zin.close();
            } else {
//                //网络或者解压缩问题失败
            }
        } catch (Exception e) {
            try {
                if (zin != null) {
                    zin.close();
                }
            } catch (Exception ex) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
        }
        LogUtils.i(TAG, "downLoadCertificates success->");
        mCertificates = certificates.toArray(new Certificate[certificates.size()]);
        return mCertificates;
    }


//    /**
//     * 需要上传的header和body参数字段
//     *
//     * @param mBodyParams
//     * @return
//     */
//    private JSONObject makePostData(JSONObject mBodyParams) {
//        JSONObject jsonObject = new JSONObject();
//        try {
//            //摘要，签名处理  body加密放在加密拦截器处理
//            jsonObject.put("header", HeaderUtils.getHeaders());
//            jsonObject.put("body", mBodyParams);
//            LogUtils.i(TAG, "requestparams: " + jsonObject.toString());
//        } catch (Exception e) {
//            LogUtils.e(TAG, Log.getStackTraceString(e));
//        }
//        return jsonObject;
//    }


    /**
     * 获取条目byte[]字节
     *
     * @param zis
     * @return
     */
    public byte[] getByte(InflaterInputStream zis) {
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            byte[] temp = new byte[1024];
            byte[] buf = null;
            int length = 0;
            while ((length = zis.read(temp, 0, 1024)) != -1) {
                bout.write(temp, 0, length);
            }
            buf = bout.toByteArray();
            bout.close();
            return buf;
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
            return null;
        }
    }


    public void dismissDialog() {
        if (progressDialog == null) {
            return;
        }
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                progressDialog = null;
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
    }


    public void showDialogNotCancel(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.dialog_message));
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
                LogUtils.e(TAG, e.getMessage());
            }
        } else {
            if (progressDialog != null) {
                try {
                    Activity activity = (Activity) context;
                    if (!activity.isFinishing()) {
                        progressDialog.setCancel(false);
                        progressDialog.show();
                    }
                } catch (Exception e) {
                    LogUtils.e(TAG, e.getMessage());
                }

            }
        }

    }


    public interface OnCertDownLoadListener {
        void onCertsDownLoaFinish(boolean isSuccess, Certificate[] certificates);
    }


//    public static class UrlData extends BaseEntity {
//        /**
//         * value : http://mbasit1.ftcwifi.com/json/certs.zip
//         */
//        private String value;
//
//        public void setValue(String value) {
//            this.value = value;
//        }
//
//        public String getValue() {
//            return value;
//        }
//    }

}
