package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 信用卡登记查询成功界面
 */
public class RewardRegisterCheckSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.id_top_success_img)
    ImageView idTopSuccessImg;
    @BindView(R.id.id_reward_status)
    TextView idRewardStatus;
    @BindView(R.id.id_reward_number_title)
    TextView idRewardNumberTitle;
    @BindView(R.id.id_reward_number)
    TextView idRewardNumber;
    @BindView(R.id.id_reward_back)
    TextView idRewardBack;
    public static final String REWARDNUMBER = "REWARDNUMBER";

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.CR209_11_1);
        idRewardBack.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                MyActivityManager.removeAllTaskExcludeMainStack();
            }
        });

        String ewardNumber = (String) getIntent().getExtras().get(REWARDNUMBER);
        idRewardNumber.setText(ewardNumber);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_rewardregister_check_success;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
    }
}
