package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Intent;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferFirstSendOtpContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferSendOtpPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.view.AbstractSendOTPActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity.REQUEST_CALLAPP_PAYMENT;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferConfirmMoneyActivity.TRANSFER_APPCALLAPP_PAYMENT;

/**
 * 第一次转账需要发送otp
 */
public class TransferFirstSendOtpActivity extends AbstractSendOTPActivity implements TransferFirstSendOtpContract.View {

    private TransferFirstSendOtpContract.Presenter mPresenterDetail;
    public static final String EXTRA_TRANSFERCHECK = "EXTRA_TRANSFERCHECK";
    private String mTnxId;
    private boolean mIsLiShi;
    private boolean mIsBocPayTransfer;
    /**
     * 确定金额前需预检查
     */
    protected TransferPreCheckEntity mPreCheckEntity;
    private int currentPaymentType;

    @Override
    public void transferFirstSenOtpSuccess(ContentEntity response, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void transferFirstSenOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(TransferFirstSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                showRetrySendOtp();
            }
        });
    }

    /**
     * EVENT_TRANSFER_SUCCESS 转账成功 关闭当前页面
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS
                || event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
    }


    @Override
    public void transferFirstVerifyOtpSuccess(ContentEntity response) {
    }

    @Override
    public void transferFirstVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(TransferFirstSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }

    @Override
    public void transferConfrimSuccess(TransferCompleteEntity response) {
        TransferConfirmReq mTransferComplete = new TransferConfirmReq();
        mTransferComplete.transferAmount = mPreCheckEntity.getDrAmt();
        mTransferComplete.bankType = mPreCheckEntity.getPreCheckReq().bankType;
        mTransferComplete.scrRefNo = mPreCheckEntity.getScrRefNo();
        mTransferComplete.isLiShi = mPreCheckEntity.getPreCheckReq().isLiShi;
        mTransferComplete.transferType = mPreCheckEntity.getTransferType();
        mTransferComplete.paymentCategory = mPreCheckEntity.getPaymentCategory();
        mTransferComplete.currency = mPreCheckEntity.getSrvcChrgDrCur();
        mTransferComplete.tansferAccount = mPreCheckEntity.getAcNo();

        mTransferComplete.bocpayRedPackeNotifyTip = response.getBocpayRedPackeNotifyTip();
        mTransferComplete.tansferToAccount = response.getCrDisplayedEngNm();
        mTransferComplete.collectionAccount = response.getCollectionAccount();
        mTransferComplete.billReference = response.getBillReference();
        mTransferComplete.mSmartLevel = response.getSmartAcLevel();
        mTransferComplete.transferDate = response.getTransferDate();
        mTransferComplete.transferBank = response.getTransferBank();
        mTransferComplete.dbtrNm = response.getDbtrNm();
        mTransferComplete.txnId = response.getTxnId();
        mTransferComplete.extCmt = response.getExtCmt();
        mTransferComplete.preBank = response.getPreBank();
        mTransferComplete.isBocPayTransfer = mPreCheckEntity.isBocPayTransfer();
        mTransferComplete.bckImgsBean = mPreCheckEntity.getBckImgsBean();
        mTransferComplete.smartAcLevel = response.getSmartAcLevel();
        mTransferComplete.transferType = mPreCheckEntity.getTransferType();
        if (response.getPostscript() != null) {
            mTransferComplete.postscript = response.getPostscript();
        }
        mHashMaps = new HashMap<>();
        mHashMaps.put(TransferConst.TRANS_FPS_CHECK_DATA, mTransferComplete);
        mHashMaps.put(TransferConst.TRANS_COLLECTION_TYPE, response.getPaymentCategory());
        if (MyActivityManager.isAppOnForeground(this)) {
            if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
                mHashMaps.put(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
                ActivitySkipUtil.startAnotherActivityForResult(this, TransferSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_CALLAPP_PAYMENT);
                mHashMaps = null;
                finish();
            } else {
                ActivitySkipUtil.startAnotherActivity(this, TransferSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                mHashMaps = null;
                finish();
            }
        }
//        Intent intent = new Intent(TransferFirstSendOtpActivity.this, TransferSuccessActivity.class);
//        intent.putExtra(TransferConst.TRANS_FPS_CHECK_DATA, mTransferComplete);
//        intent.putExtra(TransferConst.TRANS_COLLECTION_TYPE, response.getPaymentCategory());
//        if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
//            intent.putExtra(Constants.TYPE, TRANSFER_APPCALLAPP_PAYMENT);
//            startActivityForResult(intent, REQUEST_CALLAPP_PAYMENT);
//            finish();
//        } else {
//            startActivity(intent);
//            finish();
//        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mHashMaps != null) {
            if (currentPaymentType == TRANSFER_APPCALLAPP_PAYMENT) {
                ActivitySkipUtil.startAnotherActivityForResult(this, TransferSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN, REQUEST_CALLAPP_PAYMENT);
                mHashMaps = null;
                finish();
            } else {
                ActivitySkipUtil.startAnotherActivity(this, TransferSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                mHashMaps = null;
                finish();
            }
        }
    }

    @Override
    public void transferConfirmError(final String errorCode, String errorMsg) {
        if (errorCode.equals(ErrorCode.TASNSFER_ORDER_ERROR.code)) {
            TransferFailedActivity.startActivity(TransferFirstSendOtpActivity.this);
        } else {
            showErrorMsgDialog(TransferFirstSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        }
    }


    @Override
    public void init() {
        if (mPresenter instanceof TransferFirstSendOtpContract.Presenter) {
            mPresenterDetail = (TransferFirstSendOtpContract.Presenter) mPresenter;
        }
        idSubTitle.setText(getString(R.string.IDV_3a_20_3));
        mPreCheckEntity = (TransferPreCheckEntity) getIntent().getExtras().getSerializable(EXTRA_TRANSFERCHECK);
        mTnxId = mPreCheckEntity.getScrRefNo();
        mIsLiShi = mPreCheckEntity.getPreCheckReq().isLiShi;
        mIsBocPayTransfer = mPreCheckEntity.isBocPayTransfer();
        phoneNo = mPreCheckEntity.getMobile();
        currentPaymentType = getIntent().getExtras().getInt(Constants.TYPE);
        if (TextUtils.isEmpty(mPreCheckEntity.getPaymentCategory()) || TextUtils.equals(mPreCheckEntity.getPaymentCategory(), TransferConst.TRANS_TYPE_PERSON)) {
            setToolBarTitle(R.string.TF2101_5_1);
            String transferType = mPreCheckEntity.getTransferType();
            //转账确认界面 如果是商户 显示付款 如果是个人，根据不同转账方式显示
            if (TextUtils.equals(transferType, TransferConst.TRANS_FPS)) {
                setToolBarTitle(R.string.TF2101_26_1);
            } else if (TextUtils.equals(transferType, TransferConst.TRANS_CUSTOMER_ACCOUNT)) {
                setToolBarTitle(R.string.TF2101_30_1);
            } else if (TextUtils.equals(transferType, TransferConst.TRANS_TYPE_PHONE)) {
                setToolBarTitle(R.string.TF2101_5_1);
            } else {
                setToolBarTitle(R.string.TF2101_5_1);
            }
        } else {
            setToolBarTitle(R.string.FPSBP2101_1_1);
        }
        if (mIsLiShi) {
            setToolBarTitle(R.string.RP1_2_6);
        }
        super.init();
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.transferFirstVerifyOtp(mTnxId, getVerifyCode(), mPreCheckEntity, mIsLiShi, mIsBocPayTransfer);
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {

        mPresenterDetail.transferFirstSenOtp(mTnxId, isTryAgain);
    }

    @Override
    protected IPresenter createPresenter() {
        return new TransferSendOtpPresenter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CALLAPP_PAYMENT && resultCode == RESULT_OK) {
            Intent intent = new Intent();
            if (data != null) {
                intent.putExtras(data);
            }
            setResult(resultCode, intent);
            finish();
        }
    }

}
