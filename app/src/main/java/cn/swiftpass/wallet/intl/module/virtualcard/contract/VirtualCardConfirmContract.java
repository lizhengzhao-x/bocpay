package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;


public class VirtualCardConfirmContract {

    public interface View extends IView {

        void getVirtualCardListSuccess(VirtualCardListEntity response);

        void getVirtualCardListError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {
        /**
         * 拉取虚拟卡列表
         */
        void getVirtualCardList();
    }

}
