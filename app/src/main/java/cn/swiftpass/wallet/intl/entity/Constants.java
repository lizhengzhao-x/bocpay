package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.manage.AppTestManager;


public class Constants {
    public static final String BUILD_FLAVOR_PRD = "prd";
    public static final String BUILD_FLAVOR_UAT = "uat";
    public static final String BUILD_FLAVOR_SIT = "sit";
    public static final String BUILD_FLAVOR_UAT_F = "uatF";
    public static String REGISTER_VIDEO_URL;

    public static String TERM_REGISTER_ZN = null;
    public static String TERM_REGISTER_HK = null;
    public static String TERM_REGISTER_ENG = null;

    public static List<String> WHITELISTDOMAINLIST = new ArrayList<>();

    static {
        if (TextUtils.equals(BuildConfig.FLAVOR, BUILD_FLAVOR_PRD)) {
            WHITELISTDOMAINLIST.add("https://ewa.bochk.com");
            WHITELISTDOMAINLIST.add("https://www.bochk.com");
            WHITELISTDOMAINLIST.add("https://mobile.boccc.com.hk");
            REGISTER_VIDEO_URL = "https://www.bochk.com/creditcard/BOCPAY/video/IDV.mp4";

            TERM_REGISTER_ZN = "https://www.bochk.com/creditcard/BOCPAY/notices/smart_account_notes/smp/";
            TERM_REGISTER_HK = "https://www.bochk.com/creditcard/BOCPAY/notices/smart_account_notes/chi/";
            TERM_REGISTER_ENG = "https://www.bochk.com/creditcard/BOCPAY/notices/smart_account_notes/eng/ ";

        } else if (TextUtils.equals(BuildConfig.FLAVOR, BUILD_FLAVOR_UAT) || TextUtils.equals(BuildConfig.FLAVOR, BUILD_FLAVOR_SIT)|| TextUtils.equals(BuildConfig.FLAVOR, BUILD_FLAVOR_UAT_F)) {
            WHITELISTDOMAINLIST.add("https://ewa.bochk.com");
            WHITELISTDOMAINLIST.add("https://www.bochk.com");
            WHITELISTDOMAINLIST.add("https://mobile.boccc.com.hk");
            WHITELISTDOMAINLIST.add("https://whkbcuat.ftcwifi.com");
            WHITELISTDOMAINLIST.add("https://mobileuat.boccc.com.hk");
            WHITELISTDOMAINLIST.add("http://mbasit1.ftcwifi.com");

            WHITELISTDOMAINLIST.add("https://open.unionpay.com");
            WHITELISTDOMAINLIST.add("http://180.153.224.42:8848");
            WHITELISTDOMAINLIST.add("https://202.127.170.71");
            WHITELISTDOMAINLIST.add("https://202.127.170.70");
            WHITELISTDOMAINLIST.add("https://qr.95516.com");
            WHITELISTDOMAINLIST.add("http://180.153.224.42:8848");
            WHITELISTDOMAINLIST.add("https://qr.95516.com");
            WHITELISTDOMAINLIST.add("https://qr.test.95516.com");
            WHITELISTDOMAINLIST.add("https://testpos.ebankunion.com");
            WHITELISTDOMAINLIST.add("https://iservice-uat.boccc.com.hk");
            WHITELISTDOMAINLIST.add("https://marketing-test.unionpayintl.com");
            WHITELISTDOMAINLIST.add("https://shawn-dev.wepayez.com");
            REGISTER_VIDEO_URL = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/video/IDV.mp4";

            TERM_REGISTER_ZN = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/notices/smart_account_notes/smp/";
            TERM_REGISTER_HK = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/notices/smart_account_notes/chi/";
            TERM_REGISTER_ENG = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/notices/smart_account_notes/eng/";
        }
    }

    //新增
    public static final String WALLET_ID = "WALLET_ID";
    public static final String EXDATE = "EXDATE";
    public static final String QRCTYPE = "QRCTYPE";
    public static final String CARDID = "CARDID";
    public static final String EXTRA_COUNTRY_CODE = "EXTRA_COUNTRY_CODE";
    public static final String EXTRA_REMREMBER_TYPE = "EXTRA_REMREMBER_TYPE";
    public static final int EXTRA_REMREMBER_TYPE_LOGIN = 1;
    public static final int EXTRA_REMREMBER_TYPE_PAYMENT = 2;
    public static final int CHECKEDDA = 1;
    public static final int ADDEDDA = 2;
    public static final String EXTRA_VERIFY_TYPE = "EXTRA_VERIFY_TYPE";
    public static final String DATA_ORDER_NUMBER = "DATA_ORDER_NUMBER";
    public static final String DATA_QRCODE = "DATA_QRCODE";
    public static final String DATA_CARD_NUMBER = "DATA_CARD_NUMBER";
    public static final String DATA_PHONE_NUMBER = "DATA_PHONE_NUMBER";
    public static final String VIRTUALCARDLISTBEAN = "VIRTUALCARDLISTBEAN";
    public static final String VIRTUALCARDID = "VIRTUALCARDID";
    public static final String ISUPDATETOJANUARYVERSION = "ISUPDATETOJANUARYVERSION";

    public static final String FILENAME = "shareImage.jpg";

    public static final String DATA_OTP_NUMBER = "DATA_OTP_NUMBER";
    public static final String TRANSFER_IS_LISHI = "TRANSFER_IS_LISHI";
    public static final String TRANSFER_ITEM_LIST = "TRANSFER_ITEM_LIST";
    public static final String AUTO_SHOW_PACKAGE = "AUTO_SHOW_PACKAGE";
    //虚拟卡 通知栏MsgType
    public static final String MSGTYPE_VCNTF = "VCNTF";
    //消费奖赏 通知栏MsgType
    public static final String MSGTYPE_CONRE = "CONRE";
    public static final String TRANSACTIONUNIQUEID = "transactionUniqueID";
    public static final String TRANSACTIONTYPE = "transactionType";
    public static final String MIDVSEQFID = "MIDVSEQFID";
    public static final String VOICEENABLE = "voiceEnable";
    public static final String SHOWOTHERINFO = "SHOWOTHERINFO";
    public static final String MIDVSEQNUMBER = "MIDVSEQNUMBER";
    public static final String ACTION_TYPE = "ACTION_TYPE";
    public static final String QRINFO = "QRINFO";
    public static final String CARD_ENTITY_POSITION = "CARD_ENTITY_POSITION";
    public static final String FORGOT_PASSCODE_IS_GO_HOME = "FIS_GO_HOME";
    //设置密码时的第一次密码
    public static final String DATA_OLD_PWD = "DATA_OLD_PWD";
    public static final String TOP_HEADER_CHAR = "$";
    public static final String TOP_HEADER_CHAR_CUSTOM = "^";
    public static final String TOP_HEADER_CHARALL = "$$$";
    //旧密码
    public static final String DATA_OLD_PAY_PWD = "DATA_OLD_PAY_PWD";
    public static final String DATA_TRANSFER_ACCOUNTID_LIST = "DATA_TRANSFER_ACCOUNTID_LIST";
    public static final String UPLAN_ENTITY = "UPLAN_ENTITY";
    public static final String CARD_ENTITY = "CARD_ENTITY";
    public static final String TNX_ID = "TNX_ID";
    public static final String TXNTYPE = "TXNTYPE";
    public static final String OUTTRFREFNO = "OUTTRFREFNO";
    public static final String URLQRINFO_ENTITY = "URLQRINFO_ENTITY";
    public static final String GRADE_REDUCE_INFO = "GRADE_REDUCE_INFO";
    /**
     * 目前这个参数用于区分流程 有些页面公用的
     */
    public static final String CURRENT_PAGE_FLOW = "CURRENT_PAGE_FLOW";
    public static final String IS_NEED_EDIT_PWD = "IS_NEED_EDIT_PWD";
    public static final String IS_NEED_JUMP_NOTIFICATION = "IS_NEED_JUMP_NOTIFICATION";
    public static final String IS_NEED_SHOW_REGISTER_POPUP_BANNER = "IS_NEED_SHOW_REGISTER_POPUP_BANNER";
    public static final String ECOUPONCONVERTENTITY = "ECOUPONCONVERTENTITY";
    public static final String IS_FROM_LOGOUT_DEEP_LINK = "IS_FROM_LOGOUT_DEEP_LINK";
    public static final String VIRTUALCARDREGISTERVERIFINVO = "VIRTUALCARDREGISTERVERIFINVO";

    public static final String VIRTUALCARDLIST = "VIRTUALCARDLIST";
    public static final String DATA_ID_CARD = "DATA_ID_CARD";
    public static final String IS_MAJOR_SCAN = "IS_MAJOR_SCAN";
    public static final String IS_FROM_ORDERDETAIL = "IS_FROM_ORDERDETAIL";
    public static final String IS_CREDIT_CARD = "IS_CREDIT_CARD";
    public static final String OCR_INFO = "OCR_INFO";
    public static final String REG_EDDA_INFO = "REG_EDDA_INFO";
    public static final String CURRENT_BANK_CARD = "CURRENT_BANK_CARD";
    public static final String REGISTER_SUCCESS_DATA = "REGISTER_SUCCESS_DATA";
    public static final String CUSTOMER_INFO = "CUSTOMER_INFO";
    public static final String SMART_LEVEL = "SMART_LEVEL";
    public static final String HASCREDITCARD = "hasCreditCard";
    public static final String HASVIRTUALCARD = "hasVirtualCard";
    public static final String MOBILE_PHONE = "MOBILE_PHONE";
    public static final String CUSTOMID = "CUSTOMID";
    public static final String MAX_LITMIT_INFO = "MAX_LITMIT_INFO";
    public static final String ADJUST_LIMIT_TYPE = "ADJUST_LIMIT_TYPE";
    public static final String ADJUST_LIMIT_MAX = "ADJUST_LIMIT_MAX";
    public static final String ACCOUNT_BALANCE_INFO = "ACCOUNT_BALANCE_INFO";
    public static final String DATA_ID_FILE_PATH = "DATA_ID_FILE_PATH";
    public static final String SCAN_CARD_TYPE = "SCAN_CARD_TYPE";
    public static final String QRCODE_CONTENT = "QRCODE_CONTENT";
    public static final String ISAGGREGATIONCODE = "isAggregationCode";
    public static final String DATA_SMART_ACTION = "DATA_SMART_ACTION";
    public static final String DATA_SMART_INFO_ACTION = "SMART_INFO";
    public static final String DATA_NEW_DEFAULT_PANID = "DATA_NEW_DEFAULT_PANID";
    public static final String DATA_QRCODE_STR = "DATA_QRCODE_STR";
    public static final String PAN_IMG_URL = "PAN_IMG_URL";

    public static final String DATA_LIMIT = "DATA_LIMIT";
    public static final String USERID_EXTRA = "USERID_EXTRA";
    public static final String DATA_ADD_METHOD = "addedmethod";
    public static final String REGISTER_CUSTOM_INFO = "REGISTER_CUSTOM_INFO";
    public static final String LAST_SELECT_COUNTRY = "LAST_SELECT_COUNTRY";
    public static final String RECORD_INVITE_LIST = "RECORD_INVITE_LIST";
    public static final String DATA_BACK_SEL_TYPE_INTERNATIONALTION = "DATA_BACK_SEL_TYPE_INTERNATIONALTION";
    public static final String REDMID = "redmId";
    public static final String GIFTTP = "giftTp";
    public static final String REDEEMRESPONSEENTITY = "RedeemResponseEntity";
    public static final String EVOUCHERLISTBEANLIST = "EVOUCHERLISTBEANLIST";
    public static final String USEDISCOUNT = "USEDISCOUNT";
    public static final String SMART_TYPE = "SMART_TYPE";

    public static final String DISPLAY_UPLAN_DIALOG = "IS_SHOW_UPLAN_DIALOG";


    public static final String DATA_CHINESE_NAME = "DATA_CHINESE_NAME";
    public static final String DATA_ENGLISH_NAME = "DATA_ENGLISH_NAME";
    public static final String DATA_IDCARD_NUMBER = "DATA_IDCARD_NUMBER";
    public static final String DATA_DATEOFBIRTH = "DATA_DATEOFBIRTH";
    public static final String DATA_GENDER = "DATA_GENDER";
    public static final String DATA_NATIONLITY = "DATA_NATIONLITY";
    public static final String CURRENTLIMIT = "CURRENTLIMIT";

    public static final String BOCPAY = "BocPay";
    public static final String FPS = "FPS";

    public static final String ACCOUNT_TYPE = "ACCOUNT_TYPE";
    public static final String REWARDTIP = "REWARDTIP";
    public static final String SHOWINVITEPAGE = "SHOWINVITEPAGE";
    public static final String HAVE_VC_CARD = "HaveVCCard";

    public static final String TRANSFER_BOCPAY = "TRANSFER_BOCPAY";
    public static final String TRANSFER_FPS = "TRANSFER_FPS";
    public static final String TRANSFER_BANK_ACCOUNT = "TRANSFER_BANK_ACCOUNT";
    public static final String TRANSFER_EMAIL = "TRANSFER_EMAIL";

    public static final String ACCOUNT_PASSCODE = "account_code";
    public static final String ACCOUNT_MOBILE = "account_moibile";
    public static final String SPECIAL_LETTER_TEXT = "$$$------&&&";
    public static final String GLOBALROAMINGLISTBEAN = "GlobalRoamingListBean";
    public static final String SPECIAL_LETTER_BANK_TEXT = "####------####";


    public static final String USER_ACCOUNT = "user_account";
    public static final String USE_BIOMETRICAUTH = "USE_BIOMETRICAUTH";
    //我的账户
    public static final String ACCOUNT_TYPE_SMART = "2";
    //信用卡
    public static final String ACCOUNT_TYPE_CREDIT = "1";


    public static final String ZERA_STR = "0";
    public static final int SCAN_TO_TRANSFER = 100;
    public static final int SCAN_TO_FPS = 100;

    public static final int MAX_CHINA_PHONE_LENGTH = 11;


    public static final int FINGERLOCKEDOPENTIME = 40;//指纹错5次之后 暂定40s之后才可以重新使用指纹功能

    //    MPEMV, MPURL
    //E代表EMV码，U代表url码
    public static final String MAKE_ORDER_MPEMV = "E";
    public static final String MAKE_ORDER_MPURL = "U";


//    public static final int MAJOR_SCAN_QR_TYPE_URL = 0;
//    public static final int MAJOR_SCAN_QR_TYPE_EVI = 1;

//    public static final int TRANSFER_ACCOUNT_EMAIL = 0;
//    public static final int TRANSFER_ACCOUNT_PHONE = 1;

    public static final String TRANSFER_SEL_ACCOUNT_EMAIL = "1";
    public static final String TRANSFER_SEL_ACCOUNT_PHONE = "0";

    public static final int SCAN_CARD_TYPE_BACK = 0;
    public static final int SCAN_CARD_TYPE_MAJOR = 1;

    public static final int PAGE_FLOW_BIND_CARD = 0;

    public static final int PAGE_FLOW_REGISTER = 1;
    public static final int DATA_CARD_TYPE_LOGIN = 2;
    public static final int PAGE_FLOW_FORGETPASSWORD = 3;
    public static final int DATA_UPDATE_DAILY_LIMIT = 4;
    public static final int DATA_CHANGE_DEFAULT_ACCOUNT = 8;
    public static final int DATA_UPDATE_RECHARGETYPE = 5;
    public static final int DATA_UPDATE_SUSPEND = 6;
    /**
     * Pa账户注册
     */
    public static final int PAGE_FLOW_REGISTER_PA = 7;
    /**
     * 支付账户忘记密码
     */
    public static final int PAGE_FLOW_FORGETPASSWORD_PA = 9;
    /**
     * 信用卡绑定Pa账户
     */
    public static final int PAGE_FLOW_BIND_PA = 10;
    /**
     * 重设密码
     */
    public static final int PAGE_FLOW_RESET_PASSWORD = 4;

    public static final int SMART_ACCOUNT_UPDATE = 11;
    public static final int SMART_ACCOUNT_WITHDRAW = 12;
    public static final int DATA_S2_BIND_DELETE_ACCOUNT = 13;
    public static final int DATA_CARD_TYPE_SHOW_INVITE = 14;
    public static final int ADJUST_LIMIT_SINGLE_TRANSACTION = 100;
    public static final int ADJUST_LIMIT_DAILY_TRANSACTION = 200;
    public static final int ADJUST_LIMIT_VALUE_TRANSACTION = 300;
    public static final int VITUALCARD_SEND_OTP = 500;
    public static final int PAGE_FLOW_VITUALCARD_REGISTER = 600;
    public static final int PAGE_FLOW_VITUALCARD_FORGETPASSWORD = 700;
    public static final int VITUALCARD_CHECK_DETAIL = 800;
    public static final int VITUALCARD_BIND = 900;
    public static final int VERIFY_PWD_CODE = 30000;

    public static final int DATA_TYPE_SET_FIO = 10001;
    public static final int DATA_TYPE_FPS_ADD = 20001;
    public static final int DATA_TYPE_FPS_QUERY = 20002;
    public static final int ACTION_BASE_ERROR = 1000000;

    public static final int DATA_SEL_BANK_LIST = 7;
    public static final int BILL_ITEM_PAGE_SIZE = 10;
    public static final String TYPE_TITLE = "TYPE_TITLE";
    public static final String TYPE_CONTENT = "TYPE_CONTENT";
    public static final String TYPE_CHECK_MORE = "TYPE_CHECK_MORE";


    public static final String ACTION_TYPE_BIND = "B";
    public static final String ACTION_TYPE_REGISTER = "V";
    public static final String ACTION_TYPE_FORGET = "F";


    public static final int REQUEST_CODE_SEL_COUNTRY = 101;
    public static final int DATA_CARD_TYPE_UNBIND_CARD = 102;
    public static final int REQUEST_CODE_SEL_BANK = 103;

    public static final String DATA_CVV = "DATA_CVV";
    public static final String DATA_EXPIRY_DATE = "DATA_EXPIRY_DATE";
    public static final String DATA_EMAIL_ADDRESS = "DATA_EMAIL_ADDRESS";


    public static final String BANK_LIST = "BANK_LIST";

    //APP版本号
    public static final String APP_VERSION_CODE = "APP_VERSION_CODE";

    public static final String CACHE_FINGER_KEY = "CACHE_FINGER_KEY";

    public static final String CACHE_UUID_KEY = "CACHE_UUID_KEY";
//
//    public static final String LANG_CODE_ZH_CN_NEW = "zh_CN";
//
//    public static final String LANG_CODE_ZH_TW_NEW = "zh_TW";
//
//    public static final String LANG_CODE_ZH_MO_NEW = "zh_MO";
//    public static final String LANG_CODE_ZH_MO_NEW_NORMAL = "zh-MO";
//
//    public static final String LANG_CODE_ZH_HK_NEW = "zh_HK";
//
//
//    public static final String LANG_CODE_ZH_HK_NORMAL = "zh-HK";
//    /**
//     * 英语
//     */
//    public static final String LANG_CODE_EN_US_NORMAL = "en-US";
//    /**
//     * 简体中文
//     */
//    public static final String LANG_CODE_ZH_CN_NORMAL = "zh-CN";
//
//    public static final String LANG_CODE_ZH_TW_NORMAL = "zh-TW";
//
//    /**
//     * 繁体中文
//     */
//    public static final String LANG_CODE_ZH_TW = "zh_TW";
//
//    public static final String LANG_CODE_ZH_MO = "zh_MO";
//
//    public static final String LANG_CODE_ZH_HK = "zh_HK";
//    /**
//     * 英语
//     */
//    public static final String LANG_CODE_EN_US = "en_US";
//    /**
//     * 简体中文
//     */
//    public static final String LANG_CODE_ZH_CN = "zh_CN";


    public static final String CACHE_CARDLIST_FILENAME = "cardlist.txt";


    public static final String KEY_FRONT_LIGHT = "KEY_FRONT_LIGHT";


    public static final int ERROR_CODE_0 = 0;
    public static final int ERROR_CODE_F_1 = -1;//系统内部报错
    public static final int ERROR_CODE_1 = 1;
    public static final int ERROR_CODE_2 = 2;
    public static final int ERROR_CODE_3 = 3;
    public static final int ERROR_CODE_4 = 4;
    public static final int ERROR_CODE_5 = 5;
    public static final int ERROR_CODE_6 = 6;
    public static final int ERROR_CODE_7 = 7;
    public static final int ERROR_CODE_8 = 8;
    public static final int ERROR_CODE_9 = 9;


    public static final String R099 = "R099";
    public static final String R066 = "R066";
    public static final String EW009 = "EW009";
    public static final String EW034 = "EW034";
    public static final String EW031 = "EW031";
    public static final String EW014 = "EW014";
    public static final String EW013 = "EW013";
    public static final String EW102 = "EW102";
    public static final String EW101 = "EW101";
    public static final String EW105 = "EW105";
    public static final String EW029 = "EW029";
    public static final String EW108 = "EW108";
    public static final String EW109 = "EW109";
    public static final String EW111 = "EW111";
    public static final String EW100 = "EW100";
    public static final String EW110 = "EW110";
    public static final String EW027 = "EW027";
    public static final String EW107 = "EW107";
    public static final String EW041 = "EW041";
    public static final String EW103 = "EW103";
    public static final String EW007 = "EW007";
    public static final String EW040 = "EW040";
    public static final String EW036 = "EW036";
    public static final String EW104 = "EW104";
    public static final String EW106 = "EW106";

    public static final String BIOMETRIC_VALUE = "BIOMETRIC_VALUE";


    public static final String RES_CHECK_VERSION_MSG_MAJOR = "MAJOR";//强制更新
    public static final String RES_CHECK_VERSION_MSG_MINOR = "MINOR";//提示更新

    public static final String DATA_COUNTRY_CODE = "DATA_COUNTRY_CODE";
    public static final String DATA_COUNTRY_NAME = "DATA_COUNTRY_NAME";
    public static final String DATA_TYPE = "DATA_TYPE";
    public static final String HAS_EXTRA_SEL = "HAS_EXTRA_SEL";
    public static final String TRANSFER_ACCOUNT_TYPE = "TRANSFER_ACCOUNT_TYPE";
    public static final String DATA_COUNTRY_IMAGE_URL = "DATA_COUNTRY_IMAGE_URL";
    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String DEFAULTPAYTYPE = "defaultPayType";
    public static final String EVENT_TYPE = "EVENT_TYPE";
    public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
    public static final String ERROR_CODE = "ERROR_CODE";
    public static final String BANK_CODE = "BANK_CODE";
    public static final String INVITE_TYPE = "INVITE_TYPE";
    public static final String BIND_CARD_TYPE = "BIND_CARD_TYPE";
    public static final String BANK_NAME = "BANK_NAME";
    public static final String BANK_NUMBER = "BANK_NUMBER";
    public static final String CARD_ID = "CARD_ID";
    public static final String STATUS_MESSAGE = "STATUS_MESSAGE";

    public static final String WEB_VIEW_ERROR_IS_HINT = "WEB_VIEW_ERROR_IS_HINT";
    public static final String IS_VIDEO_PLAY = "IS_VIDEO_PLAY";
    public static final String DETAIL_URL = "DETAIL_URL";
    public static final String CROSS_BORDER_WEB_VIEW_RESPONSE = "CROSS_BORDER_WEB_VIEW";
    public static final String DETAIL_URL_PARAMS = "DETAIL_URL_PARAMS";
    public static final String DETAIL_TITLE = "DETAIL_TITLE";
    public static final String WALLETID = "WALLETID";
    public static final String DETAIL_HIDE_SPLIT_VIEW = "DETAIL_HIDE_SPLIT_VIEW";
    public static final String LANGUAGE_TITLE = "LANGUAGE_TITLE";
    public static final String PAN_ID = "PAN_ID";
    public static final String ISAPPCALLAPP = "ISAPPCALLAPP";
    public static final String PAN_ID_HIDE = "PAN_ID_HIDE";
    public static final String PAN_TYPE = "PAN_TYPE";
    public static final String TOTAL_GRADE = "TOTAL_GRADE";
    public static final String RETAIN_GRADE = "RETAIN_GRADE";

    //账号验证过，判断是不是需要一期兼容
    public static final String LOGIN_VERIFY_STATE_YES = "0";

    //账号验证过，判断是不是需要一期兼容
    public static final String NO_CARD_EXIST = "1";

    public static final String DATA_COUNTRY_CODES[] = {"+86", "+852", "+853"};
    public static final String DATA_COUNTRY_CODES_SPACE[] = {"+86", "+852", "+853"};
    public static final String DATA_COUNTRY_NAME_SUB[] = {"HongKong,China", "Macao,China", "China"};
    //HK CN MO 顺序不能更改
    public static final String DATA_COUNTRY_NAME_CODE[] = {"HK", "MO", "CN"};
    public static final String DATA_COUNTRY_CODE_REGISTER[] = {"852", "853", "86"};
    public static final String DATA_ID_CARD_TYPE[] = {"NM", "NL", "01"};
    public static final String DATA_ID_COUNRTY_TYPE[] = {"HK", "MO", "CN"};
    //HK CN MO 顺序不能更改

    public static final int DATA_COUNTRY_CODES_RESOURCE[] = {R.mipmap.icon_text_china, R.mipmap.icon_text_hongkong, R.mipmap.icon_text_macao};


    public static final String CURRENCY = "CURRENCY";
    public static final String ACCTYPE = "accType";
    public static final String ORDDERO = "ORDDERO";
    public static final String LAVEL = "LAVEL";
    public static final String MONEY = "MONEY";
    public static final String REFERENCENO = "REFERENCENO";
    public static final String CURRENT_SEL_ACCOUNT_NO = "CURRENT_SEL_ACCOUNT_NO";
    //主账号
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    public static final String PAYMENT_ACCOUNT = "PAYMENT_ACCOUNT";
    public static final String TOTAL_MONEY = "TOTAL_MONEY";
    public static final String IS_FIRST_WITHDRAW = "IS_FIRST_WITHDRAW";
    public static final String TYPE = "TYPE";
    public static final String FPS_CALLBACK_URL = "FPS_CALLBACK_URL";
    public static final String EXTRA_ACCOUNT_INFO = "EXTRA_ACCOUNT_INFO";
    public static final int TYPE_WITHDREA = 100;
    public static final int TYPE_TOPUP = 200;
    public static final String PAGE_FROM_REGISTER = "PAGE_FROM_REGISTER";

    public static final String RECORD_NUMBER = "RECORD_NUMBER";
    public static final String SMART_ACCOUNT = "SMART_ACCOUNT";

    public static final String SMART_ACCOUNT_A = "A";
    public static final String SMART_ACCOUNT_M = "M";
    public static final String SMART_ACCOUNT_LMT_SET = "LMT_SET";
    public static final String SMART_ACCOUNT_S = "S";
    public static final String SMART_ACCOUNT_R = "R";
    public static final String SMART_ACCOUNT_C = "C";
    public static final String SMART_ACCOUNT_D = "D";
    public static final String SMART_ACCOUNT_U = "U";
    public static final String ISREGESTER = "ISREGESTER";
    public static final String CHOICE_AREA = "choice_area";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_REAL_NAME = "USER_REAL_NAME";
    public static final String IS_EMAIL = "IS_EMAIL";
    public static final String CHOICE_AREA_POSITION = "choice_area_position";
    public static final String ECOUPON_POSITION = "ECOUPON_POSITION";
    public static final String ECOUPON_TITLE = "ECOUPON_TITLE";
    public static final String RRP_ERRORCODE = "RRP_ERRORCODE";
    public static final String FIO_ERRORCODE = "RRP_ERRORCODE";
    public static final String FIO_ERRORMESSAGE = "FIO_ERRORMESSAGE";
    public static final String BU_NUMBER = "BU_NUMBER";
    public static final String BU_NAME = "BU_NAME";
    public static final String CURRAMOUNT = "CURRAMOUNT";
    public static final String RRP_ERRORMESSAGE = "RRP_ERRORMESSAGE";
    public static final String LOGIN_TYPE = "login_type";
    public static final String MERCHANT_LINK = "MERCHANT_LINK";
    public static final String APPCALLAPP_IS_H5 = "APPCALLAPP_IS_H5";
//    A:修改主账号，M:修改限额，增值方式
//    S:暂停我的账户 R:重新激活 C:注销我的账户


    //https://www.bochk.com/dam/more/bocpay/info_zh_hk.html

    public static final String INFO_CN = "https://www.bochk.com/dam/more/bocpay/info_zh_cn.html";
    public static final String INFO_HK = "https://www.bochk.com/dam/more/bocpay/info_zh_hk.html";
    public static final String INFO_US = "https://www.bochk.com/dam/more/bocpay/info_en_us.html";

    public static final String STATUS_CN = "https://www.bochk.com/dam/more/bocpay/state_zh_cn.html";
    public static final String STATUS_HK = "https://www.bochk.com/dam/more/bocpay/state_zh_hk.html";
    public static final String STATUS_US = "https://www.bochk.com/dam/more/bocpay/state_en_us.html";


    public static final String NOTE_CN = "https://www.bochk.com/dam/more/bocpay/note_zh_cn.html";
    public static final String NOTE_HK = "https://www.bochk.com/dam/more/bocpay/note_zh_hk.html";
    public static final String NOTE_US = "https://www.bochk.com/dam/more/bocpay/note_en_us.html";
    public static final String CONDITIONSFORSERVICES_EN = "https://www.bochk.com/en/conditionsforservices.common.html";
    public static final String CONDITIONSFORSERVICES_TC = "https://www.bochk.com/tc/conditionsforservices.common.html";
    public static final String CONDITIONSFORSERVICES_SC = "https://www.bochk.com/sc/conditionsforservices.common.html";
    public static final String GEN_INFO = "https://www.bochk.com/dam/more/forms/gen_info.pdf";


    public static final String IMPORTANTNOTICE_EN = "https://www.bochk.com/en/importantnotice.common.html";
    public static final String IMPORTANTNOTICE_SC = "https://www.bochk.com/sc/importantnotice.common.html";
    public static final String IMPORTANTNOTICE_HK = "https://www.bochk.com/tc/importantnotice.common.html";

    public static final String PRIVACYPOLICY_SC = "https://www.bochk.com/sc/importantnotice.common.html";
    public static final String PRIVACYPOLICY_TC = "https://www.bochk.com/tc/importantnotice.common.html";
    public static final String PRIVACYPOLICY_EN = "https://www.bochk.com/en/importantnotice.common.html";

    public static final String ECOUP_MERCHANT_SC = "rulepages/ZH_CN/evouchers.html";
    public static final String ECOUP_MERCHANT_TC = "rulepages/ZH_HK/evouchers.html";
    public static final String ECOUP_MERCHANT_EN = "rulepages/EN_US/evouchers.html";

    //积分协议条款
    public static final String WALLET_GRADE_SIMP_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/ZH_CN/point.html";
    public static final String WALLET_GRADE_TRAD_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/ZH_HK/point.html";
    public static final String WALLET_GRADE_ENGLISH_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/EN_US/point.html";

    public static final String BOCPAYFAQT_WL_EN = "https://www.bochk.com/dam/document/mbs/bocpayfaqe_wl.html";
    public static final String BOCPAYFAQT_WL_TC = "https://www.bochk.com/dam/document/mbs/bocpayfaqt_wl.html";
    public static final String BOCPAYFAQT_WL_CN = "https://www.bochk.com/dam/document/mbs/bocpayfaqs_wl.html";


    public static final String BOCPAYTCT_WL_EN = "https://www.bochk.com/dam/document/mbs/bocpaytce_wl.html";
    public static final String BOCPAYTCT_WL_TC = "https://www.bochk.com/dam/document/mbs/bocpaytct_wl.html";
    public static final String BOCPAYTCT_WL_CN = "https://www.bochk.com/dam/document/mbs/bocpaytcs_wl.html";
    public static final String BOCPAYTCT_PLAY_VIDE_URL = "https://www.bochk.com/creditcard/BOCPAY/video/BoC_Pay_New_Function_Viideo_UserExp.mp4";

    public static final String FORGET_PASSWORD_TYPE = "FORGET_PASSWORD_TYPE";
    public static final String SMART_LEVEL_INFO = "SMART_LEVEL_INFO";
    public static final String IS_AUTO_BACK = "IS_AUTO_BACK";

    public static final String EWA5182 = "EWA5182";
    public static final String EWA5183 = "EWA5183";
    public static final String EWA5279 = "EWA5279";
    public static final String EWA5280 = "EWA5280";
    public static final String EWA5216 = "EWA5216";
    public static final String EWA5150 = "EWA5150";
    public static final String EWA5215 = "EWA5215";
    public static final String EWA5214 = "EWA5214";
    public static final String EWA5151 = "EWA5151";
    public static final String EWA5133 = "EWA5133";
    public static final String EWA5212 = "EWA5212";


    public static final String TRANSFER_TYPE_FPS = "7";
    public static final String TRANSFER_TYPE_BOCPAY = "8";
    //SEND  ： 派利是记录  RECEVIED：收利是记录
    public static final String SEND = "SEND";
    public static final String RECEVIED = "RECEVIED";

    //派利是背景和音乐
    public static final String COMON_GIF = "comon_gif";
    public static final String COMON_MP3 = "comon_mp3";

    //收利是背景和音乐
    public static final String STAFF_GIF = "staff_gif";
    public static final String STAFF_MP3 = "staff_mp3";

    //派利是背景和音乐 保存的路径
    public static final String RED_SAVE_FILE = "red_save_file";

    //中文简体 路径
    public static final String RED_SAVE_CN = "red_save_cn";
    //中文繁体 路径
    public static final String RED_SAVE_HK = "red_save_hk";
    //英文 路径
    public static final String RED_SAVE_EN = "red_save_en";

    //通过推送打开 员工红包 需要看交易记录
    public static final String RED_PACKET_PUSH_TYPE_NEED_TRANS_HIS = "red_packet_push_type_need_trans_his";
    //通过推送打开 普通红包
    public static final String RED_PACKET_PUSH_TYPE = "red_packet_push_type";
    //通过页面打开打开红包
    public static final String RED_PACKET_COMON_TYPE = "RED_PACKET_COMON_TYPE";
    //打开全部红包
    public static final String RED_PACKET_ALL_TYPE = "red_packet_all_type";

    //推送 传递红包数据
//    public static final String RED_PACKET_RED_PACKET_PARAMS = "red_packet_red_packet_params";
    public static final String REDMG_PARAM = "REDMG";
    public static final String REDMG_STAFF_PARAM = "STAFF";
    public static final String SRCREFNO_PARAM = "SrcRefNo";
    public static final String REDPACKETTYPE_PARAM = "RedPacketType";

    //3为智能账户
    public static final String ACCOUNT_SMART_ACCOUNT = "3";
    //2为支付账户
    public static final String ACCOUNT_PAY_ACCOUNT = "2";

    //STAFF表示员工利是，COMMON表示普通利是
    public static final String PUSH_REDPACKET_COMMON_TYPE = "COMMON";
    public static final String PUSH_REDPACKET_STAFF_TYPE = "STAFF";

    //利是类型 8 bocpay利是 9 员工利是
    public static final String RED_PACKET_TYPE_STAFF = "9";
    public static final String RED_PACKET_TYPE_COMMON = "8";
//    繁	http://www.bochk.com/dam/document/mbs/bocpaytct_wl.html
//    簡	http://www.bochk.com/dam/document/mbs/bocpaytcs_wl.html
//    Eng	http://www.bochk.com/dam/document/mbs/bocpaytce_wl.html

//    繁	http://www.bochk.com/dam/document/mbs/bocpayfaqt_wl.html
//    簡	http://www.bochk.com/dam/document/mbs/bocpayfaqs_wl.html
//    Eng	http://www.bochk.com/dam/document/mbs/bocpayfaqe_wl.html


    //idv背景图的resID
    public static final String BASEMAINBACKGROUNDRESID = "bg_base_act";

    //通讯录设定页面 返回值
    public static final int REQUEST_ADDRESS_SET_CODE = 0x1123;
    ;

    // //1表示是点击员工利是push通知调用此接口
    public static final int IS_STAFF_PUSH = 1;
    // //0表示不是点击员工利是push通知调用此接口
    public static final int IS_NOT_STAFF_PUSH = 0;

    // 需不需要弹出框
    public static final String IS_SHOW_DAILOG = "is_show_dailog";

    //是否是push通知
    public static final String IS_PUSH_NOTIFICATION = "IS_PUSH_NOTIFICATION";
}
