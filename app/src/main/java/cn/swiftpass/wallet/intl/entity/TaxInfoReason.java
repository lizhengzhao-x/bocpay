package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class TaxInfoReason extends BaseEntity {

    private boolean isSelect;
    private String noResidentReasonCode;

    private String noResidentReason;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getNoResidentReasonCode() {
        return noResidentReasonCode;
    }

    public void setNoResidentReasonCode(String noResidentReasonCode) {
        this.noResidentReasonCode = noResidentReasonCode;
    }

    public String getNoResidentReason() {
        return noResidentReason;
    }

    public void setNoResidentReason(String noResidentReason) {
        this.noResidentReason = noResidentReason;
    }
}
