package cn.swiftpass.wallet.intl.module.register.contract;

public interface OtpAction {
    int OTP_CREDIT_BIND_SMART_ACCOUNT=101;// s2用户充值绑定银行卡
    int OTP_SMART_ACCOUNT_TWO_REGISTER=102;// s2用户注册
    int OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD = 103;// s2用户忘记密码
    String OTP_BIND_NEW_CARD = "CCBATCHBIND";
}
