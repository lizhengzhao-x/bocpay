package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VitualCardIsBocEntity;

/**
 * 虚拟卡注册验证身份证号码
 */
public class RegisterVitualCardVerifyIdCardContract {

    public interface View extends IView {

        void virtualCardJudgeIsBocCustomerSuccess(VitualCardIsBocEntity response);

        void virtualCardJudgeIsBocCustomerError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<RegisterVitualCardVerifyIdCardContract.View> {

        void virtualCardJudgeIsBocCustomer(VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity);
    }

}
