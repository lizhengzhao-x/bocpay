package cn.swiftpass.wallet.intl.module.ecoupon.view;

import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivityEntity;

public interface EcouponsActivityItemClickListener {
    void onClick(EcouponActivityEntity type);

}
