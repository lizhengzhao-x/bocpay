package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 验证FIO的OTP
 */

public class FioAuthOtpProtocol extends BaseProtocol {
    //客户端调用Authtication 接口验证用户指纹，之后SDK返回的base64字符串
    String otp;
    boolean isEcouponAction;

    public FioAuthOtpProtocol(String param, boolean isEcouponActionIn, NetWorkCallbackListener dataCallback) {
        this.otp = param;
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/vreifyOTP";
        this.isEcouponAction = isEcouponActionIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.FIO_OTP, otp);
        if (isEcouponAction) {
            //电子券验证指纹流程 要单独处理
            mBodyParams.put(RequestParams.ACTION, "E");
        }
    }
}
