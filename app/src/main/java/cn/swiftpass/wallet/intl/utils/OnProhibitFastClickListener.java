package cn.swiftpass.wallet.intl.utils;

import android.view.View;

/**
 * 禁止快速点击 连续相应onClick事件
 */
public abstract class OnProhibitFastClickListener implements View.OnClickListener {

    private  long DIFF = 1000;
    public OnProhibitFastClickListener() {
    }

    public OnProhibitFastClickListener(long diff) {
        this.DIFF = diff;
    }

    public abstract void onFilterClick(View v);


    @Override
    public void onClick(View v) {
        if (!ButtonUtils.isFastDoubleClick(v.getId(),DIFF)) {
            onFilterClick(v);
        }
    }
}
