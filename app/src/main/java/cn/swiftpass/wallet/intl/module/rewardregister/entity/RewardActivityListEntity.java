package cn.swiftpass.wallet.intl.module.rewardregister.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class RewardActivityListEntity extends BaseEntity {


    public List<RewardActivity> getValue() {
        return value;
    }

    public void setValue(List<RewardActivity> value) {
        this.value = value;
    }

    private List<RewardActivity> value;

    public static class RewardActivity extends BaseEntity {
        private String imagesUrl;
        private String activityUrl;

        public String getExplain() {
            return explain;
        }

        public void setExplain(String explain) {
            this.explain = explain;
        }

        private String explain;

        public String getImagesUrl() {
            return imagesUrl;
        }

        public void setImagesUrl(String imagesUrl) {
            this.imagesUrl = imagesUrl;
        }

        public String getActivityUrl() {
            return activityUrl;
        }

        public void setActivityUrl(String activityUrl) {
            this.activityUrl = activityUrl;
        }

        public String getTermsAndConditionsUrl() {
            return termsAndConditionsUrl;
        }

        public void setTermsAndConditionsUrl(String termsAndConditionsUrl) {
            this.termsAndConditionsUrl = termsAndConditionsUrl;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCampaignId() {
            return campaignId;
        }

        public void setCampaignId(String campaignId) {
            this.campaignId = campaignId;
        }

        public String getBankCardType() {
            return bankCardType;
        }

        public void setBankCardType(String bankCardType) {
            this.bankCardType = bankCardType;
        }

        private String termsAndConditionsUrl;
        private String description;
        private String campaignId;
        private String bankCardType;


    }
}
