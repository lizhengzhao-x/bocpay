package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */
public class BankRegisterVerifyCodeCheckEntity extends BaseEntity {

    String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
