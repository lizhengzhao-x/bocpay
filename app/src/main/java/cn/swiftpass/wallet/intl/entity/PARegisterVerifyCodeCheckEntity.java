package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author zhaolizheng
 * create date on 2021/01/02
 */
public class PARegisterVerifyCodeCheckEntity extends BaseEntity {

    /**
     * data
     * transactionUniqueID 交易号
     */
    String data;
    private String transactionUniqueID;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getTransactionUniqueID() {
        return transactionUniqueID;
    }

    public void setTransactionUniqueID(String transactionUniqueID) {
        this.transactionUniqueID = transactionUniqueID;
    }
}
