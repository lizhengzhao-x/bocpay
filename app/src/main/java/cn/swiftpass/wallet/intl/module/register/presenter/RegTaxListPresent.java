package cn.swiftpass.wallet.intl.module.register.presenter;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckIDvProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckTaxInfoListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTaxResidentProtocol;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.entity.RegisterTaxResultEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoParamEntity;
import cn.swiftpass.wallet.intl.module.register.SkipPageFromTaxFunction;
import cn.swiftpass.wallet.intl.module.register.contract.RegisterTaxContact;

public class RegTaxListPresent extends BasePresenter<RegisterTaxContact.View> implements RegisterTaxContact.Presenter {
    @Override
    public void checkIdvRequest(String type, RegisterCustomerInfo registerCustomerInfo) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckIDvProtocol(type, new NetWorkCallbackListener<RegisterIDEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().onCheckTaxFail(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(RegisterIDEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().onCheckTaxSuccess(response, registerCustomerInfo);
                }
            }
        }).execute();
    }


    @Override
    public void getTaxResidentRelationInfo(String type, SkipPageFromTaxFunction skipPageFromTaxFunction) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new QueryTaxResidentProtocol(type,new NetWorkCallbackListener<RegisterTaxResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTaxResidentRelationInfoFail(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(RegisterTaxResultEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTaxResidentRelationInfoSuccess(response);
                    if (skipPageFromTaxFunction != null) {
                        skipPageFromTaxFunction.skipNextPage();
                    }
                }
            }
        }).execute();
    }

    @Override
    public void checkTaxList(ArrayList<TaxInfoParamEntity> data, String ifHKPeople, String ifOtherPeople, String type) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckTaxInfoListProtocol(data, ifHKPeople, ifOtherPeople, type, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkTaxInfoListProtocolFail(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkTaxInfoListProtocolSuccess(response, data);

                }
            }
        }).execute();
    }
}
