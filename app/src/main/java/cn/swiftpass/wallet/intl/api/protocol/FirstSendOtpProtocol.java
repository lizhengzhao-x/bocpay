package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class FirstSendOtpProtocol extends BaseProtocol {

    String txnId;

    public FirstSendOtpProtocol(String txnIdIn, NetWorkCallbackListener dataCallback) {
        this.txnId = txnIdIn;
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/sendOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);
        mBodyParams.put(RequestParams.ACTION, "TRANSFERPAY");
    }
}
