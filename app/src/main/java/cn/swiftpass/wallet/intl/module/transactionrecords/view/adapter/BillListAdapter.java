package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.BocPayTransUtils;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;

import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_SUCCESS;


public class BillListAdapter implements AdapterItem<OrderQueryEntity> {

    private ImageView mId_merchant_head;
    private TextView mId_merchant_name;
    private TextView mId_merchant_money;
    private TextView mId_bill_time;
    private TextView mId_pay_symbol;
    private TextView mId_discount;
    private TextView id_payment_status;
    private View mId_root_view;
    private TextView mTv_gr_status;

    private int position;
    private Context mContext;
    public static final String SUCCESS_INFO_ = "SUCCESS";

    private CardItemCallback mCallback;

    //是否支付失败
    private boolean isPayFailed = false;

    public BillListAdapter(Context mContextIn, CardItemCallback callback) {
        mCallback = callback;
        mContext = mContextIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_bill_view;
    }

    @Override
    public void bindViews(View root) {
        mId_merchant_head = root.findViewById(R.id.id_merchant_head);
        mId_merchant_name = root.findViewById(R.id.id_merchant_name);
        mId_merchant_money = root.findViewById(R.id.id_merchant_money);
        mId_bill_time = root.findViewById(R.id.id_bill_time);
        mId_pay_symbol = root.findViewById(R.id.id_pay_symbol);
        mId_root_view = root.findViewById(R.id.id_root_view);
        mId_discount = root.findViewById(R.id.id_discount);
        id_payment_status = root.findViewById(R.id.id_payment_status);
        mTv_gr_status = root.findViewById(R.id.tv_gr_status);
    }

    @Override
    public void setViews() {
        mId_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCallback != null) {
                    mCallback.onItemCardClick(position);
                }
            }
        });

    }

    @Override
    public void handleData(OrderQueryEntity cardItemEntity, int position) {
        this.position = position;
        String merchantName = "";
        merchantName = cardItemEntity.getMerchantName();
        mId_merchant_name.setText(merchantName);
        mId_bill_time.setText(AndroidUtils.formatTimeString(cardItemEntity.getTransDate()));
        mId_pay_symbol.setText(cardItemEntity.getShowType() + AndroidUtils.getTrxCurrency(cardItemEntity.getCur()));
        mId_discount.setVisibility(View.GONE);
        try {
            mId_merchant_money.setText(" " + AndroidUtils.formatPriceWithPoint(Double.valueOf(cardItemEntity.getTrxAmt()), true));
        } catch (NumberFormatException e) {
        }
        mId_merchant_money.setTextColor(BocPayTransUtils.getColor(mContext, cardItemEntity.getShowType(), false));
        mId_pay_symbol.setTextColor(BocPayTransUtils.getColor(mContext, cardItemEntity.getShowType(), false));
        //默认状态 方式复用
        id_payment_status.setVisibility(View.GONE);
        mTv_gr_status.setVisibility(View.GONE);
        mId_discount.setVisibility(View.GONE);
        mId_discount.setBackgroundColor(Color.WHITE);
        mId_discount.setTextColor(Color.parseColor("#AEB2B8"));
        //取消删除下滑线默认
        mId_discount.getPaint().setFlags(0);
        if (TextUtils.equals(cardItemEntity.getPaymentType(), "6") || TextUtils.equals(cardItemEntity.getPaymentType(), "8")) {
            //增值 首榜奖赏的时候 针对新的ui 标题要单独处理
            mId_merchant_name.setText(cardItemEntity.getTxTitle());
        }
        mId_merchant_head.setImageResource(BocPayTransUtils.getImageResource(cardItemEntity));
        if (!TextUtils.isEmpty(cardItemEntity.getGpDetail())) {
            if (cardItemEntity.isGpDetail()) {
                //App判断是否展示积分换领详情页   1:是 0：否 积分显示金额
                mId_merchant_money.setText(" " + BigDecimalFormatUtils.forMatWithDigs(cardItemEntity.getRedeemAmt(), 2));
            }
        }
        boolean showDiscount = false;
        if (!TextUtils.isEmpty(cardItemEntity.getDiscount())) {
            //优惠信息
            try {
                if (Double.valueOf(cardItemEntity.getDiscount()) > 0) {
                    mId_discount.setVisibility(View.VISIBLE);
                    mId_discount.setText(BigDecimalFormatUtils.forMatWithDigs(cardItemEntity.getOriginalAmt(), 2));
                    mId_discount.getPaint().setAntiAlias(true);
                    mId_discount.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                    showDiscount = true;
                }
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
        isPayFailed = false;
        boolean isFailed = true;
        if (!TextUtils.isEmpty(cardItemEntity.getRecordType()) && cardItemEntity.isCrossBorderTransfer()) {
            isFailed = !TextUtils.equals(cardItemEntity.getPaymentStatus(), CROSSBORDERTRANSFER_SUCCESS);
        } else {
            isFailed = !TextUtils.isEmpty(cardItemEntity.getPaymentStatus()) && !cardItemEntity.getPaymentStatus().equals(SUCCESS_INFO_);
        }
        if (showDiscount && isFailed) {
            //又有满减 又有失败类型
            if (!TextUtils.isEmpty(cardItemEntity.getTxnLabel())) {
                id_payment_status.setVisibility(View.VISIBLE);
                id_payment_status.setText(cardItemEntity.getTxnLabel());
                id_payment_status.setTextColor(Color.WHITE);
                id_payment_status.setBackgroundColor(Color.parseColor("#AEB2B8"));
                //标记支付失败
                isPayFailed = true;
            }
        } else {
            if (showDiscount && !isFailed) {
                //有满减 是成功的
                id_payment_status.setVisibility(View.GONE);
            } else if (!showDiscount && isFailed) {
                //没有满减 是失败的
                if (!TextUtils.isEmpty(cardItemEntity.getTxnLabel())) {
                    id_payment_status.setVisibility(View.VISIBLE);
                    id_payment_status.setTextColor(Color.WHITE);
                    id_payment_status.setText(cardItemEntity.getTxnLabel());
                    id_payment_status.setBackgroundColor(Color.parseColor("#AEB2B8"));
                    //标记支付失败
                    isPayFailed = true;
                }
            } else {
                mId_discount.setVisibility(View.GONE);
                id_payment_status.setVisibility(View.GONE);
            }
        }
        if (cardItemEntity.canGradeReduce()) {
            boolean isAlreadyReduce = cardItemEntity.alreadyGradeReduce();
            mTv_gr_status.setVisibility(View.VISIBLE);
            mTv_gr_status.setText(isAlreadyReduce ? mContext.getString(R.string.CCP2105_1_1) : mContext.getString(R.string.CCP2105_1_2));
            mTv_gr_status.setBackgroundResource(isAlreadyReduce ? R.drawable.bg_btn_gradereduce_disable : R.drawable.bg_btn_gradereduce_normal);
        } else {
            if (!isPayFailed) {
                //如果是支付失败，则不隐藏支付状态
                id_payment_status.setVisibility(View.GONE);
            }
        }
    }
}
