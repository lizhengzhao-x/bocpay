package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;


public class RedPacketContactSortAdapter extends BaseRecyclerAdapter<ContactEntity> {

    public RedPacketContactSortAdapter(@Nullable List<ContactEntity> data) {
        super(R.layout.item_red_packet_contact, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder holder, ContactEntity contactEntity, int position) {
        int section = getSectionForPosition(position);
        if (section == -1) {
            return;
        }
        //如果当前位置等于该分类首字母的Char的位置 ，则认为是第一次出现
        holder.setGone(R.id.tv_letter, false);
        if (position == getPositionForSection(section)) {
            holder.setGone(R.id.tv_letter, false);
            holder.setText(R.id.tv_letter, contactEntity.getSortLetter());
            if (contactEntity.getSortLetter().equals(Constants.TOP_HEADER_CHAR)) {
                holder.setVisible(R.id.tv_letter, true);
            }
        } else {
            holder.setGone(R.id.tv_letter, true);
        }

        if (TextUtils.isEmpty(contactEntity.getUserName())) {
            holder.setGone(R.id.id_contact_user_name, true);
        } else {
            holder.setGone(R.id.id_contact_user_name, false);
            holder.setText(R.id.id_contact_user_name, contactEntity.getUserName());
        }
        holder.setText(R.id.id_contact_user_number, contactEntity.getNumber());
        // holder.setImageResource(R.id.id_contact_collect_imag, contactEntity.isCollect() ? R.mipmap.icon_contactlist_favorite : R.mipmap.icon_contactlist_favorite_empty);
        holder.setImageResource(R.id.id_contact_head_img, contactEntity.isEmail() ? R.mipmap.icon_transfer_profile_email : R.mipmap.icon_transfer_profile_mobile);

        holder.addOnClickListener(R.id.id_contain_view);
    }

    /**
     * 根据ListView的当前位置获取分类的首字母的char ascii值
     */
    public int getSectionForPosition(int position) {
        ContactEntity contactEntity = getItem(position);
        if (contactEntity != null) {
            return contactEntity.getSortLetter().charAt(0);
        }
        return -1;
    }

    /**
     * 根据分类的首字母的Char ascii值获取其第一次出现该首字母的位置
     */
    public int getPositionForSection(int section) {
        for (int i = 0; i < getItemCount(); i++) {
            String sortStr = mDataList.get(i).getSortLetter();
            char firstChar = sortStr.toUpperCase().charAt(0);
            if (firstChar == section) {
                return i;
            }
        }
        return -1;
    }
}
