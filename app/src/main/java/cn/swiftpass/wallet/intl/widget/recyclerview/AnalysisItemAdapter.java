package cn.swiftpass.wallet.intl.widget.recyclerview;

import java.util.List;

import cn.swiftpass.wallet.intl.sdk.sa.AnalysisBaseEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;

public class AnalysisItemAdapter extends MultipleItemAdapter<AnalysisBaseEntity> {

    public static final int TYPE_PAGE = 1;
    public static final int TYPE_BUTTON = 2;
    public static final int TYPE_ERROR = 3;

    public AnalysisItemAdapter(List<AnalysisBaseEntity> mDataList) {
        super(mDataList);
    }

    @Override
    protected int getViewType(AnalysisBaseEntity analysisBaseEntity) {
        if (analysisBaseEntity instanceof AnalysisPageEntity) {
            return TYPE_PAGE;
        } else if (analysisBaseEntity instanceof AnalysisButtonEntity) {
            return TYPE_BUTTON;
        } else if (analysisBaseEntity instanceof AnalysisErrorEntity) {
            return TYPE_ERROR;
        }
        return 0;
    }

    @Override
    public void addItemDataBinder() {
        addDataBinder(new AnalysisPageBinder());
    }
}
