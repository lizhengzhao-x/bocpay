package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;

/**
 * 虚拟卡获取列表
 */
public class GetVitualCardListContract {

    public interface View extends IView {

//        void getVitualCardListSuccess(VitualCardListEntity response);
//
//        void getVitualCardListError(String errorCode, String errorMsg);

        void vitualCardCheckSuccess(ContentEntity response);

        void vitualCardCheckError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<GetVitualCardListContract.View> {

        //        void getVitualCardList();
        void vitualCardCheck(String pan, String expiryDate, String action);
    }

}
