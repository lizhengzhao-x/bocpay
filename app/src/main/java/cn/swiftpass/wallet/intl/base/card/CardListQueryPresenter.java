package cn.swiftpass.wallet.intl.base.card;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;

public interface CardListQueryPresenter<V extends IView> extends IPresenter<V> {
    void queryCardList(String action, boolean isShowLoading);
}
