package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class SwRegisterSendOtpProtocol extends BaseProtocol {

    public SwRegisterSendOtpProtocol(NetWorkCallbackListener dataCallback) {
        mUrl = "api/smartReg/sentOtp";
        mDataCallback = dataCallback;
    }

}
