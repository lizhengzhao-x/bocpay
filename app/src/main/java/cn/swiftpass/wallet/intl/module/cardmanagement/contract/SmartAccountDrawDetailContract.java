package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;

public class SmartAccountDrawDetailContract {
    public interface View extends IView {


        void preCheckTopUpWithSmartAccountTwoFail(String errorCode, String errorMsg);

        void preCheckTopUpWithSmartAccountTwoSuccess( STwoTopUpEntity response);


        void preCheckTopUpWithSmartAccountThreeFail( String errorCode, String errorMsg);

        void preCheckTopUpWithSmartAccountThreeSuccess( SThreeTopUpEntity response);

        void topUpWithSmartAccountTwoFail(String errorCode, String errorMsg);

        void topUpWithSmartAccountTwoSuccess(STwoTopUpEntity response);

        void topUpWithSmartAccountThreeFail(String errorCode, String errorMsg);

        void topUpWithSmartAccountThreeSuccess(TopUpResponseEntity response);
    }


    public interface Presenter extends IPresenter<SmartAccountDrawDetailContract.View> {


        void preCheckTopUpWithSmartAccountTwo( String txnAmt);


        void preCheckTopUpWithSmartAccountThree( String money);

        void topUpWithSmartAccountTwo( String money, String referenceNo);

        void topUpWithSmartAccountThree( String money, String orderNo);
    }
}
