package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;


public class BottomButtonView extends LinearLayout implements View.OnClickListener {

    public static final int TAB_INDEX_HOME = 0;
    public static final int TAB_INDEX_MAJOR_SCAN = 1;
    public static final int TAB_INDEX_BACK_SCAN = 2;
    public static final int TAB_INDEX_QR_CODE_BY_STATIC = 3;
    public static final int TAB_INDEX_TRANSFER_SELECT_LIST = 4;

    private RadioButton mTabHome;
    private RadioButton mTabTransferSelectList;
    private RadioButton mTabBackScan;
    private RadioButton mTabMajorScan;
    private RadioButton mTabQrCodeByStatic;
    private List<RadioButton> mRadioButtons;
    private int selectPosition;
    private RadioGroup mTabBodyGroup;


    public int getLastSelPosiiton() {
        return lastSelPosiiton;
    }

    /**
     * 记录上一次选中位置
     */
    private int lastSelPosiiton;

    public void setOnBottomButtonCheckListener(OnBottomButtonCheckListener onBottomButtonCheckListener) {
        this.onBottomButtonCheckListener = onBottomButtonCheckListener;
    }

    private OnBottomButtonCheckListener onBottomButtonCheckListener;

    public BottomButtonView(Context context) {
        super(context);
        initViews();
    }

    public BottomButtonView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initViews();
    }


    private void initViews() {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_buttom_view, this);
        mTabBodyGroup = rootView.findViewById(R.id.tab_body);
        mTabHome = rootView.findViewById(R.id.tab_home_rb);
        mTabTransferSelectList = rootView.findViewById(R.id.tab_transfer_select_list);
        mTabBackScan = rootView.findViewById(R.id.tab_back_scan);
        mTabMajorScan = rootView.findViewById(R.id.tab_major_scan);
        mTabQrCodeByStatic = rootView.findViewById(R.id.tab_qr_code_by_static);
        mRadioButtons = new ArrayList<>();
        mTabHome.setOnClickListener(this);
        mTabTransferSelectList.setOnClickListener(this);
        mTabBackScan.setOnClickListener(this);
        mTabMajorScan.setOnClickListener(this);
        mTabQrCodeByStatic.setOnClickListener(this);

        mRadioButtons.add(mTabHome);
        mRadioButtons.add(mTabMajorScan);
        mRadioButtons.add(mTabBackScan);
        mRadioButtons.add(mTabQrCodeByStatic);
        mRadioButtons.add(mTabTransferSelectList);


    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setBackgroundColor(Color.TRANSPARENT);
    }

    public void selActivitysPage() {
        onClick(mTabTransferSelectList);
    }


    public int getSelectPosition() {
        return selectPosition;
    }

    public void setSelectPosition(int selectPosition) {
        this.selectPosition = selectPosition;
    }

    /**
     * 更改选中的radiobutton
     *
     * @param position
     */
    public void resetSelStatus(int position) {
        selectWithPosition(position);
    }

    public void selectWithPosition(int position) {
        if (selectPosition == position) {
            return;
        }
        if (position < 0) {
            clearSelect();
        } else {
            for (int i = 0; i < mRadioButtons.size(); i++) {
                if (i == position) {
                    if (position == 0) {
                        //如果直接选中首页  lastSelPosiiton 直接为0
                        lastSelPosiiton = 0;
                    } else {
                        lastSelPosiiton = selectPosition;
                    }
                    selectPosition = i;
                    mRadioButtons.get(i).setChecked(true);

                } else {
                    mRadioButtons.get(i).setChecked(false);
                }
            }
        }


    }

    public void clearSelect() {
        if (mTabBodyGroup != null) {
            mTabBodyGroup.clearCheck();
        }
        lastSelPosiiton = -1;
        selectPosition = -1;
    }

    public void selLastPosition() {
        selectWithPosition(lastSelPosiiton);
    }

    @Override
    public void onClick(View view) {
        if (onBottomButtonCheckListener == null) {
            return;
        }
        switch (view.getId()) {
            case R.id.tab_home_rb:
                if (!ButtonUtils.isFastDoubleClick(R.id.tab_home_rb)) {
                    selectWithPosition(TAB_INDEX_HOME);
                    onBottomButtonCheckListener.OnBottomButtonCheck(TAB_INDEX_HOME);
                }
                break;
            case R.id.tab_back_scan:
                if (!ButtonUtils.isFastDoubleClick(R.id.tab_back_scan)) {
                    selectWithPosition(TAB_INDEX_BACK_SCAN);
                    onBottomButtonCheckListener.OnBottomButtonCheck(TAB_INDEX_BACK_SCAN);
                }
                break;
            case R.id.tab_major_scan:
                if (!ButtonUtils.isFastDoubleClick(R.id.tab_major_scan)) {
                    selectWithPosition(TAB_INDEX_MAJOR_SCAN);
                    onBottomButtonCheckListener.OnBottomButtonCheck(TAB_INDEX_MAJOR_SCAN);
                }
                break;
            case R.id.tab_qr_code_by_static:
                if (!ButtonUtils.isFastDoubleClick(R.id.tab_qr_code_by_static)) {
                    selectWithPosition(TAB_INDEX_QR_CODE_BY_STATIC);
                    onBottomButtonCheckListener.OnBottomButtonCheck(TAB_INDEX_QR_CODE_BY_STATIC);
                }
                break;
            case R.id.tab_transfer_select_list:
                if (!ButtonUtils.isFastDoubleClick(R.id.tab_transfer_select_list)) {
                    selectWithPosition(TAB_INDEX_TRANSFER_SELECT_LIST);
                    onBottomButtonCheckListener.OnBottomButtonCheck(TAB_INDEX_TRANSFER_SELECT_LIST);
                }

                break;
            default:
                break;
        }

    }

    public interface OnBottomButtonCheckListener {

        void OnBottomButtonCheck(int position);
    }

}
