package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class GenerateAuthTokenProtocol extends BaseProtocol {
    String cardId;

    public GenerateAuthTokenProtocol(String cardIdIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/generateAuthToken";
        this.cardId = cardIdIn;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(cardId)) {
            mBodyParams.put(RequestParams.CARDID, cardId);
        }

    }
}
