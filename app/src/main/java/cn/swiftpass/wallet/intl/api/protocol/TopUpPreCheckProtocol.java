package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class TopUpPreCheckProtocol extends BaseProtocol {
    public static final String TAG = TopUpPreCheckProtocol.class.getSimpleName();

    String trxAmount;
    String action;

    public TopUpPreCheckProtocol(String action, String trxAmount, NetWorkCallbackListener dataCallback) {
        this.trxAmount = trxAmount;
        this.action = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/createOrder";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNAMT, trxAmount);
        mBodyParams.put(RequestParams.ACTION, action);
    }


}
