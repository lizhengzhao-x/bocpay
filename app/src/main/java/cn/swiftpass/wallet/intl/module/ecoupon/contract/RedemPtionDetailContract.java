package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.base.totalgrade.TotalGradePresenter;
import cn.swiftpass.wallet.intl.base.totalgrade.TotalGradeView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.ActRedmIdEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * 换领电子券详情
 */
public class RedemPtionDetailContract {

    public interface View extends IView, TotalGradeView<Presenter> {
        void actRedmIdSuccess(ActRedmIdEntity response);

        void actRedmIdError(String errorCode, String errorMsg);

        void checkCCardSuccess(CheckCcEntity response);

        void checkCCardError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<RedemPtionDetailContract.View>, TotalGradePresenter<RedemPtionDetailContract.View> {


        void actRedmId(EcouponConvertEntity ecouponConvertEntity, String giftTp);

        void redeemGiftCheckStock(String giftTp, List<RedeemResponseEntity.ResultRedmBean> redmBeansIn);
    }

}
