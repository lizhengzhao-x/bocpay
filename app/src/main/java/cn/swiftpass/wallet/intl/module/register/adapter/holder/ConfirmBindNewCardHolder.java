package cn.swiftpass.wallet.intl.module.register.adapter.holder;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class ConfirmBindNewCardHolder extends RecyclerView.ViewHolder {
    private final Context context;
    @BindView(R.id.tv_card_type)
    TextView tvCardType;
    @BindView(R.id.tv_card_number)
    TextView tvCardNumber;

    public ConfirmBindNewCardHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }


    public void setData(BindNewCardEntity bindNewCardEntity) {
        if (bindNewCardEntity != null) {
            tvCardNumber.setText("************" + bindNewCardEntity.getPanShowNum());
            tvCardType.setText(bindNewCardEntity.getPanName());
        }
    }
}
