package cn.swiftpass.wallet.intl.module.redpacket.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.ReceivedRedPacketTurnOffProtocol;
import cn.swiftpass.wallet.intl.module.redpacket.contract.UnOpenLiShiListContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.UnOpenLiShiListEntity;

/**
 * 未拆分利是 记录
 */
public class UnOpenLiShiListPresenter extends BasePresenter<UnOpenLiShiListContract.View> implements UnOpenLiShiListContract.Presenter {
    @Override
    public void getUnOpenRedPacket(int pageNum, int pageSize, String nextTimeStamp) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ReceivedRedPacketTurnOffProtocol(pageNum, pageSize, nextTimeStamp, new NetWorkCallbackListener<UnOpenLiShiListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUnOpenRedPacketError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(UnOpenLiShiListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getUnOpenRedPacketSuccess(response);
                }

            }
        }).execute();
    }

    @Override
    public void turnOnRedPacket(String srcRefNo) {

    }
}
