package cn.swiftpass.wallet.intl.entity.event;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class AppCallAppEventItem extends BaseEntity {

    public static final int EVENT_APP_CALLAPP_SUCCESS = 100;
    public static final int EVENT_APP_CALLAPP_CANCEL = 200;
    public static final int EVENT_APP_CALLAPP_BIND_CARD = 300;


    public AppCallAppEventItem(int eventType, String message) {

        this.eventType = eventType;
        this.eventMessage = message;
    }

    public AppCallAppEventItem(int eventType, String message, String errorCodeIn) {

        this.eventType = eventType;
        this.eventMessage = message;
        this.errorCode = errorCodeIn;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }

    private int eventType;
    private String eventMessage;

    public String getErrorCode() {
        return errorCode;
    }

    private String errorCode;


}
