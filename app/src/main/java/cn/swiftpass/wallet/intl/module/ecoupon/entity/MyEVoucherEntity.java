package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/11.
 */
public class MyEVoucherEntity extends BaseEntity {


    private List<EvoucherItem> rows;

    public List<RedeemableGiftListEntity.BuMapsBean> getBuMaps() {
        return buMaps;
    }

    public void setBuMaps(List<RedeemableGiftListEntity.BuMapsBean> buMaps) {
        this.buMaps = buMaps;
    }

    private List<RedeemableGiftListEntity.BuMapsBean> buMaps;

    public List<EvoucherItem> getRows() {
        return rows;
    }

    public void setRows(List<EvoucherItem> rows) {
        this.rows = rows;
    }

    public static class EvoucherItem extends BaseEntity {
        /**
         * itemName : $20 PARKnSHOP eVoucher
         * bU : EGP
         * currAmount : 20.0
         * itemCode : 900005
         * num : 4
         * expireDate : 20190911
         * currency : HKD
         * eVoucherInfos : [{"currAmount":20,"referenceNo":"9400000216","qRCode":"9504815826135984270","expireDate":"20190911","status":"32"},{"currAmount":20,"referenceNo":"9400000211","qRCode":"9502702147843681723","expireDate":"20190921","status":"32"},{"currAmount":20,"referenceNo":"9400000207","qRCode":"9504276683140471833","expireDate":"20190931","status":"32"},{"currAmount":20,"referenceNo":"9400000208","qRCode":"9503773688377739580","expireDate":"20190931","status":"32"}]
         */

        private String itemName;
        private String bU;

        public String getRed() {
            if (TextUtils.isEmpty(red)) {

            }
            return red;
        }

        public boolean isRed() {
            if (TextUtils.isEmpty(red)) {
                return false;
            }
            return red.equals("1");
        }

        public void setRed(String red) {
            this.red = red;
        }

        private String red;

        public String getbUName() {
            return buName;
        }

        public void setbUName(String bUName) {
            this.buName = bUName;
        }

        private String buName;
        private String currAmount;
        private String itemCode;
        private int num;
        private String expireDate;
        private String currency;
        private String couponImgSmall;

        public String geteVoucherTc() {
            return eVoucherTc;
        }

        public void seteVoucherTc(String eVoucherTc) {
            this.eVoucherTc = eVoucherTc;
        }

        private String eVoucherTc;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        private String status;

        public String getCouponImgLarge() {
            return couponImgLarge;
        }

        public void setCouponImgLarge(String couponImgLarge) {
            this.couponImgLarge = couponImgLarge;
        }

        private String couponImgLarge;

        public String getCouponImgSmall() {
            return couponImgSmall;
        }

        public void setCouponImgSmall(String couponImgSmall) {
            this.couponImgSmall = couponImgSmall;
        }

        public String getMerchantIconUrl() {
            return merchantIconUrl;
        }

        public void setMerchantIconUrl(String merchantIconUrl) {
            this.merchantIconUrl = merchantIconUrl;
        }

        private String merchantIconUrl;
        private List<EVoucherInfosBean> eVoucherInfos;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        private String type;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getBU() {
            return bU;
        }

        public void setBU(String bU) {
            this.bU = bU;
        }

        public String getCurrAmount() {
            return currAmount;
        }

        public void setCurrAmount(String currAmount) {
            this.currAmount = currAmount;
        }

        public String getItemCode() {
            return itemCode;
        }

        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }

        public String getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(String expireDate) {
            this.expireDate = expireDate;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public List<EVoucherInfosBean> getEVoucherInfos() {
            return eVoucherInfos;
        }

        public void setEVoucherInfos(List<EVoucherInfosBean> eVoucherInfos) {
            this.eVoucherInfos = eVoucherInfos;
        }

        public static class EVoucherInfosBean extends BaseEntity {
            /**
             * currAmount : 20.0
             * referenceNo : 9400000216
             * qRCode : 9504815826135984270
             * expireDate : 20190911
             * status : 32
             */

            private double currAmount;
            private String referenceNo;
            private String qRCode;
            private String expireDate;
            private String status;

            public boolean isGetDetailSuccess() {
                return getDetailSuccess;
            }

            public void setGetDetailSuccess(boolean getDetailSuccess) {
                this.getDetailSuccess = getDetailSuccess;
            }

            private boolean getDetailSuccess = true;

            public EcouponsDetailEntity getEcouponsDetail() {
                return ecouponsDetail;
            }

            public void setEcouponsDetail(EcouponsDetailEntity ecouponsDetail) {
                this.ecouponsDetail = ecouponsDetail;
            }

            private EcouponsDetailEntity ecouponsDetail;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            private String type;

            public double getCurrAmount() {
                return currAmount;
            }

            public void setCurrAmount(double currAmount) {
                this.currAmount = currAmount;
            }

            public String getReferenceNo() {
                return referenceNo;
            }

            public void setReferenceNo(String referenceNo) {
                this.referenceNo = referenceNo;
            }

            public String getQRCode() {
                return qRCode;
            }

            public void setQRCode(String qRCode) {
                this.qRCode = qRCode;
            }

            public String getExpireDate() {
                return expireDate;
            }

            public void setExpireDate(String expireDate) {
                this.expireDate = expireDate;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }
        }
    }
}
