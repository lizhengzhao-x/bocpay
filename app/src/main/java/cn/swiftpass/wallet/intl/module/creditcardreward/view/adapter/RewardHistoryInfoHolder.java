package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryInfo;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;

public class RewardHistoryInfoHolder extends RecyclerView.ViewHolder {
    private final Context context;
    @BindView(R.id.tv_card_account)
    TextView tvCardAccount;
    @BindView(R.id.tv_reward_obtain)
    TextView tvRewardObtain;
    @BindView(R.id.tv_tv_reward_obtain_date)
    TextView tvTvRewardObtainDate;

    public RewardHistoryInfoHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }

    public void bindData(CreditCardRewardHistoryInfo historyInfo, String rewardsType) {
        //现金奖赏
        if (CreditRewardType.REWARD_TYPE_CASH.equals(rewardsType)) {
            tvRewardObtain.setTextColor(context.getColor(R.color.color_FF149B1A));
        } else {
            //积分奖赏
            tvRewardObtain.setTextColor(context.getColor(R.color.color_green));
        }

        if (historyInfo != null) {
            tvCardAccount.setText(historyInfo.getAccount());
            tvRewardObtain.setText(historyInfo.getObtainReward());
            tvTvRewardObtainDate.setText(historyInfo.getObtainRewardDate());
        }

    }
}
