package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;

public interface CrossBorderRemittanceContract {

    interface View extends IView {
        void getUIRecordListSuccess(TransferCrossBorderStatusEntity response, ArrayList<ItemModel> list);

        void getUIRecordListError(String errorCode, String errorMsg);

    }

    interface Presenter extends IPresenter<View> {
        void getUIRecordList(String tnxId, String type);

        void getUIRecordListWithSmart(String outtrfrefno, String txnType);
    }
}
