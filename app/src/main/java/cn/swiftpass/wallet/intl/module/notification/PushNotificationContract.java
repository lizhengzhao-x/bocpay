package cn.swiftpass.wallet.intl.module.notification;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;

public class PushNotificationContract {

    public interface View extends IView {

        void getUPlanUrlSuccess(UplanUrlEntity response);

        void getUPlanUrlFailed(String errorCode, String errorMsg);

        void getNewMessageUrlSuccess(NotificationJumpEntity response);

        void getNewMessageUrlFailed(String errorCode, String errorMsg);

        void getMyDiscountUrlSuccess(UplanUrlEntity response);

        void getMyDiscountUrlFailed(String errorCode, String errorMsg);
    }


    public interface Presenter<P extends PushNotificationContract.View> extends IPresenter<P> {

        /**
         * 优计划首页
         */
        void getUPlanUrl();

        /**
         * 最新消息
         */
        void getNewMessageUrl();

        /**
         * 我的优惠券
         */
        void getMyDiscountUrl();
    }

}
