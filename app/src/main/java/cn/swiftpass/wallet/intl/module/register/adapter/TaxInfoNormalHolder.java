package cn.swiftpass.wallet.intl.module.register.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TaxInfoStatusEntity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

public class TaxInfoNormalHolder extends RecyclerView.ViewHolder {
    private final Activity context;
    @BindView(R.id.iv_tax_info_tip)
    ImageView ivTaxInfoTip;
    @BindView(R.id.iv_tax_number_yes)
    ImageView ivTaxNumberYes;
    @BindView(R.id.iv_tax_number_not)
    ImageView ivTaxNumberNot;
    @BindView(R.id.tv_resident_area)
    TextView tvResidentArea;
    @BindView(R.id.ll_resident_area)
    LinearLayout llResidentArea;
    @BindView(R.id.rb_tax_number_yes)
    LinearLayout rbTaxNumberYes;
    @BindView(R.id.rb_tax_number_not)
    LinearLayout rbTaxNumberNot;
    @BindView(R.id.rg_tax_number)
    LinearLayout rgTaxNumber;
    @BindView(R.id.tv_tax_reason)
    TextView tvTaxReason;
    @BindView(R.id.tv_delete_tax)
    TextView tvDeleteTax;
    @BindView(R.id.tv_tax_number_error_tip)
    TextView tvTaxNumberErrorTip;
    @BindView(R.id.tv_tax_number)
    EditTextWithDel tvTaxNumber;
    @BindView(R.id.ll_tax_info)
    LinearLayout llTaxInfo;
    @BindView(R.id.ll_tax_number)
    LinearLayout llTaxNumber;
    @BindView(R.id.ll_tax_select)
    LinearLayout llTaxSelect;
    @BindView(R.id.ll_tax_reason)
    LinearLayout llTaxReason;

    private OnChangeOtherTaxListener listener;
    private int index;
    private OnTaxDataChangeListener taxCountryChangeListener;
    private int selectType;

    public TaxInfoNormalHolder(Activity context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
        llTaxSelect.setVisibility(View.GONE);
        llTaxNumber.setVisibility(View.GONE);
        llTaxReason.setVisibility(View.GONE);
        tvTaxNumber.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);

    }

    public void setData(TaxInfoStatusEntity taxInfoStatusEntity, int index) {
        this.index = index;
        clearCheck();
        tvTaxNumber.setContentText("");
        tvResidentArea.setText("");
        tvTaxReason.setText("");


        //复用时，数据绑定
        if (taxInfoStatusEntity != null) {
            if (taxInfoStatusEntity.getTaxResidence() != null && !TextUtils.isEmpty(taxInfoStatusEntity.getTaxResidence().getTaxResidentRegionName())) {
                tvResidentArea.setText(taxInfoStatusEntity.getTaxResidence().getTaxResidentRegionName());
            }
            if (taxInfoStatusEntity.getSelectType() == TaxInfoStatusEntity.TYPE_SELECT_TAX_NUM) {
                checkTaxNumber(true);
                llTaxSelect.setVisibility(View.VISIBLE);
                llTaxNumber.setVisibility(View.VISIBLE);
                llTaxReason.setVisibility(View.GONE);
                if (!TextUtils.isEmpty(taxInfoStatusEntity.getTaxNumber())) {
                    tvTaxNumber.setContentText(taxInfoStatusEntity.getTaxNumber());
                }
                //初始化页面数据后，需要马上刷新一下按钮
                checkTaxInfo(index, taxInfoStatusEntity);
            } else if (taxInfoStatusEntity.getSelectType() == TaxInfoStatusEntity.TYPE_SELECT_TAX_REASON) {
                checkTaxNumber(false);
                llTaxSelect.setVisibility(View.VISIBLE);
                llTaxNumber.setVisibility(View.GONE);
                llTaxReason.setVisibility(View.VISIBLE);
                if (taxInfoStatusEntity.getReason() != null && !TextUtils.isEmpty(taxInfoStatusEntity.getReason().getNoResidentReason())) {
                    tvTaxReason.setText(taxInfoStatusEntity.getReason().getNoResidentReason());
                }
                //初始化页面数据后，需要马上刷新一下按钮
                checkTaxInfo(index, taxInfoStatusEntity);
            } else {
                clearCheck();
                llTaxSelect.setVisibility(View.GONE);
                llTaxNumber.setVisibility(View.GONE);
                llTaxReason.setVisibility(View.GONE);
                tvTaxNumber.setLineRedVisible(false);
                tvTaxNumber.setLineVisible(true);
                tvTaxNumberErrorTip.setVisibility(View.GONE);
                taxInfoStatusEntity.setSelectType(TaxInfoStatusEntity.TYPE_SELECT_NONE);
            }

        } else {
            clearCheck();
            llTaxSelect.setVisibility(View.GONE);
            llTaxNumber.setVisibility(View.GONE);
            llTaxReason.setVisibility(View.GONE);
            tvTaxNumber.setLineRedVisible(false);
            tvTaxNumber.setLineVisible(true);
            tvTaxNumberErrorTip.setVisibility(View.GONE);
        }

        ivTaxInfoTip.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (listener != null) {
                    listener.showTaxInfoTip();
                }
            }
        });

        llResidentArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taxCountryChangeListener != null) {
                    taxCountryChangeListener.selectCountryData(index, taxInfoStatusEntity.getTaxResidence());
                }
            }
        });

        llTaxReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (taxCountryChangeListener != null) {
                    taxCountryChangeListener.selectTaxReasonData(index, taxInfoStatusEntity.getReason());
                }
            }
        });

        tvDeleteTax.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.deleteTax(index, taxInfoStatusEntity);
                }
            }
        });

        tvTaxNumber.getEditText().setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                if (taxInfoStatusEntity != null) {
                    taxInfoStatusEntity.setTaxNumber(tvTaxNumber.getText());
                }
                if (taxInfoStatusEntity != null &&
                        taxInfoStatusEntity.getTaxResidence() != null &&
                        !TextUtils.isEmpty(tvTaxNumber.getText()) &&
                        !TextUtils.isEmpty(taxInfoStatusEntity.getTaxResidence().getTaxNumberRegular())
                ) {
                    if (Pattern.matches(taxInfoStatusEntity.getTaxResidence().getTaxNumberRegular(), tvTaxNumber.getText().toLowerCase().trim())) {
                        tvTaxNumber.setLineRedVisible(true);
                        tvTaxNumber.setLineVisible(false);
                        tvTaxNumberErrorTip.setVisibility(View.VISIBLE);
                    } else {
                        tvTaxNumber.setLineRedVisible(false);
                        tvTaxNumber.setLineVisible(true);
                        tvTaxNumberErrorTip.setVisibility(View.GONE);
                    }

                }
                checkTaxInfo(index, taxInfoStatusEntity);
            }
        });
        rbTaxNumberYes.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                checkTaxNumber(true);
                llTaxSelect.setVisibility(View.VISIBLE);
                llTaxNumber.setVisibility(View.VISIBLE);
                llTaxReason.setVisibility(View.GONE);
                tvTaxReason.setText("");
                if (taxInfoStatusEntity != null) {
                    taxInfoStatusEntity.setReason(null);
                    taxInfoStatusEntity.setSelectType(TaxInfoStatusEntity.TYPE_SELECT_TAX_NUM);
                }
                checkTaxInfo(index, taxInfoStatusEntity);
            }
        });
        rbTaxNumberNot.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                checkTaxNumber(false);
                llTaxSelect.setVisibility(View.VISIBLE);
                llTaxNumber.setVisibility(View.GONE);
                llTaxReason.setVisibility(View.VISIBLE);
                tvTaxNumber.setContentText("");
                if (taxInfoStatusEntity != null) {
                    taxInfoStatusEntity.setTaxNumber("");
                    taxInfoStatusEntity.setSelectType(TaxInfoStatusEntity.TYPE_SELECT_TAX_REASON);
                }
                checkTaxInfo(index, taxInfoStatusEntity);
            }
        });


    }

    private void clearCheck() {
        selectType = TaxInfoStatusEntity.TYPE_SELECT_NONE;
        ivTaxNumberYes.setImageResource(R.mipmap.icon_transfer_unchecked);
        ivTaxNumberNot.setImageResource(R.mipmap.icon_transfer_unchecked);
    }

    private void checkTaxNumber(boolean select) {
        if (select) {
            selectType = TaxInfoStatusEntity.TYPE_SELECT_TAX_NUM;
            ivTaxNumberYes.setImageResource(R.mipmap.icon_transfer_checked);
            ivTaxNumberNot.setImageResource(R.mipmap.icon_transfer_unchecked);
        } else {
            selectType = TaxInfoStatusEntity.TYPE_SELECT_TAX_REASON;
            ivTaxNumberYes.setImageResource(R.mipmap.icon_transfer_unchecked);
            ivTaxNumberNot.setImageResource(R.mipmap.icon_transfer_checked);
        }
    }

    private void checkTaxInfo(int index, TaxInfoStatusEntity taxInfoStatusEntity) {
        //如果居住辖区为空则返回失败
        if (TextUtils.isEmpty(tvResidentArea.getText())) {
            listener.finishTaxInfo(index, false);
            return;
        }
        //如果选中了有税务号，则必须填写对应的税务号
        if (selectType == TaxInfoStatusEntity.TYPE_SELECT_TAX_NUM) {
            if (!TextUtils.isEmpty(tvTaxNumber.getText()) && taxInfoStatusEntity != null) {
                if (taxInfoStatusEntity.getTaxResidence() != null &&
                        !TextUtils.isEmpty(taxInfoStatusEntity.getTaxResidence().getTaxNumberRegular()) &&
                        !Pattern.matches(taxInfoStatusEntity.getTaxResidence().getTaxNumberRegular().toLowerCase().trim(), tvTaxNumber.getText())) {
                    listener.finishTaxInfo(index, true);
                } else {
                    listener.finishTaxInfo(index, false);
                }

            } else {
                listener.finishTaxInfo(index, false);
            }

        } else if (selectType == TaxInfoStatusEntity.TYPE_SELECT_TAX_REASON) {
            if (!TextUtils.isEmpty(tvTaxReason.getText())) {
                listener.finishTaxInfo(index, true);
            } else {
                listener.finishTaxInfo(index, false);
            }
        } else {
            listener.finishTaxInfo(index, false);
        }


    }

    public void setChangeOtherTaxInfoListener(OnChangeOtherTaxListener listener) {
        this.listener = listener;
    }

    public void setChangeTaxDataListener(OnTaxDataChangeListener listener) {
        this.taxCountryChangeListener = listener;
    }


    public interface OnChangeOtherTaxListener {

        void finishTaxInfo(int index, boolean finish);

        void deleteTax(int index, TaxInfoStatusEntity taxInfoStatusEntity);

        void showTaxInfoTip();
    }
}
