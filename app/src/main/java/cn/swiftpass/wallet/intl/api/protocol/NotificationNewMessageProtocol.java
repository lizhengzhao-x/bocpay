package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author lizheng.zhao
 * @date 2021/06/02
 * @description push通知跳转链接获取接口 - 最新消息
 */
public class NotificationNewMessageProtocol extends BaseProtocol {

    private String mNotificationJumpType = "";


    @Override
    public boolean isNeedLogin() {
        return true;
    }


    public NotificationNewMessageProtocol(String notificationJumpType, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mNotificationJumpType = notificationJumpType;
        mUrl = "/api/sys/notificationJump";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.NOTIFICATION_JUMP_TYPE, mNotificationJumpType);
    }
}
