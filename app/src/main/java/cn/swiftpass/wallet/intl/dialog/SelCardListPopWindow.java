package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.cardmanagement.SelCardListAdapter;

/**
 * Created by ZhangXinchao on 2018/1/9.
 */

public class SelCardListPopWindow extends BasePopWindow implements View.OnClickListener {

    private ImageView mId_back_image, id_more_image;
    private OnPopWindowOkClickListener mOnPopClickListener;
    private RecyclerView mRecycleView;
    private SelCardListAdapter selCardListAdapter;
    private List<BankCardEntity> cardEntities;

    /**
     * @param mActivity
     * @param onPopWindowOkClickListener
     * @param cardEntities
     * @param isDismissChangeAlpha       涉及到多个popwindow同时存在 一个popwindow消失不应该改变窗体透明度 否则影响效果
     */
    public SelCardListPopWindow(Activity mActivity, OnPopWindowOkClickListener onPopWindowOkClickListener, ArrayList<BankCardEntity> cardEntities, boolean isDismissChangeAlpha) {
        super(mActivity, isDismissChangeAlpha);
        this.mOnPopClickListener = onPopWindowOkClickListener;
        this.cardEntities = cardEntities;
        initView();
    }

    public SelCardListPopWindow(Activity mActivity, OnPopWindowOkClickListener onPopWindowOkClickListener, List<BankCardEntity> cardEntities) {
        super(mActivity);
        this.mOnPopClickListener = onPopWindowOkClickListener;
        this.cardEntities = cardEntities;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_cardlist);
        this.setAnimationStyle(R.style.dialog_style);
        mRecycleView = mContentView.findViewById(R.id.id_recyclerview);
        mId_back_image = mContentView.findViewById(R.id.id_back_image);
        id_more_image = mContentView.findViewById(R.id.id_more_image);
        mId_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onBackButtonClickListener();
                dismiss();
            }
        });
        id_more_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onMoreButtonClickListener();
                dismiss();
            }
        });

        mRecycleView.setAdapter(getAdapter(null));
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mRecycleView.setLayoutManager(layoutManager);


        ((IAdapter<BankCardEntity>) mRecycleView.getAdapter()).setData(cardEntities);
        mRecycleView.getAdapter().notifyDataSetChanged();
        setDisMissView(mContentView);
    }

    @Override
    public void onClick(View view) {

    }

    private CommonRcvAdapter<BankCardEntity> getAdapter(List<BankCardEntity> data) {
        return new CommonRcvAdapter<BankCardEntity>(data) {

            @Override
            public Object getItemType(BankCardEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new SelCardListAdapter(mActivity, new SelCardListAdapter.CardItemSelCallback() {

                    @Override
                    public void onItemCardSelClick(int position) {
                        super.onItemCardSelClick(position);
                        //切换卡支付
                        mOnPopClickListener.onCardSelListener(position);
                        for (int i = 0; i < cardEntities.size(); i++) {
                            if (i == position) {
                                cardEntities.get(i).setSel(true);
                            } else {
                                cardEntities.get(i).setSel(false);
                            }
                        }
                        mRecycleView.getAdapter().notifyDataSetChanged();
                        dismiss();
                    }
                });
            }
        };
    }

    public interface OnPopWindowOkClickListener {
        void onBackButtonClickListener();

        void onMoreButtonClickListener();

        void onSelCardClickListener(boolean complete);

        void onCardSelListener(int positon);
    }
}
