package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class ForgetPwdProtocol extends BaseProtocol {

    public ForgetPwdProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/checkDeviceStatus";
    }

    @Override
    public void packData() {
        super.packData();
    }
}
