package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 信用卡一键绑定发送otp
 */
public class BindNewCardVerifyOtpProtocol extends BaseProtocol {

    String verifyCode;

    String action;

    public BindNewCardVerifyOtpProtocol(String actionIn, String verifyCodeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.verifyCode = verifyCodeIn;
        this.action = actionIn;
        mUrl = "api/register/checkOtpBatchCc";

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
        mBodyParams.put(RequestParams.OTP, verifyCode);
    }

}
