package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


/**
 * 顶部图标 底部文案
 */

public class CustomImageButton extends RelativeLayout {
    private int mInputColor;
    private Context mContext;
    private String topText = "";

    public TextView getTvLeft() {
        return tvLeft;
    }

    private TextView tvLeft;

    public ImageView getImageRight() {
        return imageTop;
    }

    private ImageView imageTop;

    public CustomImageButton(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public CustomImageButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public CustomImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_image_button, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ImageTopButton, defStyle, 0);

        topText = a.getString(R.styleable.ImageTopButton_bottom_text);
        int d = a.getResourceId(R.styleable.ImageTopButton_top_image, R.mipmap.icon_button_nextxhdpi);
        mInputColor = a.getColor(R.styleable.ImageTopButton_bottom_text_color, Color.WHITE);
        a.recycle();

        tvLeft = rootView.findViewById(R.id.id_tv_buttom);
        imageTop = rootView.findViewById(R.id.id_image_top);
        imageTop.setImageResource(d);

        tvLeft.setText(topText);
        tvLeft.setTextColor(mInputColor);
    }

    public void setLeftTextTitle(String title) {
        tvLeft.setText(title);
    }
}
