package cn.swiftpass.wallet.intl.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.listener.OnItemSelectedListener;
import com.contrarywind.view.WheelView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardDateItemList;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


/**
 * 信用卡奖赏 日期选择器
 */
public class CreditCardRewardDateDialog {
    private final BottomDialog bottomDialog;
    private BaseCompatActivity mFragmentActivity;
    private CreditCardRewardDateItemList selectDateKey;
    private OnCardRewardDateListener listener;
    private int selectIndex;
    private final WheelView dateRewardWheel;
    private List<CreditCardRewardDateItemList> queryDateList = new ArrayList<>();


    private CreditCardRewardDateDialog(BaseCompatActivity fragmentActivity) {
        mFragmentActivity = fragmentActivity;
        bottomDialog = new BottomDialog(mFragmentActivity);

        View contentView = LayoutInflater.from(mFragmentActivity).inflate(R.layout.dialog_bottom_reward_date, null);
        ImageView ivClose = contentView.findViewById(R.id.iv_close);
        dateRewardWheel = contentView.findViewById(R.id.wl_reward_date);

        TextView confirmTv = contentView.findViewById(R.id.tv_confirm);
        TextView cancelTv = contentView.findViewById(R.id.tv_cancel);
        dateRewardWheel.setCyclic(false);


        dateRewardWheel.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(int index) {
                selectIndex = index;
                selectDateKey = queryDateList.get(index);
            }
        });


        confirmTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (listener != null) {
                        listener.OnSelectRewardDate(selectDateKey, selectIndex);
                    }
                    bottomDialog.dismiss();
                }

            }
        });

        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    bottomDialog.dismiss();
                }
            }
        };

        cancelTv.setOnClickListener(cancelListener);

        ivClose.setOnClickListener(cancelListener);

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            int height = (int) (AndroidUtils.getScreenHeight(fragmentActivity) * 0.4);
            dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            layoutParams.height = height;
            dialogWindow.setAttributes(layoutParams);
        }

    }


    public static CreditCardRewardDateDialog with(BaseCompatActivity fragmentActivity) {
        return new CreditCardRewardDateDialog(fragmentActivity);
    }

    public static CreditCardRewardDateDialog with(Fragment fragment) {
        Context context = fragment.getContext();
        if (context instanceof BaseCompatActivity) {
            return new CreditCardRewardDateDialog((BaseCompatActivity) context);
        }
        throw new IllegalArgumentException("fragment必须添加到某个FragmentActivity");
    }

    public static CreditCardRewardDateDialog with(Context context) {
        if (context instanceof BaseCompatActivity) {
            return new CreditCardRewardDateDialog((BaseCompatActivity) context);
        }
        throw new IllegalArgumentException("context必须来自某个FragmentActivity");
    }

    public CreditCardRewardDateDialog setListener(OnCardRewardDateListener listener) {
        this.listener = listener;
        return this;
    }


    public void show() {
        dateRewardWheel.setAdapter(new ArrayWheelAdapter<CreditCardRewardDateItemList>(queryDateList));
        dateRewardWheel.setCurrentItem(selectIndex);
        bottomDialog.showWithBottomAnim();
    }


    public CreditCardRewardDateDialog setSelectType(int selectIndex) {
        this.selectIndex = selectIndex;
        return this;
    }

    public CreditCardRewardDateDialog setData(List<CreditCardRewardDateItemList> queryDateList) {
        this.queryDateList = queryDateList;
        return this;
    }
}
