package cn.swiftpass.wallet.intl.entity.event;


public class RegisterEventEntity extends BaseEventEntity{

    public static final int EVENT_REGISTER = 8;
    public static final int EVENT_RETRY_EFDIT = 15;
    public static final int EVENT_IDV_SHOW_SUCCESS = 60;
    public static final int EVENT_IDV_SHOW_FAILED = 70;

    public static final int EVENT_IDV_CHECK_SUCCESS = 80;


    public static final int EVENT_IDV_CHECK_FAILED = 90;

    public static final int EVENT_IDV_SHOW_ERROR = 100;

    public static final int EVENT_IDV_SHOW_ERROR_TWO = 200;


    public RegisterEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public RegisterEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
