package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author create date on  on 2018/7/30 13:50
 * 校验我的账户OTP接口，返回对象为 BaseEntity
 */

public class CheckFioOtpProtocol extends BaseProtocol {
    public static final String TAG = CheckFioOtpProtocol.class.getSimpleName();
    /*
     * 短信验证码
     */
    String mVerifyCode;

    public CheckFioOtpProtocol(String verifyCode, NetWorkCallbackListener dataCallback) {
        this.mVerifyCode = verifyCode;
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/verifyOtpFIO";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
        mBodyParams.put(RequestParams.ACTION, "FIO");
    }
}
