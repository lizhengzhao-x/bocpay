package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.SignUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 信用卡确认注册接口，返回对象为
 */

public class RegisterByCreditProtocol extends E2eeProtocol {
    public static final String TAG = RegisterByCreditProtocol.class.getSimpleName();
    /**
     *支付密码
     */
    //String mPasscode;
    /**
     * 指纹
     */
    String mFingerprintId;
    /**
     * 人脸识别
     */
    String mFaceId;
    /**
     * 用户id
     */
    String mWalletId;
    /**
     * 银行卡id
     */
    String mCardId;
    /**
     * 标识取【V】：B：绑定检查；V：仅验证；F：忘记密码；
     */
    String mAction;

    public RegisterByCreditProtocol(String passcode,  String walletId, String cardId, String action, NetWorkCallbackListener dataCallback) {
        this.mPasscode = passcode;
        this.mWalletId = walletId;
        this.mCardId = cardId;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/register/updateCcInfo";
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PASSCODE, mPasscode);
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        mBodyParams.put(RequestParams.CARDID, mCardId);
        mBodyParams.put(RequestParams.ACTION, mAction);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        try {
            RegSucEntity entity = mGson.fromJson(data, mDataCallback.mType);
            if (null != entity && !TextUtils.isEmpty(entity.getServerPubKey())) {
                HttpCoreKeyManager.getInstance().saveServerPublicKey(entity.getServerPubKey());
                return SignUtils.verify(sign, data, entity.getServerPubKey());
            } else {
                return super.verifySign(sign, data);
            }
        } catch (Exception e) {
            return false;
        }
    }

}
