package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class CheckCardIdProtocol extends BaseProtocol {
    public static final String TAG = CheckCardIdProtocol.class.getSimpleName();
    /**
     * 用户的卡号
     */
    String cardId;

    public CheckCardIdProtocol(String cardId, NetWorkCallbackListener dataCallback) {
        this.cardId = cardId;
        this.mDataCallback = dataCallback;
        mUrl = "api/crossTransfer/autoBankName";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARD_NUMBER, cardId);
    }
}
