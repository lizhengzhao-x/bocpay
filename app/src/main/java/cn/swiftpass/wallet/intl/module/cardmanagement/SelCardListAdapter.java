package cn.swiftpass.wallet.intl.module.cardmanagement;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class SelCardListAdapter implements AdapterItem<BankCardEntity> {
    private Context context;
    private CardItemSelCallback cardItemSelCallback;

    public SelCardListAdapter(Context context, CardItemSelCallback cardItemSelCallback) {
        this.context = context;
        this.cardItemSelCallback = cardItemSelCallback;
    }

    private TextView mId_card_number;
    private ImageView mId_card_sel;
    private RoundedImageView id_image_view;
    private int selPosition;
    private FrameLayout id_parent_view;

    @Override
    public int getLayoutResId() {
        return R.layout.item_sel_card_item;
    }

    @Override
    public void bindViews(View root) {
        mId_card_number = root.findViewById(R.id.id_card_number);
        mId_card_sel = root.findViewById(R.id.id_card_sel);
        id_image_view = root.findViewById(R.id.id_image_view);
        id_parent_view = root.findViewById(R.id.id_parent_view);


        int margin = (int) context.getResources().getDimension(R.dimen.space_40_px);
        int width = AndroidUtils.getScreenWidth(context) - margin * 2;
        int height = width * 5 / 8;
        id_parent_view.setLayoutParams(new LinearLayout.LayoutParams(width, height));


    }

    @Override
    public void setViews() {
        id_image_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemSelCallback.onItemCardSelClick(selPosition);
            }
        });
    }

    @Override
    public void handleData(BankCardEntity cardEntity, int position) {
        selPosition = position;
        mId_card_number.setText(AndroidUtils.formatCardMastNumberStr(cardEntity.getPanShowNumber()));
        id_image_view.setBackgroundResource(R.mipmap.img_card_register_positive);
        if (cardEntity.isSel()) {
            mId_card_sel.setImageResource(R.mipmap.icon_check_card_choose);
        } else {
            mId_card_sel.setImageResource(R.mipmap.icon_check_card_default);
        }
        //设置图片圆角角度
        RoundedCorners roundedCorners = new RoundedCorners(10);
        //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);

        GlideApp.with(context)
                .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                .apply(options)
                .into(id_image_view);


    }

    public static class CardItemSelCallback {
        public void onItemCardSelClick(int position) {
            // do nothing
        }
    }
}
