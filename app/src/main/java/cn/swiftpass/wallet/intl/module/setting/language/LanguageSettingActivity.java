package cn.swiftpass.wallet.intl.module.setting.language;

import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.protocol.ChangeLanguageProtocol;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LanguageUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

import static cn.swiftpass.wallet.intl.module.MainHomeActivity.IS_RESET_LANGUAGE;


/**
 *
 */

public class LanguageSettingActivity extends BaseCompatActivity {
    @BindView(R.id.id_lan_eng)
    ImageArrowView idLanEng;
    @BindView(R.id.id_lan_zh)
    ImageArrowView idLanZh;
    @BindView(R.id.id_lan_zhhant)
    ImageArrowView idLanZhhant;
    @BindView(R.id.tv_confirm)
    TextView tv_confirm;

    private int selPosition;
    private String language;
    private boolean isSelect;
    /**
     * 原本语言的选中位置
     */
    private int positionSelectTag;
    private TextView rightTv;
    public static final String ACTION_RESET_APP_LANGUAGE = "ACTION_RESET_APP_LANGUAGE";

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(getString(R.string.setting_language));
        rightTv = setToolBarRightViewToText(getResources().getString(R.string.dialog_right_btn_ok));
        rightTv.setTextColor(getColor(R.color.color_btn_enable));
        idLanEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selPosition = 0;
                enableRightBtn();
                idLanZh.setRightImageShow(false);
                idLanZhhant.setRightImageShow(false);
                idLanEng.setRightImageShow(true);
            }
        });
        idLanZh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selPosition = 1;
                enableRightBtn();
                idLanZh.setRightImageShow(true);
                idLanZhhant.setRightImageShow(false);
                idLanEng.setRightImageShow(false);

            }
        });
        idLanZhhant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selPosition = 2;
                enableRightBtn();
                idLanZh.setRightImageShow(false);
                idLanZhhant.setRightImageShow(true);
                idLanEng.setRightImageShow(false);
            }
        });

        rightTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick(v.getId(), 1000)) {
                    rightTv.setEnabled(false);
                    changeLanguageWithPosition();
                }
            }
        });
        idLanZh.setRightImageShow(false);
        idLanZhhant.setRightImageShow(false);
        idLanEng.setRightImageShow(false);


        language = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(language)) {
            idLanZh.setRightImageShow(true);
            positionSelectTag = 1;
        } else if (AndroidUtils.isHKLanguage(language)) {
            idLanZhhant.setRightImageShow(true);
            positionSelectTag = 2;
        } else {
            idLanEng.setRightImageShow(true);
            positionSelectTag = 0;
        }
        rightTv.setEnabled(false);

    }


    private void changeLanguage(String language) {
        new ChangeLanguageProtocol(language, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(ContentEntity response) {

            }
        }).execute();
    }

    private void enableRightBtn() {
        rightTv.setEnabled(positionSelectTag != selPosition);
        rightTv.setTextColor(positionSelectTag != selPosition ? getColor(R.color.app_white) : Color.parseColor("#ccFFFFFF"));
    }

    private void changeLanguageWithPosition() {

        if (selPosition == 0) {
            switchLanguageAndResart(HttpCoreConstants.LANG_CODE_EN_US);
            changeLanguage(HttpCoreConstants.LANG_CODE_EN_US);
        } else if (selPosition == 1) {
            switchLanguageAndResart(HttpCoreConstants.LANG_CODE_ZH_CN);
            changeLanguage(HttpCoreConstants.LANG_CODE_ZH_CN_NEW);
        } else if (selPosition == 2) {
            switchLanguageAndResart(HttpCoreConstants.LANG_CODE_ZH_TW);
            changeLanguage(HttpCoreConstants.LANG_CODE_ZH_HK_NEW);
        }
    }


    private void switchLanguageAndResart(String language) {
        Locale locale = null;
        if (AndroidUtils.isHKLanguage(language)) {
            // 繁体
            locale = Locale.TRADITIONAL_CHINESE;
        } else if (AndroidUtils.isEGLanguage(language)) {
            locale = Locale.ENGLISH;
        } else if (AndroidUtils.isZHLanguage(language)) {
            locale = Locale.SIMPLIFIED_CHINESE;
        } else {
            // 跟随系统 //默认英文
            locale = Locale.getDefault();
        }
        LanguageUtils.setDefaultLanguage(locale);
        HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, language);
        if (CacheManagerInstance.getInstance().isLogin()) {
            Intent intent = new Intent(this, MainHomeActivity.class);
            intent.putExtra(IS_RESET_LANGUAGE,true);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, PreLoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_language_setting;
    }


}
