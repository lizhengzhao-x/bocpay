package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.text.TextUtils;

import com.bochklaunchflow.BOCHKLaunchFlow;
import com.bochklaunchflow.bean.BOCLFModel;
import com.bochklaunchflow.bean.Env;
import com.bochklaunchflow.callback.GoToPageCallback;
import com.bochklaunchflow.constant.BOCHKLFConstant;
import com.bochklaunchflow.utils.BOCLFDialogUtils;
import com.bochklaunchflow.utils.BOCLFUtils;

import java.util.HashMap;
import java.util.Map;

import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.guide.GuideNewActivity;

/**
 * Created by ramon on 2018/9/27.
 * 启动检查SDK
 */

public class BocLaunchSDK {

    public static void checkBocLaunchSDK(Activity activity, final GoToPageCallback goToPageCallback) {


        BOCLFDialogUtils.getInstance().setCustomDialogView(activity, true, R.layout.dialog_common, R.id.tvMessage, R.id.btnConfirm, R.id.btnCancel);
        //從配置文件讀出相應銀行的對應環境的一些基本配置 
        String curEnv = "";

        if (BuildConfig.FLAVOR.toLowerCase().contains(Constants.BUILD_FLAVOR_PRD)) {
            curEnv = "PROD";
        } else if (BuildConfig.FLAVOR.toLowerCase().contains(Constants.BUILD_FLAVOR_SIT)) {
            curEnv = "SIT";
        } else if (BuildConfig.FLAVOR.toLowerCase().contains(Constants.BUILD_FLAVOR_UAT)) {
            curEnv = "UAT";
        }
        if (TextUtils.isEmpty(curEnv)) {
            curEnv = "PROD";
        }
        Env env = BOCLFUtils.readConfig(activity, "BOCHKLFConfig.json", "BOC", curEnv);
        //設置請求頭
        Map<String, String> headers = new HashMap<>();
        headers.put("appname", activity.getString(R.string.app_name_main));
        //構建Model
        String lang = "";
        String lan = SpUtils.getInstance(activity).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            lang = BOCHKLFConstant.ZHCN;
        } else if (AndroidUtils.isHKLanguage(lan)) {
            lang = BOCHKLFConstant.ZHHK;
        } else {
            lang = BOCHKLFConstant.ENUS;
        }
        String goClassName = MainHomeActivity.class.getName();
        if (!CacheManagerInstance.getInstance().isShowGuidePage()) {
            goClassName = GuideNewActivity.class.getName();
        }
        goClassName = null;
        //String provisionName = ServiceAgreementActivity.class.getName();
        String provisionName = null;
        BOCLFModel model = new BOCLFModel(activity, env.getAppLinkAOS(), env.getAppBaseURL(), env.getRootReportUrl(), BuildConfig.APPLICATION_ID, env.getBankId(), lang, goClassName, provisionName, headers, null, null);
        //在網絡檢查功能中，設置無網絡是否彈框。默認為true,有彈框。若為false,無彈框
        BOCHKLaunchFlow.getInstance().setCheckNetWorkShowDialog(false);


        try {
	    /*GoToPageCallback是在啟動流程完成后的回調,如果為空,則默認為跳轉主頁和條款頁，
	       不為空的話，則可以自己在beforeToProvision()和beforeToHome()方法中自定義，最後記得寫跳轉頁面方法*/
            BOCHKLaunchFlow.getInstance().Launch(model, new GoToPageCallback() {
                @Override
                public void beforeToHome(Activity activity) {
                    goToPageCallback.beforeToHome(activity);
                }

                @Override
                public void beforeToProvision(Activity activity) {
                    goToPageCallback.beforeToProvision(activity);
                }
            });
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 更新版本号
     *
     * @param context
     */
    public static void saveAppVersion(Activity context) {
        //BOCLFUtils.saveAppVersion(activity);
//        checkIsUpdateToJanuaryVersion(context);
        SpUtils.getInstance(context).setAppVersionName(context);

    }

    /**
     * 比较两个版本号
     *
     * @param context
     * @return
     */
    public static boolean checkIsFirstTimeTC(Activity context) {
        String preVersionName = SpUtils.getInstance(context).getAppVersionName();
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException var5) {
            var5.printStackTrace();
        }
        boolean flag = false;
        String versionNum = pInfo.versionName;
        if (preVersionName == null) {
            flag = true;
        } else if (!preVersionName.equals(versionNum)) {
            flag = true;
        } else {
            flag = false;
        }
        return flag;
    }

    /**
     * 判断是否升级到1月版本
     *
     * @return
     */
    public static void checkIsUpdateToJanuaryVersion(Activity context) {
        String currentVersion = BuildConfig.VERSION_NAME;
        String preVersionName = SpUtils.getInstance(context).getAppVersionName();
        if (!(TextUtils.equals(currentVersion, preVersionName)) && !TextUtils.isEmpty(preVersionName)){
            //每次升级都要弹推广通知的弹框 所以 判断到每次升级 要把之前显示过弹框的状态清楚 保证登录态 非登录态可以弹框
            SpUtils.getInstance(context).saveShowNotificationDialog(false);
            //每次升级引导页状态要保证显示过 状态也要保存
            SpUtils.getInstance(context).saveShowGuidePage(false);
        }
    }

}
