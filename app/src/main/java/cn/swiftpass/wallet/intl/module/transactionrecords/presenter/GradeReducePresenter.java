package cn.swiftpass.wallet.intl.module.transactionrecords.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.protocol.QrcodeWithPointsProtocol;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.scan.PaySuccussActivity;
import cn.swiftpass.wallet.intl.module.transactionrecords.contract.GradeReduceContract;
import cn.swiftpass.wallet.intl.utils.DialogUtils;

public class GradeReducePresenter extends BasePresenter<GradeReduceContract.View> implements GradeReduceContract.Presenter {
    @Override
    public void gradeReduceInfo(String txnId) {

        if (getView()!=null){
            getView().showDialogNotCancel();
        }

        new QrcodeWithPointsProtocol(txnId, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                }
                getView().gradeReduceInfoFailed(errorCode,errorMsg);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                }
               getView().gradeReduceInfoSuccess(response);
            }
        }).execute();



    }


}
