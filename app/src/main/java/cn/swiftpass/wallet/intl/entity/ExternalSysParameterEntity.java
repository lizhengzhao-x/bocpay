package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class ExternalSysParameterEntity extends BaseEntity {


    public static final String QRCODETYPEFPS = "FPS";
    public static final String QRCODETYPEUPI = "UPI";
    private String qrCode;
    /**
     * FPS:fps转账   UPI:银联
     */
    private String codeType;
    private String resultCode;

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    private String appLink;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public ActionTrxGpInfoEntity getUpiData() {
        return upiData;
    }

    public void setUpiData(ActionTrxGpInfoEntity upiData) {
        this.upiData = upiData;
    }

    private FpsData fpsData;
    private ActionTrxGpInfoEntity upiData;

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getCodeType() {
        return codeType;
    }

    public void setFpsData(FpsData fpsData) {
        this.fpsData = fpsData;
    }

    public FpsData getFpsData() {
        return fpsData;
    }



    public static class FpsData {
        private String acNo;
        private String drAmt;
        private String paymentCategory;
        private String crChinNm;
        private String crModifyName;
        private String smartAcLevel;
        private String crDisplayedEngNm;
        private String srvcChrgDrCur;
        private String scrRefNo;
        private String transferAccNo;

        public String getNeedOtpVerify() {
            return needOtpVerify;
        }

        public void setNeedOtpVerify(String needOtpVerify) {
            this.needOtpVerify = needOtpVerify;
        }

        private String needOtpVerify;

        //=------------------一下参数暂时未用到 server返回----------------------//
        private String dsplName;

        private String transferCurrency;
        private String transferAmount;
        private String transferBankCode;
        private String editableTraAmountFalg;
        private String mobile;
        //=------------------一下参数暂时未用到 server返回----------------------//



        public String getTansferToAccount() {
            return transferAccNo;
        }

        public void setTansferToAccount(String tansferToAccount) {
            this.transferAccNo = tansferToAccount;
        }

        public String getTransferAccTp() {
            return transferAccTp;
        }

        public void setTransferAccTp(String transferAccTp) {
            this.transferAccTp = transferAccTp;
        }

        private String transferAccTp;

        public void setAcNo(String acNo) {
            this.acNo = acNo;
        }

        public String getAcNo() {
            return acNo;
        }

        public void setDrAmt(String drAmt) {
            this.drAmt = drAmt;
        }

        public String getDrAmt() {
            return drAmt;
        }

        public void setPaymentCategory(String paymentCategory) {
            this.paymentCategory = paymentCategory;
        }

        public String getPaymentCategory() {
            return paymentCategory;
        }

        public void setCrChinNm(String crChinNm) {
            this.crChinNm = crChinNm;
        }

        public String getCrChinNm() {
            return crChinNm;
        }

        public void setCrModifyName(String crModifyName) {
            this.crModifyName = crModifyName;
        }

        public String getCrModifyName() {
            return crModifyName;
        }

        public void setSmartAcLevel(String smartAcLevel) {
            this.smartAcLevel = smartAcLevel;
        }

        public String getSmartAcLevel() {
            return smartAcLevel;
        }

        public void setCrDisplayedEngNm(String crDisplayedEngNm) {
            this.crDisplayedEngNm = crDisplayedEngNm;
        }

        public String getCrDisplayedEngNm() {
            return crDisplayedEngNm;
        }

        public void setSrvcChrgDrCur(String srvcChrgDrCur) {
            this.srvcChrgDrCur = srvcChrgDrCur;
        }

        public String getSrvcChrgDrCur() {
            return srvcChrgDrCur;
        }

        public void setScrRefNo(String scrRefNo) {
            this.scrRefNo = scrRefNo;
        }

        public String getScrRefNo() {
            return scrRefNo;
        }

    }

}
