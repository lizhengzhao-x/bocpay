package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.PrimaryAccountEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountCheckAccountEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.adapter.PrimaryAccountAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 注册我的账户 选择账户页面
 */

public class PrimaryAccountSelActivity extends BaseCompatActivity {

    public static final String PRIMARY_ACCOUNTS = "PRIMARY_ACCOUNTS";
    public static final String PRIMARY_ACCOUNT = "PRIMARY_ACCOUNT";
    public static final String PRIMARY_ACCOUNT_CURRENT = "SEL_PRIMARY_ACCOUNT";

    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    private List<PrimaryAccountEntity> mPrimaryAccountList;
    private ArrayList<SmartAccountCheckAccountEntity> mAccountEntityList;
    String mSelAccount;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.primary_account);
        if (null == getIntent()) {
            finish();
        } else {
            mAccountEntityList = (ArrayList<SmartAccountCheckAccountEntity>) getIntent().getSerializableExtra(PRIMARY_ACCOUNTS);
            mSelAccount = getIntent().getStringExtra(PRIMARY_ACCOUNT_CURRENT);
            if (null == mAccountEntityList || mAccountEntityList.isEmpty()) {
                finish();
            } else {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                layoutManager.setRecycleChildrenOnDetach(true);
                idRecyclerview.setLayoutManager(layoutManager);
                idRecyclerview.setAdapter(getAdapter(null));
                mPrimaryAccountList = new ArrayList<>();
                for (int i = 0; i < mAccountEntityList.size(); i++) {
                    PrimaryAccountEntity primaryAccountEntity = new PrimaryAccountEntity();
                    primaryAccountEntity.setAccount(AndroidUtils.getPrimaryAccountDisplay(mAccountEntityList.get(i)));
                    primaryAccountEntity.setSel(false);
                    if (!TextUtils.isEmpty(mSelAccount)) {
                        String selAccount = AndroidUtils.getSubMasCardNumber(mSelAccount);
                        String account = primaryAccountEntity.getAccount();
                        if (!TextUtils.isEmpty(account)) {
                            account = account.replace("-", "");
                            if (account.contains(selAccount)) {
                                primaryAccountEntity.setSel(true);
                            }
                        }
                    }
                    mPrimaryAccountList.add(primaryAccountEntity);
                }
                ((IAdapter<PrimaryAccountEntity>) idRecyclerview.getAdapter()).setData(mPrimaryAccountList);
                idRecyclerview.getAdapter().notifyDataSetChanged();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_primaryaccount_sel;
    }


    private CommonRcvAdapter<PrimaryAccountEntity> getAdapter(final List<PrimaryAccountEntity> data) {
        return new CommonRcvAdapter<PrimaryAccountEntity>(data) {

            @Override
            public Object getItemType(PrimaryAccountEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new PrimaryAccountAdapter(mContext, new PrimaryAccountAdapter.MessageItemCallback() {
                    @Override
                    public void onMessageItemCallback(int position) {
                        super.onMessageItemCallback(position);
                        if (getActivity() != null) {
                            SmartAccountCheckAccountEntity account = null;
                            for (int i = 0; i < mPrimaryAccountList.size(); i++) {
                                if (position == i) {
                                    account = mAccountEntityList.get(i);
                                    boolean bSel = mPrimaryAccountList.get(i).isSel();
                                    if (!bSel) {
                                        mPrimaryAccountList.get(i).setSel(!bSel);
                                    }
                                } else {
                                    mPrimaryAccountList.get(i).setSel(false);
                                }
                            }
                            idRecyclerview.getAdapter().notifyDataSetChanged();
                            Intent in = new Intent();
                            in.putExtra(PRIMARY_ACCOUNT, account);
                            setResult(RESULT_OK, in);
                            finish();
                        }
                    }
                });
            }
        };
    }

}
