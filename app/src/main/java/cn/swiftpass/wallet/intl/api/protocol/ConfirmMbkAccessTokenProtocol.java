package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class ConfirmMbkAccessTokenProtocol extends BaseProtocol {

    private final String ts;
    private final String channel;
    private final String nonceStr;
    private final String signature;

    public ConfirmMbkAccessTokenProtocol(String ts, String channel, String nonceStr, String signature, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/onlineChat/confirmAccessToken";
        this.ts = ts;
        this.channel = channel;
        this.nonceStr = nonceStr;
        this.signature = signature;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TS, ts);
        mBodyParams.put(RequestParams.CHANNEL, channel);
        mBodyParams.put(RequestParams.NONCESTR, nonceStr);
        mBodyParams.put(RequestParams.SIGNATURE, signature);
    }
}
