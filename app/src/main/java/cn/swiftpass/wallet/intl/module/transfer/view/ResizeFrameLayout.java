package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * android:windowSoftInputMode="adjustNothing"
 */
public class ResizeFrameLayout extends RelativeLayout {
    private static final String TAG = "ResizeFrameLayout";
    private KeyboardListener mListener;
    private int minKeyboardHeight;
    private View mTranslationYView;
    private int screenHeight;
    private View currentFocusView;


    public interface KeyboardListener {
        void onKeyboardShown(int height);

        void onKeyboardHidden(int height);
    }

    public ResizeFrameLayout(Context context) {
        super(context);
        init();
    }

    public ResizeFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ResizeFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        screenHeight = AndroidUtils.getScreenHeight(getContext());
    }

    public void setKeyboardListener(KeyboardListener listener) {
        mListener = listener;
    }

    public void setTranslationYView(View view) {
        mTranslationYView = view;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                AndroidUtils.hideKeyboard(currentFocusView);//调用方法判断是否需要隐藏键盘
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }


    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        // 认为键盘最小高度为屏幕的1/4
        DisplayMetrics dm = getResources().getDisplayMetrics();
        minKeyboardHeight = dm.heightPixels / 4;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (h == 0 || oldh == 0) {
            return;
        }
        if (oldh - h > minKeyboardHeight) {
            notifyKeyboardEvent(true, oldh - h);
        } else if (h - oldh > minKeyboardHeight) {
            notifyKeyboardEvent(false, h - oldh);
        }
    }

    private void notifyKeyboardEvent(boolean show, int keyboardHeight) {
        if (getContext() != null) {
            Activity mActivity = (Activity) getContext();
            currentFocusView = mActivity.getWindow().getDecorView().findFocus();
            if (currentFocusView instanceof EditText) {
                int[] location = new int[2];
                currentFocusView.getLocationOnScreen(location);
                int currentViewYPos = location[1];
                if (currentViewYPos > screenHeight - keyboardHeight) {
                    mTranslationYView.setTranslationY(-(currentViewYPos - screenHeight + keyboardHeight));
                } else {
                }
            }
        }
        LogUtils.i(TAG, "show:" + show + " keyboardHeight: " + keyboardHeight + " view: " + currentFocusView);
        if (mListener == null) {
            return;
        }
        if (show) {
            mListener.onKeyboardShown(keyboardHeight);
        } else {
            mListener.onKeyboardHidden(keyboardHeight);
        }
    }
}
