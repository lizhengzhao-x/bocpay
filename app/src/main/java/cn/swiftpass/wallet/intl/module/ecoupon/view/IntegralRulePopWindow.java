package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.app.Activity;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class IntegralRulePopWindow extends BasePopWindow implements View.OnClickListener {


    private ImageView closeIv, tableIv;
    private RedeemableGiftListEntity.ConfigBean configBean;
    private TextView id_sub_title, id_content;


    public IntegralRulePopWindow(Activity activity, RedeemableGiftListEntity.ConfigBean configBeanIn) {
        super(activity);
        this.configBean = configBeanIn;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_integral_rule_view);

        bindViews();
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.8));
        setHeight(LinearLayout.LayoutParams.WRAP_CONTENT);

    }

    private void bindViews() {
        closeIv = (ImageView) mContentView.findViewById(R.id.iv_close);
        tableIv = (ImageView) mContentView.findViewById(R.id.iv_table);
        id_sub_title = (TextView) mContentView.findViewById(R.id.id_sub_title);
        id_content = (TextView) mContentView.findViewById(R.id.id_content);
        if (configBean != null) {
            id_sub_title.setText(configBean.getTitle());
            id_content.setText(configBean.getContent());
            if (TextUtils.isEmpty(configBean.getImageUrl())) {
                tableIv.setVisibility(View.GONE);
            } else {
                GlideApp.with(mActivity).load(AppTestManager.getInstance().getBaseUrl() + configBean.getImageUrl()).into(tableIv);
            }
        }

//        String lan = SpUtils.getInstance(mActivity).getAppLanguage();
//        if (AndroidUtils.isZHLanguage(lan)) {
//            tableIv.setImageResource(R.mipmap.table_discount_sc);
//        } else if (AndroidUtils.isHKLanguage(lan)) {
//            tableIv.setImageResource(R.mipmap.table_discount_zh);
//        } else {
//            tableIv.setImageResource(R.mipmap.table_discount_en);
//        }
        closeIv.setOnClickListener(this);
    }


    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, 0, 0);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
        }
    }
}
