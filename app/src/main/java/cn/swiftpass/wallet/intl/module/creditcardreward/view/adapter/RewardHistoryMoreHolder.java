package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class RewardHistoryMoreHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_click_more)
    TextView tvClickMore;
    @BindView(R.id.tv_tip_no_more)
    TextView tvTipNoMore;

    public RewardHistoryMoreHolder(View view, OnRewadLoadMoreListener loadMoreListener) {
        super(view);
        ButterKnife.bind(this, view);
        tvClickMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadMoreListener.onLoadMore();
            }
        });
    }

    public void bindData(boolean hasNext, String lastRewardDate) {
        tvClickMore.setVisibility(View.GONE);
        tvTipNoMore.setVisibility(View.VISIBLE);
        tvTipNoMore.setText(lastRewardDate);
        if (hasNext) {
            tvClickMore.setVisibility(View.VISIBLE);
        }
    }
}
