package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * @author zhaolizheng
 * Created on 2020/12/30
 */

public class PAAccountVerifyCodeEntity extends BaseEntity {
    //验证码base64
    String imageCode;

    //验证码长度
    String verifyCodeLength;

    public String getVerifyCodeLength() {
        return verifyCodeLength;
    }

    public void setVerifyCodeLength(String verifyCodeLength) {
        this.verifyCodeLength = verifyCodeLength;
    }


    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }
}
