package cn.swiftpass.wallet.intl.sdk.sa;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.utils.PhoneUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;


/**
 * 埋点:事件基类
 */
public abstract class AnalysisBaseEntity extends BaseEntity {
    /**
     * 埋点数据类型，ACTION_CLI_POINT 点击事件埋点；PA_REG_FLOW_POINT pa注册流程埋点；PA_REG_ERR_POINT pa注册错误码埋点
     */
    public String dataType;

    /**
     * 客户端deviceID + app versionNo 组成
     */
    public String device_id;

    /**
     * 客户端系统类型 AOS IOS
     */
    public String app_system;

    /**
     * 客户端版本号
     */
    public String app_version;

    /**
     * app名称
     */
    public String app_name;

    /**
     * 客户端使用uuid生成唯一记录id
     */
    public String record_id;

    /**
     * 事件类型抽象方法
     */
    public abstract String getDataType();

    public AnalysisBaseEntity() {
        this.app_name = "bocpay";
        this.app_system = "AOS";
        this.app_version = BuildConfig.VERSION_NAME;
        this.device_id = PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME;
        this.record_id = TempSaveHelper.getRegisterRecordId();
    }

    public static String longToStr(String time, String format) {
        if (TextUtils.isEmpty(time)) {
            return "";
        }
        return new SimpleDateFormat(format).format(longTocalendar(Long.valueOf(time)).getTime());
    }

    public static String longToStr(long time, String format) {
        return new SimpleDateFormat(format).format(longTocalendar(time).getTime());
    }

    public static String longToSecond(long time) {
        try {
            double second = time / 1000d;
            return String.format("%.3f", second);
        } catch (Exception e) {

        }
        return "";
    }

    public static Calendar longTocalendar(long time) {
        Date date = new Date(time);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c;
    }

    public JSONObject getJsonObject() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(AnalysisParams.DATA_TYPE, getDataType());
        jsonObject.put(AnalysisParams.APP_NAME, app_name);
        jsonObject.put(AnalysisParams.APP_SYSTEM, app_system);
        jsonObject.put(AnalysisParams.APP_VERSION, app_version);
        jsonObject.put(AnalysisParams.DEVICE_ID, device_id);
        jsonObject.put(AnalysisParams.RECORD_ID, record_id);
        return jsonObject;
    }
}
