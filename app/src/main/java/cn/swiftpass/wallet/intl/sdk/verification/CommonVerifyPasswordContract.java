package cn.swiftpass.wallet.intl.sdk.verification;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;


public class CommonVerifyPasswordContract {

    public interface View extends IView {

        void getCheckPasswordWithActionSuccess(BaseEntity response);

        void getCheckPasswordWithActionError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {

        void getCheckPasswordWithAction(String payPwd, String action);
    }

}
