package cn.swiftpass.wallet.intl.module.register.model;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.CheckPAAcountVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.PAAccountVerifyCodeProtocol;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeEntity;

public class PARegisterVerifyCodeModel {


    /**
     * 获取PA用户注册界面验证码
     *
     * @param action
     * @param dataCallback
     */
    public static void getPARegisterVerifyCode(String action, NetWorkCallbackListener dataCallback) {
        new PAAccountVerifyCodeProtocol(action, new NetWorkCallbackListener<PARegisterVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dataCallback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PARegisterVerifyCodeEntity response) {
                dataCallback.onSuccess(response);
            }
        }).execute();
    }


    /**
     * 检查PA用户注册界面验证码
     *
     * @param mobile
     * @param verifyCode
     * @param action
     * @param dataCallback
     */
    public static void checkPARegisterVerifyCode(String mobile, String verifyCode, String action, NetWorkCallbackListener dataCallback) {
        new CheckPAAcountVerifyCodeProtocol(mobile, verifyCode, action, new NetWorkCallbackListener<PARegisterVerifyCodeCheckEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dataCallback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PARegisterVerifyCodeCheckEntity response) {
                dataCallback.onSuccess(response);
            }
        }).execute();
    }

}
