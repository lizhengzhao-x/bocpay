package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 注册获取tnx id
 */
public class GetTnxIdForBindSmartAccountProtocol extends BaseProtocol {

    public static final String TAG = GetTnxIdForBindSmartAccountProtocol.class.getSimpleName();

    public GetTnxIdForBindSmartAccountProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartBind/getTxnId";
    }

}
