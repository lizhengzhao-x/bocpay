package cn.swiftpass.wallet.intl.widget;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.LogUtils;


public class CustomProgressDialog extends Dialog {

    private Context context = null;
    private static final String TAG = "CustomProgressDialog";

    /**
     * dialog 是否可以点击back按键退出
     *
     * @param cancel
     */
    public void setCancel(boolean cancel) {
        this.isCancel = cancel;
    }

    private boolean isCancel = true;
    private static CustomProgressDialog customProgressDialog = null;

    public CustomProgressDialog(Context context) {
        super(context);
        this.context = context;
    }

    public CustomProgressDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }
    public static CustomProgressDialog createDialog(Context context, String message) {
       return createDialog( context, message,0);
    }

    public static CustomProgressDialog createDialog(Context context, String message,int alpha) {
        customProgressDialog = new CustomProgressDialog(context, R.style.CustomProgressDialog);
        customProgressDialog.setContentView(R.layout.custom_progressdialog);
        customProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        customProgressDialog.setMessage(message);
        //去掉遮罩层（全透明）
        LogUtils.i(TAG, "dimAmount:" + customProgressDialog.getWindow().getAttributes().alpha);
        if (alpha<=0){
            customProgressDialog.getWindow().setDimAmount(0f);
        }
        return customProgressDialog;
    }


//        @Override
//        public void onWindowFocusChanged(boolean hasFocus) {
//        if (customProgressDialog == null) {
//            return;
//        }
//
//        ImageView imageView = customProgressDialog.findViewById(R.id.loadingImageView);
//        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
//        animationDrawable.start();
//    }

    @Override
    public void show() {
        try {
            super.show();
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (customProgressDialog == null) {
            return;
        }
        ImageView imageView = customProgressDialog.findViewById(R.id.loadingImageView);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();

        Window window = getWindow();
        if (hasFocus && window != null) {
            View decorView = window.getDecorView();
            if (decorView.getHeight() == 0 || decorView.getWidth() == 0) {
                decorView.requestLayout();
                LogUtils.i("CustomMsgDialog", "ERROR onWindowFocusChanged");
            }
        }
    }


    /**
     * 动态判断dialog 点击back按键是否可以取消
     *
     * @param event
     * @return
     */
    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        LogUtils.i(TAG, "isCancel:" + isCancel);
        if (!isCancel) {
            return false;
        } else {
            customProgressDialog.dismiss();
        }
        return true;
    }

    public CustomProgressDialog setMessage(String strMessage) {
        TextView tvMsg = customProgressDialog.findViewById(R.id.id_tv_loadingmsg);
        if (tvMsg != null) {
            tvMsg.setText(strMessage);
        }
        return customProgressDialog;
    }
}
