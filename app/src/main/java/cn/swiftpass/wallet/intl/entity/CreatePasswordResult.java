/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import java.io.Serializable;

/**
 * Class Name: CreatePasswordResult<br/>
 *
 * Class Description:
 *
 *
 * @author 86755221
 * @version 1 Date: 2018-1-19
 *
 */

public class CreatePasswordResult implements Serializable {
    private static final long serialVersionUID = 1512289787745158247L;

    private String biometricValue;
    private String mobile;

    /**
     *
     */
    public CreatePasswordResult() {
        super();
    }

    /**
     * @param biometricValue
     * @param mobile
     */
    public CreatePasswordResult(String biometricValue, String mobile) {
        super();
        this.biometricValue = biometricValue;
        this.mobile = mobile;
    }

    /**
     * @return the biometricValue
     */
    public String getBiometricValue() {
        return this.biometricValue;
    }

    /**
     * @param biometricValue
     *            the biometricValue to set
     */
    public void setBiometricValue(String biometricValue) {
        this.biometricValue = biometricValue;
    }

    /**
     * @return the mobile
     */
    public String getMobile() {
        return this.mobile;
    }

    /**
     * @param mobile
     *            the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * @return the serialversionuid
     */
    public static long getSerialversionuid() {
        return serialVersionUID;
    }


}
