package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * 主页 生活场景区
 */
public class HomeLifeMenuEntity extends BaseEntity {
    /*   "menuKey":"UPLAN",
          "name":"优计划"*/
    private String menuKey;
    private String name;
    private int resIdByImage;
    private int count;

    public HomeLifeMenuEntity(String menuKey, String name, int resIdByImage) {
        this.menuKey = menuKey;
        this.name = name;
        this.resIdByImage = resIdByImage;
    }


    public int getResIdByImage() {
        return resIdByImage;
    }

    public void setResIdByImage(int resIdByImage) {
        this.resIdByImage = resIdByImage;
    }

    public String getMenuKey() {
        return menuKey;
    }

    public void setMenuKey(String menuKey) {
        this.menuKey = menuKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }


    public void setCount(int count) {
        this.count = count;
    }
}
