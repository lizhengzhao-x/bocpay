package cn.swiftpass.wallet.intl.module.redpacket.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;

/**
 * 收利是
 */
public class GetLiShiContract {

    public interface View extends IView {

        void getGetLiShiHomeDataSuccess(GetLiShiHomeEntity response);

        void getGetLiShiHomeDataError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<GetLiShiContract.View> {
        void getGetLiShiHomeData();

        void getGetLiShiHomeData(boolean showDialog);
    }

}
