package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import cn.swiftpass.httpcore.entity.BankCardEntity;

class CardContentItem extends BaseCardItem {
    public CardContentItem(long id, BankCardEntity bankCardEntity) {
        super(id, bankCardEntity);
    }
}
