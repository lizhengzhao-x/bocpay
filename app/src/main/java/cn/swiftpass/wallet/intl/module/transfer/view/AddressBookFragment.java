package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferContactContract;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.ContactSortAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.ContactLocalCacheUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.SideBar;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.MyItemDecoration;

/**
 * Created by ZhangXinchao on 2019/10/30.
 * 通讯录
 */
public class AddressBookFragment extends BaseTransferSelFragment<ContactEntity, TransferContactContract.Presenter> {
    private static final String TAG = "AddressBookFrament";
    @BindView(R.id.ry_contact)
    RecyclerView ryContact;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.id_dialog)
    TextView idDialog;

    private ContactSortAdapter contactSortAdapter;
    MyItemDecoration myItemDecoration;

    public List<ContactEntity> getItemCommonCollectionList() {
        return itemCommonCollectionList;
    }

    private List<ContactEntity> itemCommonCollectionList;

//    /**
//     * 是否第一次选择拒绝通讯录权限且不再询问权限
//     */
//    public boolean firstRejectContacts = true;

    public static AddressBookFragment newInstance() {
        return new AddressBookFragment();
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {
        //getPermissionRequest();
    }

    private void updateContactList(ArrayList<ContactEntity> contactEntities) {
        if (contactEntities == null) {
            return;
        }
        //清除默认选中状态
        for (int j = 0; j < contactEntities.size(); j++) {
            ContactEntity contactEntitItem = contactEntities.get(j);
            contactEntitItem.setCollect(false);
        }
        //通讯录与本地缓存的列表对比 标记已经收藏的 ui显示不同
        List<String> contactEntitiesLocal = ContactLocalCacheUtils.getLocalCacheWithUserId(getContext(), HttpCoreKeyManager.getInstance().getUserId());
        if (contactEntitiesLocal == null || contactEntitiesLocal.size() == 0) {

        } else {
            if (!contactEntities.isEmpty()) {
                for (int i = 0; i < contactEntitiesLocal.size(); i++) {
                    String contactID = contactEntitiesLocal.get(i);
                    for (int j = 0; j < contactEntities.size(); j++) {
                        ContactEntity contactEntitItem = contactEntities.get(j);
                        if (contactEntitItem.getContactID().equals(contactID)) {
                            contactEntitItem.setCollect(true);
                            break;
                        }
                    }
                }
            }
        }
        if (itemCommonCollectionList != null) {
            itemCommonCollectionList.clear();
            itemCommonCollectionList.addAll(contactEntities);
            updateRecycleView();
        }
    }


    private void updateRecycleView() {
        if (itemCommonCollectionList.size() > 0) {
            idSideBar.setVisibility(View.VISIBLE);
            /**
             * 判断itemDecor的个数来要不要添加itemDecoration
             */
            if (ryContact.getItemDecorationCount() <= 0) {
                ryContact.addItemDecoration(myItemDecoration);
            }

        } else {
            setEmptyText(R.string.TR1_2b_1);
            idSideBar.setVisibility(View.GONE);

            /**
             * 需要删除itemDecoration  保证emptyView的位置是居中的
             */
            ryContact.removeItemDecoration(myItemDecoration);
        }

        contactSortAdapter.setDataList(itemCommonCollectionList);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateItemList();
            }
        });
        swipeRefreshLayout.setColorSchemeResources(R.color.app_dark_red);
    }


    @Override
    protected void updateContactList(ArrayList<ContactEntity> contactEntities, boolean obtain) {
        if (!obtain) {
            itemCommonCollectionList = new ArrayList<>();
            updateRecycleView();
            setEmptyText(R.string.SRP2101_1_5);
        } else {
            updateContactList(contactEntities);
            //如果发生权限变更 最近联系人列表也要刷新显示
            EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_LOCAL_CACHE_CONTACT, ""));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getView() != null) {
            getView().setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    protected TransferContactContract.Presenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_common_collection_contact_view;
    }

    @Override
    protected void initView(View v) {
        LogUtils.i(TAG, "initView AddressBookFrament--->");
        itemCommonCollectionList = new ArrayList<>();
        EventBus.getDefault().register(this);

        contactSortAdapter = new ContactSortAdapter(itemCommonCollectionList);
        contactSortAdapter.bindToRecyclerView(ryContact);
        initRecycleView(ryContact, swipeRefreshLayout, contactSortAdapter);

        contactSortAdapter.setOnItemChildClickListener(new BaseRecyclerAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseRecyclerAdapter adapter, View view, int position) {

                switch (view.getId()) {
                    case R.id.id_contact_collect_imag:
                        itemCommonCollectionList.get(position).setCollect(!itemCommonCollectionList.get(position).isCollect());
                        if (contactSortAdapter != null) {
                            contactSortAdapter.notifyDataSetChanged();
                        }
                        boolean success;
                        if (itemCommonCollectionList.get(position).isCollect()) {
                            //需要更新常用联系人列表数据
                            success = ContactLocalCacheUtils.addContractToLocalCacheWithUserId(getContext(), itemCommonCollectionList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                        } else {
                            success = ContactLocalCacheUtils.removeContractToLocalCacheWithUserId(getContext(), itemCommonCollectionList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                        }
                        if (success) {
                            EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_UPDATE_LOCAL_CACHE_CONTACT, ""));
                        }
                        break;
                    case R.id.id_contain_view:
                        if (ButtonUtils.isFastDoubleClick()) {
                            return;
                        }
                        startAnotherActivity(getActivity(), isTransferNormalType, itemCommonCollectionList.get(position).getNumber(), itemCommonCollectionList.get(position).getUserName(), null, itemCommonCollectionList.get(position).isEmail());
                        break;
                    default:
                        break;
                }
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = contactSortAdapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    if (getLayoutManager() != null) {
                        getLayoutManager().scrollToPositionWithOffset(position, 0);
                    }
                }

            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        int ryLineSpace = AndroidUtils.dip2px(mContext, 24f);
        myItemDecoration = MyItemDecoration.createVertical(getContext().getColor(R.color.app_white), ryLineSpace);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            checkPermission(mActivity);
        }
    }

    protected void checkPermission(final Activity mActivity) {
        if (mActivity == null) {
            return;
        }
        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                //拒绝并勾选不再询问
                PermissionInstance.getInstance().setHasFirstRejectContacts(false);
            } else {
                PermissionInstance.getInstance().setHasFirstRejectContacts(true);
            }
        }

        PermissionInstance.getInstance().getPermission(getActivity(), new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                final ArrayList<ContactEntity> contactEntitiesTmp = new ArrayList<>();
                if (ContactUtils.getInstance().getContacts() != null && ContactUtils.getInstance().getContacts().size() > 0) {
                    contactEntitiesTmp.addAll(ContactUtils.getInstance().getContacts());
                    updateContactList(contactEntitiesTmp, true);
                } else {
                    ContactUtils.getInstance().getAllContactsAsyn(getContext(), true, new ContactUtils.OnReadContactsSuccessCallBack() {
                        @Override
                        public void OnReadContactsSuccess(ArrayList<ContactEntity> contactEntities) {
                            contactEntitiesTmp.addAll(contactEntities);
                            updateContactList(contactEntitiesTmp, true);
                        }
                    });
                }
            }

            @Override
            public void rejectPermission() {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                    if (!PermissionInstance.getInstance().isHasFirstRejectContacts()) {
                        showLackOfPermissionDialog();
                    } else {
                        PermissionInstance.getInstance().setHasFirstRejectContacts(false);
                    }
                }
                ArrayList<ContactEntity> itemCommonCollectionList = new ArrayList<>();
                updateContactList(itemCommonCollectionList, false);
            }
        }, Manifest.permission.READ_CONTACTS);
    }

    public void showLackOfPermissionDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.STF2101_1_1));
        builder.setMessage(getString(R.string.STF2101_1_2));
        builder.setPositiveButton(getString(R.string.M10_1_25), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mActivity);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        CustomDialog mDealDialog = builder.create();
        mDealDialog.setCancelable(false);
        mDealDialog.show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_UPDATE_ADDRESS_COLLECTION_STATUS) {
            updateContactList(ContactUtils.getInstance().getAllContacts(getContext(), false));
        }
    }

    @Override
    public void updateItemList() {
        if (swipeRefreshLayout == null) {
            return;
        }
        //更新通讯录数据 只有手动刷新的操作才会重新激发从本地通讯录读取通讯录
        if (isGranted(Manifest.permission.READ_CONTACTS)) {
            //单独线程来处理 防止阻塞 华为手机上5000条通讯录会有明显卡顿
            new Thread(new Runnable() {

                @Override
                public void run() {
                    final ArrayList<ContactEntity> contactEntities = ContactUtils.getInstance().getAllContacts(getContext(), true);
                    if (getActivity() != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateContactList(contactEntities);
                            }
                        });
                    }
                }
            }).run();

        }
        swipeRefreshLayout.setRefreshing(false);
    }
}
