package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;

;

/**
 * 发送验证码
 */
public class TransferCrossFirstSendOtpContract {

    public interface View extends IView {

        void transferFirstSenOtpSuccess(ContentEntity response, boolean isTryAgain);

        void transferFirstSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void transferFirstVerifyOtpSuccess(ContentEntity response);

        void transferFirstVerifyOtpError(String errorCode, String errorMsg);

        void getTransferConfrimSuccess(ContentEntity response);

        void getTransferConfrimError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<TransferCrossFirstSendOtpContract.View> {

        void transferFirstSenOtp(String txnId, String action, boolean isTryAgain);

        void transferFirstVerifyOtp(String txnId, String action, String verifyCode);

        void transferConfirm(TransferConfirmReq transferConfirmReq);

        void getTransferConfrim(String txId);
    }

}
