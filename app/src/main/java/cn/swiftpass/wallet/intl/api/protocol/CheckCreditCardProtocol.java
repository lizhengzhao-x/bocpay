package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;

public class CheckCreditCardProtocol extends BaseProtocol {


    public CheckCreditCardProtocol(NetWorkCallbackListener<RegSucBySmartEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/register/judgeHaveVCCard";

    }

    @Override
    public void packData() {
        super.packData();

    }
}
