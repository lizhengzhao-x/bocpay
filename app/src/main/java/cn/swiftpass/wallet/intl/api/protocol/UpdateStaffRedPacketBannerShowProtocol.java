package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class UpdateStaffRedPacketBannerShowProtocol extends BaseProtocol {

    public UpdateStaffRedPacketBannerShowProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/sysconfig/updateStaffRedPacketBannerShow";
    }
}
