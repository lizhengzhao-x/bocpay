package cn.swiftpass.wallet.intl.module.ecoupon.entity;

public interface EcouponItemType {
    String TYPE_EVOUCHER = "EVOUCHER";//电子券
    String TYPE_UPLAN = "UPLAN";//优计划
    String TYPE_WEBVIEW = "WEBVIEW";//WEBVIEW 页面
    String TYPE_STATIC = "STATIC";//静态展示 页面 不支持点击
}
