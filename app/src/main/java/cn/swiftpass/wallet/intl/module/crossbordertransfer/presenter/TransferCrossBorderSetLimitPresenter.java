package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferBaseDataProtocol;
import cn.swiftpass.wallet.intl.api.protocol.SetTransferCrossBorderLimitProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderSetLimitContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;


public class TransferCrossBorderSetLimitPresenter extends BasePresenter<TransferCrossBorderSetLimitContract.View> implements TransferCrossBorderSetLimitContract.Presenter {


    @Override
    public void setTransferLimit(int mCurrentLimit) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new SetTransferCrossBorderLimitProtocol(mCurrentLimit, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().setTransferLimitError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().setTransferLimitSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void getTransferBaseData() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetTransferBaseDataProtocol(new NetWorkCallbackListener<TransferCrossBorderBaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferBaseDataFail(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderBaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferBaseDataSuccess(response);
                }
            }
        }).execute();
    }
}
