package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.RedeemableGiftListProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.api.CheckCCardProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;

/**
 * 换领电子券P
 */
public class RedemptionPresenter extends BasePresenter<RedemPtionContract.View> implements RedemPtionContract.Presenter {


    /**
     * 验证电子券库存
     */
    @Override
    public void redeemGiftCheckStock(String giftTp, List<RedeemResponseEntity.ResultRedmBean> redmBeansIn) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckCCardProtocol(giftTp, redmBeansIn, new NetWorkCallbackListener<CheckCcEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkCCardError("", errorMsg);
                }
            }

            @Override
            public void onSuccess(CheckCcEntity response) {
                if (getView() != null) {
                    getView().checkCCardSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getRedeemGiftList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new RedeemableGiftListProtocol(new NetWorkCallbackListener<RedeemableGiftListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().showRedeemGiftListInfo("", errorMsg, null);
                }
            }

            @Override
            public void onSuccess(RedeemableGiftListEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    //默认 筛选全部选中
                    if (response != null && response.getBuMaps() != null) {
                        for (int i = 0; i < response.getBuMaps().size(); i++) {
                            response.getBuMaps().get(i).setSel(true);
                        }
                    }
                    getView().showRedeemGiftListInfo("", "", response);
                }

            }
        }).execute();
    }


}
