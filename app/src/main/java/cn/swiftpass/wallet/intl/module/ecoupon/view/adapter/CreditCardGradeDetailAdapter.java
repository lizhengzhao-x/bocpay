package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.GradeEntity;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ViewHolderUtils;
import cn.swiftpass.wallet.intl.widget.AnimatedExpandableListView;

public class CreditCardGradeDetailAdapter extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
    private List<GradeEntity> inviteRecordsBeans = new ArrayList<>();
    private Context mContext;

    public CreditCardGradeDetailAdapter(Context mContextIn) {
        this.mContext = mContextIn;
    }


    public void setData(List<GradeEntity> inviteRecordsBeans) {
        this.inviteRecordsBeans = inviteRecordsBeans;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.include_grade_item, null);
        }
        SmaGpInfoBean inviteRecordsBean = inviteRecordsBeans.get(groupPosition).child.get(childPosition);
        TextView cardTitleTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_card_title);
        TextView cardGradeTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_card_grade);
        TextView cardNumberTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_card_number);
        TextView cardExpireDataTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_card_expiredata);
        ImageView leftSelImageIv = (ImageView) ViewHolderUtils.get(convertView, R.id.id_left_sel_image);
        leftSelImageIv.setVisibility(View.GONE);
        cardTitleTv.setText(inviteRecordsBean.getName());
        cardExpireDataTv.setVisibility(View.VISIBLE);
        cardGradeTv.setText("");
        if (!TextUtils.isEmpty(inviteRecordsBean.getBal())) {
            try {
                double point = Double.parseDouble(inviteRecordsBean.getBal());
                if (point>0){
                    cardGradeTv.setText(AndroidUtils.formatPrice(point, false));
                    if (TextUtils.isEmpty(inviteRecordsBean.getExpiryDate())){
                        cardExpireDataTv.setVisibility(View.GONE);
                    }else {
                        cardExpireDataTv.setVisibility(View.VISIBLE);
                        cardExpireDataTv.setText(mContext.getString(R.string.EC04_1) + "       " + inviteRecordsBean.getExpiryDate());
                    }
                }else {
                    cardGradeTv.setText("0");
                    cardExpireDataTv.setVisibility(View.GONE);
                }
            } catch (NumberFormatException e) {
                cardGradeTv.setText("0");
                cardExpireDataTv.setVisibility(View.GONE);
            }
        }else {
            cardGradeTv.setText("");
            cardExpireDataTv.setVisibility(View.GONE);
        }
        cardNumberTv.setText(inviteRecordsBean.getPan());


        return convertView;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        return inviteRecordsBeans.get(groupPosition).getChild().size();//默认只有一个
    }

    @Override
    public int getGroupCount() {
        return inviteRecordsBeans == null ? 0 : inviteRecordsBeans.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return inviteRecordsBeans.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return inviteRecordsBeans.get(groupPosition).getChild().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(mContext).inflate(R.layout.header_creditcard_grade, null);
        TextView titleTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_grade_title);
        TextView creditTotalGradeTv = (TextView) ViewHolderUtils.get(convertView, R.id.id_credit_total_grade);
        ImageView arrowRightIv = (ImageView) ViewHolderUtils.get(convertView, R.id.id_arrow_right);
        arrowRightIv.setImageResource(isExpanded ? R.mipmap.icon_text_select_up : R.mipmap.icon_text_select_right);

        try {
            double  price = Double.parseDouble(inviteRecordsBeans.get(groupPosition).getGradePoint());
            creditTotalGradeTv.setText(AndroidUtils.formatPrice(price, false));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        titleTv.setText(inviteRecordsBeans.get(groupPosition).getName());
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
