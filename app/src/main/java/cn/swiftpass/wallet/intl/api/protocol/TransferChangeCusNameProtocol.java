package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 修改转账昵称
 */
public class TransferChangeCusNameProtocol extends BaseProtocol {
    String mTransferOrderId;
    String mCrCustNameRemark;
    String type;

    public TransferChangeCusNameProtocol(String transferOrderId, String crCustNameRemark, String typeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferChangeCustName";
        mTransferOrderId = transferOrderId;
        mCrCustNameRemark = crCustNameRemark;
        type = typeIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TRANSFERORDERID, mTransferOrderId);
        mBodyParams.put(RequestParams.CRCUSTNAMEREMARK, mCrCustNameRemark);
        mBodyParams.put(RequestParams.TRANSTYPE, type);
    }
}
