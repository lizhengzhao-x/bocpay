package cn.swiftpass.wallet.intl.module.register;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.adapter.ConfirmBindeNewCardAdapter;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


public class ConfirmBindNewCardActivity extends BaseCompatActivity {


    public static final String DATA_SELECT_CARD_LIST = "DATA_SELECT_CARD_LIST";
    public static final String DATA_OTP_MOBILE = "DATA_OTP_MOBILE";
    @BindView(R.id.ry_confirm_bind_new_card)
    RecyclerView ryConfirmBindNewCard;
    @BindView(R.id.iv_check_bind_new_card_rule)
    ImageView ivCheckBindNewCardRule;
    @BindView(R.id.tv_tip_bind_new_card_rule)
    TextView tvTipBindNewCardRule;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    private ArrayList<BindNewCardEntity> selectCardList;
    //是否选中条款
    private boolean selectRule;
    private int flowType;

    @Override
    public void init() {
        setToolBarTitle(R.string.KBCC2105_2_1);
        if (getIntent() != null) {
            flowType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
            Serializable serializableExtra = getIntent().getSerializableExtra(DATA_SELECT_CARD_LIST);
            if (serializableExtra != null) {
                selectCardList = (ArrayList<BindNewCardEntity>) serializableExtra;
            }
        }
        tvConfirm.setEnabled(false);
        ryConfirmBindNewCard.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        ConfirmBindeNewCardAdapter adapter = new ConfirmBindeNewCardAdapter(this, selectCardList);

        ryConfirmBindNewCard.setAdapter(adapter);

        // 服务条款 更换
        BasicUtils.initSpannableStrWithTv(getString(R.string.KBCC2105_3_4), "##", tvTipBindNewCardRule, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitySkipUtil.startAnotherActivity(ConfirmBindNewCardActivity.this, ServerAgreementActivity.class);
            }
        });


    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_confirm_bind_new_card;
    }


    @OnClick({R.id.iv_check_bind_new_card_rule, R.id.tv_confirm})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        switch (view.getId()) {
            case R.id.iv_check_bind_new_card_rule:
                toggleSelectRule();
                updateOkBackground(tvConfirm, selectRule);
                break;
            case R.id.tv_confirm:
                HashMap<String, Object> params = new HashMap<>();
                params.put(ConfirmBindNewCardActivity.DATA_SELECT_CARD_LIST, selectCardList);
                params.put(ConfirmBindNewCardActivity.DATA_OTP_MOBILE, getIntent().getStringExtra(DATA_OTP_MOBILE));
                params.put(Constants.CURRENT_PAGE_FLOW, flowType);
                ActivitySkipUtil.startAnotherActivity(this, BindNewCardOtpActivity.class, params, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }

    private void toggleSelectRule() {
        selectRule = !selectRule;
        ivCheckBindNewCardRule.setImageResource(selectRule ? R.mipmap.list_icon_choose_circle_chosen : R.mipmap.list_icon_choose_circle_default);
    }
}
