package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;

;

public class CrossTransferFirstSendOtpContract {

    public interface View extends IView {

        void transferFirstSenOtpSuccess(ContentEntity response, boolean isTryAgain);

        void transferFirstSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void transferFirstVerifyOtpSuccess(ContentEntity response);

        void transferFirstVerifyOtpError(String errorCode, String errorMsg);

        void transferConfrimSuccess(ContentEntity response);

        void transferConfirmError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<CrossTransferFirstSendOtpContract.View> {

        void transferFirstSenOtp(String txnId, boolean isTryAgain);

        void transferFirstVerifyOtp(String txnId, String verifyCode);

        void getTransferConfrim(String txId);

    }


}
