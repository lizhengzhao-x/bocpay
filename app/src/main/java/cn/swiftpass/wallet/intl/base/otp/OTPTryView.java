package cn.swiftpass.wallet.intl.base.otp;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;

;


/**
 * @name cn.swiftpass.bocbill.model.base.otp
 * @class name：OTPTryView
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/12 14:16
 * @change
 * @chang time
 * @class describe
 */
public interface OTPTryView<V extends IPresenter> extends IView {
    void tryOTPFailed(String errorCode, String errorMsg);

    void tryOTPSuccess(ContentEntity response);

}
