package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jay.widget.StickyHeaders;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;

public class RedPacketHistoryAdapter extends RecyclerView.Adapter implements StickyHeaders {

    private static final int TYPE_PALEYSHI_LISHI_HEADER = 100;
    private static final int TYPE_PALEYSHI_LISHI_TITLE = 101;
    private static final int TYPE_PALEYSHI_LISHI_ITEM = 102;
    private static final int TYPE_GET_LISHI_HISTORY_EMPTY = 103;

    private Activity activity;
    private List<RedPacketBeanEntity> data = new ArrayList<>();
    private PaleyShiHomeEntity paleyShiHomeEntity;


    public RedPacketHistoryAdapter(Activity activity) {
        this.activity = activity;
    }

    public void setDataList(List<RedPacketBeanEntity> redPacketBeanEntities) {
        this.data = redPacketBeanEntities;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_PALEYSHI_LISHI_HEADER) {
            View titleHolder = LayoutInflater.from(activity).inflate(R.layout.item_paleyshi_header, parent, false);
            return new PaleyShiHeaderHolder(activity, titleHolder);
        }
        if (viewType == TYPE_PALEYSHI_LISHI_TITLE) {
            View titleHolder = LayoutInflater.from(activity).inflate(R.layout.item_title_paleyshi_history, parent, false);
            return new PaleyShiTitleHolder(activity, titleHolder);
        }
        if (viewType == TYPE_PALEYSHI_LISHI_ITEM) {
            View itemHolder = LayoutInflater.from(activity).inflate(R.layout.item_paleyshi_history, parent, false);
            return new PaleyShiHistoryItemHolder(activity, itemHolder);
        }
        if (viewType == TYPE_GET_LISHI_HISTORY_EMPTY) {
            View itemHolder = LayoutInflater.from(activity).inflate(R.layout.view_empty, parent, false);
            return new LishiHistoryEmptyHolder(activity, itemHolder);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PaleyShiHeaderHolder) {
            PaleyShiHeaderHolder lishiHistoryTitleHolder = (PaleyShiHeaderHolder) holder;
            lishiHistoryTitleHolder.setData(paleyShiHomeEntity);
        }
        if (holder instanceof PaleyShiTitleHolder) {
            PaleyShiTitleHolder lishiHistoryTitleHolder = (PaleyShiTitleHolder) holder;
            lishiHistoryTitleHolder.setData(paleyShiHomeEntity);
        }
        if (holder instanceof PaleyShiHistoryItemHolder) {
            PaleyShiHistoryItemHolder lishiHistoryItemHolder = (PaleyShiHistoryItemHolder) holder;
            lishiHistoryItemHolder.setData(data.get(position - 2));
        }
    }

    @Override
    public int getItemCount() {
        return 2 + (data != null && data.size() > 0 ? data.size() : 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_PALEYSHI_LISHI_HEADER;
        } else if (position == 1) {
            return TYPE_PALEYSHI_LISHI_TITLE;
        } else {
            if (data != null && data.size() > 0) {
                return TYPE_PALEYSHI_LISHI_ITEM;
            } else {
                return TYPE_GET_LISHI_HISTORY_EMPTY;
            }
        }

    }

    @Override
    public boolean isStickyHeader(int position) {
        return position == 1;
    }

    public void setPaleyShiHome(PaleyShiHomeEntity paleyShiHomeEntity) {
        this.paleyShiHomeEntity = paleyShiHomeEntity;
    }
}
