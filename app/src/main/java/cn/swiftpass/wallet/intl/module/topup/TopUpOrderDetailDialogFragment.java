package cn.swiftpass.wallet.intl.module.topup;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TopUpOrderDetailEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


/**
 * 增值--收银台
 */
public class TopUpOrderDetailDialogFragment extends DialogFragment {


    //增值收银台展示对象
    private TopUpOrderDetailEntity topUpOrderDetailEntity;


    private TopUpOrderDetailDialogFragment.OnDialogClickListener mOnPopClickListener;


    private TextView tvOrderOk;
    private TextView tvPaidvia;

    private TextView tvPaidMoney;
    private TextView tvPaySymbol;
    private ImageView ivClose;


    private void bindViews(View view) {
        ivClose = view.findViewById(R.id.iv_close_dialog);
        tvPaySymbol = view.findViewById(R.id.tv_pay_symbol);
        tvPaidMoney = view.findViewById(R.id.tv_paid_money);
        tvPaidvia = view.findViewById(R.id.tv_paidvia);
        tvOrderOk = view.findViewById(R.id.tv_order_ok);
        addListener();
        if (getDialog() != null) {
            getDialog().setCanceledOnTouchOutside(false);
            getDialog().setCancelable(false);
            getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface anInterface, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    private void addListener() {
        tvOrderOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick(view.getId())) {
                    mOnPopClickListener.onConfirmListener();
                }
            }
        });


        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onDisMissListener();
                dismiss();
            }
        });

    }




    /**
     * 增值 拉起收银台
     *
     * @param topUpOrderDetailEntity      对象不同
     * @param onPopWindowOkClickListener
     */
    public void initParams(TopUpOrderDetailEntity topUpOrderDetailEntity, TopUpOrderDetailDialogFragment.OnDialogClickListener onPopWindowOkClickListener) {
        this.topUpOrderDetailEntity = topUpOrderDetailEntity;
        this.mOnPopClickListener = onPopWindowOkClickListener;
    }



    /**
     * 被扫收银台更新
     */
    private void updateUI() {
        if (topUpOrderDetailEntity!=null){
            tvPaySymbol.setText(topUpOrderDetailEntity.getCurrency());
            try {
                String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(topUpOrderDetailEntity.getMoney()), true);
                tvPaidMoney.setText(price);
            } catch (NumberFormatException e) {
                tvPaidMoney.setText(BigDecimalFormatUtils.forMatWithDigs(topUpOrderDetailEntity.getMoney(), 2));
            }
            tvPaidvia.setText(topUpOrderDetailEntity.getAccType() + "(" + AndroidUtils.subPaymentMethod(topUpOrderDetailEntity.getPaymentMethod()) + ")");
        }


    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fmg_topup_order_detail, container, false);
        bindViews(view);
        updateUI();
        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }


    public interface OnDialogClickListener {
        void onConfirmListener();

        void onDisMissListener();
    }
}
