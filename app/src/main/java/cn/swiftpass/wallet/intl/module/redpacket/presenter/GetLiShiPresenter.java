package cn.swiftpass.wallet.intl.module.redpacket.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.ReceivedRedPacketHomePageProtocol;
import cn.swiftpass.wallet.intl.module.redpacket.contract.GetLiShiContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;

/**
 * 收利是
 */
public class GetLiShiPresenter extends BasePresenter<GetLiShiContract.View> implements GetLiShiContract.Presenter {

    @Override
    public void getGetLiShiHomeData(boolean showDialog) {
        if (getView() != null) {
            if (showDialog) {
                getView().showDialogNotCancel();
            }

        }

        new ReceivedRedPacketHomePageProtocol(new NetWorkCallbackListener<GetLiShiHomeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (showDialog) {
                        getView().dismissDialog();
                    }
                    getView().getGetLiShiHomeDataError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetLiShiHomeEntity response) {
                if (getView() != null) {
                    if (showDialog) {
                        getView().dismissDialog();
                    }
                    getView().getGetLiShiHomeDataSuccess(response);
                }

            }
        }).execute();
    }

    @Override
    public void getGetLiShiHomeData() {
        getGetLiShiHomeData(true);
    }
}
