/*
package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.entity.RegisterIDEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoEntity;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.AreaSelectionEntity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.utils.ViewAnimationUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

*
 * 1.注册/信用卡用户绑定s2界面
 * 2.税务编号的逻辑
 * 1） 总共有三个税务编号可以添加 并且去重处理
 * 2）UI上默认第一个税务编号永远是中国香港 如果再添加 列表中应该之后中国大陆 中国澳门等等
 * <p>
 * 选择地区


public class TaxResidentListActivity extends BaseCompatActivity {

    private static final int REQUEST_CHANGE_NAME = 100;
    private static final int REQUEST_ADD_NAME = 120;
    private TextView mId_resident_title;

    private TextView id_link_left, id_link_right;
    private RelativeLayout mId_sel_country;
    private View id_left_view;
    private TextView mTv_region_name;
    private cn.swiftpass.wallet.intl.widget.EditTextWithDel mTv_tax_number;
    private ArrayList<TaxInfoEntity> mTaxInfos;
    private LinearLayout mContainView, id_add_resident;
    private ScrollView id_root_scrollview;
    private TextView mId_register_next, id_add_tax;

    private int mCurrentType;
    private ArrayList<ImageView> selImageViews;

    public static int CHOICE_AREA_CODE = 1001;
    private TextView mCurrentTextView;
    private EditTextWithDel mEditTextWithDel;
    private S2RegisterInfoEntity registerInfoEntity;
    private List<AreaSelectionEntity> mList = new ArrayList<>();
    private List<String> itemCountrys;
    private String changeOldName;
    private ImageView mIdInfoImg;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        EventBus.getDefault().register(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mCurrentType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        }
        itemCountrys = new ArrayList<>();
        setToolBarTitle(R.string.P3_A1_16_1);
        DataCollectManager.getInstance().sendInputTaxInfo();
        initView();
        initData();
    }

    private void initData() {
        mList.clear();
        mList.add(new AreaSelectionEntity(getString(R.string.VC04_03_5a), false));
        mList.add(new AreaSelectionEntity(getString(R.string.P3_A1_23_b_7), false));
        mList.add(new AreaSelectionEntity(getString(R.string.P3_ewa_01_010), false));
    }

    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.INPUT_REGISTRATION_ADDRESS;
        analysisPageEntity.current_address = PagerConstant.INPUT_REGISTRATION_TAX;
        return analysisPageEntity;
    }

    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);

        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_INPUTRESIDENTNUMBER);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RegisterEventEntity event) {
        if (event.getEventType() == RegisterEventEntity.EVENT_RETRY_EFDIT) {
            finish();
        }
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!ButtonUtils.isFastDoubleClick()) {
                sendRegisterInfo();
            }
        }
    };

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mCurrentType == Constants.PAGE_FLOW_BIND_PA || mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.INPUT_REGISTRATION_TAX);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    private void initView() {
        registerInfoEntity = (S2RegisterInfoEntity) getIntent().getExtras().getSerializable(Constants.OCR_INFO);
        mId_resident_title = (TextView) findViewById(R.id.id_resident_title);
        id_link_left = (TextView) findViewById(R.id.id_link_left);
        id_link_right = (TextView) findViewById(R.id.id_link_right);
        id_add_tax = (TextView) findViewById(R.id.id_add_tax);

        id_root_scrollview = (ScrollView) findViewById(R.id.id_root_scrollview);

        mIdInfoImg = (ImageView) findViewById(R.id.id_info);
        mIdInfoImg.setVisibility(View.VISIBLE);

        selImageViews = new ArrayList<>();


        id_left_view = findViewById(R.id.id_left_view);
        mId_sel_country = (RelativeLayout) findViewById(R.id.id_sel_country);
        id_add_resident = (LinearLayout) findViewById(R.id.id_add_resident);

        id_add_tax.setText(getString(R.string.P3_A1_16_2_1));


        mTv_region_name = (TextView) findViewById(R.id.id_resident_area);
        mTv_tax_number = (cn.swiftpass.wallet.intl.widget.EditTextWithDel) findViewById(R.id.tv_tax_number);
        mTv_tax_number.hideErrorView();
        mTv_tax_number.hideDelView();
        mContainView = (LinearLayout) findViewById(R.id.id_buttom_containView);
        mContainView.setLayoutTransition(ViewAnimationUtils.getLayoutTransition());
        mId_register_next = (TextView) findViewById(R.id.id_register_next);
        mId_register_next.setOnClickListener(onClickListener);
        mTaxInfos = new ArrayList<>();

        //<string name="VC04_03_5a" formatted="false">Hong Kong, China </string>
        //<string name="P3_ewa_01_010" formatted="false">Macao, China</string>
        //<string name="P3_A1_23_b_7" formatted="false">Mainland China</string>

        //地区默认显示中国香港
        itemCountrys.add(getString(R.string.P3_ewa_01_009));
        mTv_region_name.setText(getString(R.string.P3_ewa_01_009));

        addListener();

        mTv_tax_number.getEditText().setEnabled(false);
        mTv_tax_number.getEditText().setFocusable(false);
        mTv_tax_number.getEditText().setText(registerInfoEntity.getCertNo());
        mTv_tax_number.hideDelView();

        String phoneNumber = registerInfoEntity.getPhone().replace("+", "");
        //根据用户注册的手机号 来自动添加税务编号列表 如果是香港 不自动添加 其他地区要自动添加一个栏位
        if (!TextUtils.isEmpty(phoneNumber)) {
            if (phoneNumber.startsWith(Constants.DATA_COUNTRY_CODE_REGISTER[0])) {
                //香港
            } else if (phoneNumber.startsWith(Constants.DATA_COUNTRY_CODE_REGISTER[1])) {
                //澳门
                addTaxView(getString(R.string.P3_ewa_01_010), false);
                itemCountrys.add(getString(R.string.P3_ewa_01_010));
            } else {
                //中国内地
                addTaxView(getString(R.string.P3_A1_23_b_7), false);
                itemCountrys.add(getString(R.string.P3_A1_23_b_7));
            }
        }
        //注册流程如果DJ601 activity 退出
        ProjectApp.getTempTaskStack().add(this);
    }

    private void addListener() {
        mIdInfoImg.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                String lan = SpUtils.getInstance(TaxResidentListActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_US);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.P3_A1_16_3));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        id_link_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ButtonUtils.isFastDoubleClick()) return;
                String lan = SpUtils.getInstance(TaxResidentListActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.STATUS_US);

                }
                mHashMaps.put(Constants.DETAIL_TITLE, id_link_left.getText().toString());
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        id_link_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                String lan = SpUtils.getInstance(TaxResidentListActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.NOTE_US);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, id_link_right.getText().toString());
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        id_add_resident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (itemCountrys.size() == 2) {
                        addTaxView(getRetainArea(), true);
                        itemCountrys.add(getRetainArea());
                    } else {
                        addTaxView("", true);
                    }
                }
            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.hide();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ProjectApp.getTempTaskStack().remove(this);

    }


    private void selImagePosition(int position) {
        for (int i = 0; i < selImageViews.size(); i++) {
            if (position == i) {
                selImageViews.get(i).setImageResource(R.mipmap.icon_check_choose_circle);
            } else {
                selImageViews.get(i).setImageResource(R.mipmap.icon_check_choose_circle_default);
            }
        }
    }


    private void sendRegisterInfo() {
        mTaxInfos.clear();
        RegisterCustomerInfo registerCustomerInfo = (RegisterCustomerInfo) getIntent().getExtras().getSerializable(Constants.REGISTER_CUSTOM_INFO);
        String areaOld = mTv_region_name.getText().toString();
        String taxNumberOld = mTv_tax_number.getText().toString();
        TaxInfoEntity taxInfoEntityOld = new TaxInfoEntity();
        //ocr提交界面传的是 86 852 853 这边需要转换成
        if (areaOld.equals(Constants.DATA_COUNTRY_CODE_REGISTER[0])) {
            taxInfoEntityOld.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[0]);
        } else if (areaOld.equals(Constants.DATA_COUNTRY_CODE_REGISTER[1])) {
            taxInfoEntityOld.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[0]);
        } else {
            taxInfoEntityOld.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[0]);
        }
        taxInfoEntityOld.setTaxNumber(taxNumberOld);
        mTaxInfos.add(taxInfoEntityOld);
        for (int i = 0; i < mContainView.getChildCount(); i++) {
            View itemView = mContainView.getChildAt(i);
            TextView mTv_region_name = (TextView) itemView.findViewById(R.id.id_resident_area);
            EditTextWithDel mTv_tax_number = (cn.swiftpass.wallet.intl.widget.EditTextWithDel) itemView.findViewById(R.id.tv_tax_number);
            String country = mTv_region_name.getText().toString();
            String taxNumber = mTv_tax_number.getText();
            if (TextUtils.isEmpty(country) || TextUtils.isEmpty(taxNumber)) {
                showErrorMsgDialog(TaxResidentListActivity.this, getString(R.string.string_complete_info));
                return;
            }
            TaxInfoEntity taxInfoEntity = new TaxInfoEntity();
            if (!checkIdCard(country, taxNumber, taxInfoEntity)) return;
            taxInfoEntity.setTaxNumber(taxNumber);
            mTaxInfos.add(taxInfoEntity);
        }
        String taxRedentStatus = AndroidUtils.genateJson(mTaxInfos);
        registerCustomerInfo.mTaxResidentStatus = taxRedentStatus;
        registerCustomerInfo.mTaxInfos = mTaxInfos;


        checkIdRequest(registerCustomerInfo);
    }

    private boolean checkIdCard(String country, String taxNumber, TaxInfoEntity taxInfoEntity) {
        if (country.equals(getString(R.string.P3_ewa_01_009))) {
            taxInfoEntity.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[0]);
        } else if (country.equals(getString(R.string.P3_ewa_01_010))) {
            taxInfoEntity.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[1]);
            //澳门税号
            if (taxNumber.length() != 10) {
                showErrorMsgDialog(TaxResidentListActivity.this, getString(R.string.P3_A1_16b_2));
                return false;
            }
            return true;
        } else {
            taxInfoEntity.setResidenceJurisdiction(Constants.DATA_COUNTRY_NAME_CODE[2]);
            if (taxNumber.length() == 18) {
                return true;
            } else {
                showErrorMsgDialog(TaxResidentListActivity.this, getString(R.string.P3_A1_16b_3));
                return false;
            }
        }
        return true;
    }

    private void checkIdRequest(final RegisterCustomerInfo registerCustomerInfo) {
        String type = mCurrentType == Constants.PAGE_FLOW_REGISTER_PA ? RequestParams.REGISTER_STWO_ACCOUNT : RequestParams.BIND_SMART_ACCOUNT;
        ApiProtocolImplManager.getInstance().checkIdvRequest(this, type, new NetWorkCallbackListener<RegisterIDEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);
                showErrorMsgDialog(TaxResidentListActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(RegisterIDEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentType);
                mHashMaps.put(Constants.MAX_LITMIT_INFO, response);
                mHashMaps.put(Constants.CUSTOMER_INFO, registerCustomerInfo);
                mHashMaps.put(Constants.OCR_INFO, getIntent().getExtras().getSerializable(Constants.OCR_INFO));
                ActivitySkipUtil.startAnotherActivity(TaxResidentListActivity.this, SetTransactionLimitActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_taxresident;
    }


*
 * 选择地区


    private void choiceArea(int requestCode) {
        initData();
        for (int j = 0; j < itemCountrys.size(); j++) {
            String originalStr = itemCountrys.get(j);
            for (int i = 0; i < mList.size(); i++) {
                if (originalStr.equals(mList.get(i).getArea())) {
                    mList.remove(i);
                    break;
                }
            }
        }
        if (mList.isEmpty()) return;
        Intent intent = new Intent(TaxResidentListActivity.this, AreaSelectionIDAuthActivity.class);
        intent.putExtra(Constants.CHOICE_AREA, (Serializable) mList);
        startActivityForResult(intent, requestCode);
    }

    private String getRetainArea() {
        initData();
        for (int j = 0; j < itemCountrys.size(); j++) {
            String originalStr = itemCountrys.get(j);
            for (int i = 0; i < mList.size(); i++) {
                if (originalStr.equals(mList.get(i).getArea())) {
                    mList.remove(i);
                    break;
                }
            }
        }
        return mList.get(0).getArea();
    }


    private void addTaxView(String areaTitle, boolean isDelete) {
        final View itemView = LayoutInflater.from(this).inflate(R.layout.include_resident, null);
        final TextView mTv_region_name = (TextView) itemView.findViewById(R.id.id_resident_area);
        final TextView id_resident_title = (TextView) itemView.findViewById(R.id.id_resident_title);
        final ImageView imgRight = (ImageView) itemView.findViewById(R.id.id_right_add);
        ImageView mIdInfoImg = (ImageView) itemView.findViewById(R.id.id_info);
        mIdInfoImg.setVisibility(View.INVISIBLE);
        id_resident_title.setText(getString(R.string.P3_A1_16_3));
        mTv_region_name.setText(areaTitle);
        final EditTextWithDel mTv_tax_number = (cn.swiftpass.wallet.intl.widget.EditTextWithDel) itemView.findViewById(R.id.tv_tax_number);
        mTv_tax_number.hideDelView();
        if (areaTitle.equals(getString(R.string.P3_A1_23_b_7))) {
            mTv_tax_number.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(18)});
        } else if (areaTitle.equals(getString(R.string.P3_ewa_01_010))) {
            mTv_tax_number.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        } else {

        }
        mTv_tax_number.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS);
        View selCountry = itemView.findViewById(R.id.id_sel_country);
        mTv_tax_number.hideErrorView();
        TextView mId_right_delete = (TextView) itemView.findViewById(R.id.id_right_delete);

        itemView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mContainView.addView(itemView, mContainView.getChildCount());
        //用户要求动画
        Animation animation = AnimationUtils.loadAnimation(TaxResidentListActivity.this, R.anim.slide_up);
        itemView.startAnimation(animation);

        if (mContainView.getChildCount() == 2) {
            id_add_resident.setVisibility(View.GONE);
        }
        //默认添加的税务编号 不允许更改国家名称 不允许删除
        if (isDelete) {
            imgRight.setVisibility(View.VISIBLE);
            mId_right_delete.setVisibility(View.VISIBLE);
            mId_right_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String removeitem = mTv_region_name.getText().toString();
                    if (!TextUtils.isEmpty(removeitem)) {
                        for (int i = 0; i < itemCountrys.size(); i++) {
                            if (itemCountrys.get(i).equals(removeitem)) {
                                itemCountrys.remove(i);
                            }
                        }
                    }
                    mContainView.removeView(itemView);
                    if (mContainView.getChildCount() < 2) {
                        id_add_resident.setVisibility(View.VISIBLE);
                    }
                }
            });
            selCountry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!ButtonUtils.isFastDoubleClick()) {
                        mCurrentTextView = mTv_region_name;
                        mEditTextWithDel = mTv_tax_number;
                        changeOldName = mTv_region_name.getText().toString();
                        if (TextUtils.isEmpty(changeOldName)) {
                            choiceArea(REQUEST_ADD_NAME);
                        } else {
                            choiceArea(REQUEST_CHANGE_NAME);
                        }
                    }
                }
            });
        } else {
            imgRight.setVisibility(View.INVISIBLE);
            mId_right_delete.setVisibility(View.INVISIBLE);
        }

    }

    private void updateLengthLimit(String result) {
        mEditTextWithDel.setContentText("");
        if (result.equals(getString(R.string.P3_A1_23_b_7))) {
            mEditTextWithDel.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(18)});
        } else if (result.equals(getString(R.string.P3_ewa_01_010))) {
            mEditTextWithDel.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == CHOICE_AREA_CODE) {
            String result = data.getStringExtra(Constants.CHOICE_AREA);
            if (requestCode == REQUEST_ADD_NAME) {
                //添加name
                if (!TextUtils.isEmpty(result)) {
                    mCurrentTextView.setText(result);
                    itemCountrys.add(result);
                    updateLengthLimit(result);
                }
            } else if (requestCode == REQUEST_CHANGE_NAME) {
                //更换 先删除 再添加
                if (!TextUtils.isEmpty(result)) {
                    mCurrentTextView.setText(result);
                    itemCountrys.remove(changeOldName);
                    itemCountrys.add(result);
                    updateLengthLimit(result);
                }
            }
        }
    }
}
*/
