package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;


public class GetNewMsgDataProtocol extends BaseProtocol {

    private boolean isHomePage;

    @Override
    public boolean isNeedLogin() {
        return false;
    }


    public GetNewMsgDataProtocol(boolean isHomePageIn, NetWorkCallbackListener<NewMsgListEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        this.isHomePage = isHomePageIn;
        mUrl = "api/preLogin/getPromotion";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ISHOMEPAGE, isHomePage?"Y":"N");
    }
}
