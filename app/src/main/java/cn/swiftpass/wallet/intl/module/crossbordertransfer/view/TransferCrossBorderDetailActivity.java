package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderDetailContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.TransferCrossBorderDetailPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.CheckEcouponsDetailPop;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.app.constants.SmartAccountConst.SMART_ACCOUNT_TRANFER_FEE_UPDATE;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst.DATA_TRANSFER_ORDER_ID;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.CrossTransferFirstSendOtpActivity.EXTRA_MOBILE;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.CrossTransferFirstSendOtpActivity.EXTRA_TNXID;

;

public class TransferCrossBorderDetailActivity extends BaseCompatActivity<TransferCrossBorderDetailContract.Presenter> implements TransferCrossBorderDetailContract.View {


    @BindView(R.id.tv_payee_name)
    TextView tvPayeeName;
    @BindView(R.id.tv_payee_card_id)
    TextView tvPayeeCardId;
    @BindView(R.id.tv_payee_bank)
    TextView tvPayeeBank;
    @BindView(R.id.tv_extract_amount)
    TextView tvExtractAamount;
    @BindView(R.id.tv_payee_collect)
    TextView tvPayeeCollect;
    @BindView(R.id.tv_rate_rmb)
    TextView tvRateRmb;
    @BindView(R.id.tv_rate_hkd)
    TextView tvRateHkd;
    @BindView(R.id.tv_rate_update_time)
    TextView tvRateUpdateTime;
    @BindView(R.id.tv_commission)
    TextView tvCommission;
    @BindView(R.id.tv_total_payment)
    TextView tvTotalPayment;
    @BindView(R.id.tv_purpose)
    TextView tvPurpose;
    @BindView(R.id.tv_payment_account)
    TextView tvPaymentAccount;
    @BindView(R.id.tv_customer_info)
    TextView tvCustomerInfo;
    @BindView(R.id.tv_submit)
    TextView tvSubmit;
    @BindView(R.id.tv_terms_info)
    TextView tvTermsInfo;
    @BindView(R.id.tv_fee_info)
    TextView tvFeeInfo;
    @BindView(R.id.tv_exceed_amount)
    TextView tvExceedAmount;

    @BindView(R.id.id_tv_denghao)
    TextView tvDengHao;

    private TransferCrossBorderDetailEntity detailEntity;

    @Override
    public void init() {
        setToolBarTitle(R.string.CT1_4_1);

        tvSubmit.setEnabled(false);
        String totalStr = getResources().getString(R.string.CT1_3a_23a);
        BasicUtils.initSpannableStrWithTvByColor(this,totalStr, "##", tvTermsInfo,R.color.color_green, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                if (TextUtils.isEmpty(detailEntity.tncUrl)) return;
                HashMap<String, String> commissionUrlParam = new HashMap<>();
                commissionUrlParam.put(Constants.DETAIL_URL, detailEntity.tncUrl);
                commissionUrlParam.put(Constants.DETAIL_TITLE, getString(R.string.CT1_3a_23b));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, commissionUrlParam, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        tvCustomerInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvCustomerInfo.getPaint().setAntiAlias(true);

        tvFeeInfo.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        tvFeeInfo.getPaint().setAntiAlias(true);

        if (getIntent() != null) {
            String txId = getIntent().getStringExtra(DATA_TRANSFER_ORDER_ID);
            mPresenter.getTransferDetail(txId);
        }
    }

    private void bindView() {
        if (detailEntity != null) {
            tvDengHao.setVisibility(View.VISIBLE);
            tvSubmit.setEnabled(true);
            tvPayeeName.setText(detailEntity.payeeName);
            tvPayeeCardId.setText(detailEntity.payeeCardId);

            tvPayeeBank.setText(detailEntity.payeeBank);


            tvExtractAamount.setText(getString(R.string.CT1_3a_3) + " " +AndroidUtils.formatPriceWithPoint(Double.parseDouble(detailEntity.amountHKD),true) );
            tvPayeeCollect.setText(getString(R.string.CT1_3a_6) + " " + AndroidUtils.formatPriceWithPoint(Double.parseDouble(detailEntity.amountCNY),true));


            tvCommission.setText(getString(R.string.CT1_3a_3) + " " + AndroidUtils.formatPriceWithPoint(Double.parseDouble(detailEntity.fee),true) );
            tvTotalPayment.setText(getString(R.string.CT2_1_9a) + " " + AndroidUtils.formatPriceWithPoint(Double.parseDouble(detailEntity.totalAmt),true));
            tvPurpose.setText(detailEntity.forUsed);

            String sb = ("1".equals(detailEntity.receiverCardType) ? getString(R.string.LYP01_06_6) : getString(R.string.LYP01_06_4)) + "(" + detailEntity.panFour + ")";
            tvPaymentAccount.setText(sb);
         /*   String lan = SpUtils.getInstance(this).getAppLanguage();
            if (AndroidUtils.isZHLanguage(lan)) {

                tvRateRmb.setText("1"+getString(R.string.CT1_3a_6) );
            } else if (AndroidUtils.isHKLanguage(lan)) {
                tvRateRmb.setText("1"+getString(R.string.CT1_3a_6) );
            } else {
                tvRateRmb.setText(getString(R.string.CT1_3a_6) + " 1");
            }*/
            tvRateRmb.setText("RMB 1");//log 815 要求的
            tvRateHkd.setText("HKD " + detailEntity.CNY2HKDRate);
          /*  if (AndroidUtils.isZHLanguage(lan)) {
                tvRateHkd.setText(detailEntity.CNY2HKDRate +" "+ getString(R.string.CT1_3a_3));
            } else if (AndroidUtils.isHKLanguage(lan)) {
                tvRateHkd.setText(detailEntity.CNY2HKDRate +" "+ getString(R.string.CT1_3a_3));
            } else {
                tvRateHkd.setText(getString(R.string.CT1_3a_3) +" "+ detailEntity.CNY2HKDRate );
            }*/
            tvRateUpdateTime.setText(getString(R.string.CT1_3a_8) + " " + detailEntity.lastUpdated);

            // tvExceedAmount 判断是否显示
            if ("1".equals(detailEntity.hasOverDayLimit)) {
                tvExceedAmount.setVisibility(View.VISIBLE);
                tvSubmit.setEnabled(false);
            } else {
                tvExceedAmount.setVisibility(View.GONE);
            }


        } else {
            tvExceedAmount.setVisibility(View.GONE);
            tvSubmit.setEnabled(false);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_cross_border_detail;
    }

    @Override
    protected TransferCrossBorderDetailContract.Presenter createPresenter() {
        return new TransferCrossBorderDetailPresenter();
    }

    /**
     * 密码弹出框
     */
    public void inputPswPopWindow() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (detailEntity.needSendOtp()) {
                    HashMap<String, String> commissionUrlParam = new HashMap<>();
                    commissionUrlParam.put(EXTRA_TNXID, detailEntity.txnId);
                    commissionUrlParam.put(EXTRA_MOBILE, detailEntity.account);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), CrossTransferFirstSendOtpActivity.class, commissionUrlParam, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                } else {
                    mPresenter.getTransferConfrim(detailEntity.txnId);
                }

            }

            @Override
            public void onVerifyFailed(final String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {

                    }
                });
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

    }

    @OnClick({R.id.tv_customer_info, R.id.tv_submit, R.id.tv_fee_info})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_customer_info:
                if (!TextUtils.isEmpty(detailEntity.customerNotice)) {
                    showEcouponCountDetail(getString(R.string.CT1_3a_22), detailEntity.customerNotice);
                }
                break;
            case R.id.tv_submit:
                inputPswPopWindow();
                break;
            case R.id.tv_fee_info:
                if (!TextUtils.isEmpty(detailEntity.feeDescription)) {
                    showEcouponCountDetail(getString(R.string.CT1_3a_23), detailEntity.feeDescription);
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void getTransferDetailSuccess(TransferCrossBorderDetailEntity response) {
        if (response != null) {
            detailEntity = response;
            bindView();
        }
    }

    private void showEcouponCountDetail(String titleDetail, String content) {
        if (!TextUtils.isEmpty(content)) {
            content = content + "\n";
        }
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), titleDetail, content);
        checkEcouponsDetailPop.show();
    }

    @Override
    public void getTransferDetailError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_TRANFER_NOT_IN_TIME)) {
            showErrorMsgDialog(this, errorMsg, R.string.RP1_6_7, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(TransferCrossBorderDetailActivity.this, MainHomeActivity.class);
                }
            });
        } else {
            showErrorMsgDialog(TransferCrossBorderDetailActivity.this, errorMsg);
        }
    }

    @Override
    public void getTransferConfrimSuccess(ContentEntity response) {
        HashMap<String, Object> param = new HashMap<>();
        param.put(DATA_TRANSFER_ORDER_ID, detailEntity.txnId);
        ActivitySkipUtil.startAnotherActivity(TransferCrossBorderDetailActivity.this, TransferCrossBorderStatusActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getTransferConfrimError(final String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_TRANFER_NOT_IN_TIME)) {
            showErrorMsgDialog(this, errorMsg, R.string.RP1_6_7, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(TransferCrossBorderDetailActivity.this, MainHomeActivity.class);
                }
            });
        }
//        else if (TextUtils.equals(errorCode, "EWA3301") || TextUtils.equals(errorCode, "EWA3213")) {
//            HashMap<String, Object> param = new HashMap<>();
//            param.put(DATA_TRANSFER_ORDER_ID, detailEntity.txnId);
//            ActivitySkipUtil.startAnotherActivity(TransferCrossBorderDetailActivity.this, TransferCrossBorderStatusActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//        }
        else {
            showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    if (TextUtils.equals(errorCode, SMART_ACCOUNT_TRANFER_FEE_UPDATE)) {
                        finish();
                    }
                }
            });
        }


    }
}
