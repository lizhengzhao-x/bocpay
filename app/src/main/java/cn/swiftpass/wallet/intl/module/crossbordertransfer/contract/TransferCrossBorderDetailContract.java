package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;

;


public class TransferCrossBorderDetailContract {

    public interface View extends IView {

        void getTransferDetailSuccess(TransferCrossBorderDetailEntity response);

        void getTransferDetailError(String errorCode, String errorMsg);

        void getTransferConfrimSuccess(ContentEntity response);

        void getTransferConfrimError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {

        void getTransferDetail(String txId);

        void getTransferConfrim(String txId);


    }

}
