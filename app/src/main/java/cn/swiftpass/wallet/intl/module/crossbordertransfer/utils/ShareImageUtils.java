package cn.swiftpass.wallet.intl.module.crossbordertransfer.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemType;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.OrderRecordShareViewAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.app.ProjectApp.mContext;
import static cn.swiftpass.wallet.intl.entity.Constants.FILENAME;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_FAILED;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_PROCESS;
import static cn.swiftpass.wallet.intl.entity.OrderQueryEntity.CROSSBORDERTRANSFER_SUCCESS;

/**
 * Created by ZhangXinchao on 2019/12/9.
 * 交易记录详情界面 分享组件功能一样 代码封装
 */
public class ShareImageUtils {

    public static void showLackOfPermissionDialog(final Context mContext) {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(mContext.getString(R.string.SH2_4_1a));
        builder.setMessage(mContext.getString(R.string.SH2_4_1b));
//        builder.setPositiveButton(mContext.getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        builder.setNegativeButton(mContext.getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                AndroidUtils.startAppSetting(mContext);
//            }
//        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        builder.setPositiveButton(mContext.getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(mContext);
            }
        });
        builder.setNegativeButton(mContext.getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);


        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    private static void generateToalBitmap(final Activity mActivity, ArrayList<ItemModel> itemModels, boolean isLiShi, String imageUrl, String shareText, GenerateToalBitmapCallBack generateToalBitmapCallBack) {
        View shareView = LayoutInflater.from(mActivity).inflate(R.layout.act_order_share_detail_new, null);
        NestedScrollView shareBodySl=shareView.findViewById(R.id.nsl_body);
        if (isLiShi) {
            shareView.setBackgroundResource(R.mipmap.bg_savepage_stylepink);
            itemModels.get(0).setShowHeader(true);
            itemModels.get(0).setResourceImg(R.mipmap.icon_enveloptop_bocpay_white);
            //解决派利是
            itemModels.add(1, new ItemModel.ItemModelBuilder().itemType(ItemType.SPLITLINE).build());
        }

        RecyclerView mRecycleView = shareView.findViewById(R.id.list);
        mRecycleView.setLayoutManager(new LinearLayoutManager(mActivity));
        OrderRecordShareViewAdapter orderRecordShareViewAdapter = new OrderRecordShareViewAdapter(mActivity);
        mRecycleView.setAdapter(orderRecordShareViewAdapter);

        if (!TextUtils.isEmpty(imageUrl)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.IMAGE).itemTitle(imageUrl).itemContentGone(true).build());
        } else if (!TextUtils.isEmpty(shareText)) {
            itemModels.add(new ItemModel.ItemModelBuilder().itemType(ItemType.BODY).itemTitle(shareText).itemContentGone(true).build());
        }

        orderRecordShareViewAdapter.refresh(itemModels);
        AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), AndroidUtils.getScreenHeight(mActivity));
        Handler mainHandler = new Handler(mActivity.getMainLooper());
        mainHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                shareView.setDrawingCacheEnabled(true);
                shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                shareBodySl.measure(measureSpec, measureSpec);

                //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight, mActivity.getResources().getColor(R.color.white));
                shareView.destroyDrawingCache();
                generateToalBitmapCallBack.generateBitmap(cachebmp);
            }
        }, 500);

    }


    public static void saveImageToGalleyForOrderDetail(final Activity mActivity, ArrayList<ItemModel> itemModels, boolean isLishi, String imageUrl, String shareText) {
        generateToalBitmap(mActivity, itemModels, isLishi, imageUrl, shareText, new GenerateToalBitmapCallBack() {
            @Override
            public void generateBitmap(Bitmap bitmap) {
                AndroidUtils.saveImageToGalleryByScopedStorage(mActivity, bitmap);
                Toast.makeText(mActivity, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface GenerateToalBitmapCallBack {

        void generateBitmap(Bitmap bitmap);
    }

    public static void saveImageToGalley(final Activity mActivity, ShareImageParams shareImageParam) {
        showDialog(mActivity);
        final View shareView = initShareView(mActivity, shareImageParam);
        NestedScrollView shareBodySl = shareView.findViewById(R.id.sl_share_body);
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final ImageView mButtomBannerVivew = shareView.findViewById(R.id.id_buttom_banner_view);

        if (!TextUtils.isEmpty(shareImageParam.buttomShareImageUrl)) {
            mButtomBannerVivew.setVisibility(View.VISIBLE);
            String imgUrl = shareImageParam.buttomShareImageUrl;
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mButtomBannerVivew.getLayoutParams();
            int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(mActivity, 15) * 2;
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            mButtomBannerVivew.setLayoutParams(lp);
            GlideApp.with(mActivity).load(imgUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    mButtomBannerVivew.setImageDrawable(resource);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
                    getImageToplog(mActivity, shareView, shareViewHeight);
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    super.onLoadFailed(errorDrawable);
                    mButtomBannerVivew.setVisibility(View.GONE);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
                    getImageToplog(mActivity, shareView, shareViewHeight);
                }
            });
        } else {
            shareBodySl.measure(measureSpec, measureSpec);

            //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight);
            Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight, mActivity.getResources().getColor(R.color.white));

            getImageToplog(mActivity, shareView, shareViewHeight);
        }
    }


    private static void getImageToplog(final Activity mActivity, final View shareView, int shareViewHeight) {
        GlideApp.with(mActivity).asBitmap().load(getShareImageUrl(mActivity)).skipMemoryCache(true).override(80, 80)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        shareView.setDrawingCacheEnabled(true);
                        shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                        shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                        Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mActivity), shareViewHeight, mActivity.getResources().getColor(R.color.white));
                        AndroidUtils.saveImageToGalleryByScopedStorage(mActivity, cachebmp);
                        shareView.destroyDrawingCache();
                        Toast.makeText(mActivity, R.string.save_image_suc, Toast.LENGTH_SHORT).show();
                        disMissDialog(mActivity);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        disMissDialog(mActivity);
                    }
                });
    }

    private static void showDialog(final Activity mActivity) {

        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
        baseActivity.showDialogNotCancel();
    }

    private static void disMissDialog(final Activity mActivity) {
        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
        baseActivity.dismissDialog();
    }

    public static void shareOrderImageToOther(final Activity mActivity, int positon, ArrayList<ItemModel> itemModels, boolean isLishi, String imageUrl, String shareText) {
        LogUtils.d("分享测试", "====步进4");
        generateToalBitmap(mActivity, itemModels, isLishi, imageUrl, shareText, new GenerateToalBitmapCallBack() {
            @Override
            public void generateBitmap(Bitmap bitmap) {
                LogUtils.d("分享测试", "====步进5");
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mActivity, bitmap,FILENAME);
                if (uri == null) return;
                LogUtils.d("分享测试", "====步进6");
                if (positon == 0) {
                    //whatsapp
                    boolean isSuccess = SdkShareUtil.shareToWhatsApp(mActivity, "", uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(mActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }

                } else if (positon == 1) {
                    //wechat
                    boolean isSuccess = SdkShareUtil.shareWechatFriend(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, baseActivity.getResources().getString(R.string.SH2_3_1));
                    }
                } else if (positon == 2) {
                    //line

                    boolean isSuccess = SdkShareUtil.shareToLine(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }

                } else {
                    //系统组件
                    if (uri != null) {
                        AndroidUtils.shareImage(mActivity, "", uri);
                    }
                }
            }
        });

    }

    public static void shareImageToOther(final Activity mActivity, int positon, ShareImageParams shareImageParams) {
        showDialog(mActivity);
        if (positon == 0) {
            //whatsapp
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToWhatsApp(mActivity, "", uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(mActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });

        } else if (positon == 1) {
            //wechat
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareWechatFriend(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, baseActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });
        } else if (positon == 2) {
            //line
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    boolean isSuccess = SdkShareUtil.shareToLine(mActivity, uri);
                    if (!isSuccess) {
                        BaseAbstractActivity baseActivity = (BaseAbstractActivity) mActivity;
                        baseActivity.showErrorMsgDialog(baseActivity, mActivity.getResources().getString(R.string.SH2_3_1));
                    }
                    disMissDialog(mActivity);
                }
            });

        } else {
            //系统组件
            shareGenerateImage(mActivity, shareImageParams, new OnImageBitmapSuccess() {
                @Override
                public void onImageBitmapCallBack(Uri uri) {
                    if (uri != null) {
                        AndroidUtils.shareImage(mActivity, "", uri);
                    }
                    disMissDialog(mActivity);
                }
            });
        }
    }


    /**
     * 分享图片 生成分享路径
     *
     * @param onImageBitmapSuccess
     */
    private static void shareGenerateImage(final Activity mActivity, ShareImageParams shareImageParams, final OnImageBitmapSuccess onImageBitmapSuccess) {
        final View shareView = initShareView(mActivity, shareImageParams);
        NestedScrollView shareBodySl = shareView.findViewById(R.id.sl_share_body);
        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

        final ImageView mButtomBannerVivew = shareView.findViewById(R.id.id_buttom_banner_view);
        if (!TextUtils.isEmpty(shareImageParams.buttomShareImageUrl)) {
            mButtomBannerVivew.setVisibility(View.VISIBLE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) mButtomBannerVivew.getLayoutParams();
            int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(mActivity, 15) * 2;
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            mButtomBannerVivew.setLayoutParams(lp);
            String imgUrl = shareImageParams.buttomShareImageUrl;
            GlideApp.with(mActivity).load(imgUrl).diskCacheStrategy(DiskCacheStrategy.NONE).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    mButtomBannerVivew.setImageDrawable(resource);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);

                    getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    super.onLoadFailed(errorDrawable);
                    mButtomBannerVivew.setVisibility(View.GONE);
                    shareBodySl.measure(measureSpec, measureSpec);

                    //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
                    int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

                    AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);
                    getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
                }
            });
        } else {
            shareBodySl.measure(measureSpec, measureSpec);

            //判断图片长度是否比屏幕长度长 如果大约屏幕长度 就取图片长度
            int shareViewHeight = AndroidUtils.getScreenHeight(mContext) > shareBodySl.getMeasuredHeight() ? AndroidUtils.getScreenHeight(mContext) : shareBodySl.getMeasuredHeight();

            AndroidUtils.layoutView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight);
            getImageToplogForShare(mActivity, shareView, onImageBitmapSuccess, shareViewHeight);
        }
    }

    private static void getImageToplogForShare(final Activity mContext, final View shareView, final OnImageBitmapSuccess onImageBitmapSuccess, int shareViewHeight) {
        GlideApp.with(mContext).asBitmap().load(getShareImageUrl(mContext)).override(80, 80).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                shareView.setDrawingCacheEnabled(true);
                shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                // 把一个View转换成图片
                Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight, mContext.getResources().getColor(R.color.white));
                shareView.destroyDrawingCache();
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp,FILENAME);
                onImageBitmapSuccess.onImageBitmapCallBack(uri);
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                shareView.setDrawingCacheEnabled(true);
                shareView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                shareView.setDrawingCacheBackgroundColor(Color.WHITE);
                // 把一个View转换成图片
                Bitmap cachebmp = AndroidUtils.loadBitmapFromView(shareView, AndroidUtils.getScreenWidth(mContext), shareViewHeight, mContext.getResources().getColor(R.color.white));
                shareView.destroyDrawingCache();
                Uri uri = AndroidUtils.tempSaveImageToSdCardByScopedStorageByShareImage(mContext, cachebmp,FILENAME);
                onImageBitmapSuccess.onImageBitmapCallBack(uri);
            }
        });

    }


    /**
     * 获取分享的图标 国际化支持
     *
     * @return
     */
    public static String getShareImageUrl(final Context mContext) {
        String lan = SpUtils.getInstance(mContext).getAppLanguage();
        String shareImageUrl = "en_US/";
        if (AndroidUtils.isZHLanguage(lan)) {
            shareImageUrl = "zh_CN/";
        } else if (AndroidUtils.isHKLanguage(lan)) {
            shareImageUrl = "zh_HK/";
        } else {
            shareImageUrl = "en_US/";
        }
        //http://mbasit1.ftcwifi.com/images/en_US/shareImg.png
        String url = AppTestManager.getInstance().getBaseUrl() + "images/" + shareImageUrl + "shareImg.png";
        return url;
    }

    private static View initShareView(Context mContext, ShareImageParams shareImageParam) {
        View shareView = LayoutInflater.from(mContext).inflate(R.layout.item_share_view_by_transfer_cross_border, null);
        TextView tvPayAmount = shareView.findViewById(R.id.tv_pay_amount);
        TextView tvShareView = shareView.findViewById(R.id.id_share_tv);
        ImageView headImage = shareView.findViewById(R.id.id_head_img);
        TextView tvTransferPayee = shareView.findViewById(R.id.tv_transfer_payee);


        TextView tvTransferCardId = shareView.findViewById(R.id.tv_transfer_card_id);
        TextView tvTransferBank = shareView.findViewById(R.id.tv_transfer_bank);
        TextView tvTransferAccount = shareView.findViewById(R.id.tv_transfer_account);
        TextView tvTransferPayer = shareView.findViewById(R.id.tv_transfer_payer);
        TextView tvTransferStatus = shareView.findViewById(R.id.tv_transfer_status);
        TextView tvTransferType = shareView.findViewById(R.id.tv_transfer_type);
        TextView tvTransferTime = shareView.findViewById(R.id.tv_transfer_time);
        TextView tvTransferNo = shareView.findViewById(R.id.tv_transfer_no);
        //这里网络异常拉取图片有可能出问题 暂时用一个展位图
        GlideApp.with(mContext).load(ShareImageUtils.getShareImageUrl(mContext)).placeholder(R.mipmap.shareimg).into(headImage);
        if (shareImageParam != null) {
            if (!TextUtils.isEmpty(shareImageParam.amountCNY)) {
                tvPayAmount.setText(mContext.getString(R.string.CT2_1_8a) + " " + AndroidUtils.formatPriceWithPoint(Double.parseDouble(shareImageParam.amountCNY), true));
            }
            tvTransferPayee.setText(shareImageParam.receiverName);
            tvTransferCardId.setText(shareImageParam.receiverCardNo);
            tvTransferBank.setText(shareImageParam.receiverBankName + " - " + AndroidUtils.getCardType(shareImageParam.receiverCardType, mContext));
            String sb = ("1".equals(shareImageParam.accountType) ? mContext.getString(R.string.LYP01_06_6) : mContext.getString(R.string.LYP01_06_4)) + "(" + shareImageParam.panFour + ")";
            tvTransferAccount.setText(sb);
            tvTransferPayer.setText(shareImageParam.payName);

            if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_SUCCESS)) {
                tvTransferStatus.setText(R.string.CT2_1_5);
            } else if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_FAILED)) {
                //失败时不显示手续费
                tvTransferStatus.setText(R.string.CT2_1_6);
            } else if (TextUtils.equals(shareImageParam.status, CROSSBORDERTRANSFER_PROCESS)) {
                tvTransferStatus.setText(R.string.CT2_1_7);
            } else {
            }

            tvTransferType.setText(shareImageParam.txnOptionStr);
            tvTransferTime.setText(shareImageParam.txTime);
            tvTransferNo.setText(shareImageParam.referenceNo);
            InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
            if (initBannerEntity != null) {
                String shareTxt = initBannerEntity.getCrossBorderShareText(mContext);
                if (!TextUtils.isEmpty(shareTxt)) {
                    tvShareView.setText(shareTxt);
                    tvShareView.setVisibility(View.VISIBLE);
                }
            }
        }

        return shareView;
    }

    /**
     * 交易记录详情 模型转换
     *
     * @param transferCrossBorderStatusEntity
     * @return
     */
    public static ShareImageParams generateShareParams(Context context, TransferCrossBorderStatusEntity transferCrossBorderStatusEntity) {
        ShareImageParams shareImageParams = new ShareImageParams();
        shareImageParams.currencyRMB = transferCrossBorderStatusEntity.currencyRMB;
        shareImageParams.amountCNY = transferCrossBorderStatusEntity.amountCNY;
        shareImageParams.receiverName = transferCrossBorderStatusEntity.receiverName;
        shareImageParams.fee = transferCrossBorderStatusEntity.fee;
        shareImageParams.receiverCardNo = transferCrossBorderStatusEntity.receiverCardNo;
        shareImageParams.receiverBankName = transferCrossBorderStatusEntity.receiverBankName;
        shareImageParams.receiverCardType = transferCrossBorderStatusEntity.receiverCardType;
        shareImageParams.accountType = transferCrossBorderStatusEntity.accountType;
        shareImageParams.panFour = transferCrossBorderStatusEntity.panFour;
        shareImageParams.payName = transferCrossBorderStatusEntity.payName;
        shareImageParams.txTime = transferCrossBorderStatusEntity.txTime;
        shareImageParams.txnId = transferCrossBorderStatusEntity.txnId;
        shareImageParams.referenceNo = transferCrossBorderStatusEntity.referenceNo;
        shareImageParams.txnOptionStr = transferCrossBorderStatusEntity.txnOptionStr;
        shareImageParams.status = transferCrossBorderStatusEntity.status;
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
        if (initBannerEntity != null) {
            String shareTxt = initBannerEntity.getTxnDetailShareText(context);
            if (!TextUtils.isEmpty(shareTxt)) {
                shareImageParams.buttomShareTv = shareTxt;
            }
        }
        if (initBannerEntity != null) {
            BannerImageEntity pTwoPBean = initBannerEntity.getTransferRecord(context);
            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                String imgUrl = pTwoPBean.getImagesUrl();
                shareImageParams.buttomShareImageUrl = imgUrl;
            }
        }

        return shareImageParams;
    }

//    /**
//     * 跨境汇款成功 模型转换
//     *
//     * @param statusEntity
//     * @return
//     */
//    public static ShareImageParams generateSuccessShareParams(Context context, TransferCrossBorderStatusEntity statusEntity) {
//        ShareImageParams shareImageParams = new ShareImageParams();
//        shareImageParams.currencyRMB = statusEntity.currencyRMB;
//        shareImageParams.amountCNY = statusEntity.amountCNY;
//        shareImageParams.receiverName = statusEntity.receiverName;
//        shareImageParams.fee = statusEntity.fee;
//        shareImageParams.receiverCardNo = statusEntity.receiverCardNo;
//        shareImageParams.receiverBankName = statusEntity.receiverBankName;
//        shareImageParams.receiverCardType = statusEntity.receiverCardType;
//        shareImageParams.accountType = statusEntity.accountType;
//        shareImageParams.panFour = statusEntity.panFour;
//        shareImageParams.payName = statusEntity.payName;
//        shareImageParams.txTime = statusEntity.txTime;
//        shareImageParams.txnId = statusEntity.txnId;
//        shareImageParams.txnOptionStr = statusEntity.txnOptionStr;
//        shareImageParams.referenceNo = statusEntity.referenceNo;
//        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
//        if (initBannerEntity != null) {
//            BannerImageEntity pTwoPBean = initBannerEntity.getMoneyTransfer(context);
//            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
//                String imgUrl = pTwoPBean.getImagesUrl();
//                shareImageParams.buttomShareImageUrl = imgUrl;
//            }
//        }
//        return shareImageParams;
//    }

}
