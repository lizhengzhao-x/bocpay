/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Class Name: MpQrUrlInfo<br/>
 * <p>
 * Class Description:
 *
 * @author 86755221
 * @version 1 Date: 2018-1-19
 * 银联emv码
 */

public class MpQrUrlInfoResult implements Serializable {
    private static final long serialVersionUID = -7262396864975741704L;
    //交易ID
    private String tranID;
    //交易金额
    private String tranAmt;
    //交易币种（RMB is represented by “156”）
    private String tranCur;
    //卡司接口返回字段（QRC Use Case）
    private String qRCUseCase;
    //商户名称
    private String name;
    //商户类别码
    private String mCC;
    //国家代码(China is represented by “CN”)
    private String country;
    private String city;
    //邮政编码
    private String postal;
    //商户语言
    private String lang;
    //商户别名
    private String nameAL;
    //城市别名
    private String cityAL;
    //终端ID(卡司接口返回，和deviceId不同)
    private String tID;

    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    private String panFour;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    private String cardId;

    public String getQrcType() {
        return qrcType;
    }

    public void setQrcType(String qrcType) {
        this.qrcType = qrcType;
    }

    //维码类型 E和U。E代表EMV码，U代表url码
    private String qrcType;

    private String cardNumber;


    public String getOriginalAmt() {
        return originalAmt;
    }

    public void setOriginalAmt(String originalAmt) {
        this.originalAmt = originalAmt;
    }

    private String originalAmt;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    private String cardType;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    private String discount;

    /*****************************界面传值使用******************/
    private String qrcodeStr;
    private String pandId;


    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }


    /*****************************界面传值使用******************/
    public String getQrcodeStr() {
        return qrcodeStr;
    }

    public void setQrcodeStr(String qrcodeStr) {
        this.qrcodeStr = qrcodeStr;
    }


    public String getPandId() {
        return pandId;
    }

    public void setPandId(String pandId) {
        this.pandId = pandId;
    }


    public String getTranID() {
        return this.tranID;
    }


    public void setTranID(String tranID) {
        this.tranID = tranID;
    }


    public String getTranAmt() {
        if (TextUtils.equals(tranAmt, "null")) {
            return "";
        }
        return this.tranAmt;
    }


    public void setTranAmt(String tranAmt) {
        this.tranAmt = tranAmt;
    }

    public String getTranCur() {
        return this.tranCur;
    }


    public void setTranCur(String tranCur) {
        this.tranCur = tranCur;
    }


    public String getqRCUseCase() {
        return this.qRCUseCase;
    }


    public void setqRCUseCase(String qRCUseCase) {
        this.qRCUseCase = qRCUseCase;
    }


    public String getName() {
        return this.name;
    }


    public void setName(String name) {
        this.name = name;
    }


    public String getmCC() {
        return this.mCC;
    }


    public void setmCC(String mCC) {
        this.mCC = mCC;
    }

    public String getCountry() {
        return this.country;
    }


    public void setCountry(String country) {
        this.country = country;
    }


    public String getCity() {
        return this.city;
    }


    public void setCity(String city) {
        this.city = city;
    }


    public String getPostal() {
        return this.postal;
    }


    public void setPostal(String postal) {
        this.postal = postal;
    }


    public String getLang() {
        return this.lang;
    }


    public void setLang(String lang) {
        this.lang = lang;
    }


    public String getNameAL() {
        return this.nameAL;
    }


    public void setNameAL(String nameAL) {
        this.nameAL = nameAL;
    }


    public String getCityAL() {
        return this.cityAL;
    }


    public void setCityAL(String cityAL) {
        this.cityAL = cityAL;
    }


    public String gettID() {
        return this.tID;
    }


    public void settID(String tID) {
        this.tID = tID;
    }

}
