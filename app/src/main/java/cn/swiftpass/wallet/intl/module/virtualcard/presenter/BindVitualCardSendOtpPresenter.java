package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.BindVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardVeryOtpEntiy;

/**
 * Created by ZhangXinchao on 2019/11/15.
 */
public class BindVitualCardSendOtpPresenter extends BasePresenter<BindVitualCardSendOtpContract.View> implements BindVitualCardSendOtpContract.Presenter {
    @Override
    public void bindVitualCardSenOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String action, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardSendOtpProtocol(vitualCardSendOtpEntiyIn, action, new NetWorkCallbackListener<VirtualCardSendOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindVitualCardSenOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(VirtualCardSendOtpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindVitualCardSenOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();

    }

    @Override
    public void bindVitualCardVerifyOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String action, String verifyCode, String cardId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardVerifyOtpProtocol(vitualCardSendOtpEntiyIn, action, verifyCode, cardId, new NetWorkCallbackListener<VirtualCardVeryOtpEntiy>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindVitualCardVerifyOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VirtualCardVeryOtpEntiy response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindVitualCardVerifyOtpSuccess(response);
                }
            }
        }).execute();
    }
}
