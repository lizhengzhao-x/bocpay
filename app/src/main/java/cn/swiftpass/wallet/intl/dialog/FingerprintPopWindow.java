package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

public class FingerprintPopWindow extends BasePopWindow implements View.OnClickListener {
    private ImageView mId_back_image;
    private TextView mId_switch_pwd;

    private OnPopWindowOkClickListener mOnPopClickListener;

    public FingerprintPopWindow(Activity mActivity, OnPopWindowOkClickListener onPopWindowOkClickListener) {
        super(mActivity);
        this.mOnPopClickListener = onPopWindowOkClickListener;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_fingerprint);
        this.setAnimationStyle(R.style.dialog_style);
        mId_back_image = mContentView.findViewById(R.id.id_back_image);
        mId_switch_pwd = mContentView.findViewById(R.id.id_switch_pwd);
        setDisMissView(mContentView);

        mId_switch_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnPopClickListener.onPwdSwitchListener();
            }
        });

        mId_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                mOnPopClickListener.onBackIconClickListener();
            }
        });
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnPopWindowOkClickListener {
        void onBackIconClickListener();

        void onPwdSwitchListener();
    }
}
