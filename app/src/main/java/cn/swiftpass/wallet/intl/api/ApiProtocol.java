package cn.swiftpass.wallet.intl.api;

import android.content.Context;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;


public interface ApiProtocol {

    /**
     * 账单列表
     *
     * @param context
     * @param queryType
     * @param pageNo
     * @param pageSize
     * @param panId
     * @param callback
     */
    void getQueryBill(final Context context, boolean show, String queryType, String pageNo, String pageSize, String panId, final NetWorkCallbackListener callback);


    /**
     * 主扫二维码下单
     *
     * @param context
     * @param requestEntity
     * @param callback
     */
    void getMainScanOrder(final Context context, MainScanOrderRequestEntity requestEntity, final NetWorkCallbackListener callback);

    /**
     * 获取活动列表
     *
     * @param name
     * @param callback
     */
    void getActivitiesList(String name, Context context, final NetWorkCallbackListener callback);


    /**
     * 检验支付密码
     *
     * @param payPwd
     * @param dataCallback
     */
    void getCheckPassword(Context context, String payPwd, NetWorkCallbackListener dataCallback);

    /**
     * 注册确认接口
     *
     * @param context
     * @param passCode
     * @param walletId
     * @param cardId
     * @param action
     * @param dataCallback
     */
    void getConfirmRegister(Context context, String passCode,String walletId, String cardId, String action, NetWorkCallbackListener dataCallback);


    /**
     * 获取银行卡列表
     *
     * @param context
     * @param pageNum
     * @param pageSize
     * @param logonUserId
     * @param dataCallback
     */
    void getCardList(Context context, String pageNum, String pageSize, String logonUserId, NetWorkCallbackListener dataCallback);

    void logout();


    /**
     * @param context
     * @param action    D 充值 W提现
     * @param trxAmount
     */
    void topUpEvent(Context context, String action, String trxAmount, String orderNo, NetWorkCallbackListener dataCallback);


    void getSmartAccount(Context context, NetWorkCallbackListener dataCallback);

    void updateSmartAccountInfoSendOtp(Context context, String action, NetWorkCallbackListener dataCallback);

    void verifySmartAccountInfoOtp(Context context, String action, String verifyCode, NetWorkCallbackListener dataCallback);


    void getAccountBalanceInfo(Context context, NetWorkCallbackListener dataCallback);


    void getBankList(Context context, String currency, String name, String code, NetWorkCallbackListener dataCallback);


    void parserTransferCode(Context context, String qrCode, NetWorkCallbackListener dataCallback);


    void transferSummary(Context context, String pgSz, String nxtRecKey, String frTxnDt, String toTxnDt, NetWorkCallbackListener dataCallback);


    void getnTxnHistEnquirySmartCard(Context context, String innexttstmp, NetWorkCallbackListener dataCallback);


    void checkSmartAccountNewBind(Context context, boolean show, final NetWorkCallbackListener callback);


    /**
     * s2用户注册frp之后 信息提交
     *
     * @param context
     * @param callback
     */
    void sTwoRegisterCustomerInfo(Context context, String type, RegisterCustomerInfo registerCustomerInfo, final NetWorkCallbackListener callback);

    /**
     * s2用户注册设置密码
     *
     * @param context
     * @param callback
     */
    void sTwoRegisterSetPwd(Context context, String pwdStr, final NetWorkCallbackListener callback);






    /**
     * 获取交易id接口 注册
     *
     * @param context
     * @param callback
     */
    void getTnxId(Context context, String phoneNumber, final NetWorkCallbackListener callback);


    /**
     * 获取交易id接口 信用卡绑定我的账户
     *
     * @param context
     * @param callback
     */
    void getTnxIdForBindAccount(Context context, String phoneNumber, final NetWorkCallbackListener callback);

    /**
     * 获取交易id接口 找回密码
     *
     * @param context
     * @param callback
     */
    void getTnxIdForForgetPwd(Context context, final NetWorkCallbackListener callback);


    /**
     * 获取账户级别 s2 s3
     *
     * @param context
     * @param mobile
     * @param verifyCode
     * @param action
     * @param callback
     */
    void getSmartLevel(Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback);


    void getSmartLevelForForget(Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback);


    /**
     * 验证信用卡注册验证码
     * @param context
     * @param verifyCode
     * @param action
     * @param callback
     */
    public void checkBankRegisterVerifyCode(final Context context, String verifyCode, String action, final NetWorkCallbackListener callback);

    /**
     * 验证PA用户注册验证码
     * @param context
     * @param verifyCode
     * @param action
     * @param callback
     */
    public void checkPAAccountVerifyCode(final Context context, String mobile, String verifyCode, String action, final NetWorkCallbackListener callback);

    void getSmartLevelForBind(Context context, final NetWorkCallbackListener callback);



    void regEddaInfo(Context context, String bankCode, String bankNo, String currency, final NetWorkCallbackListener callback);


    /**
     * s2用户注册 获取账户类型信息
     *
     * @param context
     * @param callback
     */
    void getRegEddaInfo(Context context, final NetWorkCallbackListener callback);


    /**
     * s2用户增值
     *
     * @param context
     * @param action
     * @param txnAmt
     * @param referenceNo
     * @param callback
     */
    void sTwoTopUp(Context context, String action, String txnAmt, String referenceNo, final NetWorkCallbackListener callback);


    /**
     * 升级我的账户第一步
     *
     * @param context
     * @param accNo
     * @param password
     * @param loginType
     * @param action
     * @param verifyCode
     * @param callback
     */
    void sTwoUpdateAccount(Context context, String accNo, String password, String loginType, String action, String verifyCode, final NetWorkCallbackListener callback);


    /**
     * 升级我的账户第二步
     *
     * @param context
     * @param primaryAc
     * @param payLimit
     * @param topupmethod
     * @param autotopupamt
     * @param callback
     */
    void sTwoUpdateAccountInfo(Context context, String primaryAc, String payLimit, String topupmethod, String autotopupamt, final NetWorkCallbackListener callback);


    void checkIdvRequest(final Context context, String type, final NetWorkCallbackListener callback);


    void sThreeTopUpPrecheck(Context context, String action, String trxAmount, final NetWorkCallbackListener callback);
}
