package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferPreCheckProtocol;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByBankAccounContract;


public class TransferByBankAccountPresenter extends BasePresenter<TransferByBankAccounContract.View> implements TransferByBankAccounContract.Presenter {

    @Override
    public void getSmartAccountInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();
    }

    @Override
    public void transferPreCheck(TransferPreCheckReq req) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferPreCheckProtocol(req, new NetWorkCallbackListener<TransferPreCheckEntity>() {

            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferPreCheckError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(TransferPreCheckEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferPreCheckSuccess(req,response);
                }
            }
        }).execute();
    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }
}
