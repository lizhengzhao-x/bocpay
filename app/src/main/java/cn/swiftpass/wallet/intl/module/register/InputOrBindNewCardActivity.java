package cn.swiftpass.wallet.intl.module.register;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.register.adapter.InputOrBindNewCardAdapter;


public class InputOrBindNewCardActivity extends BaseCompatActivity {

    @BindView(R.id.tab_bind_card_frg)
    TabLayout tabBindCardFrg;
    @BindView(R.id.vp_bind_card_frg)
    ViewPager2 vpBindCardFrg;
    private InputBankCardNumberFragment bankCardNumberFragment;


    @Override
    public void init() {
        setToolBarTitle(R.string.KBCC2105_2_1);

        vpBindCardFrg.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        tabBindCardFrg.setSelectedTabIndicatorColor(getColor(R.color.color_FFBF2F4F));

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new BindNewCardFragment());
        bankCardNumberFragment = new InputBankCardNumberFragment();
        fragments.add(bankCardNumberFragment);

        InputOrBindNewCardAdapter adapter = new InputOrBindNewCardAdapter(getSupportFragmentManager(), this.getLifecycle(), fragments);
        vpBindCardFrg.setOffscreenPageLimit(5);
        vpBindCardFrg.setAdapter(adapter);
        vpBindCardFrg.setCurrentItem(0);
        vpBindCardFrg.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //滑动到第一个界面时，需要隐藏键盘
                if (position==0) {
                    if (bankCardNumberFragment != null) {
                        bankCardNumberFragment.hideKeyboard();
                    }

//                    AndroidUtils.hideKeyBoardInput(InputOrBindNewCardActivity.this);
                }
            }
        });
        new TabLayoutMediator(tabBindCardFrg, vpBindCardFrg, true, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                if (position == 0) {
                    tab.setText(getString(R.string.KBCC2105_8_2));
                } else {
                    tab.setText(getString(R.string.KBCC2105_8_3));
                }
            }
        }).attach();


    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_input_or_bind_new_card;
    }


}
