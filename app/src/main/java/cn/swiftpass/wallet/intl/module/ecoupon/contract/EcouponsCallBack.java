package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivityEntity;

public interface EcouponsCallBack {
    void onItemClick(EcouponActivityEntity entity);
}
