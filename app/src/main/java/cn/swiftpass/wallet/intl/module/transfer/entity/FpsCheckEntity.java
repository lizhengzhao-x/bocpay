package cn.swiftpass.wallet.intl.module.transfer.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/11/8.
 */
public class FpsCheckEntity extends BaseEntity {


    /**
     * fpsSign : 1
     * bocpaySign : 1
     */

    private String fpsSign;
    /**
     * 是否是商户 0: 非商户  1： 商户
     */
    private String isFpsMer;

    public boolean isMerchantType() {
        return TextUtils.equals(isFpsMer, "1");
    }

    /**
     * bocpaySign 返回 0：未开通bocpay  2：没有pa/sa  1:允许bocpay转账
     */
    public String bocpaySign;



    public boolean getFpsSign() {
        if (TextUtils.isEmpty(fpsSign)) return false;
        return fpsSign.equals("1");
    }

    public void setFpsSign(String fpsSign) {
        this.fpsSign = fpsSign;
    }

    public boolean isOpenAccount() {
        if (TextUtils.isEmpty(bocpaySign)) return false;
        return !bocpaySign.equals("2");
    }

    public boolean getBocpaySign() {
        if (TextUtils.isEmpty(bocpaySign)) return false;
        return bocpaySign.equals("1");
    }

    public void setBocpaySign(String bocpaySign) {
        this.bocpaySign = bocpaySign;
    }
}
