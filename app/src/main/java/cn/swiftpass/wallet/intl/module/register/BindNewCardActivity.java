package cn.swiftpass.wallet.intl.module.register;

import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;


public class BindNewCardActivity extends BaseCompatActivity {
    @BindView(R.id.fl_transaction)
    FrameLayout flTransaction;
    private FragmentManager mFragmentManager;
    private BindNewCardFragment mBindNewCardNumberFragment;



    @Override
    public void init() {
        setToolBarTitle(R.string.KBCC2105_2_1);
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        mBindNewCardNumberFragment = new BindNewCardFragment();
        //这个界面现有流程只出现在注册流程中
        mBindNewCardNumberFragment.setFlowType(Constants.PAGE_FLOW_REGISTER);
        fragmentTransaction.add(R.id.fl_transaction, mBindNewCardNumberFragment);
        fragmentTransaction.commitAllowingStateLoss();

    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_bind_new_card;
    }




}
