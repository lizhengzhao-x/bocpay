package cn.swiftpass.wallet.intl.module.home;

import cn.swiftpass.wallet.intl.entity.MenuItemEntity;

public interface OnMenuItemClickListener {

    boolean onMenuItemClick(MenuItemEntity item);


}