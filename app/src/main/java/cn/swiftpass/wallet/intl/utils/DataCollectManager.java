package cn.swiftpass.wallet.intl.utils;

import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;

/**
 * 数据统计
 */
public class DataCollectManager {


    public static final String EVENT_INPUTTELE = "INPUTTELE";
    public static final String EVENT_IDVBEFORE = "IDVBEFORE";
    public static final String IEVENT_OCRCONFIRM = "OCRCONFIRM";
    public static final String EVENT_FRPBEFORE = "FRPBEFORE";
    public static final String EVENT_FRPAFTER = "FRPAFTER";
    public static final String EVENT_INPUTADDR = "INPUTADDR";
    public static final String EVENT_INPUTTAX = "INPUTTAX";
    public static final String EVENT_INFOCONFIRM = "INFOCONFIRM";
    public static final String EVENT_SEND_OTP = "OTPBEFRE";
    public static final String EVENT_SET_TRANSACTIONlIMIT = "SETLIMIT";
    public static final String EVENT_SETPWD = "SETPWD";
    public static final String EVENT_SETFIO = "SETFIO";
    public static final String EVENT_REGDONE = "REGDONE";

//    INPUTTELE //输入手机号码
//            IDVBEFORE //跳转idv前
//    OCRCONFIRM //ocr确认页
//            FRPBEFORE //跳转frp前
//    FRPAFTER //frp回ewa后
//            INPUTADDR //填写地址页
//    INPUTTAX //填写税务资料页
//            INFOCONFIRM //资料确认页
//    SETPWD //设置密码页
//            SETFIO //设置fio页
//    REGDONE //注册完成

    private static class DataCollectManagerHolder {
        private static DataCollectManager instance = new DataCollectManager();
    }

    private final String TAG = DataCollectManager.class.getSimpleName();

    public static DataCollectManager getInstance() {
        return DataCollectManager.DataCollectManagerHolder.instance;
    }


    /**
     * 输入手机号码
     */
    public void sendInputPhoneNumber() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_INPUTTELE);
    }

    /**
     * 跳转idv前
     */
    public void sendEventIdvBefore() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_IDVBEFORE);
    }

    /**
     * ocr确认页
     */
    public void sendOcrConfirm() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(IEVENT_OCRCONFIRM);
    }

    /**
     * 跳转frp前
     */
    public void sendFrpBefore() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_FRPBEFORE);
    }


    /**
     * frp回ewa后
     */
    public void sendFrpBackEwa() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_FRPAFTER);
    }

    /**
     * 填写地址页
     */
    public void sendFillAddress() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_INPUTADDR);
    }

    /**
     * 资料确认页
     */
    public void sendConfirmInformation() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_INFOCONFIRM);
    }


    /**
     * S2用户注册 发送验证码
     */
    public void sendOtp() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_SEND_OTP);
    }

    /**
     * 资料确认页
     */
    public void sendTransactionLimit() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_SET_TRANSACTIONlIMIT);
    }

    /**
     * 设置密码页
     */
    public void sendSetPwd() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_SETPWD);
    }

    /**
     * SETFIO
     */
    public void sendSetFio() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_SETFIO);
    }

    /**
     * 注册完成
     */
    public void sendCompleteRegister() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_REGDONE);
    }

    /**
     * 注册完成
     */
    public void sendInputTaxInfo() {
        ApiProtocolImplManager.getInstance().getDatacollectProtocol(EVENT_INPUTTAX);
    }


}
