package cn.swiftpass.wallet.intl.module.transactionrecords.api;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class QueryTxnDetailProtocol extends BaseProtocol {
    String srcRefNo;
    String orderType;
    String type;

    public QueryTxnDetailProtocol(String srcRefNo, String orderType, String type, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/queryTxnDetail";
        this.srcRefNo = srcRefNo;
        this.orderType = orderType;
        this.type = type;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(srcRefNo)) {
            mBodyParams.put(RequestParams.SRCREFNO, srcRefNo);
        }
        if (!TextUtils.isEmpty(orderType)) {
            mBodyParams.put(RequestParams.ORDERTYPE, orderType);
        }
        if (!TextUtils.isEmpty(type)) {
            mBodyParams.put(RequestParams.LISTTYPE, type);
        }
    }

}
