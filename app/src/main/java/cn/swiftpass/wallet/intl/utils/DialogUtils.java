package cn.swiftpass.wallet.intl.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.util.Log;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.CustomMessageDialog;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;

public class DialogUtils {
    private static final String TAG = "DialogUtils";
    private static CustomMsgDialog mDealDialog;

    public static void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (mContext == null) {
            return;
        }
        if (mDealDialog != null && mDealDialog.isShowing()) {
            //dialog 已经显示了 避免重复弹框
            return;
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.dialog_right_btn_ok), new android.content.DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
                mDealDialog = null;
            }
        });
        try {
            Activity mActivity = (Activity) mContext;
            if (mActivity != null && !mActivity.isFinishing()) {
                mDealDialog = builder.create();
                if (!mActivity.isFinishing()) {
                    mDealDialog.show();
                }

            }
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }


    public static void showErrorMsgDialog(Context mContext, String msg, String title) {
        if (mContext == null) {
            return;
        }
        if (mDealDialog != null && mDealDialog.isShowing()) {
            //dialog 已经显示了 避免重复弹框
            return;
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.dialog_right_btn_ok), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    public static void showErrorMsgDialog(Context mContext, String msg) {
        if (mContext == null) {
            return;
        }
        if (mDealDialog != null && mDealDialog.isShowing()) {
            //dialog 已经显示了 避免重复弹框
            return;
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.dialog_right_btn_ok), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

//    public static void showErrorMsgDialogAndResart(final Activity mContext, String msg) {
//        if (mContext == null) {
//            return;
//        }
//        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
//        builder.setMessage(msg);
//        builder.setPositiveButton(mContext.getString(R.string.dialog_right_btn_ok), new android.content.DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                AndroidUtils.clearMemoryCache();
//            }
//        });
//        if (mContext != null && !mContext.isFinishing()) {
//            CustomMsgDialog mDealDialog = builder.create();
//            mDealDialog.setCancelable(false);
//            mDealDialog.show();
//        }
//    }


    /**
     * @param activity
     * @param title
     * @param msg
     * @param posText
     * @param negText
     * @param posClickListener
     * @param negClickListener 主要解决 点击cancel/ok 是不是需要会掉
     */
    public static void showTipDialog(Activity activity, String title, String msg, String posText, String negText, final DialogInterface.OnClickListener posClickListener, final DialogInterface.OnClickListener negClickListener) {
        CustomMessageDialog.Builder builder = new CustomMessageDialog.Builder(activity);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        if (!TextUtils.isEmpty(msg)) {
            builder.setMessage(msg);
        }
        String strPos = TextUtils.isEmpty(posText) ? activity.getString(R.string.button_ok) : posText;
        builder.setPositiveButton(strPos, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (null != posClickListener) {
                    posClickListener.onClick(dialog, which);
                }
            }
        });
        String strNeg = TextUtils.isEmpty(negText) ? activity.getString(R.string.string_cancel) : negText;
        builder.setNegativeButton(strNeg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (null != negClickListener) {
                    negClickListener.onClick(dialog, which);
                }
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        if (!activity.isFinishing()) {
            CustomMessageDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();
        }
    }


}
