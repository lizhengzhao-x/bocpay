package cn.swiftpass.wallet.intl.widget.dialog;

import cn.swiftpass.wallet.intl.entity.CreditCardRewardDateItemList;

public interface OnCardRewardDateListener {
    void OnSelectRewardDate(CreditCardRewardDateItemList selectType, int selectIndex);
}
