package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.BindeNewCardBottomHolder;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.BindeNewCardContentHolder;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.BindeNewCardHeaderHolder;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.OnBindeNewCardListener;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class BindNewCardAdapter extends RecyclerView.Adapter implements OnBindeNewCardListener {
    private static final int TYPE_BIND_NEW_CARD_HEADER = 101;
    private static final int TYPE_BIND_NEW_CARD_BOTTOM = 102;
    private static final int TYPE_BIND_NEW_CARD_CONTENT = 103;
    private final OnConfirmCardListLinstener listener;


    private ArrayList<BindNewCardEntity> data;
    private final Context context;
    private int flowType;

    public BindNewCardAdapter(Context context, OnConfirmCardListLinstener listener) {
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_BIND_NEW_CARD_HEADER) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_bind_new_card_header, parent, false);
            return new BindeNewCardHeaderHolder(context, view);
        }
        if (viewType == TYPE_BIND_NEW_CARD_BOTTOM) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_bind_new_card_bottom, parent, false);
            return new BindeNewCardBottomHolder(view);
        }
        View view = LayoutInflater.from(context).inflate(R.layout.item_bind_new_card_content, parent, false);
        return new BindeNewCardContentHolder(context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BindeNewCardHeaderHolder) {
            BindeNewCardHeaderHolder bottomHolder = (BindeNewCardHeaderHolder) holder;
            bottomHolder.setData(flowType);
        }
        if (holder instanceof BindeNewCardBottomHolder) {
            BindeNewCardBottomHolder bottomHolder = (BindeNewCardBottomHolder) holder;
            bottomHolder.setData(data, this);
        }

        if (holder instanceof BindeNewCardContentHolder) {
            BindeNewCardContentHolder contentHolder = (BindeNewCardContentHolder) holder;
            contentHolder.setData(data.get(position - 1), this);
        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_BIND_NEW_CARD_HEADER;
        } else if (position == getItemCount() - 1) {
            return TYPE_BIND_NEW_CARD_BOTTOM;
        } else {
            return TYPE_BIND_NEW_CARD_CONTENT;
        }

    }

    @Override
    public int getItemCount() {
        return 2 + (data != null && data.size() > 0 ? data.size() : 0);
    }

    public void setData(ArrayList<BindNewCardEntity> cardInfoList) {
        this.data = cardInfoList;
    }

    @Override
    public void onConfirm() {

        if (data != null && data.size() > 0) {
            ArrayList<BindNewCardEntity> selectData = new ArrayList<>();
            for (BindNewCardEntity cardEntity : data) {
                if (cardEntity.isCheck()) {
                    selectData.add(cardEntity);
                }
            }
            if (selectData.size() > 0) {
                listener.onConfirmList(selectData);
            }
        }

    }

    @Override
    public void OnChangeCheck() {
        if (data != null && data.size() > 0) {
            int i = 0;
            for (BindNewCardEntity cardEntity : data) {
                if (cardEntity.isCheck()) {
                    i++;
                }
            }
            //每次选择后需要对整个列表的其他条目状态进行刷
            for (BindNewCardEntity cardEntity : data) {
                //如果数量达到5个，则其他的不能选择
                if (i > 4) {
                    if (!cardEntity.isCheck()) {
                        cardEntity.setCanSelect(false);
                    }
                } else {
                    cardEntity.setCanSelect(true);
                }
            }

        }
        notifyDataSetChanged();
    }

    public void setFlowType(int flowType) {
        this.flowType = flowType;
    }
}
