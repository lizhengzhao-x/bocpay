package cn.swiftpass.wallet.intl.module.register.adapter;

import android.view.View;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.radiobutton.MaterialRadioButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class TaxInfoQuestionTwoHolder extends RecyclerView.ViewHolder implements RadioGroup.OnCheckedChangeListener {
    @BindView(R.id.rb_question_two_yes)
    MaterialRadioButton rbQuestionTwoYes;
    @BindView(R.id.rb_question_two_not)
    MaterialRadioButton rbQuestionTwoNot;
    @BindView(R.id.rg_question_two)
    RadioGroup rgQuestionTwo;
    private OnChangeQuestionTwoListener listener;

    public TaxInfoQuestionTwoHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        rgQuestionTwo.setOnCheckedChangeListener(this);
    }

    public void setChangeQuestionTwoListener(OnChangeQuestionTwoListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == rbQuestionTwoYes.getId()) {
            if (listener != null) {
                listener.OnChangeQuestionTwo(TaxListAdapter.DATA_QUESTION_YES);
            }
        } else if (checkedId == rbQuestionTwoNot.getId()) {
            if (listener != null) {
                listener.OnChangeQuestionTwo(TaxListAdapter.DATA_QUESTION_NO);
            }
        }
    }

    public void setData(int resultSelectQuestionTwo) {
        if (resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_YES) {
            rgQuestionTwo.check(R.id.rb_question_two_yes);
        }
        if (resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_NO) {
            rgQuestionTwo.check(R.id.rb_question_two_not);
        }
    }

    public interface OnChangeQuestionTwoListener {
        void OnChangeQuestionTwo(int select);
    }
}
