package cn.swiftpass.wallet.intl.module.rewardregister.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.CheckRegistrationStatus;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardPansListEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardRegisterEntity;

public class RewardRegisterContract {


    public interface View extends IView {

        void rewardRegisterSuccess(RewardRegisterEntity response);

        void rewardRegisterError(String errorCode, String errorMsg);

        void rewardCardListSuccess(RewardPansListEntity response);

        void rewardCardListError(String errorCode, String errorMsg);


        void rewardCheckStatusSuccess(CheckRegistrationStatus response);

        void rewardCheckStatusError(String errorCode, String errorMsg);
    }

    public interface Presenter extends IPresenter<RewardRegisterContract.View> {


        void rewardRegister(String pan, String campaignId);

        void rewardCardList(String bankCardTye);

        void rewardCheckStatus(String cardType, String campaignId);

    }


}
