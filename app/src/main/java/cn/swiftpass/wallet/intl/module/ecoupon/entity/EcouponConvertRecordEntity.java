package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/14.
 */
public class EcouponConvertRecordEntity extends BaseEntity {


    private List<RowsBean> rows;

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean extends BaseEntity {
        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * exchangeDate : 20210517203732
         * exchangeNum : 1
         * points : 12500
         * referenceNos : []
         * itemName : PARKnSHOP eVoucher HKD50
         * merchantIconUrl : /is_stat/img/giftpoint/gifts/82110.jpg
         */
        private String status;
        private String exchangeDate;
        private String exchangeNum;
        private String points;
        private String itemName;
        private String merchantIconUrl;
        private List<String> referenceNos;

        /**
         * 该字段为CONSUME 就显示奖赏这个图标
         */
        private String distributeType;

        /**
         * 是否显示奖赏图标
         */
        public boolean isShowDistribute() {
            return "CONSUME".equals(distributeType);
        }

        public String getDistributeType() {
            return distributeType;
        }

        public void setDistributeType(String distributeType) {
            this.distributeType = distributeType;
        }

        public String getReferenceNo() {
            return referenceNo;
        }

        public void setReferenceNo(String referenceNo) {
            this.referenceNo = referenceNo;
        }

        private String referenceNo;/// 電子券編號

        public String getConsumeDate() {
            return consumeDate;
        }

        public void setConsumeDate(String consumeDate) {
            this.consumeDate = consumeDate;
        }

        private String consumeDate;/// 使用日期

        public String getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(String expireDate) {
            this.expireDate = expireDate;
        }

        private String expireDate;

        public String getExchangeDate() {
            return exchangeDate;
        }

        public void setExchangeDate(String exchangeDate) {
            this.exchangeDate = exchangeDate;
        }

        public String getExchangeNum() {
            return exchangeNum;
        }

        public void setExchangeNum(String exchangeNum) {
            this.exchangeNum = exchangeNum;
        }

        public String getPoints() {
            return points;
        }

        /**
         * 如果后台返回积分为0  则隐藏积分显示
         *
         * @return
         */
        public boolean hidePoint() {
            return "0".equals(points);
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        public String getMerchantIconUrl() {
            return merchantIconUrl;
        }

        public void setMerchantIconUrl(String merchantIconUrl) {
            this.merchantIconUrl = merchantIconUrl;
        }

        public List<?> getReferenceNos() {
            return referenceNos;
        }

        public void setReferenceNos(List<String> referenceNos) {
            this.referenceNos = referenceNos;
        }
    }
}
