package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.Manifest;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.widget.NestedScrollView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderStatusContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderConst;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderStatusEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter.TransferCrossBorderStatusPresenter;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.utils.CrossBorderShareImageUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

public class TransferCrossBorderStatusActivity extends BaseCompatActivity<TransferCrossBorderStatusContract.Presenter> implements TransferCrossBorderStatusContract.View {


    public static final String STATUS_SUCCESS = "4";
    public static final String STATUS_FAIL = "3";
    public static final String STATUS_NOT_PAYS = "1";
    public static final String STATUS_ING = "2";


    @BindView(R.id.iv_status)
    ImageView ivStatus;
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_payee_account)
    TextView tvPayeeAccount;
    @BindView(R.id.tv_payee_card_id)
    TextView tvPayeeCardId;
    @BindView(R.id.tv_order_id)
    TextView tvOrderId;
    @BindView(R.id.tv_amount_hkd)
    TextView tvAmountHkd;
    @BindView(R.id.tv_amount_rmb)
    TextView tvAmountRmb;
    @BindView(R.id.tv_commission)
    TextView tvCommission;
    @BindView(R.id.tv_total_payment)
    TextView tvTotalPayment;
    @BindView(R.id.tv_purpose)
    TextView tvPurpose;
    @BindView(R.id.tv_reject_reason)
    TextView tvRejectReason;
    @BindView(R.id.tv_payment_account)
    TextView tvPaymentAccount;

    @BindView(R.id.id_linear_charge)
    LinearLayout llCharge;
    @BindView(R.id.ll_reject_reason)
    LinearLayout llRejectReason;
    @BindView(R.id.tv_payee_card_bank)
    TextView tvBankName;
    @BindView(R.id.tv_order_time)
    TextView tvOrderTime;

    @BindView(R.id.id_root_view)
    NestedScrollView idRootView;

    private TransferCrossBorderStatusEntity statusEntity;


    @Override
    public void init() {
        setToolBarTitle(R.string.SMB1_1_4);
        hideBackIcon();
        if (getIntent() != null) {
            String orderId = getIntent().getStringExtra(TransferCrossBorderConst.DATA_TRANSFER_ORDER_ID);
            mPresenter.getTransferStatus(orderId, null);
        }

        loadDetailBanners();
    }


    /**
     * initBanner 接口主要用于拉取各种banner接口
     */
    private void loadDetailBanners() {
        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {

            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_cross_border_status;
    }

    @Override
    protected TransferCrossBorderStatusContract.Presenter createPresenter() {
        return new TransferCrossBorderStatusPresenter();
    }


    @Override
    public void getTransferStatusSuccess(TransferCrossBorderStatusEntity response) {
        statusEntity = response;
        bindView(response);
    }

    private void bindView(TransferCrossBorderStatusEntity response) {
        if (response != null) {

            if (STATUS_SUCCESS.equals(response.status)) {
                ivStatus.setImageResource(R.mipmap.icon_crossboarder_success);
                tvStatus.setText(R.string.CT1_5_1);
                llRejectReason.setVisibility(View.GONE);

            } else if (STATUS_FAIL.equals(response.status) || STATUS_NOT_PAYS.equals(response.status)) {
                ivStatus.setImageResource(R.mipmap.icon_crossboarder_fail);
                tvStatus.setText(R.string.CT1_7_1);
                llRejectReason.setVisibility(View.VISIBLE);
            } else if (STATUS_ING.equals(response.status)) {
                ivStatus.setImageResource(R.mipmap.icon_crossboarder_processing);
                tvStatus.setText(R.string.CT1_6_1);
                llRejectReason.setVisibility(View.GONE);
            }

            tvPayeeAccount.setText(response.receiverName);
            tvPayeeCardId.setText(response.receiverCardNo);
            tvOrderId.setText(response.referenceNo);
            // CT2-1-9a
            try {
                double amountHKD = Double.parseDouble(response.amountHKD);
                String amountHKDStr = AndroidUtils.formatPriceWithPoint(amountHKD, true);
                tvAmountHkd.setText(getString(R.string.CT2_1_9a) + " " + amountHKDStr);
            } catch (NumberFormatException e) {
                tvAmountHkd.setText(getString(R.string.CT2_1_9a) + " " + response.amountHKD);
            }

            try {
                double amountCNY = Double.parseDouble(response.amountCNY);
                String amountCNYStr = AndroidUtils.formatPriceWithPoint(amountCNY, true);
                tvAmountRmb.setText(getString(R.string.CR2101_5_5) + " " + amountCNYStr);
            } catch (NumberFormatException e) {
                tvAmountRmb.setText(getString(R.string.CR2101_5_5) + " " + response.amountCNY);
            }


            try {
                double amountFee = Double.parseDouble(response.fee);
                String amountFeeStr = AndroidUtils.formatPriceWithPoint(amountFee, true);
                tvCommission.setText(getString(R.string.CT2_1_9a) + " " + amountFeeStr);
            } catch (NumberFormatException e) {
                tvCommission.setText(getString(R.string.CT2_1_9a) + " " + response.fee);
            }

            tvOrderTime.setText(response.txTime);

            try {
                double totalAmount = Double.parseDouble(response.totalAmount);
                String totalAmountStr = AndroidUtils.formatPriceWithPoint(totalAmount, true);
                tvTotalPayment.setText(getString(R.string.CT2_1_9a) + " " + totalAmountStr);
            } catch (NumberFormatException e) {
                tvTotalPayment.setText(getString(R.string.CT2_1_9a) + " " + response.totalAmount);
            }

            tvBankName.setText(response.receiverBankName + " - " + AndroidUtils.getCardType(response.receiverCardType, this));
            tvPurpose.setText(response.forUsed);
            String sb = ("1".equals(response.accountType) ? getString(R.string.LYP01_06_6) : getString(R.string.LYP01_06_4)) + "(" + response.panFour + ")";
            tvPaymentAccount.setText(sb);
            //3失败 2处理中 4成功
            tvRejectReason.setText(response.rejectReason);

        }
        //防止成功/失败的
        idRootView.setVisibility(View.VISIBLE);
    }

    @Override
    public void getTransferStatusError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                MyActivityManager.removeAllTaskExcludeMainStack();
                ActivitySkipUtil.startAnotherActivity(TransferCrossBorderStatusActivity.this, MainHomeActivity.class);
            }
        });
    }



    /**
     * 具体采用系统分享方式分享
     *
     */
    private void shareImage() {
        PermissionInstance.getInstance().getStoreImagePermission(this, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                CrossBorderShareImageUtils.shareImageToOtherByNewUI(TransferCrossBorderStatusActivity.this, 99, CrossBorderShareImageUtils.generateSuccessShareParams(TransferCrossBorderStatusActivity.this,statusEntity));
            }

            @Override
            public void rejectPermission() {
                CrossBorderShareImageUtils.showLackOfPermissionDialog(TransferCrossBorderStatusActivity.this);
            }
        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @OnClick({R.id.ll_save_image, R.id.tv_back_home,R.id.ll_share_transfer_status})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_save_image:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                PermissionInstance.getInstance().getStoreImagePermission(TransferCrossBorderStatusActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        CrossBorderShareImageUtils.saveImageToGalleyByNewUI(TransferCrossBorderStatusActivity.this, CrossBorderShareImageUtils.generateSuccessShareParams(TransferCrossBorderStatusActivity.this,statusEntity));
                    }

                    @Override
                    public void rejectPermission() {
                        CrossBorderShareImageUtils.showLackOfPermissionDialog(TransferCrossBorderStatusActivity.this);
                    }
                }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
                break;
            case R.id.tv_back_home:
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                MyActivityManager.removeAllTaskExcludeMainStack();
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
                break;
            case R.id.ll_share_transfer_status:
                    shareImage();
            break;

            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }


}