package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.ConfirmTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FirstCrossTransferOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FirstCrossTransferVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.CrossTransferFirstSendOtpContract;

;

public class CrossTransferFirstSendPresenter extends BasePresenter<CrossTransferFirstSendOtpContract.View> implements CrossTransferFirstSendOtpContract.Presenter {
    @Override
    public void transferFirstSenOtp(String txnId, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FirstCrossTransferOtpProtocol(txnId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstSenOtpError(errorCode, errorMsg, isTryAgain);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstSenOtpSuccess(response, isTryAgain);
            }
        }).execute();
    }

    @Override
    public void transferFirstVerifyOtp(final String txnId, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FirstCrossTransferVerifyOtpProtocol(txnId, verifyCode, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstVerifyOtpError(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
//                if (getView() != null) {
//                    getView().dismissDialog();
//                }
//                getView().transferFirstVerifyOtpSuccess(response);
                getTransferConfrim(txnId);
            }
        }).execute();
    }

    @Override
    public void getTransferConfrim(String txId) {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }
        new ConfirmTransferProtocol(txId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferConfirmError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().transferConfrimSuccess(response);
                }
            }
        }).execute();
    }
}
