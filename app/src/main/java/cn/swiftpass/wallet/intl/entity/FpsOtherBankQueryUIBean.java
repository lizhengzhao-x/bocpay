package cn.swiftpass.wallet.intl.entity;

public class FpsOtherBankQueryUIBean extends FpsOtherBankRecordDisplayBean {


    /**
     * UI三种类型 中国银行 其他银行 其他银行标题 通过recycleview itemType来完成
     */
    public static final int TYPE_BANK_HK = 100;
    public static final int TYPE_BANK_HK_INCLUDE_DESC = 400;
    public static final int TYPE_BANK_OTHER = 200;
    public static final int TYPE_BANK_OTHER_TITLE = 300;

    public FpsOtherBankQueryUIBean() {
        this.itemUiType = TYPE_BANK_OTHER;
    }

    public int getItemUiType() {
        return itemUiType;
    }

    public void setItemUiType(int itemUiType) {
        this.itemUiType = itemUiType;
    }

    private int itemUiType;

    public boolean isEditStatus() {
        return isEditStatus;
    }

    public void setEditStatus(boolean editStatus) {
        isEditStatus = editStatus;
    }

    /**
     * 当前ui是否是编辑状态
     */
    private boolean isEditStatus;


}
