package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.os.Bundle;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ResetPwdSuccessActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.ForgetPwdVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardForgetVerifyOtpEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.VirtualCardForgetPasswordPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardForgetPwdSendOtpActivity extends AbstractSendOTPActivity implements ForgetPwdVitualCardSendOtpContract.View {
    private String mCardId;
    private ForgetPwdVitualCardSendOtpContract.Presenter mPresenterDetail;
    private String passcode;
    private String checkType;

    @Override
    public void init() {
        if (mPresenter instanceof ForgetPwdVitualCardSendOtpContract.Presenter) {
            mPresenterDetail = (ForgetPwdVitualCardSendOtpContract.Presenter) mPresenter;
        }
        phoneNo = getIntent().getExtras().getString(Constants.MOBILE_PHONE);
        passcode = getIntent().getExtras().getString(Constants.ACCOUNT_PASSCODE);
        checkType = getIntent().getExtras().getString(Constants.FORGET_PASSWORD_TYPE);
        setToolBarTitle(R.string.setting_payment_fgw);
        super.init();
    }

    @Override
    protected ForgetPwdVitualCardSendOtpContract.Presenter createPresenter() {
        return new VirtualCardForgetPasswordPresenter();
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.vVitualCardVerifyOtp(checkType, passcode, etwdOtpCode.getEditText().getText().toString().trim());
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {

        mPresenterDetail.vitualCardSenOtp(ApiConstant.VIRTUALCARD_FORGETPWD, checkType, isTryAgain);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void vitualCardSenOtpSuccess(ContentEntity response, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void vitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(VirtualCardForgetPwdSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                showRetrySendOtp();
            }
        });
    }

    @Override
    public void vitualCardVerifyOtpSuccess(VirtualCardForgetVerifyOtpEntity response) {
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        ActivitySkipUtil.startAnotherActivity(VirtualCardForgetPwdSendOtpActivity.this, ResetPwdSuccessActivity.class);
    }

    @Override
    public void vitualCardVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(VirtualCardForgetPwdSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }
}
