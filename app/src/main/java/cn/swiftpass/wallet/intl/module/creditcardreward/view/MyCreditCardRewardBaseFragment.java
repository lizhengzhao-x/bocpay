package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import android.view.View;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.MyCreditCardRewardActionList;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.MyCreditCardRewardContact;
import cn.swiftpass.wallet.intl.module.creditcardreward.presenter.MyCreditCardRewardPresent;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.CreditCardRewardBaseAdapter;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.OnRewadDetailListener;
import cn.swiftpass.wallet.intl.module.register.RegisterSuccessBannerActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

public class MyCreditCardRewardBaseFragment extends BaseFragment<MyCreditCardRewardContact.Presenter> implements MyCreditCardRewardContact.View, OnRewadDetailListener {

    private final String dateType;
    @BindView(R.id.ry_my_credit_card_reward)
    RecyclerView ryMyCreditCardReward;
    @BindView(R.id.srl_my_credit_card_reward)
    SmartRefreshLayout srlMyCreditCardReward;
    @BindView(R.id.ll_credit_card_reward_empty)
    LinearLayout llCreditCardRewardEmpty;

    private CreditCardRewardBaseAdapter adapter;
    private int currentPage = 1;

    public MyCreditCardRewardBaseFragment(String dateType) {
        this.dateType = dateType;
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {

    }

    @Override
    protected MyCreditCardRewardContact.Presenter loadPresenter() {
        return new MyCreditCardRewardPresent();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_my_credit_card_reward_base;
    }

    @Override
    protected void initView(View v) {
        llCreditCardRewardEmpty.setVisibility(View.GONE);
        ryMyCreditCardReward.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        ryMyCreditCardReward.setLayoutManager(layoutManager);
        adapter = new CreditCardRewardBaseAdapter(mContext, this);
        ryMyCreditCardReward.setAdapter(adapter);
        initListener();

        mPresenter.loadCreditCardReward(dateType, String.valueOf(currentPage));
    }

    private void initListener() {
        srlMyCreditCardReward.setOnRefreshListener(refreshLayout -> {
            currentPage = 1;
            mPresenter.loadCreditCardReward(dateType, String.valueOf(currentPage));
        });

    }

    @Override
    public void getCreditCardRewardListError(String errorCode, String errorMsg) {
        srlMyCreditCardReward.finishRefresh(false);
        srlMyCreditCardReward.finishLoadMore(false);
    }

    @Override
    public void getCreditCardRewardListSuccess(MyCreditCardRewardActionList response) {
        srlMyCreditCardReward.finishRefresh();
        srlMyCreditCardReward.finishLoadMore();

        if (response != null && response.getRows() != null && response.getRows().size() > 0) {
            adapter.setData(response.getRows());
            llCreditCardRewardEmpty.setVisibility(View.GONE);
            ryMyCreditCardReward.setVisibility(View.VISIBLE);
            adapter.notifyDataSetChanged();
        } else {
            llCreditCardRewardEmpty.setVisibility(View.VISIBLE);
            ryMyCreditCardReward.setVisibility(View.GONE);
        }
    }

    @Override
    public void OnJumpDetail(String details) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, details);
        mHashMaps.put(Constants.DETAIL_TITLE, mActivity.getString(R.string.CRQ2107_1_8));
        mHashMaps.put(WebViewActivity.NEW_MSG, false);
        ActivitySkipUtil.startAnotherActivity(mActivity, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        RegisterSuccessBannerActivity.startActivity(mActivity, details);
    }
}
