package cn.swiftpass.wallet.intl.api.protocol;

import java.util.ArrayList;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

/**
 * 信用卡绑定发送otp
 */
public class BindeNewCardSendOtpProtocol extends BaseProtocol {
    ArrayList<BindNewCardEntity> newCardEntityArrayList;

    String action;

    public BindeNewCardSendOtpProtocol(ArrayList<BindNewCardEntity> cardEntities, String actionIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        newCardEntityArrayList = cardEntities;
        this.action = actionIn;
        mUrl = "api/register/sendOtpBatchCc";

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
        ArrayList<String> cardList = new ArrayList<>();
        if (newCardEntityArrayList != null) {
            for (BindNewCardEntity bindNewCardEntity : newCardEntityArrayList) {
                cardList.add(bindNewCardEntity.getPanShowNum());
            }
        }
        mBodyParams.put(RequestParams.PAN_SHOW_NUM_LIST, cardList);
    }

}
