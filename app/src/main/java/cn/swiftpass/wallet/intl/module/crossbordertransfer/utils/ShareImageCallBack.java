package cn.swiftpass.wallet.intl.module.crossbordertransfer.utils;

public interface ShareImageCallBack {
    void OnSuccessShareImage();

    void OnFailShareImage();
}
