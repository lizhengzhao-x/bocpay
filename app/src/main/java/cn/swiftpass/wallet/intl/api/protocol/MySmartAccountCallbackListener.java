package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;

public  class MySmartAccountCallbackListener extends NetWorkCallbackListener<MySmartAccountEntity> {

    private final NetWorkCallbackListener<MySmartAccountEntity> listener;

    public MySmartAccountCallbackListener(NetWorkCallbackListener<MySmartAccountEntity> listener) {
        this.listener=listener;
    }

    @Override
    public void onFailed(String errorCode, String errorMsg) {
        if (listener!=null){
            listener.onFailed(errorCode,  errorMsg);
        }
    }

    @Override
    public void onSuccess(MySmartAccountEntity response) {
        if (listener!=null) {
            if (response!=null){
                CacheManagerInstance.getInstance().setMySmartAccountEntity(response);
            }
            listener.onSuccess(response);
        }
    }
}
