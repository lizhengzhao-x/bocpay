package cn.swiftpass.wallet.intl.module.topup;

import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @Package cn.swiftpass.wallet.intl.activity
 * @Description:
 * @date 2017/12/30.15:36.
 */

public class TopUpSuccessActivity extends BaseCompatActivity {

    @BindView(R.id.tv_content)
    TextView tvContent;
    @BindView(R.id.tv_card)
    TextView tvCard;
    @BindView(R.id.id_tv_ok)
    TextView idTvOk;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.id_title_str)
    TextView idTitleStr;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        hideBackIcon();

        int type = getIntent().getExtras().getInt(Constants.TYPE);
        if (type == Constants.TYPE_TOPUP) {
            setToolBarTitle(R.string.AC2101_2_17);
            idTitleStr.setText(getResources().getString(R.string.P3_ewa_01_034));
        } else {
            setToolBarTitle(R.string.P3_ewa_01_039);
            idTitleStr.setText(getResources().getString(R.string.P3_ewa_01_034));
        }

        idTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                //需要刷新用户信息
                EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT, ""));
                finish();
            }
        });
        String accTtype = getIntent().getExtras().getString(Constants.ACCTYPE);

        String str = getIntent().getExtras().getString(Constants.MONEY);
        String amount = AndroidUtils.formatPriceWithPoint(Double.parseDouble(str), true);
        tvContent.setText(amount);

        tvName.setText(getIntent().getExtras().getString(Constants.RECORD_NUMBER));
        idPaySymbol.setText(getIntent().getExtras().getString(Constants.CURRENCY));
        String method = getIntent().getExtras().getString(Constants.PAYMENT_METHOD);
        if (TextUtils.isEmpty(accTtype)) {
            tvCard.setText("(" + AndroidUtils.subPaymentMethod(method) + ")");
        } else {
            tvCard.setText(accTtype + "(" + AndroidUtils.subPaymentMethod(method) + ")");
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_topup_success;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
