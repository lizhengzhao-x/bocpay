package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/12/9.
 */
public class RateInfoEntity extends BaseEntity {


    private String CNY2HKDRate;

    public String getCNY2HKDRate() {
        return CNY2HKDRate;
    }

    public void setCNY2HKDRate(String CNY2HKDRate) {
        this.CNY2HKDRate = CNY2HKDRate;
    }

    public String getHKD2CNYRate() {
        return HKD2CNYRate;
    }

    public void setHKD2CNYRate(String HKD2CNYRate) {
        this.HKD2CNYRate = HKD2CNYRate;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getLastUpdatedRate() {
        return lastUpdatedRate;
    }

    public void setLastUpdatedRate(String lastUpdatedRate) {
        this.lastUpdatedRate = lastUpdatedRate;
    }

    private String HKD2CNYRate;
    private String refNo;
    private String lastUpdatedRate;


}
