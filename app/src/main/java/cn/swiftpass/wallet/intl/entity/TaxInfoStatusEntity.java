package cn.swiftpass.wallet.intl.entity;


/**
 * 带tax资料填写状态的 TaxInfo
 */
public class TaxInfoStatusEntity extends TaxInfoEntity {

    public final static int TYPE_SELECT_NONE = 0;
    public final static int TYPE_SELECT_TAX_NUM = 101;
    public final static int TYPE_SELECT_TAX_REASON = 102;

    private int selectType = TYPE_SELECT_NONE;

    private boolean finishInfo;


    public int getSelectType() {
        return selectType;
    }

    public void setSelectType(int selectType) {
        this.selectType = selectType;
    }

    public boolean isFinishInfo() {
        return finishInfo;
    }

    public void setFinishInfo(boolean finishInfo) {
        this.finishInfo = finishInfo;
    }
}
