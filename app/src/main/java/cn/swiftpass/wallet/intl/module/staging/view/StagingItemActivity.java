package cn.swiftpass.wallet.intl.module.staging.view;

import android.text.TextUtils;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.home.constants.MenuKey;

public class StagingItemActivity extends BaseCompatActivity {

    /**
     * 月结单分期
     */
    public static final String STAGINGITEM_MONTH = MenuKey.MONTH_STAGING;
    /**
     * 现金结单分期
     */
    public static final String STAGINGITEM_CASH = MenuKey.CASH_STAGING;

    public static final String STAGINGITEM_ITEM = "STAGINGITEM_ITEM";
    private FragmentManager mFragmentManager;

    @Override
    public void init() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        String currentStagItem = getIntent().getExtras().getString(STAGINGITEM_ITEM, STAGINGITEM_MONTH);
        if (TextUtils.equals(currentStagItem, STAGINGITEM_MONTH)) {
            setToolBarTitle(getString(R.string.MB2103_1_1));
        } else {
            setToolBarTitle(getString(R.string.MB2103_1_2));
        }
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        if (TextUtils.equals(currentStagItem, STAGINGITEM_MONTH)) {
            StagingItemMonthFragment stagingItemFragment = new StagingItemMonthFragment();
            fragmentTransaction.add(R.id.fl_main, stagingItemFragment);
        } else {
            StagingItemCashFragment stagingItemCashFragment = new StagingItemCashFragment();
            fragmentTransaction.add(R.id.fl_main, stagingItemCashFragment);
        }
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_staging;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


}
