package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class STwoSetPasscodeProtocol extends BaseProtocol {

    public static final String TAG = SmartForgotPwdProtocol.class.getSimpleName();
    /**
     * 支付密码
     */
    String mPasscode;


    public STwoSetPasscodeProtocol(String passcode, NetWorkCallbackListener dataCallback) {
        this.mPasscode = passcode;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/setPasscode";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PASSCODE, mPasscode);
    }

}
