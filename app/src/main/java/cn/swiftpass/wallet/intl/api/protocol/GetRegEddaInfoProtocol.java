package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class GetRegEddaInfoProtocol extends BaseProtocol {

    public GetRegEddaInfoProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/getRegEddaInfo";
    }
}
