package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class S2RegisterMobileEntity extends BaseEntity {

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    private String mobile;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    private String userId;
//    public boolean isOpenBiometricAuth() {
//        if (TextUtils.isEmpty(openBiometricAuth)) {
//            return true;
//        }
//        return TextUtils.equals(openBiometricAuth, "Y");
//    }
//
//    public void setOpenBiometricAuth(String openBiometricAuth) {
//        this.openBiometricAuth = openBiometricAuth;
//    }
//
//    /**
//     * 是否开启生物认证 Y开启 N 不开启
//     */
//    private String openBiometricAuth;
}
