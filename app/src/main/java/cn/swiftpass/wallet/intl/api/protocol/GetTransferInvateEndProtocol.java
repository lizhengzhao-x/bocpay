package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 查询邀请赚取奖赏活动是否结束
 */
public class GetTransferInvateEndProtocol extends BaseProtocol {

    public GetTransferInvateEndProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/isTransferInvateEnd";
    }


}
