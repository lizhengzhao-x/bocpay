package cn.swiftpass.wallet.intl.entity.event;


public class ForgetPwdEventEntity extends BaseEventEntity{

    public static final int EVENT_FORGET_PWD = 10001;
    public static final int EVENT_FORGET_PWD_LOGIN = 10002;


    public ForgetPwdEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public ForgetPwdEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
