package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 首页活动列表数据URL
 */

public class UnlinkCardEntity extends BaseEntity {
    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    String logout;

}
