package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 获取信用卡奖赏活动-奖赏记录
 */
public class GetHistoryCreditCardRewardListProtocol extends BaseProtocol {


    private final String currentPage;
    private final String dateKey;
    private final String rewardsType;

    public GetHistoryCreditCardRewardListProtocol(String dateKey, String rewardsType, String currentPage, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.dateKey = dateKey;
        this.rewardsType = rewardsType;
        this.currentPage = currentPage;
        mUrl = "api/creditCardReward/getRewardRecords";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAGENUM, currentPage);
        mBodyParams.put(RequestParams.DATE_KEY, dateKey);
        mBodyParams.put(RequestParams.REWARDS_TYPE, rewardsType);

    }
}
