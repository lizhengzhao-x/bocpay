package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 用户是否填写过邀请码
 */
public class ShowInviteUIProtocol extends BaseProtocol {

    public ShowInviteUIProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/isShowInviteUI";
    }
}

