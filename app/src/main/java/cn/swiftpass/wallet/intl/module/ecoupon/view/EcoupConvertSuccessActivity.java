package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.ConvertUseContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemDetailEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.ConvertDetailPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcoupConvertDescAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 电子券兑换成功界面
 */
public class EcoupConvertSuccessActivity extends BaseCompatActivity<ConvertUseContract.Presenter> implements ConvertUseContract.View {


    private ImageView mId_status_image;
    private TextView mId_payment_status;
    private androidx.recyclerview.widget.RecyclerView mId_recyclerview;
    private RelativeLayout mId_rel_money_reduce;
    private TextView mId_grade_need;
    private RelativeLayout mId_rel_money_reduc_smartaccount;
    private TextView mId_left_smart_type;
    private TextView mId_grade_reduce_smart;
    private RelativeLayout mId_rel_money_reduc_creditaccount;
    private TextView mId_left_credit_type;
    private TextView mId_grade_reduce_credit;
    private RelativeLayout mId_rel_money_retain;
    private TextView mId_grade_retain;
    private TextView mId_tv_back, mId_tv_use_now;

    private RedeemResponseEntity redeemResponseEntity;
    private List<RedeemResponseEntity.ResultRedmBean> resultRedmBeans;
    private List<RedeemableGiftListEntity.EVoucherListBean> eVoucherListBeans;

    private void bindViews() {

        mId_status_image = (ImageView) findViewById(R.id.id_status_image);
        mId_payment_status = (TextView) findViewById(R.id.id_payment_status);
        mId_recyclerview = (androidx.recyclerview.widget.RecyclerView) findViewById(R.id.id_recyclerview);
        mId_rel_money_reduce = (RelativeLayout) findViewById(R.id.id_rel_money_reduce);
        mId_grade_need = (TextView) findViewById(R.id.id_grade_need);
        mId_rel_money_reduc_smartaccount = (RelativeLayout) findViewById(R.id.id_rel_money_reduc_smartaccount);
        mId_left_smart_type = (TextView) findViewById(R.id.id_left_smart_type);
        mId_grade_reduce_smart = (TextView) findViewById(R.id.id_grade_reduce_smart);
        mId_rel_money_reduc_creditaccount = (RelativeLayout) findViewById(R.id.id_rel_money_reduc_creditaccount);
        mId_left_credit_type = (TextView) findViewById(R.id.id_left_credit_type);
        mId_grade_reduce_credit = (TextView) findViewById(R.id.id_grade_reduce_credit);
        mId_rel_money_retain = (RelativeLayout) findViewById(R.id.id_rel_money_retain);
        mId_grade_retain = (TextView) findViewById(R.id.id_grade_retain);
        mId_tv_back = (TextView) findViewById(R.id.id_tv_back);
        mId_tv_use_now = (TextView) findViewById(R.id.id_tv_use_now);
    }

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC06_1));
        setToolBarRightViewToText(R.string.EC15_1);
        getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                ActivitySkipUtil.startAnotherActivity(getActivity(), EcoupRecordsActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP, ""));
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                finish();
            }
        });
        hideBackIcon();
        bindViews();
        String retainGrade = getIntent().getExtras().getString(Constants.RETAIN_GRADE);
        redeemResponseEntity = (RedeemResponseEntity) getIntent().getExtras().getSerializable(Constants.REDEEMRESPONSEENTITY);
        eVoucherListBeans = (List<RedeemableGiftListEntity.EVoucherListBean>) getIntent().getExtras().getSerializable(Constants.ECOUPONCONVERTENTITY);
        resultRedmBeans = redeemResponseEntity.getResultRedm();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerview.setLayoutManager(layoutManager);
        mId_recyclerview.setAdapter(getAdapter(null));
        ((IAdapter<RedeemableGiftListEntity.EVoucherListBean>) mId_recyclerview.getAdapter()).setData(eVoucherListBeans);
        mId_recyclerview.getAdapter().notifyDataSetChanged();
        if (!TextUtils.isEmpty(redeemResponseEntity.getRedeemPnt())) {
            mId_grade_need.setText(AndroidUtils.formatPrice(Double.valueOf(redeemResponseEntity.getRedeemPnt()), false));
        }
        mId_grade_retain.setText(AndroidUtils.formatPrice(Double.valueOf(retainGrade), false));

        if (!TextUtils.isEmpty(redeemResponseEntity.getSmaGp()) && !TextUtils.equals(redeemResponseEntity.getSmaGp(), "0")) {
            mId_rel_money_reduc_smartaccount.setVisibility(View.VISIBLE);
            mId_grade_reduce_smart.setText(AndroidUtils.formatPrice(Double.valueOf(redeemResponseEntity.getSmaGp()), false));
            if (!TextUtils.isEmpty(redeemResponseEntity.getSmaType())) {
                if (TextUtils.equals(redeemResponseEntity.getSmaType(), "2")) {
                    //支付账户
                    mId_left_smart_type.setText(getString(R.string.P3_B1_3_a_1));
                } else {
                    //智能账户
                    mId_left_smart_type.setText(getString(R.string.CP3_2_3));
                }
            }
        }
        if (!TextUtils.isEmpty(redeemResponseEntity.getCcGp()) && !TextUtils.equals(redeemResponseEntity.getCcGp(), "0")) {
            mId_rel_money_reduc_creditaccount.setVisibility(View.VISIBLE);
            mId_grade_reduce_credit.setText(AndroidUtils.formatPrice(Double.valueOf(redeemResponseEntity.getCcGp()), false));
        }

        mId_tv_back.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP, ""));
                finish();
            }
        });
        mId_tv_use_now.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                //更新兑换的view显示底部弹框
                EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_RETRY_REFRESH_CNT, ""));
                //兑换成功 如果是一张跳转到立即使用详情页 如果是多张 跳转到我的电子券列表
                if (redeemResponseEntity.isConvertMore()) {
                    EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_CAME_MY_ECOUP, ""));
                    finish();
                } else {
                    if (redeemResponseEntity.getResultRedm() != null && !redeemResponseEntity.getResultRedm().isEmpty()) {
                        RedeemResponseEntity.ResultRedmBean resultRedmBean = redeemResponseEntity.getResultRedm().get(0);
                        mPresenter.checkEcouponInfo(resultRedmBean.getGiftCode(), resultRedmBean.getOrderTp(), redeemResponseEntity.getResultRedm().get(0).getReferenceNo());
                    }
                }
            }
        });
    }

    private CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean> getAdapter(final List<RedeemableGiftListEntity.EVoucherListBean> data) {
        return new CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean>(data) {

            @Override
            public Object getItemType(RedeemableGiftListEntity.EVoucherListBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new EcoupConvertDescAdapter(mContext);
            }
        };
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoup_convert_success;
    }

    @Override
    protected ConvertUseContract.Presenter createPresenter() {
        return new ConvertDetailPresenter();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    @Override
    public void checkEcouponInfoSuccess(RedeemDetailEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.ECOUPONCONVERTENTITY, response.parseInfo());
        mHashMaps.put(Constants.ECOUPON_TITLE, response.geteVoucherTc());
        ActivitySkipUtil.startAnotherActivity(getActivity(), EcouponsDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_CAME_MY_ECOUP, ""));
        finish();
    }

    @Override
    public void checkEcouponInfoError(String errorCode, String errorMsg) {

        showErrorMsgDialog(this, errorMsg);
    }
}
