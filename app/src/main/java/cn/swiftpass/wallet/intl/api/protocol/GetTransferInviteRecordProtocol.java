package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 获取邀请记录列表
 */
public class GetTransferInviteRecordProtocol extends BaseProtocol {


    public GetTransferInviteRecordProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/getTransferInviteRecord";
    }
}
