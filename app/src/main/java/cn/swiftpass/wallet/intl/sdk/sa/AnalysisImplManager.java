package cn.swiftpass.wallet.intl.sdk.sa;

import android.app.Activity;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bocpay.analysis.AnalysisInterface;
import com.bocpay.analysis.AnalysisManager;
import com.bocpay.analysis.AnalysisResultListener;
import com.google.gson.Gson;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.CommonEntity;
import cn.swiftpass.wallet.intl.api.protocol.SendActionDataProtocol;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;

/**
 * 统计埋点事件方法管理类
 */
public class AnalysisImplManager implements AnalysisInterface {

    private static final String TAG = "AnalysisService";

    private static class AnalysisImplManagerHolder {
        private static AnalysisImplManager instance = new AnalysisImplManager();
    }

    public static AnalysisImplManager getInstance() {
        return AnalysisImplManager.AnalysisImplManagerHolder.instance;
    }

    private AnalysisImplManager() {

    }

    /**
     * 埋点接口请求
     */
    @Override
    public void sendAnalysisEvent() {
        AnalysisManager.getInstance().setAnalysisEventListener(new AnalysisManager.AnalysisEventListener() {
            @Override
            public void sendEvent(JSONArray jsonArray, final AnalysisResultListener eventResultListener) {
                new SendActionDataProtocol(jsonArray, new NetWorkCallbackListener<String>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        if (eventResultListener != null) {
                            eventResultListener.fail();
                        }
                    }

                    @Override
                    public void onSuccess(String entity) {
                        if (eventResultListener != null) {
                            CommonEntity common = new Gson().fromJson(entity, CommonEntity.class);
                            if (ResponseCode.RESPONSE_SUCCESS.equals(common.getReturn_code())){
                                eventResultListener.success();
                            }else {
                                eventResultListener.fail();
                            }

                        }
                    }
                }).execute();
            }
        });
    }

    /**
     * 根据activity的错误码埋点
     *
     * @param activity
     * @param startTime
     * @param errorCode
     * @param errMsg
     */
    public static void sendErrorEvent(Activity activity, long startTime, String errorCode, String errMsg) {
        if (activity == null) {
            return;
        }
        if (activity instanceof BaseAbstractActivity) {
            BaseAbstractActivity mActivity = (BaseAbstractActivity) activity;
            //判断页面是否需要统计埋点(用来判断部分共用页面流程,如忘记密码 注册 绑卡等)
            if (mActivity.isAnalysisPage()) {
                AnalysisPageEntity analysisPageEntity = mActivity.getAnalysisPageEntity();
                if (analysisPageEntity != null) {
                    AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errMsg, analysisPageEntity.current_address);
                    sendAnalysisEvent(analysisErrorEntity);
                }
            }
        }
    }

    public static void sendAnalysisEvent(AnalysisBaseEntity analysisBaseEntity) {
        if (analysisBaseEntity == null) {
            return;
        }
        if (analysisBaseEntity instanceof AnalysisErrorEntity) {
            AnalysisErrorEntity analysisErrorEntity = (AnalysisErrorEntity) analysisBaseEntity;
            if (ErrorCode.CONTENT_TIME_OUT.code.equals(analysisErrorEntity.error_code)
                    || ErrorCode.NETWORK_ERROR.code.equals(analysisErrorEntity.error_code)
                    || ErrorCode.SERVER_INTER_ERROR.code.equals(analysisErrorEntity.error_code)) {
                return;
            }
        }
        JSONObject jsonObject = analysisBaseEntity.getJsonObject();
        AnalysisManager.getInstance().sendAnalysisEvent(jsonObject);
    }
}
