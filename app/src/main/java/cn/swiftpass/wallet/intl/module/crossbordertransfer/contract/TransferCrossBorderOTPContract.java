package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;


import cn.swiftpass.wallet.intl.base.otp.OTPSendPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPSendView;
import cn.swiftpass.wallet.intl.base.otp.OTPTryPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPTryView;
import cn.swiftpass.wallet.intl.base.otp.OTPVerifyPresenter;
import cn.swiftpass.wallet.intl.base.otp.OTPVerifyView;

/**
 * @name cn.swiftpass.bocbill.model.login.contract
 * @class name：LoginCheckContract
 * @class describe
 * @anthor zhangfan
 * @time 2019/6/27 21:59
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderOTPContract {

    public interface View extends OTPSendView<Presenter>, OTPTryView<Presenter>, OTPVerifyView<Presenter> {


    }


    public interface Presenter extends OTPSendPresenter<View>, OTPTryPresenter<View>, OTPVerifyPresenter<View> {


    }
}
