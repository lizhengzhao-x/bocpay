package cn.swiftpass.wallet.intl.module.setting;

import android.content.DialogInterface;
import android.view.View;

import butterknife.BindView;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.setting.about.AboutActivity;
import cn.swiftpass.wallet.intl.module.setting.contract.SettingContract;
import cn.swiftpass.wallet.intl.module.setting.language.LanguageSettingActivity;
import cn.swiftpass.wallet.intl.module.setting.notify.view.NotificationSettingActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.PaymentSettingActivity;
import cn.swiftpass.wallet.intl.module.setting.presenter.SettingPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;


public class SettingFragment extends BaseFragment {
    @BindView(R.id.id_setting_language)
    ImageArrowView idSettingLanguage;
    @BindView(R.id.id_setting_payment)
    ImageArrowView idSettingPayment;
    @BindView(R.id.id_setting_about)
    ImageArrowView idSettingAbout;
    @BindView(R.id.id_setting_logout)
    ImageArrowView idSettingLogout;
    @BindView(R.id.layout_open_notification)
    ImageArrowView layoutOpenNotification;
    @BindView(R.id.id_notification_view)
    View layoutNotificationView;


    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.M10_1_25);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLoginUiStatus();
    }

    /**
     * 动态更新登录 登出按钮状态
     */
    private void updateLoginUiStatus() {
        if (getActivity() != null) {
//            String lan = SpUtils.getInstance(getActivity()).getAppLanguage();
//            if (getActivity() instanceof BaseAbstractActivity) {
//                BaseAbstractActivity mainHomeActivity = (BaseAbstractActivity) getActivity();
//                mainHomeActivity.switchLanguage(lan);
//            }
            if (CacheManagerInstance.getInstance().isLogin()) {
                if (idSettingLogout == null) return;
                idSettingLogout.setLeftTextTitle(getActivity().getString(R.string.setting_logout));
            } else {
                if (idSettingLogout == null) return;
                idSettingLogout.setLeftTextTitle(getActivity().getString(R.string.button_login_sign));
            }
        }
    }


    @Override
    protected SettingContract.Presenter loadPresenter() {
        return new SettingPresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_me;
    }

    @Override
    protected void initView(View v) {
        if (CacheManagerInstance.getInstance().isLogin()) {
            //需求是登录态才显示通知设定按钮
            layoutNotificationView.setVisibility(View.VISIBLE);
        }
        idSettingLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick(view.getId())) return;
                ActivitySkipUtil.startAnotherActivity(mActivity, LanguageSettingActivity.class);
            }
        });

        idSettingPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick(view.getId())) return;
                if (!CacheManagerInstance.getInstance().isLogin()) {
                    ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class);
                    //非登录态下，点击支付设定时，会销毁当前页面
                    if (getActivity() instanceof SettingActivity) {
                        getActivity().finish();
                    }
                    return;
                }
                ActivitySkipUtil.startAnotherActivity(mActivity, PaymentSettingActivity.class);

            }
        });

        idSettingAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick(view.getId())) return;
                ActivitySkipUtil.startAnotherActivity(mActivity, AboutActivity.class);
            }
        });


        idSettingLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!CacheManagerInstance.getInstance().isLogin()) {
                    ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class);
                } else {
                    if (CacheManagerInstance.getInstance().isHasSessionId()) {
                        showConfirmExitDialog(true);
                    } else {
                        showConfirmExitDialog(false);
                    }
                }
            }
        });
        layoutOpenNotification.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                ActivitySkipUtil.startAnotherActivity(getActivity(), NotificationSettingActivity.class);
            }
        });
    }


    private void showConfirmExitDialog(boolean isRequest) {
        CustomDialog.Builder builder = new CustomDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.setting_logout));
        builder.setMessage(getString(R.string.app_logout_instruction));
        builder.setNegativeButton(R.string.string_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setPositiveButton(R.string.btn_logout, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isRequest) {
                    ApiProtocolImplManager.getInstance().logout();
                }
                AndroidUtils.clearMemoryCacheOnly();
                AndroidUtils.goPreLoginPage();
                MyActivityManager.removeAllTaskExcludePreLoginStack();

            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        CustomDialog mDealDialog = builder.create();
        mDealDialog.show();
    }


}
