package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;

public class OrderDetailOldActivity extends BaseCompatActivity {
    @BindView(R.id.id_merchant_head)
    ImageView idMerchantHead;
    @BindView(R.id.id_merchant_name)
    TextView idMerchantName;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.id_merchant_money)
    TextView idMerchantMoney;
    @BindView(R.id.id_payment_status)
    TextView idPaymentStatus;
    @BindView(R.id.id_payto)
    TextView idPayto;
    @BindView(R.id.id_paidvia)
    TextView idPaidvia;
    @BindView(R.id.id_ordernumber)
    TextView idOrdernumber;
    @BindView(R.id.id_createtime)
    TextView idCreatetime;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_left_remark)
    TextView idLeftRemark;
    @BindView(R.id.id_card_view)
    RelativeLayout idCardView;
    @BindView(R.id.id_remark)
    TextView idRemark;
    @BindView(R.id.id_tv_discount)
    TextView id_tv_discount;
    @BindView(R.id.id_rel_postscript)
    RelativeLayout idRelPostscript;
    @BindView(R.id.id_rel_discount)
    RelativeLayout id_rel_discount;
    @BindView(R.id.id_rel_reduce_money)
    RelativeLayout idRelReduceMoney;
    @BindView(R.id.id_rel_reduce_grade)
    RelativeLayout idRelReduceGrade;
    @BindView(R.id.id_money_layout)
    RelativeLayout mId_money_layout;

    @BindView(R.id.id_detail_reduce_money)
    TextView idReduceMoney;
    @BindView(R.id.id_detail_reduce_grade)
    TextView idReduceGrade;
    @BindView(R.id.id_jifen_status_error)
    TextView mId_jifen_status_error;
    @BindView(R.id.id_reference_name)
    TextView mId_reference_name;
    @BindView(R.id.id_reducemoney_currency)
    TextView id_reducemoney_currency;
    @BindView(R.id.id_view_hint)
    View mId_view_hint;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.CP3_3a_1);
        OrderQueryEntity paymentEnquiryResult = (OrderQueryEntity) getIntent().getExtras().getSerializable(Constants.CARD_ENTITY);
        String trxAmt = paymentEnquiryResult.getTrxAmt();
//        idMerchantMoney.setText(BigDecimalFormatUtils.forMatWithDigs(trxAmt, 2));
        idMerchantMoney.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(BigDecimalFormatUtils.forMatWithDigs(trxAmt, 2)),true));
        idOrdernumber.setText(paymentEnquiryResult.getTransactionNum());
        idCreatetime.setText(AndroidUtils.formatTimeString(paymentEnquiryResult.getTransDate()));
        idPaymentStatus.setText(paymentEnquiryResult.getPaymentStatusDesc());
        String trxCurrency = paymentEnquiryResult.getCur();
        if (TextUtils.isEmpty(paymentEnquiryResult.getOutamtsign())) {
            idPaySymbol.setText(paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency));
        } else {
            idPaySymbol.setText(paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency));
        }
        //控制附言 转账交易详情页 跟bocpay交易详情页 公用 只有转账会有附言显示
        if (TextUtils.isEmpty(paymentEnquiryResult.getPostscript())) {
            idLeftRemark.setVisibility(View.INVISIBLE);
            idRelPostscript.setVisibility(View.GONE);
        } else {
            idRemark.setText(paymentEnquiryResult.getPostscript());
        }

        String merchantName = paymentEnquiryResult.getMerchantName();
        idMerchantName.setText(merchantName);
        idPayto.setText(merchantName);

        //主要是 区分 信用卡支付 还是智能账户 还是支付账户
        if (!TextUtils.isEmpty(paymentEnquiryResult.getPanFour())) {
            if (!TextUtils.isEmpty(paymentEnquiryResult.getCardType())) {
                if (paymentEnquiryResult.getCardType().equals("2")) {
                    if (!TextUtils.isEmpty(paymentEnquiryResult.getSmartAcLevel())) {
                        if (TextUtils.equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT, paymentEnquiryResult.getSmartAcLevel())) {
                            idPaidvia.setText(AndroidUtils.getPaymentTitleNew(paymentEnquiryResult.getPanFour(), mContext));
                        } else {
                            idPaidvia.setText(AndroidUtils.getSubSmartCardNumberTitleNew(paymentEnquiryResult.getPanFour(), mContext));
                        }
                    }
                } else {
                    idPaidvia.setText(AndroidUtils.getMasCardNumberTitle(paymentEnquiryResult.getPanFour(), mContext));
                }
            } else {

            }
        }
        updateDiscountUI(paymentEnquiryResult);
        updateGradeUI(paymentEnquiryResult, trxCurrency);
        updateGiftImageIcon(paymentEnquiryResult);


    }

    private void updateGiftImageIcon(OrderQueryEntity paymentEnquiryResult) {
        if (!TextUtils.isEmpty(paymentEnquiryResult.getGiftActivity())) {
            if (paymentEnquiryResult.getGiftActivity().equals("1")) {
                //派利是图标 区分
                idMerchantHead.setImageResource(paymentEnquiryResult.getShowType().equals("+") ? R.mipmap.icon_transactions_redpacket_collect : R.mipmap.icon_transactions_redpacket_payment);
                idLeftRemark.setText(getResources().getString(R.string.RP1_6_2_1));
            }
        }

        if (paymentEnquiryResult.isWeChatPay()) {
            idMerchantHead.setImageResource(R.mipmap.img_icon_store_wechatpay);
        }
    }

    private void updateGradeUI(OrderQueryEntity paymentEnquiryResult, String trxCurrency) {
        //积分抵扣 低分抵扣的详情页 要动态显示 积分抵扣信息 和积分抵扣失败的message
        if (!TextUtils.isEmpty(paymentEnquiryResult.getGpDetail())) {
            if (paymentEnquiryResult.getGpDetail().equals("1")) {
                mId_reference_name.setText(getString(R.string.CP3_3b_5));
                //App判断是否展示积分换领详情页   1:是 0：否
                idPayto.setText(paymentEnquiryResult.getGpName());
                idRelReduceMoney.setVisibility(View.VISIBLE);
                idRelReduceGrade.setVisibility(View.VISIBLE);
                if (TextUtils.isEmpty(paymentEnquiryResult.getOutamtsign())) {
                    id_reducemoney_currency.setText(paymentEnquiryResult.getShowType() + AndroidUtils.getTrxCurrency(trxCurrency));
                    idReduceMoney.setText(BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemAmt(), 2));
                } else {
                    id_reducemoney_currency.setText(paymentEnquiryResult.getOutamtsign() + AndroidUtils.getTrxCurrency(trxCurrency));
                    idReduceMoney.setText(BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getRedeemAmt(), 2));
                }

                if (!TextUtils.isEmpty(paymentEnquiryResult.getRedeemAmt()) && Double.valueOf(paymentEnquiryResult.getRedeemAmt()) == 0) {
                    idReduceMoney.setTextColor(getResources().getColor(R.color.black));
                    id_reducemoney_currency.setTextColor(getResources().getColor(R.color.black));
                } else {
                    idReduceMoney.setTextColor(getResources().getColor(R.color.color_green_one));
                    id_reducemoney_currency.setTextColor(getResources().getColor(R.color.color_green_one));
                }
                idReduceGrade.setText(AndroidUtils.formatPrice(Double.valueOf(paymentEnquiryResult.getRedeemCount()), false));
                idMerchantHead.setImageResource(R.mipmap.icon_transactions_points);
                //积分抵扣详情页 不现实金额
                mId_money_layout.setVisibility(View.GONE);
                mId_view_hint.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(paymentEnquiryResult.getRedeemStatus()) && !paymentEnquiryResult.getRedeemStatus().equals("1")) {
                    //积分抵扣失败 需要显示原因
                    mId_jifen_status_error.setVisibility(View.VISIBLE);
                    mId_jifen_status_error.setText(paymentEnquiryResult.getRedeemStatusMsg());
                }
                if (!TextUtils.isEmpty(paymentEnquiryResult.getRedeemStatus()) && paymentEnquiryResult.getRedeemStatus().equals("2")) {
                    idPaymentStatus.setTextColor(Color.parseColor("#DE0138"));
                }
            } else {
                mId_reference_name.setText(getString(R.string.CP1_4a_2));
            }
        } else {
            mId_reference_name.setText(getString(R.string.CP1_4a_2));
        }
    }

    private void updateDiscountUI(OrderQueryEntity paymentEnquiryResult) {
        //满减活动 显示折扣
        if (!TextUtils.isEmpty(paymentEnquiryResult.getDiscount())) {
            //优惠信息
            try {
                if (Double.valueOf(paymentEnquiryResult.getDiscount()) > 0) {
                    id_rel_discount.setVisibility(View.VISIBLE);
                    id_tv_discount.setText(AndroidUtils.getTrxCurrency(paymentEnquiryResult.getCur()) + " " + BigDecimalFormatUtils.forMatWithDigs(paymentEnquiryResult.getOriginalAmt(), 2));
                    id_tv_discount.getPaint().setAntiAlias(true);
                    id_tv_discount.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                }
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_order_detail;
    }
}
