package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.GradeListSelAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 积分查询列表弹框 列表查询
 */
public class CheckAndSelGradeListPop extends BasePopWindow {
    private OnGradeWindowClickListener onGradeWindowClickListener;

    private TextView mId_grade_title;
    private ImageView mId_close_dialog;
    private TextView mTv_confirm;
    private androidx.recyclerview.widget.RecyclerView mId_recyclerView;
    private List<SmaGpInfoBean> gradeInfos;
    private int selPos;
    private int changePos;

    private void bindViews() {
        mTv_confirm = (TextView) mContentView.findViewById(R.id.tv_confirm);
        mId_grade_title = (TextView) mContentView.findViewById(R.id.id_grade_title);
        mId_close_dialog = (ImageView) mContentView.findViewById(R.id.id_close_dialog);
        mId_recyclerView = (androidx.recyclerview.widget.RecyclerView) mContentView.findViewById(R.id.id_recyclerView);
    }


    public CheckAndSelGradeListPop(Activity mActivity, List<SmaGpInfoBean> gradeInfosIn, int selPosIn, OnGradeWindowClickListener onGradeWindowClickListenerIn) {
        super(mActivity);
        this.onGradeWindowClickListener = onGradeWindowClickListenerIn;
        this.gradeInfos = gradeInfosIn;
        this.selPos = selPosIn;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_grade_sel_list);
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.9));
        setHeight((int) (AndroidUtils.getScreenHeight(mActivity) * 0.6));
        bindViews();
        mId_grade_title.setText(mActivity.getString(R.string.EC05_10));
        initSelPosition(selPos);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerView.setLayoutManager(layoutManager);
        mId_recyclerView.setAdapter(getAdapter(null));
        ((IAdapter<SmaGpInfoBean>) mId_recyclerView.getAdapter()).setData(gradeInfos);
        mId_recyclerView.getAdapter().notifyDataSetChanged();
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mTv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGradeWindowClickListener.onGradeItemClickListener(changePos);
                dismiss();
            }
        });
    }

    private void initSelPosition(int selPos) {
        for (int i = 0; i < gradeInfos.size(); i++) {
            if (selPos == i) {
                gradeInfos.get(i).setSel(true);
            } else {
                gradeInfos.get(i).setSel(false);
            }
        }
    }

    private CommonRcvAdapter<SmaGpInfoBean> getAdapter(final List<SmaGpInfoBean> data) {
        return new CommonRcvAdapter<SmaGpInfoBean>(data) {

            @Override
            public Object getItemType(SmaGpInfoBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new GradeListSelAdapter(mActivity, new GradeListSelAdapter.OnItemSelListener() {
                    @Override
                    public void onItemChecked(int position) {
                        changePos = position;
                        initSelPosition(position);
                        mId_recyclerView.getAdapter().notifyDataSetChanged();
                    }
                }, true);
            }
        };
    }

    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, Gravity.CENTER, Gravity.CENTER);
    }

    public interface OnGradeWindowClickListener {
        void onGradeItemClickListener(int position);
    }
}
