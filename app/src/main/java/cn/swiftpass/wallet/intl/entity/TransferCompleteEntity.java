package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/8/20.
 * 转账完成，不代表成功，是否转账成功通过查询接口
 */

public class TransferCompleteEntity extends BaseEntity {
    //{"acNo":"8527","drAmt":"36.96","postscript":"ghjjiuyghn","crDisplayedEngNm":"CHLOE C***","srvcChrgDrCur":"HKD","collectionAccount":"86-13923740529"}
//    {"scrRefNo":"T090619160000187","acNo":"2089","txnId":"12180906F001324257","drAmt":"300.00","crDisplayedEngNm":"CHAN Y*","srvcChrgDrCur":"HKD$"}
    //内部订单号
    private String scrRefNo;
    private String acNo;
    //红包tips
    private String bocpayRedPackeNotifyTip;

    public String getBocpayRedPackeNotifyTip() {
        return bocpayRedPackeNotifyTip;
    }

    public void setBocpayRedPackeNotifyTip(String bocpayRedPackeNotifyTip) {
        this.bocpayRedPackeNotifyTip = bocpayRedPackeNotifyTip;
    }

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    private String appLink;

    /**
     * app call app 支持h5 H5 APP
     */
    private String fromType;

    public boolean isAppCallAppFromH5() {
        if (TextUtils.isEmpty(fromType)) {
            return false;
        }
        return TextUtils.equals(fromType, "H5");
    }


    public String getCollectionAccount() {
        return collectionAccount;
    }

    public void setCollectionAccount(String collectionAccount) {
        this.collectionAccount = collectionAccount;
    }

    public String getSmartAcLevel() {
        return smartAcLevel;
    }

    public void setSmartAcLevel(String smartAcLevel) {
        this.smartAcLevel = smartAcLevel;
    }

    /**
     * 我的账户级别 1：S1  2:S2  3:S3
     */
    private String smartAcLevel;

    private String collectionAccount;
    private String drAmt;
    private String crDisplayedEngNm;
    private String srvcChrgDrCur;

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    private String postscript;

    //付款类别 0：个人，1：商户
    private String paymentCategory;

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String getBillReference() {
        return billReference;
    }

    public void setBillReference(String billReference) {
        this.billReference = billReference;
    }

    private String billReference;


    public String getAcNo() {
        return acNo;
    }

    public void setAcNo(String acNo) {
        this.acNo = acNo;
    }

    public String getDrAmt() {
        return drAmt;
    }

    public void setDrAmt(String drAmt) {
        this.drAmt = drAmt;
    }

    public String getCrDisplayedEngNm() {
        return crDisplayedEngNm;
    }

    public void setCrDisplayedEngNm(String crDisplayedEngNm) {
        this.crDisplayedEngNm = crDisplayedEngNm;
    }

    public String getSrvcChrgDrCur() {
        return srvcChrgDrCur;
    }

    public void setSrvcChrgDrCur(String srvcChrgDrCur) {
        this.srvcChrgDrCur = srvcChrgDrCur;
    }

    //交易唯一标记
    private String txnId;

    public String getScrRefNo() {
        return scrRefNo;
    }

    public void setScrRefNo(String scrRefNo) {
        this.scrRefNo = scrRefNo;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    private String transferDate;

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    public String getTransferBank() {
        return transferBank;
    }

    public void setTransferBank(String transferBank) {
        this.transferBank = transferBank;
    }

    public String getDbtrNm() {
        return dbtrNm;
    }

    public void setDbtrNm(String dbtrNm) {
        this.dbtrNm = dbtrNm;
    }

    private String transferBank;
    private String dbtrNm;

    public String getExtCmt() {
        return extCmt;
    }

    public void setExtCmt(String extCmt) {
        this.extCmt = extCmt;
    }

    private String extCmt;

    public String getPreBank() {
        return preBank;
    }

    public void setPreBank(String preBank) {
        this.preBank = preBank;
    }

    private String preBank;//是否是预设收款银行、机构  1：是 0：否

    public boolean isLiShi() {
        return isLiShi;
    }

    public void setLiShi(boolean liShi) {
        isLiShi = liShi;
    }

    public boolean isLiShi;
}
