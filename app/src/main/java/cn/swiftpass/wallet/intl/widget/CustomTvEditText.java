package cn.swiftpass.wallet.intl.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


/**
 * 上边标题 下边输入框 输入框底部线条根据焦点变色 输入框末尾可添加按钮
 */

public class CustomTvEditText extends RelativeLayout {


    private Context mContext;
    //标题
    private TextView mTitleView;


    //输入框
    private EditText mEditText;
    //输入框右侧的imageview
    private LinearLayout mRightImg;

    private View lineView;
    private LinearLayout mLl_Image_layout;

    private int mTopTitleHintColor;
    private int mTopTitleFocusColor;
    private int mInputHintColor;
    private int mInputColor;
    private int mInputLineHintColor;
    private int mInputLineFocusColor;
    private int mInputMaxLength;
    private boolean mShowRightImage;
    private int mRightImage;
    private boolean mShowBottomLine;
    private LinearLayout ll_EditTextWithDel;
    private String digits;

    public void setOnEditFocusChangeListener(OnEditFocusChangeListener onEditFocusChangeListener) {
        this.onEditFocusChangeListener = onEditFocusChangeListener;
    }

    private OnEditFocusChangeListener onEditFocusChangeListener;

    private OnEditTextChangeListener onEditTextChangeListener;

    public OnItemClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    private OnItemClickListener onClickListener;

    public void setOnEditTextChangeListener(OnEditTextChangeListener onEditTextChangeListener) {
        this.onEditTextChangeListener = onEditTextChangeListener;
    }

    public CustomTvEditText(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public CustomTvEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }


    public CustomTvEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    @SuppressLint("WrongViewCast")
    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_tv_edit_view, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLl_Image_layout = rootView.findViewById(R.id.llayout_edit_text);
        mTitleView = rootView.findViewById(R.id.id_title_tv);
        mEditText = rootView.findViewById(R.id.edit_text);
        mRightImg = rootView.findViewById(R.id.ll_del);
        lineView = rootView.findViewById(R.id.id_buttom_line);
        int inputType = EditorInfo.TYPE_NULL;
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.CustomTvEditTextView, defStyle, 0);
        String titleText = a.getString(R.styleable.CustomTvEditTextView_topTitle);
        String hintText = a.getString(R.styleable.CustomTvEditTextView_hintTitle);
        mTopTitleHintColor = a.getColor(R.styleable.CustomTvEditTextView_topTitleHintColor, Color.WHITE);
        mTopTitleFocusColor = a.getColor(R.styleable.CustomTvEditTextView_topTitleFocusColor, Color.WHITE);
        mInputHintColor = a.getColor(R.styleable.CustomTvEditTextView_inputHintColor, Color.WHITE);
        mInputColor = a.getColor(R.styleable.CustomTvEditTextView_inputColor, Color.WHITE);
        mInputLineHintColor = a.getColor(R.styleable.CustomTvEditTextView_inputLineHintColor, Color.WHITE);
        mInputLineFocusColor = a.getColor(R.styleable.CustomTvEditTextView_inputLineFocusColor, Color.WHITE);
        inputType = a.getInt(R.styleable.CustomTvEditTextView_inputTypeItem, 0);
        digits = a.getString(R.styleable.CustomTvEditTextView_digits);
        mInputMaxLength = a.getInt(R.styleable.CustomTvEditTextView_inputMaxLength, 0);
        mShowRightImage = a.getBoolean(R.styleable.CustomTvEditTextView_showRightImage, true);
        mRightImage = a.getResourceId(R.styleable.CustomTvEditTextView_rightImage, R.mipmap.icon_type_deletexhdpi);
        mShowBottomLine = a.getBoolean(R.styleable.CustomTvEditTextView_showLine, true);
        mTitleView.setText(titleText);
        mTitleView.setTextColor(mTopTitleHintColor);
        mEditText.setHintTextColor(mInputHintColor);
        mEditText.setTextColor(mInputColor);
        mEditText.setHint(hintText);
        mRightImg.setVisibility(GONE);
        mRightImg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditText.setText("");
            }
        });
        if (mInputMaxLength > 0) {
            mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(mInputMaxLength)});
        }
        lineView.setBackgroundColor(mInputLineHintColor);
        lineView.setVisibility(mShowBottomLine ? VISIBLE : INVISIBLE);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (null != onEditTextChangeListener) {
                    onEditTextChangeListener.beforeTextChanged(charSequence, i, i1, i2);
                }
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (null != onEditTextChangeListener) {
                    onEditTextChangeListener.onTextChanged(charSequence, i, i1, i2);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mShowRightImage) {
                    mRightImg.setVisibility(mEditText.getText().toString().length() > 0 ? VISIBLE : INVISIBLE);
                }
                if (null != onEditTextChangeListener) {
                    onEditTextChangeListener.afterTextChanged(editable);
                }
            }
        });
        mEditText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onItemClickListener();
                }
            }
        });
        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (onEditFocusChangeListener != null) {
                    onEditFocusChangeListener.onFocusChanged(b);
                }
                changeFocusUiStatus(b);
            }
        });
        if (inputType != EditorInfo.TYPE_NULL) {
            mEditText.setInputType(inputType);
        }

        if (!TextUtils.isEmpty(digits)){
            mEditText.setKeyListener(DigitsKeyListener.getInstance(digits));
        }
    }

    public void changeFocusUiStatus(boolean b) {
        //焦点切换的时候 标题 下划线颜色变化
        if (b) {
            lineView.setBackgroundColor(mInputLineFocusColor);
            mTitleView.setTextColor(mTopTitleFocusColor);
            if (mEditText.getText().toString().length() > 0 && mShowRightImage) {
                mRightImg.setVisibility(VISIBLE);
            }
        } else {
            lineView.setBackgroundColor(mInputLineHintColor);
            mTitleView.setTextColor(mTopTitleHintColor);
            mRightImg.setVisibility(INVISIBLE);
        }
    }

    public String getInputText() {

        if (mEditText!=null&&mEditText.getText()!=null){
            return mEditText.getText().toString().trim();
        }else {
            return "";
        }


    }

    /**
     * 设置TitleView是否可见
     * 默认可见
     */
    public void setTitleViewVisisable(boolean isTitleVisiable) {
        if (null == mTitleView) {
            return;
        }
        if (isTitleVisiable) {
            mTitleView.setVisibility(View.VISIBLE);
        } else {
            mTitleView.setVisibility(View.GONE);
        }
    }

    /**
     * 设置为输入框加入其它的imageView
     *
     * @param id image的ID，用来显示图片
     */
    public ImageView addRightImageView(int id, boolean hasLeftPadding, int dimension, OnClickListener mListener) {
        ImageView iv = getImageView(id);
        int leftPadding = 0;
        if (hasLeftPadding) {
            int spaceHor = mContext.getResources().getDimensionPixelSize(dimension);
            leftPadding = spaceHor;
        }
        iv.setPadding(leftPadding, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
        mLl_Image_layout.addView(iv, layoutParams);
        //用来设置新增的图标按钮点击的监听，点击之后回调
        if (mListener != null) {
            iv.setOnClickListener(mListener);
        }
        return iv;
    }

    public void showPasswordText() {
        mEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    public void hidePasswordText() {
        mEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    private ImageView getImageView(int id) {
        ImageView iv = (ImageView) LayoutInflater.from(mContext).inflate(R.layout.title_imageview, null);
        iv.setImageResource(id);
        return iv;
    }

    public void setContentText(String text) {
        if (mEditText != null) {
            mEditText.setText(text);
        }
    }

    /**
     * 焦点变化向外传递
     */
    interface OnEditFocusChangeListener {
        void onFocusChanged(boolean isFocus);
    }

    public interface OnItemClickListener {
        void onItemClickListener();
    }


    public interface OnEditTextChangeListener {
        void beforeTextChanged(CharSequence s, int start, int count, int after);

        void onTextChanged(CharSequence s, int start, int count, int after);

        void afterTextChanged(Editable editable);
    }

    public void setInputType(int type) {
        //InputType.TYPE_CLASS_NUMBER
        mEditText.setInputType(type);
    }

    public EditText getEditText() {
        return mEditText;
    }
}
