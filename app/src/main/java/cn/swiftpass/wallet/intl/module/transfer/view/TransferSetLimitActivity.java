package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.StaticCodeEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.ecoupon.view.AmountView;
import cn.swiftpass.wallet.intl.module.transfer.entity.TransferSetLimitEntity;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferSetLimitPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.module.transfer.view.TransferByStaticCodeFragment.DATA_SET_AMOUNT_LIMIT;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferByStaticCodeFragment.DATA_SET_LIMIT;


public class TransferSetLimitActivity extends BaseCompatActivity<BasePresenter> {

    @BindView(R.id.ed_transfer_limit_amount)
    EditTextWithDel edTransferLimitAmount;
    @BindView(R.id.tv_transfer_limit_daily)
    TextView tvTransferLimitDaily;
    @BindView(R.id.ed_transfer_message)
    ClearEditText edTransferMessage;
    @BindView(R.id.tv_amount_of_message)
    TextView tvAmountOfMessage;
    @BindView(R.id.sw_transfer_by_aa)
    Switch swTransferByAa;
    @BindView(R.id.id_switch_view)
    AmountView idSwitchView;
    @BindView(R.id.tv_transfer_limit_aa)
    TextView tvTransferLimitAa;
    @BindView(R.id.ll_transfer_by_aa)
    LinearLayout llTransferByAa;
    @BindView(R.id.tv_transfer_commit)
    TextView tvTransferCommit;
    @BindView(R.id.id_available_amount)
    TextView tvAvailableAmount;
    private TransferSetLimitEntity limitEntity = new TransferSetLimitEntity();

    private boolean canCommit = true;
    //是否超过收款限额
    private boolean overLimitAmount = false;
    private String extraLitmit;

    public static final String PAGEFROM = "PAGEFROM";
    public static final String PAGEMONEY = "PAGEMONEY";
    public static final String PAGE_COLLECTION = "PAGE_COLLECTION";
    public static final String PAGE_STATICCODE = "PAGE_STATICCODE";

    private String currentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void init() {
        if (getIntent() != null) {
            Serializable extra = getIntent().getSerializableExtra(DATA_SET_LIMIT);
            extraLitmit = getIntent().getStringExtra(DATA_SET_AMOUNT_LIMIT);
            currentType = getIntent().getStringExtra(PAGEFROM);
            if (extra != null && extra instanceof TransferSetLimitEntity) {
                limitEntity = (TransferSetLimitEntity) extra;
                String value = String.valueOf(limitEntity.totalAmount);
                String valueAA = String.valueOf(limitEntity.aaAmount);
                edTransferLimitAmount.getEditText().setText(AndroidUtils.subZeroAndDot(value));
//                tvTransferLimitAa.setText(AndroidUtils.subZeroAndDot(valueAA));
                tvTransferLimitAa.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(AndroidUtils.subZeroAndDot(valueAA)), true));
                idSwitchView.setmCurrentCount(limitEntity.aaCount);
            } else if (TextUtils.equals(currentType, PAGE_COLLECTION)) {
                String totlaMoney = getIntent().getExtras().getString(PAGEMONEY);
                edTransferLimitAmount.getEditText().setText(totlaMoney);
                idSwitchView.setmCurrentCount(2);
                limitEntity.aaCount = 2;
                updateAAamount();
                limitEntity.isAAChecked = true;
            }
        }
        setToolBarTitle(R.string.CO1_2a_5);
        if (!TextUtils.isEmpty(extraLitmit)) {
            //收款限额处理
            tvAvailableAmount.setVisibility(View.VISIBLE);
            tvAvailableAmount.setText(getString(R.string.CO1_3a_3) + " " + "HKD" + " " + AndroidUtils.formatPriceWithPoint(Double.valueOf(extraLitmit), false));
        }
        canCommit = Double.parseDouble(limitEntity.totalAmount) > 0 && (!limitEntity.isAAChecked || ((limitEntity.aaCount > 1) && Double.parseDouble(limitEntity.aaAmount) > 0));
        tvTransferCommit.setEnabled(canCommit);
        llTransferByAa.setVisibility(limitEntity.isAAChecked ? View.VISIBLE : View.GONE);
        edTransferLimitAmount.getEditText().setFilters(new InputFilter[]{new AmountLimitFilter(9, 2)});

        edTransferLimitAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateAmountLimitStatus();
                updateAAamount();
                updateCommitBtn();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (!TextUtils.isEmpty(limitEntity.msg)) {
            edTransferMessage.setText(limitEntity.msg);
            tvAmountOfMessage.setText(limitEntity.msg.length() + "/50");
        } else {
            tvAmountOfMessage.setText("0/50");
        }
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contentEquals("\n")) {
                    return "";
                }
                Pattern p = Pattern.compile("^[a-zA-Z\\u4e00-\\u9fa5\\d\\n\\/\\(\\)\\.\\,\\'\\+\\-\\?\\: ]*$");
                Matcher m = p.matcher(source.toString());
                if (m.find()) {
                    return null;
                } else {
                    return "";
                }
            }
        };
        edTransferMessage.setFilters(new InputFilter[]{new InputFilter.LengthFilter(50), filter});
        edTransferMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null) {
                    tvAmountOfMessage.setText(s.length() + "/50");
                } else {
                    tvAmountOfMessage.setText("0/50");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                limitEntity.msg = s.toString();
            }
        });

        edTransferMessage.setTextColor(Color.BLACK);
        swTransferByAa.setChecked(limitEntity.isAAChecked);
        swTransferByAa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                limitEntity.isAAChecked = isChecked;
                updateCommitBtnWithStatus();
                updateAAamount();
                llTransferByAa.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            }
        });

        idSwitchView.setChangeListener(new AmountView.OnCountChangeListener() {
            @Override
            public void onCountChanged(int count) {
                //这2个方法的顺序不能调整
                updateAAamount();
                updateCommitBtn();
            }
        });
    }

    //判断收款限额
    private void updateAmountLimitStatus() {
        if (TextUtils.isEmpty(extraLitmit)) return;
        String text = edTransferLimitAmount.getText();
        if (text.endsWith(".")) {
            text = text.replace(".", "");
        }
        //此处需要加一个是否为空判断  如果为空  则默认金额为0
        if (TextUtils.isEmpty(text)) {
            text = "0.00";
        }
        try {
            double currentV = 0;
            try {
                currentV = Double.valueOf(text);
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }

            double maxValue = 0;
            try {
                maxValue = Double.valueOf(extraLitmit);
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            //当前金额是否超过最大限额
            if (currentV <= maxValue) {
                tvAvailableAmount.setText(getString(R.string.CO1_3a_3) + " " + "HKD" + " " + extraLitmit);
                tvAvailableAmount.setTextColor(getResources().getColor(R.color.font_gray_four));
                overLimitAmount = false;
            } else {
                tvAvailableAmount.setText(getResources().getString(R.string.CO1_3a_3a));
                tvAvailableAmount.setTextColor(Color.RED);
                overLimitAmount = true;
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
    }

    private void updateAAamount() {
        String text = edTransferLimitAmount.getText();
        if (text.endsWith(".")) {
            text = text.replace(".", "");
        }
        try {
            BigDecimal amount = new BigDecimal(text);
            limitEntity.totalAmount = amount.toString();
            BigDecimal person = new BigDecimal(idSwitchView.getCurrentCount());
            if (amount.compareTo(BigDecimal.ZERO) > 0 && person.compareTo(BigDecimal.ZERO) > 0) {
                BigDecimal divide = amount.divide(person, 2, BigDecimal.ROUND_HALF_UP);
                String result = divide.toString();
//                tvTransferLimitAa.setText(result);
                tvTransferLimitAa.setText(AndroidUtils.formatPriceWithPoint(Double.parseDouble(result), true));
                limitEntity.aaAmount = result;

            } else {
                tvTransferLimitAa.setText("0.00");
                limitEntity.aaAmount = "0.00";
            }

        } catch (Exception e) {
            tvTransferLimitAa.setText("0.00");
            limitEntity.aaAmount = "0.00";
            limitEntity.totalAmount = "0";
        }
    }

    private void updateCommitBtnWithStatus() {
        if (limitEntity.isAAChecked) {
            //关闭再打开 最小值为2
            if (idSwitchView.getCurrentCount() < 2) {
                idSwitchView.setmCurrentCount(2);
            }
        }
        canCommit = !overLimitAmount && (Double.parseDouble(limitEntity.totalAmount) > 0 && (!limitEntity.isAAChecked || ((idSwitchView.getCurrentCount() > 1) && Double.parseDouble(limitEntity.aaAmount) > 0)));
        tvTransferCommit.setEnabled(canCommit);
    }

    private void updateCommitBtn() {
        canCommit = !overLimitAmount && (Double.parseDouble(limitEntity.totalAmount) > 0 && (!limitEntity.isAAChecked || ((idSwitchView.getCurrentCount() > 1) && Double.parseDouble(limitEntity.aaAmount) > 0)));
        tvTransferCommit.setEnabled(canCommit);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_set_limits;
    }

    @Override
    protected BasePresenter createPresenter() {
        return new TransferSetLimitPresenter();
    }

    @OnClick(R.id.tv_transfer_commit)
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_transfer_commit:
                if (limitEntity.isAAChecked) {
                    limitEntity.aaCount = idSwitchView.getCurrentCount();
                } else {
                    limitEntity.aaCount = 0;
                }
                if (TextUtils.equals(currentType, PAGE_COLLECTION)) {
                    ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
                    EventBus.getDefault().postSticky(new StaticCodeEventEntity(limitEntity));
                    //如果是转账成功 或者主扫成功 点击AA收款流程过来 这里应该要清楚appcallapp流程
                    AppCallAppUtils.clearAppCallAppEvent();
                } else if (TextUtils.equals(currentType, PAGE_STATICCODE)) {
                    setResult(RESULT_OK, new Intent().putExtra(DATA_SET_LIMIT, limitEntity));
                }
                finish();
                break;
            default:
                break;
        }

    }


}