package cn.swiftpass.wallet.intl.module.transfer.view;

import android.text.InputFilter;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.view
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/12 14:21
 * @change
 * @chang time
 * @class describe
 */
public class AmountLimitFilter implements InputFilter {
    Pattern mPattern;
    private  int MAX_LIMIT_LENGTH = 9;


    public AmountLimitFilter(int lengthBeforeDot, int lengthAfterDot) {
        String pattern = "((^0|^[1-9]\\d{0," + (lengthBeforeDot-1) + "})(\\.\\d{0," + lengthAfterDot + "})?)?$";//小数
        MAX_LIMIT_LENGTH=lengthBeforeDot+1+lengthAfterDot;
        mPattern = Pattern.compile(pattern);

    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {

        String formatedSource = source.subSequence(start, end).toString();
        String destPrefix = dest.subSequence(0, dstart).toString();
        String destSuffix = dest.subSequence(dend, dest.length()).toString();
        //算出变化后的值:
        String result = destPrefix + formatedSource + destSuffix;
        Matcher matcher = mPattern.matcher(result);
        if (!matcher.matches()) {
            String old = dest.subSequence(dstart, dend).toString();
            return old;//过滤,返回被替换前的原始值
        }
        if (result.length() >MAX_LIMIT_LENGTH) {
            return "";
        }
        return null;//不过滤
    }
}
