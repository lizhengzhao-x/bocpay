package cn.swiftpass.wallet.intl.module.scan.majorscan.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.protocol.ActionTrxGpInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetPayTypeListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderWithPointProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ParserTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTradeStatusProtocol;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.InputMoneyContract;

public class InputMoneyPresenter extends BasePresenter<InputMoneyContract.View> implements InputMoneyContract.Presenter {


    @Override
    public void actionMpqrPaymentWithPoint(MainScanOrderRequestEntity orderRequest) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new MainScanOrderWithPointProtocol(orderRequest, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().actionMpqrPaymentWithPointFail(errorCode,errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView()!=null){
                    //因为接下来还要调起getPaymentResult方法 ，不应该隐藏loading
//                    getView().dismissDialog();
                    getView().actionMpqrPaymentWithPointSuccess(response);
                }

            }
        }).execute();
    }

    @Override
    public void getPaymentResult(String tansId,String pandId, String qrcodeStr ) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new QueryTradeStatusProtocol(pandId, qrcodeStr, tansId, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getPaymentResultFail(errorCode,errorMsg);
                }
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
              if (getView()!=null){
                   getView().dismissDialog();
                   getView().getPaymentResultSuccess(response);
               }
            }
        }).execute();
    }

    @Override
    public void getMainScanOrder(MainScanOrderRequestEntity orderRequest) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new MainScanOrderProtocol(orderRequest, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getMainScanOrderFail(errorCode,errorMsg);
                }

            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView()!=null){
                    //因为接下来还要调起getPaymentResult方法 ，不应该隐藏loading
//                    getView().dismissDialog();
                    getView().getMainScanOrderSuccess(response);
                }
            }
        }).execute();
    }

    /**
     * 提交生成订单查询是否有积分
     */
    @Override
    public void getActionTrxGpInfo(MpQrUrlInfoResult urlQRInfoEntity) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new ActionTrxGpInfoProtocol(urlQRInfoEntity, new NetWorkCallbackListener<ActionTrxGpInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoFail(errorCode,errorMsg);
                }
            }

            @Override
            public void onSuccess(ActionTrxGpInfoEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getPaymentTypeList(String qrcode, boolean isUrlMode) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new GetPayTypeListProtocol(qrcode, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getPaymentTypeListFail(errorCode,errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().getPaymentTypeListSuccess(response,isUrlMode);
                }
            }
        }).execute();
    }


    /**
     * 转账 拉取转账信息
     *
     * @param code
     */
    @Override
    public void parserTransferCode(String qrCode) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new ParserTransferProtocol(qrCode, new NetWorkCallbackListener<ParserQrcodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().parserTransferCodeFail(errorCode,  errorMsg);
                }

            }

            @Override
            public void onSuccess(ParserQrcodeEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().parserTransferCodeSuccess(response);
                }
            }
        }).execute();
    }
}
