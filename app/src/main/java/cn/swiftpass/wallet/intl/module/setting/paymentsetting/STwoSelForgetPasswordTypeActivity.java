package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.entity.event.ForgetPwdEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterBankActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterPaymentAccountDescActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardIdCardVerificationActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 登录态忘记密码
 */
public class STwoSelForgetPasswordTypeActivity extends BaseCompatActivity {

    private TextView mTv_type_title;
    /**
     * 信用卡
     */
    private androidx.cardview.widget.CardView mId_link_credit_card;
    /**
     * 支付账户
     */
    private androidx.cardview.widget.CardView mId_payment_account;
    /**
     * 智能账户
     */
    private androidx.cardview.widget.CardView mId_smart_account;
    /**
     * 虚拟账户
     */
    private androidx.cardview.widget.CardView mId_virtual_account;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_identity_information;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_credit_card;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_smart_card;
    private cn.swiftpass.wallet.intl.widget.LeftImageArrowView mIav_virtual_card;


    private SmartLevelEntity mAccountLevelInfo;

    private void bindViews() {
        mTv_type_title = (TextView) findViewById(R.id.tv_type_title);
        mId_link_credit_card = (androidx.cardview.widget.CardView) findViewById(R.id.id_link_credit_account);
        mId_payment_account = (androidx.cardview.widget.CardView) findViewById(R.id.id_link_payment_card);
        mId_smart_account = (androidx.cardview.widget.CardView) findViewById(R.id.id_link_smart_card);
        mId_virtual_account = (androidx.cardview.widget.CardView) findViewById(R.id.id_link_virtual_card);
        mIav_identity_information = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_identity_information);
        mIav_credit_card = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_credit_card);
        mIav_smart_card = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_smart_information);
        mIav_virtual_card = (cn.swiftpass.wallet.intl.widget.LeftImageArrowView) findViewById(R.id.iav_virtual_information);

        mIav_credit_card.setSubText(getString(R.string.VC07_03_3a));
        mIav_smart_card.setSubText(getString(R.string.VC07_03_4a));
        mIav_identity_information.setSubText(getString(R.string.IDV_3a_2_4));
        mIav_virtual_card.setSubText(getString(R.string.VC07_03_5a));
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

    }

    @Override
    public void init() {
        bindViews();
        setToolBarTitle(R.string.setting_payment_short);
        EventBus.getDefault().register(this);
        CommonRequestUtils.getServerErrorCode(this);
        mId_payment_account.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_FORGETPASSWORD_PA);
                mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
                ActivitySkipUtil.startAnotherActivity(STwoSelForgetPasswordTypeActivity.this, RegisterPaymentAccountDescActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }
        });
        mId_link_credit_card.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_FORGETPASSWORD);
                mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
                ActivitySkipUtil.startAnotherActivity(STwoSelForgetPasswordTypeActivity.this, InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        mId_virtual_account.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                HashMap<String, Object> mHashMapsVirtual = new HashMap<>();
                mHashMapsVirtual.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD);
                ActivitySkipUtil.startAnotherActivity(STwoSelForgetPasswordTypeActivity.this, VirtualCardIdCardVerificationActivity.class, mHashMapsVirtual, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        mId_smart_account.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                HashMap<String, Object> mHashMaps2 = new HashMap<>();
                mHashMaps2.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_FORGETPASSWORD);
                ActivitySkipUtil.startAnotherActivity(STwoSelForgetPasswordTypeActivity.this, RegisterBankActivity.class, mHashMaps2, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        getAccountLevel();
    }

    private void getAccountLevel() {
        mAccountLevelInfo = (SmartLevelEntity) getIntent().getExtras().getSerializable(Constants.SMART_LEVEL_INFO);
        controlShowPaymentPasswordTypes();
    }

    /**
     * 根据账户类型 判断显示哪一种账户找回密码方式
     */
    private void controlShowPaymentPasswordTypes() {
        String currentAccountLevel = mAccountLevelInfo.getSmartAccLevel();
        boolean isHasCreditCard = mAccountLevelInfo.isHasCredit();
        boolean isHasVirtualCard = mAccountLevelInfo.isHasVirtualCard();
        if (currentAccountLevel.equals(SmartLevelEntity.SMART_LEVEL_STW0)) {
            //支付账户
            mId_smart_account.setVisibility(View.GONE);
            mId_payment_account.setVisibility(View.VISIBLE);
        } else if (currentAccountLevel.equals(SmartLevelEntity.SMART_LEVEL_STHREE)) {
            //智能账户
            mId_payment_account.setVisibility(View.GONE);
            mId_smart_account.setVisibility(View.VISIBLE);
        }
        mId_link_credit_card.setVisibility(isHasCreditCard ? View.VISIBLE : View.GONE);

        mId_virtual_account.setVisibility(isHasVirtualCard ? View.VISIBLE : View.GONE);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ForgetPwdEventEntity event) {
        if (event.getEventType() == ForgetPwdEventEntity.EVENT_FORGET_PWD || event.getEventType() == ForgetPwdEventEntity.EVENT_FORGET_PWD_LOGIN
        ) {
            finish();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }


    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_stwo_sel_forgetpwd;
    }

}
