package cn.swiftpass.wallet.intl.module.staging.presenter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.QueryStagesInfoProtocol;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.StagingItemEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.staging.contract.StagingItemContract;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

public class StagingItemPresenter extends BasePresenter<StagingItemContract.View> implements StagingItemContract.Presenter {


    @Override
    public void queryStagesInfo(String key) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new QueryStagesInfoProtocol(key, new NetWorkCallbackListener<StagingItemEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryStagesInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(StagingItemEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().queryStagesInfoSuccess(response);
                }
            }
        }).execute();
    }

    public void dealWithHtmlShow(String content, TextView tv, Activity activity) {
        if (activity == null) return;
        tv.setText(Html.fromHtml(content));
        tv.setMovementMethod(LinkMovementMethod.getInstance());
        CharSequence text = tv.getText();
        if (text instanceof Spannable) {
            int end = text.length();
            Spannable sp = (Spannable) tv.getText();
            URLSpan[] urls = sp.getSpans(0, end, URLSpan.class);
            SpannableStringBuilder style = new SpannableStringBuilder(text);
            style.clearSpans();
            for (final URLSpan urlSpan : urls) {
                style.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View widget) {
                        String url = urlSpan.getURL();
                        LogUtils.i("URL-click:", url);
                        if (!TextUtils.isEmpty(url)) {
                            if (url.startsWith("http") || url.startsWith("HTTP")) {
                                //网页通过webview打开
                                HashMap<String, Object> mHashMaps = new HashMap<>();
                                mHashMaps.put(Constants.DETAIL_URL, url);
                                ActivitySkipUtil.startAnotherActivity(activity, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            } else if (url.startsWith("tel")) {
                                String  tel = url.replace("tel:", "");
                                //打电话方式打开
                                //检查拨打电话权限
                                if (!TextUtils.isEmpty(url) && ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                                    Intent intent = new Intent(Intent.ACTION_CALL);
                                    intent.setData(Uri.parse("tel:" + tel));
                                    activity.startActivity(intent);
                                } else {
                                    PermissionInstance.getInstance().getPermission(activity, new PermissionInstance.PermissionCallback() {
                                        @SuppressLint("MissingPermission")
                                        @Override
                                        public void acceptPermission() {
                                            Intent intent = new Intent(Intent.ACTION_CALL);
                                            intent.setData(Uri.parse("tel:" + tel));
                                            activity.startActivity(intent);
                                        }

                                        @Override
                                        public void rejectPermission() {

                                        }
                                    }, Manifest.permission.CALL_PHONE);
                                }

                            }
                        }
                    }

                    @Override
                    public void updateDrawState(TextPaint ds) {
                        super.updateDrawState(ds);
                        ds.setUnderlineText(true);
                        ds.setColor(Color.parseColor("#136EF1"));
                    }
                }, sp.getSpanStart(urlSpan), sp.getSpanEnd(urlSpan), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
            tv.setText(style);
        }
    }
}
