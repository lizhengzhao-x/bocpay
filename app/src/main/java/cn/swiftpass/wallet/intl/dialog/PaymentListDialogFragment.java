package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.cardmanagement.SelCardListAdapter;


public class PaymentListDialogFragment extends DialogFragment {


    private ImageView mId_back_image;
    private ImageView mId_more_image;
    private RecyclerView mRecycleView;
    private OnPaymentListClickListener onPaymentListClickListener;
    private ArrayList<BankCardEntity> cardEntities;

    private void bindViews(View view) {
        mId_back_image = (ImageView) view.findViewById(R.id.id_back_image);
        mId_more_image = (ImageView) view.findViewById(R.id.id_more_image);
        mRecycleView = (RecyclerView) view.findViewById(R.id.id_recyclerview);


        mRecycleView.setAdapter(getAdapter(null));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mRecycleView.setLayoutManager(layoutManager);

        mId_back_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPaymentListClickListener.onBackBtnClickListener();
                dismiss();
            }
        });
        mId_more_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPaymentListClickListener.onMoreBtnClickListener();
            }
        });
    }


    public void initParams(OnPaymentListClickListener onPopWindowOkClickListener, ArrayList<BankCardEntity> cardEntities, String selCardId, boolean isFps) {
        this.onPaymentListClickListener = onPopWindowOkClickListener;
        this.cardEntities = cardEntities;
        //收银台显示的为默认选中的
        for (int i = 0; i < cardEntities.size(); i++) {
            if (TextUtils.equals(selCardId, cardEntities.get(i).getCardId())) {
                cardEntities.get(i).setSel(true);
            } else {
                cardEntities.get(i).setSel(false);
            }
        }
    }

    private void updateUI() {
        ((IAdapter<BankCardEntity>) mRecycleView.getAdapter()).setData(cardEntities);
        mRecycleView.getAdapter().notifyDataSetChanged();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_paymentlist, container, false);
        bindViews(view);
        updateUI();
        return view;
    }


    private CommonRcvAdapter<BankCardEntity> getAdapter(List<BankCardEntity> data) {
        return new CommonRcvAdapter<BankCardEntity>(data) {

            @Override
            public Object getItemType(BankCardEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new SelCardListAdapter(getActivity(), new SelCardListAdapter.CardItemSelCallback() {

                    @Override
                    public void onItemCardSelClick(int position) {
                        super.onItemCardSelClick(position);
                        //切换卡支付
                        onPaymentListClickListener.onCardSelListener(position);
                        for (int i = 0; i < cardEntities.size(); i++) {
                            if (i == position) {
                                cardEntities.get(i).setSel(true);
                                cardEntities.get(i).setIsDefault("1");
                            } else {
                                cardEntities.get(i).setSel(false);
                                cardEntities.get(i).setIsDefault("0");
                            }
                        }
                        mRecycleView.getAdapter().notifyDataSetChanged();
                        dismiss();
                    }
                });
            }
        };
    }


    public interface OnPaymentListClickListener {
        void onBackBtnClickListener();

        void onMoreBtnClickListener();

        void onCardSelListener(int positon);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

}
