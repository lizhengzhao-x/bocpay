package cn.swiftpass.wallet.intl.module.scan.majorscan.presenter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.IOException;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.utils.encry.Base64;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.api.protocol.ActionTrxGpInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetPayTypeListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MainScanOrderProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ParserTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.QueryTradeStatusProtocol;
import cn.swiftpass.wallet.intl.api.protocol.ScanQrCodeProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpQRCodeUrlProtocol;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.QrcodeTypeEntity;
import cn.swiftpass.wallet.intl.entity.UpQrFpsEntity;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.MajorScanContract;
import cn.swiftpass.wallet.intl.utils.FileUtils;
import cn.swiftpass.wallet.intl.utils.ParseQRUtils;
import cn.swiftpass.wallet.intl.utils.ParseQrCodeAysnTask;

public class MajorScanPresenter extends BasePresenter<MajorScanContract.View> implements MajorScanContract.Presenter {

    private String mCurrentParseCode;

    @Override
    public void getUpMerchantURLQRInfo(final String urlQrCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new UpQRCodeUrlProtocol(urlQrCode, new NetWorkCallbackListener<MpQrUrlInfoResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parserUrlCodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MpQrUrlInfoResult entity) {
                if (getView() != null) {
                    entity.setQrcodeStr(urlQrCode);
                    getView().parserUrlCodeSuccess(entity);
                    getView().dismissDialog();
                }
            }
        }).execute();
    }

    @Override
    public void dealWithEmvCode(String content) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        MpQrUrlInfoResult mpQrUrlInfoResult = null;
        HashMap<String, String> resultMap = ParseQRUtils.parseEMV(content);
        if (resultMap == null) {
            if (getView() != null) {
                getView().dismissDialog();
                getView().dealWithEmvCodeError();
            }
            return;
        }
        if (resultMap.containsKey("15") || resultMap.containsKey("16")) {
            mpQrUrlInfoResult = new MpQrUrlInfoResult();
        } else {
            mpQrUrlInfoResult = new UpQrFpsEntity();
        }

        if (resultMap.containsKey("53")) {
            mpQrUrlInfoResult.setTranCur(resultMap.get("53"));
        }
        if (resultMap.containsKey("54")) {
            mpQrUrlInfoResult.setTranAmt(resultMap.get("54"));
        }
        if (resultMap.containsKey("58")) {
            mpQrUrlInfoResult.setCountry(resultMap.get("58"));
        }
        if (resultMap.containsKey("59")) {
            mpQrUrlInfoResult.setName(resultMap.get("59"));
        }
        if (resultMap.containsKey("60")) {
            mpQrUrlInfoResult.setCity(resultMap.get("60"));
        }
        if (resultMap.containsKey("63")) {
            mpQrUrlInfoResult.setPostal(String.valueOf(Base64.decode(resultMap.get("63"))));
        }
        mpQrUrlInfoResult.setQrcodeStr(content);
        mpQrUrlInfoResult.setQrcType("E");
        if (resultMap.containsKey("26") && !resultMap.containsKey("15") && !resultMap.containsKey("16")) {
            //fps二维码 转账
//            parseTransferCode(content, false);
            mCurrentParseCode = content;
            checkSmartAccountNewBind();
        } else {
            if (getView() != null) {
                getView().dismissDialog();
                getView().dealWithEmvCodeSuccess(mpQrUrlInfoResult);
            }
        }
    }

    @Override
    public void parseTransferCode(String qrCode, boolean showDialog) {
        if (showDialog) {
            if (getView() != null) {
                getView().showDialogNotCancel();
            }
        }
        new ParserTransferProtocol(qrCode, new NetWorkCallbackListener<ParserQrcodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parseTransferCodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ParserQrcodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parseTransferCodeSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void checkSmartAccountNewBind() {
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().parseTransferCodeError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                parseTransferCode(mCurrentParseCode, false);
            }
        }).execute();
    }

    @Override
    public void getActionTrxGpInfo(MpQrUrlInfoResult mpQrUrlInfoResult) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ActionTrxGpInfoProtocol(mpQrUrlInfoResult, new NetWorkCallbackListener<ActionTrxGpInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ActionTrxGpInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getActionTrxGpInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getPaymentTypeList(String qrcode, final boolean isUrlMode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetPayTypeListProtocol(qrcode, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaymentTypeListSuccess(response.getRows(), isUrlMode);
                }
            }
        }).execute();
    }

    @Override
    public void getPayMentResult(String cardId, String cpQrCode, String txnId) {
        new QueryTradeStatusProtocol(cardId, cpQrCode, txnId, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                getView().getPayMentResultError(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
                getView().getPayMentResultSuccess(response);
            }
        }).execute();
    }

    @Override
    public void getMainScanOrder(MainScanOrderRequestEntity orderRequest) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new MainScanOrderProtocol(orderRequest, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMainScanOrderSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void parserLocalQrcode(final Activity context, Uri photoUri) {
        if (getView() != null) {
            if (getView() != null) {
                getView().dismissDialog();
            }
            getView().showDialogNotCancel();
        }
        if (photoUri == null) {
            getView().parserLocalQrcodeSuccess(null);
        } else {
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), photoUri);
            } catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
            try {
                bitmap = FileUtils.getBitmapFormUri(context, photoUri);
            } catch (IOException e) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace();
                }
            }
            if (bitmap == null) {
                getView().parserLocalQrcodeSuccess(null);
                if (getView() != null) {
                    getView().dismissDialog();
                }
            } else {
                ParseQrCodeAysnTask.onParseResultListener listener = new ParseQrCodeAysnTask.onParseResultListener() {
                    @Override
                    public void onResult(String res) {
                        if (getView() != null) {
                            getView().parserLocalQrcodeSuccess(res);
                            getView().dismissDialog();
                        }
                    }
                };
                new ParseQrCodeAysnTask(bitmap, listener).execute();
            }
        }

    }

    @Override
    public void scanQrCodeType(String mpQrCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ScanQrCodeProtocol(mpQrCode, new NetWorkCallbackListener<QrcodeTypeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().scanQrCodeTypeFailed(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(QrcodeTypeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().scanQrCodeTypeSuccess(response);
                }
            }
        }).execute();
    }


}
