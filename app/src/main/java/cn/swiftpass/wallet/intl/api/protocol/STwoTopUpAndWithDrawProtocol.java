package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class STwoTopUpAndWithDrawProtocol extends BaseProtocol {
    public static final String TAG = STwoTopUpAndWithDrawProtocol.class.getSimpleName();
    /**
     * 支付密码
     */
    String trxAmount;
    /**
     * D 充值 W提现
     */
    String mAction;

    String referenceNo;

    public STwoTopUpAndWithDrawProtocol(String action, String trxAmount, String referenceNo, NetWorkCallbackListener dataCallback) {
        this.trxAmount = trxAmount;
        this.mAction = action;
        this.referenceNo = referenceNo;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccountManager/chongzhiOrtixian";
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(mAction)) {
            mBodyParams.put(RequestParams.ACTION, mAction);
        }
        if (!TextUtils.isEmpty(trxAmount)) {
            mBodyParams.put(RequestParams.TXNAMT, trxAmount);
        }
        if (!TextUtils.isEmpty(referenceNo)) {
            mBodyParams.put(RequestParams.REFERENCENO, referenceNo);
        }
    }

}
