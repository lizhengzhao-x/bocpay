package cn.swiftpass.wallet.intl.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.entity.Constants;

/**
 * Created by admin on 2017/12/19.
 *
 * @Package cn.swiftpass.wallet.intl.utils
 * @Description: (sharePerference的实现类)
 * @date 2017/12/19.17:47.
 */

public class SpUtils extends SpBaseUtil {
    private static Context applicationContext;

    @SuppressLint("StaticFieldLeak")
    private static final class InstanceHolder {
        private static final SpUtils INSTANCE = new SpUtils();
    }

    private SpUtils() {
        super(applicationContext);
        if (applicationContext == null) {
            throw new IllegalArgumentException("请先在application中调用init方法初始化！");
        }
    }

    /**
     * 先在application中初始化
     */
    public static void init(Context context) {
        applicationContext = context.getApplicationContext();
    }

    public static SpUtils getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public static SpUtils getInstance(Context context) {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected String getSpName() {
        return "sp_config";
    }


    public String getAppLanguage() {
        String language = (String) HttpCoreSpUtils.get(HttpCoreConstants.APP_SETTING_LANGUAGE, "");
        return language;
    }
//
//    public void setAppLanguage(String language) {
//        putString("App_setting_language", language);
//    }

    public void setTestE2ee(boolean e2ee) {
        putBoolean("App_test_e2ee", e2ee);
    }

    public boolean getTestE2ee() {
        return getBoolean("App_test_e2ee", BuildConfig.E2EE);

    }

    public void setTestIdv(boolean skipIdV) {
        putBoolean("App_test_IDV", skipIdV);
    }

    public boolean getTestIdv() {
        return getBoolean("App_test_IDV", BuildConfig.SKIP_IDV);
    }

    public String getTestUrl() {
        return getString("App_test_base_url", BuildConfig.BASE_URL);
    }

    public void setTestUrl(String url) {
        putString("App_test_base_url", url);
    }


    public String getAppVersionName() {
        String language = getString("App_Version_Name", "");
        return language;
    }


    /**
     * 有没有因为升级到1月版本 弹出过通知的弹框
     *
     * @return
     */
    public boolean hasShowNotificationDialog() {
        return getBoolean(Constants.IS_NEED_JUMP_NOTIFICATION, false);
    }

    public void saveShowNotificationDialog() {
        getEdit().putBoolean(Constants.IS_NEED_JUMP_NOTIFICATION, true).commit();
    }

    public void saveShowNotificationDialog(boolean status) {
        getEdit().putBoolean(Constants.IS_NEED_JUMP_NOTIFICATION, status).commit();
    }

    public String getPreLoginBackground() {
        return getString("PreLogin_Background", "");
    }

    public void setPreLoginBackground(String uri) {
        putString("PreLogin_Background", uri);
    }

    public String getPreLoginBgName() {
        return getString("PreLogin_BgName", "");
    }

    public void setPreLoginBgName(String uri) {
        putString("PreLogin_BgName", uri);
    }

    public String getMainBackground() {
        return getString("Main_Background", "");
    }

    public void setMainBackground(String uri) {
        putString("Main_Background", uri);
    }

    public String getMainBgName() {
        return getString("Main_BgName", "");
    }

    public void setMainBgName(String uri) {
        putString("Main_BgName", uri);
    }

    public boolean HasShowGuidePageStatus() {
        return getBoolean("HAS_SHOW_GUIDEPAGE", false);
    }

    public void saveShowGuidePage(Boolean status) {
        putBoolean("HAS_SHOW_GUIDEPAGE", status);
    }


    public String getBaseBackground() {
        return getString("Base_Main_Background", "");
    }

    public void setBaseBackground(String uri) {
        putString("Base_Main_Background", uri);
    }

    public String getBaseBgName() {
        return getString("Base_BgName", "");
    }

    public void setBaseBgName(String uri) {
        putString("Base_BgName", uri);
    }


    public String getPreLoginBgVersion() {
        return getString("PreLogin_Background_Version", "");
    }

    public void setPreLoginBgVersion(String version) {
        putString("PreLogin_Background_Version", version);
    }

    public String getMainBgVersion() {
        return getString("Main_Background_Version", "");
    }

    public void setMainBgVersion(String version) {
        putString("Main_Background_Version", version);
    }


    public void setAppVersionName(Context context) {
        PackageInfo pInfo = null;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException var3) {
            LogUtils.e("setAppVersionName", Log.getStackTraceString(var3));
        }
        String versionName = pInfo.versionName;
        putString("App_Version_Name", versionName);
    }


}
