package cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.widget.CustomeLineText;
import cn.swiftpass.wallet.intl.widget.GlideRoundTransform;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel.ItemModelBuilder.STYLE_BIG_TEXT;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel.ItemModelBuilder.STYLE_LINE;

public class OrderRecordShareViewAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<ItemModel> mList;

    public OrderRecordShareViewAdapter(Context mContext) {
        this.mContext = mContext;
        mList = new ArrayList<>();
    }


    @Override
    public int getItemViewType(int position) {
        ItemModel itemModel = mList.get(position);
        int itemType = itemModel.getItemType();
        return itemType;
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ItemType.HEAD) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.ordershare_order_title, parent, false);
            return new HeaderViewHolder(view);
        } else if (viewType == ItemType.IMAGE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.ordershare_order_image, parent, false);
            return new ImageViewHolder(view);
        } else if (viewType == ItemType.SPLITLINE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.ordershare_order_spit, parent, false);
            return new SplitViewHolder(view);
        } else {
            View view = View.inflate(mContext, R.layout.ordershare_item2, null);
            return new BodyViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        ItemModel itemModel = mList.get(position);
        boolean visibleLine = itemModel.isVisibleLine();

        switch (itemViewType) {
            case ItemType.HEAD:
                HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
                headerHolder.leftImage.setImageResource(itemModel.getResourceImg());
                if (itemModel.isShowHeader()) {
                    headerHolder.leftImage.setVisibility(View.VISIBLE);
                } else {
                    headerHolder.leftImage.setVisibility(View.INVISIBLE);
                }
                break;
            case ItemType.BODY:
            case ItemType.MONEY:
                BodyViewHolder bodyViewHolder = (BodyViewHolder) holder;
                if (itemModel.getItemContentSpan() != null) {
                    bodyViewHolder.clt.setContentText(itemModel.getItemContentSpan());
                } else {
                    bodyViewHolder.clt.setContentText(itemModel.getItemContent());
                }

                bodyViewHolder.clt.setTitleText(itemModel.getItemTitle());
                if (itemModel.getTextStatus() == STYLE_BIG_TEXT) {

                } else if (itemModel.getTextStatus() == STYLE_LINE) {
//                    bodyViewHolder.clt.getmContentText().getPaint().setAntiAlias(true);
                    bodyViewHolder.clt.getmContentText().getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                }

                if (itemModel.isContentGone()) {
                    bodyViewHolder.clt.getmContentText().setVisibility(View.GONE);
                } else {
                    bodyViewHolder.clt.getmContentText().setVisibility(View.VISIBLE);
                }

                //默认颜色
                bodyViewHolder.clt.getmTitleText().setTextColor(Color.parseColor("#24272B"));
                bodyViewHolder.clt.getmContentText().setTextColor(Color.parseColor("#3E4A59"));


                if (itemModel.getItemContentColor() != 0) {
                    bodyViewHolder.clt.getmTitleText().setTextColor(itemModel.getItemContentColor());
                    bodyViewHolder.clt.getmContentText().setTextColor(itemModel.getItemContentColor());
                }
//                if (itemModel.ismItemRed()) {
//                    bodyViewHolder.clt.getmTitleText().setTextColor(itemModel.getItemContentColor());
//                }else {
//                }

                bodyViewHolder.mBodyLine.setVisibility(visibleLine ? View.VISIBLE : View.GONE);
                break;
            case ItemType.IMAGE:
                ImageViewHolder imageViewHolder = (ImageViewHolder) holder;
                GlideApp.with(mContext).load(itemModel.getItemTitle()).transform(new GlideRoundTransform(mContext, 5)).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(imageViewHolder.bannerImage);
                break;
            case ItemType.SPLITLINE:
                break;

        }
    }


    public void refresh(List<ItemModel> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public ImageView leftImage;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            leftImage = itemView.findViewById(R.id.id_order_header_iv);
        }
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView bannerImage;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);
            bannerImage = itemView.findViewById(R.id.id_order_image);
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) bannerImage.getLayoutParams();
            int width = AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 15) * 2;
            // 375 85
            int height = width * 85 / 375;
            lp.width = width;
            lp.height = height;
            bannerImage.setLayoutParams(lp);
        }
    }

    private class BodyViewHolder extends RecyclerView.ViewHolder {
        public CustomeLineText clt;
        public View mBodyLine;

        public BodyViewHolder(@NonNull View itemView) {
            super(itemView);
            clt = itemView.findViewById(R.id.clt);
            mBodyLine = itemView.findViewById(R.id.body_line);
        }
    }

    private class SplitViewHolder extends RecyclerView.ViewHolder {

        public SplitViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
