package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 解绑银行卡接口，返回对象 RemoveCardEntity
 */

public class UnlinkCardProtocol extends BaseProtocol {
    public static final String TAG = UnlinkCardProtocol.class.getSimpleName();
    /**
     * 银行卡id
     */
    String mCardId;
    /**
     * 指定新的默认卡id
     */
    String mNewDefaultCardId;

    public UnlinkCardProtocol(String cardId, String newDefaultCardId, NetWorkCallbackListener dataCallback) {
        this.mCardId = cardId;
        this.mNewDefaultCardId = (TextUtils.isEmpty(newDefaultCardId) ? "" : newDefaultCardId);
        this.mDataCallback = dataCallback;
        mUrl = "api/card/remove";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARDID, mCardId);
        mBodyParams.put(RequestParams.NEWDEFAULTCARDID, mNewDefaultCardId);

    }
}
