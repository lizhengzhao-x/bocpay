package cn.swiftpass.wallet.intl.module.transfer;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;

import java.util.HashMap;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;

public class StaticCodeUtils {

    public static void checkBindCard(Activity activity, BindSmartAccountCallBack bindSmartAccountCallBack) {
        //先检查是否绑定智能账户 再检查是否绑定FPS
        ApiProtocolImplManager.getInstance().checkStaticCodeStatus(activity, true, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
                    AndroidUtils.showBindSmartAccountAndCancelDialog(activity, activity.getString(R.string.AA2105_1_2), activity.getString(R.string.AA2105_1_3), activity.getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            VerifyPasswordCommonActivity.startActivityForResult(activity, false,new OnPwdVerifyCallBack() {
                                @Override
                                public void onVerifySuccess() {
                                    HashMap<String, Object> mHashMaps = new HashMap<>();
                                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                                    ActivitySkipUtil.startAnotherActivity(activity, SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }

                                @Override
                                public void onVerifyFailed(String errorCode, String errorMsg) {
                                    AndroidUtils.showErrorMsgDialog(activity, errorMsg, null);
                                }

                                @Override
                                public void onVerifyCanceled() {

                                }
                            });
                        }
                    });
                } else if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_UNBIND_FPS)) {
                    AndroidUtils.showBindSmartAccountAndCancelDialog(activity, activity.getString(R.string.AA2105_1_6), "", activity.getString(R.string.AA2105_1_8), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            VerifyPasswordCommonActivity.startActivityForResult(activity, new OnPwdVerifyCallBack() {
                                @Override
                                public void onVerifySuccess() {
                                    ActivitySkipUtil.startAnotherActivity(activity, FpsManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                }

                                @Override
                                public void onVerifyFailed(String errorCode, String errorMsg) {
                                    AndroidUtils.showErrorMsgDialog(activity, errorMsg, null);
                                }

                                @Override
                                public void onVerifyCanceled() {

                                }
                            });
                        }
                    });
                } else {
                    DialogUtils.showErrorMsgDialog(activity, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (bindSmartAccountCallBack != null) {
                    bindSmartAccountCallBack.onBindSmartAccountSuccess();
                }

            }
        });
    }

    public interface BindSmartAccountCallBack {

        void onBindSmartAccountSuccess();
    }
}
