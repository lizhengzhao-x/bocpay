package cn.swiftpass.wallet.intl.utils;


import cn.swiftpass.wallet.intl.BuildConfig;


public class WalletConfig {
    public static int getMajorScanQueryQRCodeResultDelayTime() {
        return BuildConfig.majorScanQueryQRCodeResultDelayTime;
    }


    public static int getMessageCountDown() {

        return BuildConfig.messageCountDown;
    }


    public static int getBackScanUpdateQRCodeTime() {

        return BuildConfig.backScanUpdateQRCodeTime;
    }


    public static int getBackScanQueryQRCodeResultTime() {
        return BuildConfig.backScanQueryQRCodeResultTime;
    }


    public static String getImageCashSDPath() {

        return BuildConfig.imageCashSDPath;
    }


    public static int getImageCashTotalSize() {

        return BuildConfig.imageCashTotalSize;
    }

    public static int getAppLoginStatusMinutes() {

        return BuildConfig.APPLoginStatusSaveTimeInMinutes;
    }











/*    public static WalletConfig.readConfig(Context context) {
        Serializer serializer = new Persister();
        InputStream fs = null;
        try {
            if (context.getFileStreamPath(CONFIG_NAME).exists()) {
                fs = new FileInputStream(context.getFileStreamPath(CONFIG_NAME));
            } else {
                fs = context.getResources().getAssets().open(CONFIG_NAME);
            }
            WalletConfig.config = serializer.read(WalletConfig.class, fs);
            return config;
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException e) {
                    LogUtils.e(TAG, Log.getStackTraceString(e));
                }
            }
        }
        return new WalletConfig.);
    }*/
}
