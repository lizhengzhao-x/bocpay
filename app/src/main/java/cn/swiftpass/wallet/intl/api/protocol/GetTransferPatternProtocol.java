package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/19
 * 得到转账方法
 */

public class GetTransferPatternProtocol extends BaseProtocol {
    public static final String TAG = GetTransferPatternProtocol.class.getSimpleName();

    public GetTransferPatternProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferPattern";
    }
}
