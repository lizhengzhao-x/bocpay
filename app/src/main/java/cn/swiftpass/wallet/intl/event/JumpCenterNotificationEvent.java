package cn.swiftpass.wallet.intl.event;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_UPGRADEDIALOG;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import cn.swiftpass.wallet.intl.dialog.CustomNotificationDialog;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.module.MainContract;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class JumpCenterNotificationEvent extends BaseEventType {
    public static final String JUMPCENTERNOTIFICATIONNAME = "JumpCenterNotificationEvent";

    private CustomNotificationDialog mNotificationDialog;

    public JumpCenterNotificationEvent() {
        super(EVENT_UPGRADEDIALOG.eventPriority, EVENT_UPGRADEDIALOG.eventName);
    }

    @Override
    protected String getCurrentEventType() {
        return JUMPCENTERNOTIFICATIONNAME;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof JumpCenterNotificationDialogDealOwner) {
            SpUtils.getInstance().saveShowNotificationDialog(true);
            JumpCenterNotificationDialogDealOwner lbsDialogDealOwner = (JumpCenterNotificationDialogDealOwner) currentActivity;
            showJumpNotifacationCenterDialog(lbsDialogDealOwner);
        }
        return true;
    }


    private void showJumpNotifacationCenterDialog(JumpCenterNotificationDialogDealOwner lbsDialogDealOwner) {
        MainContract.Presenter mPresenter = lbsDialogDealOwner.getCurrentPresenter();
        Activity activity = lbsDialogDealOwner.getCurrentActivity();
        Context context = lbsDialogDealOwner.getCurrentContext();
        if (mPresenter == null || activity == null || context == null) return;
        mNotificationDialog = AndroidUtils.showTipDialogCallBack(activity,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mPresenter != null) {
                            mNotificationDialog.showProgressDialog();
                            mPresenter.getNotificationStatus(mNotificationDialog, true, NotificationStatusEntity.NOTIFICATIONSTATUSOPEN, NotificationStatusEntity.NOTIFICATIONACTIONUPDATE);
                        }
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mNotificationDialog = null;
                    }
                });
        if (mPresenter != null) {
            mPresenter.notificationTipCallServer();
        }
    }


    public interface JumpCenterNotificationDialogDealOwner extends EventDealOwner {
        MainContract.Presenter getCurrentPresenter();
    }

}
