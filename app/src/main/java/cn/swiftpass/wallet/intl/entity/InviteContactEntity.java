package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

public class InviteContactEntity extends ContactEntity {

    /**
     * 手机号
     */
    private String account;
    /**
     * 联系人名称
     */
    private String name;
    /**
     * 提示状态 0不存在 1已存在
     */
    private String status;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 合规改动 需求变动 默认为0
     *
     * @return
     */
    public String getStatus() {

        return "0";
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 模糊搜索 去除空格
     */
    public String getPhoneNoMapping() {
        if (TextUtils.isEmpty(account)) {
            return "";
        }
        return account.replace(" ", "").replace("-", "");
    }

    public String getNameNoMapping() {
        if (TextUtils.isEmpty(name)) {
            return "";
        }
        return name.replace(" ", "");
    }
}
