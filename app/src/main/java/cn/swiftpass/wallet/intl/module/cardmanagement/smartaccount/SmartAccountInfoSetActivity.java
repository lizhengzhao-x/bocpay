package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.AmountSetDialog;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartAccountCheckAccountEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView;
import cn.swiftpass.wallet.intl.widget.ImageTopUpView;

import static cn.swiftpass.wallet.intl.utils.AndroidUtils.CURRENCY_SYMBOL;

/**
 * 绑定我的账户设置信息界面
 */

public class SmartAccountInfoSetActivity extends BaseCompatActivity {

    public static final String TAG = SmartAccountInfoSetActivity.class.getSimpleName();


    public static final int PRIMARY_REQUEST_CODE = 0x10;


    @BindView(R.id.check_primary_account)
    ImageArrowNoPaddingView idCheckAccount;
    @BindView(R.id.id_edit_daily_amount)
    ImageArrowNoPaddingView idEditDailyAmount;
    @BindView(R.id.id_seekbar)
    SeekBar idSeekbar;

    @BindView(R.id.seekbar_topup)
    SeekBar mTopupSeekbar;

    @BindView(R.id.id_topup_daily_amount)
    ImageArrowNoPaddingView idTopUpEditDailyAmount;

    @BindView(R.id.id_money_min)
    TextView mMoneyMinTV;
    @BindView(R.id.id_money_max)
    TextView mMoneyMaxTV;

    @BindView(R.id.tv_topup_money_min)
    TextView mTopupMoneyMinTV;
    @BindView(R.id.tv_topup_money_max)
    TextView mTopupMoneyMaxTV;

    @BindView(R.id.iv_confirm_mark)
    ImageView ivConfirmMark;
    @BindView(R.id.id_confirm_conditions)
    TextView idConfirmConditions;
    @BindView(R.id.id_conditions)
    LinearLayout idConditions;
    @BindView(R.id.tv_link_bo_bocpay)
    TextView tvLinkBoBocpay;
    @BindView(R.id.id_topup_direct)
    ImageTopUpView mTopUpDirectLayout;
    @BindView(R.id.id_topup_manual)
    ImageTopUpView mTopUpManualLayout;
    @BindView(R.id.id_topup_auto)
    ImageTopUpView mTopUpAutoLayout;

    @BindView(R.id.ll_topup_setting)
    View mTopUpDetailLayout;


    String mTopupMethod = SmartAccountConst.TOPUP_METHOD_DIRECT;


    String mPrimaryAccount;
    String mPrimaryAccountType;

    BankLoginResultEntity mBankEntity;
    int mFlowType;

    private int mLimitMax;

    private int mLimitMin;

    private int mLimitAmount;

    private int mTopUpLimitMax;

    private int mTopUpLimitMin;

    private int mTopUpLimitAmount;

    private ArrayList<SmartAccountCheckAccountEntity> mPrimaryAccounts;


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_smart_set;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        initAgreement();
        mTopUpDirectLayout.setCheck(true);
        mTopUpDetailLayout.setVisibility(View.GONE);
        Intent intent = getIntent();
        if (null == intent || null == intent.getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO)) {
            finish();
        } else {
            mBankEntity = (BankLoginResultEntity) intent.getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO);
            mFlowType = intent.getIntExtra(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
            updateNextStatus(false);
            initData();
            initSeekBar();
            initTopUpSeekBar();
            if (mFlowType == Constants.PAGE_FLOW_FORGETPASSWORD) {
                //重设密码成功
                setToolBarTitle(R.string.setting_payment_fgw);
                tvLinkBoBocpay.setText(R.string.next_page);
            } else if (mFlowType == Constants.SMART_ACCOUNT_UPDATE) {
                //升级
                setToolBarTitle(R.string.P3_ewa_01_028);
            } else {
                setToolBarTitle(R.string.string_regist_smart_account);
            }
        }
    }

    private void initData() {
        try {
            mPrimaryAccounts = (ArrayList<SmartAccountCheckAccountEntity>) mBankEntity.getAccountRows();
            mLimitMax = mBankEntity.getMaxPay() == null ? 5000 : Double.valueOf(mBankEntity.getMaxPay()).intValue();
            mLimitMin = mBankEntity.getMinPay() == null ? 0 : Double.valueOf(mBankEntity.getMinPay()).intValue();
            mLimitAmount = mBankEntity.getPayLimit() == null ? 0 : Double.valueOf(mBankEntity.getPayLimit()).intValue();

            mTopUpLimitMax = mBankEntity.getMethodMaxPay() == null ? 5000 : Double.valueOf(mBankEntity.getMethodMaxPay()).intValue();
            mTopUpLimitMin = mBankEntity.getMethodMinPay() == null ? 0 : Double.valueOf(mBankEntity.getMethodMinPay()).intValue();
            mTopUpLimitAmount = mBankEntity.getAutotopupamt() == null ? 0 : Double.valueOf(mBankEntity.getAutotopupamt()).intValue();
        } catch (Exception e) {
            LogUtils.e(TAG, e.getMessage());
        }
    }

    private void initSeekBar() {
        mMoneyMinTV.setText(AndroidUtils.getCurrencyAmount(mLimitMin));
        String amountMax = CURRENCY_SYMBOL + " " + AndroidUtils.formatPriceInt(mLimitMax, true);
        mMoneyMaxTV.setText(amountMax);
        idSeekbar.setMax(100);
        idSeekbar.setProgress((int) ((Double.valueOf(mLimitAmount) * 100 / Double.valueOf(mLimitMax))));
        idCheckAccount.setLeftTextTitle(getPrimaryAccount());
        String amount = CURRENCY_SYMBOL + " " + AndroidUtils.formatPriceInt(mLimitAmount, true);

        idEditDailyAmount.setLeftTextTitle(amount);
        idSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mLimitAmount = (int) ((progress * 1.0 / 100.0) * Float.valueOf(mLimitMax));
                mLimitAmount = mLimitAmount / 100 * 100;
                String amount = CURRENCY_SYMBOL + " " + AndroidUtils.formatPriceInt(mLimitAmount, true);
                idEditDailyAmount.setLeftTextTitle(amount);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private void initTopUpSeekBar() {
        mTopupMoneyMinTV.setText(AndroidUtils.getCurrencyAmount(mTopUpLimitMin));
//        mTopupMoneyMaxTV.setText(AndroidUtils.getCurrencyAmount(mTopUpLimitMax));
        mTopupMoneyMaxTV.setText(AndroidUtils.getCurrencyAmount(AndroidUtils.formatPriceInt(mTopUpLimitMax, true)));
        mTopupSeekbar.setMax(100);
        mTopupSeekbar.setProgress((int) ((Double.valueOf(mTopUpLimitAmount) * 100 / Double.valueOf(mTopUpLimitMax))));
//        idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(mTopUpLimitAmount));
        idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(AndroidUtils.formatPriceInt(mTopUpLimitAmount, true)));
        mTopupSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mTopUpLimitAmount = (int) ((progress * 1.0 / 100.0) * Float.valueOf(mTopUpLimitMax));
                mTopUpLimitAmount = mTopUpLimitAmount / 100 * 100;
//                idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(mTopUpLimitAmount));
                idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(AndroidUtils.formatPriceInt(mTopUpLimitAmount, true)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private String getPrimaryAccount() {
        String primaryAccount = "";
        if (!TextUtils.isEmpty(mBankEntity.getRelevanceAccNo())) {
            primaryAccount = AndroidUtils.getPrimaryAccountDisplay(mBankEntity.getAccType(), mBankEntity.getRelevanceAccNo());
            mPrimaryAccount = mBankEntity.getRelevanceAccNo();
            mPrimaryAccountType = mBankEntity.getAccType();
        } else {
            if (null != mBankEntity && null != mBankEntity.getAccountRows() && !mBankEntity.getAccountRows().isEmpty()) {
                primaryAccount = AndroidUtils.getPrimaryAccountDisplay(mBankEntity.getAccountRows().get(0));
                mPrimaryAccount = mBankEntity.getAccountRows().get(0).getAccountNo();
                mPrimaryAccountType = mBankEntity.getAccountRows().get(0).getAccType();
            }
        }
        return primaryAccount;
    }


    @OnClick({R.id.check_primary_account, R.id.id_edit_daily_amount, R.id.id_topup_daily_amount, R.id.iv_confirm_mark, R.id.tv_link_bo_bocpay, R.id.id_topup_direct, R.id.id_topup_manual, R.id.id_topup_auto})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.check_primary_account:
                onClickPrimaryAccount();
                break;
            case R.id.id_edit_daily_amount:
                onClickAmountLimit();
                break;
            case R.id.id_topup_daily_amount:
                onClickTopUpAmountLimit();
                break;
            case R.id.iv_confirm_mark:
                onClickCheck();
                break;
            case R.id.tv_link_bo_bocpay:
                onNext();
                break;
            case R.id.id_topup_direct:
                mTopUpDirectLayout.switchCheck();
                if (mTopUpDirectLayout.isCheck()) {
                    mTopUpManualLayout.setCheck(false);
                    mTopUpAutoLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.GONE);
                }
                break;
            case R.id.id_topup_manual:
                mTopUpManualLayout.switchCheck();
                if (mTopUpManualLayout.isCheck()) {
                    mTopUpDirectLayout.setCheck(false);
                    mTopUpAutoLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.GONE);
                }
                break;
            case R.id.id_topup_auto:
                mTopUpAutoLayout.switchCheck();
                if (mTopUpAutoLayout.isCheck()) {
                    mTopUpManualLayout.setCheck(false);
                    mTopUpDirectLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    private void onClickAmountLimit() {
        AmountSetDialog.Builder builder = new AmountSetDialog.Builder(this);
        AmountSetDialog dialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dlg, int which) {
                int limitAmount = ((AmountSetDialog) dlg).getmAmount();
                idSeekbar.setProgress((int) ((limitAmount / Float.valueOf(mLimitMax) * 100)));
                String amount = CURRENCY_SYMBOL + " " + AndroidUtils.formatPriceInt(mLimitAmount, true);
                idEditDailyAmount.setLeftTextTitle(amount);
                mLimitAmount = limitAmount;
            }
        }).create(-1, mLimitMax);
        dialog.show();
    }

    private void onClickTopUpAmountLimit() {
        AmountSetDialog.Builder builder = new AmountSetDialog.Builder(this);
        AmountSetDialog dialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dlg, int which) {
                int topUpLimitAmount = ((AmountSetDialog) dlg).getmAmount();
                mTopupSeekbar.setProgress((int) ((topUpLimitAmount / Float.valueOf(mTopUpLimitMax) * 100)));
//                idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(topUpLimitAmount));
                idTopUpEditDailyAmount.setLeftTextTitle(AndroidUtils.getCurrencyAmount(AndroidUtils.formatPriceInt(topUpLimitAmount, true)));
                mTopUpLimitAmount = topUpLimitAmount;
            }
        }).create(-1, mTopUpLimitMax);
        dialog.show();
    }

    private void onClickPrimaryAccount() {
        Intent intent = new Intent(SmartAccountInfoSetActivity.this, PrimaryAccountSelActivity.class);
        intent.putExtra(PrimaryAccountSelActivity.PRIMARY_ACCOUNTS, mPrimaryAccounts);
        intent.putExtra(PrimaryAccountSelActivity.PRIMARY_ACCOUNT_CURRENT, mPrimaryAccount);
        startActivityForResult(intent, PRIMARY_REQUEST_CODE);
    }

    //点击协议圆圈选择
    private void onClickCheck() {
        ivConfirmMark.setSelected(!ivConfirmMark.isSelected());
        updateNextStatus(ivConfirmMark.isSelected());
    }

    private void updateNextStatus(boolean check) {
        if (check && !ivConfirmMark.isSelected()) {
            return;
        }
        tvLinkBoBocpay.setEnabled(check);
        tvLinkBoBocpay.setBackgroundResource(check ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    private void initAgreement() {

//        SpannableString spannableString = new SpannableString(totalStr.replace("%%", ""));
//        spannableString.setSpan(new ClickableSpan() {
//            @Override
//            public void onClick(View view) {
//                ActivitySkipUtil.startAnotherActivity(RegisterDetailsActivity.this, ServerAgreementActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//            }
//
//            @Override
//            public void updateDrawState(TextPaint ds) {
//                super.updateDrawState(ds);
//                ds.setColor(Color.parseColor("#136EF1"));
//                ds.setUnderlineText(false);
//            }
//        }, firstSart, firstEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        String totalStr = getResources().getString(R.string.NIT2101_1_1);

        final String items[] = totalStr.split("##");
        int firstSart = totalStr.indexOf(items[1]) - 2;
        int firstEnd = firstSart + items[1].length();

        int secondSart = totalStr.indexOf(items[3]) - 6;
        int secondEnd = secondSart + items[3].length();


        SpannableString spannableString = new SpannableString(totalStr.replace("##", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(SmartAccountInfoSetActivity.this, ServerAgreementActivity.class);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, firstSart, firstEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);


        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_TITLE, items[3]);
                String lan = SpUtils.getInstance(SmartAccountInfoSetActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.TERM_REGISTER_ZN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.TERM_REGISTER_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.TERM_REGISTER_ENG);
                }
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, secondSart, secondEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        idConfirmConditions.setText(spannableString);
        idConfirmConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK != resultCode) {
            return;
        }
        if (requestCode == PRIMARY_REQUEST_CODE) {
            SmartAccountCheckAccountEntity entity = (SmartAccountCheckAccountEntity) data.getSerializableExtra(PrimaryAccountSelActivity.PRIMARY_ACCOUNT);
            mPrimaryAccount = entity.getAccountNo();
            mPrimaryAccountType = entity.getAccType();
            idCheckAccount.setLeftTextTitle(AndroidUtils.getPrimaryAccountDisplay(entity));
        }
    }

    private void onNext() {
        if (mTopUpDirectLayout.isCheck()) {
            mTopupMethod = SmartAccountConst.TOPUP_METHOD_DIRECT;
        } else if (mTopUpManualLayout.isCheck()) {
            mTopupMethod = SmartAccountConst.TOPUP_METHOD_MANUAL;
        } else if (mTopUpAutoLayout.isCheck()) {
            mTopupMethod = SmartAccountConst.TOPUP_METHOD_AUTO;
        }
        if (mTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_AUTO)) {
            //自动增值 0处理
            if (mTopUpLimitAmount == 0) {
                showErrorMsgDialog(mContext, getString(R.string.title_string_topsetting_error));
                return;
            }
        } else {
            mTopUpLimitAmount = 0;
        }
        if (mLimitAmount == 0) {
            showErrorMsgDialog(mContext, getString(R.string.title_string_daily_litmit_error));
            return;
        }
        BankLoginResultEntity info = mBankEntity;
        info.setAddedmethod(mTopupMethod);
        info.setAutotopupamt(String.valueOf(mTopUpLimitAmount));
        info.setPayLimit(String.valueOf(mLimitAmount));
        info.setRelevanceAccNo(mPrimaryAccount);
        info.setAccType(mPrimaryAccountType);
        Intent intent = new Intent(SmartAccountInfoSetActivity.this, SmartAccountInfoRegisterActivity.class);
        intent.putExtra(Constants.CURRENT_PAGE_FLOW, mFlowType);
        if (mFlowType == Constants.PAGE_FLOW_REGISTER) {
            intent.putExtra(Constants.USE_BIOMETRICAUTH, getIntent().getBooleanExtra(Constants.USE_BIOMETRICAUTH, true));
        }
        intent.putExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, info);
        startActivity(intent);
    }


}
