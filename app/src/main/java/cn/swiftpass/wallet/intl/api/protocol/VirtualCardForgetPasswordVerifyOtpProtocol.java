package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.api.protocol.E2eePreProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.manage.AppTestManager;

/**
 * Created by ZhangXinchao on 2019/11/22.
 */
public class VirtualCardForgetPasswordVerifyOtpProtocol extends BaseProtocol {

    String checkType;
    String verifyCode;
    String passcode;
    String mPin;

    public VirtualCardForgetPasswordVerifyOtpProtocol(String checkTypeIn, String passcodeIn, String verifyCodeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.checkType = checkTypeIn;
        this.verifyCode = verifyCodeIn;
        this.passcode = passcodeIn;
        mUrl = "api/virtual/forgetpwd/checkOtp";
    }


    @Override
    public void packData() {
        super.packData();
//        action	String	M	VIRPWDSET---虚拟卡忘记密码
//        checkType	String	M	1:信用卡 2：网银
        putIngoreNullParams(RequestParams.CHECKTYPE, checkType);
        putIngoreNullParams(RequestParams.VERIFYCODE, verifyCode);
        putIngoreNullParams(RequestParams.PASSCODE, mPin);
        putIngoreNullParams(RequestParams.ACTION, ApiConstant.VIRTUALCARD_FORGETPWD);
    }

    @Override
    public Void execute() {
        if (AppTestManager.getInstance().isE2ee()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    String mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    mPin = E2eeUtils.getLoginOrSetPin(mSid, passcode, pubKey, random);
                    if (TextUtils.isEmpty(mPin)) {
                        mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                    } else {
                        VirtualCardForgetPasswordVerifyOtpProtocol.super.execute();
                    }
                }
            }).execute();
        } else {
            super.execute();
        }
        return null;
    }
}
