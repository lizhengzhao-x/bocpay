package cn.swiftpass.wallet.intl.module.setting.location;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.location.Location;
import android.text.TextUtils;

import androidx.fragment.app.FragmentActivity;

import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LocationUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;

import static cn.swiftpass.wallet.intl.module.setting.location.LocationSettingActivity.LOCATION_SP;
import static cn.swiftpass.wallet.intl.utils.LocationUtils.COUNTRY_CHINA_CH;
import static cn.swiftpass.wallet.intl.utils.LocationUtils.COUNTRY_CHINA_EN;
import static cn.swiftpass.wallet.intl.utils.LocationUtils.COUNTRY_CHINA_HK;

public class LocationUIUtils {

    public static void showSelLocationDialog(Activity mActivity, DialogSelListener locationSelListener) {
        String msg = mActivity.getString(R.string.LBS209_1_2) + "\n\n" + mActivity.getString(R.string.LBS209_1_3);
        AndroidUtils.showTipDialog(mActivity, mActivity.getString(R.string.LBS209_1_1), msg,
                mActivity.getString(R.string.LBS209_1_4), mActivity.getString(R.string.LBS209_1_5), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        locationSelListener.onSelOkListener();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        locationSelListener.onSelCancelListener();
                    }
                });
    }

    public static void checkLocationService(FragmentActivity mActivity, DialogSelListener onPermissionListener) {
        PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                onPermissionListener.onSelOkListener();
            }

            @Override
            public void rejectPermission() {
                onPermissionListener.onSelCancelListener();

            }
        }, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static void showOpenLocationServiceDialog(Activity mActivity, DialogSelListener dialogSelListener) {
        AndroidUtils.showTipDialog(mActivity, mActivity.getString(R.string.LBS209_7_1), mActivity.getString(R.string.LBS209_7_2),
                mActivity.getString(R.string.LBS209_1_4), mActivity.getString(R.string.LBS209_8_4), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogSelListener.onSelOkListener();
                        LocationUtils.openGpsSettings(mActivity);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogSelListener.onSelCancelListener();
                    }
                });
    }


    /**
     * 1.每次进入被扫 更新地理位置 成功了保存到sp 被扫界面使用位置更新切换
     * 2.每次被扫成功界面 将成功的位置保存
     *
     * @param mActivity
     * @param location
     * @return
     */
    public static void onLocationChangedEvent(Activity mActivity, Location location, LocationLoadListener locationLoadListener) {
        if (locationLoadListener != null) {
            if (location == null) return;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    String countryName = LocationUtils.getCountryName(mActivity, location.getLatitude(), location.getLongitude());
                    if (mActivity != null) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!TextUtils.isEmpty(countryName)) {
                                    HttpCoreSpUtils.put(LOCATION_SP, true);
                                    //香港同事测试到 香港 返回的countryName 就是香港 需求是 港澳台地区/中国
                                    //China/中国/中國
                                    if (TextUtils.equals(countryName, COUNTRY_CHINA_EN) ||
                                            TextUtils.equals(countryName, COUNTRY_CHINA_CH) ||
                                            TextUtils.equals(countryName, COUNTRY_CHINA_HK)) {
                                        locationLoadListener.onLocationLoadSuccess(false);
                                    } else {
                                        locationLoadListener.onLocationLoadSuccess(true);
                                    }
                                    LocationUtils.unregister();
                                }
                            }
                        });
                    }
                }
            }).start();
        }
    }


    public interface DialogSelListener {
        void onSelOkListener();

        void onSelCancelListener();
    }

    public interface LocationLoadListener {
        void onLocationLoadSuccess(boolean isHk);

    }


//    public  interface LocationSelListener {
//        void onSelOkListener();
//        void onSelCancelListener();
//    }
//
//    public  interface OnPermissionListener {
//        void acceptPermission();
//        void rejectPermission();
//    }

}
