package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SelAccountEntity;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.adapter.SetAccountListAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;


public class TransferAccountListActivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    private List<StaticCodeEntity.MobnQRBean> emalQRBeans;
    private String currentSelNo;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.TF2101_5_1);
        emalQRBeans = (List<StaticCodeEntity.MobnQRBean>) getIntent().getExtras().getSerializable(Constants.DATA_TYPE);
        currentSelNo = getIntent().getExtras().getString(Constants.CURRENT_SEL_ACCOUNT_NO);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        for (int i = 0; i < emalQRBeans.size(); i++) {
            String accountNo = AndroidUtils.getPrimaryAccountDisplay(emalQRBeans.get(i).getAcTp(), AndroidUtils.subPaymentMethod(emalQRBeans.get(i).getAcNo()));
            if (accountNo.contains(currentSelNo)) {
                emalQRBeans.get(i).setSel(true);
                break;
            }
        }

        idRecyclerview.setAdapter(getAdapter(null));
        ((IAdapter<StaticCodeEntity.MobnQRBean>) idRecyclerview.getAdapter()).setData(emalQRBeans);
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }

    private CommonRcvAdapter<StaticCodeEntity.MobnQRBean> getAdapter(List<StaticCodeEntity.MobnQRBean> data) {
        return new CommonRcvAdapter<StaticCodeEntity.MobnQRBean>(data) {


            @NonNull
            @Override
            public AdapterItem createItem(Object type) {

                return new SetAccountListAdapter(mContext, new SetAccountListAdapter.CardItemSelCallback() {
                    @Override
                    public void onItemCardSelClick(int position) {
                        if (emalQRBeans.size() == 0) {
                            finish();
                            return;
                        }
                        SelAccountEntity selAccountEntity = new SelAccountEntity();
                        selAccountEntity.setSelType(getIntent().getExtras().getInt(Constants.TRANSFER_ACCOUNT_TYPE));
                        selAccountEntity.setSelPos(position);
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                        EventBus.getDefault().postSticky(selAccountEntity);
                        finish();
                    }
                });
            }
        };
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_sel_list;
    }


}
