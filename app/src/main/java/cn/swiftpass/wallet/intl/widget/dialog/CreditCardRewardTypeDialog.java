package cn.swiftpass.wallet.intl.widget.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;


/**
 *
 */
public class CreditCardRewardTypeDialog {
    private final BottomDialog bottomDialog;
    private BaseCompatActivity mFragmentActivity;
    private String selectType = CreditRewardType.REWARD_TYPE_ALL;
    private final TextView gpRewardTypeTv;
    private final TextView rbRewardTypeTv;
    private OnCardRewardTypeListener listener;


    private CreditCardRewardTypeDialog(BaseCompatActivity fragmentActivity) {
        mFragmentActivity = fragmentActivity;
        bottomDialog = new BottomDialog(mFragmentActivity);
        View contentView = LayoutInflater.from(mFragmentActivity).inflate(R.layout.dialog_bottom_reward_type, null);
        ImageView ivClose = contentView.findViewById(R.id.iv_close);
        TextView resetTv = contentView.findViewById(R.id.tv_reset);
        //积分奖赏
        gpRewardTypeTv = contentView.findViewById(R.id.tv_reward_type_gp);
        //现金奖赏
        rbRewardTypeTv = contentView.findViewById(R.id.tv_reward_type_rb);
        TextView confirmTv = contentView.findViewById(R.id.tv_confirm);
        TextView cancelTv = contentView.findViewById(R.id.tv_cancel);


        resetTv.setOnClickListener(v -> updateTypeBtn(CreditRewardType.REWARD_TYPE_ALL));
        gpRewardTypeTv.setOnClickListener(v -> updateTypeBtn(CreditRewardType.REWARD_TYPE_POINT));
        rbRewardTypeTv.setOnClickListener(v -> updateTypeBtn(CreditRewardType.REWARD_TYPE_CASH));


        confirmTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (listener != null) {
                        listener.OnSelectRewardType(selectType);
                    }
                    bottomDialog.dismiss();
                }

            }
        });

        View.OnClickListener cancelListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    bottomDialog.dismiss();
                }
            }
        };

        cancelTv.setOnClickListener(cancelListener);

        ivClose.setOnClickListener(cancelListener);

        bottomDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        });

        bottomDialog.setContentView(contentView);
        Window dialogWindow = bottomDialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams layoutParams = dialogWindow.getAttributes();
            int height = (int) (AndroidUtils.getScreenHeight(fragmentActivity) * 0.4);
            layoutParams.height = height;
            dialogWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialogWindow.setAttributes(layoutParams);
        }

    }

    private void updateTypeBtn(String type) {
        resetTypeBtn();
        if (CreditRewardType.REWARD_TYPE_CASH.equals(type)) {
            rbRewardTypeTv.setTextColor(mFragmentActivity.getColor(R.color.color_FFC53455));
            rbRewardTypeTv.setBackgroundResource(R.drawable.bg_item_reward_type_select);
            selectType = CreditRewardType.REWARD_TYPE_CASH;
        } else if (CreditRewardType.REWARD_TYPE_POINT.equals(type)) {
            gpRewardTypeTv.setTextColor(mFragmentActivity.getColor(R.color.color_FFC53455));
            gpRewardTypeTv.setBackgroundResource(R.drawable.bg_item_reward_type_select);
            selectType = CreditRewardType.REWARD_TYPE_POINT;
        } else {
            //重置时，不需要选中任何标签，但是等于选中了2种类型
            selectType = CreditRewardType.REWARD_TYPE_ALL;
        }

    }

    private void resetTypeBtn() {
        gpRewardTypeTv.setTextColor(mFragmentActivity.getColor(R.color.app_black));
        gpRewardTypeTv.setBackgroundResource(R.drawable.bg_item_reward_type);
        rbRewardTypeTv.setTextColor(mFragmentActivity.getColor(R.color.app_black));
        rbRewardTypeTv.setBackgroundResource(R.drawable.bg_item_reward_type);
    }


    public static CreditCardRewardTypeDialog with(BaseCompatActivity fragmentActivity) {
        return new CreditCardRewardTypeDialog(fragmentActivity);
    }

    public static CreditCardRewardTypeDialog with(Fragment fragment) {
        Context context = fragment.getContext();
        if (context instanceof BaseCompatActivity) {
            return new CreditCardRewardTypeDialog((BaseCompatActivity) context);
        }
        throw new IllegalArgumentException("fragment必须添加到某个FragmentActivity");
    }

    public static CreditCardRewardTypeDialog with(Context context) {
        if (context instanceof BaseCompatActivity) {
            return new CreditCardRewardTypeDialog((BaseCompatActivity) context);
        }
        throw new IllegalArgumentException("context必须来自某个FragmentActivity");
    }

    public CreditCardRewardTypeDialog setListener(OnCardRewardTypeListener listener) {
        this.listener = listener;
        return this;
    }


    public void show() {
        updateTypeBtn(selectType);
        bottomDialog.showWithBottomAnim();
    }


    public CreditCardRewardTypeDialog setSelectType(String type) {
        this.selectType = type;
        return this;
    }
}
