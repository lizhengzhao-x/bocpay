package cn.swiftpass.wallet.intl.module.setting;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.net.Uri;
import android.net.http.SslError;
import android.text.TextUtils;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static cn.swiftpass.wallet.intl.entity.Constants.BUILD_FLAVOR_PRD;

/**
 * 证书校验
 */
public class BaseCertificateVerifyWebViewActivity extends BaseCompatActivity {

    private ValueCallback<Uri[]> mValueCallback;
    protected static final String TAG = "WebViewActivity";
    private CustomDialog mDealDialog;
    private String tel;
    protected String mOriginalUrl;

    @Override
    public void init() {
        setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
        getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    protected void initWebView(WebView webView, String url) {
        LogUtils.i(TAG, "original url :" + url);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                mValueCallback = filePathCallback;
                startActivityForResult(fileChooserParams.createIntent(), 1);
                return true;
            }

        });

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        settings.setAllowFileAccessFromFileURLs(false);
        // 设置WebView可触摸放大缩小
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        //隐藏掉缩放工具
        settings.setDisplayZoomControls(false);

        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setAllowUniversalAccessFromFileURLs(false);
        //webview证书加载不能loadurl 需要校验
        verifyCertifacate(webView, url);
        //webView.loadUrl(url);
        webView.setLongClickable(true);
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return true;
            }
        });
    }


    protected void setWebViewClient(final WebView webView, final boolean isOpenErrorHint) {
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                LogUtils.i(TAG, "shouldOverrideUrlLoading:" + url);
                if (url.endsWith(".pdf")) {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } else if (url.startsWith("tel:")) {
                    showCallPhoneDialog(url);
                } else {
                    verifyCertifacate(view, url);
                }
                return true;
            }


            @Override
            public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                super.onReceivedClientCertRequest(view, request);
                LogUtils.i(TAG, "onReceivedClientCertRequest:" + view.getUrl());
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                LogUtils.i(TAG, "onReceivedError:" + error.toString());
                if (isOpenErrorHint) {
                    // 断网或者网络连接超时
                    int errorCode = error.getErrorCode();
                    if (errorCode == ERROR_HOST_LOOKUP || errorCode == ERROR_CONNECT || errorCode == ERROR_TIMEOUT) {
                        // 避免出现默认的错误界面
                        view.loadUrl("about:blank");
                        showErrorMsgDialog(view.getContext(), getString(ErrorCode.NETWORK_ERROR.msgId));
                    }
                }
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                LogUtils.i(TAG, "onReceivedHttpError:" + errorResponse.toString());
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                LogUtils.i(TAG, "onReceivedSslError:" + error.toString());
                if (BuildConfig.FLAVOR.equals("uat")) {
                    handler.proceed();
                    initSafeOkhttpClient(view, handler);
                } else {
                    //super.onReceivedSslError(view, handler, error);
                }
            }
        });
    }

    protected OkHttpClient.Builder setCertificates(OkHttpClient.Builder client) {
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            ArrayList<Certificate> certificates = new ArrayList<>();
            String path = "boc_cerFLAVORts_prd";
            if (BuildConfig.FLAVOR.equals(BUILD_FLAVOR_PRD)) {
            } else {
                path = "boc_certs_uat";
            }
            try {
                AssetManager am = getAssets();
                String[] list = am.list(path);
                for (int i = 0; i < list.length; i++) {
                    certificates.add(certificateFactory.generateCertificate(am.open(path + "/" + list[i])));
                    LogUtils.i(TAG, "cer name:" + path + "/" + list[i]);
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            int index = 0;
            for (Certificate certificate : certificates) {
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, certificate);
            }
            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            final X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            client.sslSocketFactory(sslSocketFactory, trustManager);
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return client;
    }


    /**
     * 强制证书校验 通过才可以打开url 否则需要弹框提示
     *
     * @param view
     * @param url
     */
    protected void verifyCertifacate(final WebView view, final String url) {
        OkHttpClient.Builder builder = null;
        try {
            builder = setCertificates(new OkHttpClient.Builder());
        } catch (Exception e) {
            builder = new OkHttpClient.Builder();
        }
        HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        builder.hostnameVerifier(hostnameVerifier);
        Request request = new Request.Builder().url(url).build();
        builder.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                LogUtils.i(TAG, "E:" + e.getLocalizedMessage());
                //弹框提示
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                LogUtils.i(TAG, "E:response" + response.toString());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        view.loadUrl(url);
                    }
                });
            }
        });
    }

    protected void initSafeOkhttpClient(WebView view, final SslErrorHandler handler) {
        OkHttpClient.Builder builder;
        try {
            builder = setCertificates(new OkHttpClient.Builder());
        } catch (Exception e) {
            builder = new OkHttpClient.Builder();
        }
        Request request = new Request.Builder().url(view.getUrl()).build();
        builder.build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                handler.proceed();
            }
        });
    }

    /**
     * 申请权限结果的回调方法
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] paramArrayOfInt) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            if (!TextUtils.isEmpty(tel) && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + tel));
                startActivity(intent);
            }
        }

    }


    //拨打电话
    protected void callPhone(String tel) {
        //检查拨打电话权限
        if (!TextUtils.isEmpty(tel) && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setData(Uri.parse("tel:" + tel));
            startActivity(intent);
        } else {
            checkPermissions(Manifest.permission.CALL_PHONE);
        }
    }


    protected void showCallPhoneDialog(String url) {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        tel = url.replace("tel:", "");

        if (TextUtils.isEmpty(tel)) return;
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setTitle(tel);
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(getString(R.string.POPUP1_2), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                callPhone(tel);
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

}
