package cn.swiftpass.wallet.intl.utils.transformation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * Scale the image so that either the width of the image matches the given width and the height of
 * the image is greater than the given height or vice versa, and then crop the larger dimension to
 * match the given dimension.
 *
 * <p>Does not maintain the image's aspect ratio
 */
public class HomeBgTransformation extends BitmapTransformation {
    private static final String ID = "cn.swiftpass.wallet.intl.utils.transformation.HomeMainBgTransformation";
    private static final byte[] ID_BYTES = ID.getBytes(CHARSET);

    private final String keyBg;

    public HomeBgTransformation(String keyBg) {
        this.keyBg = keyBg;
    }

    @Override
    protected Bitmap transform(
            @NonNull BitmapPool pool, @NonNull Bitmap inBitmap, int width, int height) {

        width= AndroidUtils.getScreenWidth(ProjectApp.getContext());
        height=AndroidUtils.getScreenHeight(ProjectApp.getContext());
        Bitmap result = pool.get(width,height , Bitmap.Config.ARGB_8888);

        final float scale;
        Matrix m = new Matrix();
        scale = (float) width / (float) inBitmap.getWidth();
        m.setScale(scale, scale);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        RectF rect = new RectF(0, 0, AndroidUtils.getScreenWidth(ProjectApp.getContext()), AndroidUtils.getScreenHeight(ProjectApp.getContext()));

        try {
            Canvas canvas = new Canvas(result);
            //互换位置就可以实现部分遮罩
            canvas.drawRect(rect, paint);
            canvas.drawBitmap(inBitmap, m,paint);
            clear(canvas);
        } finally {

        }

        return result;

    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof HomeBgTransformation) {
            HomeBgTransformation other = (HomeBgTransformation) o;
            return keyBg == other.keyBg;
        }
        return false;
    }


    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update((ID + keyBg).getBytes(CHARSET));
    }


    @Override
    public int hashCode() {
        return ID.hashCode() + keyBg.hashCode();
    }


    private static void clear(Canvas canvas) {
        canvas.setBitmap(null);
    }


}
