package cn.swiftpass.wallet.intl.entity;


import android.graphics.Color;
import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class ActionTrxGpInfoEntity extends BaseEntity {


    private String tranID;
    private String tranAmt;
    private String merchantName;
    private String isFPS;
    private String tranCur;
    private String qRCUseCase;
    private String name;
    private String mCC;
    private String country;
    private String city;
    private String postal;
    private String lang;
    private String nameAL;
    /**
     * 特殊颜色
     */
    private String colorSign;
    public String getQrTp() {
        return qrTp;
    }

    public void setQrTp(String qrTp) {
        this.qrTp = qrTp;
    }

    private String qrTp;
    private String cityAL;
    private String tID;
    private String gpRequired;
    private String avaGpCount;
    private String redeemGpCount;
    private String redeemGpTnxAmt;
    private String redeemFlag;
    private String gpCode;
    private String yearHolding;
    private String cardId;
    private String payAmt;
    private String cardType;
    private String gpDateCmt;
    private String isCommonQR;
    private String gpBalance;
    private String panFour;
    private String gpComment;

    public String getGpMsgUrl() {
        return gpMsgUrl;
    }

    public void setGpMsgUrl(String gpMsgUrl) {
        this.gpMsgUrl = gpMsgUrl;
    }

    private String gpMsgUrl;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    private String qrCode;

    /**
     * wxQrcode   1：微信码  不能切换  0：不是微信码 可以切换
     */
    private String wxQrcode;
    private String isProm;//是否是特别兑换率
    private boolean isUrlCode;

    public int getColorSign() {
        if (TextUtils.isEmpty(colorSign)) {
            return Color.BLACK;
        }
        return Color.parseColor(colorSign);
    }

    public void setColorSign(String colorSign) {
        this.colorSign = colorSign;
    }
    public String getTranID() {
        return tranID;
    }

    public void setTranID(String tranID) {
        this.tranID = tranID;
    }

    public String getTranAmt() {
        return tranAmt;
    }

    public void setTranAmt(String tranAmt) {
        this.tranAmt = tranAmt;
    }

    public String getTranCur() {
        return tranCur;
    }

    public void setTranCur(String tranCur) {
        this.tranCur = tranCur;
    }

    public String getqRCUseCase() {
        return qRCUseCase;
    }

    public void setqRCUseCase(String qRCUseCase) {
        this.qRCUseCase = qRCUseCase;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getmCC() {
        return mCC;
    }

    public void setmCC(String mCC) {
        this.mCC = mCC;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getNameAL() {
        return nameAL;
    }

    public void setNameAL(String nameAL) {
        this.nameAL = nameAL;
    }

    public String getCityAL() {
        return cityAL;
    }

    public void setCityAL(String cityAL) {
        this.cityAL = cityAL;
    }

    public String gettID() {
        return tID;
    }

    public void settID(String tID) {
        this.tID = tID;
    }

    public String getGpRequired() {
        return gpRequired;
    }

    public void setGpRequired(String gpRequired) {
        this.gpRequired = gpRequired;
    }

    public String getAvaGpCount() {
        return avaGpCount;
    }

    public void setAvaGpCount(String avaGpCount) {
        this.avaGpCount = avaGpCount;
    }

    public String getRedeemGpCount() {
        return redeemGpCount;
    }

    public void setRedeemGpCount(String redeemGpCount) {
        this.redeemGpCount = redeemGpCount;
    }

    public String getRedeemGpTnxAmt() {
        return redeemGpTnxAmt;
    }

    public void setRedeemGpTnxAmt(String redeemGpTnxAmt) {
        this.redeemGpTnxAmt = redeemGpTnxAmt;
    }

    public String getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(String redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public String getGpCode() {
        return gpCode;
    }

    public void setGpCode(String gpCode) {
        this.gpCode = gpCode;
    }

    public String getYearHolding() {
        return yearHolding;
    }

    public void setYearHolding(String yearHolding) {
        this.yearHolding = yearHolding;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getIsProm() {
        return isProm;
    }

    public void setIsProm(String isProm) {
        this.isProm = isProm;
    }

    /**
     * 是否是特别兑换率
     * A 大折日
     * Y 特别兑换率
     *
     * @return
     */
    public boolean isProm() {
        if (TextUtils.isEmpty(isProm)) {
            return false;
        } else {
            return isProm.equals("Y") || isProm.equals("A");
        }
    }


    public String getGpDateCmt() {
        return gpDateCmt;
    }

    public void setGpDateCmt(String gpDateCmt) {
        this.gpDateCmt = gpDateCmt;
    }


    public boolean getIsCommonQR() {
        if (TextUtils.isEmpty(isCommonQR)) {
            return false;
        }
        return isCommonQR.equals("1");
    }

    public void setIsCommonQR(String isCommonQR) {
        this.isCommonQR = isCommonQR;
    }


    public boolean isUrlCode() {
        return isUrlCode;
    }

    public void setUrlCode(boolean urlCode) {
        isUrlCode = urlCode;
    }


    public String getGpBalance() {
        return gpBalance;
    }

    public void setGpBalance(String gpBalance) {
        this.gpBalance = gpBalance;
    }


    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }


    public String getGpComment() {
        return gpComment;
    }

    public void setGpComment(String gpComment) {
        this.gpComment = gpComment;
    }


    public String getWxQrcode() {
        return wxQrcode;
    }

    public void setWxQrcode(String wxQrcode) {
        this.wxQrcode = wxQrcode;
    }


    public String getMerchantName() {
        if (TextUtils.isEmpty(merchantName)) {
            if (TextUtils.isEmpty(name)) {
                return "";
            }
            return name;
        }
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getIsFPS() {
        if (TextUtils.isEmpty(isFPS)) {
            return "0";
        }
        return isFPS;
    }

    public boolean canChangeCard() {
        if (TextUtils.isEmpty(wxQrcode)) {
            return false;
        }
        return !TextUtils.equals(wxQrcode, "1");
    }

    public void setIsFPS(String isFPS) {
        this.isFPS = isFPS;
    }

    public MpQrUrlInfoResult parseMqrCode() {
        MpQrUrlInfoResult mpQrUrlInfoResult = new MpQrUrlInfoResult();
        mpQrUrlInfoResult.setTranAmt(tranAmt);
        mpQrUrlInfoResult.setTranCur(tranCur);
        mpQrUrlInfoResult.setqRCUseCase(qRCUseCase);
        mpQrUrlInfoResult.setmCC(mCC);
        mpQrUrlInfoResult.setName(name);
        mpQrUrlInfoResult.setCountry(country);
        mpQrUrlInfoResult.setCityAL(cityAL);
        mpQrUrlInfoResult.setCity(city);
        mpQrUrlInfoResult.setNameAL(nameAL);
        //后台确认appcall app 写死是E
        mpQrUrlInfoResult.setQrcType("E");
        mpQrUrlInfoResult.setCardId(cardId);
        mpQrUrlInfoResult.setQrcodeStr(qrCode);
        return mpQrUrlInfoResult;
    }
}
