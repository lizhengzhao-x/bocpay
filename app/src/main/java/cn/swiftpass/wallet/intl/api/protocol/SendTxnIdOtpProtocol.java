package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @class name：LoginSendOtpProtocol
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/14 20:32
 * @change
 * @chang time
 * @class 登录OTP
 */
public class SendTxnIdOtpProtocol extends BaseProtocol {


    /**
     * 用户id
     */
    String txnId;


    public SendTxnIdOtpProtocol(String txnId, NetWorkCallbackListener dataCallback) {
        this.txnId = txnId;
        this.mDataCallback = dataCallback;
        mUrl = "api/crossTransfer/sendOTP";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);

    }
}
