package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.RegisterSetPaymentPswActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.GetVitualCardListContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.GetVitualCardListPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ScaleInTransformer;

public class VirtualCardBindTermsFragment extends BaseFragment<GetVitualCardListContract.Presenter> implements GetVitualCardListContract.View {

    @BindView(R.id.id_vitualcard_bind_confirm_mark)
    ImageView idVitualcardBindConfirmMark;
    @BindView(R.id.id_vitualcard_bind_conditions_next_btn)
    TextView idVitualcardBindConditionsNextBtn;

    @BindView(R.id.id_buttom_info)
    TextView idButtomTvView;


    @BindView(R.id.id_viewpager)
    ViewPager mViewPager;
    @BindView(R.id.id_top_card_view)
    FrameLayout idTopCardView;
    @BindView(R.id.id_vitualcard_bind_conditions)
    TextView idVitualcardBindConditions;
    @BindView(R.id.id_conditions_view)
    ConstraintLayout idConditionsView;
    @BindView(R.id.id_constrain_virtual_car_bind)
    LinearLayout idVirtualCarBindLayout;
    private VirtualCardListEntity virtualCardListEntity;
    private List<VirtualCardListEntity.VirtualCardListBean> mVirtualCardList;
    private int mCurrentSelPostion;
    //业务类型
    private int mTypeFlag;
    private Bundle virCardBindBundle;


    @Override
    public void initTitle() {
        if (virCardBindBundle == null) return;
        if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            mActivity.setToolBarTitle(R.string.VC02_01a_1);
            idVitualcardBindConditionsNextBtn.setEnabled(false);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            mActivity.setToolBarTitle(R.string.setting_payment_fgw);
            //忘记密码 不需要显示tnc
            idConditionsView.setVisibility(View.GONE);
            idButtomTvView.setVisibility(View.GONE);
            updateOkBackground(idVitualcardBindConditionsNextBtn, true);
        } else if (mTypeFlag == Constants.VITUALCARD_BIND) {
            mActivity.setToolBarTitle(R.string.VC02_01a_1);
            idVitualcardBindConditionsNextBtn.setEnabled(false);
        }
    }


    @Override
    protected GetVitualCardListContract.Presenter loadPresenter() {
        return new GetVitualCardListPresenter();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_vitualcard_bind_term;
    }


    @Override
    protected void initView(View v) {

        //获取传入Fragment的参数
        virCardBindBundle = getArguments();
        if (virCardBindBundle == null) {
            //显示错误信息
            showErrorMsgDialog(getActivity(), ErrorCode.VIRTUAL_LIST_FAILED.code);
            //隐藏该fragment
            idVirtualCarBindLayout.setVisibility(View.GONE);
            return;
        }

        mTypeFlag = virCardBindBundle.getInt(Constants.CURRENT_PAGE_FLOW, -1);
        virtualCardListEntity = (VirtualCardListEntity) virCardBindBundle.getSerializable(Constants.VIRTUALCARDLIST);

        int width = AndroidUtils.getScreenWidth(mContext);
        int height = (int) (width / 1.5);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(width, height);
        lp.topMargin = AndroidUtils.dip2px(mContext, 20);
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        idTopCardView.setLayoutParams(lp);
        mVirtualCardList = new ArrayList<>();
        initSpannableString();
        mVirtualCardList.addAll(virtualCardListEntity.getVirtualCardList());
        initViewPager();
    }


    private void initSpannableString() {
        BasicUtils.initSpannableStrWithTv(getString(R.string.VC02_01a_2), "##", idVitualcardBindConditions, new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                ActivitySkipUtil.startAnotherActivity(getActivity(), ServerAgreementActivity.class);
            }
        });
    }


    private void initViewPager() {
        mViewPager.setPageMargin(10);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setAdapter(new PagerAdapter() {
            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View rootView = LayoutInflater.from(mContext).inflate(R.layout.item_virtualcard_term_card_info, null);
                final View parentView = rootView.findViewById(R.id.id_linear_view);
                TextView tvCardView = rootView.findViewById(R.id.id_card_number);
                RoundedCorners roundedCorners = new RoundedCorners(10);
                RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
                VirtualCardListEntity.VirtualCardListBean cardEntity = mVirtualCardList.get(position);
                tvCardView.setText(AndroidUtils.formatCardMastNumberStr(cardEntity.getPan()));
                GlideApp.with(mContext).load(ApiConstant.CARD_ART_URL + cardEntity.getCrdArtId() + ".png").diskCacheStrategy(DiskCacheStrategy.RESOURCE).apply(options).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        parentView.setBackground(resource);
                    }
                });
                container.addView(rootView);
                return rootView;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                container.removeView((View) object);
            }

            @Override
            public int getCount() {
                return mVirtualCardList.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object o) {
                return view == o;
            }
        });
        mViewPager.setPageTransformer(true, new

                ScaleInTransformer());
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mCurrentSelPostion = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mCurrentSelPostion = 0;
        mViewPager.setCurrentItem(mCurrentSelPostion);
    }


    @Override
    public void vitualCardCheckSuccess(ContentEntity response) {
        if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
            mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, virtualCardListEntity.getVirtualCardList().get(mCurrentSelPostion));
            mHashMaps.put(Constants.WALLET_ID, virtualCardListEntity.getWalletId());
            ActivitySkipUtil.startAnotherActivity(getActivity(), RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mTypeFlag);
            mHashMaps.put(Constants.FORGET_PASSWORD_TYPE, "1");
            mHashMaps.put(Constants.MOBILE_PHONE, virtualCardListEntity.getAccount());
            ActivitySkipUtil.startAnotherActivity(getActivity(), RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            if (virtualCardListEntity != null && !virtualCardListEntity.getVirtualCardList().isEmpty()) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.VITUALCARD_BIND);
                mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, virtualCardListEntity.getVirtualCardList().get(mCurrentSelPostion));
                ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardBindSendOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        }
    }


    @Override
    public void vitualCardCheckError(final String errorCode, String errorMsg) {
        showErrorMsgDialog(getActivity(), errorMsg);
    }


    @OnClick({R.id.id_vitualcard_bind_conditions_next_btn, R.id.id_vitualcard_bind_confirm_mark})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_vitualcard_bind_conditions_next_btn:
                VirtualCardListEntity.VirtualCardListBean virtualCardListBean = virtualCardListEntity.getVirtualCardList().get(mCurrentSelPostion);
                String action = "";
                if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
                    action = ApiConstant.VIRTUALCARD_REGISTER;
                } else if (mTypeFlag == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                    action = ApiConstant.VIRTUALCARD_FORGETPWD;
                } else if (mTypeFlag == Constants.VITUALCARD_BIND) {
                    action = ApiConstant.VIRTUALCARD_BIND;
                }
                mPresenter.vitualCardCheck(virtualCardListBean.getPan(), virtualCardListBean.getExpDate(), action);
                break;

            case R.id.id_vitualcard_bind_confirm_mark:
                idVitualcardBindConfirmMark.setSelected(!idVitualcardBindConfirmMark.isSelected());
                updateOkBackground(idVitualcardBindConditionsNextBtn, idVitualcardBindConfirmMark.isSelected() && virtualCardListEntity != null && !virtualCardListEntity.getVirtualCardList().isEmpty());
                break;
            default:
                break;
        }

    }

}
