package cn.swiftpass.wallet.intl.module.redpacket.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/14.
 */
public class LishiHistoryEntity extends BaseEntity {


    private List<RedPacketBeanEntity> rows;

    private int total;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<RedPacketBeanEntity> getRows() {
        return rows;
    }

    public void setRows(List<RedPacketBeanEntity> rows) {
        this.rows = rows;
    }

}
