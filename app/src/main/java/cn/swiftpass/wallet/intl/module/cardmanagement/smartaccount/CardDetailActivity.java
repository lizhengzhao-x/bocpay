package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

/**
 * //银行卡单张卡详细信息
 */

public class CardDetailActivity extends BaseCompatActivity {
//    @BindView(R.id.id_card_no)
//    TextView idCardNo;
    @BindView(R.id.id_linear_view)
    LinearLayout idLinearView;
    @BindView(R.id.id_card_total_nunber)
    TextView idCardTotalNunber;
    private BankCardEntity cardEntity;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.AC2101_2_25);
        cardEntity = (BankCardEntity) getIntent().getSerializableExtra(Constants.CARD_ENTITY);
        ImageView faqIv = addToolBarRightViewToImage(R.mipmap.icon_navibar_morewhite);
        faqIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkCardMoreInfo();
            }
        });
        //需求改动隐藏卡号
//        idCardNo.setText(AndroidUtils.formatCardMastNumberStr(cardEntity.getPanShowNumber()));
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idLinearView.getLayoutParams();
        int margin = (int) mContext.getResources().getDimension(R.dimen.space_40_px);
        int width = AndroidUtils.getScreenWidth(mContext) - margin * 2;
        int height = width * 5 / 8;
        lp.width = width;
        lp.height = height;
        lp.gravity = Gravity.CENTER_HORIZONTAL;
        idLinearView.setLayoutParams(lp);
        RoundedCorners roundedCorners = new RoundedCorners(10);
        //通过RequestOptions扩展功能,override:采样率,因为ImageView就这么大,可以压缩图片,降低内存消耗
        RequestOptions options = RequestOptions.bitmapTransform(roundedCorners);
        GlideApp.with(mContext)
                .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                .apply(options)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        idLinearView.setBackground(resource);
                    }
                });


        idCardTotalNunber.setText(String.format(getString(R.string.card_info_number_total), AndroidUtils.formatCardMastNumber(cardEntity.getPanShowNumber())));
    }


    /**
     * 卡片详情页
     */
    private void checkCardMoreInfo() {

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CARD_ENTITY, cardEntity);
        if (cardEntity.isDefaultCard()) {
            //删除的是默认卡 就默认下一张为新的默认卡
            if (CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity() != null) {
                mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity().getCardId());
            }
        } else {
            //删除的不是默认卡 要找一个剩下的默认卡id
            BankCardEntity entity = CacheManagerInstance.getInstance().getDefaultCardEntitity();
            String cardId = "";
            if (null != entity) {
                cardId = entity.getCardId();
            }
            mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, cardId);
        }
        ActivitySkipUtil.startAnotherActivity(CardDetailActivity.this, CardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        //绑卡成功之后页面销毁
        if (event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_carditem_detail;
    }


}
