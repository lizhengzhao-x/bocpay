package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;

public class EveryMonthCreditCardRewardFragment extends MyCreditCardRewardBaseFragment {

    public EveryMonthCreditCardRewardFragment() {
        super(CreditRewardType.REWARD_DATE_TYPE_EVERY_MONTH);
    }
}