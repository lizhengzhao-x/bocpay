package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * 转账方式对象
 */
public class TransferPatternEntity extends BaseEntity {

    private String bankAccountPayment;      //银行账户转账：1显示，0不显示
    private String crossBorderPayment;      //跨境缴费：1显示，0不显示
    private String fpsPayment;              //fps转账：1显示，0不显示
    private String giftActivity;            //派利是：1显示，0不显示
    private String fpsOrBocPay;             //FPS/BoC Pay转账：1显示，0不显示

    public boolean getBankAccountPayment() {
        return isBoolean(bankAccountPayment);
    }

    public void setBankAccountPayment(String bankAccountPayment) {
        this.bankAccountPayment = bankAccountPayment;
    }

    public boolean getCrossBorderPayment() {
        return isBoolean(crossBorderPayment);
    }

    public void setCrossBorderPayment(String crossBorderPayment) {
        this.crossBorderPayment = crossBorderPayment;
    }

    public boolean getFpsPayment() {
        return isBoolean(fpsPayment);
    }

    public void setFpsPayment(String fpsPayment) {
        this.fpsPayment = fpsPayment;
    }

    public boolean getGiftActivity() {
        return isBoolean(giftActivity);
    }

    public void setGiftActivity(String giftActivity) {
        this.giftActivity = giftActivity;
    }

    public boolean getFpsOrBocPay() {
        return isBoolean(fpsOrBocPay);
    }

    public void setFpsOrBocPay(String fpsOrBocPay) {
        this.fpsOrBocPay = fpsOrBocPay;
    }

    private  boolean isBoolean(String str){
        return "1".equals(str) ? true : false;
    }

}
