package cn.swiftpass.wallet.intl.event;

import static cn.swiftpass.wallet.intl.event.AppCallAppEvent.APP_LINK_URL;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import cn.swiftpass.wallet.intl.utils.LogUtils;


public class UserLoginEventManager {

    public static final String TAG = "UserLoginEventManager";
    private static UserLoginEventManager instance;
    private static final Object INSTANCE_LOCK = new Object();

    public static List<BaseEventType> afterUserLoginEventList;

    public UserLoginEventManager() {
        afterUserLoginEventList = new LinkedList<>();
        initEvent();
    }

    public static UserLoginEventManager getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new UserLoginEventManager();
                }
            }
        }
        return instance;
    }


    enum EventPriority {
        EVENT_APPCALLAPP(0, "APP call App 支付"),
        EVENT_REGISTERBANNER(10, "注册成功 迎新banner & UPlan優惠券Welcome Package"),
        EVENT_PUSHNOTIFICATION(20, "通知栏点击推送弹框事件"),
        EVENT_PROMOTEDIALOG(30, "自动登录 推广通知"),
        EVENT_MODIFYPASSWORD(40, "账户90天未修改密码 修改密码弹框"),
        EVENT_UPGRADEDIALOG(50, "APP升级 推广通知弹框"),
        EVENT_LBSDIALOG(60, "更换设备之后LBS弹框");
        public final int eventPriority;
        public final String eventName;

        EventPriority(int eventPriority, String eventName) {
            this.eventPriority = eventPriority;
            this.eventName = eventName;
        }
    }

    private void initEvent() {
//        UserLoginEventManager.getInstance().addUserLoginEvent(new PushNotificationEvent());
//        UserLoginEventManager.getInstance().addUserLoginEvent(new LBSDialogEvent());
    }

    /**
     * 将需要登录成功处理的时间添加进去 要结合事件的优先级来进行添加
     *
     * @param eventType
     */
    public void addUserLoginEvent(BaseEventType eventType) {
        int shouldInsertEventPosition = 0;
        boolean hasFindPosition = false;
        if (afterUserLoginEventList != null) {
            for (int i = 0; i < afterUserLoginEventList.size(); i++) {
                if (afterUserLoginEventList.get(i).getCurrentEventPriority() > eventType.getCurrentEventPriority()) {
                    shouldInsertEventPosition = i;
                    hasFindPosition = true;
                    break;
                }
            }
            if (!hasFindPosition) {
                shouldInsertEventPosition = afterUserLoginEventList.size();
            }
            afterUserLoginEventList.add(shouldInsertEventPosition, eventType);
        } else {
            LogUtils.e(TAG, "afterUserLoginEventList is null error.....");
        }
    }

    public boolean dealWithEvent() {
        for (BaseEventType baseEventType : afterUserLoginEventList) {
            boolean isSuccess = baseEventType.dealWithEvent();
            if (isSuccess) {
                clearAllEvent();
                return true;
            }
        }
        clearAllEvent();
        return false;
    }


    /**
     * 获取集合中具体的事件类型
     *
     * @param c
     * @param <T>
     * @return
     */
    public <T extends BaseEventType> T getEventDetail(Class c) {
        for (BaseEventType baseEventType : afterUserLoginEventList) {
            if (c.equals(baseEventType.getClass())) {
                return (T) baseEventType;
            }
        }
        return null;
    }


    /**
     * 是否要退出appcallapp流程
     *
     * @return
     */
    public boolean isLeaveAppCallAppEvent() {
        AppCallAppEvent appCallAppEvent = getEventDetail(AppCallAppEvent.class);
        if (appCallAppEvent == null) {
            return false;
        } else {
            if (appCallAppEvent.getCurrentEventHashParams().get(APP_LINK_URL) != null) {
                return true;
//                String appCallAppEventType = (String) appCallAppEvent.getCurrentEventHashParams().get(APP_CALL_APP_TTPE);
//                return TextUtils.equals(appCallAppEventType, AppCallAppEvent.APPCALLAPPTTPE.APP_CALL_APP_PAYMENT.name());
            } else {
                return false;
            }
        }
    }

    /**
     * 当某个事件处理完之后，需要将事件移除
     *
     * @param c
     */
    public void removeItemEvent(Class c) {
        if (afterUserLoginEventList == null) return;
        Iterator<BaseEventType> iterator = afterUserLoginEventList.iterator();
        while (iterator.hasNext()) {
            BaseEventType itemEvent = iterator.next();
            if (itemEvent.getClass().equals(c)) {
                afterUserLoginEventList.remove(itemEvent);
                return;
            }
        }
    }

    public void clearAllEvent() {
        if (afterUserLoginEventList == null) return;
        afterUserLoginEventList.clear();
        afterUserLoginEventList = null;
        instance = null;
    }
}
