package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

/**
 * 加载更多监听器
 */
public interface OnRewadLoadMoreListener {
    void onLoadMore();
}
