package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivitityListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;

/**
 * 电子券活动列表
 */
public class ECouponsActivitiesContract {

    public interface View extends IView {
        void getActivitiesListSuccess(EcouponActivitityListEntity response);

        void getActivitiesListError(String errorCode, String errorMsg);

        void getUplanUrlSuccess(UplanUrlEntity response);

        void getUplanUrlError(String errorCode, String errorMsg);


    }


    public interface Presenter extends IPresenter<ECouponsActivitiesContract.View> {
        void getActivitiesList();

        void getUplanUrl();
    }

}
