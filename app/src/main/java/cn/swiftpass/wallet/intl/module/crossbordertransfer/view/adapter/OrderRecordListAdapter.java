package cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.utils.CreateOneDiCodeUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.scan.backscan.ScaleBarCodeActivity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.CustomeLineText;

import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel.ItemModelBuilder.STYLE_BIG_TEXT;
import static cn.swiftpass.wallet.intl.module.crossbordertransfer.view.adapter.ItemModel.ItemModelBuilder.STYLE_LINE;
import static cn.swiftpass.wallet.intl.module.transactionrecords.presenter.OrderDetailPresenter.SPECIAL_CHAR;

public class OrderRecordListAdapter extends RecyclerView.Adapter {
    private int shareResourceId[] = {R.mipmap.icon_app_whatsapp, R.mipmap.icon_app_wechat, R.mipmap.icon_app_line, R.mipmap.icon_app_other};
    private Context mContext;
    private List<ItemModel> mList;
    private LayoutInflater mLayoutInflater;
    private OnItemShareListener onItemShareListener;

    public OrderRecordListAdapter(Context mContext, OnItemShareListener onItemShareListenerIn) {
        this.mContext = mContext;
        mList = new ArrayList<>();
        mLayoutInflater = LayoutInflater.from(mContext);
        this.onItemShareListener = onItemShareListenerIn;
    }


    @Override
    public int getItemViewType(int position) {
        ItemModel itemModel = mList.get(position);
        int itemType = itemModel.getItemType();
        return itemType;
    }

    @Override
    public int getItemCount() {
        if (mList != null) {
            return mList.size();
        }
        return 0;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ItemType.HEAD) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.ordershare_list_item1, parent,false);
            return new HeaderViewHolder(view);
        } else if (viewType == ItemType.TAIL) {
            View view = View.inflate(mContext, R.layout.cross_list_item3, null);
            return new TailViewHolder(view);
        } else if (viewType == ItemType.BARCODE) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.orderdetail_barcode, parent, false);
            return new BarCodeViewHolder(view);
        } else if (viewType == ItemType.MONEY) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.ordershare_money_list_item, parent, false);
            return new MoneyViewHolder(view);
        } else {
            View view = View.inflate(mContext, R.layout.ordershare_item2, null);
            return new BodyViewHolder(view);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int itemViewType = getItemViewType(position);
        ItemModel itemModel = mList.get(position);
        boolean visibleLine = itemModel.isVisibleLine();

        switch (itemViewType) {
            case ItemType.HEAD:
                HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
                headerHolder.leftImage.setImageResource(itemModel.getResourceImg());
                headerHolder.content.setText(itemModel.getItemTitle());
                break;
            case ItemType.BODY:
                BodyViewHolder bodyViewHolder = (BodyViewHolder) holder;
                if (itemModel.getContentTextSize() != 0) {
                    bodyViewHolder.clt.getmContentText().setTextSize(itemModel.getContentTextSize());
                }
                bodyViewHolder.clt.setContentText(itemModel.getItemContent());
                bodyViewHolder.clt.setTitleText(itemModel.getItemTitle());
                if (itemModel.isContentGone()) {
                    bodyViewHolder.clt.getmContentText().setVisibility(View.GONE);
                } else {
                    bodyViewHolder.clt.getmContentText().setVisibility(View.VISIBLE);
                }
                if (itemModel.getItemContentColor() != 0) {
                    bodyViewHolder.clt.getmTitleText().setTextColor(itemModel.getItemContentColor());
                }
                 if (itemModel.getTextStatus() == STYLE_LINE) {
                     bodyViewHolder.clt.getmContentText().getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                }
                bodyViewHolder.mBodyLine.setVisibility(visibleLine ? View.VISIBLE : View.GONE);
                break;
            case ItemType.MONEY:
                MoneyViewHolder moneyViewHolder = (MoneyViewHolder) holder;
                String content = itemModel.getItemContent();
                if (content.contains(SPECIAL_CHAR)) {
                    moneyViewHolder.montyCurrency.setVisibility(View.VISIBLE);
                    String items[] = content.split(SPECIAL_CHAR);
                    moneyViewHolder.moncyDetail.setText(items[1]);
                    moneyViewHolder.montyCurrency.setText(items[0] + " ");
                } else {
                    moneyViewHolder.montyCurrency.setVisibility(View.INVISIBLE);
                    moneyViewHolder.moncyDetail.setText(content);
                }
                moneyViewHolder.leftTitle.setText(itemModel.getItemTitle());
                if (itemModel.getTextStatus() == STYLE_BIG_TEXT) {

                } else if (itemModel.getTextStatus() == STYLE_LINE) {
//                    moneyViewHolder.moncyDetail.getPaint().setAntiAlias(true);
//                    moneyViewHolder.montyCurrency.getPaint().setAntiAlias(true);
                    moneyViewHolder.moncyDetail.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    moneyViewHolder.montyCurrency.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    //原价字段字体大小特殊处理
                    moneyViewHolder.moncyDetail.setTextSize(13);
                    moneyViewHolder.montyCurrency.setTextSize(13);
                }
                if (itemModel.getItemContentColor() != 0) {
                    moneyViewHolder.moncyDetail.setTextColor(itemModel.getItemContentColor());
                    moneyViewHolder.montyCurrency.setTextColor(itemModel.getItemContentColor());
                }
                if (itemModel.getItemTitleColor() != 0) {
                    moneyViewHolder.leftTitle.setTextColor(itemModel.getItemTitleColor());
                }
                moneyViewHolder.mBodyLine.setVisibility(visibleLine ? View.VISIBLE : View.GONE);
                break;

            case ItemType.TAIL:
                TailViewHolder tailViewHolder = (TailViewHolder) holder;
                boolean isShow =itemModel.isShowGradeReduceBtn();
                tailViewHolder.mGradeReduce.setVisibility(isShow?View.VISIBLE:View.GONE);

                break;

            case ItemType.BARCODE:
                BarCodeViewHolder barCodeViewHolder = (BarCodeViewHolder) holder;
                int width = AndroidUtils.getScreenWidth(mContext);
                int w = (int) (width * 0.8);
                int h = (int) (w * 0.2);
                Bitmap tempBitmap = CreateOneDiCodeUtil.createCode(itemModel.getItemContent(), w, h);
                barCodeViewHolder.barCodeImage.setImageBitmap(tempBitmap);
                barCodeViewHolder.tvBarCode.setText(itemModel.getItemContent());
                barCodeViewHolder.barCodeImage.setOnClickListener(new OnProhibitFastClickListener() {
                    @Override
                    public void onFilterClick(View v) {
                        Intent intent = new Intent(mContext, ScaleBarCodeActivity.class);
                        intent.putExtra(Constants.DATA_ORDER_NUMBER, itemModel.getItemContent());
                        mContext.startActivity(intent);
                    }
                });
                barCodeViewHolder.infoLogo.setOnClickListener(new OnProhibitFastClickListener() {
                    @Override
                    public void onFilterClick(View v) {
                        if (onItemShareListener != null) {
                            onItemShareListener.onBarCodeClick();
                        }
                    }
                });

                break;
        }
    }


    public void refresh(List<ItemModel> list) {
        mList.clear();
        mList.addAll(list);
        notifyDataSetChanged();
    }


    class HeaderViewHolder extends RecyclerView.ViewHolder {
        public ImageView leftImage;
        public TextView content;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            leftImage = itemView.findViewById(R.id.id_order_header_iv);
            content = itemView.findViewById(R.id.id_order_header_txt);

        }
    }

    class MoneyViewHolder extends RecyclerView.ViewHolder {
        public TextView leftTitle;
        public TextView montyCurrency;
        public TextView moncyDetail;
        public CustomeLineText clt;
        public View mBodyLine;
        public MoneyViewHolder(@NonNull View itemView) {
            super(itemView);
            montyCurrency = itemView.findViewById(R.id.text_money_currency);
            moncyDetail = itemView.findViewById(R.id.text_money_content);
            leftTitle = itemView.findViewById(R.id.text_title_tv);
            mBodyLine = itemView.findViewById(R.id.body_line);
        }
    }

    private class TailViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout shareView;
        public TextView mSaveToLocal;
        public TextView mGradeReduce;

        public TailViewHolder(@NonNull View itemView) {
            super(itemView);
            shareView = itemView.findViewById(R.id.ll_share_item);
            mSaveToLocal = itemView.findViewById(R.id.save_detail_tv);
            mGradeReduce = itemView.findViewById(R.id.tv_grade_reduce);
            initShareButtonGroup(shareView);
            mSaveToLocal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemShareListener != null) {
                        onItemShareListener.OnItemShare(-1);
                    }
                }
            });
            mGradeReduce.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (onItemShareListener != null) {
                        onItemShareListener.OnItemGradeReduce(-1);
                    }
                }
            });

        }
    }

    private class BodyViewHolder extends RecyclerView.ViewHolder {
        public CustomeLineText clt;
        public View mBodyLine;

        public BodyViewHolder(@NonNull View itemView) {
            super(itemView);
            clt = itemView.findViewById(R.id.clt);
            mBodyLine = itemView.findViewById(R.id.body_line);
        }
    }

    private class BarCodeViewHolder extends RecyclerView.ViewHolder {
        public ImageView barCodeImage;
        public ImageView infoLogo;
        public TextView tvBarCode;

        public BarCodeViewHolder(@NonNull View itemView) {
            super(itemView);
            barCodeImage = itemView.findViewById(R.id.id_barcode);
            tvBarCode = itemView.findViewById(R.id.id_barcode_title);
            infoLogo = itemView.findViewById(R.id.iv_question_mark);
        }
    }

    private void initShareButtonGroup(LinearLayout shareView) {
        int imageWidth = AndroidUtils.dip2px(mContext, 45);
        int itemMargin = (AndroidUtils.getScreenWidth(mContext) - imageWidth * 4) / 5;
        for (int i = 0; i < shareResourceId.length; i++) {
            ImageView imageView = new ImageView(mContext);
            imageView.setImageResource(shareResourceId[i]);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(imageWidth, imageWidth);
            layoutParams.leftMargin = itemMargin;
            imageView.setLayoutParams(layoutParams);
            final int finalI = i;
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    shareImageWithPosition(finalI);
                }
            });
            shareView.addView(imageView);
        }
    }


    /**
     * 具体采用哪种方式分享
     *
     * @param positon
     */
    private void shareImageWithPosition(final int positon) {

        if (onItemShareListener != null) {
            onItemShareListener.OnItemShare(positon);
        }

    }

    public interface OnItemShareListener {

        void OnItemShare(int position);
        void OnItemGradeReduce(int position);

        void onBarCodeClick();
    }


}
