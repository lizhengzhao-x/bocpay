package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author create date on  on 2018/9/3 13:50
 * 获取我的账户信息，返回对象为 MySmartAccountEntity  账户和转账用这个
 * e2ee
 */

public class MySmartAccountProtocol extends BaseProtocol {
    public static final String TAG = MySmartAccountProtocol.class.getSimpleName();


    public MySmartAccountProtocol(MySmartAccountCallbackListener dataCallback) {
        mUrl = "api/smartAccountManager/getSmartInfo";
        this.mDataCallback = dataCallback;

    }
}
