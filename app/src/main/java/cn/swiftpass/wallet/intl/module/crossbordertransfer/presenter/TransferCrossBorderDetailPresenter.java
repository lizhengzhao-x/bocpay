package cn.swiftpass.wallet.intl.module.crossbordertransfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.ConfirmTransferProtocol;
import cn.swiftpass.wallet.intl.api.protocol.getTransferCrossBorderByTxIdProtocol;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.contract.TransferCrossBorderDetailContract;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;

;


public class TransferCrossBorderDetailPresenter extends BasePresenter<TransferCrossBorderDetailContract.View> implements TransferCrossBorderDetailContract.Presenter {


    @Override
    public void getTransferDetail(String txId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new getTransferCrossBorderByTxIdProtocol(txId, new NetWorkCallbackListener<TransferCrossBorderDetailEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferDetailError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(TransferCrossBorderDetailEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferDetailSuccess(response);
                }
            }
        }).execute();

    }

    @Override
    public void getTransferConfrim(String txId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ConfirmTransferProtocol(txId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferConfrimError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTransferConfrimSuccess(response);
                }
            }
        }).execute();
    }

}
