package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 卡列表查询，返回对象为 AutoLoginSucEntity
 */

public class TransferSummaryProtocol extends BaseProtocol {
    public static final String TAG = TransferSummaryProtocol.class.getSimpleName();

//    pgSz
//    String	O	页面大小，默认10
//    nxtRecKey	String	O	下一条记录rowid，翻页时需要
//    frTxnDt	String	O	起止时间 yyyyMMdd
//    toTxnDt	String	O	截至时间 yyyyMMdd

    private String pgSz;
    private String nxtRecKey;
    private String frTxnDt;
    private String toTxnDt;


    public TransferSummaryProtocol(String pgSz, String nxtRecKey, String frTxnDt, String toTxnDt, NetWorkCallbackListener dataCallback) {

        this.pgSz = pgSz;
        this.toTxnDt = toTxnDt;
        this.nxtRecKey = nxtRecKey;
        this.frTxnDt = frTxnDt;
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferSummaryEnquiry";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PAGESZ, pgSz);
    }


}
