package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class SmartAccountBillListEntity extends BaseEntity {


    // bocpay 付款
    public static final String SDR = "SDR";
    // fps 支付
    public static final String DR = "DR";

    // fps 收款
    public static final String CR = "CR";
    // bocpay 收款
    public static final String SCR = "SCR";

    // 积分抵销签账
    public static final String PNT = "PNT";


    // 银联消费
    public static final String PUR = "PUR";
    // 银联消费冲正
    public static final String PUC = "PUC";
    // 银联消费撤销
    public static final String VUR = "VUR";
    // 银联消费撤销冲正
    public static final String VUC = "VUC";


    // 增值
    public static final String TOP = "TOP";
    public static final String ATP = "ATP";
    public static final String MTP = "MTP";
    public static final String ETO = "ETO";

    // 转回余额
    public static final String ATR = "ATR";
    public static final String TFR = "TFR";

    public OrderBean getOrderList() {
        return orderList;
    }

    public void setOrderList(OrderBean orderList) {
        this.orderList = orderList;
    }

    public String getUiuxFunction() {
        return uiuxFunction;
    }

    public void setUiuxFunction(String uiuxFunction) {
        this.uiuxFunction = uiuxFunction;
    }

    public boolean useNewUI() {
        if (TextUtils.isEmpty(uiuxFunction)) return false;
        return TextUtils.equals(uiuxFunction, "1");
    }

    /**
     * uiuxFunction  0：关闭uiux优化  1：打开uiux优化
     */
    private String uiuxFunction;
    private OrderBean orderList;

    public List<OutgroupBean> getOutgroup() {
        return outgroup;
    }

    public void setOutgroup(List<OutgroupBean> outgroup) {
        this.outgroup = outgroup;
    }

    private List<OutgroupBean> outgroup;

    public static class OrderBean extends BaseEntity {
        /**
         * endFlg : N
         * nextTstmp : 2018-09-10-10.49.21.079988
         * outgroup : [{"outtxsts":"S","outamtsign":"+","outtxchannel":"EWA","uttxtime":"163704","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"5503","outtxamt":"2.00","outrejtype":" ","outtxrefno":"2101250000021","outtxtime":"163704","outtxdesc":"MANUAL TOP-UP FROM PRI AC","outtxtype":"MTP"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"FPP","uttxtime":"163352","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.00","outrejtype":" ","outtxrefno":"2101250000020","outtxtime":"163352","outtxdesc":"FPP DEBIT                ","outtxtype":"DR "},{"outtxsts":"S","outamtsign":"-","outtxchannel":"EWA","uttxtime":"163244","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"0.99","outrejtype":" ","outtxrefno":"2101250000019","outtxtime":"163244","outtxdesc":"PAYMENT                  ","outtxtype":"PAY"},{"outtxsts":"S","outamtsign":"+","outtxchannel":"EWA","uttxtime":"163224","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"5503","outtxamt":"22.00","outrejtype":" ","outtxrefno":"2101250000018","outtxtime":"163224","outtxdesc":"MANUAL TOP-UP FROM PRI AC","outtxtype":"MTP"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"163148","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"5.00","outrejtype":" ","outtxrefno":"2101250000016","outtxtime":"163148","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"163049","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.59","outrejtype":" ","outtxrefno":"2101250000015","outtxtime":"163049","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"162754","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.53","outrejtype":" ","outtxrefno":"2101250000014","outtxtime":"162754","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"162534","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.44","outrejtype":" ","outtxrefno":"2101250000013","outtxtime":"162534","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"155815","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.88","outrejtype":" ","outtxrefno":"2101250000006","outtxtime":"155815","outtxdesc":"UPI REFUND               ","outtxtype":"VUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"155642","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"2.88","outrejtype":" ","outtxrefno":"2101250000005","outtxtime":"155642","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"155155","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"1.95","outrejtype":" ","outtxrefno":"2101250000004","outtxtime":"155155","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"UPI","uttxtime":"154821","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"11.46","outrejtype":" ","outtxrefno":"2101250000003","outtxtime":"154821","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"},{"outtxsts":"S","outamtsign":"-","outtxchannel":"EWA","uttxtime":"140628","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"23.00","outrejtype":" ","outtxrefno":"2101110000259","outtxtime":"140628","outtxdesc":"PAYMENT                  ","outtxtype":"PAY"},{"outtxsts":"F","outamtsign":"-","outtxchannel":"UPI","uttxtime":"115618","outtxccy":"HKD","outtxdate":"2018-09-11","outtxacno":"0000","outtxamt":"0.98","outrejtype":"G","outtxrefno":"0000000000000","outtxtime":"115618","outtxdesc":"UPI WITHDRAWAL           ","outtxtype":"PUR"}]
         */

        private String endFlg;
        private String nextTstmp;
        private List<OutgroupBean> outgroup;


        public String getEndFlg() {
            return endFlg;
        }

        public void setEndFlg(String endFlg) {
            this.endFlg = endFlg;
        }

        public String getNextTstmp() {
            return nextTstmp;
        }

        public void setNextTstmp(String nextTstmp) {
            this.nextTstmp = nextTstmp;
        }

        public List<OutgroupBean> getOutgroup() {
            return outgroup;
        }

        public void setOutgroup(List<OutgroupBean> outgroup) {
            this.outgroup = outgroup;
        }


    }


    public static class OutgroupBean extends BaseEntity {


        public boolean isUplanInfo() {

            if (TextUtils.isEmpty(recordType)) {
                return false;
            }
            return TextUtils.equals(recordType, "6");
        }
        //App判断是否展示积分换领详情页 1:是 0：否
        private String gpDetail;
        private String originalAmt;
        private String giftActivity;
        private String showType;
        private String postscript;
        private String uiType;//ui界面使用区别是标题还是内容
        private String outtxsts;
        private String outamtsign;
        private String outtxchannel;
        private String uttxtime;
        private String outtxccy;
        private String outtxdate;
        private String outtxacno;
        private String outtxamt;
        private String outrejtype;
        private String outtxrefno;
        private String outtrfrefno;
        private String outtxtime;
        private String outtxdesc;
        private String outtxtype;
        private String txnType;
        private String uiTitle;
        private String merchantName;




        public String getTxTitle() {
            return txTitle;
        }

        public void setTxTitle(String txTitle) {
            this.txTitle = txTitle;
        }

        /**
         * 用于详情页 增值及奖赏 类型的标题展示
         */
        private String txTitle;


        /**
         * panFour
         */
        private String panFour;

        public String getPanFour() {
            return panFour;
        }

        public void setPanFour(String panFour) {
            this.panFour = panFour;
        }

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        /**
         * card type 1：信用卡 2：智能账户
         */
        private String cardType;


        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        private String paymentType;

        public String getAcMainNo() {
            return acMainNo;
        }

        public void setAcMainNo(String acMainNo) {
            this.acMainNo = acMainNo;
        }

        private String acMainNo;
        /**
         * 我的账户级别 1：S1  2:S2  3:S3
         */
        private String smartAcLevel;
        //1:跨境汇款 2:银联交易 3:转账
        private String recordType;
        private String transactionNum;


        /**
         * // bocpay 收款
         * public static final String SCR = "SCR";
         * // bocpay 付款
         * public static final String SDR = "SDR";
         * // fps 支付
         * public static final String DR = "DR";
         * // fps 收款
         * public static final String CR = "CR";
         * // 积分抵销签账
         * public static final String PNT = "PNT";
         * <p>
         * <p>
         * // 银联消费
         * public static final String PUR = "PUR";
         * // 银联消费冲正
         * public static final String PUC = "PUC";
         * // 银联消费撤销
         * public static final String VUR = "VUR";
         * // 银联消费撤销冲正
         * public static final String VUC = "VUC";
         * <p>
         * <p>
         * // 增值
         * public static final String TOP = "TOP";
         * public static final String ATP = "ATP";
         * public static final String MTP = "MTP";
         * public static final String ETO = "ETO";
         * <p>
         * // 转回余额
         * public static final String ATR = "ATR";
         * public static final String TFR = "TFR";
         */
        private String orderType;


        public void setRecordType(String recordType) {
            this.recordType = recordType;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getMerchantName() {
            return merchantName;
        }

        public void setMerchantName(String merchantName) {
            this.merchantName = merchantName;
        }


        public String getPostscript() {
            return postscript;
        }

        public void setPostscript(String postscript) {
            this.postscript = postscript;
        }

        public String getOriginalAmt() {
            return originalAmt;
        }

        public void setOriginalAmt(String originalAmt) {
            this.originalAmt = originalAmt;
        }

        /**
         * outtxsts : S
         * outamtsign : +
         * outtxchannel : EWA
         * uttxtime : 163704
         * outtxccy : HKD
         * outtxdate : 2018-09-11
         * outtxacno : 5503
         * outtxamt : 2.00
         * outrejtype :
         * outtxrefno : 2101250000021
         * outtxtime : 163704
         * outtxdesc : MANUAL TOP-UP FROM PRI AC
         * outtxtype : MTP
         */
        public String getGpDetail() {
            return gpDetail;
        }

        public void setGpDetail(String gpDetail) {
            this.gpDetail = gpDetail;
        }


        /**
         * 跨境汇款
         *
         * @return
         */
        public boolean isCrossBorderTransfer() {

            if (TextUtils.isEmpty(recordType)) {
                return false;
            }
            return TextUtils.equals(recordType, "1");
        }

        public String getGiftActivity() {
            return giftActivity;
        }

        public void setGiftActivity(String giftActivity) {
            this.giftActivity = giftActivity;
        }


        public String getShowType() {
            return showType;
        }

        public void setShowType(String showType) {
            this.showType = showType;
        }

        public String getUiTitle() {
            return uiTitle;
        }

        public void setUiTitle(String uiTitle) {
            this.uiTitle = uiTitle;
        }


        public String getUiType() {
            return uiType;
        }

        public void setUiType(String uiType) {
            this.uiType = uiType;
        }


        public String getTxnType() {
            return txnType;
        }

        public void setTxnType(String txnType) {
            this.txnType = txnType;
        }


        public String getOuttrfrefno() {
            return outtrfrefno;
        }

        public void setOuttrfrefno(String outtrfrefno) {
            this.outtrfrefno = outtrfrefno;
        }


        public String getTransactionNum() {
            return transactionNum;
        }

        public void setTransactionNum(String transactionNum) {
            this.transactionNum = transactionNum;
        }


        public String getRecordType() {
            return recordType;
        }

        public void setrecordType(String recordType) {
            this.recordType = recordType;
        }


        public String getSmartAcLevel() {
            return smartAcLevel;
        }

        public void setSmartAcLevel(String smartAcLevel) {
            this.smartAcLevel = smartAcLevel;
        }


        public String getOuttxsts() {
            return outtxsts;
        }

        public void setOuttxsts(String outtxsts) {
            this.outtxsts = outtxsts;
        }

        public String getOutamtsign() {
            return outamtsign;
        }

        public void setOutamtsign(String outamtsign) {
            this.outamtsign = outamtsign;
        }

        public String getOuttxchannel() {
            return outtxchannel;
        }

        public void setOuttxchannel(String outtxchannel) {
            this.outtxchannel = outtxchannel;
        }

        public String getUttxtime() {
            return uttxtime;
        }

        public void setUttxtime(String uttxtime) {
            this.uttxtime = uttxtime;
        }

        public String getOuttxccy() {
            return outtxccy;
        }

        public void setOuttxccy(String outtxccy) {
            this.outtxccy = outtxccy;
        }

        public String getOuttxdate() {
            return outtxdate;
        }

        public void setOuttxdate(String outtxdate) {
            this.outtxdate = outtxdate;
        }

        public String getOuttxacno() {
            return outtxacno;
        }

        public void setOuttxacno(String outtxacno) {
            this.outtxacno = outtxacno;
        }

        public String getOuttxamt() {
            return outtxamt;
        }

        public void setOuttxamt(String outtxamt) {
            this.outtxamt = outtxamt;
        }

        public String getOutrejtype() {
            return outrejtype;
        }

        public void setOutrejtype(String outrejtype) {
            this.outrejtype = outrejtype;
        }

        public String getOuttxrefno() {
            return outtxrefno;
        }

        public void setOuttxrefno(String outtxrefno) {
            this.outtxrefno = outtxrefno;
        }

        public String getOuttxtime() {
            return outtxtime;
        }

        public void setOuttxtime(String outtxtime) {
            this.outtxtime = outtxtime;
        }

        public String getOuttxdesc() {
            return outtxdesc;
        }

        public void setOuttxdesc(String outtxdesc) {
            this.outtxdesc = outtxdesc;
        }

        public String getOuttxtype() {
            return outtxtype;
        }

        public void setOuttxtype(String outtxtype) {
            this.outtxtype = outtxtype;
        }
    }
}
