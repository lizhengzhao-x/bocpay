package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class VirtualUrlEntity extends BaseEntity {

    private String virtualCardUrl;
    private String virtualCardShareUrl;

    public String getVirtualCardUrl() {
        return virtualCardUrl;
    }

    public void setVirtualCardUrl(String virtualCardUrl) {
        this.virtualCardUrl = virtualCardUrl;
    }

    public String getVirtualCardShareUrl() {
        return virtualCardShareUrl;
    }

    public void setVirtualCardShareUrl(String virtualCardShareUrl) {
        this.virtualCardShareUrl = virtualCardShareUrl;
    }
}
