package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;

import androidx.annotation.NonNull;

import cn.swiftpass.wallet.intl.R;

public class ToastDialog extends Dialog {

    private static ToastDialog toastDialog = null;

    public ToastDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    public static ToastDialog createToastDialog(Context context) {
        toastDialog = new ToastDialog(context, R.style.CustomProgressDialog);
        toastDialog.setContentView(R.layout.dialog_toast);
        toastDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        toastDialog.getWindow().setDimAmount(0f);
        return toastDialog;
    }


}
