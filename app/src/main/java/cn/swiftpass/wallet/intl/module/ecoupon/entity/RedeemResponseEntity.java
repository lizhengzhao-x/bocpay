package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/7/16.
 */
public class RedeemResponseEntity extends BaseEntity {


    /**
     * resultRedm : [{"aswItemCode":"88880","eVoucherQty":"1","giftCode":"66660"},{"aswItemCode":"88881","eVoucherQty":"1","giftCode":"66661"},{"aswItemCode":"88882","eVoucherQty":"1","giftCode":"66662"},{"aswItemCode":"88883","eVoucherQty":"1","giftCode":"66663"}]
     * stockStatus : 0
     * gpBalance : 29960000
     * refNo : A12345678910
     * redeemPnt : 40000
     */

    /**
     * 标识 0：正常--直接做换领 1：有库存不足电子券
     */
    private String stockStatus;
    private String gpBalance;
    private String refNo;
    private String redeemPnt;
    /**
     * ：智能账户
     */
    private String smaGp;

    public String getSmaGp() {
        return smaGp;
    }

    public void setSmaGp(String smaGp) {
        this.smaGp = smaGp;
    }

    public String getCcGp() {
        return ccGp;
    }

    public void setCcGp(String ccGp) {
        this.ccGp = ccGp;
    }

    public String getSmaType() {
        return smaType;
    }

    public void setSmaType(String smaType) {
        this.smaType = smaType;
    }

    /**
     * ：信用卡
     */
    private String ccGp;
    /**
     * 区分智能账户或支付账户的
     */
    private String smaType;

    /**
     * Y 换领多种电子券  N 换领一种电子券
     */
    private String isNotVaried;

    public boolean isConvertMore() {
        if (TextUtils.isEmpty(isNotVaried)) {
            return false;
        } else {
            return TextUtils.equals(isNotVaried, "Y");
        }
    }

    private List<ResultRedmBean> resultRedm;

    public List<ResultErrorRedmBean> geteVoucherNos() {
        return eVoucherNos;
    }

    public void seteVoucherNos(List<ResultErrorRedmBean> eVoucherNos) {
        this.eVoucherNos = eVoucherNos;
    }

    private List<ResultErrorRedmBean> eVoucherNos;

    public String getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    public String getGpBalance() {
        return gpBalance;
    }

    public void setGpBalance(String gpBalance) {
        this.gpBalance = gpBalance;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getRedeemPnt() {
        return redeemPnt;
    }

    public void setRedeemPnt(String redeemPnt) {
        this.redeemPnt = redeemPnt;
    }

    public List<ResultRedmBean> getResultRedm() {
        return resultRedm;
    }

    public void setResultRedm(List<ResultRedmBean> resultRedm) {
        this.resultRedm = resultRedm;
    }

    public static class ResultErrorRedmBean extends BaseEntity {
        /**
         * aswItemCode : 88880
         * eVoucherQty : 1
         * giftCode : 66660
         */

        private String aswItemCode;
        private String eVoucherQty;
        private String giftCode;

        public String geteVoucherQty() {
            return eVoucherQty;
        }

        public void seteVoucherQty(String eVoucherQty) {
            this.eVoucherQty = eVoucherQty;
        }

        private String eVoucherStock;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        private String itemName;

        public String getOrderTp() {
            return orderTp;
        }

        public void setOrderTp(String orderTp) {
            this.orderTp = orderTp;
        }

        private String orderTp;

        public int getCurrentSelCnt() {
            return currentSelCnt;
        }

        public void setCurrentSelCnt(int currentSelCnt) {
            this.currentSelCnt = currentSelCnt;
        }

        private int currentSelCnt;

        public String geteVoucherStock() {
            return eVoucherStock;
        }

        public void seteVoucherStock(String eVoucherStock) {
            this.eVoucherStock = eVoucherStock;
        }


        public String getAswItemCode() {
            return aswItemCode;
        }

        public void setAswItemCode(String aswItemCode) {
            this.aswItemCode = aswItemCode;
        }

        public String getEVoucherQty() {
            return eVoucherQty;
        }

        public void setEVoucherQty(String eVoucherQty) {
            this.eVoucherQty = eVoucherQty;
        }

        public String getGiftCode() {
            return giftCode;
        }

        public void setGiftCode(String giftCode) {
            this.giftCode = giftCode;
        }
    }

    public static class ResultRedmBean extends BaseEntity {
        /**
         * aswItemCode : 88880
         * eVoucherQty : 1
         * giftCode : 66660
         */

        private String aswItemCode;
        private String eVoucherQty;
        private String giftCode;

        public String getReferenceNo() {
            return referenceNo;
        }

        public void setReferenceNo(String referenceNo) {
            this.referenceNo = referenceNo;
        }

        private String referenceNo;

        public String getItemName() {
            return itemName;
        }

        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        private String itemName;

        public String getOrderTp() {
            return orderTp;
        }

        public void setOrderTp(String orderTp) {
            this.orderTp = orderTp;
        }

        private String orderTp;

        public int getCurrentSelCnt() {
            return currentSelCnt;
        }

        public void setCurrentSelCnt(int currentSelCnt) {
            this.currentSelCnt = currentSelCnt;
        }

        private int currentSelCnt;

        public String geteVoucherStock() {
            return eVoucherStock;
        }

        public void seteVoucherStock(String eVoucherStock) {
            this.eVoucherStock = eVoucherStock;
        }

        private String eVoucherStock;

        public String getAswItemCode() {
            return aswItemCode;
        }

        public void setAswItemCode(String aswItemCode) {
            this.aswItemCode = aswItemCode;
        }

        public String getEVoucherQty() {
            return eVoucherQty;
        }

        public void setEVoucherQty(String eVoucherQty) {
            this.eVoucherQty = eVoucherQty;
        }

        public String getGiftCode() {
            return giftCode;
        }

        public void setGiftCode(String giftCode) {
            this.giftCode = giftCode;
        }
    }
}
