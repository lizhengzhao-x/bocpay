package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CVVDialog;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.CustomTvEditText;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.app.ProjectApp.getContext;


public class VirtualCardRegisterCardInfoActivity extends BaseCompatActivity {
    private static final String TAG = "VirtualCardRegisterCardInfoActivity";
    @BindView(R.id.etwd_expired_date)
    EditTextWithDel etwdExpiredDate;
    @BindView(R.id.etwd_cvv)
    EditTextWithDel etwdCvv;
    @BindView(R.id.tv_card_info_nextPage)
    TextView tvCardInfoNextPage;
    @BindView(R.id.cet_verify_code)
    CustomTvEditText mVerifyCodeET;
    @BindView(R.id.iv_verify_code)
    ImageView mVerifyCodeIV;
    @BindView(R.id.iv_refresh_code)
    ImageView mRefreshCodeIV;
    @BindView(R.id.id_verifycode_view)
    LinearLayout idVerifycodeView;

    TextView tv_expiry;

    TextView tv_Number;

    @BindView(R.id.fl_cards_show)
    FrameLayout flCardsShow;

    /**
     * 跳转type 1.虚拟卡注册 2.虚拟卡忘记密码
     */
    private int mCurrentActionFlow;
    private String cardStr;

    private CVVDialog Dialog_CVV;
    public String repalceSpaceStr = "/";
    public String lastString = null;
    public int deleteSelect;
    private RelativeLayout LayoutCardInfoFront = null;
    private RelativeLayout LayoutCardInfoback = null;
    private FrameLayout.LayoutParams layoutParamsback;
    private FrameLayout.LayoutParams layoutParamsfront;

    private TextView tv_CVV;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        mCurrentActionFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        AndroidUtils.hideKeyBoardInput(this);

        initView();
        idVerifycodeView.setVisibility(View.VISIBLE);
        if (mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册
            setToolBarTitle(R.string.bind_title);
            getVerifyCodeForLogin();
        } else if (mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡注册
            setToolBarTitle(R.string.setting_payment_fgw);
            getVerifyCodeForLogin();
        }
    }

    private void initView() {

        cardStr = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        mVerifyCodeET.getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);

        LayoutCardInfoFront = (RelativeLayout) mInflater.inflate(R.layout.item_card_info_front, null);
        LayoutCardInfoback = (RelativeLayout) mInflater.inflate(R.layout.item_card_info_back, null);

        tv_Number = LayoutCardInfoFront.findViewById(R.id.card_info_number);
        tv_expiry = LayoutCardInfoFront.findViewById(R.id.tv_expiry);
        tv_CVV = LayoutCardInfoback.findViewById(R.id.tv_CVV);
        if (!TextUtils.isEmpty(cardStr)) {
            tv_Number.setText(AndroidUtils.formatCardNumberStr(cardStr));
        }
        //控件的宽高
        int width = (int) getResources().getDimension(R.dimen.space_430_px);
        int height = (int) getResources().getDimension(R.dimen.space_280_px);
        //两个控件的宽高错位的尺寸
        int Space_W = (int) getResources().getDimension(R.dimen.space_90_px);
        int Space_H = (int) getResources().getDimension(R.dimen.space_30_px);
        //获取屏幕宽高
        WindowManager wm = (WindowManager) getContext().getSystemService(Context.WINDOW_SERVICE);
        int ScreenWidth = wm.getDefaultDisplay().getWidth();
        //因为限制高度，故此处宽的尺寸要按照控件的尺寸来
        int layout_height = (int) getResources().getDimension(R.dimen.space_340_px);

        final int beforeL = (ScreenWidth - (width + Space_W)) / 2;
        final int beforeT = (layout_height - (height + Space_H)) / 2;

        final int afterL = beforeL + Space_W;
        final int afterT = beforeT + Space_H;

        layoutParamsback = new FrameLayout.LayoutParams(width, height);
        layoutParamsback.setMargins(afterL, afterT, 0, 0);
        flCardsShow.addView(LayoutCardInfoback, layoutParamsback);

        layoutParamsfront = new FrameLayout.LayoutParams(width, height);
        layoutParamsfront.setMargins(beforeL, beforeT, 0, 0);
        flCardsShow.addView(LayoutCardInfoFront, layoutParamsfront);

        etwdExpiredDate.hideErrorView();
        etwdExpiredDate.hideDelView();
        etwdExpiredDate.addTextChangedListener(textWatcher_etwdExpiredDate);
        etwdExpiredDate.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (hasFocus) {
                    //如果是ExpiredDate，如果文本改变的话，就更改layout层级。front在前面
                    layoutParamsback.setMargins(afterL, afterT, 0, 0);
                    layoutParamsfront.setMargins(beforeL, beforeT, 0, 0);
                    flCardsShow.bringChildToFront(LayoutCardInfoFront);
                    if (etwdExpiredDate.getEditText().getText().toString().length() > 0) {
                        etwdExpiredDate.setHasDelView(true);
                        etwdExpiredDate.showDelView();
                    } else {
                        etwdExpiredDate.hideDelView();
                    }
                    etwdExpiredDate.setBottomLineHighSel();
                } else {
                    etwdExpiredDate.hideDelView();
                    etwdExpiredDate.setBottomLineNoSel();
                }
            }
        });
        etwdCvv.hideErrorView();
        etwdCvv.hideDelView();
        etwdCvv.addTextChangedListener(textWatcher_etwdCvv);
        etwdCvv.setOnFocusChangeExtraListener(new EditTextWithDel.onFocusChangeListener() {
            @Override
            public void onFocusChange(boolean hasFocus) {
                if (hasFocus) {
                    //如果是CVV，如果焦点改变的话，就更改layout层级。back在前面
                    layoutParamsfront.setMargins(afterL, afterT, 0, 0);
                    layoutParamsback.setMargins(beforeL, beforeT, 0, 0);
                    flCardsShow.bringChildToFront(LayoutCardInfoback);
                }
            }
        });


        if (!TextUtils.isEmpty(cardStr)) {
            tv_Number.setText(AndroidUtils.formatCardNumberStr(cardStr));
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        //默认按钮设为不可用状态
        updateOkBackground(false);

        mRefreshCodeIV.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                getVerifyCodeForLogin();
            }
        });

        mVerifyCodeET.setOnEditTextChangeListener(new CustomTvEditText.OnEditTextChangeListener() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int count, int after) {
                checkButtonState();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    private void getVerifyCodeForLogin() {
        mRefreshCodeIV.setEnabled(false);
        ApiProtocolImplManager.getInstance().getLoginVerifyCode(this, mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER ? ApiConstant.VIRTUALCARD_REGISTER : ApiConstant.VIRTUALCARD_FORGETPWD, new NetWorkCallbackListener<BankLoginVerifyCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                mRefreshCodeIV.setEnabled(true);
                AndroidUtils.showTipDialog(VirtualCardRegisterCardInfoActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(BankLoginVerifyCodeEntity response) {
                mRefreshCodeIV.setEnabled(true);
                int codeLen = 8;
                try {
                    codeLen = Integer.valueOf(response.getVerifyCodeLength());
                } catch (Exception e) {
                    if (BuildConfig.isLogDebug) {
                        LogUtils.e(TAG, Log.getStackTraceString(e));
                    }
                }
                try {
                    String bmpCode = response.getImageCode();
                    Bitmap bmp = AndroidUtils.base64ToBitmap(bmpCode);
                    mVerifyCodeIV.setImageBitmap(bmp);
                    mVerifyCodeET.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(codeLen)});
                } catch (Exception e) {
                    if (BuildConfig.isLogDebug) {
                        LogUtils.e(TAG, Log.getStackTraceString(e));
                    }
                }
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        forbidScreen();
    }

    private void updateOkBackground(boolean isSel) {
        tvCardInfoNextPage.setEnabled(isSel);
        tvCardInfoNextPage.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }


    private TextWatcher textWatcher_etwdExpiredDate = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //因为重新排序之后setText的存在，从而会重新进行一次输入的过程
            //会导致输入框的内容从0开始输入，这里是为了避免这种情况产生一系列问题
            //此方法会替换从start开始的count个字符替换EditText旧的长度为before个字符即旧文本
            //这种情况下不会改变光标的位置，所以直接return掉
            if (start == 0 && count > 0) {
                return;
            }
            //如果是第一次设置值，如果当前的没有内容也直接return掉，默认为不改变光标位置
            String editTextContent = etwdExpiredDate.getText();
            if (TextUtils.isEmpty(editTextContent) || TextUtils.isEmpty(lastString)) {
                return;
            }
            //然后根据输入的内容来得到加完斜杠之后要显示的内容
            editTextContent = BasicUtils.addLineByInputContent(s.toString(), 5, repalceSpaceStr);
            if (tv_expiry != null) {
                tv_expiry.setText(editTextContent);
            }

            //如果最新的长度 < 上次的长度，代表进行了删除
            if (editTextContent.length() < lastString.length()) {
                deleteSelect = start;
            } else {
                deleteSelect = editTextContent.length();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            //对输入的数字，每2位加一个斜杠
            //获取输入框中的内容,不可以去斜杠
            String etContent = etwdExpiredDate.getEditText().getText().toString();
            //重新加入斜杠拼接字符串
            String newContent = BasicUtils.addLineByInputContent(s.toString(), 5, repalceSpaceStr);
            //保存本次字符串数据
            lastString = newContent;
            //如果有改变，则重新填充
            //防止EditText无限setText()产生死循环
            if (!etContent.equals(newContent)) {
                etwdExpiredDate.setContentText(newContent);
                checkButtonState();
                //保证光标的位置
                etwdExpiredDate.getEditText().setSelection(deleteSelect > newContent.length() ? newContent.length() : deleteSelect);
            }
            String cvvStr = etwdCvv.getEditText().getText().toString().trim();
            if (!TextUtils.isEmpty(etContent) && !TextUtils.isEmpty(cvvStr) && cvvStr.length() >= 3 && newContent.length() >= 5) {
                updateOkBackground(true);
            } else {
                updateOkBackground(false);
            }

            if (etContent.length() >= 5) {
                etwdExpiredDate.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        etwdExpiredDate.setFocusable(false);
                        etwdExpiredDate.hideDelView();
                    }
                }, 100);
                etwdExpiredDate.clearFocus();
                etwdCvv.getEditText().requestFocus();
            }
        }
    };


    private TextWatcher textWatcher_etwdCvv = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //文本修改就更新textView
            if (tv_CVV != null) {
                tv_CVV.setText(s);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            checkButtonState();
            String etContent = etwdCvv.getText();
            //cvv输入完成之后隐藏软键盘
            if (etContent.length() >= 3) {
                etwdCvv.hideDelView();
                etwdCvv.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        etwdCvv.setFocusable(false);
                        etwdCvv.hideDelView();
                    }
                }, 100);
                etwdCvv.clearFocus();
                mVerifyCodeET.getEditText().requestFocus();
            }
        }
    };

    public void checkButtonState() {
        int length_Cvv = etwdCvv.getTextView().length();
        int length_ExpiredDate = etwdExpiredDate.getTextView().length();
        if (length_Cvv > 2 && length_ExpiredDate > 0) {
            if (mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER || mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                updateOkBackground(mVerifyCodeET.getInputText().length() == 4);
            } else {
                updateOkBackground(true);
            }
        } else {
            updateOkBackground(false);
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_virtual_input_bank_card_info;
    }


    @OnClick({R.id.etwd_expired_date, R.id.iv_question_mark, R.id.etwd_cvv, R.id.tv_card_info_nextPage})
    public void onViewClicked(View view) {
        switch (view.getId()) {


            case R.id.etwd_expired_date:
                break;
            case R.id.iv_question_mark:

                Dialog_CVV = new CVVDialog(VirtualCardRegisterCardInfoActivity.this);
                if (Dialog_CVV != null && !Dialog_CVV.isShowing()) {
                    Dialog_CVV.show();
                }
                Window dialogWindow = Dialog_CVV.getWindow();
                WindowManager m = getActivity().getWindowManager();
                Display d = m.getDefaultDisplay();
                // 获取对话框当前的参数值
                WindowManager.LayoutParams p = dialogWindow.getAttributes();
                // 高度设置为屏幕的0.45，根据实际情况调整
                p.height = (int) (d.getHeight() * 0.47);
                // 宽度设置为屏幕的0.65，根据实际情况调整
                p.width = (int) (d.getWidth() * 0.65);
                dialogWindow.setAttributes(p);
                break;
            case R.id.etwd_cvv:
                break;
            case R.id.tv_card_info_nextPage:
                if (!ButtonUtils.isFastDoubleClick(R.id.id_send_msg)) {
                    submitCardInfo();
                }
                break;
            default:
                break;
        }
    }


    private void submitCardInfo() {
        final String cardNumber = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        String content = etwdExpiredDate.getEditText().getText().toString().trim();
        //去掉所有斜杠
        final String expiryDateStr = content.replaceAll("/", "");
        String cvvStr = etwdCvv.getEditText().getText().toString().trim();
        if (mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            registerVirtualAccount(cvvStr, expiryDateStr, cardNumber, mVerifyCodeET.getInputText());
        } else if (mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            forgetPwdVirtualAccount(cvvStr, expiryDateStr, cardNumber, mVerifyCodeET.getInputText());
        }
    }

    private VirtualcardRegisterVerifyEntity parseExraInfo(String cvv, String expireDate, String pan, String verifyCode) {
        VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntityIn = (VirtualcardRegisterVerifyEntity) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDREGISTERVERIFINVO);
        virtualcardRegisterVerifyEntityIn.setCvv(cvv);
        virtualcardRegisterVerifyEntityIn.setExpiryDate(expireDate);
        virtualcardRegisterVerifyEntityIn.setPan(pan);
        virtualcardRegisterVerifyEntityIn.setAction(mCurrentActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER ? ApiConstant.VIRTUALCARD_REGISTER : ApiConstant.VIRTUALCARD_FORGETPWD);
        virtualcardRegisterVerifyEntityIn.setVerifyCode(verifyCode);
        virtualcardRegisterVerifyEntityIn.setLoginType("3");
        virtualcardRegisterVerifyEntityIn.setEaiAction("B");
        return virtualcardRegisterVerifyEntityIn;
    }

    private void forgetPwdVirtualAccount(String cvv, String expireDate, String pan, String verifyCode) {
        ApiProtocolImplManager.getInstance().virtualCardCreditForgetPwd(this, parseExraInfo(cvv, expireDate, pan, verifyCode), new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(VirtualCardRegisterCardInfoActivity.this, errorMsg);
                mVerifyCodeET.setContentText("");
                getVerifyCodeForLogin();
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                mVerifyCodeET.setContentText("");
                parseVirtualCardForgetPasswordResult(response);
            }
        });
    }

    private void parseVirtualCardForgetPasswordResult(VirtualCardListEntity response) {

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.VIRTUALCARDLIST, response);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentActionFlow);
        mHashMaps.put(Constants.FORGET_PASSWORD_TYPE, "1");
        ActivitySkipUtil.startAnotherActivity(this, VirtualCardBindTermsActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void registerVirtualAccount(String cvv, String expireDate, String pan, String verifyCode) {
        ApiProtocolImplManager.getInstance().virtualCardCreditRegister(this, parseExraInfo(cvv, expireDate, pan, verifyCode), new NetWorkCallbackListener<VirtualCardListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(VirtualCardRegisterCardInfoActivity.this, errorMsg);
                mVerifyCodeET.setContentText("");
                getVerifyCodeForLogin();
            }

            @Override
            public void onSuccess(VirtualCardListEntity response) {
                mVerifyCodeET.setContentText("");
                Intent intent = new Intent(VirtualCardRegisterCardInfoActivity.this, VirtualCardBindTermsActivity.class);
                intent.putExtra(Constants.CURRENT_PAGE_FLOW, mCurrentActionFlow);
                intent.putExtra(Constants.VIRTUALCARDLIST, response);
                startActivity(intent);
            }
        });
    }

}
