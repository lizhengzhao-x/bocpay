package cn.swiftpass.wallet.intl.widget.recyclerview;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;

public class AnalysisPageBinder extends BaseItemDataBinder<AnalysisPageEntity> {
    @Override
    public int getViewType() {
        return AnalysisItemAdapter.TYPE_PAGE;
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_area_selection;
    }

    @Override
    public void bindData(BaseViewHolder baseViewHolder, AnalysisPageEntity item, int position) {
        baseViewHolder.setText(R.id.tv_area, item.dataType);
    }
}
