package cn.swiftpass.wallet.intl.module.ecoupon.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/8/14.
 */
public class QueryVoucherRecordsProtocol extends BaseProtocol {

    private String recordType;

    public QueryVoucherRecordsProtocol(String recordTypeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.recordType = recordTypeIn;
        mUrl = "api/eVoucher/queryVoucherRecords";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.RECORDTYPE, recordType);
    }

}
