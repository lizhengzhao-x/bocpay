package cn.swiftpass.wallet.intl.module.redpacket.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.redpacket.entity.GetLiShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.UnOpenLiShiListEntity;

/**
 * 未拆开利是 记录
 */
public class UnOpenLiShiListContract {

    public interface View extends IView {

        void getUnOpenRedPacketSuccess(UnOpenLiShiListEntity response);

        void getUnOpenRedPacketError(String errorCode, String errorMsg);

        void turnOnRedPacketSuccess(GetLiShiHomeEntity response);

        void turnOnRedPacketError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<UnOpenLiShiListContract.View> {
        void getUnOpenRedPacket(int pageNum, int pageSize, String nextTimeStamp);

        void turnOnRedPacket(String srcRefNo);
    }

}
