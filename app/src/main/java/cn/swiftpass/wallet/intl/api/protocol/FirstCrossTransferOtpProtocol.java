package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class FirstCrossTransferOtpProtocol extends BaseProtocol {

    String txnId;

    public FirstCrossTransferOtpProtocol(String txnIdIn, NetWorkCallbackListener dataCallback) {
        this.txnId = txnIdIn;
        this.mDataCallback = dataCallback;
        mUrl = "api/crossTransfer/sendOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNID, txnId);
        mBodyParams.put(RequestParams.ACTION, "CROSSTRANSFER");
    }
}

