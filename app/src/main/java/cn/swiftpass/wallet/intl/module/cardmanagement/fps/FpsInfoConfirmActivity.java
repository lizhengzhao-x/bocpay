package cn.swiftpass.wallet.intl.module.cardmanagement.fps;


import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckRequest;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.setting.about.ServerAgreementActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 登记FPS,信息确认页面
 */

public class FpsInfoConfirmActivity extends BaseCompatActivity {

    public static final String FPS_INFO = "fps info";
    public static final String FPS_ACTION = "fps action";

    @BindView(R.id.tv_fps_account_id)
    TextView mFpsAccountTV;
    @BindView(R.id.tv_account_type)
    TextView mAccountTypeTV;
    @BindView(R.id.tv_account_no)
    TextView mAccountNoTV;
    @BindView(R.id.tv_account_type_name)
    TextView mAccountTypeNameTV;

    @BindView(R.id.tv_default_account_flag)
    TextView mDefaultBankTV;
    @BindView(R.id.tv_fps_next)
    TextView mNextTV;
    @BindView(R.id.iv_confirm_mark)
    ImageView ivConfirmMark;
    @BindView(R.id.id_confirm_conditions)
    TextView idConfirmConditions;


    public FpsBankRecordBean mBankBean = null;
    public String mFpsAction = "";
    FpsPreCheckEntity mFpsCheckEntity;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C3_03_1_1);
        initAgreement();
        parseIntent();
        if (null != mBankBean) {
            initData();
            updateNextStatus(false);
        } else {
            finish();
        }
    }

    private void parseIntent() {
        if (null != getIntent()) {
            mFpsAction = getIntent().getStringExtra(FPS_ACTION);
            mBankBean = (FpsBankRecordBean) getIntent().getSerializableExtra(FPS_INFO);
        }
    }

    private void initData() {
        if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mBankBean.getAccountIdType())) {
            mFpsAccountTV.setText(AndroidUtils.fpsEmailFormat(mBankBean.getAccountId()));
        } else {
            mFpsAccountTV.setText(AndroidUtils.fpsPhoneFormat(mBankBean.getAccountId()));
        }
        mAccountNoTV.setText(mBankBean.getAccountType() + AndroidUtils.getSubNumberBrackets(mBankBean.getAccountNo()));
        mDefaultBankTV.setText(AndroidUtils.getDefaultBankFlag(mBankBean.getDefaultBank()));
        mAccountTypeNameTV.setText(mBankBean.getAccountTypeName());

        mNextTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick(R.id.tv_fps_next)) {
                    next();
                }
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_info_confirm;
    }


    @OnClick({R.id.tv_fps_next, R.id.iv_confirm_mark})
    public void onViewClicked(View view) {
        int viewId = view.getId();
        switch (viewId) {

            case R.id.iv_confirm_mark:
                onClickCheck();
                break;
            default:
                break;
        }
    }

    private void onClickCheck() {
        ivConfirmMark.setSelected(!ivConfirmMark.isSelected());
        updateNextStatus(ivConfirmMark.isSelected());
    }

    private void updateNextStatus(boolean check) {
        if (check && !ivConfirmMark.isSelected()) {
            return;
        }
        mNextTV.setEnabled(check);
        mNextTV.setBackgroundResource(check ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    private void initAgreement() {
        String totalStr = getResources().getString(R.string.card_info_notice);
        int startIndex = totalStr.indexOf("%");
        SpannableString spannableString = new SpannableString(totalStr.replace("%", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ActivitySkipUtil.startAnotherActivity(FpsInfoConfirmActivity.this, ServerAgreementActivity.class);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        idConfirmConditions.setText(spannableString);
        idConfirmConditions.setMovementMethod(LinkMovementMethod.getInstance());
    }


    public static void startActivity(Activity activity, FpsBankRecordBean bean) {
        Intent intent = new Intent(activity, FpsInfoConfirmActivity.class);
        intent.putExtra(FPS_ACTION, FpsConst.FPS_ACTION_ADD);
        intent.putExtra(FPS_INFO, bean);
        activity.startActivity(intent);
    }

    //预检查
    private void next() {
        //防止按钮多次点击 发送otp
        FpsPreCheckRequest request = new FpsPreCheckRequest();
        request.accountId = mBankBean.getAccountId();
        request.accountIdType = mBankBean.getAccountIdType();
        request.accountNo = mBankBean.getAccountNo();
        request.bankCode = "";
        request.action = mFpsAction;
        request.defaultBank = mBankBean.getDefaultBank();
        ApiProtocolImplManager.getInstance().fpsPreCheck(this, request, new NetWorkCallbackListener<FpsPreCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(getActivity(), errorMsg);
            }

            @Override
            public void onSuccess(FpsPreCheckEntity response) {
                mFpsCheckEntity = response;
                onConfirm();
            }
        });
    }

    //确认完发OTP
    private void onConfirm() {
        sendFpsOtp(mFpsCheckEntity);
    }

    private void sendFpsOtp(FpsPreCheckEntity entity) {
        if (null != entity) {
            Intent intent = new Intent(getActivity(), RegisterInputOTPActivity.class);
            intent.putExtra(FpsConst.FPS_PRE_CHECK_ENTITY_ADD, entity);
            intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, mBankBean);
            //这个accountId,可以是手机、邮箱
            intent.putExtra(Constants.DATA_PHONE_NUMBER, mBankBean.getAccountId());
            intent.putExtra(Constants.CURRENT_PAGE_FLOW, Constants.DATA_TYPE_FPS_ADD);
            startActivity(intent);
            mNextTV.setEnabled(true);
        }
    }
}
