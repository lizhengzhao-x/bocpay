package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryInfo;

public class CreditCardRewardHistoryInfoAdapter extends RecyclerView.Adapter {

    private final Context context;
    List<CreditCardRewardHistoryInfo> data;
    private String rewardsType;


    public CreditCardRewardHistoryInfoAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_reward_history_item_info, parent, false);
        return new RewardHistoryInfoHolder(context, inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        RewardHistoryInfoHolder cardRewardHolder = (RewardHistoryInfoHolder) holder;
        cardRewardHolder.bindData(data.get(position), rewardsType);
    }

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }


    public void setData(List<CreditCardRewardHistoryInfo> rows, String rewardsType) {
        data = rows;
        this.rewardsType = rewardsType;
    }
}
