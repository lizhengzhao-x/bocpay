package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionManagerContract;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.RedemPtionManagerPresenter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 电子券管理页面
 * 1.1）左边电子券兑换 2）右边我的电子券
 * 2.查看积分功能
 */
public class ECouponsManagerFragment extends BaseFragment<RedemPtionManagerContract.Presenter> implements RedemPtionManagerContract.View {


    @BindView(R.id.id_change_horizontal)
    ImageView mIdChangeHorizonal;
    @BindView(R.id.id_ecoupon_convert)
    TextView idTabEcoupLeft;
    @BindView(R.id.id_ecoupon_my)
    TextView idTabEcoupRight;
    @BindView(R.id.id_switch_view)
    View idSwitchView;
    @BindView(R.id.fl_transaction)
    FrameLayout flTransaction;

    private EcouponsConvertFragment mEcouponsItemConvert;
    private EcouponsMyFragment mEcouponsItemMy;
    private boolean isCheckPwd;

    public int getCurrentSelPosition() {
        return currentSelPosition;
    }

    private int currentSelPosition;
    /**
     * 验证支付密码之后要做什么 1.显示积分查询信息 2.直接进入兑换
     */
    private int currentVerifyAction;
    private int ACTION_VERIFY = 100;
    private int ACTION_GO_CONVERT = 200;

    private long currentPageCreateTime;

    public static final String CURRENTPAGE = "CURRENTPAGE";

    public static final int PAGE_CONVERT = 100;

    public static final int PAGE_MY = 200;

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.EC01_1);
            mActivity.setToolBarRightViewToText(R.string.EC01_3);
            mActivity.getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_E_VOU_PAGE_RECORD, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_EVOUCHER_PAGE, System.currentTimeMillis() - currentPageCreateTime);
                    sendAnalysisEvent(analysisButtonEntity);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), EcoupRecordsActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EcoupEventEntity event) {
        if (event.getEventType() == EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP||event.getEventType() == EcoupEventEntity.EVENT_RETRY_REFRESH_CNT) {
            reloadView();
        } else if (event.getEventType() == EcoupEventEntity.EVENT_CAME_MY_ECOUP) {
            setSelRightView();
        }
    }


    @Override
    protected RedemPtionManagerContract.Presenter loadPresenter() {
        return new RedemPtionManagerPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_ecoupons;
    }

    @Override
    protected void initView(View v) {

        EventBus.getDefault().register(this);
        currentPageCreateTime = System.currentTimeMillis();

        mEcouponsItemConvert = new EcouponsConvertFragment();
        mEcouponsItemMy = new EcouponsMyFragment();
        FragmentManager mFragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        boolean showLeftPage = true;
        if (getArguments() != null) {
            showLeftPage = getArguments().getInt(CURRENTPAGE, PAGE_CONVERT) == PAGE_CONVERT;
        }
        if (showLeftPage) {
            fragmentTransaction.add(R.id.fl_transaction, mEcouponsItemConvert);
        } else {
            fragmentTransaction.add(R.id.fl_transaction, mEcouponsItemMy);
        }

        fragmentTransaction.commitAllowingStateLoss();

        idTabEcoupLeft.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (currentSelPosition == 0) return;
                setSelLeftView();
            }
        });

        idTabEcoupRight.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (currentSelPosition == 1) return;
                setSelRightView();
            }
        });
        mIdChangeHorizonal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateRecycleViewLayoutManager();
            }
        });
        if (showLeftPage) {
            setDefaultSelect();
            idTabEcoupLeft.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
            idTabEcoupLeft.setTextColor(getResources().getColor(R.color.white));
            idTabEcoupRight.setTextColor(getResources().getColor(R.color.font_gray_three));
        } else {
            setDefaultRightSelect();
        }

        updateUiStatusByFragment(showLeftPage ? 0 : 1);

    }

    public void setSelRightView() {
        if (mFmgHandler != null) {
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (idSwitchView == null) return;
                    currentSelPosition = 1;
                    setDefaultSelect();
                    idSwitchView.setBackgroundResource(R.drawable.bg_ecoupon_btn_top);
                    idTabEcoupRight.setBackgroundResource(R.drawable.right_china_btn_bac);
                    idTabEcoupRight.setTextColor(getResources().getColor(R.color.app_white));
                    idTabEcoupLeft.setTextColor(getResources().getColor(R.color.font_gray_three));
                    switchDiffFragmentContent(getChildFragmentManager(), mEcouponsItemMy, mEcouponsItemConvert, R.id.fl_transaction, 1);
                    updateUiStatusByFragment(1);
                    if (mEcouponsItemMy.isCreatSuccess()) {
                        mEcouponsItemMy.showChangeBtn();
                        //需要延迟处理 防止view未创建成功
                        mFmgHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEcouponsItemMy.autoRefreshList();
                            }
                        }, 100);
                    }
                }
            }, 100);
        }
    }

    public void setSelLeftView() {
        currentSelPosition = 0;
        setDefaultSelect();

        idTabEcoupLeft.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
        idTabEcoupLeft.setTextColor(getResources().getColor(R.color.white));
        idTabEcoupRight.setTextColor(getResources().getColor(R.color.font_gray_three));
        switchDiffFragmentContent(getChildFragmentManager(), mEcouponsItemConvert, mEcouponsItemMy, R.id.fl_transaction, 0);
        updateUiStatusByFragment(0);
        mEcouponsItemConvert.showChangeBtn();
        //需要延迟处理 防止view未创建成功
        mFmgHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEcouponsItemConvert.autoRefreshList();
            }
        }, 100);

    }

    private void updateRecycleViewLayoutManager() {
        boolean isHorizonal = false;
        if (mEcouponsItemConvert.isVisible()) {
            mEcouponsItemConvert.changeHorizonal();
            isHorizonal = mEcouponsItemConvert.isHorinzonal();
        } else {
            mEcouponsItemMy.changeHorizonal();
            isHorizonal = mEcouponsItemMy.isHorinzonal();
        }
        if (isHorizonal) {
            mIdChangeHorizonal.setImageResource(R.mipmap.img__pages_card);
        } else {
            mIdChangeHorizonal.setImageResource(R.mipmap.img_pages_list);

        }
        mEcouponsItemMy.autoRefreshList();
    }


    /**
     * 需求 换领电子券界面 显示我的积分 我的电子券顶部不显示
     *
     * @param selPos
     */
    private void updateUiStatusByFragment(int selPos) {
        if (selPos == 0) {
            if (mEcouponsItemConvert.isHorinzonal()) {
                mIdChangeHorizonal.setImageResource(R.mipmap.img__pages_card);
            } else {
                mIdChangeHorizonal.setImageResource(R.mipmap.img_pages_list);
            }

        } else {
            if (mEcouponsItemMy.isHorinzonal()) {
                mIdChangeHorizonal.setImageResource(R.mipmap.img__pages_card);
            } else {
                mIdChangeHorizonal.setImageResource(R.mipmap.img_pages_list);
            }

        }
    }

    /**
     * 密码弹出框
     */
    public void inoutPswPopWindow() {
        if (currentSelPosition == 1) return;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    private void verifyResultSuc() {

        if (mEcouponsItemConvert != null) {
            mEcouponsItemConvert.redeemGiftCheckStock();
        }
    }


    private void setDefaultSelect() {
        if (idTabEcoupLeft == null) return;
        idTabEcoupLeft.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        idTabEcoupLeft.setTextColor(getResources().getColor(R.color.white));
        idTabEcoupRight.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        idTabEcoupRight.setTextColor(getResources().getColor(R.color.white));
    }

    private void setDefaultRightSelect() {
        if (idTabEcoupLeft == null) return;
        setDefaultSelect();

        idTabEcoupRight.setBackgroundResource(R.drawable.right_china_btn_bac);
        idTabEcoupRight.setTextColor(getResources().getColor(R.color.app_white));
        idTabEcoupLeft.setTextColor(getResources().getColor(R.color.font_gray_three));
        idTabEcoupLeft.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        currentSelPosition = 1;
    }

    @Override
    public void verifyPwdFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(getContext(), errorMsg);
    }

    @Override
    public void verifyPwdSuccess() {
        verifyResultSuc();
    }


    public boolean isAlreadyVerifyPwd() {
        currentVerifyAction = ACTION_GO_CONVERT;
        if (isCheckPwd) {
            return true;
        }
        inoutPswPopWindow();
        return false;
    }

    public void setShowChangeView(boolean show) {
        mIdChangeHorizonal.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    public void reloadView() {
//        if (currentSelPosition == 0) {
        if (mEcouponsItemConvert != null && mFmgHandler != null) {
            mFmgHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEcouponsItemConvert.autoRefreshList();
                }
            }, 100);
        }

//        } else {
//            if (mEcouponsItemMy != null && mEcouponsItemMy.isCreatSuccess() && mHandler != null) {
//                mHandler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        mEcouponsItemMy.autoRefreshList();
//                    }
//                }, 100);
//            }
//
//        }

    }
}
