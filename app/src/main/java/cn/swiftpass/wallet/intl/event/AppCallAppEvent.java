package cn.swiftpass.wallet.intl.event;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;

import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.module.MainContract;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppPaymentActivity;
import cn.swiftpass.wallet.intl.module.callapp.AppCallAppUtils;
import cn.swiftpass.wallet.intl.module.deepLink.DeepLinkActivity;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.event.UserLoginEventManager.EventPriority.EVENT_APPCALLAPP;

public class AppCallAppEvent extends BaseEventType {
    public static final String AppCallAppEventNAME = "AppCallAppEventNAME";

    public static final String APP_LINK_URL = "APP_LINK_URL";
//    public static final String APP_MERCHANT_CALLBACK_URL = "APP_MERCHANT_CALLBACK_URL";
    public static final String APP_CALL_APP_TTPE = "APP_CALL_APP_TTPE";

    public enum APPCALLAPPTTPE {
        APP_CALL_APP_PAYMENT("APP_CALL_APP_PAYMENT"), APP_CALL_APP_DEEPLINK_JUMP("APP_CALL_APP_DEEPLINK_JUMP");
        private final String type;

        APPCALLAPPTTPE(String type) {
            this.type = type;
        }
    }

    public AppCallAppEvent() {
        super(EVENT_APPCALLAPP.eventPriority, EVENT_APPCALLAPP.eventName);
    }

    @Override
    protected String getCurrentEventType() {
        return AppCallAppEventNAME;
    }

    @Override
    protected boolean dealWithEvent() {
        Activity currentActivity = MyActivityManager.getInstance().getCurrentActivity();
        if (currentActivity != null && currentActivity instanceof AppCallAppDealOwner) {
            AppCallAppDealOwner lbsDialogDealOwner = (AppCallAppDealOwner) currentActivity;
            return initAppLink(lbsDialogDealOwner);
        }
        return false;
    }

    private boolean initAppLink(AppCallAppDealOwner appCallAppDealOwner) {
        String actionUrl = (String) eventParams.get(APP_LINK_URL);
//        Handler mHandler = appCallAppDealOwner.getHandler();
//        MainContract.Presenter mPresenter = appCallAppDealOwner.getCurrentPresenter();
        if (!TextUtils.isEmpty(actionUrl)) {
            if (DeepLinkUtils.isNewDeepLink(actionUrl)) {
                Intent intentNext = new Intent(appCallAppDealOwner.getCurrentActivity(), DeepLinkActivity.class);
                intentNext.putExtra(AppCallAppLinkEntity.APP_LINK_URL, actionUrl);
                appCallAppDealOwner.getCurrentActivity().startActivity(intentNext);
                return true;
            } else {
                appCallAppDealOwner.AppCallAppPaymentChangeHomeFragment();
                if (AppCallAppUtils.isFromFpsUrl(actionUrl)) {
//                    String merchantLink = (String) eventParams.get(APP_MERCHANT_CALLBACK_URL);
                    AppCallAppPaymentActivity.startAppCallApp(appCallAppDealOwner.getCurrentActivity(), actionUrl, null, null, false);
                } else {
                    appCallAppDealOwner.getCurrentPresenter().getAppCallAppLink(null, actionUrl, null, null);
                }
                return true;
            }
        }
        return false;
    }

    public interface AppCallAppDealOwner extends EventDealOwner {
        Handler getHandler();

        MainContract.Presenter getCurrentPresenter();

        void AppCallAppPaymentChangeHomeFragment();
//        void updateInterceptTouchEventStatus(boolean isShouldInterceptTouchEvent);
    }
}
