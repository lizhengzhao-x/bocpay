package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.MenuFilterAdapter;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class FilterTypePopWindow extends BasePopWindow implements View.OnClickListener {


    private androidx.recyclerview.widget.RecyclerView mId_recyclerview;
    private TextView mId_clear_filter;
    private TextView mId_filter_confirm;
    private List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities;
    private OnFilterSelListener onFilterSelListener;
    private int maxWidth;

    private void bindViews(View contentView) {
        mId_recyclerview = (androidx.recyclerview.widget.RecyclerView) contentView.findViewById(R.id.id_recyclerview);
        mId_clear_filter = (TextView) contentView.findViewById(R.id.id_clear_filter);
        mId_filter_confirm = (TextView) contentView.findViewById(R.id.id_filter_confirm);
    }


    public FilterTypePopWindow(Activity mActivity, int maxWidthIn, List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntitiesIn, OnFilterSelListener onFilterSelListenerIn) {
        super(mActivity);
        this.menuFilterEntities = new ArrayList<>();
        this.menuFilterEntities.addAll(menuFilterEntitiesIn);
        this.onFilterSelListener = onFilterSelListenerIn;
        this.maxWidth = maxWidthIn;
        initView();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_ecoup_filter);
        bindViews(mContentView);
        setOutsideTouchable(false);
        setWidth(maxWidth);
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerview.setLayoutManager(layoutManager);
        mId_recyclerview.setAdapter(getAdapter(null));
        ((IAdapter<RedeemableGiftListEntity.BuMapsBean>) mId_recyclerview.getAdapter()).setData(menuFilterEntities);
        mId_recyclerview.getAdapter().notifyDataSetChanged();

        mId_clear_filter.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                for (int i = 0; i < menuFilterEntities.size(); i++) {
                    menuFilterEntities.get(i).setSel(false);
                }
                mId_recyclerview.getAdapter().notifyDataSetChanged();
            }
        });
        mId_filter_confirm.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onFilterSelListener.onFilterSelConfirm(menuFilterEntities);
                dismiss();
            }
        });
        updateClearTextColor();
    }

    private void updateClearTextColor() {
        boolean hasSel = false;
        for (int i = 0; i < menuFilterEntities.size(); i++) {
            if (menuFilterEntities.get(i).isSel()) {
                hasSel = true;
                break;
            }
        }
        mId_clear_filter.setTextColor(hasSel ? Color.parseColor("#000000") : Color.parseColor("#4A5461"));
    }

    private CommonRcvAdapter<RedeemableGiftListEntity.BuMapsBean> getAdapter(final List<RedeemableGiftListEntity.BuMapsBean> data) {
        return new CommonRcvAdapter<RedeemableGiftListEntity.BuMapsBean>(data) {

            @Override
            public Object getItemType(RedeemableGiftListEntity.BuMapsBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new MenuFilterAdapter(mActivity, new MenuFilterAdapter.OnItemSelClicklistener() {
                    @Override
                    public void onItemClick(int position) {
                        menuFilterEntities.get(position).setSel(!menuFilterEntities.get(position).isSel());
                        mId_recyclerview.getAdapter().notifyDataSetChanged();
                        updateClearTextColor();
                    }
                });
            }
        };
    }

    public interface OnFilterSelListener {
        void onFilterSelConfirm(List<RedeemableGiftListEntity.BuMapsBean> menuFilterEntities);
    }
}
