package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ramon on 2018/10/9.
 */

public class FpsBankRecordBean extends BaseEntity {
    //如果为空，说明没有登记
    // -- 账户 手机号或者邮箱
    String accountId;
    // -- 账户类型 0：手机号 1：邮箱
    String accountIdType;
    // -- 收款账户
    String accountNo;
    // -- 是否默认收款银行 Y:是 N:否
    String defaultBank;
    // -- 账户状态
    String accountStatus;
    //--- 收款账户类型
    String accountType;
    //--- 收款账户类型名称
    String accountTypeName;

    // -- 账户状态-暂时没用
    String status;
    //--- FPP唯一标识
    String fppRefNo;
    // --- 银行代码
    String bankCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFppRefNo() {
        return fppRefNo;
    }

    public void setFppRefNo(String fppRefNo) {
        this.fppRefNo = fppRefNo;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccountTypeName() {
        return accountTypeName;
    }

    public void setAccountTypeName(String accountTypeName) {
        this.accountTypeName = accountTypeName;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountIdType() {
        return accountIdType;
    }

    public void setAccountIdType(String accountIdType) {
        this.accountIdType = accountIdType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getDefaultBank() {
        return defaultBank;
    }

    public void setDefaultBank(String defaultBank) {
        this.defaultBank = defaultBank;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

}
