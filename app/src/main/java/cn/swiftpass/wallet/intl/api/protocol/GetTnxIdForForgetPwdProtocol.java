package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * 注册获取tnx id
 */
public class GetTnxIdForForgetPwdProtocol extends BaseProtocol {

    public static final String TAG = GetTnxIdForForgetPwdProtocol.class.getSimpleName();

    public GetTnxIdForForgetPwdProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/getTxnId";
    }


}
