package cn.swiftpass.wallet.intl.entity.event;


import cn.swiftpass.wallet.intl.entity.RedPackageEntity;

public class RedPushEventEntity extends BaseEventEntity{

    public static final int EVENT_KEY_NOTIFIACTION_COMMON_RED_PACKET_OPEN = 0x10211;//通知推送-派利是普通打开红包

    public static final int EVENT_KEY_NOTIFIACTION_STAFF_RED_PACKET_OPEN = 0x10212;//通知推送-派利是员工打开红包

    private RedPackageEntity redPackageEntity;

    public RedPackageEntity getRedPackageEntity() {
        return redPackageEntity;
    }

    public RedPushEventEntity(int eventType, String message,RedPackageEntity redPackageEntity) {
        super(eventType, message);
        this.redPackageEntity = redPackageEntity;
    }

    public RedPushEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
