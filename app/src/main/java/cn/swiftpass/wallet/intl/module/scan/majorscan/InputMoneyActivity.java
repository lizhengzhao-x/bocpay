package cn.swiftpass.wallet.intl.module.scan.majorscan;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.OrderDetailGradeDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentListDialogFragment;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.MpQrUrlInfoResult;
import cn.swiftpass.wallet.intl.entity.ParserQrcodeEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.scan.majorscan.contract.InputMoneyContract;
import cn.swiftpass.wallet.intl.module.scan.majorscan.presenter.InputMoneyPresenter;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.SettingCardDefaultAcitivity;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferInputMoneyActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;


/**
 * 主扫输入金额 发起支付
 */
public class InputMoneyActivity extends BaseCompatActivity<InputMoneyContract.Presenter> implements InputMoneyContract.View {
    private final String TAG = "InputMoneyActivity";
    @BindView(R.id.id_img_title)
    ImageView idImgTitle;
    @BindView(R.id.id_tv_title)
    TextView idTvTitle;
    @BindView(R.id.id_edit_money)
    EditTextWithDel idEditMoney;
    @BindView(R.id.id_btn_ok)
    Button idBtnOk;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_bottom_line)
    View idBottomLine;
    //支付列表
    private ArrayList<BankCardEntity> currentPaymentLists;
    //扫码二维码信息
    private MpQrUrlInfoResult urlQRInfoEntity;
    //二维码类型
    private String curentQrcodeType;


    //收银台
    private OrderDetailGradeDialogFragment orderDetailDialogFragment;
    //支付列表
    private PaymentListDialogFragment paymentListDialogFragment;
    //支付bean
    private ActionTrxGpInfoEntity actionTrxGpInfoEntity;
    //标识当前订单是否使用积分
    private boolean currencyUsePoint;
    /**
     * 默认的cardId 当前支付的cardId
     */
    private String originalCardId;
    private String currentOrderQrcodeStr;


    @Override
    public void init() {

        setToolBarTitle(R.string.pop_order_detail);

        idEditMoney.setLineVisible(false);

        curentQrcodeType = getIntent().getExtras().getString(Constants.SCAN_CARD_TYPE);
        //TODO 这个值并没有用到，是否真的不需要呢？？
        currentOrderQrcodeStr = getIntent().getExtras().getString(Constants.QRCODE_CONTENT);
        currentPaymentLists = new ArrayList<>();
        urlQRInfoEntity = (MpQrUrlInfoResult) getIntent().getExtras().getSerializable(Constants.URLQRINFO_ENTITY);
        idPaySymbol.setText(AndroidUtils.getTrxCurrency(urlQRInfoEntity.getTranCur()));
//        urlQRInfoEntity.setTranCur(AndroidUtils.getTrxCurrency(urlQRInfoEntity.getTranCur()));
        //扫描的二维码解析出来是否有金额参数
        if (TextUtils.isEmpty(urlQRInfoEntity.getTranAmt())) {
            showKeyBoard();
            updateOkBackground(false);
        } else {
            idEditMoney.setContentText(urlQRInfoEntity.getTranAmt());
            idEditMoney.setFocusable(false);
            idEditMoney.setEnabled(false);
        }
        idBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hintKeyboard();
                initOrderPopWindow();
            }
        });

        //两位小数过滤 8位长度限制
        idEditMoney.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(10)});
        idEditMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateOkBackground(idEditMoney.getText().toString().trim());
            }
        });

        idBottomLine.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    idBottomLine.setBackgroundColor(getResources().getColor(R.color.app_black));
                } else {
                    idBottomLine.setBackgroundColor(getResources().getColor(R.color.line_common));
                }
            }
        });
    }

    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (idEditMoney == null) return;
                idEditMoney.setFocusable(true);
                idEditMoney.requestFocus();
                AndroidUtils.showKeyboardView(idEditMoney.getEditText());
            }
        }, 500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //重新加载卡列表 有可能卡列表顺序发生了改变
        // currentPaymentLists = (ArrayList<BankCardEntity>) CacheManagerInstance.getInstance().getCardEntities();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.ac_input_money;
    }

    @Override
    protected InputMoneyContract.Presenter createPresenter() {
        return new InputMoneyPresenter();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }


    private void hintKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm.isActive() && getCurrentFocus() != null) {
            if (getCurrentFocus().getWindowToken() != null) {
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    private void updateOkBackground(String currentStr) {
        boolean isSel = true;
        if (currentStr.length() == 0) {
            isSel = false;
        }
        double currentV = 0;
        try {
            currentV = Double.valueOf(currentStr);
        } catch (NumberFormatException e) {

            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (currentV == 0) {
            isSel = false;
        }
        updateOkBackground(isSel);
    }

    private void updateOkBackground(boolean isSel) {
        idBtnOk.setEnabled(isSel);
        idBtnOk.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    public void inoutPswPopWindow() {
        if (!CacheManagerInstance.getInstance().checkLoginStatus()) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class);
            MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
            return;
        }
        if (getActivity() == null) {
            return;
        }
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (orderDetailDialogFragment != null) {
                    orderDetailDialogFragment.setInterceptClickEvent(true);
                }
                submitOrderInfoRequest();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    public void initOrderPopWindow() {
        String moneyStr = idEditMoney.getText().toString().trim();
        if (moneyStr.startsWith(".")) {
            showErrorMsgDialog(mContext, getString(R.string.error_msg_input_payment_amount));
            return;
        }
        double moneyValue = 0;
        try {
            moneyValue = Double.parseDouble(moneyStr);
        } catch (NumberFormatException e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        if (moneyValue <= 0) {
            showErrorMsgDialog(mContext, getString(R.string.error_msg_input_payment_amount));
            return;
        }
        urlQRInfoEntity.setTranAmt(moneyStr);
        if (TextUtils.equals(curentQrcodeType, Constants.MAKE_ORDER_MPURL)) {
            originalCardId = urlQRInfoEntity.getCardId();
            showCheckWithPointStand(AndroidUtils.convertActionTrxGpInfoEntity(urlQRInfoEntity));
        } else {
            urlQRInfoEntity.setCardId(null);
            mPresenter.getActionTrxGpInfo(urlQRInfoEntity);
        }
    }

    /**
     * 根据二维码信息查询支付列表
     *
     * @param qrcode
     */
    private void getPaymentList(final String qrcode, final boolean isUrlMode) {
        if (currentPaymentLists != null && currentPaymentLists.size() > 0) {
            showPaymentListDialog(currentPaymentLists, isUrlMode);
            return;
        }
        mPresenter.getPaymentTypeList(qrcode, isUrlMode);

    }


    /**
     * 根据订单结果显示收银台
     *
     * @param actionTrxGpInfoEntity
     */
    private void showCheckWithPointStand(final ActionTrxGpInfoEntity actionTrxGpInfoEntity) {
        this.actionTrxGpInfoEntity = actionTrxGpInfoEntity;
        originalCardId = actionTrxGpInfoEntity.getCardId();
        urlQRInfoEntity.setCardId(actionTrxGpInfoEntity.getCardId());
        if (orderDetailDialogFragment == null) {
            orderDetailDialogFragment = new OrderDetailGradeDialogFragment();
            OrderDetailGradeDialogFragment.OnDialogClickListener onDialogClickListener = new OrderDetailGradeDialogFragment.OnDialogClickListener() {
                @Override
                public void onOkListener(boolean isPoint) {
                    if (orderDetailDialogFragment.isFpsPaymentType()) {
                        //走fps转账流程
                        mPresenter.parserTransferCode(urlQRInfoEntity.getQrcodeStr());
                    } else {
                        currencyUsePoint = isPoint;
                        inoutPswPopWindow();
                    }
                }

                @Override
                public void onSelCardListener() {
                    getPaymentList(urlQRInfoEntity.getQrcodeStr(), actionTrxGpInfoEntity.isUrlCode());
                }

                @Override
                public void onDisMissListener() {
                    orderDetailDialogFragment = null;
                    //收银台取消 要恢复默认的cardId 防止用户在卡列表中切换卡选择改变内存中
                    if (urlQRInfoEntity != null) {
                        urlQRInfoEntity.setCardId(originalCardId);
                    }

                }
            };
            orderDetailDialogFragment.initParams(actionTrxGpInfoEntity, onDialogClickListener);
            orderDetailDialogFragment.show(getSupportFragmentManager(), "OrderDetailGradeDialogFragment");
        } else {
            orderDetailDialogFragment.updateParams(actionTrxGpInfoEntity);
        }

    }

    /**
     * 订单提交
     */
    private void submitOrderInfoRequest() {
        MainScanOrderRequestEntity orderRequest = new MainScanOrderRequestEntity();
        orderRequest.setTrxAmt(urlQRInfoEntity.getTranAmt());
        if (TextUtils.equals(curentQrcodeType, Constants.MAKE_ORDER_MPURL)) {
            orderRequest.setQrcType(Constants.MAKE_ORDER_MPURL);
        } else {
            orderRequest.setQrcType(Constants.MAKE_ORDER_MPEMV);
        }
        orderRequest.setTrxFeeAmt("0");
        orderRequest.setMpQrCode(urlQRInfoEntity.getQrcodeStr());
        orderRequest.setCardId(urlQRInfoEntity.getCardId());

        orderRequest.setTxnId(actionTrxGpInfoEntity.getTranID());
        orderRequest.setTxnCurr(actionTrxGpInfoEntity.getTranCur());

        if (currencyUsePoint && actionTrxGpInfoEntity.getRedeemFlag().equals("1")) {
            orderRequest.setMerchantName(actionTrxGpInfoEntity.getName());
            orderRequest.setRedeemGpAmt(actionTrxGpInfoEntity.getRedeemGpTnxAmt());
            orderRequest.setGpRequired(actionTrxGpInfoEntity.getGpRequired());
            orderRequest.setRedeemFlag(actionTrxGpInfoEntity.getRedeemFlag());
            orderRequest.setGpCode(actionTrxGpInfoEntity.getGpCode());
            orderRequest.setYearHolding(actionTrxGpInfoEntity.getYearHolding());
            orderRequest.setPayAmt(actionTrxGpInfoEntity.getPayAmt());
            orderRequest.setRedeemGpCount(actionTrxGpInfoEntity.getRedeemGpCount());
            orderRequest.setAvaGpCount(actionTrxGpInfoEntity.getAvaGpCount());
            mPresenter.actionMpqrPaymentWithPoint(orderRequest);
        } else {
            orderRequest.setMerchantName(urlQRInfoEntity.getName());
            mPresenter.getMainScanOrder(orderRequest);
        }


        if (mHandler != null) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (orderDetailDialogFragment != null) {
                        orderDetailDialogFragment.setInterceptClickEvent(false);
                    }
                }
            }, 200);
        }
    }

    private void showPaymentListDialog(final ArrayList<BankCardEntity> bankCardEntities, final boolean isUrlMode) {
        paymentListDialogFragment = new PaymentListDialogFragment();
        PaymentListDialogFragment.OnPaymentListClickListener onPaymentListClickListener = new PaymentListDialogFragment.OnPaymentListClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onMoreBtnClickListener() {
                ActivitySkipUtil.startAnotherActivity(getActivity(), SettingCardDefaultAcitivity.class);
            }

            @Override
            public void onCardSelListener(int positon) {
                //切换卡号   //卡列表选择
                urlQRInfoEntity.setCardId(bankCardEntities.get(positon).getCardId());
                if (!isUrlMode) {
                    //不是url mode 才需要 重新拉取积分
                    mPresenter.getActionTrxGpInfo(urlQRInfoEntity);
                } else {
                    orderDetailDialogFragment.setDefaultCardId(bankCardEntities.get(positon));
                }
            }
        };
        paymentListDialogFragment.initParams(onPaymentListClickListener, bankCardEntities, orderDetailDialogFragment.getCurrentSelCardId(), orderDetailDialogFragment.isFpsPaymentType());
        paymentListDialogFragment.show(getSupportFragmentManager(), "paymentListDialogFragment");
    }

    @Override
    public void actionMpqrPaymentWithPointFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(InputMoneyActivity.this, errorMsg);
    }

    @Override
    public void actionMpqrPaymentWithPointSuccess(ContentEntity response) {
        String tansId = response.getResult_msg();
        mPresenter.getPaymentResult(tansId, urlQRInfoEntity.getCardId(), urlQRInfoEntity.getQrcodeStr());
    }

    @Override
    public void getPaymentResultFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(InputMoneyActivity.this, errorMsg);
    }

    @Override
    public void getPaymentResultSuccess(PaymentEnquiryResult response) {
        //TODO 如果不是这个状态怎么办呢？
        if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_SUCCESS)) {
            //支付成功
            GradeReduceSuccessActivity.startActivityFromMajorScan(InputMoneyActivity.this, response);
        }
    }

    @Override
    public void getMainScanOrderFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(InputMoneyActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                boolean isFlag = TextUtils.equals(curentQrcodeType, Constants.MAKE_ORDER_MPURL) && (errorCode.equals("EWA3015") || errorCode.equals("EWA3004") || errorCode.startsWith("EWA80"));
                if (isFlag) {
                    finish();
                }
            }
        });
    }

    @Override
    public void getMainScanOrderSuccess(ContentEntity response) {
        String tansId = response.getResult_msg();
        mPresenter.getPaymentResult(tansId, urlQRInfoEntity.getCardId(), urlQRInfoEntity.getQrcodeStr());
    }

    @Override
    public void getActionTrxGpInfoFail(String errorCode, String errorMsg) {
        //用户如果切换card 拉取详情 失败 内存中订单模型需要恢复到上一次成功的cardid
        urlQRInfoEntity.setCardId(originalCardId);
        showErrorMsgDialog(InputMoneyActivity.this, errorMsg);
    }

    @Override
    public void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity response) {
        showCheckWithPointStand(response);
    }

    @Override
    public void getPaymentTypeListFail(String errorCode, String errorMsg) {

    }

    @Override
    public void getPaymentTypeListSuccess(CardsEntity response, boolean isUrlMode) {
        currentPaymentLists = response.getRows();
        showPaymentListDialog(currentPaymentLists, isUrlMode);
    }

    @Override
    public void parserTransferCodeFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(InputMoneyActivity.this, errorMsg);
    }

    @Override
    public void parserTransferCodeSuccess(ParserQrcodeEntity response) {
        response.setTransferAmount(idEditMoney.getText().toString());
        Intent intent = new Intent();
        intent.putExtra(Constants.TYPE, Constants.SCAN_TO_TRANSFER);
        intent.putExtra(TransferConst.TRANS_FPS_DATA, response);
        intent.setClass(getActivity(), TransferInputMoneyActivity.class);
        startActivity(intent);
    }
}
