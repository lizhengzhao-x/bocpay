package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

public class S2ForgetPwdCustomerInfoProtocol extends BaseProtocol {


    public S2ForgetPwdCustomerInfoProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/initCustomerInfo";

    }

}
