package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */
public class PAAccountVerifyCodeProtocol extends BaseProtocol {


    String width = "200";
    //验证码长度，不大于200
    String height = "60";
    //验证码高度，不大于150
    String codeCount = "4";
    //验证码长度，一般是4位，不大于8位
    //
    String action;


    public PAAccountVerifyCodeProtocol(String action, NetWorkCallbackListener dataCallback){
        this.action = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/getVerifyCode";
        mEncryptFlag = false;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CODE_HEIGHT, height);
        mBodyParams.put(RequestParams.CODE_WIDTH, width);
        mBodyParams.put(RequestParams.CODE_NUM, codeCount);
        mBodyParams.put(RequestParams.ACTION, action);
    }


}
