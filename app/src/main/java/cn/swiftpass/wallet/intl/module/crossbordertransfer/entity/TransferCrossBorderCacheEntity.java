package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import com.google.gson.annotations.SerializedName;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.PayeeEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:31
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderCacheEntity extends PayeeEntity {


    @SerializedName("txTime")
    public String date;

    @SerializedName("amount")
    public String amount;
    @SerializedName("cur")
    public String currency;

    public String availableLimit;


    public TransferCrossBorderAccountListEntity getAccountObj() {
        return accountObj;
    }

    public void setAccountObj(TransferCrossBorderAccountListEntity accountObj) {
        this.accountObj = accountObj;
    }

    private TransferCrossBorderAccountListEntity accountObj;

    public UsedObjBean getForUsedObj() {
        return forUsedObj;
    }

    public void setForUsedObj(UsedObjBean forUsedObj) {
        this.forUsedObj = forUsedObj;
    }

    private UsedObjBean forUsedObj;

    public String getAvailableLimit() {
        return availableLimit;
    }

    public void setAvailableLimit(String availableLimit) {
        this.availableLimit = availableLimit;
    }

    public TransferCrossBorderCacheEntity() {
        super();
    }

    public TransferCrossBorderCacheEntity(String date, String amount) {
        this.date = date;
        this.amount = amount;
    }

    public TransferCrossBorderCacheEntity(String date, String amount, String currency) {
        this.date = date;
        this.amount = amount;
        this.currency = currency;
    }

    public TransferCrossBorderCacheEntity(String payeeName, String payeeHideName, String payeeCardId, String payeeBank, String date, String amount, String currency) {
        super(payeeName, payeeHideName, payeeCardId, payeeBank);
        this.date = date;
        this.amount = amount;
        this.currency = currency;
    }

//    public static class AccountObjBean extends BaseEntity{
//
//
//        /**
//         * panFour : 2764
//         * cardType : 2
//         * cardId : 2742
//         * balance : 9890.86
//         * pan : 012048002764
//         */
//
//        private String panFour;
//        private String cardType;
//        private String cardId;
//        private String balance;
//        private String pan;
//
//        public String getPanFour() {
//            return panFour;
//        }
//
//        public void setPanFour(String panFour) {
//            this.panFour = panFour;
//        }
//
//        public String getCardType() {
//            return cardType;
//        }
//
//        public void setCardType(String cardType) {
//            this.cardType = cardType;
//        }
//
//        public String getCardId() {
//            return cardId;
//        }
//
//        public void setCardId(String cardId) {
//            this.cardId = cardId;
//        }
//
//        public String getBalance() {
//            return balance;
//        }
//
//        public void setBalance(String balance) {
//            this.balance = balance;
//        }
//
//        public String getPan() {
//            return pan;
//        }
//
//        public void setPan(String pan) {
//            this.pan = pan;
//        }
//    }


    public static class UsedObjBean extends BaseEntity {


        public UsedObjBean(String code, String value) {
            this.value = value;
            this.code = code;
        }

        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        private String code;

    }
}
