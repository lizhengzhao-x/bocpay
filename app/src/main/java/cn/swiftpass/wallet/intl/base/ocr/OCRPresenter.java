package cn.swiftpass.wallet.intl.base.ocr;


import cn.swiftpass.boc.commonui.base.mvp.IView;


public interface OCRPresenter<V extends IView> {
    /**
     * 通过查询证件信息
     *
     * @param seqNumber
     * @param fileId
     */
    void getOcrInfo(String action, String seqNumber, String fileId);
}
