package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/5.
 */
public class TransferResourcesEntity extends BaseEntity {


    /**
     * def : {"bck_imgs":[{"readPackageImageUrl":"urlllll","bacImageUrl":"dfasdfadsfadsf"},{"readPackageImageUrl":"urlllll","bacImageUrl":"dfasdfadsfadsf"}],"def_postscript":{"pst_hk":["新年快樂","龍馬精神","恭喜發財","財源廣進","身體健康","橫財就手","金銀滿屋","家肥屋潤","出入平安","心想事成","吉祥如意","福氣滿滿"],"pst_en":["新年快乐","龙马精神","恭喜发财","财源广进","身体健康","横财就手","金银满屋","家肥屋润","出入平安","心想事成","吉祥如意","福气满满"],"pst_cn":["新年快乐","龙马精神","恭喜发财","财源广进","身体健康","横财就手","金银满屋","家肥屋润","出入平安","心想事成","吉祥如意","福气满满"]},"def_cur":"$","def_amt":["10","20","50","100"]}
     */

    private DefBean def;

    public DefBean getDef() {
        return def;
    }

    public void setDef(DefBean def) {
        this.def = def;
    }

    public static class DefBean extends BaseEntity {
        /**
         * bck_imgs : [{"readPackageImageUrl":"urlllll","bacImageUrl":"dfasdfadsfadsf"},{"readPackageImageUrl":"urlllll","bacImageUrl":"dfasdfadsfadsf"}]
         * def_postscript : {"pst_hk":["新年快樂","龍馬精神","恭喜發財","財源廣進","身體健康","橫財就手","金銀滿屋","家肥屋潤","出入平安","心想事成","吉祥如意","福氣滿滿"],"pst_en":["新年快乐","龙马精神","恭喜发财","财源广进","身体健康","横财就手","金银满屋","家肥屋润","出入平安","心想事成","吉祥如意","福气满满"],"pst_cn":["新年快乐","龙马精神","恭喜发财","财源广进","身体健康","横财就手","金银满屋","家肥屋润","出入平安","心想事成","吉祥如意","福气满满"]}
         * def_cur : $
         * def_amt : ["10","20","50","100"]
         */

        private DefPostscriptBean def_postscript;
        private String def_cur;
        private List<BckImgsBean> bck_imgs;
        private List<String> def_amt;

        public DefPostscriptBean getDef_postscript() {
            return def_postscript;
        }

        public void setDef_postscript(DefPostscriptBean def_postscript) {
            this.def_postscript = def_postscript;
        }

        public String getDef_cur() {
            return def_cur;
        }

        public void setDef_cur(String def_cur) {
            this.def_cur = def_cur;
        }

        public List<BckImgsBean> getBck_imgs() {
            return bck_imgs;
        }

        public void setBck_imgs(List<BckImgsBean> bck_imgs) {
            this.bck_imgs = bck_imgs;
        }

        public List<String> getDef_amt() {
            return def_amt;
        }

        public void setDef_amt(List<String> def_amt) {
            this.def_amt = def_amt;
        }

        public static class DefPostscriptBean extends BaseEntity {
            private List<String> pst_hk;
            private List<String> pst_en;
            private List<String> pst_cn;

            public List<String> getPst_hk() {
                return pst_hk;
            }

            public void setPst_hk(List<String> pst_hk) {
                this.pst_hk = pst_hk;
            }

            public List<String> getPst_en() {
                return pst_en;
            }

            public void setPst_en(List<String> pst_en) {
                this.pst_en = pst_en;
            }

            public List<String> getPst_cn() {
                return pst_cn;
            }

            public void setPst_cn(List<String> pst_cn) {
                this.pst_cn = pst_cn;
            }
        }

        public static class BckImgsBean extends BaseEntity {

            private String coverId;

            public String getCoverId() {
                return coverId;
            }

            public void setCoverId(String coverId) {
                this.coverId = coverId;
            }

            public boolean isSel() {
                return isSel;
            }

            public void setSel(boolean sel) {
                isSel = sel;
            }

            private boolean isSel;
            /**
             * readPackageImageUrl : urlllll
             * bacImageUrl : dfasdfadsfadsf
             */

            private String readPackageImageUrl;

            public String getNewRedPackageImageUrl() {
                return redPackageNewImageUrl;
            }

            public void setNewRedPackageImageUrl(String redPackageNewImageUrl) {
                this.redPackageNewImageUrl = redPackageNewImageUrl;
            }

            private String redPackageNewImageUrl;
            private String bacImageUrl;

            private String bottomImageUrl;
            private String centerImageUrl;
            private String redPackageNewImageTopUrl;

            public String getBottomBigSizeImageUrl() {
                return bottomBigSizeImageUrl;
            }

            public void setBottomBigSizeImageUrl(String bottomBigSizeImageUrl) {
                this.bottomBigSizeImageUrl = bottomBigSizeImageUrl;
            }

            private String bottomBigSizeImageUrl;

            public String getRedPackageNewImageUrl() {
                return redPackageNewImageUrl;
            }

            public void setRedPackageNewImageUrl(String redPackageNewImageUrl) {
                this.redPackageNewImageUrl = redPackageNewImageUrl;
            }

            public String getCenterImageUrl() {
                return centerImageUrl;
            }

            public void setCenterImageUrl(String centerImageUrl) {
                this.centerImageUrl = centerImageUrl;
            }

            public String getRedPackageNewImageTopUrl() {
                return redPackageNewImageTopUrl;
            }

            public void setRedPackageNewImageTopUrl(String redPackageNewImageTopUrl) {
                this.redPackageNewImageTopUrl = redPackageNewImageTopUrl;
            }

            public String getRedPacketCoverImage() {
                return redPacketCoverImage;
            }

            public void setRedPacketCoverImage(String redPacketCoverImage) {
                this.redPacketCoverImage = redPacketCoverImage;
            }

            public String getSmallNewUrl() {
                return smallNewUrl;
            }

            public void setSmallNewUrl(String smallNewUrl) {
                this.smallNewUrl = smallNewUrl;
            }

            private String redPacketCoverImage;

            public String getBottomImageUrl() {
                return bottomImageUrl;
            }

            public void setBottomImageUrl(String bottomImageUrl) {
                this.bottomImageUrl = bottomImageUrl;
            }

            public String getSmallUrl() {
                return smallUrl;
            }

            public void setSmallUrl(String smallUrl) {
                this.smallUrl = smallUrl;
            }

            private String smallUrl;

            public String getNewSmallUrl() {
                return smallNewUrl;
            }

            public void setNewSmallUrl(String smallNewUrl) {
                this.smallNewUrl = smallNewUrl;
            }

            private String smallNewUrl;

            public String getReadPackageImageUrl() {
                return readPackageImageUrl;
            }

            public void setReadPackageImageUrl(String readPackageImageUrl) {
                this.readPackageImageUrl = readPackageImageUrl;
            }

            public String getBacImageUrl() {
                return bacImageUrl;
            }

            public void setBacImageUrl(String bacImageUrl) {
                this.bacImageUrl = bacImageUrl;
            }
        }
    }
}
