package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

/**
 * Android中为PopupWindow设置半透明背景的方案（兼容华为手机）
 * https://blog.csdn.net/biaobiao1217/article/details/51438552/
 */

public abstract class BasePopWindow extends PopupWindow {

    protected Context mContext;
    protected Activity mActivity;
    protected View mContentView;

    protected boolean isDismissChangeAlpha = true;

    public boolean isDismissChangeAlpha() {
        return isDismissChangeAlpha;
    }

    public void setDismissChangeAlpha(boolean dismissChangeAlpha) {
        isDismissChangeAlpha = dismissChangeAlpha;
    }

    public BasePopWindow(Activity mActivity, boolean isDismissChangeAlpha) {
        this.isDismissChangeAlpha = isDismissChangeAlpha;
        initView(mActivity);
    }

    public BasePopWindow(Activity mActivity) {
        initView(mActivity);
    }

    protected void initView(Activity context) {
        mActivity = context;
        this.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setHeight(ViewGroup.LayoutParams.MATCH_PARENT);
        this.setFocusable(true);
    }

    protected void setDisMissView(final View contentView) {
        contentView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int height = contentView.getTop();
                int y = (int) event.getY();
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (y < height) {
                        dismiss();
                    }
                }
                return true;
            }
        });
    }


    //设置屏幕背景透明度
    protected void setBackgroudAlpha(float alpha) {
        if (mActivity != null && !mActivity.isFinishing()) {
            WindowManager.LayoutParams l = mActivity.getWindow().getAttributes();
            l.alpha = alpha;
            if (alpha == 1) {
                //不移除该Flag的话,在有视频的页面上的视频会出现黑屏的bug
                mActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            } else {
                //此行代码主要是解决在华为手机上半透明效果无效的bug
                mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
            }
            mActivity.getWindow().setAttributes(l);
        }
    }

    //点击其他部分popwindow消失时，屏幕恢复透明度
    class popupwindowdismisslistener implements PopupWindow.OnDismissListener {

        @Override
        public void onDismiss() {
            setBackgroudAlpha((float) 1);
        }
    }

    @Override
    public void showAtLocation(View parent, int gravity, int x, int y) {
        super.showAtLocation(parent, gravity, x, y);
        setBackgroudAlpha((float) 0.5);
    }

    @Override
    public void showAsDropDown(View anchor) {
        super.showAsDropDown(anchor);
        setBackgroudAlpha((float) 0.5);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (isDismissChangeAlpha) {
            setBackgroudAlpha((float) 1);
        }
    }

    protected void setContentView(int viewId) {
        mContentView = getLayoutInfalter().inflate(viewId, null);
        setContentView(mContentView);
    }

    protected abstract void initView();

    protected LayoutInflater getLayoutInfalter() {

        return (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

}
