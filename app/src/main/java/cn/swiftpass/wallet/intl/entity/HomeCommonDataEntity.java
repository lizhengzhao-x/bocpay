package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class HomeCommonDataEntity extends BaseEntity {
    private BannerImageEntity redPacket;
    private BannerImageEntity homePage;
    /**
     * 跨境汇款结果页 banner
     */
    private BannerImageEntity moneyTransfer;
    private BannerImageEntity redEnvelopes;
    private BannerImageEntity localPage;
    private BannerImageEntity pTwoM;
    private BannerImageEntity pTwoP;
    /**
//     * 交易记录 跨境汇款保存本地的图片
//     */
//    private BannerImageEntity crossBorderRecord;
    /**
     * 交易记录 分享banner 不区分交易记录类型
     */
    private BannerImageEntity transferRecord;
    /**
     * 交易记录 分享底部文案 不区分交易记录类型
     */
    private String txnDetailShareText;

    /**
     * 跨境汇款donepage 分享文案
     */
    private String crossBorderRemittanceSuccessText;

    public String getCrossBorderShareText() {
        return crossBorderRemittanceSuccessText;
    }

    public String getCrossBorderRemittanceSuccessText() {
        return crossBorderRemittanceSuccessText;
    }

    public void setCrossBorderRemittanceSuccessText(String crossBorderRemittanceSuccessText) {
        this.crossBorderRemittanceSuccessText = crossBorderRemittanceSuccessText;
    }

    public BannerImageEntity getRedPacket() {
        return redPacket;
    }

    public void setRedPacket(BannerImageEntity redPacket) {
        this.redPacket = redPacket;
    }

    public BannerImageEntity getHomePage() {
        return homePage;
    }

    public void setHomePage(BannerImageEntity homePage) {
        this.homePage = homePage;
    }

    public BannerImageEntity getMoneyTransfer() {
        return moneyTransfer;
    }

    public void setMoneyTransfer(BannerImageEntity moneyTransfer) {
        this.moneyTransfer = moneyTransfer;
    }

    public BannerImageEntity getRedEnvelopes() {
        return redEnvelopes;
    }

    public void setRedEnvelopes(BannerImageEntity redEnvelopes) {
        this.redEnvelopes = redEnvelopes;
    }

    public BannerImageEntity getLocalPage() {
        return localPage;
    }

    public void setLocalPage(BannerImageEntity localPage) {
        this.localPage = localPage;
    }

    public BannerImageEntity getpTwoM() {
        return pTwoM;
    }

    public void setpTwoM(BannerImageEntity pTwoM) {
        this.pTwoM = pTwoM;
    }

    public BannerImageEntity getpTwoP() {
        return pTwoP;
    }

    public void setpTwoP(BannerImageEntity pTwoP) {
        this.pTwoP = pTwoP;
    }

    public String getTxnDetailShareText() {
        return txnDetailShareText;
    }

    public void setTxnDetailShareText(String txnDetailShareText) {
        this.txnDetailShareText = txnDetailShareText;
    }

//    public BannerImageEntity getCrossBorderRecord() {
//        return crossBorderRecord;
//    }
//
//    public void setCrossBorderRecord(BannerImageEntity crossBorderRecord) {
//        this.crossBorderRecord = crossBorderRecord;
//    }

    public BannerImageEntity getTransferRecord() {
        return transferRecord;
    }

    public void setTransferRecord(BannerImageEntity transferRecord) {
        this.transferRecord = transferRecord;
    }
}
