package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class InviteListEntity extends BaseEntity {

    public List<InviteContactEntity> result = new ArrayList<>();
}
