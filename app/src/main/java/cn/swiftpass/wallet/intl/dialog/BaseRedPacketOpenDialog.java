package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import me.jessyan.autosize.utils.AutoSizeUtils;

public abstract class BaseRedPacketOpenDialog extends Dialog {
    private Context mContext;
    protected static int childTotalWidth;
    protected static int childTotalHeight;

    public BaseRedPacketOpenDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    public BaseRedPacketOpenDialog(Context context, int theme) {
        super(context, theme);
        this.mContext = context;
    }

    public void showDailog() {
        android.view.WindowManager.LayoutParams p = getWindow().getAttributes();  //获取对话框当前的参数值
        p.height = AndroidUtils.getScreenHeight(mContext);
        p.width = AndroidUtils.getScreenWidth(mContext);
        getWindow().setAttributes(p);//设置生效
        show();
    }

    public static class Builder {
        protected Context context;
        protected ConFrimClick positiveButtonClickListener;
        protected View.OnClickListener closeClickListener;
        TextView tvTransferBtnNext;
        ImageView ivClose;

        RelativeLayout rlyRedPacketContain;
        TextView tvDialogRedPacketTitle;
        TextView tvDialogRedPacketBless;
        TextView tvDialogRedPacketAmount;
        TextView tvDialogRedPacketTop;


        LinearLayout llyAmountContain;
        LinearLayout llyCommonContain;
        LinearLayout llyAllContain;
        LinearLayout llyRedPacketMainContain;

        RelativeLayout rlyRedPacketProgress;
        ImageView ivRedPacketLoading;

        TextView tvDialogRedPacketTitleFlag;
        TextView tvDialogRedPacketAllNum;
        protected String title;
        protected String titleFlag;
        protected String blessingWords;
        protected String amount;
        protected String redOpenNum;

        protected String nextRedPacketRefNo;

        protected Uri gifUri;
        protected Uri mp3Uri;

        protected String openType;

        protected String transferType;

        protected String gifType;

        public void showMyDialog() {
            if (null == rlyRedPacketProgress) {
                return;
            }
            rlyRedPacketProgress.setVisibility(View.VISIBLE);
            AnimationDrawable animationDrawable = (AnimationDrawable) ivRedPacketLoading.getBackground();
            animationDrawable.start();
        }

        public void dimissMyDialog() {
            if (null == ivRedPacketLoading) {
                return;
            }
            AnimationDrawable animationDrawable = (AnimationDrawable) ivRedPacketLoading.getBackground();
            animationDrawable.stop();

            rlyRedPacketProgress.setVisibility(View.GONE);

        }


        public Builder(Context context) {
            this.context = context;
        }


        public View getLayout() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.dialog_red_packet_open, null);
            tvTransferBtnNext = layout.findViewById(R.id.tv_transfer_btn_next);
            rlyRedPacketContain = layout.findViewById(R.id.rly_red_packet_contain);
            tvDialogRedPacketTitle = layout.findViewById(R.id.tv_dialog_red_packet_title);
            tvDialogRedPacketBless = layout.findViewById(R.id.tv_dialog_red_packet_bless);
            tvDialogRedPacketTop = layout.findViewById(R.id.tv_dialog_red_packet_top);
            tvDialogRedPacketAmount = layout.findViewById(R.id.tv_dialog_red_packet_amount);
            llyAmountContain = layout.findViewById(R.id.lly_amount_contain);
            llyCommonContain = layout.findViewById(R.id.lly_common_contain);
            llyAllContain = layout.findViewById(R.id.lly_all_contain);
            tvDialogRedPacketTitleFlag = layout.findViewById(R.id.tv_dialog_red_packet_title_flag);
            llyRedPacketMainContain = layout.findViewById(R.id.lly_red_packet_main_contain);

            rlyRedPacketProgress = layout.findViewById(R.id.rly_red_packet_progress);
            ivRedPacketLoading = layout.findViewById(R.id.iv_red_packet_loading);

            tvDialogRedPacketAllNum = layout.findViewById(R.id.tv_dialog_red_packet_all_num);
            ivClose = layout.findViewById(R.id.iv_close);

            //设置控件等比宽高
            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) rlyRedPacketContain
                    .getLayoutParams();
            childTotalWidth = (int) (AndroidUtils.getScreenWidth(context) * 0.8);
            childTotalHeight = childTotalWidth * 485 / 300;

            //如果计算的高度加上下面按钮的高度比屏幕高度大，就从高来推算宽
            if ((childTotalHeight + AutoSizeUtils.dp2px(context, 80)) > AndroidUtils.getScreenHeight(context)) {
                childTotalHeight = (int) (AndroidUtils.getScreenHeight(context) * 0.6);
                childTotalWidth = childTotalWidth * 300 / 485;
            }
            params.width = childTotalWidth;
            params.height = childTotalHeight;


            initDate();
            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != closeClickListener) {
                        closeClickListener.onClick(v);
                    }
                }
            });

            tvTransferBtnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != positiveButtonClickListener) {
                        positiveButtonClickListener.onClick(tvTransferBtnNext, nextRedPacketRefNo, openType);
                    }
                }
            });
            return layout;
        }

        protected void initDate() {

        }

        ;


    }

    public interface ConFrimClick {
        void onClick(TextView v, String nextId, String openType);
    }


}

