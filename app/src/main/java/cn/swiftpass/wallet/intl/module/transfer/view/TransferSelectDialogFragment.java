package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.TransferPatternEntity;

/**
 * 转账 选择 fps/bocpay Fps ID Account NO
 */
public class TransferSelectDialogFragment extends DialogFragment {
    public static final int FPS_BOC = 0x0;
    public static final int RED_PACKET = 0x1;
    public static final int FPS_ID = 0x2;
    public static final int ACCOUNT_NO = 0x3;
    public static final int TYPE_CROSS = 0x4;
    @BindView(R.id.id_back_image)
    ImageView idBackImage;
    @BindView(R.id.lly_transfer_type_red_packet)
    LinearLayout llyTransferTypeRedPacket;
    @BindView(R.id.lly_transfer_type_fps_id)
    LinearLayout llyTransferTypeFpsId;
    @BindView(R.id.lly_transfer_type_account_no)
    LinearLayout llyTransferTypeAccountNo;
    @BindView(R.id.lly_transfer_type_cross)
    LinearLayout llyTransferTypeCross;
    @BindView(R.id.id_bac_view)
    LinearLayout idBacView;
    @BindView(R.id.lly_transfer_type_fps_boc)
    LinearLayout llyTransferTypeFpsBoc;
    private Unbinder bind;

    public void setOnTransferSelTypeClickListener(OnTransferSelTypeClickListener onTransferSelTypeClickListener) {
        this.onTransferSelTypeClickListener = onTransferSelTypeClickListener;
    }

    private OnTransferSelTypeClickListener onTransferSelTypeClickListener;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.dialogTheme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_transfer_select, container, false);
        bind = ButterKnife.bind(this, view);
        initItem();
        //selectCheck(0);
        return view;
    }

    private void initItem() {
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
        if (null == initBannerEntity) {
            return;
        }
        TransferPatternEntity transferPatternEntity = initBannerEntity.getTransferPattern();
        if (null == transferPatternEntity) {
            return;
        }

        showHideView(llyTransferTypeFpsBoc,transferPatternEntity.getFpsOrBocPay());
        showHideView(llyTransferTypeRedPacket,transferPatternEntity.getGiftActivity());
        showHideView(llyTransferTypeFpsId,transferPatternEntity.getFpsPayment());
        showHideView(llyTransferTypeAccountNo,transferPatternEntity.getBankAccountPayment());
        showHideView(llyTransferTypeCross,transferPatternEntity.getCrossBorderPayment());
    }

    /**
     * 显示和隐藏控件
     * @param llyView
     *
     * @param isSHow
     */
    private void showHideView(View llyView, boolean isSHow) {
        if (null == llyView) {
            return;
        }


        if (isSHow) {
            llyView.setVisibility(View.VISIBLE);
        }else{
            llyView.setVisibility(View.GONE);
        }
    }

    private void selectCheck(int position) {
        onTransferSelTypeClickListener.onTransferTypeSelListener(position);
        dismiss();

    }

    @OnClick({R.id.lly_transfer_type_fps_boc, R.id.lly_transfer_type_red_packet, R.id.lly_transfer_type_fps_id, R.id.lly_transfer_type_account_no,
            R.id.lly_transfer_type_cross, R.id.id_back_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lly_transfer_type_fps_boc:
                selectCheck(FPS_BOC);
                break;
            case R.id.lly_transfer_type_red_packet:
                selectCheck(RED_PACKET);
                break;
            case R.id.lly_transfer_type_fps_id:
                selectCheck(FPS_ID);
                break;
            case R.id.lly_transfer_type_account_no:
                selectCheck(ACCOUNT_NO);
                break;
            case R.id.lly_transfer_type_cross:
                selectCheck(TYPE_CROSS);
                break;
            case R.id.id_back_image:
                dismiss();
                break;

        }
    }


    public interface OnTransferSelTypeClickListener {
        void onBackBtnClickListener();

        void onTransferTypeSelListener(int positon);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setGravity(Gravity.BOTTOM);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (bind!=null){
            bind.unbind();
        }
    }
}
