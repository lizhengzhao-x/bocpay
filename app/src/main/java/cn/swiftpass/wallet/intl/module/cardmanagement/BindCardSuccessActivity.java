package cn.swiftpass.wallet.intl.module.cardmanagement;

import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 绑卡成功 /重设密码成功 复用界面
 */

public class BindCardSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.tv_card_info_nextPage)
    TextView tvCardInfoNextPage;
    @BindView(R.id.id_succcess_text)
    TextView tvSuccessText;
    @BindView(R.id.id_credit_register_info_title)
    TextView idCreditRegisterInfoTitle;
    @BindView(R.id.id_credit_register_info_desc)
    TextView idCreditRegisterInfoDesc;
    @BindView(R.id.id_center_view)
    TextView idCenterView;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        final int type = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (type == Constants.PAGE_FLOW_RESET_PASSWORD) {
            //重设密码
            setToolBarTitle(R.string.setting_payment_rgw);
            tvSuccessText.setText(getString(R.string.IDV_3a_21_2));
            idCreditRegisterInfoTitle.setVisibility(View.GONE);
            idCreditRegisterInfoDesc.setVisibility(View.GONE);
            idCenterView.setVisibility(View.GONE);
        } else if (type == Constants.DATA_CARD_TYPE_UNBIND_CARD) {
            //解绑
            setToolBarTitle(R.string.title_title_card);
            tvSuccessText.setText(getString(R.string.SMA2101_1_17));
            idCreditRegisterInfoTitle.setVisibility(View.GONE);
            idCreditRegisterInfoDesc.setVisibility(View.GONE);
            idCenterView.setVisibility(View.GONE);
        } else {
            //绑卡
            setToolBarTitle(R.string.title_bottom_card);
            tvSuccessText.setText(getString(R.string.string_bind_success));
            //是否是信用卡绑卡成功
            boolean isCredit = getIntent().getExtras().getBoolean(Constants.IS_CREDIT_CARD, false);
            if (!isCredit) {
                idCreditRegisterInfoTitle.setVisibility(View.GONE);
                idCreditRegisterInfoDesc.setVisibility(View.GONE);
                idCenterView.setVisibility(View.GONE);
            }
        }
        hideBackIcon();
        tvCardInfoNextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == Constants.PAGE_FLOW_RESET_PASSWORD) {
                    EventBus.getDefault().post(new PasswordEventEntity(PasswordEventEntity.EVENT_RESET_PWD_SUCCESS, ""));
                } else if (type == Constants.DATA_CARD_TYPE_UNBIND_CARD) {
                    EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_UNBIND_CARD_SUCCESS, ""));
                } else {
                    MyActivityManager.removeAllTaskForBindCard();
                    //TODO 这里就暂时理解 如果没有main界面 绑卡流程就是cc注册过来的
                    if (!MyActivityManager.containMainHomePage()) {
                        //如果是信用卡注册成功到达这个界面 main界面此时还没有生成
                        ActivitySkipUtil.startAnotherActivity(BindCardSuccessActivity.this, MainHomeActivity.class);
                    }else{
                        //这里有个问题，如果是cc注册成功接着绑定sa 成功之后应该要返回到首页，但是正常绑卡流程应该返回到卡列表
                    }
                    if (mHandler!=null){
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //这里有一个问题，appcappapp 主扫成功之后有一个AA收款的操作 如果用户点击AA收款并且绑定sa成功，我们需要清除appLink（在内存中）是在activity的onDestroy执行，但是这里onEvent先执行
                                //所以要做个简单的延迟避免清除迟 造成重新发起appcallapp流程
                                EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
                            }
                        },300);
                    }
                }
                finish();
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_bind_success;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            int type = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


}
