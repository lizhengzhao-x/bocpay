package cn.swiftpass.wallet.intl.module.transfer.entity;

import java.util.HashMap;
import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/11/6.
 * 1.通讯录点击收藏到最近联系人 本地只保留通讯录条目的id 为了区分手机号/邮箱 id = id_phoneNumber/id_emailAdress
 * 2.为了区分不同手机号登录的情况 本地存储的数组要以手机号为key value 就是数组
 */
public class LocalContactCacheEntity extends BaseEntity {


    public HashMap<String, List<String>> getmContacts() {
        return mContacts;
    }

    public void setmContacts(HashMap<String, List<String>> mContacts) {
        this.mContacts = mContacts;
    }

    private HashMap<String, List<String>> mContacts;

    public LocalContactCacheEntity() {
        this.mContacts = new HashMap<>();
    }


}
