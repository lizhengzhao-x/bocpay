package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.widget.ClearEditText;


public class UserNameSetDialog extends Dialog {

    private static String mInputText;

    public String getmAmount() {
        return mInputText;
    }


    public UserNameSetDialog(Context context) {
        super(context);
    }

    public UserNameSetDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String mOldText;
        private OnClickListener positiveButtonClickListener;

        public Builder(Context context, String oldText) {
            this.context = context;
            this.mOldText = oldText;
        }

        public Builder setPositiveListener(OnClickListener listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public UserNameSetDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final UserNameSetDialog dialog = new UserNameSetDialog(context, R.style.Dialog);
            View layout = inflater.inflate(R.layout.dialog_set_username_layout, null);
            final Button posBtn = layout.findViewById(R.id.positive_dl);
            Button negBtn = layout.findViewById(R.id.negative_dl);
            final ClearEditText amountET = layout.findViewById(R.id.et_amount);
            amountET.setText(mOldText);
            amountET.setSelection(mOldText.length());
            final TextView hintText = layout.findViewById(R.id.id_hint_text);
            InputFilter filter = new InputFilter() {
                @Override
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    if (source.toString().contains("\n")) {
                        return source.toString().replace("\n", "");
                    } else {
                        return null;
                    }
                }
            };

            amountET.setFocusable(true);
            amountET.setFocusableInTouchMode(true);
            amountET.requestFocus();


            amountET.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20), filter, inputFilter});
            negBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            if (TextUtils.isEmpty(mOldText)) {
                hintText.setText("0/20");
            } else {
                hintText.setText(mOldText.length() + "/20");
            }
            amountET.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String currentValueStr = amountET.getText().toString();
                    hintText.setText(currentValueStr.length() + "/20");
                }
            });

            posBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mInputText = amountET.getText().toString();
                    if (positiveButtonClickListener != null) {
                        positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                    }
                    dialog.dismiss();
                }
            });
            dialog.setContentView(layout);
            return dialog;
        }

        /**
         * 输入框禁止输入emoji表情  限制输入内容为数字 中文 字母和部分特殊字符  需要添加输入什么字符 就在正则里面添加
         */
        InputFilter inputFilter = new InputFilter() {
            Pattern pattern = Pattern.compile("[^a-zA-Z0-9\\u4E00-\\u9FA5_`~!@#$%^&*()\\-+=<>?:\"{}|,./;'\\[\\]·！￥…（）—《》？：“”【】、；‘’，。\\s]");

            @Override
            public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
                Matcher matcher = pattern.matcher(charSequence);
                if (!matcher.find()) {
                    return null;
                } else {
                    return "";
                }
            }
        };
    }
}

