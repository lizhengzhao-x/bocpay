package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class RegisterTaxResultEntity extends BaseEntity {

    ArrayList<TaxResidentEntity> taxResidentRegionList = new ArrayList<>();

    ArrayList<TaxInfoReason> noResidentReasonList = new ArrayList<>();

    public ArrayList<TaxResidentEntity> getTaxResidentRegionList() {
        return taxResidentRegionList;
    }

    public void setTaxResidentRegionList(ArrayList<TaxResidentEntity> taxResidentRegionList) {
        this.taxResidentRegionList = taxResidentRegionList;
    }

    public ArrayList<TaxInfoReason> getNoResidentReasonList() {
        return noResidentReasonList;
    }

    public void setNoResidentReasonList(ArrayList<TaxInfoReason> noResidentReasonList) {
        this.noResidentReasonList = noResidentReasonList;
    }
}
