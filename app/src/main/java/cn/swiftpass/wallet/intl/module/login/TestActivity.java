package cn.swiftpass.wallet.intl.module.login;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Process;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.widget.AppCompatEditText;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;


public class TestActivity extends BaseCompatActivity {


    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    String url = "http://mbasit1.ftcwifi.com/";

    @BindView(R.id.toggle_e2ee)
    ToggleButton toggleE2ee;
    @BindView(R.id.toggle_idv)
    ToggleButton toggleIdv;
    @BindView(R.id.iv_mbasit1)
    ImageView ivMbasit1;
    @BindView(R.id.ll_mbasit1)
    LinearLayout llMbasit1;
    @BindView(R.id.iv_ewauatf)
    ImageView ivEwaUatf;
    @BindView(R.id.ll_ewauatf)
    LinearLayout llEwaUatf;
    @BindView(R.id.iv_custom)
    ImageView ivCustom;
    @BindView(R.id.ed_custom)
    AppCompatEditText edCustom;
    @BindView(R.id.ll_custom)
    LinearLayout llCustom;
    @BindView(R.id.tv_now_url)
    TextView tvNowUrl;
    private boolean useCustom;

    @Override
    public void init() {
        String testUrl = SpUtils.getInstance().getTestUrl();
        boolean isE2ee = SpUtils.getInstance().getTestE2ee();
        boolean skipIdv = SpUtils.getInstance().getTestIdv();
        tvNowUrl.setText(testUrl);
        updateEnvironmentBtn(testUrl);

        toggleE2ee.setChecked(isE2ee);
        toggleIdv.setChecked(skipIdv);


        tvConfirm.setOnClickListener(v -> {

            if (useCustom && edCustom.getText() != null && !TextUtils.isEmpty(edCustom.getText().toString())) {
                SpUtils.getInstance().setTestUrl(edCustom.getText().toString());
                AppTestManager.getInstance().setBaseUrl(edCustom.getText().toString());
            } else {
                SpUtils.getInstance().setTestUrl(url);
                AppTestManager.getInstance().setBaseUrl(url);
            }
            SpUtils.getInstance().setTestE2ee(toggleE2ee.isChecked());
            SpUtils.getInstance().setTestIdv(toggleIdv.isChecked());
            AppTestManager.getInstance().setE2ee(toggleE2ee.isChecked());
            AppTestManager.getInstance().setSkipIdvFrp(toggleIdv.isChecked());

            // 获取启动的intent
            Intent intent = getPackageManager().getLaunchIntentForPackage(getPackageName());
            PendingIntent restartIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            // 设置杀死应用后2秒重启
            AlarmManager mgr = (AlarmManager) getSystemService(ALARM_SERVICE);
            mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, restartIntent);
            // 重启应用
            Process.killProcess(Process.myPid());

        });

    }

    private void updateEnvironmentBtn(String testUrl) {
        useCustom = false;
        ivMbasit1.setImageResource(R.mipmap.list_icon_choose_rectangle_default);
        ivEwaUatf.setImageResource(R.mipmap.list_icon_choose_rectangle_default);
        ivCustom.setImageResource(R.mipmap.list_icon_choose_rectangle_default);
        if (testUrl.contains("mbasit1")) {
            ivMbasit1.setImageResource(R.mipmap.list_icon_other_ticked_filled);
            return;
        }
        if (testUrl.contains("ewauatf")) {
            ivEwaUatf.setImageResource(R.mipmap.list_icon_other_ticked_filled);
            return;
        }

        if (!TextUtils.isEmpty(testUrl)) {
            ivCustom.setImageResource(R.mipmap.list_icon_other_ticked_filled);
            edCustom.setText(testUrl);
            useCustom = true;
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_test;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @OnClick({R.id.ll_mbasit1, R.id.ll_ewauatf, R.id.ll_custom})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_mbasit1:
                url = "http://mbasit1.ftcwifi.com/";
                updateEnvironmentBtn(url);
                break;
            case R.id.ll_ewauatf:
                url = "https://ewauatf.ftcwifi.com/";
                updateEnvironmentBtn(url);
                break;
            case R.id.ll_custom:
                url = "https://ewauatk.ftcwifi.com/";
                updateEnvironmentBtn(url);
                break;
            default:
                break;
        }
    }
}
