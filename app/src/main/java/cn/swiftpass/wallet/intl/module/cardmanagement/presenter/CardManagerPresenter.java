package cn.swiftpass.wallet.intl.module.cardmanagement.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.api.protocol.CardListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckSmartAccountBindNewProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetRegEddaInfoProtocol;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.MySmartAccountProtocol;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.CardManagerContract;


public class CardManagerPresenter extends BasePresenter<CardManagerContract.View> implements CardManagerContract.Presenter {

    private boolean isLoadingCardList=false;

    @Override
    public void queryCardList(String action, boolean isShowLoading) {
        if (isLoadingCardList){
            return;
        }
        if (getView() != null) {
            if (isShowLoading){
                getView().showDialogNotCancel();
            }
        }
        isLoadingCardList=true;
        new CardListProtocol(null, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                isLoadingCardList=false;
                if (getView() != null) {
                    if (isShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().queryCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CardsEntity response) {
                isLoadingCardList=false;
                if (getView() != null) {
                    if (isShowLoading) {
                        getView().dismissDialog();
                    }
                    getView().queryCardListSuccess(response,action);
                }
            }
        }).execute();

    }

    @Override
    public void getRegEddaInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetRegEddaInfoProtocol(new NetWorkCallbackListener<GetRegEddaInfoEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(GetRegEddaInfoEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getRegEddaInfoSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getSmartAccountInfo(boolean iShowLoading) {
        if (getView() != null&&iShowLoading) {
            getView().showDialogNotCancel();
        }
        new MySmartAccountProtocol(new MySmartAccountCallbackListener(new NetWorkCallbackListener<MySmartAccountEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (iShowLoading){
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MySmartAccountEntity response) {
                if (getView() != null) {
                    if (iShowLoading){
                        getView().dismissDialog();
                    }
                    getView().getSmartAccountInfoSuccess(response);
                }
            }
        })).execute();
    }


    @Override
    public void checkSmartAccountBind(boolean isShowLoading) {
        if (getView() != null&&isShowLoading) {
            getView().showDialogNotCancel();
        }
        new CheckSmartAccountBindNewProtocol(new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    if (isShowLoading){
                        getView().dismissDialog();
                    }
                    getView().checkSmartAccountBindError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    if (isShowLoading){
                        getView().dismissDialog();
                    }
                    getView().checkSmartAccountBindSuccess(response);
                }

            }
        }).execute();
    }


}
