package cn.swiftpass.wallet.intl.module.register.contract;


import android.content.Context;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeCheckEntity;
import cn.swiftpass.wallet.intl.entity.PARegisterVerifyCodeEntity;

/**
 * PA用户注册页面验证码功能 V、P层  关系定义
 */
public class PARegisterVerifyCodeContract {

    public interface View extends IView {
        void getPARegisterVerifyCodeSuccess(PARegisterVerifyCodeEntity response);

        void getPARegisterVerifyCodeFailed(String errorCode, String errorMsg);

        void checkPARegisterVerifyCodeSuccess(PARegisterVerifyCodeCheckEntity response);

        void checkPARegisterVerifyCodeFailed(String errorCode, String errorMsg);
    }

    public interface Presenter extends IPresenter<PARegisterVerifyCodeContract.View> {
        //获取验证码
        void getPARegisterVerifyCode(final Context context);

        //检查验证码
        void checkPARegisterVerifyCode(final Context context, String mobile, String verifyCode);
    }

}
