package cn.swiftpass.wallet.intl.module.register;

import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_REGISTER;
import static cn.swiftpass.wallet.intl.sdk.idv.IdvTransitionActivity.TRANSACTION_TYPE_RESET;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.idv.sdklibrary.util.IdvConstant;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.utils.PhoneUtils;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.CheckHKCustEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.IdvImageEntity;
import cn.swiftpass.wallet.intl.entity.OcrInfoEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.register.contract.IdAuthInfoFillContract;
import cn.swiftpass.wallet.intl.module.register.presenter.IdAuthInfoFillPresenter;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.sdk.frp.FrpConstants;
import cn.swiftpass.wallet.intl.sdk.frp.FrpTransitionActivity;
import cn.swiftpass.wallet.intl.sdk.idv.IdvConstants;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.utils.IdvSdkManager;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.ScaleImageDialog;


/**
 * 身份证信息认证页面 图片 信息
 * 1.注册流程/信用卡绑定s2流程/忘记密码流程 通用页面 根据DATA_CARD_TYPE来区分流程 然后显示不同的标题 不同的接口调用
 * 2.1)进入界面 先拉取ocrinfo接口--->2）然后去获取身份证证件 同事发起证件真伪检测(idv sdk) 3)以上两步都成功之后才可以进行人脸识别(FRP)
 * 3.人脸识别 吊起frpsdk界面 成功回调之后(根据code) 才可以进入下个界面 注意:需要处理按钮的多次点击引起的bug
 */
public class IdAuthInfoFillActivity extends BaseCompatActivity<IdAuthInfoFillContract.Presenter> implements IdAuthInfoFillContract.View {

    private static final int REQUEST_FRP_SDK = 300;
    private static final String TAG = "IdAuthInfoFillActivity";
    private ImageView mId_img_auth_image;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvChineseName;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvEnglishName;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvCardId;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvDateOfBirth;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvGender;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvNationality;
    private cn.swiftpass.wallet.intl.widget.ImageArrowTextView tvIssueDate;
    private TextView tvRegisterNext;
    private TextView tvRegisterModifyInfomation;
    private RelativeLayout rlImageView;
    private TextView tvRegisterPravite;

    private String mCountryCode = "852";
    private int mCurrentEventType;
    private String mTransactionUniqueID;
    private String mIdvSequnumber;
    private String mIdvFid;
    private int choiceAreaPosition;
    public static int CHOICE_AREA_CODE = 1001;

    //流程控制字段 A->B->C 接口前置跳过
    private boolean mSendOcrInfoSuccess;
    private boolean mCheckFRPCanRetrySuccess;
    private boolean mFrpRequestSuccess;
    private ArrayList<String> genderItems;
    private S2RegisterInfoEntity registerInfoEntity;
    private Bitmap mImageBitmap;
    private int mCurrentFrpErrorCnt;
    //这个是音频开关，checID成功的回调里返回
    private boolean voiceEnable;


    /**
     * 判断是否为sdk回调后跳转 如果是  则该条记录不埋点
     */
    private boolean isAnalysis = true;


    private Bundle bundle;

    private void bindViews() {
        tvRegisterPravite = (TextView) findViewById(R.id.tv_register_pravite);
        tvRegisterModifyInfomation = (TextView) findViewById(R.id.id_register_modify_infomation);
        mId_img_auth_image = (ImageView) findViewById(R.id.id_img_auth_image);
        tvChineseName = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_chinese_name);
        tvEnglishName = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_english_name);
        tvCardId = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_id_card);
        tvDateOfBirth = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_date_of_birth);
        tvGender = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_gender);
        tvNationality = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_nationality);
        tvIssueDate = (cn.swiftpass.wallet.intl.widget.ImageArrowTextView) findViewById(R.id.id_issueDate);
        tvRegisterNext = (TextView) findViewById(R.id.id_register_next);
        rlImageView = (RelativeLayout) findViewById(R.id.id_image_view);
        tvRegisterNext.setOnClickListener(onClickListener);
        tvGender.setOnClickListener(onClickListener);
        tvRegisterModifyInfomation.setOnClickListener(onClickListener);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        hideBackIcon();
        setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);

        //默认进来不可点击 必须checkid完成之后 才可以点击
        changeNextBtnStatus(false);


        getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                AndroidUtils.showConfrimExitDialog(IdAuthInfoFillActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                    @Override
                    public void onConfirmBtnClick() {
                        finish();
                    }
                });
            }
        });
    }

    private void changeNextBtnStatus(boolean enabled) {

        tvRegisterNext.setEnabled(enabled);
        tvRegisterModifyInfomation.setEnabled(enabled);
    }

    @Override
    protected void onStart() {
        isAnalysis = true;
        super.onStart();
    }


    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ButtonUtils.isFastDoubleClick(v.getId())) {
                return;
            }
            switch (v.getId()) {
                case R.id.id_register_next:
                    if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                        //s2用户忘记密码
                        if (mSendOcrInfoSuccess) {
                            mPresenter.checkFPRUsedCount(RequestParams.FORGET_PWD);
                        } else {
                            submitOcrInfo(RequestParams.FORGET_PWD);
                        }
                    } else if (mCurrentEventType == Constants.PAGE_FLOW_REGISTER_PA) {
                        //s2用户注册
                        if (mSendOcrInfoSuccess) {
                            mPresenter.checkFPRUsedCount(RequestParams.REGISTER_STWO_ACCOUNT);
                        } else {
                            submitOcrInfo(RequestParams.REGISTER_STWO_ACCOUNT);
                        }
                    } else if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
                        //信用卡用户绑定s2
                        if (mSendOcrInfoSuccess) {
                            if (mCheckFRPCanRetrySuccess) {
                                if (mFrpRequestSuccess) {
                                    mPresenter.checkHKCust(RequestParams.BIND_SMART_ACCOUNT);
                                } else {
                                    goFPRSdkAuth();
                                }
                            } else {
                                mPresenter.checkFPRUsedCount(RequestParams.BIND_SMART_ACCOUNT);
                            }
                        } else {
                            submitOcrInfo(RequestParams.BIND_SMART_ACCOUNT);
                        }
                    }
                    break;
                case R.id.id_register_modify_infomation:
                    finish();
                    break;
                default:
                    break;
            }
        }
    };


    @Override
    protected IdAuthInfoFillContract.Presenter createPresenter() {
        return new IdAuthInfoFillPresenter();
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_A1_8_1);
        bindViews();
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        bundle = getIntent().getExtras();
        mCurrentEventType = bundle.getInt(Constants.CURRENT_PAGE_FLOW);
        mTransactionUniqueID = bundle.getString(Constants.TRANSACTIONUNIQUEID);
        if (TextUtils.isEmpty(mTransactionUniqueID)) {
            finish();
        }
        mIdvFid = bundle.getString(Constants.MIDVSEQFID);
        mIdvSequnumber = bundle.getString(Constants.MIDVSEQNUMBER);
        tvChineseName.setRightImageShow(false);
        tvEnglishName.setRightImageShow(false);
        tvCardId.setRightImageShow(false);
        tvDateOfBirth.setRightImageShow(false);
        tvIssueDate.setRightImageShow(false);
        tvGender.setRightImageShow(false);
        tvNationality.setRightImageShow(true);

        tvNationality.setRightTextTitle(getString(R.string.P3_ewa_01_009));
        tvNationality.getmEditRight().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choiceArea();
            }
        });
        tvNationality.getImageRight().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                choiceArea();
            }
        });


        genderItems = new ArrayList<>();
        genderItems.add(getString(R.string.P3_A1_18b_5_2));
        genderItems.add(getString(R.string.P3_A1_18b_5_1));
        mCountryCode = Constants.DATA_COUNTRY_CODE_REGISTER[0];
        choiceAreaPosition = 1;
        getOcrInfoRequest(mIdvFid, mIdvSequnumber);
        showIdCardImage();
        initTextMsg();
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvRegisterModifyInfomation.setEnabled(true);
        tvRegisterNext.setEnabled(true);
    }

    private void initTextMsg() {
        final String totalStr = getResources().getString(R.string.P3_A1_8_7_0_2_a);
        final int startIndex = totalStr.indexOf("%%");
        SpannableString spannableString = new SpannableString(totalStr.replace("%%", ""));
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                String lan = SpUtils.getInstance(IdAuthInfoFillActivity.this).getAppLanguage();
                String title = totalStr.substring(startIndex + 2, totalStr.length());
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_SC);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_TC);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.PRIVACYPOLICY_EN);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, title);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, startIndex, spannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), startIndex, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        tvRegisterPravite.setText(spannableString);
        tvRegisterPravite.setMovementMethod(LinkMovementMethod.getInstance());


    }

    /**
     * 通过查询证件信息
     *
     * @param mIdvFID4
     * @param mIdvSeqNum4
     */
    private void getOcrInfoRequest(final String mIdvFID4, final String mIdvSeqNum4) {
        if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
            mPresenter.getOcrInfo(RequestParams.BIND_SMART_ACCOUNT, mIdvSeqNum4, mIdvFID4);
        } else if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            mPresenter.getOcrInfo(RequestParams.FORGET_PWD, mIdvSeqNum4, mIdvFID4);
        } else {
            mPresenter.getOcrInfo(RequestParams.REGISTER_STWO_ACCOUNT, mIdvSeqNum4, mIdvFID4);
        }
    }

    /**
     * 更新身份证信息
     *
     * @param ocrInfoEntity
     */
    private void initOcrInfo(OcrInfoEntity ocrInfoEntity) {
        if (ocrInfoEntity != null) {
            tvChineseName.setRightTextTitle(ocrInfoEntity.getChinessName());
            tvEnglishName.setRightTextTitle(ocrInfoEntity.getEnglishName());
            tvCardId.setRightTextTitle(ocrInfoEntity.getCertNo());
            tvDateOfBirth.setRightTextTitle(ocrInfoEntity.getBirthday());
            tvIssueDate.setRightTextTitle(ocrInfoEntity.getIssueDate());
            String value = "";
            if (!TextUtils.isEmpty(ocrInfoEntity.getSex())) {
                value = ocrInfoEntity.getSex().equals("0") ? getString(R.string.P3_A1_18b_5_2) : getString(R.string.P3_A1_18b_5_1);
            }
            tvGender.setRightTextTitle(value);
        }
        //TODO --- zhaolizheng 跳过idv
        //if (!BuildConfig.SKIP_IDV)
        if (!AppTestManager.getInstance().isSkipIdvFrp()) {
            checkIdRequest();
        }
        long mCheckIdStartTime = System.currentTimeMillis();
        TempSaveHelper.setCheckIdStartTime(mCheckIdStartTime);
    }


    /**
     * 显示身份证正面照
     */
    private void showIdCardImage() {
        if (BuildConfig.SKIP_IDV) {
            return;
        }
        if (!TextUtils.isEmpty(mIdvSequnumber) && !TextUtils.isEmpty(mIdvFid)) {
            mPresenter.downloadCardImage(mIdvSequnumber, mIdvFid);
        }
    }

    @Override
    public void getCardImageSuccess(IdvImageEntity response) {
        mImageBitmap = AndroidUtils.stringtoBitmap(response.getFiles());
        mId_img_auth_image.setImageBitmap(mImageBitmap);
        rlImageView.setBackgroundColor(Color.TRANSPARENT);
        //图片显示成功之后才可以点击
        mId_img_auth_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showScaleImageDialog();
            }
        });
    }

    @Override
    public void getCardImageFail(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
    }


    /**
     * sdk 通知 idvserver 开始证件真伪检测
     */
    private void checkIdRequest() {
        LogUtils.e(TAG, "mTransactionUniqueID:" + mTransactionUniqueID);
        showDialogNotCancel(this);
        String language = AndroidUtils.getIdvLanguage(this);
        if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            IdvSdkManager.getInstance().getIntegrateIdvApi().checkID(getPackageName(), mTransactionUniqueID, TRANSACTION_TYPE_RESET, "EWA-HK", "A", language, "NM", "12313131313", "1.3", this);
        } else {
            IdvSdkManager.getInstance().getIntegrateIdvApi().checkID(getPackageName(), mTransactionUniqueID, TRANSACTION_TYPE_REGISTER, "EWA-HK", "A", language, "NM", "12313131313", "1.3", this);
        }
    }

    /**
     * 选择地区
     */
    private void choiceArea() {
        Intent intent = new Intent(IdAuthInfoFillActivity.this, AreaSelectionActivity.class);
        intent.putExtra(Constants.CHOICE_AREA_POSITION, choiceAreaPosition);
        startActivityForResult(intent, CHOICE_AREA_CODE);
    }


    /**
     * 图片放大缩小
     */
    private void showScaleImageDialog() {
        ScaleImageDialog scaleImageDialog = new ScaleImageDialog(mContext);
        scaleImageDialog.setImage(mImageBitmap);
        scaleImageDialog.show();

    }

    /**
     * 注册 绑卡 提交 ocr 信息
     *
     * @param type
     */
    private void submitOcrInfo(final String type) {
        String chineseName = tvChineseName.getRightText();
        String englishName = tvEnglishName.getRightText();
        String idCard = tvCardId.getRightText();
        String dateOfBirth = tvDateOfBirth.getRightText();
        String gender = "";
        String genderStr = tvGender.getRightText();
        if (!TextUtils.isEmpty(genderStr)) {
            gender = genderStr.equals(getString(R.string.P3_A1_18b_5_2)) ? "0" : "1";
        }
        S2RegisterInfoEntity infoEntity = new S2RegisterInfoEntity();
        String phone = "";

        if (RequestParams.REGISTER_STWO_ACCOUNT.equals(type) || RequestParams.BIND_SMART_ACCOUNT.equals(type)) {
            try {
                phone = getIntent().getStringExtra(Constants.EXTRA_COUNTRY_CODE).replace("+", "") + "-" + getIntent().getStringExtra(Constants.DATA_PHONE_NUMBER);
                if (!TextUtils.isEmpty(phone)) {
                    infoEntity.setPhone(phone.replace(" ", "").replace("+", ""));
                }
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            infoEntity.setCountryName(tvNationality.getRightText());
            infoEntity.setNationalName(tvNationality.getRightText());

        } else if (RequestParams.FORGET_PWD.equals(type)) {

            //未登录态 需要传手机号码 登录态不需要传手机号码
            if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.MOBILE_PHONE))) {
                infoEntity.setPhone(getIntent().getExtras().getString(Constants.MOBILE_PHONE));
            }

        }

        infoEntity.setIssueDate(tvIssueDate.getRightText());
        infoEntity.setBirthday(dateOfBirth);
        infoEntity.setCertNo(idCard);
        infoEntity.setCertType("NM");
        infoEntity.setChinessName(chineseName);
        infoEntity.setEnglishName(englishName);
        infoEntity.setNationalCode(mCountryCode);
        infoEntity.setSex(gender);
        registerInfoEntity = infoEntity;
        mPresenter.submitRegisterData(type, infoEntity);
    }

    @Override
    public void submitDataSuccess(ContentEntity response, String action) {
        mSendOcrInfoSuccess = true;
        if (RequestParams.BIND_SMART_ACCOUNT.equals(action)) {
            mPresenter.checkFPRUsedCount(RequestParams.BIND_SMART_ACCOUNT);
        } else if (RequestParams.REGISTER_STWO_ACCOUNT.equals(action)) {
            mPresenter.checkFPRUsedCount(RequestParams.REGISTER_STWO_ACCOUNT);
        } else {
            //RequestParams.FORGET_PWD
            mPresenter.checkFPRUsedCount(RequestParams.FORGET_PWD);
        }
    }

    @Override
    public void submitDataFail(String action, String errorCode, String errorMsg) {

        if (RequestParams.FORGET_PWD.equals(action)) {
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    if (TextUtils.equals(ErrorCode.LOGIN_NEW_DEVICE_FORGETPWD.code, errorCode)) {
                        AndroidUtils.clearMemoryCacheOnly();
                        ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    }
                }
            });
        } else {
            sendErrorEvent(errorCode, errorMsg);
            AndroidUtils.showTipDialog(IdAuthInfoFillActivity.this, errorMsg);
        }

    }


    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA || mCurrentEventType == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.PHOTO_DETAILS_CONFIRM);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    @Override
    public boolean isAnalysisPage() {
        if (mCurrentEventType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
            if (isAnalysis) {
                return true;
            }
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.BACK_PHOTO_CONFIRM;
        analysisPageEntity.current_address = PagerConstant.PHOTO_DETAILS_CONFIRM;
        return analysisPageEntity;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mImageBitmap != null) {
            mImageBitmap.recycle();
            mImageBitmap = null;
        }
    }


    @Override
    public void checkFPRUsedFailed(String action, String errorCode, String errorMsg) {
        if (RequestParams.FORGET_PWD.equals(action)) {
            AndroidUtils.showTipDialog(IdAuthInfoFillActivity.this, errorMsg);
        } else if (RequestParams.BIND_SMART_ACCOUNT.equals(action)) {
            sendErrorEvent(errorCode, errorMsg);
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        } else if (RequestParams.REGISTER_STWO_ACCOUNT.equals(action)) {
            sendErrorEvent(errorCode, errorMsg);
            // 信息异常 需要允许用户重新输入（真实情况不存在）
            mSendOcrInfoSuccess = false;
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        }
    }

    @Override
    public void checkFPRUsedSuccess(String action, ContentEntity response) {
        mCheckFRPCanRetrySuccess = true;
        if (RequestParams.FORGET_PWD.equals(action)) {
            goFPRSdkAuth();
        } else if (RequestParams.BIND_SMART_ACCOUNT.equals(action)) {
            goFPRSdkAuth();
        } else if (RequestParams.REGISTER_STWO_ACCOUNT.equals(action)) {
            goFPRSdkAuth();
        }

    }


    /**
     * 调用FRP sdk
     */
    private void goFPRSdkAuth() {

        DataCollectManager.getInstance().sendFrpBefore();

        String transactionType = null;
        if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            transactionType = TRANSACTION_TYPE_RESET;
        } else {
            transactionType = TRANSACTION_TYPE_REGISTER;
        }

        FrpTransitionActivity.startActivityForResult(
                IdAuthInfoFillActivity.this,
                transactionType,
                mTransactionUniqueID,
                AndroidUtils.getIdvLanguage(IdAuthInfoFillActivity.this),
                PhoneUtils.getDeviceId() + "_" + BuildConfig.VERSION_NAME,
                TempSaveHelper.getRegisterRecordId(),
                BuildConfig.VERSION_NAME,
                bundle.getString(Constants.MIDVSEQNUMBER),
                bundle.getString(Constants.MIDVSEQFID),
                voiceEnable,
                REQUEST_FRP_SDK
        );
    }


    @Override
    public void checkHKCustSuccess(String action, CheckHKCustEntity entity) {
        isAnalysis = false;
        if (RequestParams.REGISTER_STWO_ACCOUNT.equals(action) || RequestParams.BIND_SMART_ACCOUNT.equals(action)) {
            TempSaveHelper.setIsCheckIdSuccess(false);
            String phone = "";
            try {
                phone = getIntent().getStringExtra(Constants.EXTRA_COUNTRY_CODE) + "-" + getIntent().getStringExtra(Constants.DATA_PHONE_NUMBER);
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            registerInfoEntity.setPhone(phone);
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.SHOWOTHERINFO, true);
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, bundle.getInt(Constants.CURRENT_PAGE_FLOW));
            mHashMaps.put(Constants.MOBILE_PHONE, phone);
            mHashMaps.put(Constants.OCR_INFO, registerInfoEntity);
            ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, STwoRegisterIdAuthInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (RequestParams.FORGET_PWD.equals(action)) {
            mPresenter.forgetPwdCustomerInfo();
        }
    }

    @Override
    public void checkHKCustFail(String action, String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        mSendOcrInfoSuccess = false;
        showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (RequestParams.REGISTER_STWO_ACCOUNT.equals(action)) {
                    MyActivityManager.removeAllTaskWithRegister();
                } else if (RequestParams.BIND_SMART_ACCOUNT.equals(action)) {
                    MyActivityManager.removeAllTaskWithPaBindCard();
                } else { // RequestParams.FORGET_PWD
                    if (CacheManagerInstance.getInstance().isLogin()) {
                        //登录态忘记密码
                        MyActivityManager.removeAllTaskForForgetPwdWithLoginStatusMainStack();
                    } else {
                        //登出态忘记密码
                        MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                    }
                }
            }
        });
    }

    @Override
    public void forgetPwdCustomerInfoSuccess(IdAuthIinfoEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentEventType);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, response.getMobile());
        //手机号传过去
        ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    @Override
    public void forgetPwdCustomerInfoFail(String errorCode, String errorMsg) {
        if (errorCode.equals(ErrorCode.CHECK_IDV_FAILED.code)) {
            //退出流程
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentEventType);
            ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, CheckIdvFailedActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (errorCode.equals(ErrorCode.CONTENT_TIME_OUT.code) || errorCode.equals(ErrorCode.CHECK_ID_RETRY.code)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentEventType);
            if (getIntent() != null && getIntent().getExtras() != null && !TextUtils.isEmpty(getIntent().getExtras().getString(Constants.MOBILE_PHONE))) {
                mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
            }
            ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, CheckIdvProgressActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                }
            });
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fill_authinfo;
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) return;

        if (requestCode == CHOICE_AREA_CODE && resultCode == CHOICE_AREA_CODE) {
            String result = data.getStringExtra(Constants.CHOICE_AREA);
            if (!TextUtils.isEmpty(result)) {
                tvNationality.setRightTextTitle(result);
                if (result.equals(getString(R.string.P3_ewa_01_009))) {
                    mCountryCode = Constants.DATA_COUNTRY_CODE_REGISTER[0];
                } else if (result.equals(getString(R.string.P3_ewa_01_010))) {
                    mCountryCode = Constants.DATA_COUNTRY_CODE_REGISTER[1];
                } else {
                    mCountryCode = Constants.DATA_COUNTRY_CODE_REGISTER[2];
                }
            }
            choiceAreaPosition = data.getIntExtra(Constants.CHOICE_AREA_POSITION, 0);
            return;
        }
        if (resultCode == RESULT_OK) {
            //防止从sdk出来一瞬间 用户多次点击相应（activity还没有完全跳转过去）
            tvRegisterModifyInfomation.setEnabled(false);
            tvRegisterNext.setEnabled(false);
            //frp sdk 请求成功
            if (requestCode == REQUEST_FRP_SDK) {
                DataCollectManager.getInstance().sendFrpBackEwa();
                mFrpRequestSuccess = true;
                if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                    mPresenter.checkHKCust(RequestParams.FORGET_PWD);
                } else if (mCurrentEventType == Constants.PAGE_FLOW_REGISTER_PA) {
                    mPresenter.checkHKCust(RequestParams.REGISTER_STWO_ACCOUNT);
                } else if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
                    mPresenter.checkHKCust(RequestParams.BIND_SMART_ACCOUNT);
                }
            }
        } else {
            if (requestCode == IdvConstant.CHECK_ID_REQ_CODE) {
                voiceEnable = data.getBooleanExtra(Constants.VOICEENABLE, false);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (resultCode == IdvConstant.CHECK_ID_RESULT_OK) {
                            dismissDialog();
                            changeNextBtnStatus(true);
                        } else {
                            //check id 失败 弹出提示
                            final String errorCode = data.getStringExtra(IdvConstants.IDV_ERROR_CODE);
                            final String errorMsg = data.getStringExtra(IdvConstants.IDV_ERROR_MSG);
                            sendErrorEvent(errorCode, errorMsg);
                            IdAuthInfoFillActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    dismissDialog();
                                    String message = AndroidUtils.getIdvErrorMessage(errorCode, IdAuthInfoFillActivity.this);
                                    showErrorMsgDialog(IdAuthInfoFillActivity.this, message, new OnMsgClickCallBack() {
                                        @Override
                                        public void onBtnClickListener() {
                                            finish();
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            } else if (requestCode == REQUEST_FRP_SDK) {
                //TODO --- zhaolizheng 跳过idv
                //if (BuildConfig.SKIP_IDV)
                if (AppTestManager.getInstance().isSkipIdvFrp()) {
                    //防止从sdk出来一瞬间 用户多次点击相应（activity还没有完全跳转过去）
                    tvRegisterModifyInfomation.setEnabled(false);
                    tvRegisterNext.setEnabled(false);
                    //frp sdk 请求成功
                    if (requestCode == REQUEST_FRP_SDK) {
                        mFrpRequestSuccess = true;
                        if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                            mPresenter.checkHKCust(RequestParams.FORGET_PWD);
                        } else if (mCurrentEventType == Constants.PAGE_FLOW_REGISTER_PA) {
                            mPresenter.checkHKCust(RequestParams.REGISTER_STWO_ACCOUNT);
                        } else if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
                            mPresenter.checkHKCust(RequestParams.BIND_SMART_ACCOUNT);
                        }
                    }
                } else {
                    //FRP sdk 异常
                    final String errorCode = data.getStringExtra(FrpConstants.FRP_ERROR_CODE);
                    String errorMsg = AndroidUtils.getIdvErrorMessage(errorCode, IdAuthInfoFillActivity.this);
                    LogUtils.i(TAG, "errorCode:" + errorCode + " errorMsg:" + errorMsg);

                    //sdk报错上传埋点
                    sendErrorEvent(errorCode, errorMsg);

                    if ("PJ008".equals(errorCode)) {
                        //FRP重试错误三次 要退出整个流程
                        mCurrentFrpErrorCnt++;
                        if (mCurrentFrpErrorCnt >= 3) {
                            showErrorMsgDialog(this, getString(R.string.PJ501), new OnMsgClickCallBack() {
                                @Override
                                public void onBtnClickListener() {
                                    //退出
                                    finish();
                                }
                            });
                        } else {
                            goFPRSdkAuth();
                        }
                    } else if ("PJ009".equals(errorCode)) {
                        //frp流程中 右上角X号退出流程
                        if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                            //忘记密码 绑卡 退到选择界面
                            if (CacheManagerInstance.getInstance().isLogin()) {
                                //登录态忘记密码
                                MyActivityManager.removeAllTaskForForgetPwdWithLoginStatusMainStack();
                            } else {
                                //登出态忘记密码
                                MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                            }
                        } else if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
                            MyActivityManager.removeAllTaskWithPaBindCard();
                        } else {
                            //退出
                            finish();
                        }

                    }
                }

            }
        }
    }


    @Override
    public void getORCFailed(String errorCode, String errorMsg) {
        if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
            sendErrorEvent(errorCode, errorMsg);
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        } else if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    if (TextUtils.equals(ErrorCode.LOGIN_NEW_DEVICE_FORGETPWD.code, errorCode)) {
                        AndroidUtils.clearMemoryCacheOnly();
                        ActivitySkipUtil.startAnotherActivity(IdAuthInfoFillActivity.this, LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    } else {
                        finish();
                    }
                }
            });

        } else {
            sendErrorEvent(errorCode, errorMsg);
            showErrorMsgDialog(IdAuthInfoFillActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        }

    }


    @Override
    public void getORCSuccess(OcrInfoEntity response) {
        if (mCurrentEventType == Constants.PAGE_FLOW_BIND_PA) {
            initOcrInfo(response);
            showIdCardImage();
        } else if (mCurrentEventType == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            initOcrInfo(response);
            showIdCardImage();
        } else {
            DataCollectManager.getInstance().sendOcrConfirm();
            initOcrInfo(response);

        }

    }


}
