package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.entity.ApplyVirUrlEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CircleWebview;

import static cn.swiftpass.wallet.intl.module.setting.WebViewActivity.NEW_MSG;
import static cn.swiftpass.wallet.intl.module.setting.WebViewActivity.SHARE_CONTENT;

/**
 * Created by ZhangXinchao on 2019/11/13.
 * 虚拟卡引导申请
 */
public class VirtualCardApplyActivity extends BaseWebViewActivity {

    /**
     * 是否为首页最新消息 如果是 展示分享按钮
     */
    private boolean isNewMsg;
    @BindView(R.id.webView)
    CircleWebview webView;
    @BindView(R.id.id_apply_now)
    ImageView imgApplyNow;
    private String mLanguage;
    private String shareContent;

    public static void startActivity(Activity from) {
        ActivitySkipUtil.startAnotherActivity(from, VirtualCardApplyActivity.class);
    }

    @Override
    public void init() {
        super.init();

//        if (getIntent() != null && !TextUtils.isEmpty(getIntent().getStringExtra(Constants.DETAIL_TITLE))) {
//            setToolBarTitle(getIntent().getStringExtra(Constants.DETAIL_TITLE));
//        } else {
//            setToolBarTitle(R.string.VC01_03_1);
//        }

        setToolBarTitle(R.string.VC01_03_1);

//        mLanguage = SpUtils.getInstance(mContext).getAppLanguage();
        if (getIntent() == null) {
            finish();
        }
        isNewMsg = getIntent().getBooleanExtra(NEW_MSG, true);
        shareContent = getIntent().getStringExtra(SHARE_CONTENT);
        String url = null;
        if (getIntent().getExtras() != null) {
            url = getIntent().getStringExtra(Constants.DETAIL_URL);
        }
        if (TextUtils.isEmpty(url)){
            finish();
            return;
        }
        if (isNewMsg) {
            if (!TextUtils.isEmpty(shareContent)) {
                //图片地址不为空，才需要显示分享按钮
                ImageView shareIv = addToolBarRightViewToImage(R.mipmap.icon_share);
                shareIv.setOnClickListener(new OnProhibitFastClickListener() {
                    @Override
                    public void onFilterClick(View v) {
                        shareImageUrl(shareContent);
                    }
                });
            }
        }

        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        if (TextUtils.isEmpty(url)) {
            finish();
        }
        webView.setBackgroundColor(Color.parseColor("#4B63AD"));
        webView.setNoButtomRadius();
        setWebViewClient(webView, false);
        initWebView(webView, url);
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            imgApplyNow.setImageResource(R.mipmap.icon_applynow_zh);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            imgApplyNow.setImageResource(R.mipmap.icon_applynow_sc);
        } else {
            imgApplyNow.setImageResource(R.mipmap.icon_applynow_en);
        }

        imgApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ButtonUtils.isFastDoubleClick(view.getId(),500)){
                    imgApplyNow.setEnabled(false);
                    getVirtualCardUrl();
                }
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_vitualcard_apply;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    private void getVirtualCardUrl() {
        ApiProtocolImplManager.getInstance().getVirtualCardUrl(getActivity(), new NetWorkCallbackListener<ApplyVirUrlEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                imgApplyNow.setEnabled(true);
                showErrorMsgDialog(VirtualCardApplyActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(ApplyVirUrlEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, response.getRefUrl());
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.VC01_03_1 ));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imgApplyNow.setEnabled(true);
                    }
                },200);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (webView != null) {
            webView.destroy();
            webView = null;
        }
    }
}
