package cn.swiftpass.wallet.intl.module.crossbordertransfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.RateInfoEntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderDetailEntity;


public class TransferCrossBorderAmountContract {

    public interface View extends IView {

        void commitTransferUseDataSuccess(TransferCrossBorderDetailEntity response);

        void commitTransferUseDataError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(String action, MySmartAccountEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void queryRateInfoSuccess(RateInfoEntity response);

        void queryRateInfoError(String errorCode, String errorMsg);



    }


    public interface Presenter extends IPresenter<View> {

        void commitTransferUseData(
                String receiverName,
                String cardNo,
                String receiverbankName,
                String amountHKD,
                String amountCNY,
                String forUsed,
                String sendCardId,
                String cardType,
                String refNo,
                String lastUpdatedRate,
                String HKD2CNYRate,
                String isTransferToSelf
        );

        void getSmartAccountInfo(String action);

        void queryRateInfo();



    }

}
