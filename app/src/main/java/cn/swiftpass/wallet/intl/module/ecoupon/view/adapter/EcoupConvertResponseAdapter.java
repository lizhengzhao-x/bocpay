package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * 积分兑换记录
 */
public class EcoupConvertResponseAdapter implements AdapterItem<RedeemResponseEntity.ResultRedmBean> {
    private Context context;

    public EcoupConvertResponseAdapter(Context context) {
        this.context = context;
    }

    private TextView mId_title;
    private TextView mId_detail;


    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoup_res_detail;
    }

    @Override
    public void bindViews(View root) {
        mId_title = (TextView) root.findViewById(R.id.id_title);
        mId_detail = (TextView) root.findViewById(R.id.id_detail);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(RedeemResponseEntity.ResultRedmBean resultRedmBean, int position) {
        mPosition = position;
        mId_title.setText(resultRedmBean.getAswItemCode());
        mId_detail.setText(resultRedmBean.getEVoucherQty());
    }
}
