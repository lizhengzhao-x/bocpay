package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.BackgroundUrlEntity;

public class GetBackgroundProtocol extends BaseProtocol {

    public GetBackgroundProtocol(NetWorkCallbackListener<BackgroundUrlEntity> dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/preLogin/getBackgroundImage";
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }
}
