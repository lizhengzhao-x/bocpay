package cn.swiftpass.wallet.intl.module.login.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginVerifyCodeEntity;
import cn.swiftpass.wallet.intl.entity.ForgetPasswordEntity;
import cn.swiftpass.wallet.intl.entity.LoginCheckEntity;
import cn.swiftpass.wallet.intl.entity.SystemPagerDataEntity;

public class LoginContract {

    public interface View extends IView {
//        void OnCheckAccountSuccess(LoginCheckEntity response, String phoneNumber);
//
//        void OnCheckAccountError(String errorCode, String errorMsg);

        void getInitDataFail(String errorCode, String errorMsg);

        void getInitDataSuccess(SystemPagerDataEntity response, String action);


        void getVerifyCodeFail(String errorCode, String errorMsg);

        void getVerifyCodeSuccess(BankLoginVerifyCodeEntity codeEntity);

        void verifyCodeFail(String errorCode, String errorMsg);

        void verifyCodeSuccess();

        void verifyPwdFail(String errorCode, String errorMsg);

        void verifyPwdSuccess(LoginCheckEntity loginCheckEntity);

        void forgetPwdDeviceStatusFail(String errorCode, String errorMsg);

        void forgetPwdDeviceStatusSuccess(ForgetPasswordEntity response);
    }

    public interface Presenter extends IPresenter<View> {

//        void checkAccount(String mobile, String action);

        void getInitData(String action);

        void getVerifyCode();

        void verifyCode(String code);

        void verifyPwd(String passcode,String mobile);

        void forgetPwdDeviceStatus();
    }
}
