package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;


import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class GradeListSelAdapter implements AdapterItem<SmaGpInfoBean> {
    private Context context;

    public void setOnItemSelListener(OnItemSelListener onItemSelListener) {
        this.onItemSelListener = onItemSelListener;
    }

    private OnItemSelListener onItemSelListener;
    private boolean isShowSel;

    public GradeListSelAdapter(Context context, OnItemSelListener onItemSelListenerIn, boolean show) {
        this.context = context;
        this.onItemSelListener = onItemSelListenerIn;
        this.isShowSel = show;
    }


    private LinearLayout mId_root_grade_view;
    private ImageView mId_sel_check;
    private TextView mId_card_title;
    private TextView mId_card_grade;
    private TextView mId_card_number;
    private TextView mId_card_expiredata;
    private View id_total_view;


    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_sel_grade_list;
    }

    @Override
    public void bindViews(View root) {
        mId_card_title = (TextView) root.findViewById(R.id.id_card_title);
        mId_card_grade = (TextView) root.findViewById(R.id.id_card_grade);
        mId_card_number = (TextView) root.findViewById(R.id.id_card_number);
        mId_card_expiredata = (TextView) root.findViewById(R.id.id_card_expiredata);
        mId_root_grade_view = (LinearLayout) root.findViewById(R.id.id_root_grade_view);
        mId_sel_check = (ImageView) root.findViewById(R.id.id_sel_check);
        id_total_view = root.findViewById(R.id.id_total_view);

    }


    @Override
    public void setViews() {
        id_total_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemSelListener.onItemChecked(mPosition);
            }
        });
    }

    @Override
    public void handleData(SmaGpInfoBean homeMessageEntity, int position) {
        mPosition = position;
        mId_card_title.setText(homeMessageEntity.getName());
        if (!TextUtils.isEmpty(homeMessageEntity.getBal())) {
            mId_card_grade.setText(AndroidUtils.formatPrice(Double.valueOf(homeMessageEntity.getBal()), false));
        }
        mId_card_number.setText(homeMessageEntity.getPan());
        mId_card_expiredata.setText(context.getString(R.string.EC04_1_a) + homeMessageEntity.getExpiryDate());
        mId_sel_check.setVisibility(isShowSel ? View.VISIBLE : View.GONE);
        mId_sel_check.setImageResource(homeMessageEntity.isSel() ? R.mipmap.icon_check_choose_circle : R.mipmap.icon_check_choose_circle_default);
    }

    public interface OnItemSelListener {
        void onItemChecked(int position);
    }
}
