package cn.swiftpass.wallet.intl.entity;

import com.contrarywind.interfaces.IPickerViewData;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardRewardDateItemList extends BaseEntity implements IPickerViewData {
    private String dateKey;
    private String display;

    public String getDateKey() {
        return dateKey;
    }

    public void setDateKey(String dateKey) {
        this.dateKey = dateKey;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public String getPickerViewText() {
        return display;
    }
}
