package cn.swiftpass.wallet.intl.entity.event;

import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;

public class DeepLinkSkipEventEntity extends BaseEventEntity {

    private TransferCrossBorderBaseEntity transferCrossBorderBaseEntity;


    public DeepLinkSkipEventEntity(int eventType, String message, TransferCrossBorderBaseEntity transferCrossBorderBaseEntity) {
        super(eventType, message);
        this.transferCrossBorderBaseEntity = transferCrossBorderBaseEntity;
    }


    public DeepLinkSkipEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public TransferCrossBorderBaseEntity getTransferCrossBorderBaseEntity() {
        return transferCrossBorderBaseEntity;
    }

    public void setTransferCrossBorderBaseEntity(TransferCrossBorderBaseEntity transferCrossBorderBaseEntity) {
        this.transferCrossBorderBaseEntity = transferCrossBorderBaseEntity;
    }
}
