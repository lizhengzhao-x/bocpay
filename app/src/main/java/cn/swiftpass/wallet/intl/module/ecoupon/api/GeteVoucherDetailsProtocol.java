package cn.swiftpass.wallet.intl.module.ecoupon.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/8/11.
 * 获取电子券详情
 */
public class GeteVoucherDetailsProtocol extends BaseProtocol {

    private String referenceNo;

    public GeteVoucherDetailsProtocol(String referenceNoIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/geteVoucherDetails";
        this.referenceNo = referenceNoIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.REFERENCENO, referenceNo);
    }
}
