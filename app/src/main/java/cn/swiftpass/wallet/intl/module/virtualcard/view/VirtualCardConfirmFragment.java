package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.text.TextUtils;
import android.view.View;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.VirtualCardConfirmContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.VirtualCardConfirmPresenter;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

/**
 * 確認／綁定信用卡 页面
 */
public class VirtualCardConfirmFragment extends BaseFragment<VirtualCardConfirmContract.Presenter> implements VirtualCardConfirmContract.View{
    private String virtualCardUrl = "";
    private String virtualCardShareUrl = "";

    public static VirtualCardConfirmFragment newInstance(String virtualCardUrl, String virtualCardShareUrl) {
        VirtualCardConfirmFragment fragment = new VirtualCardConfirmFragment();
        fragment.virtualCardUrl = virtualCardUrl;
        fragment.virtualCardShareUrl = virtualCardShareUrl;
        return fragment;
    }


    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.SMB1_1_4);
        }
    }


    @Override
    protected void initView(View v) {

    }

    @Override
    protected VirtualCardConfirmContract.Presenter loadPresenter() {
        return new VirtualCardConfirmPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_virtual_confirm;
    }

    @OnClick({R.id.lly_bank_card, R.id.lly_i_card})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lly_bank_card:
                jumpBindBankCard();
                break;
            case R.id.lly_i_card:
                inputPswPopWindow();
                break;
        }
    }

    /**
     * 密码弹出框
     */
    public void inputPswPopWindow() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false, false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                //打开虚拟卡需要验证支付密码
                mPresenter.getVirtualCardList();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    @Override
    public void getVirtualCardListSuccess(VirtualCardListEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.VITUALCARD_BIND);
        mHashMaps.put(Constants.VIRTUALCARDLIST, response);
        ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardBindTermsActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getVirtualCardListError(final String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, ErrorCode.VIRTUAL_LIST_FAILED.code)) {
            EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_VIRTUAL_LIST_FAILED, ""));
        } else {
            showErrorMsgDialog(getActivity(), errorMsg);
        }
    }


    /**
     * 跳入绑卡流程页面
     */
    private void jumpBindBankCard() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
        ActivitySkipUtil.startAnotherActivity(getActivity(), InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//        HashMap<String, Object> mHashMaps = new HashMap<>();
//        mHashMaps.put(Constants.DETAIL_URL, virtualCardUrl);
//        mHashMaps.put(WebViewActivity.NEW_MSG, true);
//        mHashMaps.put(WebViewActivity.SHARE_CONTENT, virtualCardShareUrl);
//        ActivitySkipUtil.startAnotherActivity(getActivity(), VirtualCardApplyActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }
}
