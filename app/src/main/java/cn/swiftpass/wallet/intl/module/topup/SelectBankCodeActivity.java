package cn.swiftpass.wallet.intl.module.topup;

import android.content.Intent;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.SortEntity;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.SortAdapter;
import cn.swiftpass.wallet.intl.utils.PinyinComparator;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.SideBar;


/**
 * s2充值 绑卡选择银行
 */
public class SelectBankCodeActivity extends BaseCompatActivity {

    @BindView(R.id.id_edit_search)
    ClearEditText idEditSearch;
    @BindView(R.id.id_recyclerView)
    RecyclerView idRecyclerView;
    @BindView(R.id.id_sideBar)
    SideBar idSideBar;
    @BindView(R.id.id_dialog)
    TextView idDialog;
    private SortAdapter adapter;
    LinearLayoutManager manager;
    private List<SortEntity> mSourceDateList;
    private PinyinComparator pinyinComparator;
    private ArrayList<GetRegEddaInfoEntity.BankListBean> mBankListInfos;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.title_region);
        pinyinComparator = new PinyinComparator();
        idSideBar.setTextView(idDialog);
        setToolBarTitle(R.string.transfer_method);
        mBankListInfos = (ArrayList<GetRegEddaInfoEntity.BankListBean>) getIntent().getExtras().getSerializable(Constants.BANK_LIST);

        getBankList();

        //根据输入框输入值的改变来过滤搜索
        idEditSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //当输入框里面的值为空，更新为原来的列表，否则为过滤数据列表
                filterData(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        //屏蔽换行键
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contentEquals("\n")) {
                    return "";
                } else {
                    return null;
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter};
        idEditSearch.setFilters(filters);
    }


    private void getBankList() {
        List<String> arrays = new ArrayList<>();
        for (int i = 0; i < mBankListInfos.size(); i++) {
            arrays.add(mBankListInfos.get(i).getParticipantName() + Constants.SPECIAL_LETTER_BANK_TEXT + mBankListInfos.get(i).getParticipantCode());
        }
        updateUi(arrays);
    }


    private void updateUi(List<String> arrays) {
        String[] dataStrs = arrays.toArray(new String[arrays.size()]);
        mSourceDateList = filledData(dataStrs);
        // 根据a-z进行排序源数据
        Collections.sort(mSourceDateList, pinyinComparator);
        //RecyclerView社置manager
        manager = new LinearLayoutManager(this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        idRecyclerView.setLayoutManager(manager);
        adapter = new SortAdapter(this, mSourceDateList);
        idRecyclerView.setAdapter(adapter);
        //item点击事件
        adapter.setOnItemClickListener(new SortAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                String name = ((SortEntity) adapter.getItem(position)).getName();
                String code = name.substring(name.indexOf(Constants.SPECIAL_LETTER_BANK_TEXT) + Constants.SPECIAL_LETTER_BANK_TEXT.length(), name.length());
                GetRegEddaInfoEntity.BankListBean bankListBean = (infilterWithBankCode(code));
                Intent intent = new Intent();
                intent.putExtra(Constants.BANK_NAME, bankListBean.getParticipantName());
                intent.putExtra(Constants.BANK_CODE, bankListBean.getParticipantCode());
                intent.putExtra(Constants.CURRENCY, bankListBean.getCurrency());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        //设置右侧SideBar触摸监听
        idSideBar.setOnTouchingLetterChangedListener(new SideBar.OnTouchingLetterChangedListener() {

            @Override
            public void onTouchingLetterChanged(String s) {
                //该字母首次出现的位置
                int position = adapter.getPositionForSection(s.charAt(0));
                if (position != -1) {
                    manager.scrollToPositionWithOffset(position, 0);
                }
            }
        });
    }


    private GetRegEddaInfoEntity.BankListBean infilterWithBankCode(String bankCode) {
        for (int i = 0; i < mBankListInfos.size(); i++) {
            if (mBankListInfos.get(i).getParticipantCode().equals(bankCode)) {
                return mBankListInfos.get(i);
            }
        }
        return null;
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_select_countrycode;
    }


    /**
     * 为RecyclerView填充数据
     *
     * @param date
     * @return
     */
    private List<SortEntity> filledData(String[] date) {
        List<SortEntity> mSortList = new ArrayList<>();
        for (int i = 0; i < date.length; i++) {
            SortEntity sortModel = new SortEntity();
            sortModel.setName(date[i]);
            //汉字转换成拼音
//            String pinyin = PinyinUtils.getPingYin(date[i]);
            String pinyin = Pinyin.toPinyin(date[i], "");
            String sortString = pinyin.substring(0, 1).toUpperCase();
            // 正则表达式，判断首字母是否是英文字母
            if (sortString.matches("[A-Z]")) {
                sortModel.setLetters(sortString.toUpperCase());
            } else {
                sortModel.setLetters("#");
            }

            mSortList.add(sortModel);
        }
        return mSortList;

    }

    /**
     * 根据输入框中的值来过滤数据并更新RecyclerView
     *
     * @param filterStr
     */
    private void filterData(String filterStr) {
        List<SortEntity> filterDateList = new ArrayList<>();
        if (TextUtils.isEmpty(filterStr)) {
            filterDateList = mSourceDateList;
        } else {
            filterDateList.clear();
            for (SortEntity sortModel : mSourceDateList) {
                String name = sortModel.getName();
                String firstSpell = Pinyin.toPinyin(name, "");
                if (name.indexOf(filterStr.toString()) != -1 ||
                        firstSpell.startsWith(filterStr.toString())
                        //不区分大小写
                        || firstSpell.startsWith(filterStr.toString()) ||
                        firstSpell.toUpperCase().startsWith(filterStr.toString())) {
                    filterDateList.add(sortModel);
                }
            }
        }
        // 根据a-z进行排序
        Collections.sort(filterDateList, pinyinComparator);
        adapter.updateList(filterDateList);
    }

}
