package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.BillListEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.OrderQueryEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.view.CrossBorderRemittanceResultActivity;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListCheckMoreAdapter;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListTitleAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

import static cn.swiftpass.wallet.intl.module.transactionrecords.view.OrderDetailActivity.BOCPAYENTITY;
import static cn.swiftpass.wallet.intl.module.transactionrecords.view.OrderDetailActivity.ISFROMBOCPAY;


public class BocPayTransacFragment extends BaseFragment {
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    @BindView(R.id.id_empty_view)
    View id_empty_view;
    @BindView(R.id.id_nodata_image)
    ImageView idNodataImage;
    private List<OrderQueryEntity> paymentEnquiryResults;
    private BillListEntity billListEntity;
    private HashMap<String, List<OrderQueryEntity>> paymentResults;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transac;
    }

    @Override
    public void initTitle() {
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    protected void initView(View v) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        idRecyclerview.addItemDecoration(itemDecoration);
        idRecyclerview.setEmptyView(id_empty_view);
        paymentEnquiryResults = new ArrayList<>();
        ((IAdapter<OrderQueryEntity>) idRecyclerview.getAdapter()).setData(paymentEnquiryResults);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        paymentResults = new HashMap<>();
        getOrderListByCard();
        //下拉刷新
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getOrderListByCard();
            }
        });
    }


    public void clearData() {
        if (paymentEnquiryResults != null && idRecyclerview != null && idRecyclerview.getAdapter() != null) {
            paymentEnquiryResults.clear();
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }


//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(GradeReduceEntity event) {
//        if (event.getEventType() == GradeReduceEntity.EVENT_GRADE_REDUCE_SUCCESS) {
//            getOrderListByCard();
//        }
//    }


    private CommonRcvAdapter<OrderQueryEntity> getAdapter(List<OrderQueryEntity> data) {
        return new CommonRcvAdapter<OrderQueryEntity>(data) {

            @Override
            public Object getItemType(OrderQueryEntity demoModel) {
                return demoModel.getUiType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                //recycleView item样式分为两种 1：标题 2：内容
                if (type.equals(Constants.TYPE_TITLE)) {
                    return new BillListTitleAdapter(getContext());
                } else if (type.equals(Constants.TYPE_CHECK_MORE)) {
                    return new BillListCheckMoreAdapter(getContext(), new BillListCheckMoreAdapter.CheckMoreItemCallback() {
                        @Override
                        public void checkMoreClick() {
                            super.checkMoreClick();
                            TransactionManagementFragment transactionManagementFragment = (TransactionManagementFragment) getParentFragment();
                            transactionManagementFragment.checkMoreEvent();
                        }
                    });
                } else {
                    return new BillListAdapter(getContext(), new CardItemCallback() {

                        @Override
                        public void onItemCardClick(int position) {
                            super.onItemCardClick(position);
                            if (!ButtonUtils.isFastDoubleClick()) {
                                if (!TextUtils.isEmpty(paymentEnquiryResults.get(position).getRecordType()) && paymentEnquiryResults.get(position).isCrossBorderTransfer()) {
                                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                                    mHashMapsLogin.put(Constants.TNX_ID, paymentEnquiryResults.get(position).getTransactionNum());
                                    ActivitySkipUtil.startAnotherActivity(getActivity(), CrossBorderRemittanceResultActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                                } else {
                                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                                    OrderQueryEntity orderQueryEntity = paymentEnquiryResults.get(position);
                                    orderQueryEntity.setQrcVoucherNumber(orderQueryEntity.getTransactionNum());
                                    mHashMapsLogin.put(BOCPAYENTITY, orderQueryEntity);
                                    mHashMapsLogin.put(ISFROMBOCPAY, true);
                                    ActivitySkipUtil.startAnotherActivity(getActivity(), OrderDetailActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

                                }
                            }
                        }
                    });
                }

            }
        };
    }

    public void getOrderListByCard() {
        showDialogNotCancel();
//        查询方式 0全部 1信用卡 2电子账户
        ApiProtocolImplManager.getInstance().getQueryBill(getActivity(), true, "0", "1", "40", "", new NetWorkCallbackListener<BillListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                if (getActivity() == null) {
                    return;
                }
                showErrorMsgDialog(getActivity(), errorMsg);
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                    if (idRecyclerview.getAdapter() != null) {
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                    }
                }
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                }
            }

            @Override
            public void onSuccess(BillListEntity response) {
                dismissDialog();
                if (idRecyclerview != null) {
                    idRecyclerview.setLoadSuccess(true);
                }
                //停止刷新
                if (smartRefreshLayout != null) {
                    smartRefreshLayout.finishRefresh();
                } else {
                    return;
                }
                billListEntity = response;
                paymentEnquiryResults.clear();
                paymentResults.clear();
                if (response.getOrderList() != null) {
                    int size = response.getOrderList().size();
                    dealWithData(size > Constants.BILL_ITEM_PAGE_SIZE ? Constants.BILL_ITEM_PAGE_SIZE : size, size > Constants.BILL_ITEM_PAGE_SIZE ? true : false);
                } else {
                    if (idRecyclerview.getAdapter() != null) {
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                    }
                }
            }
        });

    }


    private void dealWithData(int sizeIndex, boolean addCheckMore) {
        paymentEnquiryResults.clear();
        paymentResults.clear();
        //所有交易列表客户端根据日期进行分组处理
        for (int i = 0; i < sizeIndex; i++) {
            OrderQueryEntity paymentEnquiryResult = billListEntity.getOrderList().get(i);
            paymentEnquiryResult.setUiType(Constants.TYPE_CONTENT);
            String transDate = paymentEnquiryResult.getTransDate().trim();
            if (!TextUtils.isEmpty(transDate) && transDate.length() > 4) {
                String items[] = transDate.split("-");
                String monthTtitle = items[1] + "/" + items[0];
                if (paymentResults.containsKey(monthTtitle)) {
                    List<OrderQueryEntity> paymentEnquiryResults = paymentResults.get(monthTtitle);
                    paymentEnquiryResults.add(paymentEnquiryResult);
                } else {
                    OrderQueryEntity paymentEnquiryTitle = new OrderQueryEntity();
                    paymentEnquiryTitle.setUiTitle(monthTtitle);
                    paymentEnquiryTitle.setUiType(Constants.TYPE_TITLE);
                    paymentEnquiryResults.add(paymentEnquiryTitle);
                    List<OrderQueryEntity> paymentEnquiryResultItems = new ArrayList<>();
                    paymentEnquiryResultItems.add(paymentEnquiryResult);
                    paymentResults.put(monthTtitle, paymentEnquiryResultItems);
                }
                paymentEnquiryResults.add(paymentEnquiryResult);
            } else {
                paymentEnquiryResults.add(paymentEnquiryResult);
            }
        }
        if (addCheckMore) {
            OrderQueryEntity orderQueryEntity = new OrderQueryEntity();
            orderQueryEntity.setUiType(Constants.TYPE_CHECK_MORE);
            paymentEnquiryResults.add(orderQueryEntity);
        }
        if (idRecyclerview.getAdapter() != null) {
            idRecyclerview.getAdapter().notifyDataSetChanged();
        }
    }

    public void updateCheckMoreUI() {
        if (billListEntity != null && billListEntity.getOrderList().size() > Constants.BILL_ITEM_PAGE_SIZE) {
            dealWithData(billListEntity.getOrderList().size(), false);
        }
    }

}
