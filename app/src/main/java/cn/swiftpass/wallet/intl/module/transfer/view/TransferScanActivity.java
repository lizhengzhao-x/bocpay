package cn.swiftpass.wallet.intl.module.transfer.view;

import android.os.Bundle;
import android.view.WindowManager;
import android.widget.LinearLayout;

import androidx.fragment.app.Fragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.activitys.MessageFragAdapter;
import cn.swiftpass.wallet.intl.widget.NoTouchEventViewPager;


public class TransferScanActivity extends BaseCompatActivity {
    @BindView(R.id.id_center_view)
    NoTouchEventViewPager idCenterView;
    @BindView(R.id.ll_parent)
    LinearLayout parentLayout;
    private FPSPaymentScanFragment mScanFragment;
    private ArrayList<Fragment> mFragmentList;
    public static final int FPS_EVENT = 1110;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        idCenterView.setEnabled(true);

        mScanFragment = new FPSPaymentScanFragment();

        mFragmentList = new ArrayList<>();
        mFragmentList.add(mScanFragment);
        MessageFragAdapter messageFragAdapter = new MessageFragAdapter(getSupportFragmentManager(), mFragmentList);
        idCenterView.setAdapter(messageFragAdapter);
        idCenterView.setCurrentItem(0);
        idCenterView.setEnabled(false);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        //设置屏幕亮度最大
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        //取消屏幕最亮
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_SUCCESS) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_scan_main;
    }


}
