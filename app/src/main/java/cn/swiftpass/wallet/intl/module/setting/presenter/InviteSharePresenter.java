package cn.swiftpass.wallet.intl.module.setting.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetTransferInviteRecordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferShareUiInfoProtocol;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.entity.InviteShareEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.InviteShareContract;


public class InviteSharePresenter extends BasePresenter<InviteShareContract.View> implements InviteShareContract.Presenter {

    @Override
    public void getInviteShareList() {
        new GetTransferInviteRecordProtocol(new NetWorkCallbackListener<InviteRecordListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getInviteShareListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(InviteRecordListEntity response) {
                if (getView() != null) {
                    getView().getInviteShareListSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void getInviteShareDetail(String type) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferShareUiInfoProtocol(new NetWorkCallbackListener<InviteShareEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInviteShareDetailError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(InviteShareEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getInviteShareDetailSuccess(response, type);
                }
            }
        }).execute();
    }


}
