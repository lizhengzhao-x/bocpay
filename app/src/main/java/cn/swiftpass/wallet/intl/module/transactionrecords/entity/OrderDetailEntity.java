package cn.swiftpass.wallet.intl.module.transactionrecords.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class OrderDetailEntity extends BaseEntity {


    /**
     * 0：手机号 1：邮箱
     */
    private String crAcTp;
    /**
     * 手机号/邮箱
     */
    private String crAcNo;

    public String getAcMainNo() {
        return acMainNo;
    }

    public void setAcMainNo(String acMainNo) {
        this.acMainNo = acMainNo;
    }

    private String acMainNo;
    /**
     * 收款银行
     */
    private String bankName;
    /**
     * 付款人名称
     */
    private String debitName;
    /**
     * 收款人名称
     */
    private String creditName;

    /**
     * panFour
     */
    private String panFour;
    /**
     * smartLevel：账户登记 2：支付账户 3：智能账户
     */
    private String smartLevel;
    /**
     * card type 1：信用卡 2：智能账户
     */
    private String cardType;
    /**
     * 商户名
     */
    private String merchantName;
    /**
     * 扣除积分
     */
    private String redeemCount;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 附言
     */
    private String remark;


    public String getOutrejtype() {
        return outrejtype;
    }

    public void setOutrejtype(String outrejtype) {
        this.outrejtype = outrejtype;
    }

    /**
     * 失败类型
     */
    private String outrejtype;

    public OrderDetailEntity() {
        this.crAcTp = "";
        this.crAcNo = "";
        this.bankName = "";
        this.debitName = "";
        this.creditName = "";
        this.panFour = "";
        this.smartLevel = "";
        this.cardType = "";
        this.merchantName = "";
        this.redeemCount = "";
    }


    public String getCrAcTp() {
        return crAcTp;
    }

    public void setCrAcTp(String crAcTp) {
        this.crAcTp = crAcTp;
    }

    public String getCrAcNo() {
        return crAcNo;
    }

    public void setCrAcNo(String crAcNo) {
        this.crAcNo = crAcNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getDebitName() {
        return debitName;
    }

    public void setDebitName(String debitName) {
        this.debitName = debitName;
    }

    public String getCreditName() {
        return creditName;
    }

    public void setCreditName(String creditName) {
        this.creditName = creditName;
    }

    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    public String getSmartLevel() {
        return smartLevel;
    }

    public void setSmartLevel(String smartLevel) {
        this.smartLevel = smartLevel;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getReduceGp() {
        return redeemCount;
    }

    public void setReduceGp(String reduceGp) {
        this.redeemCount = reduceGp;
    }


}
