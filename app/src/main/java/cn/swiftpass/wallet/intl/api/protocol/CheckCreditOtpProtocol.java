package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 校验信用卡OTP接口，返回对象为 SendOTPEntity
 */

public class CheckCreditOtpProtocol extends BaseProtocol {
    public static final String TAG = CheckCreditOtpProtocol.class.getSimpleName();
    /**
     * 用户id
     */
    String mWalletId;
    /**
     * 卡号
     */
    String mPan;
    /**
     * 过期时间
     */
    String mExpDate;
    /**
     * 标识取【L】：B：绑定检查；V：仅验证；F：忘记密码，L：登录；
     */
    String mAction;
    /**
     * 短信验证码
     */
    String mOtp;

    String mAccount;

    public CheckCreditOtpProtocol(String account, String walletId, String pan, String expDate, String action, String otp, NetWorkCallbackListener dataCallback) {
        this.mWalletId = walletId;
        this.mPan = pan;
        this.mExpDate = expDate;
        this.mAction = action;
        this.mOtp = otp;
        mAccount = account;
        this.mDataCallback = dataCallback;
        mUrl = "api/register/checkOtpCc";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, mWalletId);
        mBodyParams.put(RequestParams.PAN, mPan);
        mBodyParams.put(RequestParams.EXDATE, mExpDate);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.OTP, mOtp);
        mBodyParams.put(RequestParams.ACCOUNT, mAccount);
    }
}
