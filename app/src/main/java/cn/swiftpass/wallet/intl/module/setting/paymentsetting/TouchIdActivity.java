package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tradelink.boc.authapp.utils.AuthCommonUtils;
import com.tradelink.boc.authapp.utils.FidoOperationUtils;
import com.tradelink.boc.sotp.utils.SoftTokenOperationUtils;

import butterknife.BindView;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.FioConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.PaymentPasswordDialogFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.FioCheckEntity;
import cn.swiftpass.wallet.intl.entity.FioCheckValidEntity;
import cn.swiftpass.wallet.intl.module.register.RegisterInputOTPActivity;
import cn.swiftpass.wallet.intl.module.setting.contract.TouchIDContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.TouchIDPresenter;
import cn.swiftpass.wallet.intl.sdk.fingercore.MyFingerPrintManager;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;

/**
 * @author
 * @date 2018/9/7
 */

public class TouchIdActivity extends BaseCompatActivity<TouchIDContract.Presenter> implements TouchIDContract.View {

    private boolean bTouchOpen = false;
    private int POLICY_ACTION_REG = 1;
    private int POLICY_ACTION_OPEN = 2;
    private int POLICY_ACTION_CHECK = 3;
    private int ACTION_VERIFY_PWD = 4;
    private int POLICY_ACTION_CHECK_NOT_OPEN = 548;
    private int mCheckPolicyAction = -1;
    private static final String TAG = TouchIdActivity.class.getSimpleName();
    private static final int TOUCH_ID_REQUEST_CODE = 0x16;
    @BindView(R.id.tv_touch_tip)
    TextView mTouchTipTV;
    private PaymentPasswordDialogFragment pwdDilaog;
    @BindView(R.id.layout_touchpay)
    ImageArrowView mTouchImage;

    private String deletefio;


    @Override
    public void init() {
        setToolBarTitle(R.string.IDV_32_1);
        checkValidFio();
        mTouchImage.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (bTouchOpen) {
                    String msg = getString(R.string.FI_4_1);
                    String title = "    ";
                    String cancelText = getString(R.string.FI_4_3);
                    String comfirmText = getString(R.string.FI_4_2);
                    AndroidUtils.showTipDialog(TouchIdActivity.this, title, msg, comfirmText, cancelText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deletefio = AuthCommonUtils.getFioKey();
                            FIOActivity.startCheckPolicy(TouchIdActivity.this);
                            mCheckPolicyAction = POLICY_ACTION_REG;
                        }
                    });
                } else {
                    mCheckPolicyAction = ACTION_VERIFY_PWD;
                    initPwdWindow();
                }
            }
        });


    }


    @Override
    public void onFioSuccess(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case FioConst.REQUEST_CODE_CHECK_POLICY:
            case FioConst.REQUEST_CODE_CHECK_VALID_FIO:
                checkFailResult();
                break;
            case FioConst.REQUEST_CODE_REG_FIO:
                mPresenter.onFIOCheck();
                break;
            case FioConst.REQUEST_CODE_DE_REG_FIO:
                bTouchOpen = false;
                MyFingerPrintManager.getInstance().clearFIO();
                initTouchView(false);
                if (!TextUtils.isEmpty(deletefio)) {
                    mPresenter.onFioDelete(deletefio);
                }
                break;
            default:
                break;

        }

    }

    private void checkFailResult() {
        if (mCheckPolicyAction == POLICY_ACTION_REG) {
            //注销前要调一次checkpolicy
            FIOActivity.startDegregisterFingerPrint(this);
        } else if (mCheckPolicyAction == POLICY_ACTION_OPEN) {
            initPayPwdVerify();
        } else if (mCheckPolicyAction == POLICY_ACTION_CHECK) {
            boolean bRegister = SoftTokenOperationUtils.isRegistered(this);
            boolean bChange = FidoOperationUtils.isFingerPrintChanged(this);
            if (bRegister) {
                if (bChange) {
                    //已经注册指纹，但是发生变化（新增，删除），需要注销原来指纹(需要调一次checkpolicy)
                    FIOActivity.startDegregisterFingerPrint(this);
                } else {
                    String fio = AuthCommonUtils.getFioKey();
                    mPresenter.onFioCheckValid(fio);
                }
            }
        }
    }

    @Override
    public void onFioFail(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FioConst.REQUEST_CODE_CHECK_POLICY:
                if (mCheckPolicyAction == POLICY_ACTION_OPEN) {
                    showErrorMsgDialog(TouchIdActivity.this, getString(R.string.not_support_finger));
                }
                break;
            case FioConst.REQUEST_CODE_CHECK_VALID_FIO:
                if (mCheckPolicyAction == POLICY_ACTION_OPEN) {
                    showErrorMsgDialog(TouchIdActivity.this, getString(R.string.open_touch_fail_tip));
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void verifyPwdFailed(String errorCode, String errorMsg) {
        if (pwdDilaog != null) {
            pwdDilaog.clearPwd();
        }
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void verifyPwdSuccess(BaseEntity response) {
        if (pwdDilaog != null) {
            pwdDilaog.dismiss();
        }
        if (mCheckPolicyAction == ACTION_VERIFY_PWD) {
            FIOActivity.startCheckPolicy(TouchIdActivity.this);
            mCheckPolicyAction = POLICY_ACTION_OPEN;
        } else {
            FIOActivity.startSetFingerPrint(TouchIdActivity.this, HttpCoreKeyManager.getInstance().getUserId());
        }

    }


    @Override
    public void onFioCheckSuccess(FioCheckEntity response) {
        sendFioOtp(response);
    }

    @Override
    public void onFioCheckFail(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void onFioCheckValidSuccess(FioCheckValidEntity response) {
        initTouchView(true);
        bTouchOpen = true;
    }

    @Override
    public void onFioCheckValidFail(String errorCode, String errorMsg) {
        if (TextUtils.equals(ErrorCode.FIO_SERVER_VERIFY_ERROR.code, errorCode) || TextUtils.equals(ErrorCode.FIO_SERVER_REG_ERROR.code, errorCode)) {
            FIOActivity.startDegregisterFingerPrint(TouchIdActivity.this);
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_touch_id;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        dismissDialog();

        if (requestCode == TOUCH_ID_REQUEST_CODE) {
            if (resultCode == RESULT_OK && null != data && data.getBooleanExtra(FioConst.FIO_ACTION_RESULT_OTP, false)) {
                onOpenTouchSuc();
            }
        } else {
            if (mPresenter != null) {
                mPresenter.onFioResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void initTouchView(boolean open) {
        if (open) {
            mTouchImage.setRightImage(R.mipmap.button_switch_on_s);
        } else {
            mTouchImage.setRightImage(R.mipmap.button_switch_off_s);
        }
    }


    //检测设备是否支持FIO
    private void checkValidFio() {
        if (!NetworkUtil.isNetworkAvailable()) {
            showErrorMsgDialog(this, getString(R.string.string_network_available));
            return;
        }
        //showDialogNotCancel();
        FIOActivity.startCheckPolicy(TouchIdActivity.this);
        mCheckPolicyAction = POLICY_ACTION_CHECK;
    }

    private void onOpenTouchSuc() {
        bTouchOpen = true;
        initTouchView(true);
        showErrorMsgDialog(this, getString(R.string.open_success));

    }


    private void sendFioOtp(FioCheckEntity response) {
        if (null != response) {
            String walletId = TextUtils.isEmpty(response.getWalletId()) ? HttpCoreKeyManager.getInstance().getUserId() : response.getWalletId();
            Intent intent = new Intent(TouchIdActivity.this, RegisterInputOTPActivity.class);
            intent.putExtra(Constants.DATA_PHONE_NUMBER, response.getAccount());
            intent.putExtra(Constants.EXDATE, response.getExpDate());
            intent.putExtra(Constants.ACCOUNT_TYPE, response.getAccountType());
            intent.putExtra(Constants.USER_ACCOUNT, response.getAccount());
            intent.putExtra(Constants.WALLET_ID, walletId);
            intent.putExtra(Constants.DATA_CARD_NUMBER, response.getPAN());
            intent.putExtra(Constants.CURRENT_PAGE_FLOW, Constants.DATA_TYPE_SET_FIO);
            startActivityForResult(intent, TOUCH_ID_REQUEST_CODE);
        }
    }


    private void initPayPwdVerify() {
        FIOActivity.startSetFingerPrint(TouchIdActivity.this, HttpCoreKeyManager.getInstance().getUserId());

    }

    private void initPwdWindow() {
        if (!NetworkUtil.isNetworkAvailable()) {
            showErrorMsgDialog(this, getString(R.string.string_network_available));
            return;
        }
        if (pwdDilaog == null) {
            pwdDilaog = new PaymentPasswordDialogFragment();
            PaymentPasswordDialogFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogFragment.OnPwdDialogClickListener() {
                @Override
                public void onPwdCompleteListener(String psw, boolean complete) {
                    if (complete) {
                        mPresenter.verifyPwd("FIO", psw);
                    }
                }

                @Override
                public void onPwdBackClickListener() {
                    setBackBtnEnable(true);
                }
            };
            pwdDilaog.initParams(onPwdDialogClickListener, false);
            pwdDilaog.show(getSupportFragmentManager(), "PaymentPasswordDialogFragment");
            pwdDilaog.clearPwd();
        } else {
            pwdDilaog.show(getSupportFragmentManager(), "PaymentPasswordDialogFragment");
            pwdDilaog.clearPwd();
        }

    }

    @Override
    protected TouchIDContract.Presenter createPresenter() {
        return new TouchIDPresenter();
    }


}
