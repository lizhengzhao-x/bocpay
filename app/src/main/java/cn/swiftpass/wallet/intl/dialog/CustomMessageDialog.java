package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


public class CustomMessageDialog extends Dialog {

    public CustomMessageDialog(Context context) {
        super(context);
    }

    public CustomMessageDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String title;
        private String message;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        private OnClickListener positiveButtonClickListener;
        private OnClickListener negativeButtonClickListener;
        private int leftColor;
        private int rightColor;


        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setLeftColor(int color) {
            leftColor = color;
            return this;
        }

        public Builder setRightColor(int color) {
            rightColor = color;
            return this;
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title
         * @return
         */
        public Builder setTitle(int title) {
            this.title = (String) context.getText(title);
            return this;
        }

        /**
         * Set the Dialog title from String
         *
         * @param title
         * @return
         */

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(int negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = (String) context.getText(negativeButtonText);
            this.negativeButtonClickListener = listener;
            return this;
        }

        public Builder setNegativeButton(String negativeButtonText, OnClickListener listener) {
            this.negativeButtonText = negativeButtonText;
            this.negativeButtonClickListener = listener;
            return this;
        }

        public CustomMessageDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomMessageDialog dialog = new CustomMessageDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_normal_msg_layout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title

            TextView titleTV = layout.findViewById(R.id.title_dl);
            TextView messageTV = layout.findViewById(R.id.message_dl);

            Button posBtn = layout.findViewById(R.id.positive_dl);
            Button negBtn = layout.findViewById(R.id.negative_dl);

            if (!TextUtils.isEmpty(title)) {
                titleTV.setVisibility(View.VISIBLE);
                titleTV.setText(title);
            } else {
                titleTV.setVisibility(View.GONE);
            }
            // set the confirm button
            if (positiveButtonText != null) {
                posBtn.setText(positiveButtonText);
//                ((Button) layout.findViewById(R.id.negative_dl)).setTextColor(rightColor);
                if (positiveButtonClickListener != null) {
                    posBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                posBtn.setVisibility(View.GONE);
            }
            // set the cancel button
            if (negativeButtonText != null) {
                negBtn.setText(negativeButtonText);
//                ((Button) layout.findViewById(R.id.negative_dl)).setTextColor(leftColor);
                if (negativeButtonClickListener != null) {
                    negBtn.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            negativeButtonClickListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                negBtn.setVisibility(View.GONE);
                posBtn.setBackgroundResource(R.drawable.dialog_buttom_total_bg);
            }
            // set the content message
            if (message != null) {
                messageTV.setText(message.trim());
                if (0 != gravity) {
                    messageTV.setGravity(gravity);
                }
            }

            dialog.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });

            dialog.setContentView(layout);
            return dialog;
        }
    }
}

