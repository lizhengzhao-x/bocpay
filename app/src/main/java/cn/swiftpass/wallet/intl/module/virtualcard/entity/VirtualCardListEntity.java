package cn.swiftpass.wallet.intl.module.virtualcard.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


/**
 * Created by ZhangXinchao on 2019/11/15.
 */
public class VirtualCardListEntity extends BaseEntity {


    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    private String account;

    public String getWalletId() {
        return walletId;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    private String walletId;
    private List<VirtualCardListBean> virtualCardList;

    public List<VirtualCardListBean> getVirtualCardList() {
        return virtualCardList;
    }

    public void setVirtualCardList(List<VirtualCardListBean> virtualCardList) {
        this.virtualCardList = virtualCardList;
    }

    public static class VirtualCardListBean extends BaseEntity {
        /**
         * olSpendingLmt : 50000
         * cvv : 220
         * account : 86-18588429401
         * expDate : 10/21
         * crLmt : 50000
         * crdArtId : ABCDEFGHKIGK
         * pan : 8888888888880
         */

        private String olSpendingLmt;
        private String cvv;
        private String account;
        private String expDate;
        private String crLmt;
        private String crdArtId;
        private String pan;

        public String getCardType() {
            return cardType;
        }

        public void setCardType(String cardType) {
            this.cardType = cardType;
        }

        //1:信用卡  2：智能账号
        private String cardType;

        public String getCardFaceId() {
            return cardFaceId;
        }

        public void setCardFaceId(String cardFaceId) {
            this.cardFaceId = cardFaceId;
        }

        private String cardFaceId;

        public String getCardId() {
            return cardId;
        }

        public void setCardId(String cardId) {
            this.cardId = cardId;
        }

        private String cardId;

        public String getOlSpendingLmt() {
            return olSpendingLmt;
        }

        public void setOlSpendingLmt(String olSpendingLmt) {
            this.olSpendingLmt = olSpendingLmt;
        }

        public String getCvv() {
            return cvv;
        }

        public void setCvv(String cvv) {
            this.cvv = cvv;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getExpDate() {
            return expDate;
        }

        public void setExpDate(String expDate) {
            this.expDate = expDate;
        }

        public String getCrLmt() {
            return crLmt;
        }

        public void setCrLmt(String crLmt) {
            this.crLmt = crLmt;
        }

        public String getCrdArtId() {
            return crdArtId;
        }

        public void setCrdArtId(String crdArtId) {
            this.crdArtId = crdArtId;
        }

        public String getPan() {
            return pan;
        }

        public void setPan(String pan) {
            this.pan = pan;
        }
    }
}
