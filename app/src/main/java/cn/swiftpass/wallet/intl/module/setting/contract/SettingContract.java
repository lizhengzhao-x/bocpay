package cn.swiftpass.wallet.intl.module.setting.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;

public class SettingContract {

    public interface View extends IView {


        void getNotificationStatusSuccess(NotificationStatusEntity notificationStatusEntity);

        void getNotificationStatusFailed(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<SettingContract.View> {

        /**
         *
         * @param status 0：关 1：开
         * @param action Q:查询 U：更新
         */
        void getNotificationStatus(boolean showDialog,String status, String action);

    }

}
