package cn.swiftpass.wallet.intl.module.ecoupon.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;

/**
 * 电子券换领
 */
public class RedeemGiftProtocol extends BaseProtocol {
    EcouponConvertEntity ecouponConvertEntity;

    public RedeemGiftProtocol(EcouponConvertEntity ecouponConvertEntityIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.ecouponConvertEntity = ecouponConvertEntityIn;
        mUrl = "api/eVoucher/redeemGift";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.REQACT, ecouponConvertEntity.getReqAct());
        mBodyParams.put(RequestParams.REDMID, ecouponConvertEntity.getRedmId());
        mBodyParams.put(RequestParams.REDEEMITEMS, ecouponConvertEntity.getRedeemItems());
        mBodyParams.put(RequestParams.GIFTTP, ecouponConvertEntity.getGiftTp());
        mBodyParams.put(RequestParams.REDEEMPOINT, ecouponConvertEntity.getRedeemPoint());
        mBodyParams.put(RequestParams.PAN_ID, ecouponConvertEntity.getPan());
    }
}
