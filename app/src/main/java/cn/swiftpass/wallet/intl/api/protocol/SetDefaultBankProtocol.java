package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


/**
 * fps设置默认银行
 */
public class SetDefaultBankProtocol extends BaseProtocol {
    String accountId;
    String accountIdType;

    public SetDefaultBankProtocol(String accountIdIn, String accountIdTypeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        accountId = accountIdIn;
        accountIdType = accountIdTypeIn;
        mUrl = "api/fps/setDefaultBank";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACCOUNTID, accountId);
        mBodyParams.put(RequestParams.ACCOUNTIDTYPE, accountIdType);
    }

}
