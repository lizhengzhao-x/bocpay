package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 积分查询
 */

public class ActionQueryGpEcoupInfoProtocol extends BaseProtocol {
    String cardId;

    public ActionQueryGpEcoupInfoProtocol(String cardIdIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        cardId = cardIdIn;
        mUrl = "api/eVoucher/actRedmPnt";
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(cardId)) {
            mBodyParams.put(RequestParams.CARDID, cardId);
        }
    }

}
