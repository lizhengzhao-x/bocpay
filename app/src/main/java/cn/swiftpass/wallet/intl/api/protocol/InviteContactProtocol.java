package cn.swiftpass.wallet.intl.api.protocol;

import java.util.List;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;

public class InviteContactProtocol extends BaseProtocol {

    private List<InviteContactEntity> mAccountList;

    public InviteContactProtocol(List<InviteContactEntity> accountList, NetWorkCallbackListener netWorkCallbackListener) {
        this.mAccountList = accountList;
        this.mDataCallback = netWorkCallbackListener;
        mUrl = "api/transfer/listAccountBindStatus";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.DATA, mAccountList);
    }
}
