package cn.swiftpass.wallet.intl.entity;


import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class SmartLevelEntity extends BaseEntity {

    public static final String SMART_LEVEL_STW0 = "2";
    public static final String SMART_LEVEL_STHREE = "3";

    public boolean isHasCredit() {
        if (TextUtils.isEmpty(hasCreditCard)) {
            return false;
        }
        return hasCreditCard.equals("1");
    }

    private boolean hasCredit;

    public String getSmartAccLevel() {
        return smartAccLevel;
    }

    public void setSmartAccLevel(String smartAccLevel) {
        this.smartAccLevel = smartAccLevel;
    }

    //纯信用卡：0；s1：1；s2：2；s3：3；
    private String smartAccLevel;

    public String getHasCreditCard() {
        return hasCreditCard;
    }

    public void setHasCreditCard(String hasCreditCard) {
        this.hasCreditCard = hasCreditCard;
    }

    private String hasCreditCard;

    public String getHasVirtualCard() {
        return hasVirtualCard;
    }

    public void setHasVirtualCard(String hasVirtualCard) {
        this.hasVirtualCard = hasVirtualCard;
    }

    /**
     * 1.是有虚拟卡
     */
    private String hasVirtualCard;

    public boolean isHasVirtualCard() {
        if (TextUtils.isEmpty(hasVirtualCard)) return false;
        return hasVirtualCard.equals("1");
    }
}
