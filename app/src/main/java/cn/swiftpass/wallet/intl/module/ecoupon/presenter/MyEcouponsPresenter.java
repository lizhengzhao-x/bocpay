package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetMyeVoucherListProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.MyEcouponsContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;

/**
 * Created by ZhangXinchao on 2019/7/25.
 */
public class MyEcouponsPresenter extends BasePresenter<MyEcouponsContract.View> implements MyEcouponsContract.Presenter {


    @Override
    public void getMyEcouponsList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetMyeVoucherListProtocol(new NetWorkCallbackListener<MyEVoucherEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyEcouponsListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(MyEVoucherEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getMyEcouponsListSuccess(response);
                }
            }
        }).execute();
    }


}
