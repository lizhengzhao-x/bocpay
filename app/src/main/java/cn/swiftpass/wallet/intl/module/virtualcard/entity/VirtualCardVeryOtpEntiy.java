package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardVeryOtpEntiy extends BaseEntity {


    /**
     * cvv : 555
     * expDate : 10/21
     * olSpendLmt : 55555
     * crLmt : 5555
     * pan : 8888888888880
     */

    private String cvv;
    private String expDate;
    private String olSpendLmt;
    private String crLmt;
    private String pan;

    public String getCrdArtId() {
        return crdArtId;
    }

    public void setCrdArtId(String crdArtId) {
        this.crdArtId = crdArtId;
    }

    private String crdArtId;

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getOlSpendLmt() {
        return olSpendLmt;
    }

    public void setOlSpendLmt(String olSpendLmt) {
        this.olSpendLmt = olSpendLmt;
    }

    public String getCrLmt() {
        return crLmt;
    }

    public void setCrLmt(String crLmt) {
        this.crLmt = crLmt;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}
