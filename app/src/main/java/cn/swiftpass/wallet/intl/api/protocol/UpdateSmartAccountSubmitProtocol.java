package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * s1 s2升级
 */
public class UpdateSmartAccountSubmitProtocol extends BaseProtocol {

    String mPrimaryAc;
    String mPayLimit;
    String mTopupmethod;
    String mAutotopupamt;

    public UpdateSmartAccountSubmitProtocol(String primaryAc, String payLimit, String topupmethod, String autotopupamt, NetWorkCallbackListener dataCallback) {
        mDataCallback = dataCallback;
        mUrl = "api/smartAcUpGrade/upGrade";
        mPrimaryAc = primaryAc;
        mPayLimit = payLimit;
        mTopupmethod = topupmethod;
        mAutotopupamt = autotopupamt;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.PRIMARYAC, mPrimaryAc);
        mBodyParams.put(RequestParams.PAYLIMIT, mPayLimit);
        mBodyParams.put(RequestParams.TOPUPMETHOD, mTopupmethod);
        mBodyParams.put(RequestParams.AUTO_TOPUP_LIMIT, mAutotopupamt);
    }
}
