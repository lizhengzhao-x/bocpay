package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class RedpacketPopResourceOutEntity extends BaseEntity {
    private RedpacketPopResourceEntity zh_HK;
    private RedpacketPopResourceEntity en_US;
    private RedpacketPopResourceEntity zh_CN;

    public RedpacketPopResourceEntity getZh_HK() {
        return zh_HK;
    }

    public void setZh_HK(RedpacketPopResourceEntity zh_HK) {
        this.zh_HK = zh_HK;
    }

    public RedpacketPopResourceEntity getEn_US() {
        return en_US;
    }

    public void setEn_US(RedpacketPopResourceEntity en_US) {
        this.en_US = en_US;
    }

    public RedpacketPopResourceEntity getZh_CN() {
        return zh_CN;
    }

    public void setZh_CN(RedpacketPopResourceEntity zh_CN) {
        this.zh_CN = zh_CN;
    }
}
