package cn.swiftpass.wallet.intl.module.callapp;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.ExternalSysParameterEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;

public class AppCallAppPaymentContract {

    public interface View extends IView {
        void getActionTrxGpInfoError(String errorCode, String errorMsg);

        /**
         * 查新积分信息
         *
         * @param parserQrcodeEntity
         */
        void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity parserQrcodeEntity);

        /**
         * 获取银行卡列表
         *
         * @param bankCards
         */
        void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards);

        void getPaymentTypeListError(String errorCode, String errorMsg);

        void getPayMentResultSuccess(PaymentEnquiryResult response);

        void getPayMentResultError(String errorCode, String errorMsg);

        void parseTransferCodeError(String errorCode, String errorMsg);

        void parseTransferCodeSuccess(TransferPreCheckEntity response);

//        /**
//         * 主扫成功
//         *
//         * @param response
//         */
//        void getMainScanOrderSuccess(ContentEntity response);

        /**
         * 主扫下单报错
         *
         * @param errorCode
         * @param errorMsg
         */
        void getMainScanOrderError(String errorCode, String errorMsg);

        void getMainScanOrderParamsError(String errorCode, String errorMsg);


        void parserQrcodeError(String errorCode, String errorMsg);

        void parserQrcodeSuccess(ExternalSysParameterEntity qrcode);


        void verifyOCSPUrlError(String errorCode, String errorMsg,String callbackUrl);


    }


    public interface Presenter extends IPresenter<AppCallAppPaymentContract.View> {

        /**
         * app call app 支付
         */
        void parserQrcode(String qrcode);

        void parserFpsQrcode(String qrCode);

        void verifyOCSPUrl(String fpsUrl);

        /**
         * 查询二维码积分信息
         *
         * @param mActionUpiInfo
         */
        void getActionTrxGpInfo(ActionTrxGpInfoEntity mActionUpiInfo);

        /**
         * 拉取当前二维码支付的卡列表
         */
        void getPaymentTypeList();

        /**
         * 主扫下单轮训接口
         *
         * @param cpQrCode
         * @param txnId
         */
        void getPayMentResult(String txnId);

        /**
         * 主扫下单
         *
         * @param orderRequest
         */
        void getMainScanOrder(MainScanOrderRequestEntity orderRequest);


        void actionMpqrPaymentWithPoint(MainScanOrderRequestEntity orderRequest);

    }

}
