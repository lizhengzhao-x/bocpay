package cn.swiftpass.wallet.intl.module.transfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/11.
 */
public class GreetingTxtEntity extends BaseEntity {

    public String getTitleStr() {
        return titleStr;
    }

    public void setTitleStr(String titleStr) {
        this.titleStr = titleStr;
    }

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }

    private String titleStr;
    private boolean isSel;
}
