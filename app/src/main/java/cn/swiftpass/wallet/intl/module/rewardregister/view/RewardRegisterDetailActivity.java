package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.RewardRegisterEventEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.contract.RewardRegisterContract;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.CheckRegistrationStatus;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardPansListEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardRegisterEntity;
import cn.swiftpass.wallet.intl.module.rewardregister.presenter.RewardRegisterPresenter;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterCheckSuccessActivity.REWARDNUMBER;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterSuccessActivity.REWARDNUMBERINFO;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.BANKCARDTYPE;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;
import static cn.swiftpass.wallet.intl.module.setting.WebViewActivity.SHOW_TOOLBAR_COLOSE_IMG;

/**
 * 信用卡奖赏登记详情页
 */
public class RewardRegisterDetailActivity extends BaseCompatActivity<RewardRegisterContract.Presenter> implements RewardRegisterContract.View {
    @BindView(R.id.id_top_image)
    ImageView idTopImage;
    @BindView(R.id.id_title_image)
    FrameLayout idTitleImage;
    @BindView(R.id.id_use_card_title)
    TextView idUseCardTitle;
    @BindView(R.id.id_use_card_reduce_grade)
    TextView idUseCardReduceGrade;
    @BindView(R.id.id_use_card_retain_grade)
    TextView idUseCardRetainGrade;
    @BindView(R.id.id_ll_selcard)
    View idLlSelcard;
    @BindView(R.id.id_tv_term_info)
    ImageView idTvTermInfo;
    @BindView(R.id.id_tv_term_detail)
    TextView idTvTermDetail;
    @BindView(R.id.id_conditions_view)
    ConstraintLayout idConditionsView;
    //    @BindView(R.id.id_tv_agree_info)
//    ImageView idTvAgreeInfo;
//    @BindView(R.id.id_tv_agree_detail)
    TextView idTvAgreeDetail;
    @BindView(R.id.id_tv_check_record)
    TextView idTvCheckRecord;
    @BindView(R.id.id_tv_register)
    TextView idTvRegister;
    @BindView(R.id.id_change_sel)
    View idTvChangeSel;
    @BindView(R.id.id_lin_content)
    View idLinChangeSel;
    @BindView(R.id.id_change_default_sel)
    TextView idDefaultSel;


    @BindView(R.id.id_linear_grade_btn)
    LinearLayout idLinearGradeBtn;
    @BindView(R.id.id_sel_cardlist)
    ImageView idSelCardlist;
    /**
     * 当前卡选择位置
     */
    private int currentCardSelPosition = -1;

    private String pan = "";
    private String campaignId;
    private String banckCardType;
    private String imageUrl, termsInfoUrl;

    public static final String CAMPAIGNID = "CAMPAIGNID";
    public static final String IMAGEURL = "IMAGE_URL";
    public static final String TERMINFOURL = "TERMINFOURL";
    public static final String CARDLIST = "CARDLIST";
    private RewardPansListEntity rewardPansListEntity;


    @Override
    public void init() {
        setToolBarTitle(R.string.CR209_11_1);
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        EventBus.getDefault().register(this);
        campaignId = getIntent().getExtras().getString(CAMPAIGNID);
        imageUrl = getIntent().getExtras().getString(IMAGEURL);
        termsInfoUrl = getIntent().getExtras().getString(TERMINFOURL);
        banckCardType = getIntent().getExtras().getString(BANKCARDTYPE);
        rewardPansListEntity = (RewardPansListEntity) getIntent().getExtras().getSerializable(CARDLIST);
        idTvCheckRecord.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                mPresenter.rewardCheckStatus(pan, campaignId);
            }
        });

        idTvRegister.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                mPresenter.rewardRegister(pan, campaignId);
            }
        });
        idTvChangeSel.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                if (rewardPansListEntity != null) {
                    showCardGradeDetailSelPop(rewardPansListEntity.getCardList());
                } else {
                    mPresenter.rewardCardList(banckCardType);
                }
            }
        });
        updateNextBtnStatus();
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) idTopImage.getLayoutParams();
        lp.width = AndroidUtils.getScreenWidth(this);
        lp.height = AndroidUtils.getScreenWidth(this) / 2;
        idTopImage.setLayoutParams(lp);
        GlideApp.with(mContext).load(imageUrl).placeholder(R.mipmap.img_cardpromotion_skeleton).into(idTopImage);
        BasicUtils.initSpannableStrWithTv(getString(R.string.CR209_7_4), "##", idTvTermDetail, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) {
                    return;
                }
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, termsInfoUrl);
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                mHashMaps.put(SHOW_TOOLBAR_COLOSE_IMG, true);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RewardRegisterEventEntity event) {
        if (event.getEventType() == RewardRegisterEventEntity.EVENT_BACK_REWARD_LIST) {
            finish();
        }
    }


    /**
     * 切换信用卡 使用不同卡兑换
     */
    private void showCardGradeDetailSelPop(List<RewardPansListEntity.RewardPan> cardList) {
        CheckCardRewardListPop checkGradeListPop = new CheckCardRewardListPop(this, cardList, currentCardSelPosition, new CheckCardRewardListPop.OnGradeWindowClickListener() {
            @Override
            public void onGradeItemClickListener(int position) {
                currentCardSelPosition = position;
                updateGradeLayout(cardList.get(position));
            }
        });
        checkGradeListPop.show();
    }


    //    @OnClick({R.id.id_tv_term_info, R.id.id_tv_agree_info})
    @OnClick({R.id.id_tv_term_info})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.id_tv_term_info:
                idTvTermInfo.setSelected(!idTvTermInfo.isSelected());
                updateNextBtnStatus();
                break;

//            case R.id.id_tv_agree_info:
//                idTvAgreeInfo.setSelected(!idTvAgreeInfo.isSelected());
//                updateNextBtnStatus();
//                break;
            default:
                break;
        }

    }

    private void updateNextBtnStatus() {
//        boolean canSel = idTvTermInfo.isSelected() && !TextUtils.isEmpty(idUseCardTitle.getText()) && !TextUtils.isEmpty(pan);
        boolean canSel = !TextUtils.isEmpty(idUseCardTitle.getText()) && !TextUtils.isEmpty(pan);
        idTvRegister.setSelected(canSel);
        idTvRegister.setEnabled(canSel);

        idTvCheckRecord.setSelected(canSel);
        idTvCheckRecord.setEnabled(canSel);

        if (canSel) {
            idTvCheckRecord.setTextColor(Color.parseColor("#77808A"));
            idTvCheckRecord.setBackgroundResource(R.drawable.bg_btn_reward_check_sel);
        } else {

            idTvCheckRecord.setTextColor(Color.parseColor("#E2E2E2"));
            idTvCheckRecord.setBackgroundResource(R.drawable.bg_btn_reward_check_normal);
        }
    }


    private void updateGradeLayout(RewardPansListEntity.RewardPan rewardPan) {
        pan = rewardPan.getPan();
        idLinChangeSel.setVisibility(View.VISIBLE);
        idDefaultSel.setVisibility(View.GONE);
        idUseCardReduceGrade.setText(rewardPan.getCardType());
        idUseCardRetainGrade.setText(rewardPan.getPanShowNumber());
        updateNextBtnStatus();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_rewardregister_detail;
    }

    @Override
    protected RewardRegisterContract.Presenter createPresenter() {
        return new RewardRegisterPresenter();
    }


    @Override
    public void rewardRegisterSuccess(RewardRegisterEntity response) {
        HashMap<String, Object> maps = new HashMap<>();
        maps.put(REWARDNUMBERINFO, response.getRefId());
        maps.put(REMARK, getIntent().getExtras().getString(REMARK));
        ActivitySkipUtil.startAnotherActivity(RewardRegisterDetailActivity.this, RewardRegisterSuccessActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void rewardRegisterError(String errorCode, String errorMsg) {

        backToList(errorCode, errorMsg);
    }

    @Override
    public void rewardCardListSuccess(RewardPansListEntity response) {

        showCardGradeDetailSelPop(response.getCardList());
    }

    @Override
    public void rewardCardListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void rewardCheckStatusSuccess(CheckRegistrationStatus response) {
        if (!TextUtils.isEmpty(response.getValidRegisterCode()) && TextUtils.equals(response.getValidRegisterCode(), "2")) {
            HashMap<String, Object> maps = new HashMap<>();
            maps.put(REWARDNUMBER, response.getRefId());
            ActivitySkipUtil.startAnotherActivity(RewardRegisterDetailActivity.this, RewardRegisterCheckSuccessActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            HashMap<String, Object> maps = new HashMap<>();
            ActivitySkipUtil.startAnotherActivity(RewardRegisterDetailActivity.this, RewardRegisterCheckFailedActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void rewardCheckStatusError(String errorCode, String errorMsg) {
        backToList(errorCode, errorMsg);
    }

    private void backToList(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                //EWA3082\3083\3084\3087 跳转到活动列表页面
                if (TextUtils.equals(errorCode, "EWA3082") || TextUtils.equals(errorCode, "EWA3083") || TextUtils.equals(errorCode, "EWA3084") || TextUtils.equals(errorCode, "EWA3087")) {
                    //登记其他活动
                    EventBus.getDefault().postSticky(new RewardRegisterEventEntity(RewardRegisterEventEntity.EVENT_BACK_REWARD_LIST, ""));
                    finish();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
