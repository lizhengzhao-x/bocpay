package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 设置FIO时，校验账户返回对象
 */

public class FioCheckEntity extends BaseEntity {
    //expDate
    String walletId;
    //卡号
    String PAN;
    //过期时间
    String expDate;

    //银行卡类型.1：信用卡 2：我的账户
    String accountType;

    //账户
    String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }


    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public void setPAN(String PAN) {
        this.PAN = PAN;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getWalletId() {
        return walletId;
    }

    public String getPAN() {
        return PAN;
    }

    public String getExpDate() {
        return expDate;
    }


    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
