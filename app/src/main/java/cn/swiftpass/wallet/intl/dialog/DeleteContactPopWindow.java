package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * 删除联系人
 */

public class DeleteContactPopWindow extends BasePopWindow implements View.OnClickListener {

    private TextView mId_delete_contact;

    private OnPopWindowClickListener mOnPopClickListener;

    public DeleteContactPopWindow(Activity mActivity, OnPopWindowClickListener onPopWindowOkClickListener) {
        super(mActivity);
        this.mOnPopClickListener = onPopWindowOkClickListener;
        initView();
    }


    @Override
    protected void initView() {
        setContentView(R.layout.pop_delete_contact);
        this.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        mId_delete_contact = mContentView.findViewById(R.id.id_delete_contact);
        mId_delete_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnPopClickListener != null) {
                    dismiss();
                    mOnPopClickListener.onDeleteClickListener();
                }
            }
        });
        setDisMissView(mContentView);
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnPopWindowClickListener {
        void onDeleteClickListener();
    }
}
