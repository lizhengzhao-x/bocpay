package cn.swiftpass.wallet.intl.app.constants;

/**
 * Created by ramon on 2018/8/24.
 */

public interface SmartAccountConst {
    String DATA_SMART_ACCOUNT_INFO = "REGISTER ENTITY";

    //充值方式
    String TOPUP_METHOD_DIRECT = "TOP";
    String TOPUP_METHOD_MANUAL = "MTOP";
    String TOPUP_METHOD_AUTO = "ATOP";

    String TO_BIND_SMART_ACCOUNT = "bind_smart_account";

    String CARD_TYPE_CREDIT = "1";
    String CARD_TYPE_SMART_ACCOUNT = "2";
    String CARD_TYPE_SMART_ACCOUNT_THREE = "3";

    String SMART_ACCOUNT_EXIST = "0";
    String SMART_ACCOUNT_EXIST_NOT = "EWA3019";
    String SMART_ACCOUNT_EXIST_NOT_TWO = "EWA3207";
    String SMART_ACCOUNT_UPDATE_STATUS = "EWA3211";
    String SMART_ACCOUNT_UPDATE_STATUS_TWO = "EWA3212";
    String SMART_ACCOUNT_UNBIND_FPS = "EWA3026";
    String SMART_ACCOUNT_TRANFER_NOT_IN_TIME = "EWA3303";//跨境汇款之服务时间已过。请于上午7:00至下午9:30之间登入此服务
    String SMART_ACCOUNT_TRANFER_FEE_UPDATE = "EWA3304";//跨境汇款汇率更新

    String SMART_ACCOUNT = "My Account";


    //A:修改主账号，L:修改限额，M:增值方式
    //S:暂停我的账户 R:重新激活 C:注销我的账户
    String UPDATE_SMARTACCOUNT_PRIMARY_ACCOUNT = "A";
    //    String UPDATE_SMARTACCOUNT_DAILY_LIMIT = "L";
    String UPDATE_SMARTACCOUNT_TOP_UP = "M";
    String UPDATE_SMARTACCOUNT_SUSPEND = "S";
    String UPDATE_SMARTACCOUNT_ACTIVATE_ACCOUNT = "R";
    String UPDATE_SMARTACCOUNT_CANCEL = "C";

}
