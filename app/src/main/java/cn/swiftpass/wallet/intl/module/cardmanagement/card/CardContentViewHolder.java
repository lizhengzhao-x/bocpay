package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


public class CardContentViewHolder extends CardBaseViewHolder {

    public final LinearLayout accountBalanceLayout;
    public final LinearLayout accountItemLayout;
    public final LinearLayout topAccountLayout;
    public final LinearLayout withdrawalLayout;

    public final LinearLayout updateAccountLayout;
    public final LinearLayout virtualCardManagerLayout;
    public final LinearLayout accountManagerLayout;
    public final ImageView toggleSeeIv;
    public final ImageView limitBalanceIv;
    public final TextView amountTipTv;
    public final TextView amountTv;
    public final TextView amountManagerTv;
    public final TextView amountUsableTv;
    public final TextView amountUsableTipTv;

    public CardContentViewHolder(View itemView) {
        super(itemView);
        accountBalanceLayout = itemView.findViewById(R.id.ll_account_balance);
        accountItemLayout = itemView.findViewById(R.id.ll_account_item);
        topAccountLayout = itemView.findViewById(R.id.ll_topUpAccount);
        withdrawalLayout = itemView.findViewById(R.id.ll_withdrawal);

        updateAccountLayout = itemView.findViewById(R.id.ll_update_account);
        virtualCardManagerLayout = itemView.findViewById(R.id.ll_virtual_card_manager);
        accountManagerLayout = itemView.findViewById(R.id.ll_account_manager);
        toggleSeeIv = itemView.findViewById(R.id.iv_toggle_see);
        limitBalanceIv = itemView.findViewById(R.id.iv_set_daily_limit_balance);
        amountTipTv = itemView.findViewById(R.id.tv_amount_tip);
        amountTv = itemView.findViewById(R.id.tv_amount);
        amountManagerTv = itemView.findViewById(R.id.tv_account_manager);
        amountUsableTv = itemView.findViewById(R.id.tv_amount_usable);
        amountUsableTipTv = itemView.findViewById(R.id.tv__amount_usable_tip);
        itemView.findViewById(R.id.ll_dont_touch_click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }
}
