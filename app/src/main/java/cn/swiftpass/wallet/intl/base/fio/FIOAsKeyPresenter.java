package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOAsKeyPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 18:11
 * @change
 * @chang time
 * @class 获取服务器的FIO Askey
 */
public interface FIOAsKeyPresenter<V extends IView> {
    void onFioGetAsKey(String walletId, String regType);
}
