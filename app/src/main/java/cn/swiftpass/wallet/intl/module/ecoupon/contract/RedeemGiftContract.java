package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * 换领电子券
 */
public class RedeemGiftContract {

    public interface View extends IView {
        void redeemGiftSuccess(RedeemResponseEntity response);

        void redeemGiftError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<RedeemGiftContract.View> {
        /**
         * 电子券换领最后一步
         *
         * @param ecouponConvertEntity
         */
        void redeemGift(EcouponConvertEntity ecouponConvertEntity);
    }

}
