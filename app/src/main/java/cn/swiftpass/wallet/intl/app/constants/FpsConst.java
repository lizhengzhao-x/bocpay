package cn.swiftpass.wallet.intl.app.constants;

/**
 * Created by ramon on 2018/10/15.
 */

public interface FpsConst {
    //fps
    String FPS_ACCOUNT_ID = "accountId";
    String FPS_ACCOUNT_ID_TYPE = "accountIdType";
    String FPS_DEFAULT_BANK = "defaultBank";
    String FPS_ACCOUNT_NO = "accountNo";
    String FPS_BANK_CODE = "bankCode";
    String FPS_ACTION = "action";
    String FPS_REF_NO = "fppRefNo";
    String FPS_OTP = "otp";
    String FPS_REPEAT_OTP = "repeatOtp";

    String FPS_RECORD_DEL_TYPE = "deleteType";
    String FPS_RECORD_DEL_TYPE_ADD = "AD";
    String FPS_RECORD_DEL_TYPE_OTHER = "OD";

    String FPS_ACTION_ADD = "A";
    String FPS_ACTION_DEL = "D";
    String FPS_ACTION_MODIFY = "U";
    String FPS_ACTION_QUERY = "E";

    String ACCOUNT_ID_TYPE_MOBILE = "0";
    String ACCOUNT_ID_TYPE_EMAIL = "1";

    String FPS_PRE_CHECK_ENTITY_ADD = "fps_add_pre_check";
    String FPS_PRE_CHECK_ENTITY_DEL = "fps_del_pre_check";

    String FPS_ADD_DATA_BANK = "fps_bank_data";

    String FPS_OTHER_BANK_RECORDS = "other_bank_records";
    String FPS_DEL_RECORDS = "del_bank_records";

    String FPS_DEFAULT_NO = "N";
    String FPS_DEFAULT_YES = "Y";

    String FPS_SEND_OTP_DEF = "0";
    String FPS_SEND_OTP_AGAIN = "1";

    String FPS_OTP_DISPLAY_RESEND = "1";
}
