package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.TaxResidentEntity;
import cn.swiftpass.wallet.intl.module.register.adapter.OnClickTaxResidenceJurisdictionListener;
import cn.swiftpass.wallet.intl.module.register.adapter.TaxCountryCodeAdapter;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

public class SelectTaxResidenceJurisdictionCodeActivity extends BaseCompatActivity implements OnClickTaxResidenceJurisdictionListener {


    @BindView(R.id.ry_tax_country)
    RecyclerView ryTaxCountry;
    private int index;
    //传递过来的，已经选中的税务地区，可以为空
    private TaxResidentEntity currentCountryData;
    private ArrayList<TaxResidentEntity> taxCountryList;
    //当前UI操作中，选中的税务地区，如果为空或者等于currentCountryData 时，不能点击完成按钮
    private TaxResidentEntity selectCountryData;
    private TextView doneTv;

    @Override
    protected int getLayoutId() {
        return R.layout.act_select_tax_country_code;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    protected void init() {
        if (getIntent() != null) {
            if (getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_COUNTRY) != null) {
                currentCountryData = (TaxResidentEntity) getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_COUNTRY);
            }
            index = getIntent().getIntExtra(RegisterTaxListActivity.DATA_TAX_INDEX, -1);
            if (getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_TAX_COUNTRY_LIST) != null) {
                taxCountryList = (ArrayList<TaxResidentEntity>) getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_TAX_COUNTRY_LIST);
            }
        }

        setToolBarTitle(R.string.PA2109_1_4);
        doneTv = setToolBarRightViewToText(R.string.btn_finish);
        getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (!checkTaxResidence()) {
                    Intent intent = new Intent();
                    intent.putExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_COUNTRY, selectCountryData);
                    intent.putExtra(RegisterTaxListActivity.DATA_TAX_INDEX, index);
                    setResult(RESULT_OK, intent);
                    finish();
                } else {
                    showErrorMsgDialog(SelectTaxResidenceJurisdictionCodeActivity.this, getString(R.string.PA2109_1_18));
                }

            }
        });

        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ryTaxCountry.setLayoutManager(layout);
        TaxCountryCodeAdapter adapter = new TaxCountryCodeAdapter(this, taxCountryList, this);
        if (currentCountryData != null) {
            adapter.setCurrentCountry(currentCountryData);
        }
        ryTaxCountry.setAdapter(adapter);
        getToolBarRightView().setEnabled(false);
        doneTv.setTextColor(getColor(R.color.gray_two));
    }

    private boolean checkTaxResidence() {
        return selectCountryData.isSelectStatus();
    }


    private void updateBtn(TaxResidentEntity selectCountryData) {
        if (currentCountryData != null && currentCountryData.getResidenceJurisdiction().equals(selectCountryData.getResidenceJurisdiction())) {
            //选中了和进来时相同的条目
            getToolBarRightView().setEnabled(false);
            doneTv.setTextColor(getColor(R.color.gray_two));
        } else {
            getToolBarRightView().setEnabled(true);
            doneTv.setTextColor(getColor(R.color.app_white));
        }

    }

    @Override
    public void OnClickTaxResidenceJurisdiction(TaxResidentEntity selectCountryData) {
        this.selectCountryData = selectCountryData;
        updateBtn(selectCountryData);

    }
}