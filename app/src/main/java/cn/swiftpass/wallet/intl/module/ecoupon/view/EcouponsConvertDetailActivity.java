package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionDetailContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.ActRedmIdEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponConvertEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.RedemptionDetailPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcouponsDetailVerticalAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 积分兑换详情
 */
public class EcouponsConvertDetailActivity extends BaseCompatActivity<RedemPtionDetailContract.Presenter> implements RedemPtionDetailContract.View {
    /**
     * 此用户是否能参与优化
     */
    public boolean isDiscount = false;
    @BindView(R.id.id_sel_ecoup_recycleview)
    RecyclerView idRecyclerview;
    @BindView(R.id.id_need_grade_detail)
    TextView idNeedGradeDetail;
    @BindView(R.id.id_need_grade_view)
    LinearLayout idNeedGradeView;
    @BindView(R.id.id_grade_lack)
    TextView idGradeLack;
    @BindView(R.id.id_grade_desc)
    TextView idGradeDesc;
    @BindView(R.id.tv_next_step)
    TextView tvNextStep;
    @BindView(R.id.id_available_year)
    TextView idAvailableYear;
    @BindView(R.id.id_card_title)
    TextView idCardTitle;
    @BindView(R.id.id_card_number)
    TextView idCardNumber;
    @BindView(R.id.id_card_grade)
    TextView idCardGrade;
    @BindView(R.id.id_right_icon)
    ImageView idRightIcon;
    @BindView(R.id.id_grade_layout)
    RelativeLayout idGradeLayout;
    @BindView(R.id.id_card_expiredata)
    TextView idCardExpiredata;
    @BindView(R.id.id_sel_left_text)
    TextView idSelLeftText;
    @BindView(R.id.id_grade_title)
    TextView id_grade_title;
    @BindView(R.id.id_change_credit)
    View idChangeCreditCard;
    private List<RedeemableGiftListEntity.EVoucherListBean> mECoupItems;
    private List<SmaGpInfoBean> smaGpInfoBeans;
    private SmaGpInfoBean currentSelsmaGpInfo;
    private CreditCardGradeEntity creditCardGradeEntity;
    /**
     * 当前卡选择位置
     */
    private int currentCardSelPosition;
    /**
     * 电子券唯一标识
     */
    private String redmId;
    /**
     * gift类型
     */
    private String giftTp;
    private int needGradeCnt;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC05_0));
        smaGpInfoBeans = new ArrayList<>();
        EventBus.getDefault().register(this);
        mECoupItems = (List<RedeemableGiftListEntity.EVoucherListBean>) getIntent().getExtras().getSerializable(Constants.EVOUCHERLISTBEANLIST);
        redmId = getIntent().getExtras().getString(Constants.REDMID);
        giftTp = getIntent().getExtras().getString(Constants.GIFTTP);
        isDiscount = getIntent().getExtras().getBoolean(Constants.USEDISCOUNT);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        ((IAdapter<RedeemableGiftListEntity.EVoucherListBean>) idRecyclerview.getAdapter()).setData(mECoupItems);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        tvNextStep.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                getRedeemIdRequest();
            }
        });
        currentCardSelPosition = 0;
        idChangeCreditCard.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                showCardGradeDetailSelPop();
            }
        });
        mPresenter.getUserTotalGrade();
        updateOkBackground(tvNextStep, false);
    }

    private void getRedeemIdRequest() {
        if (currentSelsmaGpInfo == null) return;
        EcouponConvertEntity ecouponConvertEntityIn = new EcouponConvertEntity();
        ecouponConvertEntityIn.setGiftTp(giftTp);
        ecouponConvertEntityIn.setPan(currentSelsmaGpInfo.getRealPan());
        ecouponConvertEntityIn.setRedeemPoint(needGradeCnt + "");
        List<EcouponConvertEntity.RedeemItem> redeemItems = new ArrayList<>();
        for (int i = 0; i < mECoupItems.size(); i++) {
            RedeemableGiftListEntity.EVoucherListBean eVoucherListBean = mECoupItems.get(i);
            EcouponConvertEntity.RedeemItem redeemItem = new EcouponConvertEntity.RedeemItem();
            redeemItem.setGiftCode(eVoucherListBean.getGiftCode());
            redeemItem.seteVoucherQty(eVoucherListBean.getCurrentSelCnt() + "");
            redeemItem.setAswItemCode(eVoucherListBean.getAswItemCode());
            redeemItem.setOrderTp(eVoucherListBean.getOrderTp());
            if (!TextUtils.isEmpty(redeemItem.geteVoucherQty()) && Integer.valueOf(redeemItem.geteVoucherQty()) > 0) {
                redeemItems.add(redeemItem);
            }
        }
        ecouponConvertEntityIn.setRedeemItems(redeemItems);
        mPresenter.actRedmId(ecouponConvertEntityIn, giftTp);
    }


    /**
     * 切换信用卡 使用不同卡兑换
     */
    private void showCardGradeDetailSelPop() {
        if (smaGpInfoBeans == null || smaGpInfoBeans.size() == 0) {
            mPresenter.getUserTotalGrade();
            return;
        }
        CheckAndSelGradeListPop checkGradeListPop = new CheckAndSelGradeListPop(this, smaGpInfoBeans, currentCardSelPosition, new CheckAndSelGradeListPop.OnGradeWindowClickListener() {
            @Override
            public void onGradeItemClickListener(int position) {
                currentCardSelPosition = position;
                updateGradeLayout(position);
            }
        });
        checkGradeListPop.show();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EcoupEventEntity event) {
        if (event.getEventType() == EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP||event.getEventType() == EcoupEventEntity.EVENT_CAME_MY_ECOUP) {
            finish();
        } else if (event.getEventType() == EcoupEventEntity.EVENT_RETRY_COUNT) {
            //库存不足 需要重新获取库存数量 更新库存
            redeemGiftCheckStock();
        }
    }

    /**
     * 更改选择卡信息
     *
     * @param position
     */
    private void updateGradeLayout(int position) {
        if (smaGpInfoBeans == null || smaGpInfoBeans.size() == 0) return;
        currentSelsmaGpInfo = smaGpInfoBeans.get(position);
        String year = mContext.getString(R.string.EC03_2) + creditCardGradeEntity.getYearHolding();
        if (TextUtils.isEmpty(creditCardGradeEntity.getYearHolding())) {
            idAvailableYear.setVisibility(View.GONE);
        } else {
            idAvailableYear.setText(year);
            idAvailableYear.setVisibility(View.VISIBLE);
        }

        idCardTitle.setText(currentSelsmaGpInfo.getName());
        idCardNumber.setText(currentSelsmaGpInfo.getPan());

        idCardGrade.setText(AndroidUtils.formatPrice(Double.valueOf(currentSelsmaGpInfo.getBal()), false));
        if (!TextUtils.isEmpty(currentSelsmaGpInfo.getExpiryDate())) {
            idCardExpiredata.setText(getString(R.string.EC04_1_a) + currentSelsmaGpInfo.getExpiryDate());
        }
        updateTotalNeedGrade();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoupon_convert_detail;
    }

    @Override
    protected RedemPtionDetailContract.Presenter createPresenter() {
        return new RedemptionDetailPresenter();
    }


    private CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean> getAdapter(final List<RedeemableGiftListEntity.EVoucherListBean> data) {
        return new CommonRcvAdapter<RedeemableGiftListEntity.EVoucherListBean>(data) {

            @Override
            public Object getItemType(RedeemableGiftListEntity.EVoucherListBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {

                return new EcouponsDetailVerticalAdapter(mContext, isDiscount, new EcouponSelChangedListener() {
                    @Override
                    public void onItemEcouponCountChanged(int count, int positon) {
                        mECoupItems.get(positon).setCurrentSelCnt(count);
                        idRecyclerview.getAdapter().notifyDataSetChanged();
                        updateTotalNeedGrade();
                    }

                    @Override
                    public void onItemCheckMore(int position) {

                    }

                    @Override
                    public void onReachMaxValue() {
                        showErrorMsgDialog(getActivity(), getString(R.string.EC03_4));
                    }

                    @Override
                    public void onShowGradeInFo() {

                    }

                    @Override
                    public void onEditTextValue(int positon) {
                        editValueWithDialog(positon);
                    }


                });

            }
        };
    }


    private void editValueWithDialog(final int position) {
        int defaultStr = mECoupItems.get(position).getCurrentSelCnt() > 0 ? mECoupItems.get(position).getCurrentSelCnt() : -1;
        final EcouponEditCntDialog ecouponEditCntDialog = new EcouponEditCntDialog(this, defaultStr, 50, new EcouponEditCntDialog.OnValueChangedListener() {
            @Override
            public void onValueChanged(int value) {
                if (value == -1) return;
                RedeemableGiftListEntity.EVoucherListBean eVoucherListBean = mECoupItems.get(position);
                if (!TextUtils.isEmpty(eVoucherListBean.geteVoucherStock()) && Integer.valueOf(eVoucherListBean.geteVoucherStock()) > value) {
                    mECoupItems.get(position).setCurrentSelCnt(value);
                } else {
                    mECoupItems.get(position).setCurrentSelCnt(Integer.valueOf(eVoucherListBean.geteVoucherStock()));
                }
                idRecyclerview.getAdapter().notifyDataSetChanged();
                updateTotalNeedGrade();
            }
        });
        ecouponEditCntDialog.show();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ecouponEditCntDialog.showKeyboard();
            }
        }, 100);
    }

    /**
     * 计算总积分
     */
    private void updateTotalNeedGrade() {
        //兑换的总数是否大于0
        boolean isLargerZero = false;
        needGradeCnt = 0;
        for (int i = 0; i < mECoupItems.size(); i++) {
            if (mECoupItems.get(i).getCurrentSelCnt() > 0) {
                isLargerZero = true;
            }
            if (isDiscount) {
                needGradeCnt = needGradeCnt + mECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mECoupItems.get(i).getEVoucherUnitPrice());
            } else {
                needGradeCnt = needGradeCnt + mECoupItems.get(i).getCurrentSelCnt() * Integer.valueOf(mECoupItems.get(i).getEVoucherOriginalPrice());
            }
        }
        idNeedGradeDetail.setText(AndroidUtils.formatPrice(Double.valueOf(needGradeCnt), false) + "");
        if (creditCardGradeEntity != null && !TextUtils.isEmpty(creditCardGradeEntity.getSumGp())) {
            if (needGradeCnt > Integer.valueOf(creditCardGradeEntity.getSumGp())) {
                String text = getString(R.string.EC05_4).replace("xx", AndroidUtils.formatPrice(Double.valueOf(creditCardGradeEntity.getSumGp()), false) + "");
                idGradeLack.setText(text);
                idGradeLack.setTextColor(Color.parseColor("#fc4949"));
                updateOkBackground(tvNextStep, false);
            } else {
                updateOkBackground(tvNextStep, true);
                idGradeLack.setText(getString(R.string.EC03_1) + " " + AndroidUtils.formatPrice(Double.valueOf(creditCardGradeEntity.getSumGp()), false));
                idGradeLack.setTextColor(Color.parseColor("#424242"));
                //兑换的总数是否大于0
                updateOkBackground(tvNextStep, isLargerZero);
            }
        } else {
            //卡片信息拉取失败
            updateOkBackground(tvNextStep, false);
            if (creditCardGradeEntity != null) {
                idGradeLack.setText(getString(R.string.EC03_1) + " " + AndroidUtils.formatPrice(Double.valueOf(creditCardGradeEntity.getSumGp()), false));
                idGradeLack.setTextColor(Color.parseColor("#424242"));
            }
        }

    }


    @Override
    public void actRedmIdSuccess(ActRedmIdEntity response) {
        if (currentSelsmaGpInfo == null) return;
        List<RedeemableGiftListEntity.EVoucherListBean> mECoupItemsFilter = new ArrayList<>();
        for (int i = 0; i < mECoupItems.size(); i++) {
            RedeemableGiftListEntity.EVoucherListBean eVoucherListBean = mECoupItems.get(i);
            if (eVoucherListBean.getCurrentSelCnt() > 0) {
                mECoupItemsFilter.add(eVoucherListBean);
            }
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.ECOUPONCONVERTENTITY, mECoupItemsFilter);
        mHashMaps.put(Constants.GIFTTP, response.getGiftTp());
        mHashMaps.put(Constants.REDMID, response.getRedmId());
        mHashMaps.put(Constants.PAN_ID, currentSelsmaGpInfo.getRealPan() + "");
        mHashMaps.put(Constants.PAN_ID_HIDE, currentSelsmaGpInfo.getPan() + "");
        mHashMaps.put(Constants.PAN_TYPE, currentSelsmaGpInfo.getName() + "");
        String totalGrade = (idNeedGradeDetail.getText() + "").replace(",", "");
        mHashMaps.put(Constants.TOTAL_GRADE, totalGrade);
        try {
            int retainGp = Integer.valueOf(creditCardGradeEntity.getSumGp()) - Integer.valueOf(totalGrade);
            mHashMaps.put(Constants.RETAIN_GRADE, retainGp + "");
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        ActivitySkipUtil.startAnotherActivity(this, EcouponsConvertDescripActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void actRedmIdError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void checkCCardSuccess(CheckCcEntity response) {
        //更新库存的操作
        List<CheckCcEntity.EVoucherNosBean> eVoucherNos = response.getEVoucherNos();
        for (int i = 0; i < eVoucherNos.size(); i++) {
            CheckCcEntity.EVoucherNosBean eVoucherNosBean = eVoucherNos.get(i);
            for (int j = 0; j < mECoupItems.size(); j++) {
                if (mECoupItems.get(j).getAswItemCode().equals(eVoucherNosBean.getAswItemCode())) {
                    mECoupItems.get(j).seteVoucherStock(eVoucherNosBean.getEVoucherStock());
                }
            }
        }
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }

    private void redeemGiftCheckStock() {
        //查询库存
        List<RedeemResponseEntity.ResultRedmBean> mRedmeList = new ArrayList<>();
        for (int i = 0; i < mECoupItems.size(); i++) {
            RedeemResponseEntity.ResultRedmBean resultRedmBean = new RedeemResponseEntity.ResultRedmBean();
            resultRedmBean.setAswItemCode(mECoupItems.get(i).getAswItemCode());
            resultRedmBean.setEVoucherQty(mECoupItems.get(i).getCurrentSelCnt() + "");
            resultRedmBean.setGiftCode(mECoupItems.get(i).getGiftCode());
            resultRedmBean.setOrderTp(mECoupItems.get(i).getOrderTp());
            mRedmeList.add(resultRedmBean);
        }
        mPresenter.redeemGiftCheckStock(giftTp, mRedmeList);
    }


    @Override
    public void checkCCardError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void showTotalGradeFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
        id_grade_title.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTotalGradeSuccess(CreditCardGradeEntity response) {
        id_grade_title.setVisibility(View.VISIBLE);
        creditCardGradeEntity = response;
        smaGpInfoBeans.addAll(response.getCcGpInfo());
        int defaultSel = 0;
        for (int i = 0; i < smaGpInfoBeans.size(); i++) {
            if (!TextUtils.isEmpty(smaGpInfoBeans.get(i).getBal()) && Double.valueOf(smaGpInfoBeans.get(i).getBal()) > 0) {
                defaultSel = i;
                currentCardSelPosition = defaultSel;
                break;
            }
        }
        smaGpInfoBeans.get(defaultSel).setSel(true);
        updateGradeLayout(defaultSel);
        updateTotalNeedGrade();
    }


}
