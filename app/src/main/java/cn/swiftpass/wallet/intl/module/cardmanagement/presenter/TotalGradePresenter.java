package cn.swiftpass.wallet.intl.module.cardmanagement.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.ActionQueryGpInfoProtocol;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.TotalCreditCardGradeContract;

/**
 * Created by ZhangXinchao on 2019/7/3.
 */
public class TotalGradePresenter extends BasePresenter<TotalCreditCardGradeContract.View> implements TotalCreditCardGradeContract.Presenter {


    @Override
    public void getTotalGradeInfo() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }

        new ActionQueryGpInfoProtocol(null, new NetWorkCallbackListener<CreditCardGradeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTotalGradeInfoError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(CreditCardGradeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getTotalGradeInfoSuccess(response);
                }
            }
        }).execute();

    }
}
