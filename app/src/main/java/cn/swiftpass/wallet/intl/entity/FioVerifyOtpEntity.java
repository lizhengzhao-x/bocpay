package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/5/22.
 * FIO 注册第二步返回
 */

public class FioVerifyOtpEntity extends BaseEntity {


    public String getFailCnt() {
        return failCnt;
    }

    public void setFailCnt(String failCnt) {
        this.failCnt = failCnt;
    }

    //错误次数
    String failCnt;

}
