package cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import java.util.List;

public class CreditCardRewardAdapter extends FragmentStateAdapter {
    private final List<Fragment> fragments;

    public CreditCardRewardAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, List<Fragment> fragments) {
        super(fragmentManager, lifecycle);
        this.fragments = fragments;
    }


    @NonNull
    @Override
    public Fragment createFragment(int position) {
        return fragments.get(position);
    }

    @Override
    public int getItemCount() {
        return fragments != null ? fragments.size() : 0;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }
}
