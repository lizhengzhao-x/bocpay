package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;


/**
 * 利是 一级页面
 */
public class RedPacketMainFragment extends BaseFragment implements UpdateTvTabDot {
    private static final String TAG = "TransferSelTypeActivity";
    @BindView(R.id.tbl_tabLayout)
    TabLayout tblTabLayout;
    @BindView(R.id.vwp_viewpager)
    ViewPager vwpViewpager;

    private ArrayList<Fragment> mFragments;
    private List<String> tabNames;

    List<InviteContactEntity> allContactList = new ArrayList<>();
    private PaleyShiFragment paleyShiFragment;
    private GetLiShiFragment getLiShiFragment;
    TextView tvTabDot;
    TextView tvContent;
    //默认tab选中派利是页面
    private int tabDefaultPosition = PALEY_SHI_TAB;

    //派利是页面
    public static final int PALEY_SHI_TAB = 0;
    //收利是页面
    public static final int GET_LI_SHI_TAB = 1;

    public static RedPacketMainFragment newInstance(int tabDefaultPosition) {
        RedPacketMainFragment redPacketMainFragment = new RedPacketMainFragment();
        redPacketMainFragment.tabDefaultPosition = tabDefaultPosition;
        return redPacketMainFragment;
    }


    @Override
    protected boolean isResetTitleByInit() {
        return true;
    }


    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(getString(R.string.RP2101_9_1));
            mActivity.setToolBarRightViewToText("");
        }
    }

    @Override
    protected void initView(View v) {
        checkPermission(getActivity());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //每次退出页面 清除通讯录
        ContactCompareUtils.clearAllContactList();
    }

    private void bindData() {
        //清除通讯录 每次进入这个页面都是最新的通讯录
        ContactCompareUtils.clearAllContactList();
        tabNames = new ArrayList<>();
        tabNames.add(getString(R.string.RP2101_10_2));
        tabNames.add(getString(R.string.RP2101_10_3));

        tblTabLayout.addTab(tblTabLayout.newTab().setText(tabNames.get(0)));
        tblTabLayout.addTab(tblTabLayout.newTab());

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_red_packet_tab, tblTabLayout, false);
        tvTabDot = view.findViewById(R.id.tv_main_tab_dot);
        tvContent = view.findViewById(R.id.tv_content);
        tvContent.setText(tabNames.get(1));

        tblTabLayout.getTabAt(1).setCustomView(view);

        if (1 == tabDefaultPosition) {
            getLiShiFragment = GetLiShiFragment.newInstance(this, true);
            paleyShiFragment = PaleyShiFragment.newInstance(this, false);
        } else {
            paleyShiFragment = PaleyShiFragment.newInstance(this);
            getLiShiFragment = GetLiShiFragment.newInstance(this);
        }

        mFragments = new ArrayList<>();
        mFragments.add(paleyShiFragment);
        mFragments.add(getLiShiFragment);

        vwpViewpager.setOffscreenPageLimit(mFragments.size());
        tblTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tblTabLayout.getSelectedTabPosition();
                if (0 == position) {
                    if (null != paleyShiFragment) {
                        paleyShiFragment.tabClickFresh();
                    }
                } else {
                    if (null != getLiShiFragment) {
                        getLiShiFragment.tabClickFresh();
                    }
                }

                setTabTextColor(position);
                vwpViewpager.setCurrentItem(tblTabLayout.getSelectedTabPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        vwpViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                tblTabLayout.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        vwpViewpager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Nullable
            @Override
            public CharSequence getPageTitle(int position) {
                return tabNames.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });

        if (0 != tabDefaultPosition) {
            vwpViewpager.setCurrentItem(tabDefaultPosition);
        }
        setTabTextColor(tabDefaultPosition);
    }

    /**
     * 设置自定义tab 需要手动设置tab的color
     *
     * @param currentTab
     */
    private void setTabTextColor(int currentTab) {
        Activity mActivity = getActivity();
        if (null == mActivity) {
            return;
        }

        if (1 == currentTab) {
            tvContent.setTextColor(mActivity.getColor(R.color.black));
        } else {
            tvContent.setTextColor(mActivity.getColor(R.color.color_FF9B9B9B));
        }
    }

    /**
     * 获取通讯录权限后才初始化数据
     * 如果拒绝通讯录权限分为下面两种情况
     * 1拒绝 初始化数据
     * 2不在询问 再一次进入会弹出dialog 这里会从通过startActivityForResult来得到从系统页面的结果
     *
     * @param mActivity
     */
    protected void checkPermission(final Activity mActivity) {
        if (mActivity == null) {
            return;
        }
        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                //拒绝并勾选不再询问
                PermissionInstance.getInstance().setHasFirstRejectContacts(false);
            } else {
                PermissionInstance.getInstance().setHasFirstRejectContacts(true);
            }
        }

        PermissionInstance.getInstance().getPermission(mActivity, new PermissionInstance.PermissionCallback() {
            @Override
            public void acceptPermission() {
                bindData();
            }

            @Override
            public void rejectPermission() {
                if (!ActivityCompat.shouldShowRequestPermissionRationale(mActivity, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                    if (!PermissionInstance.getInstance().isHasFirstRejectContacts()) {
                        showLackOfPermissionDialog();
                    } else {
                        PermissionInstance.getInstance().setHasFirstRejectContacts(false);
                        bindData();
                    }
                } else {
                    bindData();
                }
            }
        }, Manifest.permission.READ_CONTACTS);
    }

    public void showLackOfPermissionDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(mContext);
        builder.setTitle(getString(R.string.STF2101_1_1));
        builder.setMessage(getString(R.string.STF2101_1_2));
        builder.setPositiveButton(getString(R.string.M10_1_25), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                AndroidUtils.startAppSettingForResult(mContext, RedPacketMainFragment.this, Constants.REQUEST_ADDRESS_SET_CODE);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                bindData();
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);

        CustomDialog mDealDialog = builder.create();
        mDealDialog.setCancelable(false);
        mDealDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_ADDRESS_SET_CODE) {
            checkPermission(getActivity());
            return;
        }
    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_red_packet_main;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    public void changeTab(int tab) {
        if (null != vwpViewpager) {
            vwpViewpager.setCurrentItem(tab);
        }
    }


    @Override
    public void upDateTvTabDot(int number) {
        if (null == tvTabDot) {
            return;
        }
        String showDot = "";
        if (number <= 99) {
            showDot = number + "";
        } else {
            showDot = "99+";
        }
        //number大于0 才显示
        if (number > 0) {
            tvTabDot.setVisibility(View.VISIBLE);
            tvTabDot.setText(showDot);
        } else {
            tvTabDot.setVisibility(View.INVISIBLE);
        }
    }

}
