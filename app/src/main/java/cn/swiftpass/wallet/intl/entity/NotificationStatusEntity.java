package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class NotificationStatusEntity extends BaseEntity {

    public static final String NOTIFICATIONACTIONQUERY = "Q";
    public static final String NOTIFICATIONACTIONUPDATE = "U";

    public static final String NOTIFICATIONSTATUSOPEN = "1";
    public static final String NOTIFICATIONSTATUSCLOSE = "0";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 开关状态 0：关 1：开
     */
    private String status;

    public boolean isOpenStatus() {
        if (TextUtils.isEmpty(status)) {
            return true;
        } else {
            return TextUtils.equals(status, "1");
        }
    }
}
