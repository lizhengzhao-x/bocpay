package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/26 10:39
 * 主扫二维码-银联商户URL解析
 * 对应的返回对象为 MainScanCodeInfoEntity
 */

public class UpQRCodeUrlProtocol extends BaseProtocol {
    public static final String TAG = UpQRCodeUrlProtocol.class.getSimpleName();
    /**
     * qrcodeurl
     */
    String mUrlQrCode;

    public UpQRCodeUrlProtocol(String qrcodeUrl, NetWorkCallbackListener dataCallback) {
        this.mUrlQrCode = qrcodeUrl;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actionMpqrurlInfo";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.URLQRCODE, mUrlQrCode);
    }
}
