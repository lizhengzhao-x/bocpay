package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.text.SpannableString;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

public class CustomeLineText extends RelativeLayout {
    private Context mContext;

    public TextView getmTitleText() {
        return mTitleText;
    }

    private TextView mTitleText;

    public TextView getmContentText() {
        return mContentText;
    }

    private TextView mContentText;

    public CustomeLineText(Context context) {
        super(context);
        this.mContext = context;
        initViews();
    }

    public CustomeLineText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews();
    }

    public CustomeLineText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initViews();
    }

    public CustomeLineText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mContext = context;
        initViews();
    }


    private void initViews() {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.custome_line_text, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mTitleText = rootView.findViewById(R.id.text_title_tv);
        mContentText = rootView.findViewById(R.id.text_content_tv);
    }

    public void setContentText(String text) {
        this.mContentText.setText(text);
    }

    public void setContentText(SpannableString text) {
        this.mContentText.setText(text);
    }

    public void setTitleText(String text) {
        this.mTitleText.setText(text);
    }
}
