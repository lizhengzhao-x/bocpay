package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author ramon
 * create date on  on 2018/8/20
 * 转账查询余额接口
 */

public class TransferQueryBalanceProtocol extends BaseProtocol {
    public static final String TAG = TransferQueryBalanceProtocol.class.getSimpleName();


    public TransferQueryBalanceProtocol(NetWorkCallbackListener callback) {
        mDataCallback = callback;
        mUrl = "api/smartAccountManager/getAccountBalance";
    }

    @Override
    public void packData() {

    }
}
