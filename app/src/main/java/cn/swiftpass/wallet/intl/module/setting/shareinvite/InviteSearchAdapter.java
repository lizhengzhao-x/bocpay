package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.utils.SdkShareUtil;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

/**
 * @author shican
 */
public class InviteSearchAdapter extends BaseRecyclerAdapter<InviteContactEntity> {

    /**
     * 判断是否获取了状态
     */
    private boolean getStatus = false;

    public void setGetStatus(boolean mGetStatus) {
        this.getStatus = mGetStatus;
    }

    public boolean isGetStatus() {
        return getStatus;
    }

    public InviteSearchAdapter(@Nullable List<InviteContactEntity> data) {
        super(R.layout.item_invite_contact, data);
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, InviteContactEntity inviteContactEntity, int position) {
        baseViewHolder.setText(R.id.tv_user_name, inviteContactEntity.getName());
        baseViewHolder.setText(R.id.tv_phone, inviteContactEntity.getAccount());

        if (getStatus) {
            if ("0".equals(inviteContactEntity.getStatus())) {
                setItemStatus(baseViewHolder, false, true);
            } else {
                setItemStatus(baseViewHolder, true, false);
            }
        } else {
            setItemStatus(baseViewHolder, false, true);
        }

        baseViewHolder.addOnClickListener(R.id.iv_sms, R.id.iv_whatsapp);
    }

    private void setItemStatus(BaseViewHolder baseViewHolder, boolean showText, boolean showWhatApp) {
        baseViewHolder.setGone(R.id.tv_bocpay, !showText);

        if (showWhatApp) {
            //需求变动 用户安装whatsapp 才需要显示图标 否则不显示
            boolean isInstall = SdkShareUtil.isAppInstalled(mContext, SdkShareUtil.WHATS_APP);
            baseViewHolder.setVisible(R.id.iv_whatsapp, isInstall);
        } else {
            baseViewHolder.setGone(R.id.iv_whatsapp, true);
        }

        baseViewHolder.setVisible(R.id.iv_sms, showWhatApp);
    }
}
