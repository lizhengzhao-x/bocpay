package cn.swiftpass.wallet.intl.module.redpacket.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;

/**
 * 派利是
 */
public class PaleyShiContract {

    public interface View extends IView {
        void getPaleyShiHomeDataSuccess(PaleyShiHomeEntity response);

        void getPaleyShiHomeDataError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<PaleyShiContract.View> {
        void getPaleyShiHomeData();
    }

}
