package cn.swiftpass.wallet.intl.base;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Outline;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisBaseEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisImplManager;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.widget.CustomProgressDialog;


public abstract class BaseFragment<P extends IPresenter> extends Fragment implements IView {
    private static final String TAG = "BaseFragment";

    protected BaseCompatActivity mActivity;

    protected boolean mNeedRequestPermissions = true;

    protected static final int PERMISSON_REQUESTCODE = 0;

    protected P mPresenter;
    private CustomMsgDialog mDealDialog;
    private boolean isSave;
    private ViewGroup container;
    protected Handler mFmgHandler;
    protected long startTime = 0L;


    public abstract void initTitle();

    protected abstract P loadPresenter();

    private CustomProgressDialog progressDialog;


    protected Unbinder unbinder;

    protected Context mContext;
    protected int screenWidth, screenHeight;

    public ViewGroup getContainer() {
        return container;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        screenWidth = AndroidUtils.getScreenWidth(mContext);
        screenHeight = AndroidUtils.getScreenHeight(mContext);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity activity = getActivity();
        if (activity instanceof BaseCompatActivity) {
            mActivity = (BaseCompatActivity) activity;
        }
        if (isResetTitleByInit()) {
            resetTitle();
        }
        initTitle();

    }

    protected boolean isResetTitleByInit() {
        return true;
    }

    private void resetTitle() {
        if (mActivity != null) {
            mActivity.resetToolbar();
        }
    }

    public void showDialogNotCancel(Context context) {
        if (progressDialog == null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(context, context.getString(R.string.dialog_message));
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
            }
        }
        if (progressDialog != null) {
            try {
                Activity activity = (Activity) context;
                if (!activity.isFinishing()) {
                    progressDialog.setCancel(false);
                    progressDialog.show();
                }
            } catch (Exception e) {
            }

        }
    }

    @Override
    public void showDialogNotCancel() {
        if (progressDialog == null) {
            try {
                BaseAbstractActivity activity = (BaseAbstractActivity) this.getActivity();
                if (activity != null && !activity.isFinishing()) {
                    progressDialog = CustomProgressDialog.createDialog(activity, activity.getString(R.string.dialog_message));
                    progressDialog.setCancelable(false);
                    progressDialog.setCancel(false);
                    if (!activity.isFinishing()) {
                        progressDialog.show();
                    }
                }
            } catch (Exception e) {
            }
        }
        if (progressDialog != null) {
            try {
                Activity activity = getActivity();
                if (activity != null && !activity.isFinishing()) {
                    progressDialog.setCancel(false);
                    progressDialog.show();
                }
            } catch (Exception e) {
            }

        }
    }

    @Override
    public void dismissDialog() {
        if (progressDialog == null) {
            return;
        }
        progressDialog.dismiss();
        progressDialog = null;
    }

    public void sendAnalysisEvent(AnalysisBaseEntity analysisBaseEntity) {
        AnalysisImplManager.sendAnalysisEvent(analysisBaseEntity);
    }


    public void updateOkBackground(TextView tv, boolean isSel) {
        tv.setEnabled(isSel);
        //tv.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    protected void switchDiffFragmentContent(FragmentManager mFragmentManager, Fragment toFragment, Fragment mCurrentFragment, int resId, int index) {
        if (null == mCurrentFragment || null == toFragment) {
            return;
        }
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        if (!toFragment.isAdded()) {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.add(resId, toFragment, String.valueOf(index));
            fragmentTrasaction.show(toFragment);
            fragmentTrasaction.commitAllowingStateLoss();
        } else {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.show(toFragment);
            fragmentTrasaction.commitAllowingStateLoss();
        }
    }

    protected boolean useBackgroundRadius() {
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), null);
        if (!notUsedCustomBackground()) {
            view.setBackgroundResource(R.drawable.bg_base_view);
        }
        if (useBackgroundRadius()) {
            view.setOutlineProvider(new ViewOutlineProvider() {

                @Override
                public void getOutline(View view, Outline outline) {
                    Rect rect = new Rect();
                    outline.setRoundRect(rect, ScreenUtils.dip2px(20f));
                    int radius = ScreenUtils.dip2px(20);
                    outline.setRoundRect(0, 0, view.getWidth(), view.getHeight() + radius, radius);
                }
            });
            view.setClipToOutline(true);
        }
        this.container = container;
        unbinder = ButterKnife.bind(this, view);
        mPresenter = loadPresenter();
        if (mPresenter != null) {
            mPresenter.onAttachView(this);
        }
        mFmgHandler = new Handler();

        initView(view);
        if (getActivity() != null) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        startTime = System.currentTimeMillis();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {   // 切换到可见时;
            startTime = System.currentTimeMillis();
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg) {
        if (mContext == null) return;
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.POPUP1_1), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            if (mDealDialog == null || !mDealDialog.isShowing()) {
                mDealDialog = builder.create();
                mDealDialog.show();
            } else {
                // 不为空并且在展示
                if (TextUtils.isEmpty(mDealDialog.getMessage()) || !mDealDialog.getMessage().equals(msg)) {
                    mDealDialog = builder.create();
                    mDealDialog.show();
                }
            }
        }
    }

    public void showErrorMsgDialog(Context mContext, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (mContext == null) return;
        //TODO 这里需要做一个优化，防止fragment弹框 与 activity 报错弹框冲突
        try {
            Activity mActivity = (Activity) mContext;
            if (mActivity instanceof BaseAbstractActivity) {
                BaseAbstractActivity baseAbstractActivity = (BaseAbstractActivity) mActivity;
                HashMap<String, CustomMsgDialog> dialogHashMap = baseAbstractActivity.getDialogShowMsg();
                if (dialogHashMap != null && dialogHashMap.containsKey(msg)) {
                    return;
                }
            }
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }

        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setPositiveButton(mContext.getString(R.string.POPUP1_1), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            if (mDealDialog == null || !mDealDialog.isShowing()) {
//                LogUtils.i("TESTCLICK","parserLocalQrcodeSuccess3---->");
                mDealDialog = builder.create();
                mDealDialog.setCancelable(false);
                mDealDialog.show();
            } else {
                // 不为空并且在展示
                if (TextUtils.isEmpty(mDealDialog.getMessage()) || !mDealDialog.getMessage().equals(msg)) {
                    mDealDialog = builder.create();
                    mDealDialog.show();
                }
            }
        }
    }


    public boolean isGranted(String permission) {
        return !isMarshmallow() || isGranted_(permission);
    }

    private boolean isMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }

    public void checkPermissions(String... permissions) {
        List<String> needRequestPermissonList = findDeniedPermissions(permissions);
        if (null != needRequestPermissonList && needRequestPermissonList.size() > 0) {
            requestPermissions(needRequestPermissonList.toArray(new String[needRequestPermissonList.size()]), PERMISSON_REQUESTCODE);
        }
    }

    protected List<String> findDeniedPermissions(String[] permissions) {
        List<String> needRequestPermissonList = new ArrayList<String>();
        for (String perm : permissions) {
            if (ContextCompat.checkSelfPermission(mActivity, perm) != PackageManager.PERMISSION_GRANTED) {
                needRequestPermissonList.add(perm);
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(mActivity, perm)) {
                    needRequestPermissonList.add(perm);
                }
            }
        }
        return needRequestPermissonList;
    }

    /**
     * 检查是否有permission权限  返回true则表示有
     */
    public boolean isGranted_(String permission) {
        if (getActivity() == null) {
            return false;
        }
        mActivity = (BaseCompatActivity) getActivity();
        int checkSelfPermission = ActivityCompat.checkSelfPermission(mActivity, permission);
        return checkSelfPermission == PackageManager.PERMISSION_GRANTED;
    }

    public void setSaveStatus(boolean isSave) {
        this.isSave = isSave;
    }

    public boolean B() {
        return this.isSave;
    }

    public interface OnMsgClickCallBack {
        void onBtnClickListener();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mFmgHandler != null) {
            //解决界面移除时，progressDialog没有取消
            if (progressDialog != null && progressDialog.isShowing()) {
                dismissDialog();
            }
            mFmgHandler.removeCallbacksAndMessages(null);
            mFmgHandler = null;
        }
    }

    protected boolean notUsedCustomBackground() {
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unbinder.unbind();
        if (mPresenter != null) {
            mPresenter.onDetachView();
        }
    }


    protected abstract int getLayoutId();

    protected abstract void initView(View v);


    public Intent getIntent() {
        return mActivity.getIntent();
    }


}
