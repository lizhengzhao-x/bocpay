package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;


public class InviteListAdapter implements AdapterItem<InviteRecordListEntity.InviteRecordsBean> {
    private Context context;

    public InviteListAdapter(Context context) {
        this.context = context;
    }

    TextView id_nickname;
    TextView id_tel;
    TextView id_date;
    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_invite_list;
    }

    @Override
    public void bindViews(View root) {
        id_nickname = root.findViewById(R.id.id_nickname);
        id_tel = root.findViewById(R.id.id_tel);
        id_date = root.findViewById(R.id.id_date);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(InviteRecordListEntity.InviteRecordsBean inviteRecordsBean, int position) {
        mPosition = position;
        id_tel.setVisibility(View.VISIBLE);
        id_nickname.setVisibility(View.VISIBLE);
        if (inviteRecordsBean.isShowNumber()) {
            //通讯录匹配
            id_nickname.setText(inviteRecordsBean.getReCustName());
            id_tel.setText(inviteRecordsBean.getReMobileHidden());
            id_tel.setVisibility(View.VISIBLE);
        } else {
            //通讯录不匹配
            id_nickname.setText(inviteRecordsBean.getReMobileHidden());
            id_nickname.setVisibility(View.VISIBLE);
            id_tel.setVisibility(View.GONE);
        }
        id_date.setText(inviteRecordsBean.getCreateTime());

    }
}
