package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class NotificationStatusProtocol extends BaseProtocol {
    public static final String TAG = ParserTransferProtocol.class.getSimpleName();
    String status;
    String action;

    public NotificationStatusProtocol(String statusIn, String actionIn, NetWorkCallbackListener dataCallback) {
        this.status = statusIn;
        this.action = actionIn;
        this.mDataCallback = dataCallback;
        mUrl = "api/messageCenter/queryOrUpdateSwitch";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.STATUS, status);
        mBodyParams.put(RequestParams.ACTION, action);
    }
}




