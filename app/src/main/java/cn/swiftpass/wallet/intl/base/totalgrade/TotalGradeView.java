package cn.swiftpass.wallet.intl.base.totalgrade;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;

/**
 * 查询用户总积分 通用
 * Created by ZhangXinchao on 2019/7/16.
 */
public interface TotalGradeView<V extends IPresenter> {
    void showTotalGradeFailed(String errorCode, String errorMsg);

    void showTotalGradeSuccess(CreditCardGradeEntity response);
}
