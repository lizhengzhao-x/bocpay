package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/20
 * 转账二维码
 */

public class TransferNewQRCodeProtocol extends BaseProtocol {
    public static final String TAG = TransferNewQRCodeProtocol.class.getSimpleName();
    private String amount;
    private String remarks;

    public TransferNewQRCodeProtocol(String amount, String remarks, NetWorkCallbackListener callback) {
        mDataCallback = callback;
        this.amount = amount;
        this.remarks = remarks;
        mUrl = "api/transfer/transferFundQRCodeWithAmount";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.AMOUNT, amount);
        mBodyParams.put(RequestParams.REMARKS, remarks);
    }

}
