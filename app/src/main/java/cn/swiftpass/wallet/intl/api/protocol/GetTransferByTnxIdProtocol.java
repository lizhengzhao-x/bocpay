package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetTransferByTnxIdProtocol extends BaseProtocol {


    private final String txnId;
    private String outtrfrefno;

    public GetTransferByTnxIdProtocol(String txnId, String outtrfrefno, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.txnId = txnId;
        this.outtrfrefno = outtrfrefno;
        mUrl = "api/crossTransfer/orderInfo";

    }


    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(txnId)) {
            mBodyParams.put(RequestParams.TXNID, txnId);
        }
        if (!TextUtils.isEmpty(outtrfrefno)) {
            mBodyParams.put(RequestParams.OUTTRFREFNO, outtrfrefno);
        }
    }

}
