package cn.swiftpass.wallet.intl.module.register;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.adapter.BindNewCardAdapter;
import cn.swiftpass.wallet.intl.module.register.adapter.OnConfirmCardListLinstener;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardContact;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardListEntity;
import cn.swiftpass.wallet.intl.module.register.presenter.BindNewCardPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;

public class BindNewCardFragment extends BaseFragment<BindNewCardContact.Presenter> implements BindNewCardContact.View, OnConfirmCardListLinstener {
    @BindView(R.id.ry_bind_new_card)
    RecyclerView ryBindNewCard;
    @BindView(R.id.iv_network_error_refresh)
    ImageView ivNetworkErrorRefresh;
    @BindView(R.id.ll_network_error)
    LinearLayout llNetworkError;
    private BindNewCardAdapter newCardAdapter;
    private BindNewCardListEntity bindNewCardListEntity;
    //默认为登录用户的绑卡流程
    private int flowType = Constants.PAGE_FLOW_BIND_CARD;

    @Override
    public void initTitle() {
        newCardAdapter = new BindNewCardAdapter(mActivity, this);
        newCardAdapter.setFlowType(flowType);
        ryBindNewCard.setAdapter(newCardAdapter);
        if (mPresenter != null) {
            mPresenter.getBankCardList();
        }
        ivNetworkErrorRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) {
                    return;
                }
                if (mPresenter != null) {
                    mPresenter.getBankCardList();
                }
            }
        });
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    protected BindNewCardContact.Presenter loadPresenter() {
        return new BindNewCardPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_bind_new_card;
    }

    @Override
    protected void initView(View v) {
        llNetworkError.setVisibility(View.GONE);
        ryBindNewCard.setVisibility(View.GONE);
        ryBindNewCard.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

    }




    @Override
    public void getBankCardListSuccess(BindNewCardListEntity response) {
        ryBindNewCard.setVisibility(View.VISIBLE);
        llNetworkError.setVisibility(View.GONE);
        this.bindNewCardListEntity = response;
        if (response != null && response.getCardInfoList() != null && response.getCardInfoList().size() > 0) {
            newCardAdapter.setData(response.getCardInfoList());
            newCardAdapter.notifyDataSetChanged();
        } else {
            if (flowType == Constants.PAGE_FLOW_REGISTER) {
                showErrorMsgDialogAndBack();
            }
        }
    }


    /**
     * 如果是注册流程时，获取卡列表报错，或者卡列表数据为空，点击确定返回上一个页面
     */
    private void showErrorMsgDialogAndBack() {
        ryBindNewCard.setVisibility(View.GONE);
        llNetworkError.setVisibility(View.GONE);
        showErrorMsgDialog(getContext(), getString(R.string.KBCC2105_X_1), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    getActivity().finish();
                }
            }
        });
    }

    @Override
    public void getBankCardListFailed(String errorCode, String errorMsg) {

        if (flowType == Constants.PAGE_FLOW_REGISTER) {
            showErrorMsgDialogAndBack();
        } else {
            showErrorMsgDialog(getContext(), errorMsg);
            ryBindNewCard.setVisibility(View.GONE);
            llNetworkError.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onConfirmList(ArrayList<BindNewCardEntity> selectData) {
        HashMap<String, Object> params = new HashMap<>();
        params.put(ConfirmBindNewCardActivity.DATA_SELECT_CARD_LIST, selectData);
        if (this.bindNewCardListEntity!=null){
            params.put(ConfirmBindNewCardActivity.DATA_OTP_MOBILE, this.bindNewCardListEntity.getMobile());
        }
        params.put(Constants.CURRENT_PAGE_FLOW, flowType);
        ActivitySkipUtil.startAnotherActivity(mActivity, ConfirmBindNewCardActivity.class, params, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    public void setFlowType(int flowType) {
        this.flowType = flowType;
    }
}
