package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * @author ramon
 * Created by on 2018/1/10.
 */

public class ATMTipDialog extends Dialog {

    public ATMTipDialog(Context context) {
        super(context);
    }

    public ATMTipDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;

        public Builder(Context context) {
            this.context = context;
        }

        public ATMTipDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final ATMTipDialog dialog = new ATMTipDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_atm_tip, null);
            TextView posBtn = layout.findViewById(R.id.tv_ok);

            posBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

