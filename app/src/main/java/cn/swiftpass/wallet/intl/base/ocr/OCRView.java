package cn.swiftpass.wallet.intl.base.ocr;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.entity.OcrInfoEntity;


public interface OCRView<V extends IPresenter> {
    void getORCFailed(String errorCode, String errorMsg);

    void getORCSuccess(OcrInfoEntity response);
}
