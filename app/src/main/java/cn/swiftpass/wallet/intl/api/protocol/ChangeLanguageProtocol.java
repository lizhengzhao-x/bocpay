package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class ChangeLanguageProtocol extends BaseProtocol {

    String language;

    public ChangeLanguageProtocol(String language, NetWorkCallbackListener dataCallback) {
        this.language = language;
        this.mDataCallback = dataCallback;
        mUrl = "api/sysconfig/updateLang";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.LANG, language);
    }

}
