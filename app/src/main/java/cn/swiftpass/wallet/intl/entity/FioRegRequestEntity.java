package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/5/22.
 * FIO 注册第一步返回
 */

public class FioRegRequestEntity extends BaseEntity {
    public String getFioServerResponse() {
        return fioServerResponse;
    }

    public void setFioServerResponse(String fioServerResponse) {
        this.fioServerResponse = fioServerResponse;
    }

    //服务端调用FIO SERVER Registration Request接口返回的BASE64 字符串，客户端调用Registration Part 1接口需要此参数
    String fioServerResponse;
}
