package cn.swiftpass.wallet.intl.module.virtualcard.view;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;

/**
 * Created by ZhangXinchao on 2019/11/14.
 * 网上消费限额
 */
public class VirtualCardOnlineSpendingLimitActivity extends BaseCompatActivity {
    @Override
    public void init() {
        setToolBarTitle(R.string.AC2101_17_1);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_onlinespendinglimit;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }
}
