package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemDetailEntity;

/**
 * 换领电子券成功界面
 */
public class ConvertUseContract {

    public interface View extends IView {
        void checkEcouponInfoSuccess(RedeemDetailEntity response);

        void checkEcouponInfoError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<ConvertUseContract.View> {


        void checkEcouponInfo(String giftCode,String orderType,String referenceNo);
    }

}
