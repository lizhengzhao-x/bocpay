package cn.swiftpass.wallet.intl.utils.transformation;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import java.security.MessageDigest;

import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * Scale the image so that either the width of the image matches the given width and the height of
 * the image is greater than the given height or vice versa, and then crop the larger dimension to
 * match the given dimension.
 *
 * <p>Does not maintain the image's aspect ratio
 */
public class BaseBgTransformation extends BitmapTransformation {
    private static final String ID = "cn.swiftpass.wallet.intl.utils.transformation.BaseMainBgTransformation";
    private static final byte[] ID_BYTES = ID.getBytes(CHARSET);


    private String keyBg;

    public BaseBgTransformation(String keyBg) {
        this.keyBg = keyBg;

    }

    @Override
    protected Bitmap transform(
            @NonNull BitmapPool pool, @NonNull Bitmap inBitmap, int width, int height) {

        width= AndroidUtils.getScreenWidth(ProjectApp.getContext());
        height=AndroidUtils.getScreenHeight(ProjectApp.getContext());
        Bitmap result = pool.get(width,height , Bitmap.Config.ARGB_8888);

        final float scale;
        Matrix m = new Matrix();
        scale = (float) width / (float) inBitmap.getWidth();
        m.setScale(scale, scale);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.WHITE);
        float present=0.3f;
        int top = (int) (AndroidUtils.getScreenHeight(ProjectApp.getContext())*present);
        int bottom = (int) (AndroidUtils.getScreenHeight(ProjectApp.getContext()));
        RectF rect = new RectF(0, top, AndroidUtils.getScreenWidth(ProjectApp.getContext()), bottom);

        try {
            Canvas canvas = new Canvas(result);
            //互换位置就可以实现部分遮罩
            canvas.drawBitmap(inBitmap, m,paint);
            canvas.drawRect(rect, paint);
            clear(canvas);
        } finally {
        }

        return result;

    }



    private static void clear(Canvas canvas) {
        canvas.setBitmap(null);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof BaseBgTransformation) {
            BaseBgTransformation other = (BaseBgTransformation) o;
            return keyBg == other.keyBg;
        }
        return false;
    }


    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {
        messageDigest.update((ID + keyBg).getBytes(CHARSET));
    }


    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + ID.hashCode();
        result = 31 * result + keyBg.hashCode();
        return result;
    }

}
