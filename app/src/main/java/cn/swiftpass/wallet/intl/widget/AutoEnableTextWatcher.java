package cn.swiftpass.wallet.intl.widget;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import cn.swiftpass.wallet.intl.R;

public class AutoEnableTextWatcher {

    private static final int TAG_COUNT = R.id.Tag_Auto_Enable_Count;
    private static final String SPACE_STR = " ";

    public static void bind(final View enabledView, EditText... etArray) {
        bind(enabledView, 1, etArray);
    }

    public static void bind(final View enabledView, int minInputLength, EditText... etArray) {
        enabledView.setTag(TAG_COUNT, etArray.length);
        enabledView.setEnabled(false);
        for (EditText et : etArray) {
            et.addTextChangedListener(new LengthTextWatcher(enabledView, et, minInputLength));
        }
    }

    private static class LengthTextWatcher implements TextWatcher {

        private View mEnabledView;
        private boolean isAllowed;
        private int mMinInputLength;

        private LengthTextWatcher(View enabledView, EditText editText, int minInputLength) {
            mEnabledView = enabledView;
            mMinInputLength = minInputLength;
            afterTextChanged(editText.getText());
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {


        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int length = s == null ? 0 : s.length();
            boolean isAllowed = length >= mMinInputLength;
            if (s.toString().replace(SPACE_STR, "").length() == 0) {
                //如果全部是空格
                isAllowed = false;
            }

            if (isAllowed != this.isAllowed) {
                this.isAllowed = isAllowed;
                Integer integer = (Integer) mEnabledView.getTag(TAG_COUNT);
                if (!isAllowed) {
                    integer = integer + 1;
                } else {
                    integer = integer - 1;
                }
                mEnabledView.setTag(TAG_COUNT, integer);
                mEnabledView.setEnabled(integer == 0);
            }

        }
    }

}
