package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class NewMsgEntity extends BaseEntity {
    private String fullBannerUrl;
    private String bottomImageUrl;
    private String isRandomDisplay;
    private String openExtBrowserMsg;
    private String shareImageUrl;
    /**
     * 新增字段
     */
    private String businessParam;


    public String getBusinessParam() {
        return businessParam;
    }

    public void setBusinessParam(String businessParam) {
        this.businessParam = businessParam;
    }

    public int getLocalDefaultImageUrl() {
        return localDefaultImageUrl;
    }

    /**
     * 本地默认的资源文件
     */
    private int localDefaultImageUrl = -1;


    public NewMsgEntity(int resId) {
        this.localDefaultImageUrl = resId;
    }

    /**
     * 1:从浏览器打开
     * 2:为信用卡登记奖赏
     * 3:虚拟卡
     */
    private String newsType;
    private String url;

    public String getFullBannerUrl() {
        return fullBannerUrl;
    }

    public void setFullBannerUrl(String fullBannerUrl) {
        this.fullBannerUrl = fullBannerUrl;
    }

    public String getBottomImageUrl() {
        return bottomImageUrl;
    }

    public void setBottomImageUrl(String bottomImageUrl) {
        this.bottomImageUrl = bottomImageUrl;
    }

    public String getIsRandomDisplay() {
        return isRandomDisplay;
    }

    public void setIsRandomDisplay(String isRandomDisplay) {
        this.isRandomDisplay = isRandomDisplay;
    }

    public String getOpenExtBrowserMsg() {
        return openExtBrowserMsg;
    }

    public void setOpenExtBrowserMsg(String openExtBrowserMsg) {
        this.openExtBrowserMsg = openExtBrowserMsg;
    }

    public String getShareImageUrl() {
        return shareImageUrl;
    }

    public void setShareImageUrl(String shareImageUrl) {
        this.shareImageUrl = shareImageUrl;
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isVirtualCard() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return newsType.equals("3");
    }

    public boolean isCreditCardReward() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return TextUtils.equals(newsType, "2");
    }

    public boolean isCreditCardRewardList() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return TextUtils.equals(newsType, "4");
    }

    public boolean isOpenExtBrowser() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return TextUtils.equals(newsType, "1");
    }

    /**
     * 9月版 - 推广通知
     *
     * @return
     */
    public boolean isNotification() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return TextUtils.equals(newsType, "5");
    }

    public boolean isCreditCardRewards() {
        if (TextUtils.isEmpty(newsType)) {
            return false;
        }
        return TextUtils.equals(newsType, "6");
    }

}
