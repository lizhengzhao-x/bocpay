package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.ModifyInfoSuccessDialog;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountListEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.manage.OtpCountDownManager;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.UpdateSmartAccountInputOTPContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.UpdateSmartAccountOTPPresenter;
import cn.swiftpass.wallet.intl.module.topup.TopUpBindCardSuccessActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpSuccessActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

/**
 * 我的账户更新信息
 */
public class UpdateSmartAccountInputOTPActivity extends BaseCompatActivity<UpdateSmartAccountInputOTPContract.Presenter> implements UpdateSmartAccountInputOTPContract.View {

    @BindView(R.id.tv_smsCode)
    TextView tvSmsPhoneNumber;
    @BindView(R.id.tv_OTP_nextPage)
    TextView tvOTPNextPage;
    @BindView(R.id.etwd_otp_code)
    EditTextWithDel etwdOtpCode;
    @BindView(R.id.id_send_msg)
    TextView tvSendMsg;
    @BindView(R.id.id_sub_title)
    TextView idSubTitle;
    private int type;//跳转type 1.注册 2.绑卡 3.登录

    private String smartAction;
    private String dailyLimit;
    private String money;

    public void setResetTime(boolean isReset, String phoneNo) {
        if (isReset) {
            tvSendMsg.setClickable(false);
            tvSendMsg.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg==null)return;
                    tvSendMsg.setText(String.format(getString(R.string.register_send_msg_time), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                    tvSendMsg.setClickable(false);
                    tvSendMsg.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg==null)return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(getString(R.string.IDV_3a_20_5));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg==null)return;
                    tvSendMsg.setText(String.format(getString(R.string.register_send_msg_time), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg==null)return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(getString(R.string.IDV_3a_20_5));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }



    @Override
    public void init() {


        tvSendMsg.setVisibility(View.INVISIBLE);
        type = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        setToolBarTitle(R.string.smart_account_service);
        tvSmsPhoneNumber.setVisibility(View.VISIBLE);
        String phoneNumber = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        tvSmsPhoneNumber.setText(AndroidUtils.formatCenterPhoneNumberStr(phoneNumber));
        idSubTitle.setText(R.string.IDV_3a_20_3);
        smartAction = getIntent().getExtras().getString(Constants.DATA_SMART_ACTION);
        money = getIntent().getExtras().getString(Constants.MONEY);
        if (type == Constants.DATA_UPDATE_DAILY_LIMIT) {
            dailyLimit = getIntent().getExtras().getString(Constants.DATA_LIMIT);
//            setToolBarTitle(R.string.P3_ewa_01_044);
        } else if (type == Constants.DATA_CHANGE_DEFAULT_ACCOUNT) {

        } else if (type == Constants.DATA_UPDATE_SUSPEND) {

        } else if (type == Constants.ADJUST_LIMIT_SINGLE_TRANSACTION) {

        } else if (type == Constants.ADJUST_LIMIT_SINGLE_TRANSACTION) {

        } else if (type == Constants.ADJUST_LIMIT_DAILY_TRANSACTION) {

        } else if (type == Constants.ADJUST_LIMIT_VALUE_TRANSACTION) {

        } else if (type == Constants.PAGE_FLOW_BIND_PA) {
            setToolBarTitle(R.string.P3_ewa_01_044);
        } else if (type == Constants.DATA_S2_BIND_DELETE_ACCOUNT) {
            setToolBarTitle(R.string.IDV_3a_20_7);
        } else if (type == Constants.SMART_ACCOUNT_UPDATE) {
            //s2用户充值绑定银行卡
            setToolBarTitle(R.string.P3_T1_3_1);
        } else if (type == Constants.DATA_UPDATE_RECHARGETYPE) {
            setToolBarTitle(R.string.smart_account_service);
        } else if (type == Constants.SMART_ACCOUNT_WITHDRAW) {
            setToolBarTitle(R.string.P3_ewa_01_039);
        }

        mPresenter.sendOTP("","", smartAction);
        etwdOtpCode.hideErrorView();
        etwdOtpCode.setEditTextGravity(Gravity.CENTER);
        //监听验证码是否够6位 去校验otp接口
        etwdOtpCode.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etwdOtpCode.getEditText().getText().toString().trim().length() == 8) {
                    setBackBtnEnable(false);
                    etwdOtpCode.getmDelImageView().setEnabled(false);
                    verfiyOtpRequest();
                } else {
                    setBackBtnEnable(true);
                    etwdOtpCode.getmDelImageView().setEnabled(true);
                }
            }
        });


        tvSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                mPresenter.tryOTP("","", smartAction);

            }
        });

        etwdOtpCode.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (etwdOtpCode.getEditText().getText().toString().trim().length() == 8) {
                        verfiyOtpRequest();
                    }
                    return true;
                }
                return false;
            }
        });
        ProjectApp.getTempTaskStack().add(this);
    }


    private void verfiyOtpRequest() {
        String verifyCode = etwdOtpCode.getEditText().getText().toString().trim();
        mPresenter.verifyOTP("","",verifyCode,smartAction);
      }

    private void goWithDrawSuccessPage() {
        String referenceNo = getIntent().getExtras().getString(Constants.REFERENCENO);
        ApiProtocolImplManager.getInstance().sTwoTopUp(this, "W", money, referenceNo, new NetWorkCallbackListener<STwoTopUpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(UpdateSmartAccountInputOTPActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(STwoTopUpEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, getIntent().getExtras().get(Constants.PAYMENT_METHOD));
                mHashMaps.put(Constants.TYPE, Constants.TYPE_WITHDREA);
                mHashMaps.put(Constants.CURRENCY, response.getSrvcChrgDrCur());
                mHashMaps.put(Constants.MONEY, BigDecimalFormatUtils.forMatWithDigs(money, 2));
                mHashMaps.put(Constants.RECORD_NUMBER, response.getScrRefNo());
                mHashMaps.put(Constants.ACCTYPE, getIntent().getExtras().getString(Constants.ACCTYPE));
                ActivitySkipUtil.startAnotherActivity(UpdateSmartAccountInputOTPActivity.this, TopUpSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
            }
        });
    }



    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (etwdOtpCode==null)
                    return;
                etwdOtpCode.getEditText().setFocusable(true);
                etwdOtpCode.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etwdOtpCode.getEditText());
            }
        }, 1000);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_updatesmart_input_otp;
    }

    @Override
    protected UpdateSmartAccountInputOTPContract.Presenter createPresenter() {
        return new UpdateSmartAccountOTPPresenter();
    }


    @OnClick({R.id.tv_smsCode, R.id.tv_OTP_nextPage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_smsCode:
                break;
            case R.id.tv_OTP_nextPage:

                break;
            default:
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    private void initPasswordDialog() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                updateSmartAccountInfo();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

    }


    private void clearOtpInput() {
        etwdOtpCode.getEditText().setText("");
        showKeyBoard();
    }

    private void showSuccessInfo(String title, String msg, final OnMsgClickCallBack onMsgClickCallBack) {
        if (TextUtils.isEmpty(msg)) {
            msg = "";
        }
        ModifyInfoSuccessDialog.Builder builder = new ModifyInfoSuccessDialog.Builder(mContext);
        builder.setMessage(msg);
        builder.setTitle(title);
        builder.setPositiveButton(mContext.getString(R.string.POPUP1_1), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                onMsgClickCallBack.onBtnClickListener();
            }
        });
        Activity mActivity = (Activity) mContext;
        if (!mActivity.isFinishing()) {
            ModifyInfoSuccessDialog mDealDialog = builder.create();
            mDealDialog.show();
        }
    }


    private void updateSmartAccountInfo() {
        if (type == Constants.DATA_UPDATE_DAILY_LIMIT) {
            //修改限额
            ApiProtocolImplManager.getInstance().updateSmartAccountDailyLimit(mContext, dailyLimit, new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    showErrorMsgDialog(mContext, errorMsg);
                }

                @Override
                public void onSuccess(BaseEntity response) {
                    //修改限额
                    showSuccessInfo(getString(R.string.title_saved), getString(R.string.string_modify_information), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            //先修改内存中的限额金额，同时发eventbus触发接口请求，静默刷新智能账户数据
                            MySmartAccountEntity mySmartAccountEntity = CacheManagerInstance.getInstance().getMySmartAccountEntity();
                            if (mySmartAccountEntity != null) {
                                mySmartAccountEntity.setPayLimit(dailyLimit + "");
                            }
                            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT, ""));
                            EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                            finish();
                        }
                    });
                }
            });
        } else if (type == Constants.DATA_UPDATE_RECHARGETYPE) {
            //修改充值方式
            String topUpMethod = getIntent().getExtras().getString(Constants.DATA_ADD_METHOD);
            ApiProtocolImplManager.getInstance().updateSmartAccountTopUp(mContext, topUpMethod, TextUtils.isEmpty(money) ? "" : money, new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    showErrorMsgDialog(mContext, errorMsg);
                }

                @Override
                public void onSuccess(BaseEntity response) {

                    showSuccessInfo(getString(R.string.title_saved), getString(R.string.string_modify_information), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                            finish();
                        }
                    });
                }
            });
        } else if (type == Constants.DATA_UPDATE_SUSPEND) {
            ApiProtocolImplManager.getInstance().updateSmartAccountActive(mContext, new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(final String errorCode, String errorMsg) {
                    showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            if (TextUtils.equals(errorCode, ErrorCode.ACCOUNT_ACTIVED.code)) {
                                EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                                finish();
                            }
                        }
                    });
                }

                @Override
                public void onSuccess(BaseEntity response) {

                    showSuccessInfo(getString(R.string.title_saved), getString(R.string.string_modify_information), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                            finish();
                        }
                    });
                }
            });
        } else if (type == Constants.DATA_CHANGE_DEFAULT_ACCOUNT) {
            //更改主賬戶
            ApiProtocolImplManager.getInstance().updateSmartAccountPrimaryAccount(mContext, ((SmartAccountListEntity.RowsBean) getIntent().getExtras().getSerializable(Constants.EXTRA_ACCOUNT_INFO)).getAccountNo(), new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    showErrorMsgDialog(mContext, errorMsg);
                }

                @Override
                public void onSuccess(BaseEntity response) {
                    //更改主賬戶
                    showSuccessInfo(getString(R.string.title_saved), getString(R.string.string_modify_information), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                            finish();
                        }
                    });
                }
            });
        } else if (type == Constants.PAGE_FLOW_BIND_PA) {
            //注册edda
            String bankCode = getIntent().getExtras().getString(Constants.BANK_CODE);
            String bankNumber = getIntent().getExtras().getString(Constants.BANK_NUMBER);
            String currency = getIntent().getExtras().getString(Constants.CURRENCY);
            ApiProtocolImplManager.getInstance().regEddaInfo(this, bankCode, bankNumber, currency, new NetWorkCallbackListener<RegEddaInfoEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    AndroidUtils.showTipDialog(UpdateSmartAccountInputOTPActivity.this, errorMsg);
                }

                @Override
                public void onSuccess(RegEddaInfoEntity response) {
                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                    mHashMapsLogin.put(Constants.DATA_TYPE, Constants.PAGE_FLOW_BIND_PA);
                    mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, getIntent().getExtras().getBoolean(Constants.PAGE_FROM_REGISTER));
                    ActivitySkipUtil.startAnotherActivity(UpdateSmartAccountInputOTPActivity.this, TopUpBindCardSuccessActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });

        } else if (type == Constants.DATA_S2_BIND_DELETE_ACCOUNT) {
            //删除edda
            ApiProtocolImplManager.getInstance().deleteEddaInfo(this, new NetWorkCallbackListener<RegEddaInfoEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    AndroidUtils.showTipDialog(UpdateSmartAccountInputOTPActivity.this, errorMsg);
                }

                @Override
                public void onSuccess(RegEddaInfoEntity response) {
                    HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                    mHashMapsLogin.put(Constants.DATA_TYPE, Constants.DATA_S2_BIND_DELETE_ACCOUNT);
                    mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, getIntent().getExtras().getBoolean(Constants.PAGE_FROM_REGISTER));
                    ActivitySkipUtil.startAnotherActivity(UpdateSmartAccountInputOTPActivity.this, TopUpBindCardSuccessActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
        } else if (type == Constants.SMART_ACCOUNT_UPDATE) {
            updateSmartAccountInfoSubmit();
        }
    }

    private void updateSmartAccountInfoSubmit() {
        BankLoginResultEntity smartInfo = (BankLoginResultEntity) getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO);
        ApiProtocolImplManager.getInstance().sTwoUpdateAccountInfo(this, smartInfo.getRelevanceAccNo(), smartInfo.getPayLimit(), smartInfo.getAddedmethod(), smartInfo.getAutotopupamt(), new NetWorkCallbackListener() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(UpdateSmartAccountInputOTPActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(Object response) {
                goUpdateSmartAccountPage();
            }
        });
    }

    private void goUpdateSmartAccountPage() {

        ActivitySkipUtil.startAnotherActivity(UpdateSmartAccountInputOTPActivity.this, UpdateAccountSuccessActivity.class);
    }


    @Override
    public void sendOTPFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                tvSendMsg.setClickable(true);
                tvSendMsg.setEnabled(true);
                tvSendMsg.setVisibility(View.VISIBLE);
                tvSendMsg.setText(getString(R.string.IDV_3a_20_5));
                tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
            }
        });
    }

    @Override
    public void sendOTPSuccess(ContentEntity response) {
        setResetTime(true, null);
        clearOtpInput();
    }

    @Override
    public void tryOTPFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                tvSendMsg.setClickable(true);
                tvSendMsg.setEnabled(true);
                tvSendMsg.setVisibility(View.VISIBLE);
                tvSendMsg.setText(getString(R.string.IDV_3a_20_5));
                tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
            }
        });
    }

    @Override
    public void tryOTPSuccess(ContentEntity response) {
        setResetTime(true, null);
        clearOtpInput();
        showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }

    @Override
    public void verifyOTPFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }

    @Override
    public void verifyOTPSuccess(ContentEntity response) {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (type == Constants.SMART_ACCOUNT_UPDATE) {
                    updateSmartAccountInfo();
                } else if (type == Constants.SMART_ACCOUNT_WITHDRAW) {
                    goWithDrawSuccessPage();
                } else if (type == Constants.PAGE_FLOW_BIND_PA || type == Constants.DATA_S2_BIND_DELETE_ACCOUNT) {
                    updateSmartAccountInfo();
                } else {
                    initPasswordDialog();
                }
            }
        }, 200);
    }
}
