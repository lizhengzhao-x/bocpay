package cn.swiftpass.wallet.intl.module.notification;

import android.text.TextUtils;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_MY_DISCOUNT_PARAM;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_NEW_MESSAGE;
import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.EWA_U_PLAN_HOME_PARAM;

/**
 * @author lizheng.zhao
 * @date 2020/06/07
 * @description 登录态，app运行中，点击push通知，二级界面跳转返回时要返回之前界面，而非主页
 * 比如，当前处于MGM界面，点击UPlan主页的push通知，通过PushNotificationActivity跳转至UPlan界面，然后关闭PushNotificationActivity，UPlan界面点击返回值便返回MGM界面
 */
public class PushNotificationActivity extends BaseCompatActivity<PushNotificationContract.Presenter> implements PushNotificationContract.View {

    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";

    @Override
    protected void init() {

        if (getIntent() != null) {
            dealWithMessage(getIntent().getStringExtra(MESSAGE_TYPE));
        }
    }

    /**
     * 处理消息
     *
     * @param messageType
     */
    private void dealWithMessage(String messageType) {
        if (TextUtils.equals(messageType, EWA_U_PLAN_HOME_PARAM)) {
            //UPlan首页
            if (mPresenter != null) {
                mPresenter.getUPlanUrl();
            }
        } else if (TextUtils.equals(messageType, EWA_NEW_MESSAGE)) {
            //最新消息
            if (mPresenter != null) {
                mPresenter.getNewMessageUrl();
            }
        } else if (TextUtils.equals(messageType, EWA_MY_DISCOUNT_PARAM)) {
            //我的优惠券
            if (mPresenter != null) {
                mPresenter.getMyDiscountUrl();
            }
        }
    }


    @Override
    public void getUPlanUrlSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        finish();
    }

    @Override
    public void getUPlanUrlFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    public void getNewMessageUrlSuccess(NotificationJumpEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.DETAIL_URL, response.getTarget());
            mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.what_is_new));
            mHashMaps.put(WebViewActivity.NEW_MSG, true);
            mHashMaps.put(WebViewActivity.SHARE_CONTENT, response.getShareTarget());
            ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        finish();
    }

    @Override
    public void getNewMessageUrlFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    public void getMyDiscountUrlSuccess(UplanUrlEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(UplanActivity.URL_COUPONPICTURE, response.getCouponPictureUrl());
            mHashMaps.put(UplanActivity.URL_REDIRECT, response.getRedirectUrl());
            mHashMaps.put(UplanActivity.URL_MYCOUPON, response.getMyCoupon());
            mHashMaps.put(UplanActivity.URL_INSTRUCTIONS, response.getInstructions());
            ActivitySkipUtil.startAnotherActivity(getActivity(), UplanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        finish();
    }

    @Override
    public void getMyDiscountUrlFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                finish();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_push_notification;
    }

    @Override
    protected PushNotificationContract.Presenter createPresenter() {
        return new PushNotificationPresenter();
    }
}