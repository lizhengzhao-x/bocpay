package cn.swiftpass.wallet.intl.module.register.adapter.holder;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class BindeNewCardBottomHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;

    public BindeNewCardBottomHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void setData(ArrayList<BindNewCardEntity> data, OnBindeNewCardListener listener) {
        tvConfirm.setEnabled(false);
        if (data != null && data.size() > 0) {
            for (BindNewCardEntity cardEntity : data) {
                if (cardEntity.isCheck()) {
                    tvConfirm.setEnabled(true);
                    break;
                }
            }
        }

        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onConfirm();
            }
        });
    }
}
