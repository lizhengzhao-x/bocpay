package cn.swiftpass.wallet.intl.module.register;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FioConst;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.intl.dialog.FingerprintPopWindow;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterMobileEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.event.RegisterPopBannerDialogEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.cardmanagement.BindCardSuccessActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.FIOActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ResetPwdSuccessActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSetPwdEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardForgetPwdSendOtpActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.view.VirtualCardRegisterSendOtpActivity;
import cn.swiftpass.wallet.intl.sdk.fingercore.MyFingerPrintManager;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.safekeyboard.SafeKeyNumberBoard;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.PasswordInputView;


public class RegisterConfirmPaymentPswActivity extends BaseCompatActivity implements PasswordInputView.OnFinishListener, FingerprintPopWindow.OnPopWindowOkClickListener {
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    @BindView(R.id.password_view)
    PasswordInputView passwordView;

    private int mPageFlow;
    private String cardId;
    private String cardNo;
    private String walletId;
    private String phoneNo;
    private String mAccountType;
    private String mTypeFlow;
    private String expiryDateStr;
    String mPwdStr;
    private S2RegisterMobileEntity mRegisterMobileEntity;
    private SafeKeyNumberBoard safeKeyboardLeft;
    private VirtualCardSetPwdEntity virtualCardSetPwdEntity;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                //软键盘弹出 用户手势滑动的时候 隐藏软键盘
                if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()
                        && !AndroidUtils.inRangeOfView(safeKeyboardLeft.getKeyboardView(), ev)
                        && !AndroidUtils.inRangeOfView(passwordView, ev)) {
                    safeKeyboardLeft.hideKeyboard();
                }
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void init() {

        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        walletId = getIntent().getExtras().getString(Constants.WALLET_ID);
        cardId = getIntent().getExtras().getString(Constants.CARDID);
        phoneNo = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        mAccountType = getIntent().getExtras().getString(Constants.ACCOUNT_TYPE);
        cardNo = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        expiryDateStr = getIntent().getExtras().getString(Constants.EXDATE);
        initKeyBoard();

        passwordView.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        AndroidUtils.forbidCopyForEditText(passwordView);
        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD || mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //重设密码成功
            setToolBarTitle(R.string.IDV_3a_19_1);
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            //s2用户注册
            setToolBarTitle(R.string.IDV_2_1);
        } else if (mPageFlow == Constants.PAGE_FLOW_RESET_PASSWORD) {
            //重设密码
            setToolBarTitle(R.string.setting_payment_rgw);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER || mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡注册设置密码 iOS标题bug 跟用户解释困难 先改成跟iOS一样 用旧的key
            //setToolBarTitle(R.string.IDV_2_1);
            setToolBarTitle(R.string.comfirm_psw_instruction);
        } else {
            //注册成功 登录接口
            setToolBarTitle(R.string.IDV_2_1);
        }


        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                initSDKListener();
            }
        }
        mTypeFlow = AndroidUtils.getFlowFromType(mPageFlow);
        initListener();
        updateOkBackground(false);
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (passwordView.getText().toString().trim().length() < 6) {
                        showErrorMsgDialog(mContext, getString(R.string.payment_psw_instruction), new OnMsgClickCallBack() {

                            @Override
                            public void onBtnClickListener() {
                                showKeyBoard();
                            }
                        });
                    }
                    return true;
                }
                return false;
            }
        });
        passwordView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (safeKeyboardLeft != null && !safeKeyboardLeft.isShow()) {
                    safeKeyboardLeft.showKeyboard();
                }
            }
        });

    }

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.PAYMENT_PASSCODE_SETTING);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    @Override
    public boolean isAnalysisPage() {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.ALL_INFORMATION_CONFIRM;
        analysisPageEntity.current_address = PagerConstant.PAYMENT_PASSCODE_SETTING;
        return analysisPageEntity;
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }

    }


    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);
        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_CONFIRMPAYMENTPASSCODE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                ChatSDKManager.hide();
            }
        }
    }

    private void initListener() {
        passwordView.setOnFinishListener(this);
    }

    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        if (mHandler == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (passwordView == null) return;
                passwordView.setFocusable(true);
                passwordView.setFocusableInTouchMode(true);
                passwordView.requestFocus();
                // AndroidUtils.showKeyboard(RegisterConfirmPaymentPswActivity.this, passwordView);
            }
        }, 200);
    }

    private void initKeyBoard() {
        LinearLayout keyboardContainer = findViewById(R.id.keyboardViewPlace);
        View view = LayoutInflater.from(this).inflate(R.layout.number_keyboard_containor, null);
        safeKeyboardLeft = new SafeKeyNumberBoard(getApplicationContext(), keyboardContainer, passwordView, R.layout.number_keyboard_containor, view.findViewById(R.id.safeKeyboardLetter).getId());
        safeKeyboardLeft.setDelDrawable(this.getResources().getDrawable(R.drawable.icon_del));
    }


    @Override
    public void setOnPasswordFinished() {
        if (passwordView.getOriginText().length() >= passwordView.getMaxPasswordLength()) {
            AndroidUtils.hideKeyboard(passwordView);
            updateOkBackground(true);
        } else {
            updateOkBackground(false);
        }
    }


    private void updateOkBackground(boolean isSel) {
        tvConfirm.setEnabled(isSel);
        tvConfirm.setBackgroundResource(isSel ? R.drawable.bg_btn_next_page_normal : R.drawable.bg_btn_next_page_disable);
    }

    //找回密码我的账户
    private void sendSmartSetPwdRequest(final String enableBiometricAuth) {
        String pwdStr = passwordView.getOriginText();
        ApiProtocolImplManager.getInstance().sendSmartForgotPwd(mContext, pwdStr, mTypeFlow, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
            }


            @Override
            public void onSuccess(ContentEntity response) {
//                if (TextUtils.equals(response.getResult_code(), ResponseCode.RESPONSE_SUCCESS)) {
                ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, ResetPwdSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//                } else {
//                    AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, response.getResult_msg());
//                }
            }
        });
    }

    //找回密码信用卡
    private void sendCreditSetPwdRequest(final String enableBiometricAuth) {
        String cardNo = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        String pwdStr = passwordView.getOriginText();

        ApiProtocolImplManager.getInstance().getConfirmRegister(mContext, pwdStr, walletId, cardId, mTypeFlow, new NetWorkCallbackListener<RegSucEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
            }


            @Override
            public void onSuccess(RegSucEntity response) {
                if (!TextUtils.isEmpty(response.getNewUserPopUpBanner())) {
                    RegisterPopBannerDialogEvent registerPopBannerDialogEvent = new RegisterPopBannerDialogEvent();
                    registerPopBannerDialogEvent.updateEventParams(RegisterPopBannerDialogEvent.POPUPBANNERURL, response.getNewUserPopUpBanner());
                    UserLoginEventManager.getInstance().addUserLoginEvent(registerPopBannerDialogEvent);
                }
                if (CacheManagerInstance.getInstance().checkLoginStatus() && TextUtils.equals(response.getLogout(), Constants.NO_CARD_EXIST)) {
                    AndroidUtils.clearMemoryCache();
                } else {
                    if (CacheManagerInstance.getInstance().checkLoginStatus() && !TextUtils.isEmpty(response.getsId())) {
                        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
                        HttpCoreKeyManager.getInstance().setUserId(walletId);
                    }
                    ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, ResetPwdSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        EventBus.getDefault().unregister(this);
        if (safeKeyboardLeft != null && safeKeyboardLeft.isShow()) {
            safeKeyboardLeft.hideKeyboard();
            safeKeyboardLeft = null;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD
                || event.getEventType() == PasswordEventEntity.EVENT_RESET_PWD_SUCCESS) {
            finish();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_register_confirm_payment_psw;
    }


    private void showErrorPwdDialog() {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(RegisterConfirmPaymentPswActivity.this);
        builder.setMessage(getString(R.string.error_msg_psw_different));
        builder.setPositiveButton(getString(R.string.dialog_right_btn_ok), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterSetPaymentPswActivity.class);
                passwordView.setText("");
            }
        });

        if (!isFinishing()) {
            CustomMsgDialog mDealDialog = builder.create();
            mDealDialog.show();
        }

    }

    @OnClick({R.id.tv_confirm})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) return;
        switch (view.getId()) {
            case R.id.tv_confirm:
                //检查两次密码是否一致
                String oldPwd = getIntent().getExtras().getString(Constants.DATA_OLD_PWD);
                String pwdStr = passwordView.getOriginText();
                if (!oldPwd.equals(pwdStr)) {
                    showErrorPwdDialog();
                    return;
                }
                if (mPageFlow == Constants.PAGE_FLOW_RESET_PASSWORD) {
                    updatePwdRequest();
                } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
                    creditCardOrSmartAccountForgetPwd();
                } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                    creditCardOrSmartAccountRegister();
                } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                    s2RegisterSetPwd(pwdStr);
                } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
                    s2ForgetPwd(pwdStr);
                } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
                    //虚拟卡注册设置密码
                    virtualCardRegister(pwdStr);
                } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
                    //虚拟卡忘记密码
                    virtualCardForgetPassword(pwdStr);
                }

//                else {
//                    creditCardOrSmartAccountRegister();
//                }
                break;
            default:
                break;
        }
    }

    private void virtualCardRegister(String pwdStr) {
        String walletId = getIntent().getExtras().getString(Constants.WALLET_ID);
        VirtualCardListEntity.VirtualCardListBean virtualCardListBean = (VirtualCardListEntity.VirtualCardListBean) getIntent().getSerializableExtra(Constants.VIRTUALCARDLISTBEAN);
        ApiProtocolImplManager.getInstance().virtualCardRegisterSetPwd(this, virtualCardListBean, walletId, pwdStr, new NetWorkCallbackListener<VirtualCardSetPwdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(VirtualCardSetPwdEntity response) {
                virtualCardSetPwdEntity = response;
                //TODO 虚拟卡注册 指纹开启流程要验证
                if (virtualCardSetPwdEntity.isOpenBiometricAuth()) {
                    FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this);
                } else {
                    registerBySendVirtualCard(virtualCardSetPwdEntity);
                }
            }
        });
    }

    private void registerBySendVirtualCard(VirtualCardSetPwdEntity response) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.MOBILE_PHONE, response.getAccount());
        mHashMaps.put(Constants.CARD_ID, response.getCardId());
        mHashMaps.put(Constants.WALLET_ID, getIntent().getStringExtra(Constants.WALLET_ID));
        mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, getIntent().getSerializableExtra(Constants.VIRTUALCARDLISTBEAN));
        ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, VirtualCardRegisterSendOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void virtualCardForgetPassword(String pwdStr) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.ACCOUNT_PASSCODE, pwdStr);
        mHashMaps.put(Constants.MOBILE_PHONE, getIntent().getExtras().getString(Constants.MOBILE_PHONE));
        mHashMaps.put(Constants.FORGET_PASSWORD_TYPE, getIntent().getExtras().getString(Constants.FORGET_PASSWORD_TYPE));
        ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, VirtualCardForgetPwdSendOtpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void s2ForgetPwd(String pwdStr) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //忘记密码 发送验证码 需要传递otp
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, getIntent().getStringExtra(Constants.DATA_PHONE_NUMBER));
        }
        mHashMaps.put(Constants.ACCOUNT_PASSCODE, pwdStr);
        ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void s2RegisterSetPwd(String pwdStr) {
        ApiProtocolImplManager.getInstance().sTwoRegisterSetPwd(this, pwdStr, new NetWorkCallbackListener<S2RegisterMobileEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);
                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(S2RegisterMobileEntity response) {
                mRegisterMobileEntity = response;
                walletId = response.getUserId();
                FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this);
            }
        });

    }

    /**
     * 身份证扫描
     */
    private void goScanIdCardPage(String mobile, String userId) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.ACCOUNT_PASSCODE, mPwdStr);
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, mobile);
        mHashMaps.put(Constants.WALLET_ID, userId);
        ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void creditCardOrSmartAccountRegister() {
        mPwdStr = passwordView.getOriginText();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
            boolean isSupportAutn = getIntent().getBooleanExtra(Constants.USE_BIOMETRICAUTH, true);
            if (isSupportAutn) {
                FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this, Constants.PAGE_FLOW_REGISTER, mAccountType);
            } else {
                gotoRegisterOTP();
            }
        }
    }

    private void gotoRegisterOTP() {
        if (TextUtils.equals(Constants.ACCOUNT_TYPE_CREDIT, mAccountType)) {
            //信用卡注册
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
            mHashMaps.put(Constants.DATA_CARD_NUMBER, cardNo);
            mHashMaps.put(Constants.WALLET_ID, walletId);
            mHashMaps.put(Constants.CARDID, cardId);
            mHashMaps.put(Constants.EXDATE, expiryDateStr);
            mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_CREDIT);
            mHashMaps.put(Constants.USER_ACCOUNT, phoneNo);
            mHashMaps.put(Constants.ACCOUNT_PASSCODE, mPwdStr);
            ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            goScanIdCardPage(mRegisterMobileEntity.getMobile(), mRegisterMobileEntity.getUserId());
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            registerBySendVirtualCard(virtualCardSetPwdEntity);
        } else {
            //智能账户
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
            mHashMaps.put(Constants.ACCOUNT_TYPE, Constants.ACCOUNT_TYPE_SMART);
            mHashMaps.put(Constants.ACCOUNT_PASSCODE, mPwdStr);
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
            mHashMaps.put(Constants.WALLET_ID, walletId);
            mHashMaps.put(SmartAccountConst.DATA_SMART_ACCOUNT_INFO, getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO));
            ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterInputOTPActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

//    private void creditCardOrSmartAccountRegister() {
//        String pwdStr = passwordView.getOriginText();
//        if (TextUtils.equals(Constants.ACCOUNT_TYPE_CREDIT, mAccountType)) {
//            //信用卡注册
//            registerByCredit(pwdStr);
//        } else {
//            //智能账户注册
//            registerBySmart(pwdStr);
//        }
//
//    }

    //业务二，忘记密码
    private void creditCardOrSmartAccountForgetPwd() {
        String auth = CacheManagerInstance.getInstance().isOpenFingerPrint() ? "Y" : "N";
        if (TextUtils.equals(Constants.ACCOUNT_TYPE_CREDIT, mAccountType)) {
            //信用卡忘记密码
            sendCreditSetPwdRequest(auth);
        } else {
            //智能账户忘记密码
            sendSmartSetPwdRequest(auth);
        }
    }

    //业务三，重置密码
    private void updatePwdRequest() {
        String oldword = getIntent().getExtras().getString(Constants.DATA_OLD_PAY_PWD);
        String pwdStr = passwordView.getOriginText();
        ApiProtocolImplManager.getInstance().getModifyPassword(mContext, pwdStr, oldword, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW));
                ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, BindCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    /*
     *注册账户，旧流程，已弃用
     */
//    private void registerBySmart(String passcode) {
//        ApiProtocolImplManager.getInstance().registerBySmartAccount(mContext, passcode, "", mTypeFlow, new NetWorkCallbackListener<RegSucBySmartEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
//            }
//
//            @Override
//            public void onSuccess(RegSucBySmartEntity response) {
//                //CacheManagerInstance.getInstance().saveLoginStatus();
//                //HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
////                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
//                if (response.isOpenBiometricAuth()) {
//                    FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this);
//                } else {
//                    gotoRegisterOTP();
//                }
////                } else {
////                    showRegisterSuccessPage(response);
////                }
//            }
//        });
//    }

//    //注册账户，旧流程，已弃用
//    private void registerByCredit(String passcode) {
//        ApiProtocolImplManager.getInstance().getConfirmRegister(mContext, passcode, walletId, cardId, mTypeFlow, new NetWorkCallbackListener<RegSucEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                AndroidUtils.showTipDialog(RegisterConfirmPaymentPswActivity.this, errorMsg);
//            }
//
//            @Override
//            public void onSuccess(RegSucEntity response) {
//                HttpCoreKeyManager.getInstance().setUserId(walletId);
//                if (response.isOpenBiometricAuth()) {
//                    FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this, Constants.PAGE_FLOW_REGISTER, mAccountType);
//                } else {
//                    gotoRegisterOTP();
////                    showRegisterSuccessPage(response);
//                }
////                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
////                    if (response.isOpenBiometricAuth()) {
////                        FIOActivity.startCheckPolicy(RegisterConfirmPaymentPswActivity.this, Constants.PAGE_FLOW_REGISTER, mAccountType);
////                    } else {
////                        showRegisterSuccessPage(response);
////                    }
////                } else {
////                    showRegisterSuccessPage(response);
////                }
//            }
//        });
//    }

//    private void showRegisterSuccessPage(RegSucEntity response) {
//        HashMap<String, Object> mHashMaps = new HashMap<>();
//        if (response != null) {
//            mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
//            mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
//        }
//        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
//        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
//        mHashMaps.put(Constants.ACCOUNT_TYPE, mAccountType);
//
//        ActivitySkipUtil.startAnotherActivity(RegisterConfirmPaymentPswActivity.this, RegisterCreditCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
//    }

    @Override
    public void onBackIconClickListener() {

    }

    @Override
    public void onPwdSwitchListener() {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FioConst.REQUEST_CODE_CHECK_POLICY) {
            if (resultCode == RESULT_OK) {
                boolean bCheckPolicy = data.getBooleanExtra(FioConst.ACTION_CHECK_POLICY, false);
                boolean hasFingerPrintEnrolled = data.getBooleanExtra(FioConst.HAS_FINGERPRINT_ENROLLED, false);
                boolean hasFingerPrint = data.getBooleanExtra(FioConst.HAS_FINGERPRINT, false);
                boolean hasRegister = MyFingerPrintManager.getInstance().isRegisterFIO(this);

                if (hasRegister) {
                    MyFingerPrintManager.getInstance().clearFIO();
                }

                if (bCheckPolicy && hasFingerPrintEnrolled && hasFingerPrint) {
                    showFingerprintGuideDialog();
                } else {
                    gotoRegisterOTP();
                }
            } else {
                gotoRegisterOTP();
            }
        } else if (requestCode == FioConst.REQUEST_CODE_REG_FIO) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                DataCollectManager.getInstance().sendSetFio();
            }
            if (resultCode == RESULT_OK) {
                boolean bRegFio = data.getBooleanExtra(FioConst.ACTION_REG_FIO, false);
                if (bRegFio) {
                    gotoRegisterOTP();
                } else {
                    DialogInterface.OnClickListener posListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            FIOActivity.startRegFingerPrint(RegisterConfirmPaymentPswActivity.this, walletId);
                        }
                    };

                    DialogInterface.OnClickListener negListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            gotoRegisterOTP();
                        }
                    };
                    AndroidUtils.showOpenFioFailed(RegisterConfirmPaymentPswActivity.this, posListener, negListener);
                }
            } else {
                showFingerprintGuideDialog();
            }
        }
    }

    private void showFingerprintGuideDialog() {
        View.OnClickListener posListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyFingerPrintManager.getInstance().clearFIO();
                FIOActivity.startRegFingerPrint(RegisterConfirmPaymentPswActivity.this, walletId);
            }
        };

        View.OnClickListener negListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoRegisterOTP();
            }
        };
        AndroidUtils.showFingerprintGuideDialog(RegisterConfirmPaymentPswActivity.this, posListener, negListener);
    }
}
