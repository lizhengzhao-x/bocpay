package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class STwoTopUpEntity extends BaseEntity {
    // {"acNo":"2493","drAmt":"3.00","paymentCategory":"0","crDisplayedEngNm":"CHAN, W*** T***","scrRefNo":"T011516330000039","srvcChrgDrCur":"HKD"}
    public String getSrvcChrgDrCur() {
        return srvcChrgDrCur;
    }

    public void setSrvcChrgDrCur(String srvcChrgDrCur) {
        this.srvcChrgDrCur = srvcChrgDrCur;
    }

    private String srvcChrgDrCur;

    public String getScrRefNo() {
        return scrRefNo;
    }

    public void setScrRefNo(String scrRefNo) {
        this.scrRefNo = scrRefNo;
    }

    private String acNo;

    public String getAcNo() {
        return acNo;
    }

    public void setAcNo(String acNo) {
        this.acNo = acNo;
    }

    private String scrRefNo;
    private String drAmt;

    public String getDrAmt() {
        return drAmt;
    }

    public void setDrAmt(String drAmt) {
        this.drAmt = drAmt;
    }

    public String getPaymentCategory() {
        return paymentCategory;
    }

    public void setPaymentCategory(String paymentCategory) {
        this.paymentCategory = paymentCategory;
    }

    public String getCrDisplayedEngNm() {
        return crDisplayedEngNm;
    }

    public void setCrDisplayedEngNm(String crDisplayedEngNm) {
        this.crDisplayedEngNm = crDisplayedEngNm;
    }

    private String paymentCategory;
    private String crDisplayedEngNm;
}
