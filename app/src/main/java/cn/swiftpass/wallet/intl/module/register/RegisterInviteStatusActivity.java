package cn.swiftpass.wallet.intl.module.register;


import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InviteStatusEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 邀请成功 失败 界面 ui细微差别
 */
public class RegisterInviteStatusActivity extends BaseCompatActivity {


    private ImageView mId_invite_status_image;
    private TextView mId_invite_status;
    private TextView mId_invite_sub_message;
    private TextView mId_invite_message;
    private TextView mId_tv_back;
    //标识输入邀请码是从注册流程 还是在设置流程
    public static int INVITE_TYPE_REGISTER = 100;
    public static int INVITE_TYPE_SETTING = 200;
    private int eventType;
    private String statusMessage;

    private void bindViews() {
        mId_invite_status_image = (ImageView) findViewById(R.id.id_invite_status_image);
        mId_invite_status = (TextView) findViewById(R.id.id_invite_status);
        mId_invite_message = (TextView) findViewById(R.id.id_invite_message);
        mId_tv_back = (TextView) findViewById(R.id.id_tv_back);
        mId_invite_sub_message = (TextView) findViewById(R.id.id_invite_sub_message);
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        bindViews();
        eventType = getIntent().getExtras().getInt(Constants.INVITE_TYPE);
        //注册流程 隐藏back按键
        hideBackIcon();
        if (eventType == INVITE_TYPE_REGISTER) {
            setToolBarTitle(R.string.IDV_2_1);
            mId_invite_status.setText(R.string.register_title_registration);
        } else {
            setToolBarTitle(R.string.MGM1_2_1);
            mId_invite_status.setText(R.string.MGM3_2_1);
        }
        if (eventType == INVITE_TYPE_SETTING) {
            statusMessage = getIntent().getStringExtra(Constants.STATUS_MESSAGE);
            if (!TextUtils.isEmpty(statusMessage)) {
                //设置里头邀请码词条动态
                mId_invite_sub_message.setText(statusMessage);
            }
        } else {
            mId_invite_sub_message.setVisibility(View.GONE);
        }
        mId_tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goNextPage();
            }
        });
    }

    private void goNextPage() {
        if (eventType == INVITE_TYPE_REGISTER) {
            goHomePage();
        } else {
            //返回到设置页面
            EventBus.getDefault().post(new InviteStatusEntity());
            finish();
        }
    }

    /**
     * 直接到达首页
     */
    private void goHomePage() {
        MyActivityManager.removeAllTaskExcludeMainStack();
        Intent intent = new Intent(RegisterInviteStatusActivity.this, MainHomeActivity.class);
        intent.putExtra(Constants.ISREGESTER, true);
//        boolean isNeedJumpNotification = false;
//        if (getIntent() != null) {
//            isNeedJumpNotification = getIntent().getBooleanExtra(Constants.IS_NEED_JUMP_NOTIFICATION, false);
//            intent.putExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, getIntent().getStringExtra(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER));
//        }
//        intent.putExtra(Constants.IS_NEED_JUMP_NOTIFICATION, isNeedJumpNotification);
        startActivity(intent);
        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_invite_status;
    }

    @Override
    public void onBackPressed() {
//        if (eventType == INVITE_TYPE_REGISTER) {
//        } else {
//            super.onBackPressed();
//        }

    }
}


