package cn.swiftpass.wallet.intl.entity;

public class SelCountyEntity {
    private String countyName;
    private String countyCode;
    private boolean isSelection;

    public SelCountyEntity(String countyCode, String countyName, boolean isSelection) {
        this.countyName = countyName;
        this.countyCode = countyCode;
        this.isSelection = isSelection;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public boolean isSelection() {
        return isSelection;
    }

    public void setSelection(boolean selection) {
        isSelection = selection;
    }
}
