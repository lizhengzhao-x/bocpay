package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author ramon
 * Created by  on 2018/8/20.
 * 真正的转账结果
 */

public class BankLoginVerifyCodeEntity extends BaseEntity {
    //验证码base64
    String imageCode;

    //验证码长度
    String verifyCodeLength;

    public String getVerifyCodeLength() {
        return verifyCodeLength;
    }

    public void setVerifyCodeLength(String verifyCodeLength) {
        this.verifyCodeLength = verifyCodeLength;
    }


    public String getImageCode() {
        return imageCode;
    }

    public void setImageCode(String imageCode) {
        this.imageCode = imageCode;
    }

}
