package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update;

import android.view.View;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;


public class UpdateAccountSuccessActivity extends BaseCompatActivity {
    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        //升级
        hideBackIcon();
        setToolBarTitle(R.string.P3_T1_3_1);
        findViewById(R.id.tv_link_bo_bocpay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyActivityManager.removeAllTaskExcludeMainStack();
                ActivitySkipUtil.startAnotherActivity(UpdateAccountSuccessActivity.this, MainHomeActivity.class);
                EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_update_account;
    }

    @Override
    public void onBackPressed() {

    }
}
