package cn.swiftpass.wallet.intl.dialog;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


public class DeleteCardPopWindow extends BasePopWindow implements View.OnClickListener {


    private TextView mId_sel_confirm;
    private TextView mId_sel_cancel;
    private TextView mMsgTV;
    private LinearLayout mDeleteAccount;

    private OnPopWindowOkClickListener mOnPopClickListener;

    public DeleteCardPopWindow(Activity mActivity, OnPopWindowOkClickListener onPopWindowOkClickListener) {
        super(mActivity);
        this.mOnPopClickListener = onPopWindowOkClickListener;
        initView();
    }

    public void setMsg(String msg) {
        mMsgTV.setText(msg);
    }

    public void setMsg(int msgID) {
        mMsgTV.setText(msgID);
    }

    public void setConfirmText(String txt) {
        mId_sel_confirm.setText(txt);
    }

    public void setConfirmText(int resId) {
        mId_sel_confirm.setText(resId);
    }

    public void setCancelText(String txt) {
        mId_sel_cancel.setText(txt);
    }

    public void setCancelText(int resId) {
        mId_sel_cancel.setText(resId);
    }


    @Override
    protected void initView() {
        setContentView(R.layout.pop_deletecard_view);
        this.setAnimationStyle(R.style.dialog_style);
        mMsgTV = mContentView.findViewById(R.id.tv_pop_msg);
        mDeleteAccount = mContentView.findViewById(R.id.id_delete_account);
        mId_sel_confirm = mContentView.findViewById(R.id.id_sel_confirm);
        mId_sel_cancel = mContentView.findViewById(R.id.id_sel_cancel);
        mId_sel_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mOnPopClickListener) {
                    mOnPopClickListener.onPopWindowOkClickListener(true);
                }
            }
        });
        mId_sel_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mOnPopClickListener) {
                    mOnPopClickListener.onPopWindowOkClickListener(false);
                }
            }
        });

        mDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (null != mOnPopClickListener) {
                    mOnPopClickListener.onPopWindowDeleteClickListener();
                }
            }
        });

        setDisMissView(mContentView);
    }

    @Override
    public void onClick(View view) {

    }

    public void showDeleteAccountView() {
        mDeleteAccount.setVisibility(View.VISIBLE);
    }

    public void hideUnBindAccountView() {
        mId_sel_confirm.setVisibility(View.GONE);
        mDeleteAccount.setVisibility(View.VISIBLE);
    }

    public void show() {
        showAtLocation(mContentView, Gravity.BOTTOM, Gravity.BOTTOM, Gravity.BOTTOM);
    }

    public interface OnPopWindowOkClickListener {
        void onPopWindowOkClickListener(boolean delete);

        void onPopWindowDeleteClickListener();
    }
}
