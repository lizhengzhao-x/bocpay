package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponActivityEntity;

public class EcouponsActivityItemHolder extends RecyclerView.ViewHolder {

    public ImageView imageItem;
    public TextView textItemBtn;
    public TextView statementItemBtn;
    private EcouponsActivityItemClickListener onClickListener;
    private EcouponActivityEntity activityEntity;


    public EcouponsActivityItemHolder(@NonNull View itemView) {
        super(itemView);
        imageItem = (ImageView) itemView.findViewById(R.id.iv_activity_pic);
        textItemBtn = (TextView) itemView.findViewById(R.id.tv_activity_text);
        statementItemBtn = (TextView) itemView.findViewById(R.id.tv_activity_statement);
        textItemBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener != null) {
                    onClickListener.onClick(activityEntity);
                }
            }
        });
    }


    public void setOnItemClickListener(EcouponActivityEntity activityEntity, EcouponsActivityItemClickListener onClickListener) {
        this.activityEntity = activityEntity;
        this.onClickListener = onClickListener;
    }


}
