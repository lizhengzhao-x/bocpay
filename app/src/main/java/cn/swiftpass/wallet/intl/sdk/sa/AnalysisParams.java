package cn.swiftpass.wallet.intl.sdk.sa;

public class AnalysisParams {

    public static final String DATA_TYPE = "dataType";
    public static final String DEVICE_ID = "device_id";
    public static final String APP_SYSTEM = "app_system";
    public static final String APP_VERSION = "app_version";
    public static final String APP_NAME = "app_name";
    public static final String RECORD_ID = "record_id";
    public static final String ACTION_ID = "action_id";
    public static final String DATE_TIME = "date_time";
    public static final String LAST_ADDRESS = "last_address";
    public static final String CURRENT_ADDRESS = "current_address";
    public static final String TIME_DURATION = "time_duration";
    public static final String ERROR_MSG = "error_msg";
    public static final String ERROR_CODE = "error_code";
}
