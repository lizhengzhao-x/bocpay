package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class CheckHKCustProtocol extends BaseProtocol {

    public CheckHKCustProtocol(String type, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        //不同流程 后台区分 ur不同
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/checkHKCust";
        } else if (type.equals(RequestParams.REGISTER_STWO_ACCOUNT)) {
            mUrl = "api/smartReg/checkHKCust";
        } else if (type.equals(RequestParams.FORGET_PWD)) {
            mUrl = "api/smartForgot/checkHKCust";
        }
    }

}
