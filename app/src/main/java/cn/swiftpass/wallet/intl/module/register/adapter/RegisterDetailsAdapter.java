package cn.swiftpass.wallet.intl.module.register.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.BaseRecycleAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.RegisterDetailsEntity;

public class RegisterDetailsAdapter extends BaseRecycleAdapter<RegisterDetailsEntity> {


    private List<RegisterDetailsEntity> mList = new ArrayList<>();
    private RegisterDetailsEntity mEntity;

    public RegisterDetailsAdapter(List<RegisterDetailsEntity> datas) {
        super(datas);
        this.mList = datas;
    }

    @Override
    protected void bindData(BaseViewHolder holder, int position) {
        TextView tv_title = (TextView) holder.getView(R.id.tv_title);
        TextView tv_info = (TextView) holder.getView(R.id.tv_info);
        mEntity = mList.get(position);
        if (mEntity != null) {

            if (!TextUtils.isEmpty(mEntity.getTitle())) {
                tv_title.setText(mEntity.getTitle());
            } else {
                tv_title.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(mEntity.getInfo())) {
                tv_info.setText(mEntity.getInfo());
            }
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.item_register_details;
    }
}
