package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


/**
 * @author zhaolizheng
 * create date on 2020/12/30
 */

public class BankRegisterVerifyCodeProtocol extends BaseProtocol {

    public static final String TAG = BankRegisterVerifyCodeProtocol.class.getSimpleName();


    String width = "200";
    //验证码长度，不大于200
    String height = "60";
    //验证码高度，不大于150
    String codeCount = "4";
    //验证码长度，一般是4位，不大于8位
    //CC_V：信用卡注册验证码
    String action;

    public BankRegisterVerifyCodeProtocol(String action, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.action = action;
        mUrl = "api/smartAccOp/getVerifyCode";
        mEncryptFlag = false;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CODE_HEIGHT, height);
        mBodyParams.put(RequestParams.CODE_WIDTH, width);
        mBodyParams.put(RequestParams.CODE_NUM, codeCount);
        mBodyParams.put(RequestParams.ACTION, action);
    }

}
