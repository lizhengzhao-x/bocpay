package cn.swiftpass.wallet.intl.app.constants;

/**
 * Created by Jamy on 2018/7/20 13:51
 * 各个接口的请求参数
 */

public class RequestParams {
    public static final String APPVERTION = "appVertion";
    public static final String SHAINFO = "shaInfo";
    public static final String VIRTUALCARDIND = "virtualCardInd";
    public static final String EXPDATE = "expDate";
    public static final String EAIACTION = "eaiAction";
    public static final String OTPACT = "otpAct";
    public static final String VERIFYCODE = "verifyCode";
    public static final String TYPE = "type";
    public static final String PHONENO = "phoneNo";
    public static final String NID = "nid";
    public static final String NIDTYPE = "nidType";
    public static final String EXPIRYDATE = "expiryDate";
    public static final String NIDRGN = "nidRgn";
    public static final String CHECKACT = "checkAct";
    public static final String CHECKTYPE = "checkType";
    public static final String PAGEID = "pageId";
    public static final String DESC = "desc";
    //--------获取服务端公钥接口--------------//
    //公钥
    public static String APP_PUBKEY = "appPubKey";

    //--------信用卡注册部分接口--------------//
    //卡号
    public static String CARDNO = "PAN";
    //有效期
    public static String EXPRIYDATE = "expiryDate";
    //安全码
    public static String CVV = "CVV";
    //操作标识
    public static String ACTION = "action";
    public static String ORDERNO = "orderNo";

    public static String ACCOUNT = "account";

    public static String IS_CONTINUE = "isContinue";

    //--------查询单币种接口--------------//
    //币种数字代码.
    public static String NUMBERCODE = "number_code";

    //--------首页活动列表接口--------------//
    //参数名
    public static String ACTIVITIES_NAME = "name";

    //--------我的账户注册接口--------------//
    //我的账户号
    public static String ACCOUNTNO = "accNo";
    //我的账户密码
    public static String PASSWORD = "password";
    //登录类型
    public static String LOGINTYPE = "loginType";
    public static String TOPUPMETHOD = "topupmethod";

    //--------主扫二维码信息接口--------------//
    public static String URLQRCODE = "urlQrCode";

    //--------账单查询接口--------------//
    public static String QUERYTYPE = "queryType";
    public static String PAGENO = "pageNo";
    public static String PAGESIZE = "pageSize";
    public static String PANID = "panId";

    //--------主扫下单接口--------------//
    //二维码类型  MPEMV和MPURL
    public static String QRCTYPE = "qrcType";
    //银行卡号（全数字）
    public static String CARDID = "cardId";
    public static String KEY = "installmentType";
    //交易金额
    public static String TRXAMOUNT = "trxAmt";
    //费率,EPEMV码需要费率
    public static String TRXFEEAMOUNT = "trxFeeAmt";
    //二维码信息
    public static String MPQRCODE = "mpQrCode";
    public static String TXNAMT = "txnAmt";
    //商家名称
    public static String MERCHANTNAME = "merchantName";
    //优惠券信息
    public static String COUPINFO = "couponInfo";
    //账单号
    public static String BILLNUMBER = "billNumber";
    //手机号
    public static String MOBILE = "mobile";
    //交易Id
    public static String TXNID = "txnId";
    public static String OUTTRFREFNO = "outtrfrefno";
    public static String TXNTYPE = "txnType";
    //店铺标签
    public static String STORELABEL = "storeLabel";
    //Loyalty Number
    public static String LOYALTYNO = "loyaltyNo";
    //Reference Label
    public static String REFLABEL = "refLabel";
    //买主标签
    public static String CUSTOMERLABEL = "customerLable";
    //终端标签
    public static String TERMINALABEL = "terminalLabel";
    //交易目的
    public static String TRXPURPOSE = "trxPurpose";
    //消费者邮箱
    public static String CONSUMEREMAIL = "consumerEmail";
    //消费这地址
    public static String CONSUMERADDRESS = "consumerAddress";
    //消费者电话
    public static String CONSUMERMOBILE = "consumerMobile";
    public static String OTPCODE = "otpCode";
    //交易币种
    public static String TXNCURR = "txnCurr";
    public static String TXNCUR = "txnCur";

    //--------我的账户绑定接口--------------//
    //登陆用户的唯一标识
    public static String PAYUSERID = "payUserId";
    //登陆账号（网银或者卡号）
    public static String ACCNO = "accNo";
    //密码
    public static String PWD = "password";
    //登陆类型
    public static String LOGTYPE = "loginType";
    //1：注册  2：绑定
    public static String OPTTYPE = "opType";
    public static String PAYLIMIT = "payLimit";

    public static String REFERENCENO = "referenceNo";
    public static String REFERENCENOS = "referenceNos";
    public static String BUSINESSUNITS = "businessUnits";
    public static String RELOAD = "reload";

    //--------被扫二维码信息接口--------------//
    //交易银行卡Id
    public static String CARD_ID = "cardId";
    //优惠券信息
    public static String COUPONINFO = "couponInfo";
    public static String DEFAULTPAYTYPE = "defaultPayType";

    //--------查询交易状态接口--------------//
    //二维码信息
    public static String CP_QRCODE = "cpQrCode";
    public static String QRCODE = "qrCode";
    //当前页码
    public static String CURRENT_PAGE = "currentPage";
    public static String DATE_TYPE = "dateType";
    public static String DATE_KEY = "dateKey";
    public static String REWARDS_TYPE = "rewardsType";
    //交易单号
    public static String TXN_ID = "txnId";

    //--------银行卡列表接口--------------//
    //第几页
    public static String PAGENUM = "pageNum";
    //每页大小
    public static String PAGE_SIZE = "pageSize";
    //登陆钱包的用户id
    public static String LOGONUSERID = "logonUserId";

    //--------发送及校验信用卡OTP接口--------------//
    //用户id
    public static String WALLETID = "walletId";
    //卡号
    public static String PAN = "PAN";
    public static String PAN_ID = "pan";
    //过期时间
    public static String EXDATE = "expDate";
    //短信验证码
    public static String OTP = "OTP";
    public static String LANG = "lang";
    public static String INCODE = "inCode";


    //开通我的账户的主账户
    public static String SMART_ACCOUNT_PRIMARY = "relevanceAccNo";

    //开通我的账户的增值方式
    public static String TOPUP_METHOD = "addedmethod";

    //当选择auto增值方式，设置上额，最大5000
    public static String AUTO_TOPUP_LIMIT = "autotopupamt";


    //开通我的账户的日交易限额，最大5000
    public static String DAILY_PAY_LIMIT = "limitPay";


    //--------确认登陆接口--------------//
    //支付密码
    public static String PASSCODE = "passcode";
    //区号
    public static String REGION = "region";
    //取值：Y；是否可以在其它设备继续登录
    public static String ISCONTINUE = "isContinue";

    public static String FIRST_PIN = "pin";


    //--------确认注册接口--------------//
    //指纹
    public static String FINGERPRINTID = "fingerprintId";
    //人脸识别
    public static String FACEID = "faceId";

    //--------接触绑定银行卡接口--------------//
    //默认卡id
    public static String NEWDEFAULTCARDID = "newDefaultCardId";

    //--------校验修改账户支付密码接口--------------//
    //Userid  用户唯一标识
    public static String WALLECTUSERID = "walletUserId";
    //钱包登陆的原密码
    public static String PAY_PASSCODE = "payPwd";
    public static String PAY_PASSCODE_OLD = "oldPayPwd";


    //--------验证我的账户otp接口--------------//
    //我的账户验证码
    public static String VERIFY = "verifyCode";
    public static String VCODE = "vcode";
    public static String PAY_PASS_CODE = "payPassword";
    public static String PRIMARYAC = "primaryAc";

    //--------确认付款转账--------------//
    //我的账户验证码
    public static String TRANSFER_ORDER_NUM = "scrRefNo";
    //收款账号
    public static String TRANSFER_ACCOUNT = "tansferToAccount";
    public static String TRANSFER_POSTSCRIPT = "postscript";
    //转账金额
    public static String TRANSFER_AMOUNT = "transferAmount";
    //银行标识,行内转账，行外转账标识0：FPS-Transfer to other banks     1: Bank of China (HK)
    public static String TRANSFER_BANK_TYPE = "bankType";
    //债务方账户类型 0：电话，1：邮箱
    public static String TRANSFER_TYPE = "transferType";
    public static String TRANSFER_ISQRCODE = "isQrCode";
    public static String ORGLIST = "orglist";
    public static String TRANSFER_BANK_NAME = "bankName";
    public static String TRANSFER_REGION = "region";
    public static String TRANSFERTONAME = "transferToName";
    public static String TRANSFERCATEGORY = "transferCategory";
    public static String TRANSFER_COVERID = "coverId";

    //--------FIO--------------//
    public static String FIO_REG_TYPE = "fioRegisterType";
    public static String FIO_PARAM = "fioParam";
    public static String FIO_ID = "fioId";
    public static String FIO_OTP = "otp";

    //bank login
    public static String CODE_HEIGHT = "height";
    public static String CODE_WIDTH = "width";
    public static String CODE_NUM = "codeCount";
    public static String PHONE = "phone";

    public static String CURRENCY = "currency";
    public static String NAME = "name";
    public static String CODE = "code";
    public static String PAN_SHOW_NUM_LIST = "panShowNumList";
    public static String PAN_LIST = "panList";


    //5.1.24我的账户流水查询
    public static String INNEXTTSTMP = "innexttstmp";
    public static String PAGESZ = "pgSz";

    public static String REDEEMPOINT = "redeemPoint";
    public static String GIFTTP = "giftTp";
    public static String REDEEMITEMS = "redeemItems";
    public static String REQACT = "reqAct";
    public static String REDMID = "redmId";
    public static String RECORDTYPE = "recordType";


    public static String EMAIL = "email";

    public static String CHINESSNAME = "chinessName";
    public static String ENGLISHNAME = "englishName";

    public static String CERTNO = "certNo";
    public static String CERTTYPE = "certType";

    public static String BIRTHDAY = "birthday";
    public static String SEX = "sex";
    public static String NATIONALCODE = "nationalCode";
    public static String USERID = "userId";
    public static String AREA = "area";
    public static String STREETNAME = "streetName";
    public static String BUILDINGNAME = "buildingName";
    public static String ROOM = "room";
    public static String SMARTACCLEVEL = "smartAccLevel";
    public static String TAXRESIDENTSTATUS = "taxResidentStatus";
    public static String NORESIDENTRESON = "noResidentReson";
    public static String ISSUEDATE = "issueDate";


    public static String TRANSFERORDERID = "transferOrderId";
    public static String CRCUSTNAMEREMARK = "crCustNameRemark";
    public static String TRANSFERRECENTLYID = "transferRecentlyId";
    public static String SEQNUMBER = "seqNumber";
    public static String FILEID = "fileId";
    public static String DAILYTXNLIMIT = "dailyTxnLimit";
    public static String DAILYLIMIT = "dailyLimit";
    public static String ISHOMEPAGE = "isHomePage";


    public static final String BIND_SMART_ACCOUNT = "BIND_SMART_ACCOUNT";

    public static final String REGISTER_STWO_ACCOUNT = "REGISTER_STWO_ACCOUNT";
    public static final String FORGET_PWD = "FORGET_PWD";

    public static final String BANKCODE = "bankCode";
    public static final String BANKNO = "bankNo";
    public static final String TRANSTYPE = "transType";
    public static final String VERSION = "version";
    public static final String TRANCUR = "tranCur";
    public static final String TRANAMT = "tranAmt";
    public static final String QRCUSECASE = "qRCUseCase";
    public static final String MCC = "mCC";
    public static final String CITY = "city";
    public static final String NAMEAL = "nameAL";
    public static final String CITYAL = "cityAL";
    public static final String CARDNUMBER = "cardNumber";
    public static final String REDEEMGPAMT = "redeemGpAmt";
    public static final String REDEEMFLAG = "redeemFlag";
    public static final String GPCODE = "gpCode";
    public static final String YEARHOLDING = "yearHolding";
    public static final String PAYAMT = "payAmt";
    public static final String GPREQUIRED = "gpRequired";
    public static final String AVAGPCOUNT = "avaGpCount";
    public static final String REDEEMGPCOUNT = "redeemGpCount";
    public static final String VALUE = "value";
    public static final String HKID = "hkId";
    public static final String TOKEN = "token";
    public static final String CHECKSUM = "checkSum";
    public static final String DIGEST = "digest";
    public static final String URL = "url";
    public static final String DATA = "data";
    public static final String CHANNEL = "channel";
    public static final String REQUESTERACCESSTOKEN = "requesterAccessToken";
    public static final String TS = "ts";
    public static final String NONCESTR = "nonceStr";
    public static final String SIGNATURE = "signature";
    public static final String REMARKS = "remarks";
    public static final String AMOUNT = "amount";


    public static final String RECEIVERNAME = "receiverName";
    public static final String CARD_NUMBER = "cardNo";
    public static final String RECEIVERBANKNAME = "receiverbankName";
    public static final String AMOUNTHKD = "amountHKD";
    public static final String AMOUNTCNY = "amountCNY";
    public static final String FORUSED = "forUsed";
    public static final String SENDCARDID = "sendCardId";
    public static final String CARDTYPE = "cardType";
    public static final String REFNO = "refNo";
    public static final String LASTUPDATEDRATE = "lastUpdatedRate";
    public static final String HKD2CNYRATE = "HKD2CNYRate";
    public static final String ISTRANSFERTOSELF = "isTransferToSelf";
    public static final String CAMPAIGNID = "campaignId";
    public static final String BUSINESSTYPE = "businessType";
    public static final String BUSINESSPARAM = "businessParam";

    public static final String NEXTTIME_STAMP = "nextTimeStamp";
    public static final String PAYORDERINFO = "payOrderInfo";
    public static final String SRCREFNO = "srcRefNo";
    public static final String ORDERTYPE = "orderType";
    public static final String LISTTYPE = "listType";
    public static final String GIFTCODE = "giftCode";
    public static final String ORDERTP = "orderTp";
    public static final String STATUS = "status";
    public static final String ACCOUNTID = "accountId";
    public static final String ACCOUNTIDTYPE = "accountIdType";
    public static final String EXTERNALSYSPARAMETER = "externalSysParameter";
    public static final String BILLREFERENCE = "billReference";
    public static final String ISPUSH = "isPush";
    public static final String PARAMETER = "parameter";
    public static final String REDEEMTYPE = "redeemType";
    public static final String NOTIFICATION_JUMP_TYPE = "notificationJumpType";
    public static final String ENTRANCE = "entrance";
    public static final String IFHKPEOPLE = "ifHKPeople";
    public static final String IFOTHERPEOPLE = "ifOtherPeople";
    public static final String TAXRESIDENTINFOLIST = "taxResidentInfoList";
}
