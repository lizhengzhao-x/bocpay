package cn.swiftpass.wallet.intl.module.register.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.radiobutton.MaterialRadioButton;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class TaxInfoQuestionOneHolder extends RecyclerView.ViewHolder implements RadioGroup.OnCheckedChangeListener {

    private final Activity activity;
    @BindView(R.id.rb_question_one_yes)
    MaterialRadioButton rbQuestionOneYes;
    @BindView(R.id.rb_question_one_not)
    MaterialRadioButton rbQuestionOneNot;
    @BindView(R.id.rg_question_one)
    RadioGroup rgQuestionOne;
    @BindView(R.id.iv_tax_info_tip)
    ImageView ivTaxInfoTip;
    @BindView(R.id.tv_resident_area)
    TextView tvResidentArea;
    @BindView(R.id.tv_tax_number)
    TextView tvTaxNumber;
    @BindView(R.id.ll_tax_info)
    LinearLayout llTaxInfo;
    private OnChangeQuestionOneListener listener;
    private S2RegisterInfoEntity registerInfoEntity;

    public TaxInfoQuestionOneHolder(Activity activity, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.activity = activity;
        llTaxInfo.setVisibility(View.GONE);
        rgQuestionOne.setOnCheckedChangeListener(this);
        ivTaxInfoTip.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                String lan = SpUtils.getInstance(activity).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_CN);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_HK);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_US);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, activity.getString(R.string.P3_A1_16_3));
                ActivitySkipUtil.startAnotherActivity(activity, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    public void setChangeQuestionOneListener(OnChangeQuestionOneListener listener) {
        this.listener = listener;
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == rbQuestionOneYes.getId()) {
            llTaxInfo.setVisibility(View.VISIBLE);
            tvResidentArea.setText(activity.getString(R.string.VC04_03_5a));
            if (registerInfoEntity != null) {
                tvTaxNumber.setText(registerInfoEntity.getCertNo());
            }
            if (listener != null) {
                listener.OnChangeQuestionOne(TaxListAdapter.DATA_QUESTION_YES);
            }
        } else {
            llTaxInfo.setVisibility(View.GONE);
            if (listener != null) {
                listener.OnChangeQuestionOne(TaxListAdapter.DATA_QUESTION_NO);
            }
        }
    }

    public void setData(S2RegisterInfoEntity registerInfoEntity, int resultSelectQuestionOne) {
        this.registerInfoEntity = registerInfoEntity;
        if (resultSelectQuestionOne == TaxListAdapter.DATA_QUESTION_YES) {
            rgQuestionOne.check(R.id.rb_question_one_yes);
        }
        if (resultSelectQuestionOne == TaxListAdapter.DATA_QUESTION_NO) {
            rgQuestionOne.check(R.id.rb_question_one_not);
        }
    }

    public interface OnChangeQuestionOneListener {
        void OnChangeQuestionOne(int select);
    }
}
