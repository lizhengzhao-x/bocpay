package cn.swiftpass.wallet.intl.entity;


import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class InviteRecordListEntity extends BaseEntity {


    private List<InviteRecordsBean> inviteRecords;

    public String getUpToLimit() {
        return upToLimit;
    }

    public void setUpToLimit(String upToLimit) {
        this.upToLimit = upToLimit;
    }

    private String upToLimit;

    public List<InviteRecordsBean> getInviteRecords() {
        return inviteRecords;
    }

    public void setInviteRecords(List<InviteRecordsBean> inviteRecords) {
        this.inviteRecords = inviteRecords;
    }

    public static class InviteRecordsBean extends BaseEntity {
        /**
         * id : 15
         * createTime : 2019-05-09 14:57:32
         * reMobile : +86-17620398534
         * reCustName : YU, CHI CHUNG
         * reMobileHidden : +86-176****8534
         */

        private int id;
        private String createTime;
        private String reMobile;
        private String reCustName;
        private String reMobileHidden;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getReMobile() {
            return reMobile;
        }

        public void setReMobile(String reMobile) {
            this.reMobile = reMobile;
        }

        public String getReCustName() {
            return reCustName;
        }

        public void setReCustName(String reCustName) {
            this.reCustName = reCustName;
        }

        public String getReMobileHidden() {
            return reMobileHidden;
        }

        public void setReMobileHidden(String reMobileHidden) {
            this.reMobileHidden = reMobileHidden;
        }

        private String type;

        public String getType() {
            return type;
        }

        public boolean isShowNumber() {
            return showNumber;
        }

        private boolean showNumber;

        public boolean isMatcher() {
            return isMatcher;
        }

        public void setMatcher(boolean matcher) {
            isMatcher = matcher;
        }

        private boolean isMatcher;

        public void setShowNumber(boolean b) {
            this.showNumber = b;
        }
    }
}
