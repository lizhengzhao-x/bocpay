package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.entity.SmaGpInfoBean;
import cn.swiftpass.wallet.intl.module.cardmanagement.view.GradeListAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 积分查询列表弹框 列表查询
 */
public class CheckGradeListPop extends BasePopWindow {

    private TextView mId_grade_title;
    private ImageView mId_close_dialog;
    private androidx.recyclerview.widget.RecyclerView mId_recyclerView;
    private List<SmaGpInfoBean> gradeInfos;

    private void bindViews() {
        mId_grade_title = (TextView) mContentView.findViewById(R.id.id_grade_title);
        mId_close_dialog = (ImageView) mContentView.findViewById(R.id.id_close_dialog);
        mId_recyclerView = (androidx.recyclerview.widget.RecyclerView) mContentView.findViewById(R.id.id_recyclerView);
    }


    public CheckGradeListPop(Activity mActivity, List<SmaGpInfoBean> gradeInfosIn) {
        super(mActivity);
        this.gradeInfos = gradeInfosIn;
        initView();
    }

    @Override
    protected void initView() {
        setContentView(R.layout.pop_grade_list);
        bindViews();
        mId_grade_title.setText(mActivity.getString(R.string.EC01_24));
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.8));
        setHeight((int) (AndroidUtils.getScreenHeight(mActivity) * 0.6));
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerView.setLayoutManager(layoutManager);
        mId_recyclerView.setAdapter(getAdapter(null));
        ((IAdapter<SmaGpInfoBean>) mId_recyclerView.getAdapter()).setData(gradeInfos);
        mId_recyclerView.getAdapter().notifyDataSetChanged();
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, Gravity.CENTER, Gravity.CENTER);
    }


    private CommonRcvAdapter<SmaGpInfoBean> getAdapter(final List<SmaGpInfoBean> data) {
        return new CommonRcvAdapter<SmaGpInfoBean>(data) {

            @Override
            public Object getItemType(SmaGpInfoBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new GradeListAdapter(mActivity, true);
            }
        };
    }

}
