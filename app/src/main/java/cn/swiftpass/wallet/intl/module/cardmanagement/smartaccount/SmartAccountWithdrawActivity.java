package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;


/**
 * 转回余额页面
 */
public class SmartAccountWithdrawActivity extends BaseCompatActivity {


    @BindView(R.id.etwd_balance)
    EditTextWithDel idEditMoney;
    @BindView(R.id.tv_balance)
    TextView tvAvailableBalance;
    @BindView(R.id.tv_error_tip)
    TextView tvAvailableBalanceError;
    @BindView(R.id.tv_all_top_up)
    TextView tvAllBalanceTopUp;
    @BindView(R.id.tv_link_bo_bocpay)
    TextView tvNextPage;
    @BindView(R.id.id_currency)
    TextView idCurrency;
    @BindView(R.id.tv_daily_limit)
    TextView tvDailyLitmit;
    @BindView(R.id.id_account_name)
    TextView id_account_name;
    @BindView(R.id.id_account_number)
    TextView id_account_number;
    @BindView(R.id.iv_toggle_see)
    ImageView toggleSeeIv;
    @BindView(R.id.ll_account_balance)
    LinearLayout accountBalanceLayout;
    private String mMaxAmount = "";
    private String currency;
    private String mSmartLevel;
    private MySmartAccountEntity mSmartAccountInfo;

    private boolean isOpenEyes = false;

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_ewa_01_039);
        EventBus.getDefault().register(this);
        mSmartAccountInfo = (MySmartAccountEntity) getIntent().getExtras().getSerializable(Constants.DATA_SMART_INFO_ACTION);
        mSmartLevel = mSmartAccountInfo.getSmartAcLevel();
        mMaxAmount = mSmartAccountInfo.getBalance();
        currency = mSmartAccountInfo.getCurrency();
        if (mSmartLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            //s2账户
            if (mSmartAccountInfo.getBankCards() != null && mSmartAccountInfo.getBankCards().size() > 0) {
                id_account_name.setText(mSmartAccountInfo.getBankCards().get(0).getBankName() + "(" + AndroidUtils.getFormatBankCardNumber(mSmartAccountInfo.getBankCards().get(0).getBankNo()) + ")");
                //id_account_number.setText(mSmartAccountInfo.getSmartNo());
            }
        } else if (mSmartLevel.equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            //s3账户
            id_account_name.setText(getString(R.string.bank_of_china));
            id_account_number.setText(mSmartAccountInfo.getAccType() + "(" + AndroidUtils.getSubMasCardNumber(mSmartAccountInfo.getRelevanceAccNo()) + ")");
            id_account_number.setVisibility(View.VISIBLE);
        }
        idCurrency.setText(currency);


        idEditMoney.setLineVisible(false);
        updateOkBackground(tvNextPage, false);
        hideAccountBalance();
        idEditMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                tvAvailableBalanceError.setVisibility(View.GONE);
                accountBalanceLayout.setVisibility(View.VISIBLE);
                String moneyStr = idEditMoney.getEditText().getText().toString();
                if (moneyStr.length() == 0) {
                    String amount = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mMaxAmount), true);
                    if (isOpenEyes) {
                        tvAvailableBalance.setText(getString(R.string.P3_C0_1) + ": "  + currency + " " + amount);
                        String str = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
                        tvDailyLitmit.setText(" " + currency + " " + str);
                    } else {
                        hideAccountBalance();
                    }
                    updateOkBackground(tvNextPage, false);
                    return;
                }
                if (!TextUtils.isEmpty(mMaxAmount)) {
                    if (moneyStr.endsWith(".")) {
                        moneyStr = moneyStr.substring(0, moneyStr.length() - 1);
                    }
                    double currentMoney = Double.valueOf(moneyStr);
                    if (currentMoney == 0) {
                        updateOkBackground(tvNextPage, false);
                        return;
                    }
                    if (isOpenEyes) {
                        updateOkBackground(tvNextPage, true);
                        String amount = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mMaxAmount), true);
                        tvAvailableBalance.setText(getString(R.string.P3_C0_1) + ": " + currency + " " + amount);
                        String str = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
                        tvDailyLitmit.setText(" " + currency + " " + str);
                    } else {
                        hideAccountBalance();
                        updateOkBackground(tvNextPage, true);
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //两位小数过滤 8位长度限制
        idEditMoney.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(9)});
        AndroidUtils.showKeyboard(this, idEditMoney.getEditText());
        idEditMoney.getEditText().setTextSize(18);
        tvAllBalanceTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemInitManager.getInstance().isValidShowAccount()) {
                    idEditMoney.setContentText(mMaxAmount);
                    idEditMoney.getEditText().setSelection(mMaxAmount.length());
                } else {
                    VerifyPasswordCommonActivity.startActivityForResult(SmartAccountWithdrawActivity.this, new OnPwdVerifyCallBack() {
                        @Override
                        public void onVerifySuccess() {
                            EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                            SystemInitManager.getInstance().setValidShowAccount(true);
                            idEditMoney.setContentText(mMaxAmount);
                            idEditMoney.getEditText().setSelection(mMaxAmount.length());
                        }

                        @Override
                        public void onVerifyFailed(String errorCode, String errorMsg) {
                        }

                        @Override
                        public void onVerifyCanceled() {

                        }
                    });
                }
            }
        });


    }

    public void toggleSee() {
        if (isOpenEyes) {
            hideAccountBalance();
        } else {
            if (SystemInitManager.getInstance().isValidShowAccount()) {
                tvAvailableBalanceError.setVisibility(View.GONE);
                accountBalanceLayout.setVisibility(View.VISIBLE);
                isOpenEyes = true;
                toggleSeeIv.setImageResource(R.mipmap.icon_eye_open_purple);
                double maxAmount = Double.parseDouble(mMaxAmount);
                String amount = AndroidUtils.formatPriceWithPoint(maxAmount, true);
                tvAvailableBalance.setText(getString(R.string.P3_C0_1) + ": " + currency + " " + amount);
                String str = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
                tvDailyLitmit.setText(" " + currency + " " + str);

                String moneyStr = idEditMoney.getEditText().getText().toString();
                if (moneyStr.length() > 0) {
                    if (moneyStr.endsWith(".")) {
                        moneyStr = moneyStr.substring(0, moneyStr.length() - 1);
                    }
                    try {
                        double currentMoney = Double.parseDouble(moneyStr);
                        if (currentMoney == 0) {
                            updateOkBackground(tvNextPage, false);
                        }
                    } catch (NumberFormatException e) {

                    }
                }
            } else {
                VerifyPasswordCommonActivity.startActivityForResult(SmartAccountWithdrawActivity.this, new OnPwdVerifyCallBack() {
                    @Override
                    public void onVerifySuccess() {
                        EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
                        tvAvailableBalanceError.setVisibility(View.GONE);
                        accountBalanceLayout.setVisibility(View.VISIBLE);
                        SystemInitManager.getInstance().setValidShowAccount(true);
                        isOpenEyes = true;
                        toggleSeeIv.setImageResource(R.mipmap.icon_eye_open_purple);
                        double maxAmount = Double.parseDouble(mMaxAmount);
                        String amount = AndroidUtils.formatPriceWithPoint(maxAmount, true);
                        tvAvailableBalance.setText(getString(R.string.P3_C0_1) + ": "+ currency + " " + amount);
                        String str = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
                        tvDailyLitmit.setText(" " + currency + " " + str);

                        String moneyStr = idEditMoney.getEditText().getText().toString();
                        if (moneyStr.length() > 0) {
                            if (moneyStr.endsWith(".")) {
                                moneyStr = moneyStr.substring(0, moneyStr.length() - 1);
                            }
                            try {
                                double currentMoney = Double.parseDouble(moneyStr);
                                if (currentMoney == 0) {
                                    updateOkBackground(tvNextPage, false);
                                }
                            } catch (NumberFormatException e) {

                            }
                        }

                    }

                    @Override
                    public void onVerifyFailed(String errorCode, String errorMsg) {
                    }

                    @Override
                    public void onVerifyCanceled() {

                    }
                });
            }
        }
    }

    public void hideAccountBalance() {
        isOpenEyes = false;
        toggleSeeIv.setImageResource(R.mipmap.icon_eye_close_purple);
        tvAvailableBalance.setText(getString(R.string.P3_C0_1) + ": " + "*****");
        tvDailyLitmit.setText(" " + "*****");
    }

    @Override
    protected void onStop() {
        super.onStop();
        hideAccountBalance();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_smart_account_withdraw;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            hideAccountBalance();
        }
    }


    private void sTwoUserWithDrawPreEvent() {
        final String money = idEditMoney.getText().toString().trim();
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENCY, currency);
        mHashMaps.put(Constants.MONEY, money);
        //s3体现到主账户 s2体现到银行卡账户
        mHashMaps.put(Constants.PAYMENT_ACCOUNT, mSmartAccountInfo.getBankCards().get(0).getBankNo());
        mHashMaps.put(Constants.ACCTYPE, getIntent().getExtras().getString(Constants.ACCTYPE));
        mHashMaps.put(Constants.MOBILE_PHONE, mSmartAccountInfo.getMobile());
        mHashMaps.put(Constants.IS_FIRST_WITHDRAW, getIntent().getExtras().getBoolean(Constants.IS_FIRST_WITHDRAW));
        mHashMaps.put(Constants.LAVEL, mSmartLevel);
        ActivitySkipUtil.startAnotherActivity(SmartAccountWithdrawActivity.this, SmartAccountWithDrawDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }


    @OnClick(R.id.tv_link_bo_bocpay)
    public void onViewClicked() {
        if (ButtonUtils.isFastDoubleClick()) return;
        if (mSmartLevel.equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            sTwoUserWithDrawPreEvent();
        } else {
            sThreeUserWithDrawPreCheck();
        }


    }

    private void sThreeUserWithDrawPreCheck() {
        withDrawDetailEvent();

    }

    private void withDrawDetailEvent() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENCY, currency);
        mHashMaps.put(Constants.MONEY, idEditMoney.getText().toString().trim());
        //s3体现到主账户 s2体现到银行卡账户
        mHashMaps.put(Constants.PAYMENT_ACCOUNT, mSmartAccountInfo.getRelevanceAccNo());
        mHashMaps.put(Constants.ACCTYPE, mSmartAccountInfo.getAccType());
        mHashMaps.put(Constants.LAVEL, mSmartLevel);
        mHashMaps.put(Constants.IS_FIRST_WITHDRAW, false);
        ActivitySkipUtil.startAnotherActivity(SmartAccountWithdrawActivity.this, SmartAccountWithDrawDetailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @OnClick({R.id.ll_day_balance_limit,R.id.tv_balance, R.id.iv_toggle_see})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_balance:
            case R.id.iv_toggle_see:
            case R.id.ll_day_balance_limit:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_REVERSAL_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                toggleSee();
                break;
            default:
                break;
        }
    }
}
