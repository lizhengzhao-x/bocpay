package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class ContractListEntity extends BaseEntity {


    private List<RecentlyBean> recently;

    public List<RecentlyBean> getRecently() {
        return recently;
    }

    public void setRecently(List<RecentlyBean> recently) {
        this.recently = recently;
    }

    public static class RecentlyBean extends BaseEntity {

        public static final String UI_RECENTLY = "0";
        public static final String UI_FREQUENT = "1";
        public static final String UI_ADDRESSBOOK = "2";
        public static final String UI_TITLE = "3";
        /**
         * 有一种类型是 通讯录中 常用联系人中都要显示
         */
        public static final String UI_ADDRESS_FREQUENT = "4";

        /**
         * disPhoneNo : 86 13923***529
         * id : 1
         * disCustName : CHLOE C***
         * phoneNo : 86-13923740529
         */
        public String getUiType() {
            return uiType;
        }

        public void setType(String type) {
            this.uiType = type;
        }

        private String uiType;


        private String disPhoneNo;
        private int id;
        private String disCustName;
        private String phoneNo;

        public boolean isHasChanged() {
            if (TextUtils.isEmpty(modifyName)) {
                return false;
            }
            return modifyName.equals("1");
        }


        /**
         * 昵称是否修改过
         */
        private boolean hasChanged;

        public String getModifyName() {
            return modifyName;
        }

        public void setModifyName(String modifyName) {
            this.modifyName = modifyName;
        }

        /**
         * 1：展示 0：不展示
         */
        private String modifyName;


        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getDisEmail() {
            return disEmail;
        }

        public void setDisEmail(String disEmail) {
            this.disEmail = disEmail;
        }

        private String email;
        private String disEmail;

        public String getType() {
            if (TextUtils.isEmpty(type)) {
                return "0";
            }
            return type;
        }


        private String type;

        /**
         * 最近转账列表 返回邮箱type=1代表邮箱格式 通讯录里头列表需要转一下这个参数
         *
         * @param type
         */
        public void setEmailType(String type) {
            this.type = type;
        }

        public String getTransType() {
            return transType;
        }

        public void setTransType(String transType) {
            this.transType = transType;
        }

        private String transType;


        public boolean isEmail() {
            if (TextUtils.isEmpty(type)) {
                return false;
            }
            return TextUtils.equals(type, "1");
        }

        public void setEmail(boolean email) {
            isEmail = email;
        }

        private boolean isEmail;

        public String getUiShowType() {
            return uiShowType;
        }

        public void setUiShowType(String uiShowType) {
            this.uiShowType = uiShowType;
        }

        /**
         * 区分最近转账联络人 0 /常用联络人/通讯录
         */
        private String uiShowType;

        public String getUiTitle() {
            return uiTitle;
        }

        public void setUiTitle(String uiTitle) {
            this.uiTitle = uiTitle;
        }

        /**
         * 分组显示的标题
         */
        private String uiTitle;

        public String getUiContent() {
            return uiContent;
        }

        public void setUiContent(String uiContent) {
            this.uiContent = uiContent;
        }

        private String uiContent;

        public String getDisPhoneNo() {
            return disPhoneNo;
        }

        public void setDisPhoneNo(String disPhoneNo) {
            this.disPhoneNo = disPhoneNo;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDisCustName() {
            return disCustName;
        }

        public String getPhoneNo() {
            return phoneNo;
        }

        /**
         * 模糊搜索 去除空格
         *
         * @return
         */
        public String getPhoneNoMapping() {
            if (TextUtils.isEmpty(phoneNo)) {
                return "";
            }
            return phoneNo.replace(" ", "").replace("-", "");
        }

        /**
         * 模糊搜索 去除空格
         *
         * @return
         */
        public String getdisPhoneNoMapping() {
            if (TextUtils.isEmpty(disPhoneNo)) {
                return "";
            }
            return disPhoneNo.replace(" ", "");
        }


        public String getDisCustNameMapping() {
            if (TextUtils.isEmpty(disCustName)) {
                return "";
            }
            return disCustName.replace(" ", "");
        }

        public void setDisCustName(String disCustName) {
            this.disCustName = disCustName;
        }

        public void setPhoneNo(String phoneNo) {
            this.phoneNo = phoneNo;
        }

        public String getContactID() {
            return contactID;
        }

        public void setContactID(String contactID) {
            this.contactID = contactID;
        }

        private String contactID;

        public boolean isCollect() {
            return isCollect;
        }

        public void setCollect(boolean collect) {
            isCollect = collect;
        }

        /**
         * 是否是收藏状态
         */
        private boolean isCollect;


    }
}
