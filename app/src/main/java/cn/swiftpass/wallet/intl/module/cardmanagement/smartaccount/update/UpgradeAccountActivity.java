package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update;


import android.view.View;
import android.widget.TextView;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.RegisterBankActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

public class UpgradeAccountActivity extends BaseCompatActivity implements View.OnClickListener {

    private TextView tv_cancel, tv_upgrade;
    private static final int REQUEST_IDV_SDK = 30000;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_ewa_01_028);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_upgrade = (TextView) findViewById(R.id.tv_upgrade);
        tv_cancel.setOnClickListener(this);
        tv_upgrade.setOnClickListener(this);

    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_upgrade_account;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_cancel:
                finish();
                break;
            case R.id.tv_upgrade:
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.SMART_ACCOUNT_UPDATE);
                ActivitySkipUtil.startAnotherActivity(this, RegisterBankActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            default:
                break;
        }
    }
}
