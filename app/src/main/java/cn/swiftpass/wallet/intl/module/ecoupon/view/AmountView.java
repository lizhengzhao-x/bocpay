package cn.swiftpass.wallet.intl.module.ecoupon.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;


public class AmountView extends LinearLayout {

    private Context mContext;
    private ImageView subIv;
    private EditText numberEd;
    private ImageView addIv;
    private int mMaxCount, mMinCount, mDefaultCount;

    public void setmCurrentCount(int mCurrentCount) {
        this.mCurrentCount = mCurrentCount;
        updateCnt();
    }

    private int mCurrentCount;
    private OnCountChangeListener changeListener;

    public void setChangeListener(OnCountChangeListener changeListener) {
        this.changeListener = changeListener;
    }


    public AmountView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }


    public AmountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    public AmountView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, defStyle);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.view_amount, this);
        subIv = (ImageView) rootView.findViewById(R.id.iv_sub);
        numberEd = (EditText) rootView.findViewById(R.id.ed_number);
        addIv = (ImageView) rootView.findViewById(R.id.iv_add);
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.AmountView, defStyle, 0);

        mMaxCount = a.getInt(R.styleable.AmountView_a_max_count, 100);
        mMinCount = a.getInt(R.styleable.AmountView_a_min_count, 0);
        mDefaultCount = a.getInt(R.styleable.AmountView_default_count, 0);

        a.recycle();
        mCurrentCount = mDefaultCount;
        updateCnt();
        subIv.setImageResource(R.mipmap.icon_card_min_notavailable);
        addViewListener();


    }

    private void addViewListener() {
        subIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                numberEd.clearFocus();
                subEvent();
            }
        });

        addIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                numberEd.clearFocus();
                addEvent();
            }
        });
        numberEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (TextUtils.isEmpty(s)) {
                        numberEd.setText(Constants.ZERA_STR);
                        numberEd.setSelection(1);
                        return;
                    }
                    String text = s.toString();
                    int len = s.toString().length();
                    if (len > 1 && text.startsWith(Constants.ZERA_STR)) {
                        String replaceStr = text.replaceFirst(Constants.ZERA_STR, "");
                        numberEd.setText(replaceStr);
                        numberEd.setSelection(replaceStr.length());
                        return;
                    }

                    int countText = Integer.parseInt(String.valueOf(s));
                    mCurrentCount = countText;
                    if (mCurrentCount <= mMinCount) {
                        subIv.setImageResource(R.mipmap.icon_card_min_notavailable);
                        subIv.setEnabled(false);
                    } else {
                        subIv.setImageResource(R.mipmap.icon_card_min);
                        subIv.setEnabled(true);
                    }
                    if (mCurrentCount >= mMaxCount) {
                        addIv.setImageResource(R.mipmap.icon_card_plus_notavailable);
                        addIv.setEnabled(false);
                    } else {
                        addIv.setImageResource(R.mipmap.icon_card_plus);
                        addIv.setEnabled(true);
                    }
                    if (changeListener!=null){
                        changeListener.onCountChanged(countText);
                    }
                } catch (Exception e) {
                    if (BuildConfig.isLogDebug) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void updateCnt() {
        numberEd.setText(String.valueOf(mCurrentCount));
        if (mCurrentCount >= mMaxCount) {
            addIv.setImageResource(R.mipmap.icon_card_plus_notavailable);
            addIv.setEnabled(false);
        }
        if (mCurrentCount <= mMinCount) {
            subIv.setImageResource(R.mipmap.icon_card_min_notavailable);
            subIv.setEnabled(false);
        }
    }

    public int getCurrentCount() {
        return mCurrentCount;
    }

    private void subEvent() {
        subIv.setFocusable(true);
        subIv.setImageResource(R.mipmap.icon_card_min);
        mCurrentCount--;

        if (mCurrentCount <= mMinCount) {
            subIv.setImageResource(R.mipmap.icon_card_min_notavailable);
            changeListener.onCountChanged(mCurrentCount);
            subIv.setEnabled(false);
            mCurrentCount = mMinCount;
        }
        if (mCurrentCount >= mMaxCount) {
            mCurrentCount = mMaxCount;
        }
        numberEd.setText(String.valueOf(mCurrentCount));
        addIv.setImageResource(R.mipmap.icon_card_plus);
        addIv.setEnabled(true);
        if (changeListener != null) {
            changeListener.onCountChanged(mCurrentCount);
        }
    }

    private void addEvent() {
        addIv.setFocusable(true);
        subIv.setImageResource(R.mipmap.icon_card_min);
        subIv.setEnabled(true);

        addIv.setImageResource(R.mipmap.icon_card_plus);
        mCurrentCount++;

        if (mCurrentCount >= mMaxCount) {
            addIv.setImageResource(R.mipmap.icon_card_plus_notavailable);
            addIv.setEnabled(false);
            mCurrentCount = mMaxCount;
        }
        if (mCurrentCount <= mMinCount) {
            mCurrentCount = mMinCount;
        }

        numberEd.setText(String.valueOf(mCurrentCount));
        if (changeListener != null) {
            changeListener.onCountChanged(mCurrentCount);
        }
    }


    public void setMaxCount(String geteVoucherStock) {
        int maxCount = 0;
        try {
            maxCount = Integer.valueOf(geteVoucherStock);
        } catch (NumberFormatException e) {
            if (!BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        mMaxCount = maxCount;
    }

    public interface OnCountChangeListener {

        void onCountChanged(int count);

    }
}
