package cn.swiftpass.wallet.intl.module.cardmanagement.fps;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 绑定FPS的银行列表
 */

public class FpsBankRecordAdapter implements AdapterItem<FpsBankRecordBean> {

    private View mRootView;
    private TextView mRegId;
    private TextView mAccountTypeTV;
    private TextView mAccountNoTV;
    private TextView mDefaultTV;
    private TextView mStatusTV;
    private TextView mRecordTV;
    private TextView mDeleteTV;

    private int position;
    private Context mContext;

    private BankRecordCallback mCallback;

    public FpsBankRecordAdapter(Context context, BankRecordCallback callback) {
        mCallback = callback;
        mContext = context;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.fps_bank_record_item;
    }

    @Override
    public void bindViews(View root) {
        mRootView = root;
        mRegId = root.findViewById(R.id.tv_reg_id);
        mAccountTypeTV = root.findViewById(R.id.tv_account_type);
        mAccountNoTV = root.findViewById(R.id.tv_account_no);
        mDefaultTV = root.findViewById(R.id.tv_default_record);
        mStatusTV = root.findViewById(R.id.tv_fps_record_status);
        mRecordTV = root.findViewById(R.id.tv_other_record);
        mDeleteTV = root.findViewById(R.id.tv_delete_record);
    }

    @Override
    public void setViews() {
        mDeleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onDelete(position);
                }
            }
        });

        mRecordTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCallback != null) {
                    mCallback.onRecord(position);
                }
            }
        });

    }

    @Override
    public void handleData(FpsBankRecordBean entity, int position) {
        this.position = position;
        if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(entity.getAccountIdType())) {
            mRegId.setText(AndroidUtils.fpsEmailFormat(entity.getAccountId()));
        } else {
            mRegId.setText(AndroidUtils.fpsPhoneFormat(entity.getAccountId()));
        }
        mAccountNoTV.setText(entity.getAccountTypeName() + AndroidUtils.getSubNumberBrackets(entity.getAccountNo()));
        mDefaultTV.setText(AndroidUtils.getDefaultBankFlag(entity.getDefaultBank()));
        mStatusTV.setText(entity.getAccountStatus());
    }

    public interface BankRecordCallback {

        void onDelete(int position);

        void onRecord(int position);
    }
}
