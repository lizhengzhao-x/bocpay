package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 查询其他行的fps绑定记录
 */

public class FpsQueryBankOtherRecords extends BaseProtocol {
    private String mAccountId;
    private String mAccountIdType;

    public FpsQueryBankOtherRecords(String accountId, String accountIdType, NetWorkCallbackListener dataCallback) {
        mAccountId = accountId;
        mAccountIdType = accountIdType;
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/queryItsRowRecord";

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID, mAccountId);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID_TYPE, mAccountIdType);
    }
}
