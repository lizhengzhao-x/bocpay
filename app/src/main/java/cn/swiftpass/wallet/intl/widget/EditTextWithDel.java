package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.intl.R;

/**
 * 下划线 删除图标
 */
public class EditTextWithDel extends LinearLayout {
    /**
     * 正则表达式
     */
    private Context context;
    private LinearLayout ll_EditTextWithDel;
    private EditText mEditText;

    public ImageView getmDelImageView() {
        return mDelImageView;
    }

    private ImageView mDelImageView;
    private LinearLayout ll_Image_layout;
    private LinearLayout ll_line;
    private LinearLayout ll_line_red;
    private LinearLayout ll_errorTextView;
    private TextView mErrorTextView;
    private TextView mLeftTitleView;
    /**
     * 是否显示错误提示的view,在定制EditText的某些情况下不需要显示ErrorView
     */
    private boolean hasErrorView = true;
    /**
     * 是否显示底部线条
     */
    private boolean hasBottomLine = true;

    public void setHasDelView(boolean hasDelView) {
        this.hasDelView = hasDelView;
    }

    /**
     * 是否显示删除图标 ,在定制EditText的某些情况下需要永远的不显示删除按钮则此值为false
     */
    private boolean hasDelView = true;
    private TextWatcher mTextWatcher;
    /**
     * 所有要满足的正则表达式的列表
     */
    private List<RegExpBean> mRegExp = new ArrayList<RegExpBean>();
    /**
     * 对字符串长度的正则，如果长度不满足就显示errorMsg
     */
    private RegExpBean mLengthRegExp = null;
    private LayoutInflater mInflater;
    /**
     * 属性设置采用dp
     */
    private int mLeftPadding;
    private int mRightPadding;

    private boolean isFocus;
    /**
     * 是否是金额的输入框 0.09 009的问题
     */
    private boolean mMoneyFormat;

    public EditTextWithDel.onFocusChangeListener getOnFocusChangeExtraListener() {
        return onFocusChangeExtraListener;
    }

    public void setOnFocusChangeExtraListener(EditTextWithDel.onFocusChangeListener onFocusChangeExtraListener) {
        this.onFocusChangeExtraListener = onFocusChangeExtraListener;
    }

    /**
     * 焦点问题 外部也需要处理 需要控件本身处理完之后将事件传出去
     */
    private onFocusChangeListener onFocusChangeExtraListener;

    public EditTextWithDel(Context context) {
        super(context);
        init(context, null, 0);
    }

    public EditTextWithDel(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public EditTextWithDel(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public class RegExpBean {
        public RegExpBean(String mRegExp, String mErrorMsg) {
            this(mRegExp, mErrorMsg, true);
        }

        public RegExpBean(String mRegExp, String mErrorMsg, boolean isNotRegExp) {
            this.mErrorMsg = mErrorMsg;
            this.mRegExp = mRegExp;
            this.regExpIsTrue = isNotRegExp;
        }

        public String mRegExp;
        public String mErrorMsg;
        /**
         * 正则表达式有两种情况，第一种是要符合正则表达式的条件就算通过，另外一种是不符合正则表达式的条件才算通过
         * 假设条件是要限制输入的不能全部为数字，传入^[\\d)+$表示全为数字，让regExpIsTrue=false，
         * 此时正则通过则表示不能全为数字。默认是正向的。
         */
        public boolean regExpIsTrue = true;
    }

    public TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (mTextWatcher != null) {
                mTextWatcher.beforeTextChanged(s, start, count, after);
            }
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //TODO FIX XINCHAO 在这里有一个问题 例如0.36 用户删除. 变成了036 需要要求要变成36 通过inputFilter无法处理 需要在onTextChanged处理如果用了这个控件 会有这个效果
            String str = String.valueOf(s);
            if (mMoneyFormat && !str.contains(".") && str.startsWith("0") && str.length() != 1) {
                str = str.substring(1);
                //这里有个问题 0.06 删除点之后 变成 06
                mEditText.setText(Integer.valueOf(str) + "");
            }
            if (mTextWatcher != null) {
                mTextWatcher.onTextChanged(str, start, before, count);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mTextWatcher != null) {
                mTextWatcher.afterTextChanged(s);
            }
            if (TextUtils.isEmpty(s.toString())) {
                hideDelView();
            } else {
                if (isFocus) {
                    showDelView();
                }
            }
        }
    };

    public void addTextChangedListener(TextWatcher watcher) {
        mTextWatcher = watcher;
    }

    public OnClickListener delClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mEditText.setText(null);
        }
    };

    public void init(Context context, AttributeSet attrs, int defStyle) {
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.edit_text_with_del, null);
        addView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        ll_EditTextWithDel = view.findViewById(R.id.llayout_edit_text);
        mEditText = view.findViewById(R.id.edit_text);
        mDelImageView = view.findViewById(R.id.iv_del);
        mLeftTitleView = view.findViewById(R.id.id_left_title);
        ll_Image_layout = view.findViewById(R.id.ll_Image_layout);
        ll_line = view.findViewById(R.id.ll_line_common);
        ll_line_red = view.findViewById(R.id.ll_line_red);
        ll_errorTextView = view.findViewById(R.id.ll_errorTextView);
        mErrorTextView = view.findViewById(R.id.tv_error);
        mEditText.addTextChangedListener(textWatcher);
        mDelImageView.setOnClickListener(delClickListener);
        mEditText.setFocusableInTouchMode(true);
        mEditText.setOnFocusChangeListener(onFocusChangeListener);
        mInflater = LayoutInflater.from(context);
        final Resources.Theme theme = context.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.EditTextWithDel, defStyle, 0);
        //以下的部分为默认的值，如有需要可以定义方法可以修改下面的值
        int maxLength = -1;
        float textSize = 15;
        CharSequence text = "";
        CharSequence hint = null;
        boolean singleLine = false;
        boolean showLeftTitle = false;
        int ellipsize = -1;
        int inputType = EditorInfo.TYPE_NULL;
        ColorStateList textColor = null;
        ColorStateList textColorHint = null;
        int n = a.getIndexCount();
        //设置默认的值
        for (int i = 0; i < n; i++) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.EditTextWithDel_hasErrorView) {
                hasErrorView = a.getBoolean(attr, true);
            } else if (attr == R.styleable.EditTextWithDel_hasBottomLine) {
                hasBottomLine = a.getBoolean(attr, true);
            } else if (attr == R.styleable.EditTextWithDel_hasDelView) {
                hasDelView = a.getBoolean(attr, true);
            } else if (attr == R.styleable.EditTextWithDel_textSize) {
                textSize = a.getDimension(attr, textSize);
                mEditText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            } else if (attr == R.styleable.EditTextWithDel_maxLength) {
                maxLength = a.getInt(attr, -1);
            } else if (attr == R.styleable.EditTextWithDel_hasLeftTitle) {
                showLeftTitle = a.getBoolean(attr, showLeftTitle);
            } else if (attr == R.styleable.EditTextWithDel_singleLine) {
                singleLine = a.getBoolean(attr, singleLine);
            } else if (attr == R.styleable.EditTextWithDel_minLines) {
                mEditText.setMinLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_maxLines) {
                mEditText.setMaxLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_lines) {
                mEditText.setLines(a.getInt(attr, -1));
            } else if (attr == R.styleable.EditTextWithDel_textColor) {
                textColor = a.getColorStateList(attr);
            } else if (attr == R.styleable.EditTextWithDel_textColorHint) {
                textColorHint = a.getColorStateList(attr);
            } else if (attr == R.styleable.EditTextWithDel_textIsSelectable) {
                mEditText.setTextIsSelectable(a.getBoolean(attr, false));
            } else if (attr == R.styleable.EditTextWithDel_letterSpacing) {
            } else if (attr == R.styleable.EditTextWithDel_ellipsize) {
                ellipsize = a.getInt(attr, ellipsize);
            } else if (attr == R.styleable.EditTextWithDel_inputType) {
                inputType = a.getInt(attr, EditorInfo.TYPE_NULL);
            } else if (attr == R.styleable.EditTextWithDel_hint) {
                hint = a.getText(attr);
            } else if (attr == R.styleable.EditTextWithDel_text) {
                text = a.getText(attr);
            } else if (attr == R.styleable.EditTextWithDel_regExpText) {
                //mRegExp.add(a.getText(attr).toString());
            } else if (attr == R.styleable.EditTextWithDel_errorText) {
                //mErrorMsg.add(a.getText(attr).toString());
            } else if (attr == R.styleable.EditTextWithDel_isMoneyFormat) {
                //mErrorMsg.add(a.getText(attr).toString());
                mMoneyFormat = a.getBoolean(attr, false);
            }
        }
        switch (ellipsize) {
            case 1:
                mEditText.setEllipsize(TextUtils.TruncateAt.START);
                break;
            case 2:
                mEditText.setEllipsize(TextUtils.TruncateAt.MIDDLE);
                break;
            case 3:
                mEditText.setEllipsize(TextUtils.TruncateAt.END);
                break;
            case 4:
                mEditText.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                break;
            default:
                break;
        }
        if (textColor != null) {
            mEditText.setTextColor(textColor);
        }
        if (textColorHint != null) {
            mEditText.setHintTextColor(textColorHint);
        }

        if (maxLength >= 0) {
            mEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
        }

        if (showLeftTitle) {
            mLeftTitleView.setVisibility(VISIBLE);
        } else {
            mLeftTitleView.setVisibility(GONE);
        }
        if (!TextUtils.isEmpty(text)) {
            mEditText.setText(text);
        }
        if (hint != null) {
            mEditText.setHint(hint);
        }
        mEditText.setSingleLine(singleLine);
        //必须放到setSingleLine后，否则密码不能隐藏
        if (inputType != EditorInfo.TYPE_NULL) {
            mEditText.setInputType(inputType);
        }
        //mLeftPadding = a.getDimensionPixelSize(R.styleable.EditTextWithDel_paddingLeft, 20);
        //TODO 为什么这里有20padding ?
        mLeftPadding = a.getDimensionPixelSize(R.styleable.EditTextWithDel_paddingLeft, 0);
        mRightPadding = a.getDimensionPixelSize(R.styleable.EditTextWithDel_paddingRight, 20);
        mEditText.getPaddingBottom();
        mEditText.setPadding(mLeftPadding, mEditText.getPaddingTop(), mRightPadding, mEditText.getPaddingBottom());

        if (!hasBottomLine) {
            ll_line.setVisibility(GONE);
            ll_line_red.setVisibility(GONE);
        }

        a.recycle();
    }

    InputFilter mInputFilter = new InputFilter() {
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            return source.toString().replace("\n", "");
        }

    };

    /***
     * 正则判断表达式字符的长度
     * addLengthRegExp(getString(R.string.reg_exp_length, 4, 20), getString(R.string.error_member_nickname));
     * 注意要判断中文的长度，一个中文等于2个字符
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addLengthRegExp(String mRegExp, String mErrorMsg) {
        mLengthRegExp = new RegExpBean(mRegExp, mErrorMsg);
    }

    /**
     * 设置正则表达式
     *
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addRegExp(String mRegExp, String mErrorMsg) {
        this.mRegExp.add(new RegExpBean(mRegExp, mErrorMsg));
    }

    /**
     * 设置正则表达式
     *
     * @param mRegExp
     * @param mErrorMsg
     */
    public void addRegExpIsFalse(String mRegExp, String mErrorMsg) {
        this.mRegExp.add(new RegExpBean(mRegExp, mErrorMsg, false));
    }

    public String getHint() {
        if (mEditText == null) {
            return "";
        }
        return mEditText.getHint().toString().trim();
    }

    public void setHint(String HintString) {

        if (HintString != null) {
            mEditText.setHint(HintString);
        }
    }

    /**
     * 开始正则
     *
     * @return 是否正则成功
     */
    public boolean onRegExp(boolean isShowErrorView) {

        if (mLengthRegExp != null) {//对字符长度的正则
            //即ASCII编码不在0-255的字符,也就是汉字转换成两个字符来判断长度
            String newStr = mEditText.getText().toString().replaceAll("[^\\x00-\\xff]", "**");
            boolean res = onOneRegExp(mLengthRegExp, newStr, isShowErrorView);
            if (!res) {
                return res;
            }
        }
        //如果长度正则通过才进行下面的条件正则
        for (int i = 0; i < mRegExp.size(); i++) {
            RegExpBean regExp = mRegExp.get(i);

            boolean res = onOneRegExp(regExp, mEditText.getText().toString(), isShowErrorView);
            if (!res) {
                return res;
            }
        }
        hideErrorView();
        return true;
    }

    private boolean onOneRegExp(RegExpBean regExp, String text, boolean isShowErrorView) {
        if (!TextUtils.isEmpty(regExp.mRegExp)) {
            if (matcherRegExp(regExp.mRegExp, text) != regExp.regExpIsTrue) {
                if (isShowErrorView) {
                    showErrorViewWithMsg(regExp.mErrorMsg);
                }
                return false;
            }
        }
        return true;
    }

    public boolean matcherRegExp(String regExp, String str) {
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(str);
        return matcher.matches();
    }

    private OnFocusChangeListener onFocusChangeListener = new OnFocusChangeListener() {

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            isFocus = hasFocus;
            if (hasFocus) {
                mEditText.setCursorVisible(true);
                hideErrorView();
                if (hasDelView) {
                    mDelImageView.setVisibility(mEditText.getText().toString().length() > 0 ? VISIBLE : GONE);
                } else {
                    mDelImageView.setVisibility(GONE);
                }
                ll_line.setBackgroundColor(getContext().getResources().getColor(R.color.app_black));
            } else {
                onRegExp(true);
                mDelImageView.setVisibility(GONE);
                ll_line.setBackgroundColor(getContext().getResources().getColor(R.color.line_main));
            }
            if (onFocusChangeExtraListener != null) {
                //外部view处理
                onFocusChangeExtraListener.onFocusChange(hasFocus);
            }
        }
    };


    /**
     * 左边文案
     *
     * @param title
     */
    public void setLeftTitle(String title) {
        if (!TextUtils.isEmpty(title)) {
            mLeftTitleView.setText(title);
        }
    }

    /**
     * EditText 获取焦点的时候 底部的线条变黑
     */
    public void setBottomLineHighSel() {

        ll_line.setBackgroundColor(getContext().getResources().getColor(R.color.app_black));
    }

    public void setBottomLineNoSel() {

        ll_line.setBackgroundColor(getContext().getResources().getColor(R.color.line_main));
    }

    public void showDelView() {
        if (mDelImageView != null) {
            if (hasDelView) {
                mDelImageView.setVisibility(VISIBLE);
            } else {
                mDelImageView.setVisibility(GONE);
            }
        }
    }

    public void hideDelView() {
        if (mDelImageView != null) {
            mDelImageView.setVisibility(GONE);
            // hasDelView = false;
        }
    }

    public void showErrorViewWithMsg(String errorMsg) {
        if (mErrorTextView != null && !TextUtils.isEmpty(errorMsg)) {
            mErrorTextView.setText(errorMsg);
            if (hasErrorView) {
                ll_errorTextView.setVisibility(VISIBLE);
                mErrorTextView.setVisibility(VISIBLE);
            } else {
                ll_errorTextView.setVisibility(GONE);
                mErrorTextView.setVisibility(GONE);
            }
        }
    }

    public void hideErrorView() {
        if (mErrorTextView != null) {
            ll_errorTextView.setVisibility(GONE);
            mErrorTextView.setVisibility(GONE);
        }
    }

    /**
     * 获取输入文字，如果正则不通过则返回null,默认显示错误信息
     *
     * @return
     */
    public String getText() {
        return getText(true);
    }

    /**
     * 获取输入文字，如果正则不通过则返回null
     *
     * @param isShowErrorView 是否显示错误提示信息
     * @return
     */
    public String getText(boolean isShowErrorView) {
        if (mEditText != null) {
            if (onRegExp(isShowErrorView)) {
                return mEditText.getText().toString();
            }
        }
        return null;
    }

    /**
     * 设置内容
     *
     * @param text
     */
    public void setContentText(String text) {
        if (mEditText != null) mEditText.setText(text);
    }

    /**
     * 光标放到末尾
     */
    public void setSelectionLast() {
        if (mEditText != null) {
            mEditText.setSelection(mEditText.getText().length());
        }
    }

    public EditText getTextView() {
        return mEditText;
    }

    /**
     * 设置为输入框加入其它的imageView
     *
     * @param id image的ID，用来显示图片
     */
    public ImageView addRightImageView(int id, boolean hasLeftPadding, int dimension, OnClickListener mListener) {
        ImageView iv = getImageView(id);
        int leftPadding = 0;
        if (hasLeftPadding) {
            int spaceHor = context.getResources().getDimensionPixelSize(dimension);
            leftPadding = spaceHor;
        }
        iv.setPadding(leftPadding, 0, 0, 0);
        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        ll_Image_layout.addView(iv, layoutParams);

        //用来设置新增的图标按钮点击的监听，点击之后回调
        if (mListener != null) {
            iv.setOnClickListener(mListener);
        }
        return iv;
    }


    public EditText getEditText() {
        return mEditText;
    }

    public ImageView getImageView(int id) {
        ImageView iv = (ImageView) mInflater.inflate(R.layout.title_imageview, null);
        iv.setImageResource(id);
        return iv;
    }

    public void showPasswordText() {
        mEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    public void hidePasswordText() {
        mEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        mEditText.postInvalidate();
        mEditText.setSelection(mEditText.length());//将光标追踪到内容的最后
    }

    //为整个EditText设置背景,如果需要为editText设置边框或者背景
    public void setEditTextBackgroundDrawable(int resid) {
        ll_EditTextWithDel.setBackgroundResource(resid);
    }

    //如果更改EditText的背景，或者输入正则不通过，记得要把线条设置成GONE
    public void setLineVisible(boolean isVisible) {
        ll_line.setVisibility(isVisible ? VISIBLE : GONE);
    }

    //如果更改EditText的背景，或者输入正则不通过，记得要把线条设置成GONE
    public void setEditTextEnable(boolean enable) {
        mEditText.setEnabled(enable);
    }

    //如果更改EditText的背景，或者输入正则不通过，记得要把线条设置成VISIBLE
    public void setLineRedVisible(boolean isVisible) {
        ll_line_red.setVisibility(isVisible ? VISIBLE : GONE);
    }

    //是否显示删除按钮
    public void showDeleteView(Boolean show) {
        hasDelView = show;
    }

    //是否显示出错的提示框
    public void showErrorView(Boolean show) {
        hasErrorView = show;
    }

    //设置editText的文字的显示发gravity，是居中还是靠左，还是
    public void setEditTextGravity(int textGravity) {
        mEditText.setGravity(textGravity);
    }

    /**
     * 内部外部viewd都需要事件监听
     */
    public interface onFocusChangeListener {

        void onFocusChange(boolean hasFocus);
    }

}
