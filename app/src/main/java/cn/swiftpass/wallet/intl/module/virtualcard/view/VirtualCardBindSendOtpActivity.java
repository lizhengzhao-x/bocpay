package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.os.Bundle;
import android.view.View;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.BindVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardVeryOtpEntiy;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.BindVitualCardSendOtpPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardBindSendOtpActivity extends AbstractSendOTPActivity implements BindVitualCardSendOtpContract.View {
    private VirtualCardListEntity.VirtualCardListBean virtualCardListBean;
    private String mCardId;
    private BindVitualCardSendOtpContract.Presenter mPresenterDetail;
    private int currentActionType;

    @Override
    public void init() {
        virtualCardListBean = (VirtualCardListEntity.VirtualCardListBean) getIntent().getExtras().getSerializable(Constants.VIRTUALCARDLISTBEAN);
        currentActionType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        phoneNo = virtualCardListBean.getAccount();
        if (currentActionType == Constants.VITUALCARD_CHECK_DETAIL) {
            setToolBarTitle(R.string.VC01_02_1);
        } else if (currentActionType == Constants.VITUALCARD_BIND) {
            setToolBarTitle(R.string.IDV_3_1a);
            setBackBtnListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    AndroidUtils.showConfrimExitDialog(VirtualCardBindSendOtpActivity.this, new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskExcludeMainStack();
//                            ActivitySkipUtil.startAnotherActivity(VirtualCardBindSendOtpActivity.this, LoginActivity.class);
                        }
                    });
                }
            });
        }
        if (mPresenter instanceof BindVitualCardSendOtpContract.Presenter) {
            mPresenterDetail = (BindVitualCardSendOtpContract.Presenter) mPresenter;
        }
        super.init();
    }

    @Override
    protected BindVitualCardSendOtpContract.Presenter createPresenter() {
        return new BindVitualCardSendOtpPresenter();
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.bindVitualCardVerifyOtp(virtualCardListBean, currentActionType == Constants.VITUALCARD_BIND ? ApiConstant.VIRTUALCARD_BIND : ApiConstant.VIRTUALCARD_QUERY, etwdOtpCode.getEditText().getText().toString().trim(), mCardId);
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {
        mPresenterDetail.bindVitualCardSenOtp(virtualCardListBean, currentActionType == Constants.VITUALCARD_BIND ? ApiConstant.VIRTUALCARD_BIND : ApiConstant.VIRTUALCARD_QUERY, isTryAgain);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void bindVitualCardSenOtpSuccess(VirtualCardSendOtpEntity response, boolean isTryAgain) {
        mCardId = response.getCardId();
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void bindVitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(VirtualCardBindSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
        showRetrySendOtp();
    }

    @Override
    public void bindVitualCardVerifyOtpSuccess(VirtualCardVeryOtpEntiy response) {
        if (currentActionType == Constants.VITUALCARD_CHECK_DETAIL) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, response);
            mHashMaps.put(Constants.VIRTUALCARDID, mCardId);
            ActivitySkipUtil.startAnotherActivity(VirtualCardBindSendOtpActivity.this, VirtualCardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            this.finish();
        } else {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, currentActionType);
            mHashMaps.put(Constants.VIRTUALCARDLISTBEAN, response);
            mHashMaps.put(Constants.VIRTUALCARDID, mCardId);
            ActivitySkipUtil.startAnotherActivity(VirtualCardBindSendOtpActivity.this, VirtualCardBindSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            this.finish();
        }
    }

    @Override
    public void bindVitualCardVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(VirtualCardBindSendOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }
}
