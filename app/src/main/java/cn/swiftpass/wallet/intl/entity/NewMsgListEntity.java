package cn.swiftpass.wallet.intl.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class NewMsgListEntity extends BaseEntity {

      ArrayList<NewMsgEntity>promotions=new ArrayList();

    public ArrayList<NewMsgEntity> getPromotionResults() {
        return promotions;
    }

    public void setPromotionResults(ArrayList<NewMsgEntity> promotionResults) {
        this.promotions = promotionResults;
    }
}
