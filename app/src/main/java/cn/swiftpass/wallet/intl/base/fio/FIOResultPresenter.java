package cn.swiftpass.wallet.intl.base.fio;


import android.content.Intent;

import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOResultPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 17:21
 * @change
 * @chang time
 * @class FIO回调处理
 */
public interface FIOResultPresenter<V extends IView> {
    void onFioResult(int requestCode, int resultCode, Intent data);
}
