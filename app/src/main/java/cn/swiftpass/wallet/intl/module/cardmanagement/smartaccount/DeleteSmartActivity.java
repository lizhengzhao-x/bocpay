package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;


import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountDeleteContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.SmartAccountDeletePresenter;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 取消智能账户
 */
public class DeleteSmartActivity extends BaseCompatActivity<SmartAccountDeleteContract.Presenter> implements SmartAccountDeleteContract.View {

    private TextView mId_sub_title;
    private TextView mId_error_message;
    private TextView mId_cancel_condiction;
    private TextView mId_delete_account;
    private TextView mId_clear_ecoupon;
    private BankCardEntity cardEntity;
    private View id_buttom_view;

    private void bindViews() {
        mId_sub_title = (TextView) findViewById(R.id.id_sub_title);
        mId_error_message = (TextView) findViewById(R.id.id_error_message);
        mId_cancel_condiction = (TextView) findViewById(R.id.id_cancel_condiction);
        mId_delete_account = (TextView) findViewById(R.id.id_delete_account);
        mId_clear_ecoupon = (TextView) findViewById(R.id.id_clear_ecoupon);
        id_buttom_view = findViewById(R.id.id_buttom_view);
    }


    @Override
    public void init() {
        bindViews();
        setToolBarTitle(R.string.D1_4a_1);
        cardEntity = (BankCardEntity) getIntent().getExtras().getSerializable(Constants.CARD_ENTITY);
        if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT_THREE)) {
            //智能账户
            mId_error_message.setText(getString(R.string.SMA2101_1_28));
            mId_clear_ecoupon.setText(getString(R.string.SMA2101_1_34));
        } else if (!TextUtils.isEmpty(cardEntity.getSmartAccLevel()) && cardEntity.getSmartAccLevel().equals(SmartAccountConst.CARD_TYPE_SMART_ACCOUNT)) {
            //支付账户
            mId_error_message.setText(getString(R.string.SMA2101_1_29));
            mId_clear_ecoupon.setText(getString(R.string.SMA2101_1_35));
        }

        mId_delete_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    if (mId_delete_account.getText().toString().equals(getString(R.string.D1_4a_7))) {
                        MyActivityManager.removeAllTaskExcludeMainStack();
                    } else {
                        initPayPwdPopWindow();
                    }
                }
            }
        });

    }


    public void initPayPwdPopWindow() {
        if (!CacheManagerInstance.getInstance().checkLoginStatus()) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
            return;
        }
        if (getActivity() == null) {
            return;
        }
        setBackBtnEnable(false);
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                cancelSuspendSmartAccountInfo();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                setBackBtnEnable(true);
            }
        });

    }



    /**
     * 取消我的账户
     */
    private void cancelSuspendSmartAccountInfo() {
        //取消我的账户
        mPresenter.updateSmartAccountCancel();
     }


    @Override
    protected int getLayoutId() {
        return R.layout.act_delete_smart_account;
    }

    @Override
    protected SmartAccountDeleteContract.Presenter createPresenter() {
        return new SmartAccountDeletePresenter();
    }

    @Override
    public void updateSmartAccountCancelFail(String errorCode, String errorMsg) {
        mId_error_message.setText(errorMsg);
        mId_sub_title.setText(getString(R.string.SMA2101_1_37));
        mId_delete_account.setText(getString(R.string.D1_4a_7));
        setBackBtnEnable(true);
        id_buttom_view.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updateSmartAccountCancelSuccess(SmartAccountUpdateEntity response) {
        setBackBtnEnable(true);
        if (null == response) {
            return;
        }
        final boolean isLogout = TextUtils.equals(response.getLogout(), Constants.NO_CARD_EXIST);
        if (isLogout) {
            AndroidUtils.clearMemoryCacheOnly();
        }
        showErrorMsgDialog(mContext, getString(R.string.cancel_smart_suc_tip), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (isLogout) {
                    AndroidUtils.clearMemoryCacheOnly();
                    AndroidUtils.goPreLoginPage();
                    MyActivityManager.removeAllTaskExcludePreLoginStack();
                } else {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_UNBIND_CARD_SUCCESS, ""));
                }
            }
        });
    }
}
