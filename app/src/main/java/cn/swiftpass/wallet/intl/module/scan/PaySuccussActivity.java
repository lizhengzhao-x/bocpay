package cn.swiftpass.wallet.intl.module.scan;

import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.OrderDetailGradeDialogFragment;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.BannerImageEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.CheckEcouponsDetailPop;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.transfer.StaticCodeUtils;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.InitBannerUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEFROM;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGEMONEY;
import static cn.swiftpass.wallet.intl.module.transfer.view.TransferSetLimitActivity.PAGE_COLLECTION;

/**
 * 主扫/被扫成功界面
 */
public class PaySuccussActivity extends BaseCompatActivity {

    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_content)
    TextView tvPaymentMoney;
    @BindView(R.id.tv_paidto)
    TextView tvPaidto;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_pay_via)
    TextView tvPayVia;
    @BindView(R.id.tv_card)
    TextView tvCard;
    @BindView(R.id.id_tv_ok)
    TextView idTvOk;
    @BindView(R.id.id_pay_symbol)
    TextView idPaySymbol;
    @BindView(R.id.id_status_image)
    ImageView idStatusImage;
    @BindView(R.id.id_jifen_send_message_img)
    ImageView idSendImg;
    @BindView(R.id.id_grade_reduce_info_view)
    View mIdGradeReduceInfoView;
    @BindView(R.id.id_tv_use_grade)
    TextView idUseGrade;
    //    @BindView(R.id.ll_line_oringinal)
//    LinearLayout ll_line_oringinal;
    @BindView(R.id.id_rel_discount)
    RelativeLayout idRelDiscount;
    @BindView(R.id.tv_discount)
    TextView tvOriginalMoney;
    @BindView(R.id.id_grade_total)
    TextView idGradeTotal;
    @BindView(R.id.id_grade_deduct)
    TextView idGradeDeduct;
    @BindView(R.id.id_grade_retain)
    TextView idGradeRetain;
    @BindView(R.id.id_dikou_money)
    TextView idDikouMoney;
    @BindView(R.id.id_buttom_text)
    TextView idButtomText;
    @BindView(R.id.id_read_info)
    TextView mId_read_info;
    @BindView(R.id.id_term_rule)
    TextView mId_term_rule;
    @BindView(R.id.id_backhome_single)
    TextView mId_backhome_single;
    @BindView(R.id.id_linear_grade_btn)
    LinearLayout idLinearGradeBtn;
    @BindView(R.id.id_split_line_center)
    View splitLineView;
    @BindView(R.id.id_read_info_img)
    ImageView readInfoImg;
    @BindView(R.id.ll_postscript)
    RelativeLayout ll_postscript;
    @BindView(R.id.tv_postscript)
    TextView tvPostscript;
    @BindView(R.id.tv_staticCode)
    TextView tvStaticCode;

    @BindView(R.id.iv_gr_centerimage)
    ImageView mIv_gr_centerimage;

    /**
     * 收银台
     */
    private OrderDetailGradeDialogFragment orderDetailDialogFragment;
    @BindView(R.id.id_jifen_send_message)
    TextView idJiFenSendMessage;
    /**
     * 支付成功 结果详情
     */
    private PaymentEnquiryResult mPaymentEnquiryResult;
    private boolean mIsLoadGradeInfoSuccess;
    private long currentPageCreateTime;
    @BindView(R.id.id_bannber_detail)
    ImageView idBannerDetail;

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.pop_order_detail);
        currentPageCreateTime = System.currentTimeMillis();
        mPaymentEnquiryResult = (PaymentEnquiryResult) getIntent().getExtras().getSerializable(Constants.URLQRINFO_ENTITY);
        tvPaymentMoney.setText(BigDecimalFormatUtils.forMatWithDigs(mPaymentEnquiryResult.getTrxAmt(), 2));
        //商户名称
        tvName.setText(mPaymentEnquiryResult.getMerchantName());
        //支付的卡号后四位
        tvCard.setText(AndroidUtils.getSubMasCardNumberTitle(mContext, mPaymentEnquiryResult.getPanFour(), mPaymentEnquiryResult.getCardType()));
        //支付的币种符号
        idPaySymbol.setText(AndroidUtils.getTrxCurrency(mPaymentEnquiryResult.getTrxCurrency()));
        if (!TextUtils.isEmpty(mPaymentEnquiryResult.getPostscript())) {
            ll_postscript.setVisibility(View.VISIBLE);
            tvPostscript.setText(mPaymentEnquiryResult.getPostscript());
        } else {
            ll_postscript.setVisibility(View.GONE);
        }

        updateDiscountInfo(mPaymentEnquiryResult.getDiscount(), mPaymentEnquiryResult.getTrxCurrency(), mPaymentEnquiryResult.getOriginalAmt());
        initListener();
        //被扫 只能信用卡才可以积分抵扣
        if (!mPaymentEnquiryResult.getGpFlag()) {
            //不支持积分抵扣 只显示返回首页
            idUseGrade.setVisibility(View.GONE);
            mId_backhome_single.setVisibility(View.VISIBLE);
            tvStaticCode.setVisibility(View.VISIBLE);
            showDetailBanners();
        } else {
            idUseGrade.setVisibility(View.VISIBLE);
            //自动查询积分信息
            queryGradeInfoAutomatic();
        }

    }


    private void showDetailBanners() {
        InitBannerUtils.loadDetailBanners(new InitBannerUtils.LoadBannerListener() {
            @Override
            public void onLoadBannerSuccess() {
                updateBannerView();
            }
        });
    }

    private void updateBannerView() {
        InitBannerEntity initBannerEntity = TempSaveHelper.getInitBannerUrlParams();
        if (initBannerEntity != null) {
            BannerImageEntity pTwoPBean = initBannerEntity.getPTwoM(this);
            // 0 是不显示 1 利利是 2 邀请
            if (pTwoPBean != null && !TextUtils.isEmpty(pTwoPBean.getImagesUrl()) && !TextUtils.equals(pTwoPBean.getType(), "0")) {
                idBannerDetail.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) idBannerDetail.getLayoutParams();
                int width = ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(this, 15) * 2;
                // 375 85
                int height = width * 85 / 375;
                lp.width = width;
                lp.height = height;
                idBannerDetail.setLayoutParams(lp);
                String imageUrl = pTwoPBean.getImagesUrl();
                if (imageUrl.endsWith(".gif")) {
                    GlideApp.with(this).asGif().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).load(imageUrl).into(idBannerDetail);
                } else {
                    GlideApp.with(this).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).into(idBannerDetail);
                }
            }
        }
    }

    private void initListener() {
        String totalStr = getResources().getString(R.string.CCP2105_3_11);
        BasicUtils.initSpannableStrWithTv(totalStr, "##", mId_term_rule, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                String lan = SpUtils.getInstance(PaySuccussActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_SIMP_URL);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_TRAD_URL);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_ENGLISH_URL);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        idTvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                finish();
            }
        });
        mId_backhome_single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                finish();
            }
        });
        idUseGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //被扫成功之后 使用积分抵扣选项
                if (!ButtonUtils.isFastDoubleClick()) {
                    showVerifyQueryDialog();
                }
            }
        });
        readInfoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                DialogUtils.showErrorMsgDialog(PaySuccussActivity.this, getResources().getString(R.string.LYP06_01_2), getResources().getString(R.string.CP1_4a_16));
            }
        });

        idSendImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                String message = "";
                if (mPaymentEnquiryResult == null) return;
                if (!TextUtils.isEmpty(mPaymentEnquiryResult.getCardType()) && mPaymentEnquiryResult.getCardType().equals("1")) {
                    message = getString(R.string.EPTNC_CC_01);
                } else {
                    message = getString(R.string.EPTNC_AC_01);
                }
                showEcouponCountDetail(message);
            }
        });

        tvStaticCode.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                checkBindSmartAccount();
            }
        });
    }

    private void checkBindSmartAccount() {
        StaticCodeUtils.checkBindCard(this, new StaticCodeUtils.BindSmartAccountCallBack() {
            @Override
            public void onBindSmartAccountSuccess() {
                //先通知关闭其他转账流程的页面
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(PAGEFROM, PAGE_COLLECTION);
                mHashMaps.put(PAGEMONEY, mPaymentEnquiryResult.getTrxAmt());
                ActivitySkipUtil.startAnotherActivity(PaySuccussActivity.this, TransferSetLimitActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }

    /**
     * 积分抵消前要加一个弹框提示
     */
    private void showVerifyQueryDialog() {
        AndroidUtils.showTipDialog(PaySuccussActivity.this, null, getString(R.string.CP2_7_1),
                getString(R.string.VC01_03_7), getString(R.string.VC01_03_8), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mIsLoadGradeInfoSuccess) {
                            gradeReduceEvent();
                        } else {
                            queryGradeInfo();
                        }
                    }
                });
    }


    /**
     * 判断这笔订单是否有优惠信息 如果有 需要显示折扣信息
     * id_read_info_img
     *
     * @param discount
     * @param currency
     * @param originalAmt
     */
    private void updateDiscountInfo(String discount, String currency, String originalAmt) {
        if (!TextUtils.isEmpty(discount)) {
            //优惠信息
            try {
                if (Double.valueOf(discount) > 0) {
                    idRelDiscount.setVisibility(View.VISIBLE);
                    tvOriginalMoney.setText(AndroidUtils.getTrxCurrency(currency) + " " + BigDecimalFormatUtils.forMatWithDigs(originalAmt, 2));
                    tvOriginalMoney.getPaint().setAntiAlias(true);
                    tvOriginalMoney.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    //显示 原价与金额之间的分割线
//                    ll_line_oringinal.setVisibility(View.VISIBLE);
                }
            } catch (NumberFormatException e) {
            }
        }
    }

    /**
     * 被扫成功之后 显示积分获取详情
     *
     * @param titleDetail
     */
    private void showEcouponCountDetail(String titleDetail) {
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), "", titleDetail);
        checkEcouponsDetailPop.show();
    }

    /**
     * 被扫成功跳转到该界面 首先自动查询积分兑换信息 如果查询积分失败 显示重新查询
     */
    private void queryGradeInfoAutomatic() {
        String txnId = mPaymentEnquiryResult.getTxnId();
        ApiProtocolImplManager.getInstance().getSweptCodePoints(this, txnId, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                mIdGradeReduceInfoView.setVisibility(View.GONE);
                idLinearGradeBtn.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                setGradeInfoViewData(response);
            }
        });
    }

    private void setGradeInfoViewData(SweptCodePointEntity sweptCodePointEntity) {
        mIsLoadGradeInfoSuccess = true;
        idLinearGradeBtn.setVisibility(View.VISIBLE);
        //显示整个积分的view
        mIdGradeReduceInfoView.setVisibility(View.VISIBLE);
        splitLineView.setVisibility(View.VISIBLE);
        idGradeTotal.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getAvaGpCount()), false));
        idGradeDeduct.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getRedeemGpCount()), false));
        idGradeRetain.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getGpBalance()), false));
        idDikouMoney.setText(AndroidUtils.getTrxCurrency(sweptCodePointEntity.getTranCur()) + " " + sweptCodePointEntity.getRedeemGpTnxAmt());
        TextPaint tp = idDikouMoney.getPaint();
        tp.setFakeBoldText(false);

        idDikouMoney.setTextColor(sweptCodePointEntity.getColorSign());

        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpDateCmt())) {
            idButtomText.setText(sweptCodePointEntity.getGpDateCmt());
        } else {
            idButtomText.setVisibility(View.GONE);
        }
        idUseGrade.setVisibility(View.VISIBLE);

        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpDateCmt())) {
            idButtomText.setText(sweptCodePointEntity.getGpDateCmt());
        } else {
            idButtomText.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(sweptCodePointEntity.getRedeemFlag()) && sweptCodePointEntity.getRedeemFlag().equals("0")) {
            idUseGrade.setBackgroundResource(R.drawable.bg_btn_next_page_disable);
            idUseGrade.setEnabled(false);
        } else {
            idUseGrade.setBackgroundResource(R.drawable.bg_btn_next_page);
            idUseGrade.setTextColor(Color.WHITE);
            idUseGrade.setEnabled(true);
        }


        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpMsgUrl())) {
            if (sweptCodePointEntity.getGpMsgUrl().endsWith(".gif")) {
                GlideApp.with(this).asGif().load(sweptCodePointEntity.getGpMsgUrl())
                        .skipMemoryCache(true)
                        .placeholder(R.mipmap.banner_white)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mIv_gr_centerimage);
            } else {
                GlideApp.with(this).load(sweptCodePointEntity.getGpMsgUrl())
                        .skipMemoryCache(true)
                        .placeholder(R.mipmap.banner_white)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(mIv_gr_centerimage);
            }
        }

    }

    /**
     * 手动查询积分信息 正常弹框
     */
    private void queryGradeInfo() {
        String txnId = mPaymentEnquiryResult.getTxnId();
        ApiProtocolImplManager.getInstance().getSweptCodePoints(this, txnId, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(PaySuccussActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                showCheckWithPointStand(response);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //appcall app 因为涉及增加了aa收款按钮 需要清理link 否则到首页会重新触发
        //1.点击返回商户 清理 2.点击aa收款 到设置金额界面如果返回 还可以点击返回商户 所以稳妥起见 当这个页面关闭的时候清理
        TempSaveHelper.clearLink();
    }

    /**
     * 根据订单结果显示收银台
     *
     * @param sweptCodePointEntity
     */
    private void showCheckWithPointStand(SweptCodePointEntity sweptCodePointEntity) {
        orderDetailDialogFragment = new OrderDetailGradeDialogFragment();
        OrderDetailGradeDialogFragment.OnDialogClickListener onDialogClickListener = new OrderDetailGradeDialogFragment.OnDialogClickListener() {
            @Override
            public void onOkListener(boolean isPoint) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    gradeReduceEvent();
                }
            }

            @Override
            public void onSelCardListener() {
            }

            @Override
            public void onDisMissListener() {

            }
        };
        orderDetailDialogFragment.initParams(sweptCodePointEntity, false, true, onDialogClickListener);
        orderDetailDialogFragment.show(getSupportFragmentManager(), "OrderDetailGradeDialogFragment");
    }

    /**
     * 被扫成功界面进行低分抵扣
     */
    private void gradeReduceEvent() {
        if (CacheManagerInstance.getInstance().isLogin()) {
            AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_QR_PAY_POINT, currentPageCreateTime, PagerConstant.ADDRESS_PAGE_QR_CODE_PAGE, PagerConstant.ADDRESS_PAGE_QR_CODE_PAYMENT_PAGE, System.currentTimeMillis() - currentPageCreateTime);
            sendAnalysisEvent(analysisButtonEntity);
        }
        String txnId = mPaymentEnquiryResult.getTxnId();
        ApiProtocolImplManager.getInstance().qrcodeWithPoints(this, txnId, new NetWorkCallbackListener<SweptCodePointEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(PaySuccussActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(SweptCodePointEntity response) {
                if (orderDetailDialogFragment != null) {
                    orderDetailDialogFragment.dismiss();
                }
                GradeReduceSuccessActivity.startActivityFromBackScan(PaySuccussActivity.this, response);
                finish();
            }
        });


    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_pay_success;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
