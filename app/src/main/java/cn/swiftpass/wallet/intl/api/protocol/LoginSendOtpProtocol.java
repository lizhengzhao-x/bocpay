package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class LoginSendOtpProtocol extends BaseProtocol {
    public static final String TAG = LoginSendOtpProtocol.class.getSimpleName();
    /**
     * 用户id
     */
    String mMobile;
    /**
     * 取值：L：登录
     */
    String mAction;

    public LoginSendOtpProtocol(String mobile, String action, NetWorkCallbackListener dataCallback) {
        this.mMobile = mobile;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/login/sendOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, mAction);
    }
}
