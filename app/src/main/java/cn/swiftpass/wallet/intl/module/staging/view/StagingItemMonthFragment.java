package cn.swiftpass.wallet.intl.module.staging.view;

import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.StagingItemEntity;
import cn.swiftpass.wallet.intl.module.home.constants.MenuKey;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.staging.contract.StagingItemContract;
import cn.swiftpass.wallet.intl.module.staging.presenter.StagingItemPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

import static cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity.ISSUPPORTZOOMIN;

/**
 * 结单分期月原生界面
 */
public class StagingItemMonthFragment extends BaseFragment<StagingItemContract.Presenter> implements StagingItemContract.View {


    @BindView(R.id.id_tv_content)
    TextView tvContentDesc;

    @BindView(R.id.id_tv_contentb)
    TextView tvContentDescB;

    @BindView(R.id.id_tv_contentc)
    TextView tvContentDescC;

    @BindView(R.id.tv_apply_now)
    TextView tvApplyNow;


    @BindView(R.id.id_tv_title)
    TextView tvTitle;

    @BindView(R.id.id_rootview)
    View rootView;
    @BindView(R.id.id_rootScrollView)
    View idRootScrolllView;


    @BindView(R.id.id_top_banner)
    ImageView imgTopImage;


    private StagingItemEntity stagingItemEntity;

    @Override
    public void initTitle() {

        mActivity.setToolBarTitle(getString(R.string.MB2103_1_1));

    }

    @Override
    protected StagingItemContract.Presenter loadPresenter() {
        return new StagingItemPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_stagingitem;
    }


    @Override
    protected void initView(View v) {

        tvApplyNow.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (stagingItemEntity == null) return;
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.MB2103_1_1));
                mHashMaps.put(Constants.DETAIL_URL, stagingItemEntity.getLoadUrl(mContext));
                mHashMaps.put(ISSUPPORTZOOMIN, true);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        rootView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //防止进来异常接口请求失败等，做了整体view点击重试
                if (stagingItemEntity == null) {
                    mPresenter.queryStagesInfo(MenuKey.MONTH_STAGING);
                }
            }
        });

        idRootScrolllView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (stagingItemEntity == null) {
                    mPresenter.queryStagesInfo(MenuKey.MONTH_STAGING);
                }
                return false;
            }
        });

        mPresenter.queryStagesInfo(MenuKey.MONTH_STAGING);
    }

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }


    @Override
    public void queryStagesInfoSuccess(StagingItemEntity response) {
        stagingItemEntity = response;
        tvTitle.setText(response.getTitle(mContext));
        mPresenter.dealWithHtmlShow(response.getContentA(mContext), tvContentDesc, getActivity());
        mPresenter.dealWithHtmlShow(response.getContentB(mContext), tvContentDescB, getActivity());
        mPresenter.dealWithHtmlShow(response.getContentC(mContext), tvContentDescC, getActivity());
        if (TextUtils.isEmpty(response.getContentA(mContext))) {
            tvContentDesc.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(response.getContentB(mContext))) {
            tvContentDescB.setVisibility(View.GONE);
        }
        tvApplyNow.setVisibility(View.VISIBLE);
        Glide.with(mContext).load(response.getTopImgUrl(mContext)).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).placeholder(R.mipmap.img_banner_paymentinstallments_skeleton).into(imgTopImage);
    }

    @Override
    public void queryStagesInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mContext, errorMsg);
    }
}
