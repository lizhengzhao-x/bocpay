package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.DialogInterface;
import android.os.Build;
import android.text.TextUtils;
import android.view.View;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.DeleteSmartActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.setting.location.LocationSettingActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.ImageArrowView;


public class PaymentSettingActivity extends BaseCompatActivity {
    public static final String TAG = PaymentSettingActivity.class.getSimpleName();

    @BindView(R.id.id_forget_pwd)
    ImageArrowView idForgetPwd;
    @BindView(R.id.id_reset_pwd)
    ImageArrowView idResetPwd;
    @BindView(R.id.id_touchpay_pwd)
    ImageArrowView idTouchpayPwd;
    @BindView(R.id.id_set_card)
    ImageArrowView idSetCard;

    @BindView(R.id.id_set_fps)
    ImageArrowView mSetFpsView;
    @BindView(R.id.id_cancel_account)
    ImageArrowView mCancelAccount;
    @BindView(R.id.id_set_location)
    ImageArrowView mLocationSetting;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.setting_payment);
        boolean openBiometricAuth = CacheManagerInstance.getInstance().isOpenBiometricAuth();
        idTouchpayPwd.setVisibility(openBiometricAuth ? View.VISIBLE : View.GONE);
        idForgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                getSmartLevelRequest();
            }
        });

        idResetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_RESET_PASSWORD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), OldPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        idSetCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                if (CacheManagerInstance.getInstance().checkLoginStatus()) {
                    ActivitySkipUtil.startAnotherActivity(getActivity(), SettingCardDefaultAcitivity.class);
                }
            }
        });

        idTouchpayPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                ActivitySkipUtil.startAnotherActivity(PaymentSettingActivity.this, TouchIdActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        mCancelAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                ActivitySkipUtil.startAnotherActivity(PaymentSettingActivity.this, DeleteSmartActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        //checkShowCancelSmartAccountBtn();
        mSetFpsView.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                onClickFpsSetting();
            }
        });

        mLocationSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ButtonUtils.isFastDoubleClick()) return;
                ActivitySkipUtil.startAnotherActivity(PaymentSettingActivity.this, LocationSettingActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });


        //6.0以下 不支持指纹
        if (Build.VERSION.SDK_INT < 23) {
            idTouchpayPwd.setVisibility(View.GONE);
        }
//        queryLbsSwitch();
    }


//    /**
//     * 定位的开关 如果后台开 页面才需要展示入口
//     */
//    private void queryLbsSwitch() {
//        if (TempSaveHelper.getLbsAppParameters() != null) {
//            //表示接口已经访问成功过一次 无需再次访问
//            if (TempSaveHelper.getLbsAppParameters().getMultipleSwitch() != null && !TempSaveHelper.getLbsAppParameters().getMultipleSwitch().isLbsClose()) {
//                mLocationSetting.setVisibility(View.VISIBLE);
//            }
//            return;
//        }
//        new GetAppParameter(new NetWorkCallbackListener<AppParameters>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//
//            }
//
//            @Override
//            public void onSuccess(AppParameters response) {
//                TempSaveHelper.setLbsSwitch(response);
//                if (!response.getMultipleSwitch().isLbsClose()) {
//                    mLocationSetting.setVisibility(View.VISIBLE);
//                }
//
//            }
//        }).execute();
//    }


    /**
     * 忘记密码前 查询我的账户级别
     */
    private void getSmartLevelRequest() {
        ApiProtocolImplManager.getInstance().getSmartLevel(getActivity(), "", "", "", new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(getActivity(), errorMsg);
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.SMART_LEVEL_INFO, response);
                ActivitySkipUtil.startAnotherActivity(getActivity(), STwoSelForgetPasswordTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
    }


    private void onClickFpsSetting() {
        verifyPwdAction();
    }


    private void checkSmartAccount() {
        ApiProtocolImplManager.getInstance().checkSmartAccountNewBind(getActivity(), true, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
                    AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity(), getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            HashMap<String, Object> mHashMaps = new HashMap<>();
                            mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                            ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        }
                    });
                } else {
                    showErrorMsgDialog(getActivity(), errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_payment_view;
    }


    public void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                checkSmartAccount();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

}
