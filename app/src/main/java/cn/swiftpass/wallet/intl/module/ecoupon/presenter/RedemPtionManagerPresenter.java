package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckPasswordProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.RedemPtionManagerContract;

/**
 * Created by ZhangXinchao on 2019/7/26.
 * 电子券管理
 */
public class RedemPtionManagerPresenter extends BasePresenter<RedemPtionManagerContract.View> implements RedemPtionManagerContract.Presenter {
    @Override
    public void verifyPwd(String pwd, String action) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckPasswordProtocol(pwd, "", new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().verifyPwdFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().verifyPwdSuccess();
//                    getView().dismissDialog();
                }
            }
        }).execute();
    }

//    @Override
//    public void getUserTotalGrade() {
//        if (getView()!=null){
//            getView().showDialogNotCancel();
//        }
//
//        new ActionQueryGpEcoupInfoProtocol(null, new NetWorkCallbackListener<CreditCardGradeEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView()!=null){
//                    getView().dismissDialog();
//                    getView().showTotalGradeFailed(errorCode,errorMsg);
//                }
//            }
//
//            @Override
//            public void onSuccess(CreditCardGradeEntity response) {
//                if (getView()!=null){
//                    getView().dismissDialog();
//                    getView().showTotalGradeSuccess(response);
//                }
//            }
//        }).execute();
//    }
}
