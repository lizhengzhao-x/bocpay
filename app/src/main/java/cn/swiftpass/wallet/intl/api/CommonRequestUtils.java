package cn.swiftpass.wallet.intl.api;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.entity.NewErrorCodeEntity;


public class CommonRequestUtils {

    /**
     * 查询订单结果代码
     *
     * @param context
     * @param currentSelPanId
     * @param cpQrCode
     * @param mpTxnId
     */
    public static void getPayMentResult(Activity context, String currentSelPanId, String cpQrCode, String mpTxnId, boolean show, NetWorkCallbackListener callback) {
        ApiProtocolImplManager.getInstance().getQueryTradeStatus(context, currentSelPanId, cpQrCode, mpTxnId, show, callback);
    }


    /**
     * 新增获取卡列表
     *
     * @param context
     * @param userId
     * @param callback
     */

    public static void getCardsList(Context context, String userId, final NetWorkCallbackListener callback) {
        ApiProtocolImplManager.getInstance().getCardList(context, "1", "10", userId, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                callback.onSuccess(response);
            }
        });
    }

    public static void getCardsList(Context context, String userId, boolean showDialog, final NetWorkCallbackListener callback) {
        ApiProtocolImplManager.getInstance().getCardList(context, showDialog, "1", "10", userId, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                callback.onSuccess(response);
            }
        });
    }

    public static void getCardsListWithNoDialog(Context context, String userId, boolean showDialog, final NetWorkCallbackListener callback) {
        ApiProtocolImplManager.getInstance().getCardListWithNoDialog(context, showDialog, "1", "10", userId, new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                callback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                callback.onSuccess(response);
            }
        });
    }

    /**
     * 从server更新errorcode idv/frp
     *
     * @param mActiviy
     */
    public static void getServerErrorCode(Activity mActiviy) {
        if (TempSaveHelper.getErrorCodeList() != null) return;
        ApiProtocolImplManager.getInstance().getSDKErrMsgNew(mActiviy, new NetWorkCallbackListener<NewErrorCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(NewErrorCodeEntity response) {
                if (response != null && !TextUtils.isEmpty(response.getValue())) {
                    String errorStr = new String(Base64.decode(response.getValue().getBytes(), Base64.DEFAULT));
                    try {
                        JSONObject jsonObject = new JSONObject(errorStr);
                        TempSaveHelper.setErrorCodeObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }


}
