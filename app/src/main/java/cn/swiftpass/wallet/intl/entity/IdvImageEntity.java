package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class IdvImageEntity extends BaseEntity {


    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    private String files;
}
