package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.BuildConfig;
import cn.swiftpass.httpcore.entity.BaseEntity;

public class RedeemDetailEntity extends BaseEntity {

    private String itemName;
    private String expireDate;
    private String red;
    private String couponImgLarge;
    private String eVoucherTc;
    private String bU;
    private String currAmount;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }


    private String num;


    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getRed() {
        return red;
    }

    public void setRed(String red) {
        this.red = red;
    }

    public String getCouponImgLarge() {
        return couponImgLarge;
    }

    public void setCouponImgLarge(String couponImgLarge) {
        this.couponImgLarge = couponImgLarge;
    }

    public String geteVoucherTc() {
        return eVoucherTc;
    }

    public void seteVoucherTc(String eVoucherTc) {
        this.eVoucherTc = eVoucherTc;
    }

    public String getbU() {
        return bU;
    }

    public void setbU(String bU) {
        this.bU = bU;
    }

    public String getCurrAmount() {
        return currAmount;
    }

    public void setCurrAmount(String currAmount) {
        this.currAmount = currAmount;
    }


    public MyEVoucherEntity.EvoucherItem parseInfo() {

        MyEVoucherEntity.EvoucherItem evoucherItem = new MyEVoucherEntity.EvoucherItem();
        evoucherItem.setEVoucherInfos(new ArrayList<>());
        evoucherItem.setItemName(itemName);
        evoucherItem.setExpireDate(expireDate);
        evoucherItem.setRed(red);
        evoucherItem.setCouponImgLarge(couponImgLarge);
        evoucherItem.seteVoucherTc(eVoucherTc);
        evoucherItem.setBU(bU);
        evoucherItem.setCurrAmount(currAmount);
        List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> eVoucherInfos = null;
        if (!TextUtils.isEmpty(num)) {
            //ui 使用这个参数 当前币种的电子券数量
            try {
                int size = Integer.parseInt(num);
                eVoucherInfos = new ArrayList<>(size);
                for (int i = 0; i < size; i++) {
                    MyEVoucherEntity.EvoucherItem.EVoucherInfosBean eVoucherInfosBean = new MyEVoucherEntity.EvoucherItem.EVoucherInfosBean();
                    eVoucherInfos.add(eVoucherInfosBean);
                }
                evoucherItem.setEVoucherInfos(eVoucherInfos);
            } catch (NumberFormatException e) {
                if (BuildConfig.DEBUG){
                    e.printStackTrace();
                }
            }
        }
        return evoucherItem;
    }
}
