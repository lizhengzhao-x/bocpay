package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardSetPwdEntity extends BaseEntity {

    public boolean isOpenBiometricAuth() {
        if (TextUtils.isEmpty(openBiometricAuth)) {
            return true;
        }
        return TextUtils.equals(openBiometricAuth, "Y");
    }

    public void setOpenBiometricAuth(String openBiometricAuth) {
        this.openBiometricAuth = openBiometricAuth;
    }

    /**
     * 是否开启生物认证 Y开启 N 不开启
     */
    private String openBiometricAuth;
    /**
     * cardId : 1022
     * account : 86-17051132830
     */

    private String cardId;
    private String account;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
