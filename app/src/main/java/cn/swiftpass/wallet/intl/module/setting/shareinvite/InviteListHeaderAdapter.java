package cn.swiftpass.wallet.intl.module.setting.shareinvite;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.utils.ViewHolderUtils;

public class InviteListHeaderAdapter extends BaseAdapter {
    private List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeans = new ArrayList<>();
    private Context mContext;

    public InviteListHeaderAdapter(List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeansIn, Context mContextIn) {
        this.inviteRecordsBeans.clear();
        this.inviteRecordsBeans.addAll(inviteRecordsBeansIn);
        this.mContext = mContextIn;
    }

    public void updateList(List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeansIn) {
        this.inviteRecordsBeans.clear();
        this.inviteRecordsBeans.addAll(inviteRecordsBeansIn);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return inviteRecordsBeans.size();
    }

    @Override
    public Object getItem(int position) {
        return inviteRecordsBeans.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_invite_list, null);
        }
        TextView id_nickname = (TextView) ViewHolderUtils.get(convertView, R.id.id_nickname);
        TextView id_tel = (TextView) ViewHolderUtils.get(convertView, R.id.id_tel);
        TextView id_date = (TextView) ViewHolderUtils.get(convertView, R.id.id_date);
        id_tel.setVisibility(View.VISIBLE);
        id_nickname.setVisibility(View.VISIBLE);

        InviteRecordListEntity.InviteRecordsBean inviteRecordsBean = inviteRecordsBeans.get(position);
        if (inviteRecordsBean.isShowNumber()) {
            //通讯录匹配
            id_nickname.setText(inviteRecordsBean.getReCustName());
            id_tel.setText(inviteRecordsBean.getReMobileHidden());
            id_tel.setVisibility(View.VISIBLE);
        } else {
            //通讯录不匹配
            id_nickname.setText(inviteRecordsBean.getReMobileHidden());
            id_nickname.setVisibility(View.VISIBLE);
            id_tel.setVisibility(View.GONE);
        }
        id_date.setText(inviteRecordsBean.getCreateTime());
        return convertView;
    }
}
