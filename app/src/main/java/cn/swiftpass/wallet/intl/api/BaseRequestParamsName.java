package cn.swiftpass.wallet.intl.api;

/**
 * Created by Jamy on 2018/7/20.
 * 头部请求参数
 */

public interface BaseRequestParamsName {

    /**
     * 请求时间戳
     */
    String TIMETAMP = "timestamp";

}
