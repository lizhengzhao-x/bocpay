package cn.swiftpass.wallet.intl.module.virtualcard.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardSendOtpEntity extends BaseEntity {


    /**
     * cardId : 1022
     * account : 86-17051132830
     */

    private String cardId;
    private String account;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
}
