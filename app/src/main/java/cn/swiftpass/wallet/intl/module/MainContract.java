package cn.swiftpass.wallet.intl.module;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.dialog.CustomNotificationDialog;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.AppParameters;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;
import cn.swiftpass.wallet.intl.entity.MenuItemEntity;
import cn.swiftpass.wallet.intl.entity.MenuListEntity;
import cn.swiftpass.wallet.intl.entity.NotificationJumpEntity;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.entity.TurnOnRedPackEtntity;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderBaseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.UplanUrlEntity;
import cn.swiftpass.wallet.intl.module.transfer.entity.ApkVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.utils.GlideImageDownLoadListener;

/**
 * 主界面
 */
public class MainContract {

    public interface View extends IView {

        void getNotificationStatusSuccess(CustomNotificationDialog mNotificationDialog,NotificationStatusEntity notificationStatusEntity);

        void getNotificationStatusFailed(CustomNotificationDialog mNotificationDialog,String errorCode, String errorMsg);

        void getMenuListSuccess(MenuListEntity response);

        void getMenuListFail(String errorCode, String errorMsg);

        void getTransferBaseDataSuccess(TransferCrossBorderBaseEntity entity);

        void getTransferBaseDataFail(String errorCode, String errorMsg);

        void getVirtualCardListSuccess(VirtualCardListEntity response);

        void getVirtualCardListError(String errorCode, String errorMsg);


        void apkVerifyWithSHAError(String errorCode, String errorMsg);

        void apkVerifyWithSHASuccess(ApkVerifyEntity otpCountDownInfoEntity);

        void doAutoLoginError(String errorCode, String errorMsg);

        void doAutoLoginSuccess(AutoLoginSucEntity entity);

        void queryBankRecordsByFpsError(String errorCode, String errorMsg);

        void queryBankRecordsByFpsSuccess(FpsBankRecordsEntity entity);

        void appCallAppLinkSuccess(AppCallAppLinkEntity response, String url);

        void appCallAppLinkError(String errorCode, String errorMsg);

        void getAppLoginConfigSuccess(InitBannerEntity response, String action);

        void getAppLoginConfigError(String errorCode, String errorMsg, String action);


//        void getAppParametersError(String errorCode, String errorMsg);
//
//        void getAppParametersSuccess(AppParameters otpCountDownInfoEntity);

        void checkSmartAccountBindSuccess(ContentEntity response, MenuItemEntity cls);

        void checkSmartAccountBindError(String errorCode, String errorMsg, MenuItemEntity cls);


        void getUplanUrlSuccess(UplanUrlEntity response);

        void getUplanUrlError(String errorCode, String errorMsg);

        void refreshCardListError(String errorCode, String errorMsg);

        void refreshCardListSuccess(CardsEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(MySmartAccountEntity response);

        void getStaffRedPacketError(String errorCode, String errorMsg);

        void getStaffRedPacketSuccess(TurnOnRedPackEtntity.ReceivedRedPacketDetailBean response);

        void getNewMessageUrlSuccess(NotificationJumpEntity response);

        void getNewMessageUrlError(String errorCode, String errorMsg);

        void getMyDiscountUrlSuccess(UplanUrlEntity response);

        void getMyDiscountUrlError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View> {


        /**
         * 获取首页基础配置数据
         *
         * @param action
         */
        void getAppLoginConfigParams(String action);

        /**
         * 校验是否有绑定的只能账户
         *
         * @param cls
         */
        void checkSmartAccountBind(MenuItemEntity cls);

        /**
         * 校验是否有绑定的只能账户
         *
         * @param cls
         */
        void checkSmartAccountBind(MenuItemEntity cls, boolean showDialog);

        /**
         * 校验applink是否正确可用
         *
         * @param token
         * @param url
         * @param checkSum
         * @param digest
         */
        void getAppCallAppLink(String token, String url, String checkSum, String digest);

        /**
         * 获取菜单栏
         */
        void getMenuList();

        /**
         * 获取跨境转账基础数据
         */
        void getTransferBaseData();

        /**
         * 拉取虚拟卡列表
         */
        void getVirtualCardList();


        /**
         * 校验app SHA
         *
         * @param shaInfo
         */
        void apkVerifyWithSHA(String shaInfo);

        /**
         * 主动调用自动登录
         */
        void doAutoLogin(boolean iShowLoading);

        /**
         * 通知服务器点击了密码最近修改弹出窗口
         */
        void pwdTipCallServer();

        /**
         * 通知服务器点击了通知设定弹出窗口
         */
        void notificationTipCallServer();

        /**
         * 查询FPS银行记录
         */
        void queryBankRecordsByFps();

//        void getAppParameters();

        /**
         * 获取Uplan页面 url
         */
        void getUplanUrl();

        /**
         * 刷新卡列表 目的 获得新的账户类型， 账户类型的判断由卡列表中卡的类型决定
         */
        void refreshCardList();


        /**
         * 获取背景图地址
         */
        void getBackgroundUrl(GlideImageDownLoadListener refresh);

        void getSmartAccountInfo();

        /**
         * 获取员工利是红包
         */
        void getStaffRedPacket(boolean isShowDialog, int isPush);


        /**
         * 推广通知状态同步
         *
         * @param showDialog
         * @param status
         * @param action
         */
        void getNotificationStatus(CustomNotificationDialog mNotificationDialog,boolean showDialog, String status, String action);

        /**
         * push通知跳转链接获取接口 - 最新消息
         */
        void getNewMessageUrl();

        /**
         * push通知跳转链接获取接口 - 我的优惠券
         */
        void getMyDiscountUrl();
    }

}
