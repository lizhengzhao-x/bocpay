package cn.swiftpass.wallet.intl.module.scan.backscan;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.utils.CreateOneDiCodeUtil;
import cn.swiftpass.boc.commonui.base.utils.QRCodeUtils;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.intl.dialog.SelCardListPopWindow;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.QrCodeErrorEntity;
import cn.swiftpass.wallet.intl.entity.QrIsSweptEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.UplanActivity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.scan.PaySuccussActivity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.SettingCardDefaultAcitivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.WalletConfig;

/**
 * Created by ZhangXinchao on 2017/12/28.
 */

public class BackScanFragment extends BaseFragment implements SelCardListPopWindow.OnPopWindowOkClickListener {
    //中国内地数字条形码
    @BindView(R.id.img_bar_code)
    ImageView mMainLandBarCode;
    @BindView(R.id.tv_bar_number)
    TextView tvBarNumber;
    //港澳台二维码
    @BindView(R.id.img_hongkong_qrcode)
    ImageView mHongKongQrcode;
    @BindView(R.id.tv_bank_name)
    TextView tvBankName;
    @BindView(R.id.tv_uplan_tip)
    TextView tvUplanTip;
    @BindView(R.id.rel_sel_bank)
    RelativeLayout relSelBank;
    @BindView(R.id.id_cardlist_right)
    ImageView idCardlistRight;
    @BindView(R.id.id_root_view)
    LinearLayout idRootView;

    @BindView(R.id.id_global_btn)
    TextView idGlobalBtn;
    @BindView(R.id.id_china_btn)
    TextView idChinaBtn;
    @BindView(R.id.id_view_barcode)
    LinearLayout idViewBarcode;

    //中国内地数字二维码
    @BindView(R.id.img_mainland_qrcode)
    ImageView mMainLandQrcode;
    @BindView(R.id.rootView)
    LinearLayout rootView;
    @BindView(R.id.view_line)
    View lineView;

    private final int MAX_SCREEN_WIDTH = 1080;

    private SelCardListPopWindow mSelCardListPopWindow;
    /**
     * 卡列表
     */
    private List<BankCardEntity> cardEntities;
    /**
     * 当前获取二维码信息
     */
    private QrIsSweptEntity currentQrcodeEntity;
    /**
     * 上一个获取二维码信息
     */
    private QrIsSweptEntity lastQrcodeEntity;
    private String currentSelCardId;
    private String currentUplanId = "";
    private Handler mBackScanHandler;
    private int mUpldateQrcodeTime = WalletConfig.getBackScanUpdateQRCodeTime();

    //fragment创建 fragment显示的时候才需要生成二维码
    private boolean isCreatQrCodeSuccess;
    private static final String TAG = "BackScanFragment";

    @BindView(R.id.cl_line)
    ConstraintLayout lineLayout;
    private boolean isInLeft = true;

    public static final String LOCATION_IN_HK = "LOCATION_IN_HK";

    /**
     * 到达二维码放大界面 轮训不停止
     */
    private boolean isGoBigPage = false;


    private boolean isForceStop = false;


    /**
     * qrCodeErrorEntity 记录错误信息，切换至内地二维码时，也弹框报错
     */
    private QrCodeErrorEntity qrCodeErrorEntity = new QrCodeErrorEntity();

    /**
     * 定时重新获取二维码
     */
    Runnable resetQrcodeRunnable = new Runnable() {
        @Override
        public void run() {
            //倒计时结束 重新获取
            LogUtils.i(TAG, "倒计时结束 重新获取qrcode");
            getQrcode(currentSelCardId, currentUplanId, action);
        }
    };

    /**
     * 获取当前二维码支付状态
     */
    Runnable reQurtCurrentQrStatusRunnable = new Runnable() {
        @Override
        public void run() {
            getCurrentPaymentResultRequest();
        }
    };

    /**
     * 获取上一个二维码支付状态
     */
    Runnable reQuryLastQrStatusRunnable = new Runnable() {
        @Override
        public void run() {

            getLastPaymentResultRequest();
        }
    };
    private String action = "";
    private CustomMsgDialog mDealDialog;

    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.M10_1_7);
        }
    }


    public void setWindowBrightness(float brightness, Activity mActivity) {
        if (mActivity == null) return;
        Window window = mActivity.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = brightness;
        window.setAttributes(lp);
    }

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    public void initCardListPopWindow() {
        for (int i = 0; i < cardEntities.size(); i++) {
            if (TextUtils.equals(currentSelCardId, cardEntities.get(i).getCardId())) {
                cardEntities.get(i).setSel(true);
            } else {
                cardEntities.get(i).setSel(false);
            }
        }
        mSelCardListPopWindow = new SelCardListPopWindow(getActivity(), this, cardEntities);
        Rect rect = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        int winHeight = getActivity().getWindow().getDecorView().getHeight();
        mSelCardListPopWindow.showAtLocation(getActivity().getWindow().getDecorView(), Gravity.BOTTOM, 0, winHeight - rect.bottom);
    }


    public void animToRight() {
        if (isInLeft) {
            isInLeft = false;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineLayout.getPaddingLeft(), lineView.getWidth());
            animator.setDuration(200);
            animator.start();
        }

    }

    public void animToLeft() {
        if (!isInLeft) {
            isInLeft = true;
            ObjectAnimator animator = ObjectAnimator.ofFloat(lineView, "translationX", lineView.getWidth(), 0);
            animator.setDuration(200);
            animator.start();
        }

    }


    @Override
    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_back_scan;
    }

    @Override
    protected void initView(View v) {
        mBackScanHandler = new Handler();
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL, getActivity());
        getCardList();
        addListener(v);
        //是否是定位是香港 先判断是否定位成功 如果成功按照定位成功的切换 如果定位失败 按照上一次保存在本地被扫成功的结果来
        boolean isHongKongQrcode = false;
        Bundle bundle = getArguments();
        if (bundle != null) {
            isHongKongQrcode = bundle.getBoolean(LOCATION_IN_HK);
        } else {
            isHongKongQrcode = CacheManagerInstance.getInstance().currentLocationInHongKong();
        }
        isInLeft = !isHongKongQrcode;
        if (isHongKongQrcode) {
            //TODO 如果是走这里的话，加在onresume  走2次qrcode请求
            //如果默认展示左边 不需要的动画切换
            isInLeft = true;
            selLeftMode(false);
        } else {
            qrCodeErrorEntity.setError(false);
            selRightMode();
        }
    }

    private void addListener(View v) {
        relSelBank.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                initCardListPopWindow();
            }
        });

        mMainLandBarCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBarcodeWainingDialog();
            }
        });

        mMainLandQrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentQrcodeEntity != null && !TextUtils.isEmpty(currentQrcodeEntity.getBarcode())) {
                    isGoBigPage = true;
                    Intent intent = new Intent(getContext(), ScaleQrCodeActivity.class);
                    intent.putExtra(Constants.DATA_QRCODE, currentQrcodeEntity.getBarcode());
                    startActivity(intent);
                }
            }
        });

        mHongKongQrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentQrcodeEntity != null && !TextUtils.isEmpty(currentQrcodeEntity.geteMVQR())) {
                    isGoBigPage = true;
                    Intent intent = new Intent(getContext(), ScaleQrCodeActivity.class);
                    intent.putExtra(Constants.DATA_QRCODE, currentQrcodeEntity.geteMVQR());
                    startActivity(intent);
                }
            }
        });


        idGlobalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selLeftMode(true);
            }
        });


        idChinaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selRightMode();
            }
        });
    }


    private void getCardList() {
        //获取卡列表 先从内存中获取 如果不存在 网络加载
        cardEntities = new ArrayList<>();
        if (CacheManagerInstance.getInstance().getCardEntities() == null) {
            getCardListFromServer();
        } else {
            cardEntities = CacheManagerInstance.getInstance().getCardEntities();
            BankCardEntity cardEntity = CacheManagerInstance.getInstance().getDefaultCardEntitity();
            if (cardEntity != null) {
                tvBankName.setText(AndroidUtils.getSubMasCardNumberTitle(getActivity(), cardEntity.getPanShowNumber(), cardEntity.getCardType()));
                currentSelCardId = CacheManagerInstance.getInstance().getDefaultCardEntitity().getCardId();
            }
        }
    }


    /**
     * 其他区域码
     */
    private void selLeftMode(boolean isNeedAnimation) {
        if (isNeedAnimation) {
            mBackScanHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animToLeft();
                }
            }, 0);
        }
        mHongKongQrcode.setVisibility(View.VISIBLE);
        idViewBarcode.setVisibility(View.INVISIBLE);
        idGlobalBtn.setTextColor(Color.parseColor("#24272B"));
        idChinaBtn.setTextColor(Color.parseColor("#77808A"));
        if (isNeedAnimation) {
            getQrcodeRetry();
        }
    }

    private void getQrcodeRetry() {
        //切换二维码如果之前由于网络异常没有获取二维码成功
        if (mMainLandQrcode != null && mMainLandQrcode.getDrawable() == null) {
            getQrcode(currentSelCardId, currentUplanId, action);
        }
    }

    /**
     * 内地二维码
     */
    private void selRightMode() {
        mBackScanHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animToRight();
            }
        }, 0);

        idViewBarcode.setVisibility(View.VISIBLE);
        mHongKongQrcode.setVisibility(View.GONE);

        idChinaBtn.setTextColor(Color.parseColor("#24272B"));
        idGlobalBtn.setTextColor(Color.parseColor("#77808A"));

        if (qrCodeErrorEntity != null && qrCodeErrorEntity.isError()) {
            updateUIWithErrorCode(qrCodeErrorEntity.getErrCode(), qrCodeErrorEntity.getErrMsg());
        }
    }


    /**
     * 弹框提示
     */
    private void showBarcodeWainingDialog() {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.string_payment_code_for_only));
        builder.setPositiveButton(getString(R.string.dialog_right_btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                isGoBigPage = true;
                if (getIntent() != null) {
                    Intent intent = new Intent(getContext(), ScaleBarCodeActivity.class);
                    intent.putExtra(Constants.DATA_ORDER_NUMBER, currentQrcodeEntity.getBarcode());
                    startActivity(intent);
                }
            }
        });
        mDealDialog = builder.create();
        mDealDialog.show();
    }


    /**
     * 开始轮询
     */
    private void startTimer() {
        mUpldateQrcodeTime = WalletConfig.getBackScanUpdateQRCodeTime() * 1000;
        //多久之后重新生成二维码

        //每次更新二维码之后，将轮训全部清除掉，重新开始 新的二维码/旧的二维码状态查询/重新更新二维码
        mBackScanHandler.removeCallbacksAndMessages(null);
        mBackScanHandler.postDelayed(resetQrcodeRunnable, mUpldateQrcodeTime);
        //多久之后开始查询二维码支付状态
        mBackScanHandler.postDelayed(reQurtCurrentQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
        mBackScanHandler.postDelayed(reQuryLastQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
    }

    private void stopTimer() {
        mBackScanHandler.removeCallbacks(resetQrcodeRunnable);
    }


    /**
     * 轮训查询订单状态
     */
    private void getCurrentPaymentResultRequest() {
        if (getActivity() == null || getActivity().isFinishing()) {
            return;
        }
        if (currentQrcodeEntity == null) return;
        CommonRequestUtils.getPayMentResult(getActivity(), currentSelCardId, null, currentQrcodeEntity.getTxnId(), false, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                LogUtils.i(TAG, "errorMsg=" + errorMsg);
                if (mBackScanHandler == null) return;
                mBackScanHandler.removeCallbacks(reQurtCurrentQrStatusRunnable);
                mBackScanHandler.postDelayed(reQurtCurrentQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
                if (mBackScanHandler == null) return;
                if (response == null) {
                    mBackScanHandler.postDelayed(reQurtCurrentQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
                    return;
                }
                if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_SUCCESS)) {
                    if (isForceStop) return;
                    //将支持成功当前的二维码切换地区保存到本地
                    CacheManagerInstance.getInstance().saveLocationInHongKong(isInLeft);
                    //支付成功
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.URLQRINFO_ENTITY, response);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), PaySuccussActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    isForceStop = true;
                } else if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_CLOSED) ||
                        response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_REJECT) ||
                        response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_CANCEL)) {
                    mBackScanHandler.removeCallbacks(resetQrcodeRunnable);
                    mBackScanHandler.removeCallbacks(reQurtCurrentQrStatusRunnable);
                    //重新生成二维码
                    getQrcode(currentSelCardId, currentUplanId, action);
                } else {
                    mBackScanHandler.removeCallbacks(reQurtCurrentQrStatusRunnable);
                    mBackScanHandler.postDelayed(reQurtCurrentQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
                }
            }
        });
    }


    /**
     * 轮训查询上一个二维码订单订单状态
     */
    private void getLastPaymentResultRequest() {
        if (getActivity() == null || lastQrcodeEntity == null) {
            return;
        }
        CommonRequestUtils.getPayMentResult(getActivity(), currentSelCardId, null, lastQrcodeEntity.getTxnId(), false, new NetWorkCallbackListener<PaymentEnquiryResult>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                LogUtils.i(TAG, "errorMsg=" + errorMsg);
                if (mBackScanHandler == null) return;
                mBackScanHandler.removeCallbacks(reQuryLastQrStatusRunnable);
                mBackScanHandler.postDelayed(reQuryLastQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
            }

            @Override
            public void onSuccess(PaymentEnquiryResult response) {
                if (mBackScanHandler == null) return;
                if (response == null) return;
                if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_SUCCESS)) {
                    if (isForceStop) return;
                    //支付成功
                    //将支持成功当前的二维码切换地区保存到本地
                    CacheManagerInstance.getInstance().saveLocationInHongKong(isInLeft);
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.URLQRINFO_ENTITY, response);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), PaySuccussActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    isForceStop = true;
                } else if (response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_CLOSED) || response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_REJECT) || response.getPaymentStatus().equals(PaymentEnquiryResult.PAYMENT_STATUS_CANCEL)) {
                } else {
                    mBackScanHandler.removeCallbacks(reQuryLastQrStatusRunnable);
                    mBackScanHandler.postDelayed(reQuryLastQrStatusRunnable, WalletConfig.getBackScanQueryQRCodeResultTime() * 1000);
                }
            }
        });
    }


    @Override
    public void onBackButtonClickListener() {

    }

    /**
     * 卡列表弹框 更多 到达设置默认卡界面
     */
    @Override
    public void onMoreButtonClickListener() {
        if (getActivity() != null) {
            ActivitySkipUtil.startAnotherActivity(getActivity(), SettingCardDefaultAcitivity.class);
        }
    }

    @Override
    public void onSelCardClickListener(boolean complete) {

    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.i(TAG, "onResume:" + getUserVisibleHint());
        if (getUserVisibleHint() && !isGoBigPage) {
            getQrcode(currentSelCardId, currentUplanId, action);
        }
        tvUplanTip.setVisibility(UplanActivity.action.equals(action) ? View.VISIBLE : View.GONE);
        isGoBigPage = false;
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        LogUtils.i(TAG, "hidden:" + hidden);
        if (!hidden && !isCreatQrCodeSuccess) {
            getQrcode(currentSelCardId, currentUplanId, action);
        }
    }

    /**
     * 切换卡的时候 重新获取二维码
     *
     * @param positon
     */
    @Override
    public void onCardSelListener(int positon) {
        //更换交易选择卡
        currentSelCardId = cardEntities.get(positon).getCardId();
        BankCardEntity cardEntity = cardEntities.get(positon);
        if (cardEntity != null) {
            tvBankName.setText(AndroidUtils.getSubMasCardNumberTitle(getActivity(), cardEntity.getPanShowNumber(), cardEntity.getCardType()));
            getQrcode(currentSelCardId, currentUplanId, action);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        LogUtils.i(TAG, "onPause:" + getUserVisibleHint());
        if (isGoBigPage) {
        } else {
            //移除轮训
            mBackScanHandler.removeCallbacks(resetQrcodeRunnable);
            mBackScanHandler.removeCallbacks(reQurtCurrentQrStatusRunnable);
            mBackScanHandler.removeCallbacks(reQuryLastQrStatusRunnable);
            mBackScanHandler.removeCallbacksAndMessages(null);
            currentQrcodeEntity = null;
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mDealDialog != null) {
            mDealDialog.dismiss();
        }
        if (mSelCardListPopWindow != null) {
            mSelCardListPopWindow.dismiss();
            mSelCardListPopWindow = null;
        }

        stopTimer();
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE, mActivity);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 根据pandId couponNo 获取qrcode
     */
    private void getQrcode(String pandId, String couponNo, String action) {
        if (getActivity() != null) {
            if (pandId == null) {
                //卡列表还没有拉取成功 等待卡列表拉取 之后才请求
                return;
            }
            LogUtils.i("time_value get qrcode start", System.currentTimeMillis());
            showDialogNotCancel();
            ApiProtocolImplManager.getInstance().getCPQRCode(getActivity(), pandId, couponNo, action, new NetWorkCallbackListener<QrIsSweptEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    dismissDialog();
                    qrCodeErrorEntity.setError(true);
                    qrCodeErrorEntity.setErrCode(errorCode);
                    qrCodeErrorEntity.setErrMsg(errorMsg);
                    updateUIWithErrorCode(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(QrIsSweptEntity entity) {
                    dismissDialog();
                    qrCodeErrorEntity.setError(false);
                    updateUIWithSuccess(entity);
                }
            });
        }
    }

    private void updateUIWithSuccess(QrIsSweptEntity entity) {
        if (getActivity() == null) {
            return;
        }
        //关闭定位服务
        EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_CLOSE_GPS_SERVICE, ""));
        LogUtils.i("time_value get qrcode end", System.currentTimeMillis());
        isCreatQrCodeSuccess = true;
        if (currentQrcodeEntity != null) {
            //赋值上一个二维码信息
            lastQrcodeEntity = currentQrcodeEntity;
        }
        currentQrcodeEntity = entity;
        try {
            Bitmap bitmap = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrcodeEntity.geteMVQR(), AndroidUtils.dip2px(getActivity(), 220)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_appicon_bocpay), getActivity());
            mHongKongQrcode.setImageBitmap(bitmap);
            int screenWidth = AndroidUtils.getScreenWidth(getActivity());
            int width = 0;
            if (screenWidth < MAX_SCREEN_WIDTH) {
                width = AndroidUtils.getScreenWidth(getActivity());
            } else {
                width = (int) (AndroidUtils.getScreenWidth(getActivity()) * 0.8);
            }
            Bitmap tempBitmap = CreateOneDiCodeUtil.createCode(currentQrcodeEntity.getBarcode(), width, AndroidUtils.dip2px(getActivity(), getResources().getDimension(R.dimen.back_scan_bar_height)));
            mMainLandBarCode.setImageBitmap(tempBitmap);
            tvBarNumber.setText(String.format(getString(R.string.backscan_barcode_number), currentQrcodeEntity.getBarcode().substring(0, 4) + "****"));

            int relativeWidth = 185;
            Bitmap bitmapBar = QRCodeUtils.addLogo(QRCodeUtils.createQRCode(currentQrcodeEntity.getBarcode(), AndroidUtils.dip2px(getActivity(), relativeWidth)), BitmapFactory.decodeResource(getResources(), R.mipmap.icon_appicon_bocpay), getActivity());
            mMainLandQrcode.setImageBitmap(bitmapBar);
            mMainLandQrcode.setLayoutParams(new LinearLayout.LayoutParams(AndroidUtils.dip2px(getActivity(), relativeWidth), AndroidUtils.dip2px(getActivity(), relativeWidth)));

            idRootView.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            LogUtils.e(TAG, Log.getStackTraceString(e));
        }
        //倒计时进行更新
        startTimer();
    }

    private void updateUIWithErrorCode(String errorCode, String errorMsg) {
        //获取二维码失败之后清除显示内容
        mMainLandQrcode.setImageDrawable(null);
        mHongKongQrcode.setImageDrawable(null);
        mMainLandBarCode.setImageDrawable(null);
        tvBarNumber.setText("");
        showErrorMsgDialog(getActivity(), errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                // 被取消了 还有信用卡
                if (TextUtils.equals(errorCode, "EWA5110")) {
                    CacheManagerInstance.getInstance().setCardEntities(null);
                    getCardListFromServer();
                } else if (TextUtils.equals(errorCode, "EWA5111")) {
                    // 生成主被扫我的账户不可用错误码 (无信用卡了，要跳登录界面)
                    ActivitySkipUtil.startAnotherActivity(MyActivityManager.getInstance().getCurrentActivity(), LoginActivity.class);
                    MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                }
            }
        });
    }

    private void getCardListFromServer() {
        CommonRequestUtils.getCardsList(getActivity(), HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(CardsEntity response) {
                cardEntities = response.getRows();
                if (cardEntities != null && cardEntities.size() > 0) {
                    CacheManagerInstance.getInstance().setCardEntities(cardEntities);
                    BankCardEntity cardEntity = CacheManagerInstance.getInstance().getDefaultCardEntitity();
                    if (cardEntity != null) {
                        tvBankName.setText(AndroidUtils.getSubMasCardNumberTitle(getActivity(), cardEntity.getPanShowNumber(), cardEntity.getCardType()));
                        currentSelCardId = CacheManagerInstance.getInstance().getDefaultCardEntitity().getCardId();
                        //刚进入的时候 卡列表为空 需要先拉取卡列表 然后再进行生成二维码操作
                        if (isVisible()) {
                            getQrcode(currentSelCardId, currentUplanId, action);
                        }
                    }
                }
            }
        });
    }


    public void setData(String action, String couponInfo) {
        this.currentUplanId = couponInfo;
        this.action = action;
    }
}
