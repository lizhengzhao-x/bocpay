package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import java.util.List;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author create date on  on 2018/7/30 13:50
 */

public class ParserTransferProtocol extends BaseProtocol {
    public static final String TAG = ParserTransferProtocol.class.getSimpleName();
    private String qrCode;
    private String action;
    private List orgList;


    public ParserTransferProtocol(String qrCode, String action, List<String> orgList, NetWorkCallbackListener dataCallback) {
        this.action = action;
        this.orgList = orgList;
        this.qrCode = qrCode;
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferAnalyzeQRCode";
    }

    public ParserTransferProtocol(String qrCode, NetWorkCallbackListener dataCallback) {
        this.qrCode = qrCode;
        this.mDataCallback = dataCallback;
        mUrl = "api/transfer/transferAnalyzeQRCode";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.QRCODE, qrCode);
        mBodyParams.put(RequestParams.TRANSFER_ISQRCODE, "Y");
//        if (!TextUtils.isEmpty(action)) {
//            mBodyParams.put(RequestParams.ACTION, action);
//        }
        if (orgList != null) {
            mBodyParams.put(RequestParams.ORGLIST, orgList);
        }
    }
}
