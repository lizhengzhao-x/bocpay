package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.TransferBillEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class TransferBillListAdapter implements AdapterItem<TransferBillEntity.TransferInfosBean> {

    private ImageView mId_merchant_head;
    private TextView mId_merchant_name;
    private TextView mId_merchant_money;
    private TextView mId_bill_time;
    private TextView mId_pay_symbol;
    private View mId_root_view;

    private int position;
    private Context mContext;

    private CardItemCallback mCallback;

    public TransferBillListAdapter(Context mContextIn, CardItemCallback callback) {
        mCallback = callback;
        mContext = mContextIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_bill_view;
    }

    @Override
    public void bindViews(View root) {
        mId_merchant_head = root.findViewById(R.id.id_merchant_head);
        mId_merchant_name = root.findViewById(R.id.id_merchant_name);
        mId_merchant_money = root.findViewById(R.id.id_merchant_money);
        mId_bill_time = root.findViewById(R.id.id_bill_time);
        mId_pay_symbol = root.findViewById(R.id.id_pay_symbol);
        mId_root_view = root.findViewById(R.id.id_root_view);
    }

    @Override
    public void setViews() {
        mId_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCallback != null) {
                    mCallback.onItemCardClick(position);
                }
            }
        });

    }

    @Override
    public void handleData(TransferBillEntity.TransferInfosBean cardItemEntity, int position) {
        this.position = position;
        String merchantName = "";
        merchantName = cardItemEntity.getPayee();
        mId_merchant_name.setText(merchantName);
        mId_bill_time.setText(AndroidUtils.formatTimeString(cardItemEntity.getCreateTimeStr()));
        mId_pay_symbol.setText(cardItemEntity.getCur());
        String trxAmt = cardItemEntity.getAmount();
        if (TextUtils.isEmpty(trxAmt)) {
            trxAmt = "0";
        }
        mId_merchant_money.setText(trxAmt);
    }
}
