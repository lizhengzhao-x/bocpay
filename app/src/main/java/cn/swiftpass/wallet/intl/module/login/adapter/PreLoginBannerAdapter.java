package cn.swiftpass.wallet.intl.module.login.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.PreLoginBannerEntity;
import cn.swiftpass.wallet.intl.module.home.adapter.HomeBannerAdapter;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;

import static cn.swiftpass.wallet.intl.module.home.utils.BannerUtils.BANNER_IMAGE_PROPORTION;

public class PreLoginBannerAdapter extends RecyclerView.Adapter {
    private ArrayList<PreLoginBannerEntity> data = new ArrayList<>();

    private Context context;
    private HomeBannerAdapter.OnItemClickListener onItemClickListener;

    public PreLoginBannerAdapter(Context context, HomeBannerAdapter.OnItemClickListener onItemClickListenerIn) {
        this.context = context;
        this.onItemClickListener = onItemClickListenerIn;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_pre_login_bottom_bannner, parent, false);
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        layoutParams.height = (int) (ScreenUtils.getDisplayHeight() * 0.234f);
        layoutParams.width = (int) (ScreenUtils.getDisplayHeight() * 0.234f / BANNER_IMAGE_PROPORTION);
        return new PreLoginBannerHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PreLoginBannerHolder) {
            PreLoginBannerHolder bannerHolder = (PreLoginBannerHolder) holder;
            PreLoginBannerEntity preLoginBannerEntity = data.get(position);

            if (TextUtils.isEmpty(preLoginBannerEntity.getImageUrl())) {
                if (preLoginBannerEntity.getLocalDefaultImageUrl() != -1) {
                    //防止后台返回这个url为空
                    GlideApp.with(context)
                            .load(preLoginBannerEntity.getLocalDefaultImageUrl())
                            .placeholder(R.mipmap.img_banner_skeleton)
                            .into(bannerHolder.preLoginBannerIv);
                }
            } else {
                GlideApp.with(context)
                        .load(preLoginBannerEntity.getImageUrl())
                        .placeholder(R.mipmap.img_banner_skeleton)
                        .into(bannerHolder.preLoginBannerIv);
            }

            bannerHolder.preLoginBannerIv.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position);
                    }
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public void setData(ArrayList<PreLoginBannerEntity> data) {
        this.data = data;
        notifyDataSetChanged();
    }
}
