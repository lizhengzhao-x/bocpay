package cn.swiftpass.wallet.intl.base.fpr;


import cn.swiftpass.boc.commonui.base.mvp.IView;

public interface FPRUsedCountPresenter<V extends IView> {
    void checkFPRUsedCount(String action);
}
