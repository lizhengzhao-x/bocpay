package cn.swiftpass.wallet.intl.base.fpr;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.entity.ContentEntity;

public interface FPRUsedCountView<V extends IPresenter> {
    void checkFPRUsedFailed(String action, String errorCode, String errorMsg);

    void checkFPRUsedSuccess(String action, ContentEntity response);
}
