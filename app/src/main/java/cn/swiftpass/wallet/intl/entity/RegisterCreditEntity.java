package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/21 16:31
 */
public class RegisterCreditEntity extends BaseEntity {

    String unqKey;
    String srvrNm;
    String reqTS;
    String CDCRevTS;
    String CDCRpyTS;
    String respCode;
    String respMsg;
    String cardArtId;
    String mobile; //手机
    String NID;
    String NIDTp;
    String NIDRgn;
    String walletId;
    String cardId;

    public void setUnqKey(String unqKey) {
        this.unqKey = unqKey;
    }

    public void setSrvrNm(String srvrNm) {
        this.srvrNm = srvrNm;
    }

    public void setReqTS(String reqTS) {
        this.reqTS = reqTS;
    }

    public void setCDCRevTS(String CDCRevTS) {
        this.CDCRevTS = CDCRevTS;
    }

    public void setCDCRpyTS(String CDCRpyTS) {
        this.CDCRpyTS = CDCRpyTS;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public void setRespMsg(String respMsg) {
        this.respMsg = respMsg;
    }

    public void setCardArtId(String cardArtId) {
        this.cardArtId = cardArtId;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setNID(String NID) {
        this.NID = NID;
    }

    public void setNIDTp(String NIDTp) {
        this.NIDTp = NIDTp;
    }

    public void setNIDRgn(String NIDRgn) {
        this.NIDRgn = NIDRgn;
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUnqKey() {
        return unqKey;
    }

    public String getSrvrNm() {
        return srvrNm;
    }

    public String getReqTS() {
        return reqTS;
    }

    public String getCDCRevTS() {
        return CDCRevTS;
    }

    public String getCDCRpyTS() {
        return CDCRpyTS;
    }

    public String getRespCode() {
        return respCode;
    }

    public String getRespMsg() {
        return respMsg;
    }

    public String getCardArtId() {
        return cardArtId;
    }

    public String getMobile() {
        return mobile;
    }

    public String getNID() {
        return NID;
    }

    public String getNIDTp() {
        return NIDTp;
    }

    public String getNIDRgn() {
        return NIDRgn;
    }

    public String getWalletId() {
        return walletId;
    }

    public String getCardId() {
        return cardId;
    }


}
