package cn.swiftpass.wallet.intl.module.register.adapter;

import cn.swiftpass.wallet.intl.entity.TaxResidentEntity;

public interface OnClickTaxResidenceJurisdictionListener {
    void OnClickTaxResidenceJurisdiction(TaxResidentEntity currentCountryData);
}
