package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class ExternalSysParameterProtocol extends BaseProtocol {
    String externalSysParameter;

    public ExternalSysParameterProtocol(String externalSysParameterIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.externalSysParameter = externalSysParameterIn;
        mUrl = "api/sys/appCallApp";
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.EXTERNALSYSPARAMETER, externalSysParameter);
    }
}
