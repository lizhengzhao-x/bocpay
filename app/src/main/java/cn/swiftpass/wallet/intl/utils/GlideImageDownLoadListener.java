package cn.swiftpass.wallet.intl.utils;

public interface GlideImageDownLoadListener {
    /**
     * 注意返回的线程
     */
    void onSuccessImageDownLoad();
    void onFailImageDownLoad();

}
