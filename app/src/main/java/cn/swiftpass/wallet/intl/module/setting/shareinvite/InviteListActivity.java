package cn.swiftpass.wallet.intl.module.setting.shareinvite;

import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteRecordListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

/**
 * 邀请人记录
 */
public class InviteListActivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    @BindView(R.id.id_nodata_image)
    ImageView idNodataImage;
    private List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeans;
    private List<InviteRecordListEntity.InviteRecordsBean> inviteRecordsBeansLocal;
    @BindView(R.id.id_empty_view)
    View id_empty_view;
    @BindView(R.id.id_empty_activity)
    TextView idEmptyActivity;
    @BindView(R.id.id_hint_text)
    TextView idHintText;
    @BindView(R.id.id_swipeRefreshLayout)
    SwipeRefreshLayout idSwipeRefreshLayout;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                inviteRecordsBeans.clear();
                inviteRecordsBeans.addAll(inviteRecordsBeansLocal);
                idRecyclerview.getAdapter().notifyDataSetChanged();
                dismissDialog();
            }
        };
        setToolBarTitle(R.string.MGM1_1_1);
        idNodataImage.setVisibility(View.INVISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        inviteRecordsBeans = new ArrayList<>();
        inviteRecordsBeansLocal = new ArrayList<>();
        idEmptyActivity.setText(getString(R.string.string_activity_no));
        idRecyclerview.setEmptyView(id_empty_view);
        idSwipeRefreshLayout.setColorSchemeResources(R.color.app_dark_red);
        ((IAdapter<InviteRecordListEntity.InviteRecordsBean>) idRecyclerview.getAdapter()).setData(inviteRecordsBeans);
        idRecyclerview.getAdapter().notifyDataSetChanged();
        idSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTransferInviteRecord();
            }
        });
        getTransferInviteRecord();
    }

    /**
     * 获取广告列表
     */
    private void getTransferInviteRecord() {
        showDialogNotCancel();
        ApiProtocolImplManager.getInstance().getTransferInviteRecord(this, false, new NetWorkCallbackListener<InviteRecordListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                showErrorMsgDialog(mContext, errorMsg);
                if (idSwipeRefreshLayout == null) return;
                idSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onSuccess(InviteRecordListEntity response) {
                dismissDialog();
                if (idSwipeRefreshLayout == null) {
                    return;
                }
                idSwipeRefreshLayout.setRefreshing(false);
                if (response.getInviteRecords() != null) {
                    if (!TextUtils.isEmpty(response.getUpToLimit())) {
                        idHintText.setText(response.getUpToLimit());
                        idHintText.setVisibility(View.VISIBLE);
                    }
                    inviteRecordsBeansLocal.clear();
                    inviteRecordsBeansLocal.addAll(response.getInviteRecords());
                    if (isGranted(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                //如果有读取本地通讯录的权限
                                //本地通讯录跟server返回对比 如果本地通讯录中有这个号码 就显示明文 否则显示密文
                                ArrayList<ContactEntity> contactEntities = null;
                                if (ContactUtils.getInstance().getContacts() != null) {
                                    contactEntities = ContactUtils.getInstance().getContacts();
                                } else {
                                    contactEntities = ContactUtils.getInstance().getAllContacts(getApplicationContext(), false);
                                }

                                //ArrayList<ContactEntity> contactEntities = ContactUtils.getAllContacts(mContext);
                                if (contactEntities != null && contactEntities.size() > 0) {
                                    for (int i = 0; i < inviteRecordsBeansLocal.size(); i++) {
                                        InviteRecordListEntity.InviteRecordsBean inviteRecordsBean = inviteRecordsBeansLocal.get(i);
                                        for (int j = 0; j < contactEntities.size(); j++) {
                                            ContactEntity contactEntity = contactEntities.get(j);
                                            if (AndroidUtils.isSameNumber(inviteRecordsBean.getReMobile(), contactEntity.getNumber())) {
                                                inviteRecordsBean.setShowNumber(true);
                                                inviteRecordsBean.setReCustName(contactEntity.getUserName());
                                            }
                                        }
                                    }
                                }
                                mHandler.sendEmptyMessage(-1);
                            }
                        }).start();
                    } else {
                        mHandler.sendEmptyMessage(-1);
//                        idRecyclerview.getAdapter().notifyDataSetChanged();
////                        dismissDialog();
                    }
                } else {
                    idRecyclerview.getAdapter().notifyDataSetChanged();
                }

            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_invitelist;
    }

    private CommonRcvAdapter<InviteRecordListEntity.InviteRecordsBean> getAdapter(final List<InviteRecordListEntity.InviteRecordsBean> data) {
        return new CommonRcvAdapter<InviteRecordListEntity.InviteRecordsBean>(data) {

            @Override
            public Object getItemType(InviteRecordListEntity.InviteRecordsBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new cn.swiftpass.wallet.intl.module.setting.shareinvite.InviteListAdapter(mContext);
            }
        };
    }
}
