package cn.swiftpass.wallet.intl.module.crossbordertransfer.view;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.crossbordertransfer.entity.TransferCrossBorderUsedEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 21:55
 * @change
 * @chang time
 * @class describe
 */
public class PurposeListAdapter extends RecyclerView.Adapter {
    private final Context context;
    private final ArrayList<TransferCrossBorderUsedEntity> list;
    private final OnItemClickListener listener;
    private String select;


    public PurposeListAdapter(Context context, ArrayList<TransferCrossBorderUsedEntity> list, OnItemClickListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public void setSelect(String select) {
        this.select = select;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int posotion) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_purpose, parent, false);
        return new PurposeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        PurposeViewHolder purposeViewHolder = (PurposeViewHolder) viewHolder;
        purposeViewHolder.tvPurpose.setText(list.get(i).getValue());
        if (!TextUtils.isEmpty(select) && select.equals(list.get(i).getValue())) {
            purposeViewHolder.ivSelect.setVisibility(View.VISIBLE);
        } else {
            purposeViewHolder.ivSelect.setVisibility(View.INVISIBLE);
        }
        purposeViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onItemClick(i, list.get(i).getValue(), list.get(i).getCode());
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, String select, String code);
    }

    public class PurposeViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_purpose)
        TextView tvPurpose;
        @BindView(R.id.iv_select)
        ImageView ivSelect;

        public PurposeViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
