package cn.swiftpass.wallet.intl.module.ecoupon.view.adapter;


import android.content.Context;
import android.view.View;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.FilterDateEntity;

/**
 * 电子券 列表查询 展示
 */
public class EcouponCntAdapter implements AdapterItem<FilterDateEntity> {
    private Context context;

    public EcouponCntAdapter(Context context) {
        this.context = context;
    }

    private TextView id_left_name;
    private TextView id_right_cnt;
    private int mPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_ecoupon_ctn_view;
    }

    @Override
    public void bindViews(View root) {
        id_left_name = (TextView) root.findViewById(R.id.id_left_name);
        id_right_cnt = (TextView) root.findViewById(R.id.id_right_cnt);
    }


    @Override
    public void setViews() {


    }

    @Override
    public void handleData(FilterDateEntity homeMessageEntity, int position) {
        mPosition = position;
        id_left_name.setText(homeMessageEntity.getDateStr() + "");
        id_right_cnt.setText(homeMessageEntity.getCountStr() + "");
    }


}
