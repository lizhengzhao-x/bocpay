package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardFailContract;
import cn.swiftpass.wallet.intl.module.register.contract.OtpAction;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;
import cn.swiftpass.wallet.intl.module.register.presenter.BindNewCardFailPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * 一键绑定信用卡绑卡失败
 */
public class BindNewCardFailActivity extends BaseCompatActivity<BindNewCardFailContract.Presenter> implements BindNewCardFailContract.View {

    @BindView(R.id.tv_bind_result_error_msg)
    TextView tvBindResultErrorMsg;
    @BindView(R.id.tv_try_again_bind)
    TextView tvTryAgainBind;
    @BindView(R.id.tv_back_home)
    TextView tvBackHome;

    private BindNewCardResultEntity bindCardResult;
    private int flowType;

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.KBCC2105_6_1);
        if (getIntent() != null) {
            flowType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
            Serializable result = getIntent().getSerializableExtra(BindNewCardOtpActivity.DATA_BIND_NEW_CARD_RESULT);
            if (result != null) {
                bindCardResult = (BindNewCardResultEntity) result;
            }
        }

        tvBindResultErrorMsg.setText(bindCardResult.getErrorMsg());
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_bind_new_card_fail;
    }

    @Override
    protected BindNewCardFailContract.Presenter createPresenter() {
        return new BindNewCardFailPresenter();
    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public void bindNewCardListError(String errorCode, String errorMsg) {
        if (ErrorCode.SA_BIND_NUM_OVER_FIVE.code.equals(errorCode) ||
                ErrorCode.SA_BIND_NUM_OVER_FLOW.code.equals(errorCode)) {
            showErrorMsgDialog(this, errorMsg);
            tvTryAgainBind.setVisibility(View.INVISIBLE);
        } else {
            if (!TextUtils.isEmpty(errorMsg)) {
                tvBindResultErrorMsg.setText(errorMsg);
            }
        }
    }

    @Override
    public void bindNewCardListSuccess(BindNewCardResultEntity response) {
        if (response != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            if (TextUtils.isEmpty(response.getErrorMsg())) {
                mHashMaps.put(BindNewCardOtpActivity.DATA_BIND_NEW_CARD_RESULT, response);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, flowType);
                ActivitySkipUtil.startAnotherActivity(BindNewCardFailActivity.this, BindNewCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                this.finish();
            }

        }
    }

    @OnClick({R.id.tv_try_again_bind, R.id.tv_back_home})
    public void onClick(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId())) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_try_again_bind:
                if (!NetworkUtil.isNetworkAvailable()) {
                    showErrorMsgDialog(BindNewCardFailActivity.this, getString(R.string.string_network_available));
                } else {
                    mPresenter.bindNewCardList(new ArrayList<>(), OtpAction.OTP_BIND_NEW_CARD);
                }
                break;
            case R.id.tv_back_home:
                if (bindCardResult != null
                        && BindNewCardResultEntity.TYPE_SHOW_INVITE.equals(bindCardResult.getShowInvite())
                        && flowType == Constants.PAGE_FLOW_REGISTER
                ) {
                    Intent intent = new Intent(this, RegisterInviteActivity.class);
                    intent.putExtra(Constants.INVITE_TYPE, RegisterInviteActivity.INVITE_TYPE_REGISTER);
                    intent.putExtra(Constants.BIND_CARD_TYPE, true);
                    startActivity(intent);
                } else {
                    EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_SHOW_HOMEPAGE, ""));
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class);
                }
                break;
            default:
                break;
        }
    }
}
