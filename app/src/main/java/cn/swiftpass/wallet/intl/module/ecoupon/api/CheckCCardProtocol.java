package cn.swiftpass.wallet.intl.module.ecoupon.api;

import java.util.List;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;

/**
 * 验证电子券库存
 */
public class CheckCCardProtocol extends BaseProtocol {

    String giftTp;
    List<RedeemResponseEntity.ResultRedmBean> redmBeans;

    public CheckCCardProtocol(String giftTp, List<RedeemResponseEntity.ResultRedmBean> redmBeansIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/redeemGiftCheckStock";
        this.giftTp = giftTp;
        this.redmBeans = redmBeansIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.GIFTTP, giftTp);
        mBodyParams.put(RequestParams.REDEEMITEMS, redmBeans);
    }
}
