package cn.swiftpass.wallet.intl.entity.event;


public class TransferEventEntity extends BaseEventEntity{
    public static final int EVENT_TRANSFER_FAILED = 200;
    public static final int EVENT_UPDATE_LOCAL_CACHE_CONTACT = 201;

    public static final int EVENT_UPDATE_ADDRESS_COLLECTION_STATUS = 202;

    public static final int EVENT_TRANSFER_SUCCESS = 203;

    public static final int UPDATE_TRANSFER_LIST = 300;
    //打开红包的 event
    public static final int EVENT_RED_PACKET_OPEN = 0x400;

    public TransferEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public TransferEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
