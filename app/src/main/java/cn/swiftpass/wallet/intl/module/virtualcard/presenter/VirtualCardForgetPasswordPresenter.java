package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardForgetPasswordSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardForgetPasswordVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.ForgetPwdVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardForgetVerifyOtpEntity;

/**
 * Created by ZhangXinchao on 2019/11/22.
 */
public class VirtualCardForgetPasswordPresenter extends BasePresenter<ForgetPwdVitualCardSendOtpContract.View> implements ForgetPwdVitualCardSendOtpContract.Presenter {
    @Override
    public void vitualCardSenOtp(String action, String checkType, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardForgetPasswordSendOtpProtocol(action, checkType, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardSenOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardSenOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();
    }

    @Override
    public void vVitualCardVerifyOtp(String action, String passcode, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardForgetPasswordVerifyOtpProtocol(action, passcode, verifyCode, new NetWorkCallbackListener<VirtualCardForgetVerifyOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardVerifyOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VirtualCardForgetVerifyOtpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().vitualCardVerifyOtpSuccess(response);
                }
            }
        }).execute();
    }
}
