package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

public class CardGroupViewHolder extends CardBaseViewHolder {
    public int scrollViewTop;
    public ImageView cardImageIv;
    public ImageView cardShadowIv;
    public TextView cardNumberTv;
    public TextView cardNameTv;
    public TextView cardApplyVirtualCardTv;
    public TextView cardBindSmartAccountTv;
    public LinearLayout cardDataLayout;
    public LinearLayout cardApplyCardBtnLayout;
    public LinearLayout cardBindSmartAccountBtnLayout;
    public LinearLayout registerFpsLayout;
    private int firstVisibleItemPosition;

    public CardGroupViewHolder(View itemView) {
        super(itemView);

        cardDataLayout = itemView.findViewById(R.id.ll_card_data);
        cardApplyCardBtnLayout = itemView.findViewById(R.id.ll_card_btn);
        cardBindSmartAccountBtnLayout = itemView.findViewById(R.id.ll_card_btn_bind_smart_account);
        cardBindSmartAccountTv = itemView.findViewById(R.id.tv_card_btn_bind_smart_account);
        registerFpsLayout = itemView.findViewById(R.id.ll_register_fps);
        cardApplyVirtualCardTv = itemView.findViewById(R.id.tv_apply_virtual_card);
        cardNumberTv = itemView.findViewById(R.id.tv_card_no);
        cardNameTv = itemView.findViewById(R.id.tv_card_name_tip);
        cardShadowIv = itemView.findViewById(R.id.iv_card_shadow);
        cardImageIv = itemView.findViewById(R.id.iv_card_img);
        //屏蔽这个容器的点击事件，关闭card面
        itemView.findViewById(R.id.ll_dont_touch_click).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    public int getScrollViewTop() {
        return scrollViewTop;
    }

    public void setScrollViewTop(int scrollViewTop) {
        this.scrollViewTop = scrollViewTop;
    }

    public void setFirstPosition(int firstVisibleItemPosition) {
        this.firstVisibleItemPosition = firstVisibleItemPosition;
    }

    public int getFirstVisibleItemPosition() {
        return firstVisibleItemPosition;
    }
}
