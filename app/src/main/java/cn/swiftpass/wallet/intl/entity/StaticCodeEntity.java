package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class StaticCodeEntity extends BaseEntity {

    private String phone;
    private String email;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private List<MobnQRBean> emalQR;
    private List<MobnQRBean> mobnQR;


    public List<MobnQRBean> getEmalQR() {
        return emalQR;
    }

    public void setEmalQR(List<MobnQRBean> emalQR) {
        this.emalQR = emalQR;
    }

    public List<MobnQRBean> getMobnQR() {
        return mobnQR;
    }

    public void setMobnQR(List<MobnQRBean> mobnQR) {
        this.mobnQR = mobnQR;
    }

    public static class MobnQRBean extends BaseEntity {


        /**
         * acNo : 2068
         * qrCode : 00020101021126400012hk.com.hkicl01030120313+852-6388206853033446304CBBC
         */
        //acTp ------账号类型
        //acNo ------账号
        //qrCode-----二维码信息

        private String acTp;
        private String acNo;
        private String qrCode;

        public String getProxyId() {
            return proxyId;
        }

        public void setProxyId(String proxyId) {
            this.proxyId = proxyId;
        }

        public String getOutBalLmt() {
            return outBalLmt;
        }

        public void setOutBalLmt(String outBalLmt) {
            this.outBalLmt = outBalLmt;
        }

        private String proxyId;
        private String outBalLmt;
        private boolean isSel;

        public String getAcTp() {
            return acTp;
        }

        public void setAcTp(String acTp) {
            this.acTp = acTp;
        }

        public boolean isSel() {
            return isSel;
        }

        public void setSel(boolean sel) {
            isSel = sel;
        }

        public String getAcNo() {
            return acNo;
        }

        public void setAcNo(String acNo) {
            this.acNo = acNo;
        }

        public String getQrCode() {
            return qrCode;
        }

        public void setQrCode(String qrCode) {
            this.qrCode = qrCode;
        }
    }
}
