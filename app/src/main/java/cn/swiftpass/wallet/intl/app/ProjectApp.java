package cn.swiftpass.wallet.intl.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;

import com.bochklaunchflow.utils.BOCLFSharedPreferencesUtils;
import com.bocpay.analysis.AnalysisManager;
import com.egoo.chat.ChatSDKManager;
import com.egoo.sdk.GlobalManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.tradelink.boc.authapp.utils.PreferencesConstants;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.CurrencyCodeEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisImplManager;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.CertsDownloadManager;
import cn.swiftpass.wallet.intl.utils.CrashHandler;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.GlideImageDownloadManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import me.jessyan.autosize.AutoSizeConfig;


public class ProjectApp extends Application {
    private static List<Activity> taskStack;
    private static List<Activity> mTemptaskStack;
    private static final String TAG = "ProjectApp";
    public static Context mContext;


    public static int getFrontActivityNumber() {
        return frontActivityNumber;
    }

    /**
     * 前台的activity个数 主要是为了app后台的时候上报数据
     */
    private static int frontActivityNumber = 0;


    public static List<Activity> getTaskStack() {
        return taskStack;
    }

    public static List<Activity> getTempTaskStack() {
        return mTemptaskStack;
    }


    //================================权限相关配置==================================//
    private String getProcessName(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningApps = am.getRunningAppProcesses();
        if (runningApps == null) {
            return null;
        }
        for (ActivityManager.RunningAppProcessInfo proInfo : runningApps) {
            if (proInfo.pid == android.os.Process.myPid()) {
                if (proInfo.processName != null) {
                    return proInfo.processName;
                }
            }
        }
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        String processName = getProcessName(this);
        LogUtils.i(TAG, "processName:" + processName);
        if (processName != null) {
            if (processName.equals(BuildConfig.APPLICATION_ID)) {
                taskStack = new ArrayList<Activity>();
                mTemptaskStack = new ArrayList<Activity>();
                mContext = getApplicationContext();
                TempSaveHelper.init();
                SpUtils.init(this);
                GlideImageDownloadManager.init(this);
                registerActivityCallBack();
                initCurrencyCode();
                initFio();
                //app异常捕获 只在uat生效 目前只是toast显示 方便定位异常
                if (!Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
                    // 初始化CrashHandler
                    CrashHandler.getInstance().init(this);
                }
                //通用功能sdk 初始化
                BOCLFSharedPreferencesUtils.init(this, "bochklaunch");

                //onLineChat SDK 初始化 不可删除
                if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                    ChatSDKManager.initChatSDK(getApplicationContext());
                    GlobalManager.getInstance().initApp(this);
                }

                //Google 推送
                FirebaseApp.initializeApp(this);
                //测试开关相关的存储 目前只会在uat生效，通过登录界面长按logo
                if (!Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
                    String testUrl = SpUtils.getInstance().getTestUrl();
                    boolean isE2ee = SpUtils.getInstance().getTestE2ee();
                    boolean skipIdv = SpUtils.getInstance().getTestIdv();
                    AppTestManager.getInstance().setBaseUrl(testUrl);
                    AppTestManager.getInstance().setE2ee(isE2ee);
                    AppTestManager.getInstance().setSkipIdvFrp(skipIdv);
                }
                //HttpCoreKeyManager 初始化 必须存在
                HttpCoreKeyManager.getInstance().initGlobalContext(this, BuildConfig.isLogDebug, AppTestManager.getInstance().getBaseUrl(), AppTestManager.getInstance().isE2ee(), BuildConfig.HTTPS_CER, mContext.getPackageName(), BuildConfig.VERSION_NAME);
                HttpCoreKeyManager.getInstance().setmGcmDeviceToken(FirebaseInstanceId.getInstance().getToken());
                LogUtils.i(TAG, "onMessageReceived: " + FirebaseInstanceId.getInstance().getToken());
                //解决 整体解决屏幕缩放问题 华为mate10 400
                AutoSizeConfig.getInstance().setDesignWidthInDp(400).setDesignHeightInDp(853).setExcludeFontScale(true);
                initPhotoError();
                //初始化证书下载功能 提前下载证书，方便用到idv/网页时候使用
                if (BuildConfig.DOWNLOAD_CER_SERVER) {
                    CertsDownloadManager.getInstance().initCertificate();
                }
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        //glide 缓存 缓存在sd卡上 涉及到卡司信用卡其他的图片路径url不改 有可能造成图片更新不了 所以建议在app启动的时候 清除一下缓存
                        GlideApp.get(getApplicationContext()).clearDiskCache();
                    }
                }).start();
            } else {
                mContext = getApplicationContext();
                //多进程 埋点服务需要 涉及到context问题
                HttpCoreKeyManager.getInstance().initGlobalContext(this, BuildConfig.isLogDebug, AppTestManager.getInstance().getBaseUrl(), AppTestManager.getInstance().isE2ee(), BuildConfig.HTTPS_CER, mContext.getPackageName(), BuildConfig.VERSION_NAME);
            }
            //埋点统计服务
            startAnalysisService();
        }
    }

    private void initFio() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PreferencesConstants.PREF_RELYING_PARTY_URL, com.fio.lib.BuildConfig.FIO_URL);
        editor.putString(PreferencesConstants.PREF_RELYING_PARTY_APPID, BuildConfig.APPLICATION_ID);
        editor.commit();
    }


    private void startAnalysisService() {
        //埋点功能的service
        AnalysisManager.getInstance().init(mContext);
        //监听埋点发送事件
        AnalysisImplManager.getInstance().sendAnalysisEvent();
    }

    private void initPhotoError() {
        // android 7.0系统解决拍照的问题
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        builder.detectFileUriExposure();
    }


    /**
     * 每次app启动 读取本地货币符号
     */
    private void initCurrencyCode() {
        String countryStr = AndroidUtils.getFromAssets(getApplicationContext(), "country.json");
        CurrencyCodeEntity currencyCodeEntity = new Gson().fromJson(countryStr, CurrencyCodeEntity.class);
        TempSaveHelper.setCurrencyList(currencyCodeEntity.getData());
    }

    /**
     * 全局activity 生命周期的监听
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void registerActivityCallBack() {
        ActivityLifecycleCallbacks mActivityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                LogUtils.i(TAG, "onActivityCreated->" + activity);
                if (activity instanceof BaseAbstractActivity) {
                    BaseAbstractActivity abstractActivity = (BaseAbstractActivity) activity;
                    MyActivityManager.getInstance().setCurrentActivity(abstractActivity);
                }
            }

            @Override
            public void onActivityStarted(Activity activity) {
                frontActivityNumber++;
                LogUtils.i(TAG, "onActivityStarted->" + activity);
            }

            @Override
            public void onActivityResumed(Activity activity) {
                if (activity instanceof BaseAbstractActivity) {
                    BaseAbstractActivity abstractActivity = (BaseAbstractActivity) activity;
                    MyActivityManager.getInstance().setCurrentActivity(abstractActivity);
                }
                LogUtils.i(TAG, "onActivityResumed->" + activity);
            }

            @Override
            public void onActivityPaused(Activity activity) {
                LogUtils.i(TAG, "onActivityPaused->" + activity);
            }

            @Override
            public void onActivityStopped(Activity activity) {
                LogUtils.i(TAG, "onActivityStopped->" + activity);
                frontActivityNumber--;
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                LogUtils.i(TAG, "onActivityDestroyed->" + activity);
//                if (ProjectApp.getTaskStack().size() == 0) {
//                    LogUtils.i(TAG, "STOP SERVICE");
//                    //因为现在未登录态 登录态 rootActivity 为 PreLoginActivity/MainHomeActivity 要控制app退出的时候 关闭埋点的服务
//                    AnalysisManager.getInstance().stopAnalysisService();
//                }
            }
        };
        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks);
    }


    public static boolean addTastStack(Activity act) {
        if (taskStack != null) {
            return taskStack.add(act);
        }
        return false;
    }

    public static boolean removeTaskStack(Activity act) {
        if (taskStack != null && taskStack.size() > 0) {
            if (taskStack.contains(act)) {
                return taskStack.remove(act);
            }
        }
        return false;
    }


    /**
     * @return 全局的上下文
     */
    public static Context getContext() {
        return mContext;
    }


    @Override
    protected void attachBaseContext(android.content.Context base) {
        super.attachBaseContext(base);
        androidx.multidex.MultiDex.install(this);
    }

}
