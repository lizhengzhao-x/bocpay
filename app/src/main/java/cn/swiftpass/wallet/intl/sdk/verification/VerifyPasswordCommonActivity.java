package cn.swiftpass.wallet.intl.sdk.verification;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.fio.FIODispatcher;
import cn.swiftpass.wallet.intl.base.fio.FIOResultView;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentPasswordDialogWithFioFragment;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.FIOActivity;
import cn.swiftpass.wallet.intl.sdk.fingercore.MyFingerPrintManager;

/**
 * Created by ZhangXinchao on 2019/11/28.
 * 登录态的忘记密码 非登录态暂时用SelectPopupWindow 后期要优化
 * 验证支付密码 指纹模块/密码模块通用处理
 * 暂时用 static onPwdVerifyCallBack callback
 * setResult 有可能在fragment拿不到回调出问题
 */
public class VerifyPasswordCommonActivity extends BaseCompatActivity implements FIOResultView {


    private static OnPwdVerifyCallBack onPwdVerifyCallBack;
    private static OnPwdVerifyWithMajorScanCallBack onPwdVerifyWithMajorScanCallBack;
    private static boolean isSupportFingerPrint;

    private PaymentPasswordDialogWithFioFragment paymentPasswordDialogFragment;
    private static String mCurrentAction;
    private static boolean isDissMissForForgetPwd = false;


    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    private Handler mPwdHandler;

    public static void startActivityForResult(Activity mContext, OnPwdVerifyCallBack onPwdVerifyCallBackIn) {
        if (mContext == null) return;
        mCurrentAction="";
        isDissMissForForgetPwd = false;
        onPwdVerifyWithMajorScanCallBack=null;
        isSupportFingerPrint = true;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }

    public static void startActivityForResult(Activity mContext, OnPwdVerifyCallBack onPwdVerifyCallBackIn, OnPwdVerifyWithMajorScanCallBack onPwdVerifyWithMajorScanCallBackIn) {
        if (mContext == null) return;
        onPwdVerifyWithMajorScanCallBack = onPwdVerifyWithMajorScanCallBackIn;
        mCurrentAction="";
        isDissMissForForgetPwd = false;
        isSupportFingerPrint = true;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }

    /**
     * @param mContext
     * @param action                不同的action 区分不同的支付流程
     * @param onPwdVerifyCallBackIn
     */
    public static void startActivityForActionResult(Activity mContext, String action, OnPwdVerifyCallBack onPwdVerifyCallBackIn) {
        if (mContext == null) return;
        mCurrentAction = action;
        isDissMissForForgetPwd = false;
        onPwdVerifyWithMajorScanCallBack=null;
        isSupportFingerPrint = true;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }


    public static void startActivityForActionResult(Activity mContext, String action, boolean isSupportFingerPrintIn, OnPwdVerifyCallBack onPwdVerifyCallBackIn) {
        if (mContext == null) return;
        mCurrentAction = action;
        isDissMissForForgetPwd = false;
        onPwdVerifyWithMajorScanCallBack=null;
        isSupportFingerPrint = isSupportFingerPrintIn;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }


    public static void startActivityForResult(Activity mContext, boolean isSupportFingerPrintIn, OnPwdVerifyCallBack onPwdVerifyCallBackIn) {
        if (mContext == null) return;
        mCurrentAction="";
        isDissMissForForgetPwd = false;
        onPwdVerifyWithMajorScanCallBack=null;
        isSupportFingerPrint = isSupportFingerPrintIn;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }


    public static void startActivityForResult(Activity mContext, boolean isSupportFingerPrintIn, boolean isDissMissForForgetPwdIn, OnPwdVerifyCallBack onPwdVerifyCallBackIn) {
        if (mContext == null) return;
        mCurrentAction="";
        isDissMissForForgetPwd = isDissMissForForgetPwdIn;
        onPwdVerifyWithMajorScanCallBack=null;
        isSupportFingerPrint = isSupportFingerPrintIn;
        onPwdVerifyCallBack = onPwdVerifyCallBackIn;
        startActivityForResultInThis(mContext);
    }

    public static void startActivityForResultInThis(Activity mContext) {
        if (mContext == null) return;

        Intent intent = new Intent(mContext, VerifyPasswordCommonActivity.class);
        mContext.startActivity(intent);
        //去掉切换动画关键
        mContext.overridePendingTransition(0, 0);
    }





    @Override
    public void init() {
        EventBus.getDefault().register(this);
        verifyPwdAction();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_fio_common;
    }

    private void verifyPwdAction() {
        if (MyFingerPrintManager.getInstance().isRegisterFIO(getActivity()) && !MyFingerPrintManager.getInstance().isChangeFIO(getActivity()) && isSupportFingerPrint && CacheManagerInstance.getInstance().isOpenBiometricAuth()) {
            FIOActivity.startAuthFingerPrint(getActivity());
        } else {
            initPwdWindow();
        }

    }

    private void initPwdWindow() {
        paymentPasswordDialogFragment = new PaymentPasswordDialogWithFioFragment();
        PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener() {
            @Override
            public void onPwdCompleteListener(String psw, boolean complete) {
                if (complete) {
                    checkPasscode(paymentPasswordDialogFragment, psw);
                }
            }

            @Override
            public void onPwdBackClickListener() {
                //内存释放问题
                VerifyPasswordCommonActivity.this.finish();
                mPwdHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (onPwdVerifyCallBack != null) {
                            onPwdVerifyCallBack.onVerifyCanceled();
                        }
                        onPwdVerifyCallBack = null;
                    }
                }, 300);

            }

            @Override
            public void onPwdFailed(String errorCode, String errorMsg) {
                showErrorDialogMsgDialog(errorMsg);
            }
        };
        paymentPasswordDialogFragment.initParams(onPwdDialogClickListener, isDissMissForForgetPwd);
        paymentPasswordDialogFragment.show(getSupportFragmentManager(), "PaymentPasswordDialogWithFioFragment");
    }

    private void showErrorDialogMsgDialog(String errorMsg) {
        try {
            CustomMsgDialogFragment dialogFragment = new CustomMsgDialogFragment();
            dialogFragment.setCanceledOnTouchOutside(false);
            dialogFragment.setMessage(errorMsg);
            dialogFragment.setCancelable(false);

            FragmentManager fm = getSupportFragmentManager();
            if (fm != null) {
                fm.executePendingTransactions();
                if (!dialogFragment.isAdded()) {
                    dialogFragment.show(fm, "CustomMsgDialogFragment");
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    private void checkPasscode(final PaymentPasswordDialogWithFioFragment paymentPasswordDialogFragment, String psw) {
        ApiProtocolImplManager.getInstance().getCheckPasswordWithEcoup(getActivity(), psw, mCurrentAction, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorDialogMsgDialog(errorMsg);
                if (paymentPasswordDialogFragment != null) {
                    paymentPasswordDialogFragment.clearPwd();
                }
                // verifyFailedResult(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (paymentPasswordDialogFragment != null) {
                    paymentPasswordDialogFragment.clearPwd();
                    paymentPasswordDialogFragment.dismiss();
                }
                verifySuccessResult();
            }
        });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_CLOSE_VERIFY_PASSWORD) {
            finish();
        }
    }


//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void onEvent(MainEventEntity event) {
//        if (event.getEventType() == MainEventEntity.EVENT_KEY_CLICK_HOME_NEED_CLOSE_PAGER) {
//            //这里有个问题 app点击home 退到后台 应该只要当前是主扫 或者被扫 才需要返回到首页
//
//            finish();
//        }
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        FIODispatcher.requestCallBack(this, requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onFioSuccess(int requestCode, int resultCode, Intent data) {
        verifySuccessResult();
    }

    @Override
    public void onFioFail(int requestCode, int resultCode, Intent data) {
        initPwdWindow();
    }

    public void verifyCanceled() {
        if (onPwdVerifyCallBack != null) {
            onPwdVerifyCallBack.onVerifyCanceled();
        }
    }

    private void verifySuccessResult() {
        finish();
        if (onPwdVerifyCallBack != null) {
            onPwdVerifyCallBack.onVerifySuccess();
        }
//        mHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //这里为什么要用延迟 为了解决dialog dismiss activity 跳转之间点击 dialog dismiss 做了延迟 造成 java.lang.IllegalArgumentException: View=DecorView@4b56afc[] not attached to window manager
//                finish();
//            }
//        },500);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (onPwdVerifyWithMajorScanCallBack != null) {
            onPwdVerifyWithMajorScanCallBack.onActivityResume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPwdHandler = new android.os.Handler();
    }

    @Override
    public void finish() {
        super.finish();
        dismissDialog();
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismiss();
        }
        overridePendingTransition(0, 0);
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }
}
