package cn.swiftpass.wallet.intl.base.otp;


import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;

public interface OTPSendPresenter<V extends IView> extends IPresenter<V> {
    void sendOTP(String walletId, String phone, String action);
}
