package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.manage.AppTestManager;


public class ApiConstant {

    //    B: Binding, V: forget Password, L:Login
    //V：注册流程  B：绑定流程  F：忘记密码流程 L:Login
    public static final String FLOW_BINDING = "B";
    public static final String FLOW_FORGET_PASSWORD = "F";
    public static final String FLOW_LOGIN = "L";
    public static final String FLOW_REGISTER = "V";
    public static final String UPDATE_SMARTACCOUNT = "U";
    public static final String FLOW_FIO = "FIO";
    public static final String VIRTUALCARD_REGISTER = "VIRREG";
    public static final String VIRTUALCARD_FORGETPWD = "VIRPWDSET";
    public static final String VIRTUALCARD_BIND = "VIRBIND";
    public static final String VIRTUALCARD_QUERY = "VIRQUERY";

    public static final String FLOW_TRANSFER_CROSS_BORDER = "FLOW_TRANSFER_CROSS_BORDER";
    //public static final String FLOW_FORGET_PASSWORD_NOT_LOGIN = "F";
    public static final String ACTIVES_LIST = "APP_INDEX_ACTIVITY_URL";
    public static final String DISCOVER_LIST = "APP_DISCOVERY_URL";
    public static final String CROSS_BORDER_PAYMENT_PLATFORM = "CROSS_BORDER_PAYMENT_PLATFORM_NEW";
    public static final String HELP_LIST = "APP_HELP_URL";
    public static final String ABOUT_LIST = "APP_ABOUT_URL";
    public static final String APP_VIRTUALCARD_URL = "APP_VIRTUALCARD_URL";


    public static final String CARD_ART_URL = BuildConfig.CARD_ART_URL;
    public static final String SMART_CARD_ART_URL = BuildConfig.SMART_CARD_ART_URL;

    public static final String WALLET_SERVICE_SIMP_URL = "https://www.bochk.com/dam/document/mbs/ewallet2018_faqt.html";
    public static final String WALLET_SERVICE_TRAD_URL = " https://www.bochk.com/dam/document/mbs/ewallet2018_faqs.html";
    public static final String WALLET_SERVICE_ENGLISH_URL = "https://www.bochk.com/dam/document/mbs/ewallet2018_faqe.html";


    //    //中銀香港電子錢包服務之條款及細則URL links
    public static String WALLET_RULE_TRAD_URL;
    public static String WALLET_RULE_SIMP_URL;
    public static String WALLET_RULE_ENGLISH_URL;


    // 中银二维条码支付服务的条款及细则
    public static String WALLET_DATA_TRAD_URL;
    public static String WALLET_DATA_SIMP_URL;
    public static String WALLET_DATA_ENGLISH_URL;


    ////中银信用卡主要条款及细则摘要
    public static String WALLET_SERVICE_URL_TRAD_URL;
    public static String WALLET_SERVICE_URL_SIMP_URL;
    public static String WALLET_SERVICE_URL_ENGLISH_URL;


    //隱私政策說明
    public static String WALLET_POLICY_TRAD_URL;
    public static String WALLET_POLICY_SIMP_URL;
    public static String WALLET_POLICY_ENGLISH_URL;


    //資料政策通告
    public static String WALLET_SERVICE_QR_TRAD_URL;
    public static String WALLET_SERVICE_QR_SIMP_URL;
    public static String WALLET_SERVICE_QR_ENGLISH_URL;

    //积分协议条款
    public static String WALLET_GRADE_SIMP_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/ZH_CN/point.html";
    public static String WALLET_GRADE_TRAD_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/ZH_HK/point.html";
    public static String WALLET_GRADE_ENGLISH_URL = AppTestManager.getInstance().getBaseUrl() + "rulepages/EN_US/point.html";


    public static String WALLET_SHARE_SIMP_URL;
    public static String WALLET_SHARE_TRAD_URL;
    public static String WALLET_SHARE_ENGLISH_URL;

    public static String CONFIG_PRD_FILE_NAME = "config_prd.xml";

    public static String WALLET_BIGSHOP_SIMP_URL;
    public static String WALLET_BIGSHOP_URL;
    public static String WALLET_BIGSHOP_TRAD_URL;
    public static String WALLET_BIGSHOP_ENGLISH_URL;


    static {
        if (BuildConfig.APP_CONFIG_NAME.equals(CONFIG_PRD_FILE_NAME)) {
            //生产环境
            //    //中銀香港電子錢包服務之條款及細則URL links
            WALLET_RULE_TRAD_URL = "https://www.bochk.com/dam/document/mbs/ewallett2018.html";
            WALLET_RULE_SIMP_URL = "https://www.bochk.com/dam/document/mbs/ewallets2018.html";
            WALLET_RULE_ENGLISH_URL = "https://www.bochk.com/dam/document/mbs/ewallete2018.html";

            WALLET_SHARE_SIMP_URL = "https://www.bochk.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=chi";
            WALLET_SHARE_TRAD_URL = "https:/www.bochk.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=smp";
            WALLET_SHARE_ENGLISH_URL = "https://www.bochk.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=eng";


            // 中银二维条码支付服务的条款及细则
            WALLET_DATA_TRAD_URL = "http://www.bochk.com/creditcard/pdf/chi/QR_Payment_TnC.pdf";
            WALLET_DATA_SIMP_URL = "http://www.bochk.com/creditcard/pdf/smp/QR_Payment_TnC.pdf";
            WALLET_DATA_ENGLISH_URL = "http://www.bochk.com/creditcard/pdf/eng/QR_Payment_TnC.pdf";


            ////中银信用卡主要条款及细则摘要
            WALLET_SERVICE_URL_TRAD_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_chi.pdf";
            WALLET_SERVICE_URL_SIMP_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_smp.pdf";
            WALLET_SERVICE_URL_ENGLISH_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_eng.pdf";


            //隱私政策說明
            WALLET_POLICY_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ppst2014.html";
            WALLET_POLICY_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ppss2014.html";
            WALLET_POLICY_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ppse2014.html";


            //資料政策通告
            WALLET_SERVICE_QR_TRAD_URL = "http://www.bochk.com/dam/document/mbs/dpnt2014.html";
            WALLET_SERVICE_QR_SIMP_URL = "http://www.bochk.com/dam/document/mbs/dpns2014.htm";
            WALLET_SERVICE_QR_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/dpne2014.htm";

            WALLET_BIGSHOP_SIMP_URL = "https://ewa.bochk.com/api/applyCreditCard/BigBigShop/smp/";
            WALLET_BIGSHOP_TRAD_URL = "https://ewa.bochk.com/api/applyCreditCard/BigBigShop/chi/";
            WALLET_BIGSHOP_ENGLISH_URL = "https://ewa.bochk.com/api/applyCreditCard/BigBigShop/eng/";

            WALLET_BIGSHOP_URL = "https://ewa.bochk.com/api/applyCreditCard";

        } else {
            //sit uat uat_internet环境

            WALLET_SHARE_SIMP_URL = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=chi";
            WALLET_SHARE_TRAD_URL = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=smp";
            WALLET_SHARE_ENGLISH_URL = "https://whkbcuat.ftcwifi.com/creditcard/BOCPAY/mgm/tnc_control.html?lang=eng";

            //中銀香港電子錢包服務之條款及細則URL links
            WALLET_RULE_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ewallett2018.html";
            WALLET_RULE_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ewallets2018.html";
            WALLET_RULE_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ewallete2018.html";
            //uat http
            WALLET_GRADE_SIMP_URL = "http://mbasit1.ftcwifi.com/rulepages/ZH_CN/point.html";
            WALLET_GRADE_TRAD_URL = "http://mbasit1.ftcwifi.com/rulepages/ZH_HK/point.html";
            WALLET_GRADE_ENGLISH_URL = "http://mbasit1.ftcwifi.com/rulepages/EN_US/point.html";

            // 中银二维条码支付服务的条款及细则
            WALLET_DATA_TRAD_URL = "http://10.41.8.72/pdf/chi/QR_Payment_TnC.pdf";
            WALLET_DATA_SIMP_URL = "http://10.41.8.72/pdf/smp/QR_Payment_TnC.pdf";
            WALLET_DATA_ENGLISH_URL = "http://10.41.8.72/pdf/eng/QR_Payment_TnC.pdf";


            ////中银信用卡主要条款及细则摘要
            WALLET_SERVICE_URL_TRAD_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_chi.pdf";
            WALLET_SERVICE_URL_SIMP_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_smp.pdf";
            WALLET_SERVICE_URL_ENGLISH_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_eng.pdf";

            //隱私政策說明
            WALLET_POLICY_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ppst2014.html";
            WALLET_POLICY_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ppss2014.html";
            WALLET_POLICY_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ppse2014.html";


            //資料政策通告
            WALLET_SERVICE_QR_TRAD_URL = "http://www.bochk.com/dam/document/mbs/dpnt2014.html";
            WALLET_SERVICE_QR_SIMP_URL = "http://www.bochk.com/dam/document/mbs/dpns2014.htm";
            WALLET_SERVICE_QR_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/dpne2014.htm";

            WALLET_BIGSHOP_SIMP_URL = "https://whkbcuat.ftcwifi.com/creditcard/BigBigShop/smp/";
            WALLET_BIGSHOP_TRAD_URL = "https://whkbcuat.ftcwifi.com/creditcard/BigBigShop/chi/";
            WALLET_BIGSHOP_ENGLISH_URL = "https://whkbcuat.ftcwifi.com/creditcard/BigBigShop/eng/";

            WALLET_BIGSHOP_URL = "https://mbauatk.ftcwifi.com/api/applyCreditCard";
        }
    }


 /*   //中銀香港電子錢包服務之條款及細則URL links
    public static final String WALLET_RULE_TRAD_URL =  "http://www.bochk.com/dam/document/mbs/ewallett2018.html";
    public static final String WALLET_RULE_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ewallets2018.html";
    public static final String WALLET_RULE_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ewallete2018.html";


    // 中银二维条码支付服务的条款及细则
    public static final String WALLET_DATA_TRAD_URL = "http://10.41.8.72/pdf/chi/QR_Payment_TnC.pdf";
    public static final String WALLET_DATA_SIMP_URL = "http://10.41.8.72/pdf/smp/QR_Payment_TnC.pdf";
    public static final String WALLET_DATA_ENGLISH_URL = "http://10.41.8.72/pdf/eng/QR_Payment_TnC.pdf";


    ////中银信用卡主要条款及细则摘要
    public static final String WALLET_SERVICE_URL_TRAD_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_chi.pdf";
    public static final String WALLET_SERVICE_URL_SIMP_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_smp.pdf";
    public static final String WALLET_SERVICE_URL_ENGLISH_URL = "http://10.41.8.72/bocci/agt/key_facts_statement_eng.pdf";
*/

    //隱私政策說明
/*    public static final String WALLET_POLICY_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ppst2014.html";
    public static final String WALLET_POLICY_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ppss2014.html";
    public static final String WALLET_POLICY_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ppse2014.html";


    //資料政策通告
    public static final String WALLET_SERVICE_QR_TRAD_URL = "http://www.bochk.com/dam/document/mbs/dpnt2014.html";
    public static final String WALLET_SERVICE_QR_SIMP_URL = "http://www.bochk.com/dam/document/mbs/dpns2014.htm";
    public static final String WALLET_SERVICE_QR_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/dpne2014.htm";*/




/*//    //中銀香港電子錢包服務之條款及細則URL links
    public static final String WALLET_RULE_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ewallett2018.html";
    public static final String WALLET_RULE_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ewallets2018.html";
    public static final String WALLET_RULE_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ewallete2018.html";


    // 中银二维条码支付服务的条款及细则
    public static final String WALLET_DATA_TRAD_URL = "http://www.bochk.com/creditcard/pdf/chi/QR_Payment_TnC.pdf";
    public static final String WALLET_DATA_SIMP_URL = "http://www.bochk.com/creditcard/pdf/smp/QR_Payment_TnC.pdf";
    public static final String WALLET_DATA_ENGLISH_URL = "http://www.bochk.com/creditcard/pdf/eng/QR_Payment_TnC.pdf";


    ////中银信用卡主要条款及细则摘要
    public static final String WALLET_SERVICE_URL_TRAD_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_chi.pdf";
    public static final String WALLET_SERVICE_URL_SIMP_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_smp.pdf";
    public static final String WALLET_SERVICE_URL_ENGLISH_URL = "http://www.bochk.com/creditcard/bocci/agt/key_facts_statement_eng.pdf";


    //隱私政策說明
    public static final String WALLET_POLICY_TRAD_URL = "http://www.bochk.com/dam/document/mbs/ppst2014.html";
    public static final String WALLET_POLICY_SIMP_URL = "http://www.bochk.com/dam/document/mbs/ppss2014.html";
    public static final String WALLET_POLICY_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/ppse2014.html";


    //資料政策通告
    public static final String WALLET_SERVICE_QR_TRAD_URL = "http://www.bochk.com/dam/document/mbs/dpnt2014.html";
    public static final String WALLET_SERVICE_QR_SIMP_URL = "http://www.bochk.com/dam/document/mbs/dpns2014.htm";
    public static final String WALLET_SERVICE_QR_ENGLISH_URL = "http://www.bochk.com/dam/document/mbs/dpne2014.htm";*/


}
