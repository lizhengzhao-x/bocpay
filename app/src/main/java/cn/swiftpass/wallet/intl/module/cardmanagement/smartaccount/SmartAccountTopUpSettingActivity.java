package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.AmountSetDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView;
import cn.swiftpass.wallet.intl.widget.ImageTopUpView;

/**
 * @author Created by on 2018/7/25.
 */

public class SmartAccountTopUpSettingActivity extends BaseCompatActivity {


    @BindView(R.id.id_topup_direct)
    ImageTopUpView mTopUpDirectLayout;
    @BindView(R.id.id_topup_auto)
    ImageTopUpView mTopUpAutoLayout;
    @BindView(R.id.id_topup_manual)
    ImageTopUpView mTopUpManualLayout;
    @BindView(R.id.id_edit_daily_amount)
    ImageArrowNoPaddingView mIdEditDailyAmount;
    @BindView(R.id.id_seekbar)
    SeekBar mSeekbar;
    @BindView(R.id.id_money_min)
    TextView mMoneyMinTV;
    @BindView(R.id.id_money_max)
    TextView mMoneyMaxTV;
    @BindView(R.id.ll_topup_setting)
    LinearLayout mTopUpDetailLayout;

    @BindView(R.id.tv_submit)
    TextView mSubmitTV;
    @BindView(R.id.id_hint_text)
    TextView idHintText;

    MySmartAccountEntity mSmartAccountInfo;
    private String mCurrentTopupMethod;
    private boolean isAuSmallMode = false;

    int mLimitMin;
    int mLimitMax;
    int mOldLimit;
    private int currentLimit;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.P3_C0_6);
        Intent intent = getIntent();
        EventBus.getDefault().register(this);
        if (null == intent) {
            finish();
        }
        mSmartAccountInfo = (MySmartAccountEntity) intent.getSerializableExtra(SmartAccountConst.SMART_ACCOUNT);
        if (null == mSmartAccountInfo) {
            finish();
        }
        if (!TextUtils.isEmpty(mSmartAccountInfo.getMethodMinPay())) {
            mLimitMin = Double.valueOf(mSmartAccountInfo.getMethodMinPay()).intValue();
        } else {
            mLimitMin = 0;
        }

        if (!TextUtils.isEmpty(mSmartAccountInfo.getMethodMaxPay())) {
            mLimitMax = Double.valueOf(mSmartAccountInfo.getMethodMaxPay()).intValue();
        } else {
            mLimitMax = 5000;
        }

        if (!TextUtils.isEmpty(mSmartAccountInfo.getAutotopupamt())) {
            mOldLimit = Double.valueOf(mSmartAccountInfo.getAutotopupamt()).intValue();
            currentLimit = mOldLimit;
        }
        initSeekbar();
        initTopupMethod();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            finish();
        }
    }

    private void initTopupMethod() {
        if (TextUtils.equals(mSmartAccountInfo.getAddedmethod(), SmartAccountConst.TOPUP_METHOD_DIRECT)) {
            onClickTopMethod(R.id.id_topup_direct);
            idHintText.setText(getString(R.string.SMA2101_1_6));
        } else if (TextUtils.equals(mSmartAccountInfo.getAddedmethod(), SmartAccountConst.TOPUP_METHOD_MANUAL)) {
            onClickTopMethod(R.id.id_topup_manual);
            idHintText.setText(getString(R.string.SMA2101_1_12));
        } else if (TextUtils.equals(mSmartAccountInfo.getAddedmethod(), SmartAccountConst.TOPUP_METHOD_AUTO)) {
            onClickTopMethod(R.id.id_topup_auto);
            idHintText.setText(getString(R.string.SMA2101_1_9));
        }
    }


    private void initSeekbar() {

        String strMin = AndroidUtils.formatPriceInt(mLimitMin, true);
        mMoneyMinTV.setText(mSmartAccountInfo.getCurrency() + " " + strMin);

        String str = AndroidUtils.formatPriceInt(mLimitMax, true);
        mMoneyMaxTV.setText(mSmartAccountInfo.getCurrency() + " " + str);
        mSeekbar.setMax(100);
        mSeekbar.setProgress((int) (mOldLimit * 1.0 / mLimitMax * 1.0 * 100));
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentLimit = (int) ((progress * 1.0 / 100.0) * mLimitMax);
                currentLimit = currentLimit / 100 * 100;

                String amount = AndroidUtils.formatPriceInt(currentLimit, true);
                mIdEditDailyAmount.setLeftTextTitle(mSmartAccountInfo.getCurrency() + " " + amount);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mIdEditDailyAmount.setLeftTextTitle(mSmartAccountInfo.getCurrency() + " " + new BigDecimal(mSmartAccountInfo.getAutotopupamt()).intValue());
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_smartaccount_topup_setting;
    }


    private void onClickAmountLimit() {
        AmountSetDialog.Builder builder = new AmountSetDialog.Builder(this);
        AmountSetDialog dialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                //不太确认seekbar setProgress = 1 的时候移动显示是 0
                int limitAmount = ((AmountSetDialog) dlg).getmAmount();
                int value = (int) ((limitAmount / Float.valueOf(mLimitMax)) * 100);
                if (value < 150) {
                    // 设置很小的时候放大数据 显示放大 解决显示问题
                    mSeekbar.setProgress(value + 1);
                } else {
                    mSeekbar.setProgress(value);
                }
                mIdEditDailyAmount.setLeftTextTitle(mSmartAccountInfo.getCurrency() + " " + String.valueOf(limitAmount));
                currentLimit = limitAmount;
            }
        }).create(-1, mLimitMax);
        dialog.show();
    }

    @OnClick({R.id.id_topup_direct, R.id.id_topup_auto, R.id.id_edit_daily_amount, R.id.id_seekbar, R.id.id_money_min, R.id.id_money_max, R.id.id_topup_manual, R.id.tv_submit})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.id_topup_direct:
                onClickTopMethod(R.id.id_topup_direct);
                idHintText.setText(getString(R.string.SMA2101_1_6));
                break;
            case R.id.id_topup_auto:
                onClickTopMethod(R.id.id_topup_auto);
                idHintText.setText(getString(R.string.SMA2101_1_9));
                break;
            case R.id.id_topup_manual:
                onClickTopMethod(R.id.id_topup_manual);
                idHintText.setText(getString(R.string.SMA2101_1_12));
                break;
            case R.id.id_edit_daily_amount:
                onClickAmountLimit();
                break;
            case R.id.tv_submit:
                onSubmit();
                break;
            default:
                break;
        }
    }

    private void onSubmit() {
        mCurrentTopupMethod = SmartAccountConst.TOPUP_METHOD_DIRECT;
        if (mTopUpDirectLayout.isCheck()) {
            mCurrentTopupMethod = SmartAccountConst.TOPUP_METHOD_DIRECT;
        } else if (mTopUpManualLayout.isCheck()) {
            mCurrentTopupMethod = SmartAccountConst.TOPUP_METHOD_MANUAL;
        } else if (mTopUpAutoLayout.isCheck()) {
            mCurrentTopupMethod = SmartAccountConst.TOPUP_METHOD_AUTO;
        }

        if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_AUTO)) {
            //自动增值 0处理
            if (currentLimit == 0) {
                showErrorMsgDialog(mContext, getString(R.string.title_string_topsetting_error));
                return;
            }
        }

        String oldTopupMethod = mSmartAccountInfo.getAddedmethod();
        if (oldTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_DIRECT)) {
            //之前是直接
            if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_DIRECT)) {
                //未改动
                finish();
            } else if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_MANUAL)) {
                //直接改为手动 不需要发发送otp
                initPasswordDialog();
            } else {
                //直接改为自动 需要发发送otp
                exchangeMoneyType(mCurrentTopupMethod, currentLimit + "");
            }

        } else if (oldTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_MANUAL)) {
            //之前是手动
            if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_DIRECT)) {
                exchangeType(mCurrentTopupMethod);
            } else if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_MANUAL)) {
                finish();
            } else {
                exchangeMoneyType(mCurrentTopupMethod, currentLimit + "");
            }
        } else {
            //之前是自动
            if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_DIRECT)) {
                exchangeType(mCurrentTopupMethod);
            } else if (mCurrentTopupMethod.equals(SmartAccountConst.TOPUP_METHOD_MANUAL)) {
                initPasswordDialog();
            } else {
                //自动 --->自动 金额变大
                if (currentLimit > mOldLimit) {
                    exchangeMoneyType(mCurrentTopupMethod, currentLimit + "");
                } else {
                    isAuSmallMode = true;
                    initPasswordDialog();
                }
                //自动 --->自动 金额变小
            }
        }

    }

    private void initPasswordDialog() {

        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                updateSmartAccountInfo();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

//        if (MyFingerPrintManager.getInstance().isRegisterFIO(getActivity()) && !MyFingerPrintManager.getInstance().isChangeFIO(getActivity())) {
//            FIOActivity.startAuthFingerPrint(getActivity());
//        } else {
//            initPwdWindow();
//        }
    }

//    private void initPwdWindow() {
//        mPwdPopWindos = AndroidUtils.showPwdDialog(this, false, this);
//    }


    private void exchangeType(String mTopupMethod) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_UPDATE_RECHARGETYPE);
        mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_M);
        mHashMapsLogin.put(Constants.DATA_ADD_METHOD, mTopupMethod);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
        ActivitySkipUtil.startAnotherActivity(SmartAccountTopUpSettingActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void exchangeMoneyType(String mTopupMethod, String topMoney) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_UPDATE_RECHARGETYPE);
        mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_M);
        mHashMapsLogin.put(Constants.DATA_ADD_METHOD, mTopupMethod);
        mHashMapsLogin.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
        mHashMapsLogin.put(Constants.MONEY, topMoney);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER));
        ActivitySkipUtil.startAnotherActivity(SmartAccountTopUpSettingActivity.this, SmartAccountTopUpSettingDetailActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void onClickTopMethod(int viewId) {
        switch (viewId) {
            case R.id.id_topup_direct:
                mTopUpDirectLayout.switchCheck();
                if (mTopUpDirectLayout.isCheck()) {
                    mTopUpManualLayout.setCheck(false);
                    mTopUpAutoLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.GONE);
                }
                break;
            case R.id.id_topup_auto:
                mTopUpAutoLayout.switchCheck();
                if (mTopUpAutoLayout.isCheck()) {
                    mTopUpManualLayout.setCheck(false);
                    mTopUpDirectLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.id_topup_manual:
                mTopUpManualLayout.switchCheck();
                if (mTopUpManualLayout.isCheck()) {
                    mTopUpDirectLayout.setCheck(false);
                    mTopUpAutoLayout.setCheck(false);
                    mTopUpDetailLayout.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }

    }

    public static void startActivity(Activity activity, MySmartAccountEntity smartAccountInfo, String phoneNumber) {
        Intent intent = new Intent(activity, SmartAccountTopUpSettingActivity.class);
        intent.putExtra(SmartAccountConst.SMART_ACCOUNT, smartAccountInfo);
        intent.putExtra(Constants.DATA_PHONE_NUMBER, phoneNumber);
        activity.startActivity(intent);
    }

//    /**
//     * 校验支付密码
//     *
//     * @param pwd
//     */
//    private void verifypwdEvent(String pwd) {
//        ApiProtocolImplManager.getInstance().getCheckPassword(this, pwd, new NetWorkCallbackListener<BaseEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                showErrorMsgDialog(mContext, errorMsg);
//            }
//
//            @Override
//            public void onSuccess(BaseEntity response) {
//                mPwdPopWindos.dismiss();
//                updateSmartAccountInfo();
//            }
//        });
//    }

    private void updateSmartAccountInfo() {
        //修改充值方式
        ApiProtocolImplManager.getInstance().updateSmartAccountTopUp(mContext, mCurrentTopupMethod, isAuSmallMode ? currentLimit + "" : null, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onSuccess(BaseEntity response) {
                showErrorMsgDialog(mContext, getString(R.string.title_saved), getString(R.string.string_modify_information), new OnMsgClickCallBack() {
                    @Override
                    public void onBtnClickListener() {
                        EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                        finish();
                    }
                });
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

//    @Override
//    public void onPopWindowClickListener(String psw, boolean complete) {
//        if (complete) {
//            verifypwdEvent(psw);
//        }
//    }

//    @Override
//    public void onPopWindowBackClickListener() {
//
//    }
}
