package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.adapter.AreaSelectionAdapter;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.AreaSelectionEntity;

import static cn.swiftpass.wallet.intl.module.register.IdAuthInfoFillActivity.CHOICE_AREA_CODE;


/**
 * 地区选中
 */
public class AreaSelectionIDAuthActivity extends BaseCompatActivity {

    private RecyclerView mRecyclerView;
    private List<AreaSelectionEntity> mList = new ArrayList<>();
    private AreaSelectionAdapter mAdapter;
    private WeakHandler mHandler;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        initData();
        setToolBarTitle(R.string.title_region);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_info);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AreaSelectionAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mHandler = new WeakHandler(AreaSelectionIDAuthActivity.this);
        mAdapter.setOnItemClickListener(new AreaSelectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position, final String area) {
                showLoading(AreaSelectionIDAuthActivity.this, true);
                for (int i = 0; i < mList.size(); i++) {
                    if (i == position) {
                        mList.get(i).setSelection(true);
                    } else {
                        mList.get(i).setSelection(false);
                    }
                }
                mAdapter.notifyDataSetChanged();
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent();
                        intent.putExtra(Constants.CHOICE_AREA, area);
                        intent.putExtra(Constants.CHOICE_AREA_POSITION, position);
                        setResult(CHOICE_AREA_CODE, intent);
                        hideLoading();
                        AreaSelectionIDAuthActivity.this.finish();
                    }
                }, 500);
            }
        });
    }

    private void initData() {
        mList = (List<AreaSelectionEntity>) getIntent().getSerializableExtra(Constants.CHOICE_AREA);
        if (mList == null) {
            mList = new ArrayList<>();
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_area_selection;
    }


    public static class WeakHandler extends Handler {
        private WeakReference<AreaSelectionIDAuthActivity> mActivity;

        public WeakHandler(AreaSelectionIDAuthActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }
    }
}
