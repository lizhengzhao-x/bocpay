package cn.swiftpass.wallet.intl.module.setting.paymentsetting;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.PaymentListEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;


public class SettingPaymentDefaultActivity extends BaseCompatActivity {
    @BindView(R.id.id_recyclerview)
    RecyclerView idRecyclerview;
    //支付列表
    private ArrayList<PaymentListEntity.RowsBean.TypesBean> cardEntities;
    private String cardId;


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        setToolBarTitle(R.string.title_default_account);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        cardEntities = new ArrayList<>();
        List<PaymentListEntity.RowsBean.TypesBean> typesBeans = (List<PaymentListEntity.RowsBean.TypesBean>) getIntent().getExtras().getSerializable(Constants.EXTRA_TYPE);
        cardId = getIntent().getExtras().getString(Constants.CARD_ID);
        cardEntities.addAll(typesBeans);
        refreshViewWithData();
    }


    private void refreshViewWithData() {
        for (int i = 0; i < cardEntities.size(); i++) {
            PaymentListEntity.RowsBean.TypesBean typesBean = cardEntities.get(i);
            typesBean.setShowTitle(typesBean.getPayType().equals("1") ? getResources().getString(R.string.A1_2_1) : getResources().getString(R.string.A1_3_1));
        }

        ((IAdapter<PaymentListEntity.RowsBean.TypesBean>) idRecyclerview.getAdapter()).setData(cardEntities);
        idRecyclerview.getAdapter().notifyDataSetChanged();
    }

    private void setDefaultCardRequest(String newPanId, final String defaultPayType) {
        ApiProtocolImplManager.getInstance().setDefaultCard(mContext, newPanId, defaultPayType, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                AndroidUtils.showTipDialog(SettingPaymentDefaultActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                Intent intent = new Intent();
                SettingPaymentDefaultActivity.this.setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_setdefaultcard;
    }


    private CommonRcvAdapter<PaymentListEntity.RowsBean.TypesBean> getAdapter(final List<PaymentListEntity.RowsBean.TypesBean> data) {
        return new CommonRcvAdapter<PaymentListEntity.RowsBean.TypesBean>(data) {

            @Override
            public Object getItemType(PaymentListEntity.RowsBean.TypesBean demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new ShowDefaultCardListAdapter(mContext, new ShowDefaultCardListAdapter.CardItemSelCallback() {
                    @Override
                    public void onItemCardSelClick(int position) {
                        if (cardEntities.size() == 0) {
                            finish();
                            return;
                        }
                        setDefaultCardRequest(cardId, cardEntities.get(position).getPayType());
                    }
                });
            }
        };

    }

}
