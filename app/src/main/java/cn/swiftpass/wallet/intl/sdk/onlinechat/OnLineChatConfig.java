package cn.swiftpass.wallet.intl.sdk.onlinechat;

/**
 * Created by ZhangXinchao on 2019/12/5.
 */
public interface OnLineChatConfig {


    String PAGEID_REGISTER_INPUTADDRESS = "Register_InputAddress";
    String PAGEID_REGISTER_INPUTRESIDENTNUMBER = "Register_InputResidenNumber";
    String PAGEID_REGISTER_SETDAILYTRANSACTIONLIMIT = "Register_SetDailyTransactionLimit";
    String PAGEID_REGISTER_CONFIRMREGISTERDATA = "Register_ConfirmRegisterData";
    String PAGEID_REGISTER_COMPAREPEOPLEANDCARDS = "Register_ComparePeopleAndCards";
    String PAGEID_REGISTER_SETPAYMENTPASSCODE = "Register_SetPaymentPasscode";
    String PAGEID_REGISTER_CONFIRMPAYMENTPASSCODE = "Register_ConfirmPaymentPasscode";
    String PAGEID_REGISTER_SENDOTP = "Register_SendOTP";
    String PAGEID_HELPPAGE_HELP = "HelpPage_Help";
    String FUNCTIONID_REGISTER = "Register";
    String FUNCTIONID_HELP = "Help";


}
