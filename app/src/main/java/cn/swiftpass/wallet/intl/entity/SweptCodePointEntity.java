package cn.swiftpass.wallet.intl.entity;

import android.graphics.Color;
import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class SweptCodePointEntity extends BaseEntity {


    private String trxAmt;
    private String trxCurrency;
    private String merchantName;
    private String panFour;
    private String avaGpCount;
    private String redeemGpCount;
    private String redeemGpTnxAmt;
    private String gpBalance;
    private String gpRequired;
    private String payAmt;
    private String smaUsedGp;


    /**
     * 特殊颜色
     */
    private String colorSign;

    public String getGpMsgUrl() {
        if (TextUtils.isEmpty(gpMsgUrl)) {
            return "";
        }
        return gpMsgUrl;
    }

    public void setGpMsgUrl(String gpMsgUrl) {
        this.gpMsgUrl = gpMsgUrl;
    }

    private String gpMsgUrl;

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    private String txnId;

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    private String postscript;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOriginalAmt() {
        return originalAmt;
    }

    public void setOriginalAmt(String originalAmt) {
        this.originalAmt = originalAmt;
    }

    private String discount;
    private String originalAmt;

    public String getSmaType() {
        return smaType;
    }

    public void setSmaType(String smaType) {
        this.smaType = smaType;
    }

    //2：支付账户 3：智能账户
    private String smaType;

    public String getSmaUsedGp() {
        return smaUsedGp;
    }

    public void setSmaUsedGp(String smaUsedGp) {
        this.smaUsedGp = smaUsedGp;
    }

    public String getCcUsedGP() {
        return ccUsedGP;
    }

    public void setCcUsedGP(String ccUsedGP) {
        this.ccUsedGP = ccUsedGP;
    }

    private String ccUsedGP;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    private String cardType;

    public String getIsProm() {
        return isProm;
    }

    public void setIsProm(String isProm) {
        this.isProm = isProm;
    }

    /**
     * 是否是特别兑换率
     *
     * @return
     */
    public boolean isProm() {
        if (TextUtils.isEmpty(isProm)) {
            return false;
        } else {
            return isProm.equals("Y") || isProm.equals("A");
        }
    }

    private String isProm;//是否是特别兑换率

    public String getRedeemStatusMsg() {
        return redeemStatusMsg;
    }

    public void setRedeemStatusMsg(String redeemStatusMsg) {
        this.redeemStatusMsg = redeemStatusMsg;
    }

    private String redeemStatusMsg;

    public String getTranCur() {
        return tranCur;
    }

    public void setTranCur(String tranCur) {
        this.tranCur = tranCur;
    }

    private String tranCur;
    private String redeemFlag;

    public String getGpComment() {
        return gpComment;
    }

    public void setGpComment(String gpComment) {
        this.gpComment = gpComment;
    }

    private String gpComment;


    public String getRedeemStatus() {
        return redeemStatus;
    }

    public void setRedeemStatus(String redeemStatus) {
        this.redeemStatus = redeemStatus;
    }

    private String redeemStatus;

    public String getGpDateCmt() {
        return gpDateCmt;
    }

    public void setGpDateCmt(String gpDateCmt) {
        this.gpDateCmt = gpDateCmt;
    }

    private String gpDateCmt;


    public String getTrxAmt() {
        return trxAmt;
    }

    public void setTrxAmt(String trxAmt) {
        this.trxAmt = trxAmt;
    }

    public String getTrxCurrency() {
        return trxCurrency;
    }

    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    public String getAvaGpCount() {
        return avaGpCount;
    }

    public void setAvaGpCount(String avaGpCount) {
        this.avaGpCount = avaGpCount;
    }

    public String getRedeemGpCount() {
        return redeemGpCount;
    }

    public void setRedeemGpCount(String redeemGpCount) {
        this.redeemGpCount = redeemGpCount;
    }

    public String getRedeemGpTnxAmt() {
        return redeemGpTnxAmt;
    }

    public void setRedeemGpTnxAmt(String redeemGpTnxAmt) {
        this.redeemGpTnxAmt = redeemGpTnxAmt;
    }

    public String getGpBalance() {
        return gpBalance;
    }

    public void setGpBalance(String gpBalance) {
        this.gpBalance = gpBalance;
    }

    public String getGpRequired() {
        return gpRequired;
    }

    public void setGpRequired(String gpRequired) {
        this.gpRequired = gpRequired;
    }

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getRedeemFlag() {
        return redeemFlag;
    }

    public void setRedeemFlag(String redeemFlag) {
        this.redeemFlag = redeemFlag;
    }

    public int getColorSign() {
        if (TextUtils.isEmpty(colorSign)) {
            return Color.BLACK;
        }
        return Color.parseColor(colorSign);
    }

    public void setColorSign(String colorSign) {
        this.colorSign = colorSign;
    }


    /**
     * 主要被扫  返回的实体类不同 所以需要转换成一个
     *
     * @return
     */
    public PaymentEnquiryResult convertBySweptCodePointEntity() {

        PaymentEnquiryResult paymentEnquiryResult = new PaymentEnquiryResult();
        paymentEnquiryResult.setMerchantName(merchantName);
        paymentEnquiryResult.setCardType(cardType);
        paymentEnquiryResult.setPanFour(panFour);
        paymentEnquiryResult.setTrxCurrency(trxCurrency);
        paymentEnquiryResult.setOriginalAmt(originalAmt);
        paymentEnquiryResult.setDiscount(discount);
        paymentEnquiryResult.setGpBalance(gpBalance);
        paymentEnquiryResult.setRedeemGpTnxAmt(redeemGpTnxAmt);
        paymentEnquiryResult.setRedeemCount(redeemGpCount);
        paymentEnquiryResult.setRedeemStatus(redeemStatus);
        paymentEnquiryResult.setRedeemStatusMsg(redeemStatusMsg);
        paymentEnquiryResult.setIsProm(isProm);
        paymentEnquiryResult.setSmaType(smaType);
        paymentEnquiryResult.setSmaUsedGp(smaUsedGp);
        paymentEnquiryResult.setCcUsedGP(ccUsedGP);
        paymentEnquiryResult.setDiscount(discount);
        paymentEnquiryResult.setPayWithPointFlag("1");
        paymentEnquiryResult.setTrxAmt(trxAmt);
        paymentEnquiryResult.setPostscript(postscript);
        paymentEnquiryResult.setColorSign(colorSign);
        return paymentEnquiryResult;
    }
}
