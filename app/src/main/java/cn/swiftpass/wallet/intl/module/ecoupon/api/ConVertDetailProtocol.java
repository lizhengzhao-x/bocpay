package cn.swiftpass.wallet.intl.module.ecoupon.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class ConVertDetailProtocol extends BaseProtocol {

    private String giftCode;
    private String referenceNo;
    private String orderType;

    public ConVertDetailProtocol(String giftCodeIn, String orderTypeIn,String referenceNoIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/applyVoucherDetail";
        this.giftCode = giftCodeIn;
        this.referenceNo = referenceNoIn;
        this.orderType = orderTypeIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.GIFTCODE, giftCode);
        mBodyParams.put(RequestParams.REFERENCENO, referenceNo);
        mBodyParams.put(RequestParams.ORDERTP, orderType);
    }
}
