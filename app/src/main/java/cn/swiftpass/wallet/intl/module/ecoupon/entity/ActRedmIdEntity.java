package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/8/9.
 */
public class ActRedmIdEntity extends BaseEntity {

    private String redmId;

    public String getRedmId() {
        return redmId;
    }

    public void setRedmId(String redmId) {
        this.redmId = redmId;
    }

    public String getGiftTp() {
        return giftTp;
    }

    public void setGiftTp(String giftTp) {
        this.giftTp = giftTp;
    }

    private String giftTp;

}
