package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardRewardHistoryItemList extends BaseEntity {
    private String bannerUrl;
    private String content;
    private String title;
    private String details;
    private List<CreditCardRewardHistoryInfo> rewardsInfo;
    private String rewardsType;

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public List<CreditCardRewardHistoryInfo> getRewardsInfo() {
        return rewardsInfo;
    }

    public void setRewardsInfo(List<CreditCardRewardHistoryInfo> rewardsInfo) {
        this.rewardsInfo = rewardsInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRewardsType() {
        return rewardsType;
    }

    public void setRewardsType(String rewardsType) {
        this.rewardsType = rewardsType;
    }

}
