package cn.swiftpass.wallet.intl.module.creditcardreward.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardDateItemList;
import cn.swiftpass.wallet.intl.entity.CreditCardRewardHistoryList;
import cn.swiftpass.wallet.intl.entity.MyCreditCardRewardActionList;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditCardRewardListContact;
import cn.swiftpass.wallet.intl.module.creditcardreward.contract.CreditRewardType;
import cn.swiftpass.wallet.intl.module.creditcardreward.presenter.CreditCardRewardHistoryPresenter;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.CreditCardRewardHistoryAdapter;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.OnRewadDetailListener;
import cn.swiftpass.wallet.intl.module.creditcardreward.view.adapter.OnRewadLoadMoreListener;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.widget.dialog.CreditCardRewardDateDialog;
import cn.swiftpass.wallet.intl.widget.dialog.CreditCardRewardTypeDialog;
import cn.swiftpass.wallet.intl.widget.dialog.OnCardRewardDateListener;
import cn.swiftpass.wallet.intl.widget.dialog.OnCardRewardTypeListener;

public class CreditCardRewardHistoryListFragment extends BaseFragment<CreditCardRewardListContact.Presenter> implements CreditCardRewardListContact.View, OnCardRewardTypeListener, OnCardRewardDateListener, OnRewadLoadMoreListener, OnRewadDetailListener {


    @BindView(R.id.tv_reward_history_date)
    TextView tvRewardHistoryDate;
    @BindView(R.id.iv_reward_history_date)
    ImageView ivRewardHistoryDate;
    @BindView(R.id.tv_reward_history_reward_type)
    TextView tvRewardHistoryRewardType;
    @BindView(R.id.iv_reward_history_reward_type)
    ImageView ivRewardHistoryRewardType;
    @BindView(R.id.ry_history_reward)
    RecyclerView ryHistoryReward;
    @BindView(R.id.srl_my_credit_card_reward)
    SmartRefreshLayout srlMyCreditCardReward;

    @BindView(R.id.ll_credit_card_reward_empty)
    LinearLayout llCreditCardRewardEmpty;
    @BindView(R.id.ll_filter)
    LinearLayout llFilter;
    private CreditCardRewardDateItemList selectDateType;
    private String rewardsType = CreditRewardType.REWARD_TYPE_ALL;
    private int pageNum = 1;
    private CreditCardRewardHistoryAdapter adapter;
    private CreditCardRewardTypeDialog creditCardRewardTypeDialog;
    private CreditCardRewardDateDialog creditCardRewardDateDialog;
    private int selectDateIndex;
    private CreditCardRewardDateItemList defaultDateItem;
    private List<CreditCardRewardDateItemList> queryDateList;

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {

    }

    @Override
    protected CreditCardRewardListContact.Presenter loadPresenter() {
        return new CreditCardRewardHistoryPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_credit_card_reward_list;
    }

    @Override
    protected void initView(View v) {
        llCreditCardRewardEmpty.setVisibility(View.GONE);
        llFilter.setVisibility(View.GONE);
        ryHistoryReward.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, RecyclerView.VERTICAL, false);
        ryHistoryReward.setLayoutManager(layoutManager);
        adapter = new CreditCardRewardHistoryAdapter(mContext, this, this);
        ryHistoryReward.setAdapter(adapter);
        initListener();
        tvRewardHistoryRewardType.setText(getString(R.string.CRQ2107_2_10));
        mPresenter.getRewardHistoryList("", rewardsType, pageNum, true);

    }

    private void initListener() {
        srlMyCreditCardReward.setOnRefreshListener(refreshLayout -> {
            pageNum = 1;
            String dateKey = "";
            if (selectDateType != null) {
                dateKey = selectDateType.getDateKey();
            }
            mPresenter.getRewardHistoryList(dateKey, rewardsType, pageNum, true);
        });

    }

    @Override
    public void getHistoryCreditCardRewardListError(String errorCode, String errorMsg, boolean isRefresh) {
        srlMyCreditCardReward.finishRefresh(false);
    }

    @Override
    public void getHistoryCreditCardRewardListSuccess(CreditCardRewardHistoryList response, boolean isRefresh) {
        srlMyCreditCardReward.finishRefresh();
        //页面创建的时候，筛选条件为隐藏状态，只要有一次接口成功，那么筛选条件就可以展示
        if (response != null && response.getQueryDateList() != null && response.getQueryDateList().size() > 0) {
            llFilter.setVisibility(View.VISIBLE);
            queryDateList = response.getQueryDateList();
            defaultDateItem = getDefaultDate(queryDateList);
            //如果有选中的情况下，不再刷新为默认的选中
            if (selectDateType != null) {
                tvRewardHistoryDate.setText(selectDateType.getDisplay());
            } else {
                if (defaultDateItem != null) {
                    selectDateType = defaultDateItem;
                    tvRewardHistoryDate.setText(selectDateType.getDisplay());
                }
            }

        }

        //页面创建的时候，列表为显示状态，空列表为隐藏状态
        if (response != null && response.getRows() != null && response.getRows().size() > 0) {
            llCreditCardRewardEmpty.setVisibility(View.GONE);
            ryHistoryReward.setVisibility(View.VISIBLE);
            //因为上拉加载，只有2次，第一次为10个数据，第二次为全部的数据，所以用setData和下拉刷新一样
            adapter.setData(response.getRows(), !MyCreditCardRewardActionList.NOT_HAS_NEXT_PAGE.equals(response.getHasNext()), response.getLastRewardDate());
            adapter.notifyDataSetChanged();
        } else {
            llCreditCardRewardEmpty.setVisibility(View.VISIBLE);
            ryHistoryReward.setVisibility(View.GONE);
        }
    }

    private CreditCardRewardDateItemList getDefaultDate(List<CreditCardRewardDateItemList> list) {
        for (int i = 0; i < list.size(); i++) {
            CreditCardRewardDateItemList rewardDateItemList = list.get(i);
            if (rewardDateItemList != null) {
                if (CreditRewardType.REWARD_DATE_TYPE_DEFAULT.equals(rewardDateItemList.getDateKey())) {
                    selectDateIndex = i;
                    return rewardDateItemList;
                }
            }
        }

        return null;
    }

    @OnClick({R.id.ll_filter_date, R.id.ll_filter_reward_type})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_filter_date:
                if (creditCardRewardDateDialog == null) {
                    creditCardRewardDateDialog = CreditCardRewardDateDialog.with(mActivity)
                            .setData(queryDateList)
                            .setListener(CreditCardRewardHistoryListFragment.this);
                    creditCardRewardDateDialog.setSelectType(selectDateIndex);
                } else {
                    creditCardRewardDateDialog.setSelectType(selectDateIndex);
                }
                creditCardRewardDateDialog.show();
                break;
            case R.id.ll_filter_reward_type:
                if (creditCardRewardTypeDialog == null) {
                    creditCardRewardTypeDialog = CreditCardRewardTypeDialog.with(mActivity)
                            .setListener(CreditCardRewardHistoryListFragment.this);
                } else {
                    creditCardRewardTypeDialog.setSelectType(rewardsType);
                }
                creditCardRewardTypeDialog.show();
                break;
            default:
                break;
        }
    }

    @Override
    public void OnSelectRewardType(String selectType) {
        rewardsType = selectType;
        if (CreditRewardType.REWARD_TYPE_ALL.equals(selectType)) {
            tvRewardHistoryRewardType.setText(R.string.CRQ2107_2_10);
        } else if (CreditRewardType.REWARD_TYPE_POINT.equals(selectType)) {
            tvRewardHistoryRewardType.setText(R.string.CRQ2107_2_8);
        } else if (CreditRewardType.REWARD_TYPE_CASH.equals(selectType)) {
            tvRewardHistoryRewardType.setText(R.string.CRQ2107_2_9);
        }
        refreshData();
    }

    private void refreshData() {
        if (adapter.getData() != null) {
            adapter.getData().clear();
            adapter.notifyDataSetChanged();
            String dateKey = "";
            if (selectDateType != null) {
                dateKey = selectDateType.getDateKey();
            }
            mPresenter.getRewardHistoryList(dateKey, rewardsType, 1, true);
        }
    }

    @Override
    public void OnSelectRewardDate(CreditCardRewardDateItemList selectType, int selectIndex) {
        selectDateType = selectType;
        if (selectDateType != null) {
            tvRewardHistoryDate.setText(selectDateType.getDisplay());
        }
        selectDateIndex = selectIndex;
        refreshData();
    }

    @Override
    public void onLoadMore() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (pageNum < 1) {
                    pageNum = 1;
                }
                String dateKey = "";
                if (selectDateType != null) {
                    dateKey = selectDateType.getDateKey();
                }
                mPresenter.getRewardHistoryList(dateKey, rewardsType, pageNum + 1, false);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {


            }
        });

    }

    @Override
    public void OnJumpDetail(String details) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, details);
        mHashMaps.put(Constants.DETAIL_TITLE, mActivity.getString(R.string.CRQ2107_1_8));
        mHashMaps.put(WebViewActivity.NEW_MSG, false);
        ActivitySkipUtil.startAnotherActivity(mActivity, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }
}
