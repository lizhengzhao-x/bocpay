/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import java.io.Serializable;

/**
 * Class Name: TxnHistEnquiryResult<br/>
 *
 * Class Description:
 *
 *
 * @author 86755221
 * @version 1 Date: 2018-1-19
 *
 */

public class TxnHistEnquiryResult implements Serializable {

    private static final long serialVersionUID = -6057466449803559577L;
    private String txnId;
    private String trxAmt;
    private String trxCurrency;
    private String discount;
    private String originalAmt;
    private String merchantName;
    private String qrcVoucherNumber;
    private String retrievalRefNum;
    private String acquirerIIN;
    private String forwardingIIN;
    private String sysTraceAuditNumber;
    private String transDate;
    private String paymentStatus;
    private String rejectionReason;

    /**
     *
     */
    public TxnHistEnquiryResult() {
        super();
    }

    /**
     * @param txnId
     * @param trxAmt
     * @param trxCurrency
     * @param discount
     * @param originalAmt
     * @param merchantName
     * @param qrcVoucherNumber
     * @param retrievalRefNum
     * @param acquirerIIN
     * @param forwardingIIN
     * @param sysTraceAuditNumber
     * @param transDate
     * @param paymentStatus
     * @param rejectionReason
     */
    public TxnHistEnquiryResult(String txnId, String trxAmt, String trxCurrency, String discount, String originalAmt, String merchantName, String qrcVoucherNumber, String retrievalRefNum, String acquirerIIN, String forwardingIIN,
                                String sysTraceAuditNumber, String transDate, String paymentStatus, String rejectionReason) {
        super();
        this.txnId = txnId;
        this.trxAmt = trxAmt;
        this.trxCurrency = trxCurrency;
        this.discount = discount;
        this.originalAmt = originalAmt;
        this.merchantName = merchantName;
        this.qrcVoucherNumber = qrcVoucherNumber;
        this.retrievalRefNum = retrievalRefNum;
        this.acquirerIIN = acquirerIIN;
        this.forwardingIIN = forwardingIIN;
        this.sysTraceAuditNumber = sysTraceAuditNumber;
        this.transDate = transDate;
        this.paymentStatus = paymentStatus;
        this.rejectionReason = rejectionReason;
    }

    /**
     * @return the txnId
     */
    public String getTxnId() {
        return txnId;
    }

    /**
     * @param txnId
     *            the txnId to set
     */
    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    /**
     * @return the trxAmt
     */
    public String getTrxAmt() {
        return trxAmt;
    }

    /**
     * @param trxAmt
     *            the trxAmt to set
     */
    public void setTrxAmt(String trxAmt) {
        this.trxAmt = trxAmt;
    }

    /**
     * @return the trxCurrency
     */
    public String getTrxCurrency() {
        return trxCurrency;
    }

    /**
     * @param trxCurrency
     *            the trxCurrency to set
     */
    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    /**
     * @return the discount
     */
    public String getDiscount() {
        return discount;
    }

    /**
     * @param discount
     *            the discount to set
     */
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    /**
     * @return the originalAmt
     */
    public String getOriginalAmt() {
        return originalAmt;
    }

    /**
     * @param originalAmt
     *            the originalAmt to set
     */
    public void setOriginalAmt(String originalAmt) {
        this.originalAmt = originalAmt;
    }

    /**
     * @return the merchantName
     */
    public String getMerchantName() {
        return merchantName;
    }

    /**
     * @param merchantName
     *            the merchantName to set
     */
    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    /**
     * @return the qrcVoucherNumber
     */
    public String getQrcVoucherNumber() {
        return qrcVoucherNumber;
    }

    /**
     * @param qrcVoucherNumber
     *            the qrcVoucherNumber to set
     */
    public void setQrcVoucherNumber(String qrcVoucherNumber) {
        this.qrcVoucherNumber = qrcVoucherNumber;
    }

    /**
     * @return the retrievalRefNum
     */
    public String getRetrievalRefNum() {
        return retrievalRefNum;
    }

    /**
     * @param retrievalRefNum
     *            the retrievalRefNum to set
     */
    public void setRetrievalRefNum(String retrievalRefNum) {
        this.retrievalRefNum = retrievalRefNum;
    }

    /**
     * @return the acquirerIIN
     */
    public String getAcquirerIIN() {
        return acquirerIIN;
    }

    /**
     * @param acquirerIIN
     *            the acquirerIIN to set
     */
    public void setAcquirerIIN(String acquirerIIN) {
        this.acquirerIIN = acquirerIIN;
    }

    /**
     * @return the forwardingIIN
     */
    public String getForwardingIIN() {
        return forwardingIIN;
    }

    /**
     * @param forwardingIIN
     *            the forwardingIIN to set
     */
    public void setForwardingIIN(String forwardingIIN) {
        this.forwardingIIN = forwardingIIN;
    }

    /**
     * @return the sysTraceAuditNumber
     */
    public String getSysTraceAuditNumber() {
        return sysTraceAuditNumber;
    }

    /**
     * @param sysTraceAuditNumber
     *            the sysTraceAuditNumber to set
     */
    public void setSysTraceAuditNumber(String sysTraceAuditNumber) {
        sysTraceAuditNumber = sysTraceAuditNumber;
    }

    /**
     * @return the transDate
     */
    public String getTransDate() {
        return transDate;
    }

    /**
     * @param transDate
     *            the transDate to set
     */
    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    /**
     * @return the paymentStatus
     */
    public String getPaymentStatus() {
        return paymentStatus;
    }

    /**
     * @param paymentStatus
     *            the paymentStatus to set
     */
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    /**
     * @return the rejectionReason
     */
    public String getRejectionReason() {
        return rejectionReason;
    }

    /**
     * @param rejectionReason
     *            the rejectionReason to set
     */
    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

}
