package cn.swiftpass.wallet.intl.module.virtualcard.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardVeryOtpEntiy;

/**
 * 发送验证码
 */
public class BindVitualCardSendOtpContract {

    public interface View extends IView {

        void bindVitualCardSenOtpSuccess(VirtualCardSendOtpEntity response, boolean isTryAgain);

        void bindVitualCardSenOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void bindVitualCardVerifyOtpSuccess(VirtualCardVeryOtpEntiy response);

        void bindVitualCardVerifyOtpError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<BindVitualCardSendOtpContract.View> {

        void bindVitualCardSenOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String action, boolean isTryAgain);

        void bindVitualCardVerifyOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String action, String verifyCode, String cardId);
    }

}
