package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.api.protocol.E2eePreProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.manage.AppTestManager;


public class SwForgetPwdVerifyOtpProtocol extends BaseProtocol {

    private String mPin;
    private String mSid;
    String mVerifyCode;
    String mPasscode;

    public SwForgetPwdVerifyOtpProtocol(String verifyCode, String passcode, NetWorkCallbackListener dataCallback) {
        mVerifyCode = verifyCode;
        mPasscode = passcode;
        this.mPin = mPasscode;
        mUrl = "api/smartForgot/verifyOtp";
        mDataCallback = dataCallback;
    }

    @Override
    public void packData() {
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
        mBodyParams.put(RequestParams.PASSCODE, mPin);
    }

    @Override
    public Void execute() {
        if (AppTestManager.getInstance().isE2ee()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    mPin = E2eeUtils.getLoginOrSetPin(mSid, mPasscode, pubKey, random);
                    if (TextUtils.isEmpty(mPin)) {
                        mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                    } else {
                        SwForgetPwdVerifyOtpProtocol.super.execute();
                    }
                }
            }).execute();
        } else {
            super.execute();
        }
        return null;
    }

}
