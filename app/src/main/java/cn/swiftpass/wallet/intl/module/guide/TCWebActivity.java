package cn.swiftpass.wallet.intl.module.guide;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.BocLaunchSDK;
import cn.swiftpass.wallet.intl.utils.LogUtils;

/**
 * Created by 2018.9.27.
 * 应用安装，更新时TC
 */

public class TCWebActivity extends BaseCompatActivity {
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.tv_accept)
    TextView mAcceptTV;

    @BindView(R.id.tv_refresh)
    TextView mRefreshtTV;

    @BindView(R.id.tv_cancel)
    TextView mCancelTV;

    @BindView(R.id.ll_load_failed)
    View mRefreshLayout;

    @BindView(R.id.ll_load_webview)
    View mWebLayout;
    @BindView(R.id.tv_error)
    TextView mErrorTV;


    String mUrl;
    public static final String TC_EN = "https://www.bochk.com/dam/document/mbs/bocpayftce.html";
    public static final String TC_SC = "https://www.bochk.com/dam/document/mbs/bocpayftcs.html";
    public static final String TC_TC = "https://www.bochk.com/dam/document/mbs/bocpayftct.html";

    public boolean mLoadFailed = false;
    private boolean isNeedAutoLogin;
//    private String appLinkUrl;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
//        String lan = SpUtils.getInstance(mContext).getAppLanguage();
//        switchLanguage(lan);
        setToolBarTitle(R.string.service_fact_statement);
        //hideBackIcon();
        mUrl = getString(R.string.force_tc_url);
        //白名单功能
//        if (AndroidUtils.isZHLanguage(lan)) {
//            mUrl = Constants.BOCPAYTCT_WL_CN;
//        } else if (AndroidUtils.isHKLanguage(lan)) {
//            mUrl = Constants.BOCPAYTCT_WL_TC;
//        } else {
//            mUrl = Constants.BOCPAYTCT_WL_EN;
//        }
        if (getIntent() != null) {
            isNeedAutoLogin = getIntent().getBooleanExtra(MainHomeActivity.IS_AUTO_LOGIN, false);
//            appLinkUrl = getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
        }


        mAcceptTV.setText(R.string.accept);
        mCancelTV.setText(R.string.string_cancel);
        mRefreshtTV.setText(R.string.try_again);
        mErrorTV.setText(R.string.load_tc_fail);

        hideBackIcon();

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();
                LogUtils.i("URL", "URL:" + url);

                if (url.toLowerCase().endsWith(".pdf")) {
                    Uri uri = Uri.parse(url);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                    return true;
                } else {
                    return super.shouldOverrideUrlLoading(view, request);
//                    return false;
                }
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                ApiProtocolImplManager.getInstance().showDialog(TCWebActivity.this);
                if (mWebLayout != null) {
                    mWebLayout.setVisibility(View.INVISIBLE);
                }
                if (mRefreshLayout != null) {
                    mRefreshLayout.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (!mLoadFailed) {
                    super.onPageFinished(view, url);
                    ApiProtocolImplManager.getInstance().dismissDialog();
                    if (mWebLayout != null) {
                        mWebLayout.setVisibility(View.VISIBLE);
                    }
                }
                if (view != null && view.canGoBack()) {
                    showBackIcon();
                } else {
                    hideBackIcon();
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                ApiProtocolImplManager.getInstance().dismissDialog();
                if (mRefreshLayout != null) {
                    mRefreshLayout.setVisibility(View.VISIBLE);
                }
                mLoadFailed = true;
            }
        });

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        webView.loadUrl(mUrl);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            isNeedAutoLogin = intent.getBooleanExtra(MainHomeActivity.IS_AUTO_LOGIN, false);
//            appLinkUrl = intent.getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
//            if (!TextUtils.isEmpty(intent.getStringExtra(AppCallAppLinkEntity.APP_LINK_URL))) {
//                AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
//                appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, intent.getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
//            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_tc_webview;
    }


    @OnClick({R.id.tv_refresh, R.id.tv_cancel, R.id.tv_accept})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_refresh:
                onRefresh();
                break;
            case R.id.tv_cancel:
                onCancel();
                break;
            case R.id.tv_accept:
                onAccept();
                break;
            default:
                break;
        }
    }

    private void onAccept() {
        BocLaunchSDK.saveAppVersion(TCWebActivity.this);
        HashMap<String, Object> param = new HashMap<>();
        param.put(MainHomeActivity.IS_AUTO_LOGIN, isNeedAutoLogin);
//        param.put(AppCallAppLinkEntity.APP_LINK_URL, appLinkUrl);
//        if (null != getIntent() && null != getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS)) {
//            param.put(RED_PACKET_RED_PACKET_PARAMS, getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
//        }
        ActivitySkipUtil.startAnotherActivity(this, GuideNewActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        finish();
    }

    private void onRefresh() {
        mLoadFailed = false;
        webView.loadUrl(mUrl);
    }

    private void onCancel() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}
