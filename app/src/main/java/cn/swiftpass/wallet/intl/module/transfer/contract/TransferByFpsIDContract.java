package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;

/**
 * fpsid转账
 */
public class TransferByFpsIDContract {

    public interface View extends IView {
        void getSmartAccountInfoSuccess(MySmartAccountEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void transferPreCheckSuccess(TransferPreCheckReq req, TransferPreCheckEntity response);

        void transferPreCheckError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);

        void getRegEddaInfoError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<TransferByFpsIDContract.View> {
        void getSmartAccountInfo();

        void transferPreCheck(TransferPreCheckReq req);

        void getRegEddaInfo();
    }

}
