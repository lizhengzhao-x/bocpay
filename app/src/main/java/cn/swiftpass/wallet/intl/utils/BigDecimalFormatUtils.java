package cn.swiftpass.wallet.intl.utils;

import android.text.TextUtils;

import java.math.BigDecimal;

/**
 * 小数点处理工具类
 */

public class BigDecimalFormatUtils {


    /**
     * LogUtils.i("TEST", BigDecimalFormatUtils.forMatWithDigs("3.152", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.1", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.15", 2));
     * LogUtils.i("TEST",BigDecimalFormatUtils.forMatWithDigs("3.159", 2));
     *
     * @param str
     * @param count 小数点位数
     * @return
     */
    public static String forMatWithDigs(String str, int count) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        BigDecimal b = new BigDecimal(str);
        return forMatWithDigs(b, count);
    }

    public static String forMatWithDigsWithNoPoint(String str, int count) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        BigDecimal b = new BigDecimal(str);
        String value = forMatWithDigs(b, count);
        return value.replace(".00", "");
    }

    public static String forMatWithDigs(BigDecimal str, int count) {
        return str.setScale(count, BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String forMatWithDigs(double str, int count) {
        return forMatWithDigs(str + "", count);
    }

}
