package cn.swiftpass.wallet.intl.module.transfer.view;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.promeg.pinyinhelper.Pinyin;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.WalletConstants;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.module.transfer.utils.TransferUtils;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.TransferItemTitleAdapter;
import cn.swiftpass.wallet.intl.module.transfer.view.adapter.TransferSearchItemAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactLocalCacheUtils;
import cn.swiftpass.wallet.intl.utils.ContactUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.PermissionInstance;
import cn.swiftpass.wallet.intl.widget.ClearEditText;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

import static cn.swiftpass.wallet.intl.entity.ContractListEntity.RecentlyBean.UI_ADDRESSBOOK;
import static cn.swiftpass.wallet.intl.entity.ContractListEntity.RecentlyBean.UI_ADDRESS_FREQUENT;
import static cn.swiftpass.wallet.intl.entity.ContractListEntity.RecentlyBean.UI_FREQUENT;
import static cn.swiftpass.wallet.intl.entity.ContractListEntity.RecentlyBean.UI_RECENTLY;

/**
 * Created by ZhangXinchao on 2019/10/31.
 * 转账搜索界面
 */
public class TransferItemSearchActivity extends BaseCompatActivity {
    @BindView(R.id.id_transfer_search_et)
    ClearEditText idTransferSearchEt;
    @BindView(R.id.id_transfer_search_view)
    RelativeLayout idTransferSearchView;
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    @BindView(R.id.id_nodata_image)
    ImageView idNodataImage;
    @BindView(R.id.id_empty_activity)
    TextView idEmptyTv;
    @BindView(R.id.id_empty_view)
    View idEmptyView;
    @BindView(R.id.id_search_cancel)
    TextView idSearchCancel;
    @BindView(R.id.id_total_bac_view)
    View bacView;
    private static final String TYPE_TITLE = "TYPE_TITLE";
    private static final String TYPE_TITLE_ADDRESS = "TYPE_TITLE_ADDRESS";
    private static final String TYPE_TITLE_FREQUENT = "UI_FREQUENT";
    private static final String TYPE_CONTENT = "TYPE_CONTENT";
    private static final String TYPE_TITLE_OTHER = "TYPE_TITLE_OTHER";
    private static final String TAG = "TransferItemSearchActivity";

    /**
     * 所有的数据
     */
    public static List<ContractListEntity.RecentlyBean> recentlyBeanList;
    /**
     * 绑定adapter list
     */
    private List<ContractListEntity.RecentlyBean> recentlyOriginalList;
    /**
     * 筛选出来的总集合
     */
    private List<ContractListEntity.RecentlyBean> filterRecentlyAlllBeanList;
    /**
     * 最近转账联系人
     */
    private List<ContractListEntity.RecentlyBean> filterRecentlyBeanList;
    /**
     * 常用联系人筛选列表
     */
    private List<ContractListEntity.RecentlyBean> filterFrequentBeanList;
    /**
     * 本地通讯录列表
     */
    private List<ContractListEntity.RecentlyBean> filterAddressBeanList;

    /**
     * 三种列表总和
     */
    private List<ContractListEntity.RecentlyBean> filterRecentlyTmpBeanList;

    private boolean isLiShi;

    //是否第一次选择拒绝通讯录权限且不再询问权限
    public boolean firstRejectContacts = true;

    private CustomDialog mDealDialog;
    private String mLastFilterStr;
    private String lastFilterStr;

    public static void setRecentlyInfo(List<ContractListEntity.RecentlyBean> recentlyBeansIn) {
        recentlyBeanList = recentlyBeansIn;
    }

    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.TR1_7_7));
        EventBus.getDefault().register(this);

        isLiShi = getIntent().getBooleanExtra(Constants.TRANSFER_IS_LISHI, false);
        if (isLiShi) {
            //背景图不一样
            setToolBarTitle(getString(R.string.RP1_2_6));
            bacView.setBackgroundResource(R.drawable.bg_red_pocket);
        }
        initRecycleView();
        initEditTextListener();
        getFocusView();

        //检查是否有通讯录权限
        if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferItemSearchActivity.this, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                //拒绝并勾选不再询问
                firstRejectContacts = false;
            } else {
                firstRejectContacts = true;
            }
        }
    }


    private void getFocusView() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                idTransferSearchEt.setFocusable(true);
                idTransferSearchEt.requestFocus();
                AndroidUtils.showKeyboardView(idTransferSearchEt);
            }
        }, 200);
    }

    private void initRecycleView() {
        filterRecentlyAlllBeanList = new ArrayList<>();
        recentlyOriginalList = new ArrayList<>();
        filterRecentlyBeanList = new ArrayList<>();
        filterFrequentBeanList = new ArrayList<>();
        filterAddressBeanList = new ArrayList<>();
        filterRecentlyTmpBeanList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(recentlyOriginalList));
        idEmptyTv.setText(getString(R.string.TR1_2b_1));
        ((IAdapter<ContractListEntity.RecentlyBean>) idRecyclerview.getAdapter()).setData(recentlyOriginalList);
        idRecyclerview.getAdapter().notifyDataSetChanged();


        idSearchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    public void showLackOfPermissionDialog() {
        if (mDealDialog != null && mDealDialog.isShowing()) {
            return;
        }
        CustomDialog.Builder builder = new CustomDialog.Builder(TransferItemSearchActivity.this);
        builder.setTitle(getString(R.string.TR1_4b_2));
        builder.setMessage(getString(R.string.TR1_4b_3));

        builder.setPositiveButton(getString(R.string.dialog_turn_on), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                AndroidUtils.startAppSetting(TransferItemSearchActivity.this);
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);


        if (!this.isFinishing()) {
            mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    private void initEditTextListener() {
        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (source.toString().contentEquals("\n")) {
                    return "";
                } else {
                    return null;
                }
            }
        };
        InputFilter[] filters = {new InputFilter.LengthFilter(50), filter};
        idTransferSearchEt.setFilters(filters);
        idTransferSearchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                lastFilterStr = s.toString();
                searchFilterWithAllList(lastFilterStr);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSON_REQUESTCODE) {
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    //判断是否点击了 不再询问
                    if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferItemSearchActivity.this, permissions[i])) {
                        if (isGranted(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                            if (mDealDialog != null) {
                                mDealDialog.dismiss();
                            }
                        } else {
                            if (!firstRejectContacts) {
                                showLackOfPermissionDialog();
                            } else {
                                //如果是第一次选择不再询问并拒绝  则不弹出自定义权限选择框
                                firstRejectContacts = false;
                            }
                        }
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * 根据用户关键词输入 筛选 最近联系人 通讯录 常用联系人 分三组ui展示
     */
    private void searchFilterWithAllList(String filterStr) {
        recentlyOriginalList.clear();
        //去除空格的匹配规则
        filterStr = filterStr.replace(" ", "").replace("*", "").replace(",", "");
        if (TextUtils.isEmpty(filterStr)) {
            if (idEmptyView != null) {
                idEmptyView.setVisibility(View.GONE);
            }
            recentlyOriginalList.clear();
            idRecyclerview.setLoadSuccess(false);
            idRecyclerview.getAdapter().notifyDataSetChanged();
            return;
        }
        mLastFilterStr = filterStr;
        filterRecentlyAlllBeanList.clear();
        filterRecentlyBeanList.clear();
        filterFrequentBeanList.clear();
        filterAddressBeanList.clear();
        filterRecentlyTmpBeanList.clear();
        if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
            for (int i = 0; i < recentlyBeanList.size(); i++) {
                ContractListEntity.RecentlyBean telListBean = recentlyBeanList.get(i);
                String name = telListBean.getDisCustNameMapping() + telListBean.getPhoneNoMapping() + telListBean.getdisPhoneNoMapping();
//                LogUtils.i(TAG, "name: " + name);
                if (telListBean.isEmail()) {
                    name = telListBean.getDisCustName() + telListBean.getEmail();
                }
                String firstChat = Pinyin.toPinyin(name, "");
                if (name.indexOf(filterStr.toString()) != -1 ||
                        firstChat.startsWith(filterStr.toString()) ||
                        firstChat.toLowerCase().startsWith(filterStr.toString()) ||
                        firstChat.toUpperCase().startsWith(filterStr.toString()) ||
                        name.contains(filterStr.toString()) ||
                        name.contains(filterStr.toString().toLowerCase()) ||
                        name.contains(filterStr.toString().toUpperCase())) {
                    filterRecentlyAlllBeanList.add(telListBean);
                }
            }

            //将搜索的列表分组展示
            for (int i = 0; i < filterRecentlyAlllBeanList.size(); i++) {
                ContractListEntity.RecentlyBean recentlyBean = filterRecentlyAlllBeanList.get(i);
                if (recentlyBean.getUiShowType().equals(UI_RECENTLY)) {
                    filterRecentlyBeanList.add(recentlyBean);
                } else if (recentlyBean.getUiShowType().equals(UI_FREQUENT)) {
                    //常用联系人
                    filterFrequentBeanList.add(recentlyBean);
                } else if (recentlyBean.getUiShowType().equals(UI_ADDRESSBOOK)) {
                    filterAddressBeanList.add(recentlyBean);
                } else if (recentlyBean.getUiShowType().equals(UI_ADDRESS_FREQUENT)) {
                    filterFrequentBeanList.add(0, recentlyBean);
                    filterAddressBeanList.add(recentlyBean);
                }
            }

            if (filterRecentlyBeanList.size() > 0) {
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(TYPE_TITLE);
                recentlyBean.setUiTitle(getString(R.string.TR1_2_2));
                filterRecentlyTmpBeanList.add(recentlyBean);
            }

            filterRecentlyTmpBeanList.addAll(filterRecentlyBeanList);

            if (filterFrequentBeanList.size() > 0) {
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(TYPE_TITLE_FREQUENT);
                recentlyBean.setUiTitle(getString(R.string.TR1_2_3));
                filterRecentlyTmpBeanList.add(recentlyBean);
            }

            filterRecentlyTmpBeanList.addAll(filterFrequentBeanList);
            addItemTransferToOther(filterStr);

        } else {
            //所有数据都为空
            addItemTransferToOther(filterStr);
        }
        recentlyOriginalList.clear();
        recentlyOriginalList.addAll(filterRecentlyTmpBeanList);

        if (!TextUtils.isEmpty(filterStr)) {
            idRecyclerview.setLoadSuccess(true);
        }
        idRecyclerview.getAdapter().notifyDataSetChanged();
        if (idRecyclerview.getAdapter().getItemCount() == 0) {
            idEmptyView.setVisibility(View.VISIBLE);
        } else if (idRecyclerview.getAdapter().getItemCount() > 1) {
            idEmptyView.setVisibility(View.GONE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(TransferEventEntity event) {
        if (event.getEventType() == TransferEventEntity.EVENT_TRANSFER_FAILED) {
            finish();
        }
    }


    private void addItemTransferToOther(String filterStr) {
        if (filterAddressBeanList.size() > 0) {
            if (idEmptyView != null) {
                idEmptyView.setVisibility(View.GONE);
            }
            ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
            recentlyBean.setUiShowType(TYPE_TITLE_ADDRESS);
            recentlyBean.setUiTitle(getString(R.string.TR1_2_4));
            filterRecentlyTmpBeanList.add(recentlyBean);
        } else {
            if (!isGranted(Manifest.permission.READ_CONTACTS)) {
                //需要 如果没有获取通讯录权限 需要添加词条提示
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(TYPE_TITLE_ADDRESS);
                recentlyBean.setUiTitle(getString(R.string.TR1_2_4));
                recentlyBean.setUiContent(getString(R.string.TR1_4b_6));
                filterRecentlyTmpBeanList.add(recentlyBean);
                if (idEmptyView != null) {
                    idEmptyView.setVisibility(View.VISIBLE);
                }
            }
        }
        filterRecentlyTmpBeanList.addAll(filterAddressBeanList);

        //if (filterRecentlyTmpBeanList.size() == 0 && (AndroidUtils.isNumeric(filterStr) || AndroidUtils.isEmail(filterStr) || filterStr.startsWith("+"))) {
        if ((AndroidUtils.isNumeric(filterStr) || AndroidUtils.isEmail(filterStr) || filterStr.startsWith("+"))) {
            //如果完全没有匹配到 手动添加一个到列表中去
            ContractListEntity.RecentlyBean recentlyBeanTtile = new ContractListEntity.RecentlyBean();
            recentlyBeanTtile.setUiShowType(TYPE_TITLE_OTHER);
            if (isLiShi) {
                recentlyBeanTtile.setUiTitle(getString(R.string.TR1_6_1a));
            } else {
                recentlyBeanTtile.setUiTitle(getString(R.string.TR1_6_1));
            }

            filterRecentlyTmpBeanList.add(recentlyBeanTtile);
            ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
            recentlyBean.setPhoneNo(filterStr);
            recentlyBean.setDisPhoneNo(filterStr);
            recentlyBean.setEmail(filterStr);
            recentlyBean.setDisEmail(filterStr);
            recentlyBean.setEmailType(AndroidUtils.isEmail(filterStr) ? "1" : null);
            filterRecentlyTmpBeanList.add(recentlyBean);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_search_view;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    /**
     * 区分搜索列表 显示的是标题 还是内容 通过 uiType 来处理
     *
     * @param data
     * @return
     */
    private CommonRcvAdapter<ContractListEntity.RecentlyBean> getAdapter(final List<ContractListEntity.RecentlyBean> data) {
        return new CommonRcvAdapter<ContractListEntity.RecentlyBean>(data) {

            @Override
            public Object getItemType(ContractListEntity.RecentlyBean demoModel) {
                return demoModel.getUiShowType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                if (type != null && (type.equals(TYPE_TITLE) || type.equals(TYPE_TITLE_ADDRESS) || type.equals(TYPE_TITLE_FREQUENT) || type.equals(TYPE_TITLE_OTHER))) {
                    TransferItemTitleAdapter transferItemTitleAdapter = new TransferItemTitleAdapter(TransferItemSearchActivity.this, new TransferItemTitleAdapter.OnItemContactClickCallback() {
                        @Override
                        public void onItemClick() {
                            super.onItemClick();
                            if (AndroidUtils.needRequestApplyPermissien()) {
                                //检查是否有通讯录权限
                                if (!isGranted_(WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                                    if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferItemSearchActivity.this, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                                        //拒绝并勾选不再询问
                                        firstRejectContacts = false;
                                    } else {
                                        firstRejectContacts = true;
                                    }
                                    checkPermissionEvent();
                                } else {
                                    updateUIAfterGetPremissions();
                                }
                            }
                        }
                    });
                    return transferItemTitleAdapter;
                } else {
                    TransferSearchItemAdapter searchItemAdapter = new TransferSearchItemAdapter(TransferItemSearchActivity.this, new TransferSearchItemAdapter.OnItemContactClickCallback() {
                        @Override
                        public void onItemClick(int position) {
                            super.onItemClick(position);
                            String phone = recentlyOriginalList.get(position).getPhoneNo();
                            String name = recentlyOriginalList.get(position).getDisCustName();
                            boolean isHasChanged = recentlyOriginalList.get(position).isHasChanged();
                            boolean isEmail = recentlyOriginalList.get(position).isEmail();
                            if (isEmail) {
                                phone = recentlyOriginalList.get(position).getEmail();
                            }
                            startAnotherActivity(getActivity(), !isLiShi, phone, name, isHasChanged ? name : null, isEmail);
                        }

                        @Override
                        public void onItemCollectClick(int position) {
                            super.onItemCollectClick(position);
                            if (!TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_RECENTLY)) {
                                if (!recentlyOriginalList.get(position).isCollect()) {
                                    ContactLocalCacheUtils.addContractToLocalCacheWithUserId(TransferItemSearchActivity.this, recentlyOriginalList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                                    addToRecentlyList(recentlyOriginalList.get(position));
                                } else {
                                    //取消收藏 可能是常用联系人中取消 也可以是通讯录中取消
                                    ContactLocalCacheUtils.removeContractToLocalCacheWithUserId(TransferItemSearchActivity.this, recentlyOriginalList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                                    if (TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_FREQUENT)) {
                                        removeItemWithPostion(position, false);
                                    } else if (TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_ADDRESSBOOK)) {
                                        removeItemWithPostion(position, true);
                                    } else if (TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_ADDRESS_FREQUENT)) {
                                        ContactLocalCacheUtils.removeContractToLocalCacheWithUserId(TransferItemSearchActivity.this, recentlyOriginalList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
                                        specialRemoveItemWithPosition(position);
                                    }
                                }
                                idRecyclerview.getAdapter().notifyDataSetChanged();
                            }
                        }
                    });
                    return searchItemAdapter;

                }
            }

            //申请系统权限
            private void checkPermissionEvent() {
                PermissionInstance.getInstance().getPermission(TransferItemSearchActivity.this, new PermissionInstance.PermissionCallback() {
                    @Override
                    public void acceptPermission() {
                        updateUIAfterGetPremissions();
                    }

                    @Override
                    public void rejectPermission() {
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(TransferItemSearchActivity.this, WalletConstants.PERMISSIONS_READ_CONTACTS[0])) {
                            if (!firstRejectContacts) {
                                showLackOfPermissionDialog();
                            } else {
                                firstRejectContacts = false;
                            }
                        }
                    }
                }, Manifest.permission.READ_CONTACTS);
            }
        };
    }


    private void removeItemWithPostion(int position, boolean isFromAddress) {
        ContactLocalCacheUtils.removeContractToLocalCacheWithUserId(TransferItemSearchActivity.this, recentlyOriginalList.get(position).getContactID(), HttpCoreKeyManager.getInstance().getUserId());
        if (TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_ADDRESSBOOK) || TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_FREQUENT) || TextUtils.equals(recentlyOriginalList.get(position).getUiShowType(), UI_ADDRESS_FREQUENT)) {
            //如果是常用联系人中点击取消收藏 该栏位要在列表中删除 同时通讯录中已有的状态应该为未收藏
            removeItemWithPosition(position, isFromAddress);
        }
    }

    private void addToRecentlyList(ContractListEntity.RecentlyBean recentlyBean) {
        recentlyBean.setCollect(true);
        recentlyBean.setUiShowType(UI_ADDRESS_FREQUENT);
        if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
            for (int i = 0; i < recentlyBeanList.size(); i++) {
                if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID())) {
                    recentlyBeanList.get(i).setCollect(true);
                    break;
                }
            }
        }
        searchFilterWithAllList(lastFilterStr);
    }


    /**
     * 分组列表中如果是常用联系人 点击收藏 要把该组数据条目删除，同时要把整个列表的数据条目删除
     *
     * @param position
     * @param isFromAddress 是否是通过通讯录取消收藏
     */
    private void removeItemWithPosition(int position, boolean isFromAddress) {
        int deleteIndex = -1;
        //移除掉adpter中的条目
        ContractListEntity.RecentlyBean recentlyBean = recentlyOriginalList.get(position);
        recentlyOriginalList.remove(position);

        //移除只能从 常用联系人中点击取消收藏
        if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
            for (int i = 0; i < recentlyBeanList.size(); i++) {
                //删除常用联系人组 中的条目
                if (isFromAddress) {
                    if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
                            (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_ADDRESSBOOK))) {
                        deleteIndex = i;
                        break;
                    }
                } else {
                    if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
                            (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_FREQUENT))) {
                        deleteIndex = i;
                        break;
                    }
                }

            }
        }
        if (deleteIndex != -1) {
            if (isFromAddress) {
                //不需要删除 只需要将收藏状态更新
                recentlyBeanList.get(deleteIndex).setCollect(false);
                //同时删除掉常用联系人中的条目
                if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
                    for (int i = 0; i < recentlyBeanList.size(); i++) {
                        //删除常用联系人组 中的条目
                        if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
                                (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_FREQUENT))) {
                            deleteIndex = i;
                            break;
                        }
                    }
                }
                recentlyBeanList.remove(deleteIndex);
            } else {
                recentlyBeanList.remove(deleteIndex);
                //将通讯录组中的该条目状态改为未收藏
                if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
                    for (int i = 0; i < recentlyBeanList.size(); i++) {
                        if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
                                (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_ADDRESSBOOK))) {
                            recentlyBeanList.get(i).setCollect(false);
                            break;
                        }
                    }
                }
            }
            searchFilterWithAllList(lastFilterStr);
        }
    }

    private void specialRemoveItemWithPosition(int position) {
        int deleteIndex = -1;
        //移除掉adpter中的条目
        ContractListEntity.RecentlyBean recentlyBean = recentlyOriginalList.get(position);
        recentlyOriginalList.remove(position);

        //移除只能从 常用联系人中点击取消收藏
        if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
            for (int i = 0; i < recentlyBeanList.size(); i++) {
                //删除常用联系人组 中的条目
                if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
                        (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_ADDRESS_FREQUENT))) {
                    deleteIndex = i;
                    break;
                }

            }
        }
        if (deleteIndex != -1) {
            recentlyBeanList.get(deleteIndex).setUiShowType(UI_ADDRESSBOOK);
            recentlyBeanList.get(deleteIndex).setCollect(false);
//            recentlyBeanList.remove(deleteIndex);
//            //将通讯录组中的该条目状态改为未收藏
//            if (recentlyBeanList != null && recentlyBeanList.size() > 0) {
//                for (int i = 0; i < recentlyBeanList.size(); i++) {
//                    if (TextUtils.equals(recentlyBeanList.get(i).getContactID(), recentlyBean.getContactID()) &&
//                            (TextUtils.equals(recentlyBeanList.get(i).getUiShowType(), UI_ADDRESSBOOK))) {
//                        recentlyBeanList.get(i).setCollect(false);
//                        break;
//                    }
//                }
//            }
        }
        searchFilterWithAllList(lastFilterStr);
    }

    private void updateUIAfterGetPremissions() {
        //更新通讯录数据
        final ArrayList<ContactEntity> contactEntities = ContactUtils.getInstance().getAllContacts(TransferItemSearchActivity.this, true);
        if (contactEntities != null) {
            for (int i = 0; i < contactEntities.size(); i++) {
                ContactEntity contactEntity = contactEntities.get(i);
                ContractListEntity.RecentlyBean recentlyBean = new ContractListEntity.RecentlyBean();
                recentlyBean.setUiShowType(UI_ADDRESSBOOK);
                recentlyBean.setDisCustName(contactEntity.getUserName());
                if (contactEntity.isEmail()) {
                    recentlyBean.setDisEmail(contactEntity.getNumber());
                    recentlyBean.setEmail(contactEntity.getNumber());
                } else {
                    recentlyBean.setPhoneNo(contactEntity.getNumber());
                    recentlyBean.setDisPhoneNo(contactEntity.getNumber());
                }
                recentlyBean.setEmailType(contactEntity.isEmail() ? "1" : null);
                recentlyBeanList.add(recentlyBean);
            }
        }
        searchFilterWithAllList(mLastFilterStr);
    }



    private void startAnotherActivity(final Activity activity,
                                      final boolean isTransferNormalType, final String phone, final String name,
                                      final String realName, final boolean isEmail) {
        ApiProtocolImplManager.getInstance().checkSmartAccountNewBind(getActivity(), true, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(final String errorCode, String errorMsg) {
                if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
                    AndroidUtils.showBindSmartAccountAndCancelDialog(getActivity());
                } else {
                    showErrorMsgDialog(getActivity(),errorMsg);
                }
            }

            @Override
            public void onSuccess(ContentEntity response) {
                TransferUtils.transferWithPhoneNumber(activity, isTransferNormalType, phone, name, realName, isEmail);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recentlyBeanList = null;
        EventBus.getDefault().unregister(this);
    }
}
