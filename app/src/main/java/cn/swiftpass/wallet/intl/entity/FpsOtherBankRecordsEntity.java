package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * fps 行外绑定记录
 */

public class FpsOtherBankRecordsEntity extends BaseEntity {


    //如果为空，说明没有登记
    //-- 账户 手机号或者邮箱
    String accountId;
    //-- 账户类型 0：手机号 1：邮箱
    String accountIdType;

    private List<FpsOtherBankRecordBean> data;


    private List<FpsOtherBankRecordBean> bankChina;

    public List<FpsOtherBankRecordBean> getBankChina() {
        return bankChina;
    }

    public void setBankChina(List<FpsOtherBankRecordBean> bankChina) {
        this.bankChina = bankChina;
    }


    public List<FpsOtherBankRecordBean> getData() {
        return data;
    }

    public void setData(List<FpsOtherBankRecordBean> data) {
        this.data = data;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountIdType() {
        return accountIdType;
    }

    public void setAccountIdType(String accountIdType) {
        this.accountIdType = accountIdType;
    }


}
