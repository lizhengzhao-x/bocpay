package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;

public class S2RegisterCustomerInfoProtocol extends BaseProtocol {

    String mRegion;
    String mArea;
    String mStreetName;
    String mBuildingName;
    String mRoom;
    String mEmail;

    String mDailyTxnLimit;

    public S2RegisterCustomerInfoProtocol(String type, RegisterCustomerInfo registerCustomerInfo, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        if (type.equals(RequestParams.BIND_SMART_ACCOUNT)) {
            mUrl = "api/smartBind/initCustomerInfo";
        } else {
            mUrl = "api/smartReg/initCustomerInfo";
        }
        mRegion = registerCustomerInfo.mRegion;
        mArea = registerCustomerInfo.mArea;
        mStreetName = registerCustomerInfo.mStreetName;
        mBuildingName = registerCustomerInfo.mBuildingName;

        mEmail = registerCustomerInfo.mEmail;
        mRoom = registerCustomerInfo.mRoom;
        mDailyTxnLimit = registerCustomerInfo.mCurrentLimit;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.REGION, mRegion);
        mBodyParams.put(RequestParams.AREA, mArea);
        mBodyParams.put(RequestParams.STREETNAME, mStreetName);
        mBodyParams.put(RequestParams.BUILDINGNAME, mBuildingName);
        mBodyParams.put(RequestParams.ROOM, mRoom);
        mBodyParams.put(RequestParams.EMAIL, mEmail);
        mBodyParams.put(RequestParams.DAILYTXNLIMIT, mDailyTxnLimit);
    }

}
