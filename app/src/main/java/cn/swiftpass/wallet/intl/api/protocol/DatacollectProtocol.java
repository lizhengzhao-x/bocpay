package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


/**
 * 埋点报表数据统计
 */
public class DatacollectProtocol extends BaseProtocol {

    String action;

    public DatacollectProtocol(String action, NetWorkCallbackListener dataCallback) {
        mUrl = "api/datacollect/save";
        this.action = action;
        mDataCallback = dataCallback;
    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, action);
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }
}
