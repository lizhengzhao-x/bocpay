package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Intent;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.PagerEventEntity;
import cn.swiftpass.wallet.intl.entity.event.TransferEventEntity;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;


public class TransferFailedActivity extends BaseCompatActivity {
    @BindView(R.id.transfer_check)
    TextView transferCheck;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.TF2101_5_1);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_failed;
    }


    @OnClick(R.id.transfer_check)
    public void onViewClicked() {
        if (ButtonUtils.isFastDoubleClick()) return;
        MyActivityManager.removeAllTaskWithMainActivity();
        EventBus.getDefault().postSticky(new TransferEventEntity(TransferEventEntity.EVENT_TRANSFER_FAILED, ""));
        finish();
    }

    public static void startActivity(Activity activity) {
        activity.startActivity(new Intent(activity, TransferFailedActivity.class));
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }
    //    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_BACK) {
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }
}
