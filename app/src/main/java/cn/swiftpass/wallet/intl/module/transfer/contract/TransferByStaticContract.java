package cn.swiftpass.wallet.intl.module.transfer.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;


public class TransferByStaticContract {

    public interface View extends IView {

        void updateQRCodeSuccess(StaticCodeEntity response);

        void updateQRCodeError(String errorCode, String errorMsg);

        void pullQRCodeSuccess(StaticCodeEntity response);

        void pullQRCodeError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<TransferByStaticContract.View> {

        void pullQRCode();

        void updateQRCode(String account, String remarks);
    }

}
