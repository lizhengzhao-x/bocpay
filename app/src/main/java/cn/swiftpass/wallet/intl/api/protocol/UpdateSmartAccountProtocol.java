package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * s1 s2升级
 */
public class UpdateSmartAccountProtocol extends E2eeProtocol {

    String mAccNo;
    String mLoginType;
    String mAction;
    String mVerifyCode;

    public UpdateSmartAccountProtocol(String accNo, String password, String loginType, String action, String verifyCode, NetWorkCallbackListener dataCallback) {
        mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/loginNetBank";
        mAccNo = accNo;
        mPasscode = password;
        mLoginType = loginType;
        mAction = action;
        mVerifyCode = verifyCode;
    }

    @Override
    public void packData() {
        mBodyParams.put(RequestParams.ACCNO, mAccNo);
        mBodyParams.put(RequestParams.PWD, mPasscode);
        mBodyParams.put(RequestParams.LOGINTYPE, mLoginType);
        mBodyParams.put(RequestParams.ACTION, mAction);
        mBodyParams.put(RequestParams.VERIFY, mVerifyCode);
    }
}
