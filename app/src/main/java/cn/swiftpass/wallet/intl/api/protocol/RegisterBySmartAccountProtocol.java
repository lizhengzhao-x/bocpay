package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.SignUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;

/**
 * @author create date on  on 2018/7/30 13:50
 * 通过我的账户确认注册接口，返回对象为
 */

public class RegisterBySmartAccountProtocol extends E2eeProtocol {
    public static final String TAG = RegisterBySmartAccountProtocol.class.getSimpleName();
    /**
     *支付密码
     */
    //String mPasscode;
    /**
     * 指纹\人脸识别
     */
    String mFioId;
    /**
     * 标识取【V】：V：注册流程  B：绑定流程  F：忘记密码流程
     */
    String mAction;

    public RegisterBySmartAccountProtocol(String passcode, String action, NetWorkCallbackListener dataCallback) {
        this.mAction = action;
        mPasscode = passcode;
        this.mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/smartAccountRegOrBind";
    }

    @Override
    public void packData() {
        mBodyParams.put(RequestParams.PAY_PASS_CODE, mPasscode);
        mBodyParams.put(RequestParams.ACTION, mAction);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        try {
            RegSucBySmartEntity entity = mGson.fromJson(data, mDataCallback.mType);
            if (null != entity && !TextUtils.isEmpty(entity.getServerPubKey())) {
                HttpCoreKeyManager.getInstance().saveServerPublicKey(entity.getServerPubKey());
                return SignUtils.verify(sign, data, entity.getServerPubKey());
            } else {
                return super.verifySign(sign, data);
            }
        } catch (Exception e) {
            return false;
        }
    }

}
