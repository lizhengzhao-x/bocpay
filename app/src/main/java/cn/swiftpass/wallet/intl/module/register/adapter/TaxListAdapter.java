package cn.swiftpass.wallet.intl.module.register.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;
import cn.swiftpass.wallet.intl.entity.TaxInfoStatusEntity;
import cn.swiftpass.wallet.intl.module.register.model.RegisterDetailsData;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class TaxListAdapter extends RecyclerView.Adapter implements TaxInfoQuestionOneHolder.OnChangeQuestionOneListener, TaxInfoQuestionTwoHolder.OnChangeQuestionTwoListener, TaxInfoQuestionBottomHolder.OnBottomListener, TaxInfoNormalHolder.OnChangeOtherTaxListener {
    public static final int DATA_QUESTION_NO_SELECT = 0;
    public static final int DATA_QUESTION_YES = 1;
    public static final int DATA_QUESTION_NO = 2;


    private static final int TYPE_HOLDER_QUESTION_ONE = 101;
    private static final int TYPE_HOLDER_QUESTION_TWO = 102;
    private static final int TYPE_HOLDER_BOTTOM_CHECK = 103;
    private static final int TYPE_HOLDER_OTHER_TAX = 104;
    private final Activity context;
    private S2RegisterInfoEntity registerInfoEntity;
    private ArrayList<TaxInfoStatusEntity> data = new ArrayList<>();
    private boolean hasQuestionTwo;
    private boolean hasOtherTaxInfo;
    private boolean showBottomCheck;
    public int resultSelectQuestionOne;
    public int resultSelectQuestionTwo;
    private boolean selectUsTip;
    private OnTaxDataChangeListener listener;


    public TaxListAdapter(Activity context) {
        this.context = context;
    }

    public void initData(RegisterDetailsData registerDetailsData) {
        data = registerDetailsData.data;
        hasQuestionTwo = registerDetailsData.hasQuestionTwo;
        hasOtherTaxInfo = registerDetailsData.hasOtherTaxInfo;
        showBottomCheck = registerDetailsData.showBottomCheck;
        resultSelectQuestionOne = registerDetailsData.resultSelectQuestionOne;
        resultSelectQuestionTwo = registerDetailsData.resultSelectQuestionTwo;

    }

    public RegisterDetailsData getTaxListUIData() {
        RegisterDetailsData registerDetailsData = new RegisterDetailsData();
        registerDetailsData.data = data;
        registerDetailsData.hasQuestionTwo = hasQuestionTwo;
        registerDetailsData.hasOtherTaxInfo = hasOtherTaxInfo;
        registerDetailsData.showBottomCheck = showBottomCheck;
        registerDetailsData.resultSelectQuestionOne = resultSelectQuestionOne;
        registerDetailsData.resultSelectQuestionTwo = resultSelectQuestionTwo;


        return registerDetailsData;
    }

    public void setRegisterInfoData(S2RegisterInfoEntity registerInfoEntity) {
        this.registerInfoEntity = registerInfoEntity;
    }


    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if (TYPE_HOLDER_QUESTION_ONE == viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reg_tax_ques_one, parent, false);

            return new TaxInfoQuestionOneHolder(context, view);

        } else if (TYPE_HOLDER_QUESTION_TWO == viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.item_reg_tax_ques_two, parent, false);

            return new TaxInfoQuestionTwoHolder(view);

        } else if (TYPE_HOLDER_BOTTOM_CHECK == viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reg_tax_bottom_check, parent, false);

            return new TaxInfoQuestionBottomHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_reg_tax_info, parent, false);

            return new TaxInfoNormalHolder(context, view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof TaxInfoQuestionOneHolder) {
            TaxInfoQuestionOneHolder infoHolder = (TaxInfoQuestionOneHolder) holder;
            infoHolder.setData(registerInfoEntity, resultSelectQuestionOne);
            infoHolder.setChangeQuestionOneListener(this);
        } else if (holder instanceof TaxInfoQuestionTwoHolder) {
            TaxInfoQuestionTwoHolder infoHolder = (TaxInfoQuestionTwoHolder) holder;
            infoHolder.setData(resultSelectQuestionTwo);
            infoHolder.setChangeQuestionTwoListener(this);
        } else if (holder instanceof TaxInfoQuestionBottomHolder) {
            TaxInfoQuestionBottomHolder infoHolder = (TaxInfoQuestionBottomHolder) holder;
            infoHolder.setData(resultSelectQuestionTwo);
            infoHolder.setBottomListener(this);
        } else if (holder instanceof TaxInfoNormalHolder) {
            TaxInfoNormalHolder infoHolder = (TaxInfoNormalHolder) holder;
            infoHolder.setChangeOtherTaxInfoListener(this);
            infoHolder.setChangeTaxDataListener(listener);
            //除去第一个问题和第二个问题
            TaxInfoStatusEntity taxInfoStatusEntity = data.get(position - 2);
            if (taxInfoStatusEntity != null) {
                infoHolder.setData(taxInfoStatusEntity, position - 2);
            }
        }


    }

    @Override
    public int getItemCount() {
        //默认必定会有问题1
        int itemCount = 1;
        //问题一选择后会有问题二，则加一
        if (hasQuestionTwo) {
            itemCount++;
        }
        //问题二选择后，如果选择是，会有其他tax信息，则加上其他data
        if (hasOtherTaxInfo) {
            itemCount = itemCount + data.size();
        }
        //问题二选择后，则显示底部check提示
        if (showBottomCheck) {
            itemCount++;
        }
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
        //第一条为问题一
        if (position == 0) {
            return TYPE_HOLDER_QUESTION_ONE;
        }
        //第二条为问题二
        if (position == 1) {
            return TYPE_HOLDER_QUESTION_TWO;
        }
        //如果需要展示底部，则最后一条为底部信息
        if (showBottomCheck && position == getItemCount() - 1) {
            return TYPE_HOLDER_BOTTOM_CHECK;
        }
        //除了以上的情况外，都是税务信息
        return TYPE_HOLDER_OTHER_TAX;
    }

    @Override
    public void OnChangeQuestionOne(int select) {
        //只要触发了选择，无论是选择什么都显示问题二
        resultSelectQuestionOne = select;
        hasQuestionTwo = true;
        notifyDataSetChanged();
        updateNextBtn();
    }


    @Override
    public void OnChangeQuestionTwo(int select) {
        //切换时，清除数据，清除数据必须在第一行
        data.clear();
        resultSelectQuestionTwo = select;
        //如果选择是，则会有其他税务资料填写
        hasOtherTaxInfo = (select == TaxListAdapter.DATA_QUESTION_YES);
        //如果选择是，默认添加一条data数据
        if (hasOtherTaxInfo) {
            data.add(new TaxInfoStatusEntity());
        } else {
            if (listener != null) {
                listener.resetDataResidentList();
            }
        }
        //问题二选择后，无论选择什么，都会显示底部check提示
        showBottomCheck = true;
        notifyDataSetChanged();
        updateNextBtn();
    }

    @Override
    public void addTaxInfo() {
        if (data.size() > 24) {
            showAddErrorMsg();
        } else {
            data.add(new TaxInfoStatusEntity());
            notifyDataSetChanged();
            updateNextBtn();
        }

    }

    @Override
    public void OnSelectUSTip(boolean select) {
        selectUsTip = select;
        updateNextBtn();
    }


    @Override
    public void finishTaxInfo(int index, boolean finish) {
        if (index < data.size()) {
            TaxInfoStatusEntity infoStatusEntity = data.get(index);
            if (infoStatusEntity != null) {
                infoStatusEntity.setFinishInfo(finish);
            }
            updateNextBtn();
        }

    }

    @Override
    public void deleteTax(int index, TaxInfoStatusEntity taxInfoStatusEntity) {
        if (data != null && index < data.size()) {
            if (data.size() == 1) {
                showDeleteErrorMsg();
            } else {
                data.remove(index);
                if (listener != null) {
                    listener.deleteTaxInfo(index, taxInfoStatusEntity);
                }
                notifyDataSetChanged();
                updateNextBtn();
            }
        }


    }

    @Override
    public void showTaxInfoTip() {
        showTaxInfoTipMsg();
    }


    /**
     * 更新按钮状态
     */
    private void updateNextBtn() {
        //如果未选中条款则按钮为灰色
        if (!selectUsTip) {
            changeBtnStatus(false);
            return;
        }
        //如果有存在未完成的内容则跳出循环体
        if (data != null && data.size() > 0) {
            for (TaxInfoStatusEntity infoStatusEntity : data) {
                if (!infoStatusEntity.isFinishInfo()) {
                    changeBtnStatus(false);
                    return;
                }
            }
        }
        //如果上面2个都没有触发退出，则可以点击下一步
        changeBtnStatus(true);

    }

    private void changeBtnStatus(boolean status) {
        if (listener != null) {
            listener.OnChangeNextBtn(status);
        }
    }

    public void setChangeTaxDataListener(OnTaxDataChangeListener listener) {
        this.listener = listener;
    }


    public ArrayList<TaxInfoStatusEntity> getData() {
        return data;
    }

    public void refreshCheckItem(int index) {
        TaxInfoStatusEntity taxInfoStatusEntity = data.get(index);
        if (taxInfoStatusEntity != null) {
            taxInfoStatusEntity.setFinishInfo(false);
            //判断地区是否选择
            if (taxInfoStatusEntity.getTaxResidence() != null && !TextUtils.isEmpty(taxInfoStatusEntity.getTaxResidence().getTaxResidentRegionName())) {
                //判断如果选中税号，是否已经填写
                if (taxInfoStatusEntity.getSelectType() == TaxInfoStatusEntity.TYPE_SELECT_TAX_NUM) {
                    if (!TextUtils.isEmpty(taxInfoStatusEntity.getTaxNumber())) {
                        taxInfoStatusEntity.setFinishInfo(true);
                    }
                } else if (taxInfoStatusEntity.getSelectType() == TaxInfoStatusEntity.TYPE_SELECT_TAX_REASON) {
                    //判断如果选中原因，原因是否已经选择
                    if (taxInfoStatusEntity.getReason() != null && !TextUtils.isEmpty(taxInfoStatusEntity.getReason().getNoResidentReason())) {
                        taxInfoStatusEntity.setFinishInfo(true);
                    }
                }
            }
            updateNextBtn();
        }

    }

    private void showDeleteErrorMsg() {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(context);
        builder.setMessage(context.getString(R.string.PA2109_1_22));
        builder.setPositiveButton(context.getString(R.string.PA2109_1_20), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        CustomMsgDialog mDealDialog = builder.create();
        mDealDialog.show();
    }

    private void showAddErrorMsg() {
        CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(context);
        builder.setMessage(context.getString(R.string.PA2109_1_16));
        builder.setPositiveButton(context.getString(R.string.PA2109_1_20), new android.content.DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        CustomMsgDialog mDealDialog = builder.create();
        mDealDialog.show();
    }

    private void showTaxInfoTipMsg() {
        String lan = SpUtils.getInstance(context).getAppLanguage();
        HashMap<String, Object> mHashMaps = new HashMap<>();
        if (AndroidUtils.isZHLanguage(lan)) {
            mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_CN);
        } else if (AndroidUtils.isHKLanguage(lan)) {
            mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_HK);
        } else {
            mHashMaps.put(Constants.DETAIL_URL, Constants.INFO_US);
        }
        mHashMaps.put(Constants.DETAIL_TITLE, context.getString(R.string.P3_A1_16_3));
        ActivitySkipUtil.startAnotherActivity(context, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

}
