package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class TransferBillEntity extends BaseEntity {


    private List<TransferInfosBean> transferInfos;

    public List<TransferInfosBean> getTransferInfos() {
        return transferInfos;
    }

    public void setTransferInfos(List<TransferInfosBean> transferInfos) {
        this.transferInfos = transferInfos;
    }

    public static class TransferInfosBean extends BaseEntity {
        public String getUiType() {
            return uiType;
        }

        public void setUiType(String uiType) {
            this.uiType = uiType;
        }

        public String getUiTitle() {
            return uiTitle;
        }

        public void setUiTitle(String uiTitle) {
            this.uiTitle = uiTitle;
        }

        private String uiTitle;
        /**
         * cur : HKD
         * status : SUCC
         * orderNumber : 12180906F001323355
         * amount : 25.00
         * payee : CHAN YU
         * createTime : 20180906
         */
        private String uiType;//ui界面使用区别是标题还是内容
        private String cur;
        private String status;
        private String orderNumber;
        private String amount;
        private String payee;
        private String createTime;

        public String getCreateTimeStr() {
            return createTimeStr;
        }

        public void setCreateTimeStr(String createTimeStr) {
            this.createTimeStr = createTimeStr;
        }

        private String createTimeStr;

        public String getPostscript() {
            return postscript;
        }

        public void setPostscript(String postscript) {
            this.postscript = postscript;
        }

        private String postscript;

        public String getStatusDesc() {
            return statusDesc;
        }

        public void setStatusDesc(String statusDesc) {
            this.statusDesc = statusDesc;
        }

        private String statusDesc;

        public String getCur() {
            return cur;
        }

        public void setCur(String cur) {
            this.cur = cur;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getPayee() {
            return payee;
        }

        public void setPayee(String payee) {
            this.payee = payee;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }
    }
}
