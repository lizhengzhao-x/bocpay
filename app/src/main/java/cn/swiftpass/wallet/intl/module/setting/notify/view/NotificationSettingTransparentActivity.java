package cn.swiftpass.wallet.intl.module.setting.notify.view;

import android.os.Handler;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.SettingContract;
import cn.swiftpass.wallet.intl.module.setting.presenter.SettingPresenter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;

public class NotificationSettingTransparentActivity extends BaseCompatActivity<SettingContract.Presenter> implements SettingContract.View {

    @Override
    protected void init() {
        mHandler = new Handler();
        if (mPresenter != null) {
            mPresenter.getNotificationStatus(true, NotificationStatusEntity.NOTIFICATIONSTATUSCLOSE, NotificationStatusEntity.NOTIFICATIONACTIONQUERY);
        }
    }

    @Override
    protected int getLayoutId() {
        return 0;
    }

    @Override
    protected SettingContract.Presenter createPresenter() {
        return new SettingPresenter();
    }

    @Override
    public void getNotificationStatusSuccess(NotificationStatusEntity notificationStatusEntity) {
        boolean isOpenStatus = notificationStatusEntity.isOpenStatus();
        if (isOpenStatus) {
            //已开启推广通知 - 弹框提示
            showErrorMsgDialog(getActivity(), getString(R.string.NT2101_1_9), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        } else {
            //未开启推广通知
            ActivitySkipUtil.startAnotherActivity(getActivity(), NotificationSettingActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            finish();
        }
    }

    @Override
    public void getNotificationStatusFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    /**
     * 设置结束动画
     */
    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }
}
