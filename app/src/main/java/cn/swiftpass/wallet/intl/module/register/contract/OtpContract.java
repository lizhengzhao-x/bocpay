package cn.swiftpass.wallet.intl.module.register.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.ForgetpwdEntity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtpEntity;
import cn.swiftpass.wallet.intl.entity.LoginSendOtpEntity;
import cn.swiftpass.wallet.intl.entity.NormalLoginSucEntity;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.RegisterAccountEntity;
import cn.swiftpass.wallet.intl.entity.SendCreditOTPEntity;
import cn.swiftpass.wallet.intl.entity.VerifyOTPEntity;

public class OtpContract {
    public interface View extends IView {

        void sendOtpSuccess(int action, ContentEntity entity, boolean isTryAgain);

        void sendOtpError(int action, String errorCode, String errorMsg, boolean isTryAgain);

        void sendFioOtpSuccess(BaseEntity entity, boolean isTryAgain);

        void sendFioOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void sendFpsOtpSuccess(FpsOtpEntity entity, String accountIdType, boolean isTryAgain);

        void sendFpsOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void sendCreditOtpSuccess(SendCreditOTPEntity entity, boolean isTryAgain);

        void sendCreditOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void verifyCreditOtpSuccess(VerifyOTPEntity entity);

        void verifyCreditOtpError(String errorCode, String errorMsg);


        void sendSmartAccountOtpSuccess(BaseEntity entity, boolean isTryAgain);

        void sendSmartAccountOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void verifyOtpSuccess(int action, RegisterAccountEntity entity);

        void verifyOtpError(int action, String errorCode, String errorMsg);

        void verifyFioOtpSuccess(BaseEntity entity);

        void verifyFioOtpError(String errorCode, String errorMsg);

//        void verifyFpsOtpSuccess(BaseEntity entity, String accountId, String accountIdType);

        void verifyFpsOtpError(String errorCode, String errorMsg);

        void verifyOtpWithPwdError(int action, String errorCode, String errorMsg);

        void verifyOtpWithPwdSuccess(int action, ForgetpwdEntity response);

        void verifySmartAccountOtpError(String errorCode, String errorMsg);

        void verifySmartAccountOtpSuccess(BaseEntity response);

        void registerBySmartAccountError(String errorCode, String errorMsg);

        void registerBySmartAccountSuccess(RegSucBySmartEntity response);

        void confirmRegisterError(String errorCode, String errorMsg);

        void confirmRegisterSuccess(RegSucEntity response);

        void confirmLoginError(String errorCode, String errorMsg, String isContinue, String passcode, String region, String action, boolean needVerify);

        void confirmLoginSuccess(NormalLoginSucEntity response);

        void fpsQueryBankOtherRecordsError(String errorCode, String errorMsg);

        void fpsQueryBankOtherRecordsSuccess(FpsOtherBankRecordsEntity response);

        void fpsConfirmAddError(String errorCode, String errorMsg);

        void fpsConfirmAddSuccess(FpsConfirmEntity response);

        void loginSendOtpError(String errorCode, String errorMsg);

        void loginSendOtpSuccess(LoginSendOtpEntity response,boolean isTryAgain);


        void loginVerifyError(String errorCode, String errorMsg);

        void loginVerifySuccess(NormalLoginSucEntity response);


        FpsBankRecordBean getBankRecord();

        String getFppRefNo();
    }


    public interface Presenter extends IPresenter<View> {
        void sendOtp(int action, boolean isTryAgain);

        void sendFioOtp(boolean isTryAgain);

        void sendFpsOtp(String accountId, String accountIdType, String action, String repeatOtp, boolean isTryAgain);

        void sendCreditOtp(String account, String walletId, String pan, String expDate, String action, boolean isTryAgain);

        void verifyCreditOtp(String account, String walletId, String pan, String expDate, String action, String otp);

        void sendSmartAccountOtp(String action, String walletId, boolean isTryAgain);

        void verifyOtp(int action, String otpNo, String smartLevel);

        void verifyOtpWithPwd(int action, String otpNo, String pwd);

        void verifyFioOtp(String otpNo);

        void verifyFpsOtp(String otpNo, String action, String accountId, String accountIdType, int pageAction);

        void verifySmartAccountOtp(String walletId, String typeFlow, String otpNo, BankLoginResultEntity smartInfo);

        void registerBySmartAccount(String passcode, String typeFlow);

        void getConfirmRegister(String passcode, String walletId, String cardID, String typeFlow);

        void getConfirmLogin(String mAccount, String walletId, String psw, String region, String actionFlow, String isContinue, boolean needVerify);

        void fpsQueryBankOtherRecords(String accountId, String accountIdType, int pageAction);

        void fpsConfirmAdd(FpsBankRecordBean mFpsBankRecord, String fppRefNo, boolean showDialog);


        void loginSendOtp(String mobile,boolean isTryAgain);
        void loginVerifyOtp(String mobile, String action, String cardId, boolean isContinue, String otpCode);

    }
}
