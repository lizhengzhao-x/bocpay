package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.view.View;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.InputBankCardNumberActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.widget.LeftImageArrowView;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardRegisterSelActivity extends BaseCompatActivity {
    @BindView(R.id.register_type_creditcard)
    LeftImageArrowView registerTypeCreditcard;
    @BindView(R.id.register_type_virtualcard)
    LeftImageArrowView registerTypeVirtualcard;

    @Override
    public void init() {
        setToolBarTitle(R.string.bind_title);
        registerTypeCreditcard.setSubText(getString(R.string.VC04_02_4));
        registerTypeVirtualcard.setSubText(getString(R.string.VC04_02_6));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_register;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @OnClick({R.id.register_type_creditcard, R.id.register_type_virtualcard})
    public void onViewClicked(View view) {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        switch (view.getId()) {
            case R.id.register_type_creditcard:
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER);
                ActivitySkipUtil.startAnotherActivity(VirtualCardRegisterSelActivity.this, InputBankCardNumberActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case R.id.register_type_virtualcard:
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_VITUALCARD_REGISTER);
                ActivitySkipUtil.startAnotherActivity(VirtualCardRegisterSelActivity.this, VirtualCardIdCardVerificationActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
        }
    }
}
