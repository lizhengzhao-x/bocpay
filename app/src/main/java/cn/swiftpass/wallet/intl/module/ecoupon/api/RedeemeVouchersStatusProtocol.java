package cn.swiftpass.wallet.intl.module.ecoupon.api;

import java.util.List;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * Created by ZhangXinchao on 2019/8/11.
 * 用我的电子券，被扫券二维码后，轮询后台是否核销成功
 */
public class RedeemeVouchersStatusProtocol extends BaseProtocol {

    private List<String> referenceNos;

    public RedeemeVouchersStatusProtocol(List<String> referenceNosIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/eVoucher/redeemeVouchersStatus";
        this.referenceNos = referenceNosIn;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.REFERENCENOS, referenceNos);
    }
}
