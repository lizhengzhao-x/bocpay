package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class GetMbkLgnInfoProtocol extends BaseProtocol {

    private final String channel;

    public GetMbkLgnInfoProtocol(String channel, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/onlineChat/getLgnInfo";
        this.channel = channel;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CHANNEL, channel);
    }
}
