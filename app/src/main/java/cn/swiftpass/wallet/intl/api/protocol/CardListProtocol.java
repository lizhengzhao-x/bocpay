package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 卡列表查询，返回对象为 AutoLoginSucEntity
 */

public class CardListProtocol extends BaseProtocol {
    public static final String TAG = CardListProtocol.class.getSimpleName();

    /**
     * 登陆钱包的用户id
     */
    String mLogonUserId;

    public CardListProtocol( String logonUserId, NetWorkCallbackListener dataCallback) {
        this.mLogonUserId = logonUserId;
        this.mDataCallback = dataCallback;

        mUrl = "api/querycard/getCardListData";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.LOGONUSERID, mLogonUserId);
    }
}
