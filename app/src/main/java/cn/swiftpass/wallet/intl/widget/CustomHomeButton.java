package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


public class CustomHomeButton extends RelativeLayout {
    private Context mContext;

    public TextView getTvTitle() {
        return tvTitle;
    }

    private TextView tvTitle;

    public TextView getTvSubTitle() {
        return tvSubTitle;
    }

    private TextView tvSubTitle;

    public ImageView getImageTop() {
        return imageTop;
    }

    private ImageView imageTop;

    public CustomHomeButton(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public CustomHomeButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public CustomHomeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.home_item_btn, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.HomeButton, defStyle, 0);

        String topText = a.getString(R.styleable.HomeButton_btn_title_text);
        String subTitle = a.getString(R.styleable.HomeButton_btn_sub_text);
        int d = a.getResourceId(R.styleable.HomeButton_btn_image_detail, R.mipmap.icon_button_nextxhdpi);
        a.recycle();

        tvTitle = rootView.findViewById(R.id.id_title);
        tvTitle.setText(topText);

        tvSubTitle = rootView.findViewById(R.id.id_subtitle);
        tvSubTitle.setText(subTitle);

        imageTop = rootView.findViewById(R.id.id_top_image);
        imageTop.setImageResource(d);
    }

    public void resizeSize() {

        tvTitle.setTextSize(14);
        tvSubTitle.setTextSize(10);
    }
}
