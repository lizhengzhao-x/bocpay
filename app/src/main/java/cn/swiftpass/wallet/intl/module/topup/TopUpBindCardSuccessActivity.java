package cn.swiftpass.wallet.intl.module.topup;

import android.view.View;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;


/**
 * 充值绑卡成功
 */
public class TopUpBindCardSuccessActivity extends BaseCompatActivity {
    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        hideBackIcon();
        setToolBarTitle(R.string.P3_D1_2_1_1);
        ProjectApp.getTempTaskStack().add(this);
        int currentType = getIntent().getExtras().getInt(Constants.DATA_TYPE);
        TextView centerView = (TextView) findViewById(R.id.id_title);
        if (currentType == Constants.DATA_S2_BIND_DELETE_ACCOUNT) {
            centerView.setText(getString(R.string.P3_ewa_01_047));
        } else {
            centerView.setText(getString(R.string.P3_D1_4_1));
        }
        findViewById(R.id.tv_card_info_nextPage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean eddaEventFromRegisterPage = getIntent().getExtras().getBoolean(Constants.PAGE_FROM_REGISTER);
                //s2账户注册成功之口绑定edda true(返回到主页) 从智能账户里头绑定智能账户 false(返回到智能账户)
                if (eddaEventFromRegisterPage) {
                    MyActivityManager.removeAllTaskExcludeMainStack();
                    ActivitySkipUtil.startAnotherActivity(TopUpBindCardSuccessActivity.this, MainHomeActivity.class);
                    //用户要求跳转到卡列表
                    EventBus.getDefault().post(new CardEventEntity(CardEventEntity.EVENT_BIND_CARD, ""));
                } else {
                    EventBus.getDefault().postSticky(new PayEventEntity(PayEventEntity.EVENT_PAY_SUCCESS, ""));
                    MyActivityManager.removeAllTempTaskStack();
                }
            }
        });
    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_topup_bindcard_success;
    }

    @Override
    public void onBackPressed() {

    }
}
