package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardRegisterSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.VirtualCardRegisterVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.RegisterVitualCardSendOtpContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardRegisterSuccessEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardSendOtpEntity;

/**
 * Created by ZhangXinchao on 2019/11/15.
 */
public class RegisterVitualCardSendOtpPresenter extends BasePresenter<RegisterVitualCardSendOtpContract.View> implements RegisterVitualCardSendOtpContract.Presenter {
    @Override
    public void registerVitualCardSenOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardRegisterSendOtpProtocol(vitualCardSendOtpEntiyIn, new NetWorkCallbackListener<VirtualCardSendOtpEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerVitualCardSenOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(VirtualCardSendOtpEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerVitualCardSenOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();

    }

    @Override
    public void registerVitualCardVerifyOtp(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, String walletId,String verifyCode, String cardId) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualCardRegisterVerifyOtpProtocol(vitualCardSendOtpEntiyIn, walletId,verifyCode, cardId, new NetWorkCallbackListener<VirtualCardRegisterSuccessEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerVitualCardVerifyOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VirtualCardRegisterSuccessEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().registerVitualCardVerifyOtpSuccess(response);
                }
            }
        }).execute();
    }
}
