package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.protocol.BaseProtocol;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 登出接口，返回对象 BaseEntity
 */

public class LogoutProtocol extends BaseProtocol {
    public LogoutProtocol() {
        mUrl = "api/login/out";
    }
}
