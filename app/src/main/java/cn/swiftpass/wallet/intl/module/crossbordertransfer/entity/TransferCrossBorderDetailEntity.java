package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 20:01
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderDetailEntity extends BaseEntity {


    /**
     * {
     * "customerNotice": "",
     * "feeCur": "HKD",
     * "amountHKD": "60",
     * "lastUpdated": "2019/11/22 00:00",
     * "totalAmt": "70",
     * "txnId": "EWAC2019112700000024",
     * "receiverCardType": "D",
     * "receiverCardNo": "6213560028484",
     * "forUsed": "购物",
     * "fee": "10",
     * "panFour": "2365",
     * "totalAmtCur": "HKD",
     * "CNY2HKDRate": "0.8998",
     * "accountType": "2",
     * "receiverName": "SERVICE NAME",
     * "receiverBankName": "Jingshan BOC Fullerton bank",
     * "amountCNY": "53.99",
     * "exchangeRateDesc": "",
     * "mobile": "+86-17051132830"
     * }
     */


    public String txnId;
    public String mobile;

    @SerializedName("receiverName")
    public String payeeName;

    @SerializedName("receiverCardNo")
    public String payeeCardId;

    @SerializedName("receiverBankName")
    public String payeeBank;

    public String customerNotice;
    public String feeCur;
    public String amountHKD;
    public String lastUpdated;
    public String totalAmt;
    public String receiverCardType;
    public String forUsed;
    public String fee;
    public String panFour;
    public String totalAmtCur;
    public String CNY2HKDRate;
    public String accountType;
    public String amountCNY;
    public String exchangeRateDesc;
    public String tncUrl;
    public String feeDescription;
    public String hasOverDayLimit;
    /**
     * 转账手机号
     */
    public String account;

    public String getNeedOtpVerify() {
        return needOtpVerify;
    }

    public void setNeedOtpVerify(String needOtpVerify) {
        this.needOtpVerify = needOtpVerify;
    }

    private String needOtpVerify;

    /**
     * 转账需要发送otp
     *
     * @return
     */
    public boolean needSendOtp() {

        if (TextUtils.isEmpty(needOtpVerify)) {
            return false;
        }
        return TextUtils.equals(needOtpVerify, "1");

    }

}
