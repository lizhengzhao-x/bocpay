package cn.swiftpass.wallet.intl.module.register.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class TaxInfoQuestionBottomHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_add_tax_info)
    TextView tvAddTaxInfo;
    @BindView(R.id.iv_check_usa)
    ImageView ivCheckUsa;
    private OnBottomListener listener;
    private boolean selectUsa;
    private int resultSelectQuestionTwo;

    public TaxInfoQuestionBottomHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);

        ivCheckUsa.setOnClickListener(v -> {
            toggleSelectUS();
            if (listener != null) {
                listener.OnSelectUSTip(selectUsa);
            }
        });
        if (resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_YES) {
            tvAddTaxInfo.setVisibility(View.VISIBLE);
            tvAddTaxInfo.setOnClickListener(v -> {
                if (listener != null) {
                    listener.addTaxInfo();
                }
            });
        } else {
            tvAddTaxInfo.setVisibility(View.GONE);
        }

    }

    private void toggleSelectUS() {
        selectUsa = !selectUsa;
        ivCheckUsa.setImageResource(selectUsa ? R.mipmap.list_icon_choose_circle_chosen : R.mipmap.list_icon_choose_circle_default);
    }

    public void setBottomListener(OnBottomListener listener) {
        this.listener = listener;
    }


    public void setData(int resultSelectQuestionTwo) {
        this.resultSelectQuestionTwo = resultSelectQuestionTwo;
        if (resultSelectQuestionTwo == TaxListAdapter.DATA_QUESTION_YES) {
            tvAddTaxInfo.setVisibility(View.VISIBLE);
            tvAddTaxInfo.setOnClickListener(v -> {
                if (listener != null) {
                    listener.addTaxInfo();
                }
            });
        } else {
            tvAddTaxInfo.setVisibility(View.GONE);
        }
    }


    public interface OnBottomListener {
        void OnSelectUSTip(boolean select);

        void addTaxInfo();
    }
}
