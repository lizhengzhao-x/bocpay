package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.module.rewardregister.api.GetRewardRegistrationListProtocol;
import cn.swiftpass.wallet.intl.module.rewardregister.entity.RewardActivityListEntity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.CAMPAIGNID;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.IMAGEURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterDetailActivity.TERMINFOURL;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.BANKCARDTYPE;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;
import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.URL_REWARD;

/**
 * 信用卡奖赏登记列表
 */
public class RewardRegisterSelectListActivity extends BaseCompatActivity {
    @BindView(R.id.id_recycleview)
    RecyclerView recyclerView;
    @BindView(R.id.id_smart_refreshLayout)
    SmartRefreshLayout smartRefreshLayout;
    private RewardSelListAdapter rewardSelListAdapter;
    private List<RewardActivityListEntity.RewardActivity> rewardActivities;
    private View emptyView;

    @Override
    public void init() {
        setToolBarTitle(R.string.CR209_7_1);
        rewardActivities = new ArrayList<>();
        rewardSelListAdapter = new RewardSelListAdapter(rewardActivities);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(rewardSelListAdapter);
        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        recyclerView.addItemDecoration(itemDecoration);
        smartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                getActivityList();
            }
        });
        emptyView = getEmptyView(mContext, recyclerView);
        rewardSelListAdapter.setEmptyView(emptyView);
        rewardSelListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseRecyclerAdapter baseRecyclerAdapter, View view, int position) {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(CAMPAIGNID, rewardActivities.get(position).getCampaignId());
                maps.put(URL_REWARD, rewardActivities.get(position).getActivityUrl());
                maps.put(IMAGEURL, rewardActivities.get(position).getImagesUrl());
                maps.put(TERMINFOURL, rewardActivities.get(position).getTermsAndConditionsUrl());
                maps.put(BANKCARDTYPE, rewardActivities.get(position).getBankCardType());
                maps.put(REMARK, rewardActivities.get(position).getExplain());
                ActivitySkipUtil.startAnotherActivity(RewardRegisterSelectListActivity.this, RewardRegisterWebviewActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });
        getActivityList();
    }

    /**
     * 页面添加空布局  直接用该方法
     *
     * @param mContext
     * @param recyclerView
     * @return
     */
    protected View getEmptyView(Context mContext, RecyclerView recyclerView) {
        View emptyView = LayoutInflater.from(mContext).inflate(R.layout.view_empty, (ViewGroup) recyclerView.getParent(), false);
        ImageView noDataImg = emptyView.findViewById(R.id.empty_img);
        TextView noDataText = emptyView.findViewById(R.id.empty_content);
        noDataText.setText(getString(R.string.LI10_1_2));
        noDataImg.setVisibility(View.GONE);
        return emptyView;
    }


    private void getActivityList() {
        showDialogNotCancel();
        new GetRewardRegistrationListProtocol(new NetWorkCallbackListener<RewardActivityListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                dismissDialog();
                smartRefreshLayout.finishRefresh();
                emptyView.setVisibility(View.VISIBLE);
                showErrorMsgDialog(RewardRegisterSelectListActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(RewardActivityListEntity response) {
                dismissDialog();
                smartRefreshLayout.finishRefresh();
                if (response != null && !response.getValue().isEmpty()) {
                    emptyView.setVisibility(View.GONE);
                    rewardActivities.clear();
                    rewardActivities.addAll(response.getValue());
                    rewardSelListAdapter.notifyDataSetChanged();
                } else {
                    emptyView.setVisibility(View.VISIBLE);
                }
            }
        }).execute();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_reward_sellist;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }


}
