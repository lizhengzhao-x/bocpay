package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 验证FIO的OTP
 */

public class FpsOtpSendProtocol extends BaseProtocol {
    //客户端调用Authtication 接口验证用户指纹，之后SDK返回的base64字符串

    String mAccountId;
    String mAccountIdType;
    String mAction;
    String mRepeatOtp; //0:不是点击重新发送，1：点击重新发送

    public FpsOtpSendProtocol(String accountId, String accountIdType, String action, String repeatOtp, NetWorkCallbackListener dataCallback) {
        mAccountId = accountId;
        mAccountIdType = accountIdType;
        mAction = action;
        mRepeatOtp = repeatOtp;
        this.mDataCallback = dataCallback;
        mUrl = "api/fps/triggerOtpFPS";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID, mAccountId);
        mBodyParams.put(FpsConst.FPS_ACCOUNT_ID_TYPE, mAccountIdType);
        mBodyParams.put(FpsConst.FPS_ACTION, mAction);
        mBodyParams.put(FpsConst.FPS_REPEAT_OTP, mRepeatOtp);
    }
}
