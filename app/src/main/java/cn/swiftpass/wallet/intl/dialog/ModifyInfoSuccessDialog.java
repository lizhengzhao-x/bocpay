package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;


public class ModifyInfoSuccessDialog extends Dialog {

    public ModifyInfoSuccessDialog(Context context) {
        super(context);
    }

    public ModifyInfoSuccessDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder {
        private Context context;
        private String message;
        private String title;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        private OnClickListener positiveButtonClickListener;
        private int leftColor;
        private int rightColor;
        private int textSize;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessageTextSize(int textSize) {
            this.textSize = textSize;
            return this;
        }

        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }


        public ModifyInfoSuccessDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final ModifyInfoSuccessDialog dialog = new ModifyInfoSuccessDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_modify_info_layout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title
            // set the confirm button
            if (positiveButtonText != null) {
                ((Button) layout.findViewById(R.id.positive_dl)).setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    layout.findViewById(R.id.positive_dl).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(R.id.positive_dl).setVisibility(View.GONE);
            }
            // set the content message
            if (message != null) {
                if (textSize != 0) {
                    ((TextView) layout.findViewById(R.id.message_dl)).setTextSize(textSize);
                }
                ((TextView) layout.findViewById(R.id.message_dl)).setText(message.trim());
                if (0 != gravity) {
                    ((TextView) layout.findViewById(R.id.message_dl)).setGravity(gravity);
                }
            }
            if (title != null) {
                ((TextView) layout.findViewById(R.id.id_title)).setText(title);
                ((LinearLayout) layout.findViewById(R.id.id_center_view)).setBackgroundResource(R.drawable.dialog_center_bg);
            } else {
                ((LinearLayout) layout.findViewById(R.id.id_title_layout)).setVisibility(View.GONE);
            }
            dialog.setContentView(layout);
            return dialog;
        }
    }
}

