package cn.swiftpass.wallet.intl.module.activitys;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.DeepLinkSkipEventEntity;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.module.home.presenter.NewMsgPresenter;
import cn.swiftpass.wallet.intl.utils.DeepLinkUtils;

/**
 * 最新消息
 *
 * @author can.shi
 */
public class NewMsgActivity extends BaseCompatActivity {

    private FragmentManager mFragmentManager;
    private NewMsgFragment newMsgFragment;

    @Override
    public void init() {
        setToolBarTitle(R.string.what_is_new);

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        newMsgFragment = new NewMsgFragment();
        fragmentTransaction.add(R.id.fl_main, newMsgFragment);
        fragmentTransaction.commitAllowingStateLoss();

        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(DeepLinkSkipEventEntity event) {
        if (event.getEventType() == DeepLinkUtils.TYPE_REMITTANCE_CLOSE_MSG_PAGE) {
            finish();
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(UplanEntity event) {
        if (event.getEventType() == UplanEntity.EVENT_UPLAN_BACK_SCAN) {
            finish();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_new_msg;
    }

    @Override
    protected NewMsgPresenter createPresenter() {
        return new NewMsgPresenter();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
