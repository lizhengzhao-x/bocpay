package cn.swiftpass.wallet.intl.module.cardmanagement.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.protocol.STwoTopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpAndWithDrawProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TopUpPreCheckProtocol;
import cn.swiftpass.wallet.intl.api.protocol.UpdateSmartAccountInfoProtocol;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.entity.SThreeTopUpEntity;
import cn.swiftpass.wallet.intl.entity.STwoTopUpEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;
import cn.swiftpass.wallet.intl.entity.TopUpResponseEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountDeleteContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountTopUpContract;


public class SmartAccountDeletePresenter extends BasePresenter<SmartAccountDeleteContract.View> implements SmartAccountDeleteContract.Presenter {


    @Override
    public void updateSmartAccountCancel() {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new UpdateSmartAccountInfoProtocol(SmartAccountConst.UPDATE_SMARTACCOUNT_CANCEL, null, null, null, null, new NetWorkCallbackListener<SmartAccountUpdateEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().updateSmartAccountCancelFail(errorCode,  errorMsg);
                }

            }

            @Override
            public void onSuccess(SmartAccountUpdateEntity response) {
                if (getView()!=null){
                    getView().dismissDialog();
                    getView().updateSmartAccountCancelSuccess(response);
                }

            }
        }).execute();
    }
}
