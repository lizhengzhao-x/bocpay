package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.GetVitualCardListContract;
import cn.swiftpass.wallet.intl.module.virtualcard.presenter.GetVitualCardListPresenter;

/**
 * Created by ZhangXinchao on 2019/11/13.
 * 虚拟卡绑定 协议界面
 */
public class VirtualCardBindTermsActivity extends BaseCompatActivity {

    private FragmentManager mFragmentManager;
    private VirtualCardBindTermsFragment virtualCardBindTermsFragment;

    @Override
    protected void init() {

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        Bundle bundle = intent.getExtras();
        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putInt(Constants.CURRENT_PAGE_FLOW, bundle.getInt(Constants.CURRENT_PAGE_FLOW));
        fragmentBundle.putSerializable(Constants.VIRTUALCARDLIST, bundle.getSerializable(Constants.VIRTUALCARDLIST));
        virtualCardBindTermsFragment = new VirtualCardBindTermsFragment();
        virtualCardBindTermsFragment.setArguments(fragmentBundle);

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fl_main, virtualCardBindTermsFragment);
        fragmentTransaction.commitAllowingStateLoss();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_virtualcard_bind_term_2;
    }


    @Override
    protected GetVitualCardListContract.Presenter createPresenter() {
        return new GetVitualCardListPresenter();
    }


}
