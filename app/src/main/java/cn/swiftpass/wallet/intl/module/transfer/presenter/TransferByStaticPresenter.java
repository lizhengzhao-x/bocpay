package cn.swiftpass.wallet.intl.module.transfer.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.TransferNewQRCodeProtocol;
import cn.swiftpass.wallet.intl.entity.StaticCodeEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByStaticContract;


public class TransferByStaticPresenter extends BasePresenter<TransferByStaticContract.View> implements TransferByStaticContract.Presenter {


    @Override
    public void pullQRCode() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferNewQRCodeProtocol("", "", new NetWorkCallbackListener<StaticCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().pullQRCodeError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(StaticCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().pullQRCodeSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void updateQRCode(String account, String remarks) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new TransferNewQRCodeProtocol(account, remarks, new NetWorkCallbackListener<StaticCodeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateQRCodeError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(StaticCodeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().updateQRCodeSuccess(response);
                }
            }
        }).execute();
    }


}
