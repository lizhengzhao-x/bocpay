package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * 兑换失败页面
 */
public class EcouponsConvertStatusActivity extends BaseCompatActivity {
    @BindView(R.id.id_invite_status_image)
    ImageView idInviteStatusImage;
    @BindView(R.id.id_invite_status)
    TextView idInviteStatus;
    @BindView(R.id.id_invite_message)
    TextView idInviteMessage;
    @BindView(R.id.id_invite_sub_message)
    TextView idInviteSubMessage;
    @BindView(R.id.id_tv_back)
    TextView idTvBack;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC06_1));
        hideBackIcon();
        String errorMsg = getIntent().getExtras().getString(Constants.ERROR_MESSAGE);
        idInviteMessage.setText(errorMsg);
        idTvBack.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                EventBus.getDefault().post(new EcoupEventEntity(EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP, ""));
                finish();
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_convert_fail;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
        //  super.onBackPressed();
    }
}
