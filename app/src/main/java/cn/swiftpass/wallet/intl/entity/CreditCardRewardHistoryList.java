package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class CreditCardRewardHistoryList extends BaseEntity {

    private String currentPage;
    private String hasNext;
    private String lastRewardDate;

    private List<CreditCardRewardHistoryItemList> rows;
    private List<CreditCardRewardDateItemList> queryDateList;


    public String getLastRewardDate() {
        return lastRewardDate;
    }

    public void setLastRewardDate(String lastRewardDate) {
        this.lastRewardDate = lastRewardDate;
    }

    public List<CreditCardRewardDateItemList> getQueryDateList() {
        return queryDateList;
    }

    public void setQueryDateList(List<CreditCardRewardDateItemList> queryDateList) {
        this.queryDateList = queryDateList;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getHasNext() {
        return hasNext;
    }

    public void setHasNext(String hasNext) {
        this.hasNext = hasNext;
    }

    public List<CreditCardRewardHistoryItemList> getRows() {
        return rows;
    }

    public void setRows(List<CreditCardRewardHistoryItemList> rows) {
        this.rows = rows;
    }

}
