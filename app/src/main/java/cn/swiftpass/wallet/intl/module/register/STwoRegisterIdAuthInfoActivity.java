package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.graphics.Color;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AreaEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RegisterCustomerInfo;
import cn.swiftpass.wallet.intl.module.register.model.RegisterDetailsData;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

import static cn.swiftpass.wallet.intl.module.register.RegisterDetailsActivity.DATA_REGISTER_DETAIL_DATA;

/**
 * 证件信息识别成功之后 需要填写注册人的具体地址信息 邮件信息 非空检测等
 */
public class STwoRegisterIdAuthInfoActivity extends BaseCompatActivity {
    @BindView(R.id.id_error_street)
    TextView idErrorStreet;
    @BindView(R.id.id_error_buildingName)
    TextView idErrorBuildingName;
    @BindView(R.id.id_error_roomName)
    TextView idErrorRoomName;
    @BindView(R.id.id_error_email)
    TextView idErrorEmail;
    private RelativeLayout mId_sel_country;
    private TextView mTv_region;
    private LinearLayout mLl_line_common, mId_no_customer;
    private LinearLayout mLinear_area;
    private RelativeLayout mId_sel_area;
    private TextView mTv_area;
    private EditTextWithDel mTv_buildingName;
    private EditTextWithDel mTv_street_name;
    private EditTextWithDel mTv_room_name;
    private EditTextWithDel mEtwd_email;
    private TextView mId_register_next;
    private AreaEntity mAreaEntity;
    private int mCurrentSelPostion;
    private int mCurrentType;
    private RegisterDetailsData registerDetailsData;


    private void bindViews() {
        mId_sel_country = (RelativeLayout) findViewById(R.id.id_sel_country);
        mTv_region = (TextView) findViewById(R.id.tv_region);
        mLl_line_common = (LinearLayout) findViewById(R.id.ll_line_common);
        mId_no_customer = (LinearLayout) findViewById(R.id.id_no_customer);
        mLinear_area = (LinearLayout) findViewById(R.id.linear_area);
        mId_sel_area = (RelativeLayout) findViewById(R.id.id_sel_area);
        mTv_area = (TextView) findViewById(R.id.tv_area);
        mTv_buildingName = (EditTextWithDel) findViewById(R.id.tv_buildingName);
        mTv_street_name = (EditTextWithDel) findViewById(R.id.tv_street_name);
        mTv_room_name = (EditTextWithDel) findViewById(R.id.tv_room_name);
        mEtwd_email = (EditTextWithDel) findViewById(R.id.etwd_email);
        mId_register_next = (TextView) findViewById(R.id.id_register_next);


        mTv_buildingName.hideErrorView();
        mTv_street_name.hideErrorView();
        mTv_room_name.hideErrorView();
        mEtwd_email.hideErrorView();
        mEtwd_email.hideDelView();

        mTv_buildingName.hideDelView();
        mTv_street_name.hideDelView();
        mTv_room_name.hideDelView();

        mTv_street_name.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(35)});
        mTv_buildingName.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(35)});
        mTv_room_name.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(25)});

        mEtwd_email.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(34)});

        mTv_street_name.addTextChangedListener(textWatcherStreet);
        mTv_buildingName.addTextChangedListener(textWatcherBuilding);
        mTv_room_name.addTextChangedListener(textWatcherRoom);
        mEtwd_email.addTextChangedListener(textWatcherEmail);

        hideBackIcon();
        setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);
    }


    private TextWatcher textWatcherStreet = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String streetName = mTv_street_name.getText().toString().trim();
            idErrorStreet.setVisibility(!TextUtils.isEmpty(streetName) ? View.INVISIBLE : View.VISIBLE);
        }
    };

    private TextWatcher textWatcherBuilding = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String buildingName = mTv_buildingName.getText().toString().trim();
            idErrorBuildingName.setVisibility(!TextUtils.isEmpty(buildingName) ? View.INVISIBLE : View.VISIBLE);
        }
    };

    private TextWatcher textWatcherRoom = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String room = mTv_room_name.getText().toString().trim();
            idErrorRoomName.setVisibility(!TextUtils.isEmpty(room) ? View.INVISIBLE : View.VISIBLE);

        }
    };

    private TextWatcher textWatcherEmail = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            String email = mEtwd_email.getText().toString().trim();
            idErrorEmail.setVisibility(!TextUtils.isEmpty(email) ? View.INVISIBLE : View.VISIBLE);
        }
    };


    private void checkVailed() {
        String streetName = mTv_street_name.getText().toString().trim();
        String buildingName = mTv_buildingName.getText().toString().trim();
        String room = mTv_room_name.getText().toString().trim();
        String email = mEtwd_email.getText().toString().trim();

        idErrorStreet.setVisibility(!TextUtils.isEmpty(streetName) ? View.INVISIBLE : View.VISIBLE);
        idErrorBuildingName.setVisibility(!TextUtils.isEmpty(buildingName) ? View.INVISIBLE : View.VISIBLE);
        idErrorRoomName.setVisibility(!TextUtils.isEmpty(room) ? View.INVISIBLE : View.VISIBLE);
        idErrorEmail.setVisibility(!TextUtils.isEmpty(email) ? View.INVISIBLE : View.VISIBLE);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //如果从编辑页面跳转过来
        registerDetailsData = (RegisterDetailsData) intent.getSerializableExtra(DATA_REGISTER_DETAIL_DATA);
    }

    @Override
    public boolean isAnalysisPage() {
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA || mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        analysisPageEntity.last_address = PagerConstant.FACIAL_RECOGNITION_THIRD_ACTION;
        analysisPageEntity.current_address = PagerConstant.INPUT_REGISTRATION_ADDRESS;
        return analysisPageEntity;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {

        hideBackIcon();
        bindViews();
        DataCollectManager.getInstance().sendFillAddress();
        mCurrentType = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                initSDKListener();
            }
        } else {
            setToolBarTitle(R.string.IDV_3_1a);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        mId_register_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ButtonUtils.isFastDoubleClick()) {
                    sendRegisterInfo();
                }

            }
        });
        initAreas();
        addItemButtons();
        mTv_region.setText(getString(R.string.P3_ewa_01_009));
        getToolBarRightView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
                    AndroidUtils.showConfrimExitDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskWithRegister();
                            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                                ChatSDKManager.hide();
                            }
                        }
                    });
                } else if (mCurrentType == Constants.PAGE_FLOW_BIND_PA) {
                    AndroidUtils.showConfrimExitDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            //信用卡邦我的账户
                            MyActivityManager.removeAllTaskWithPaBindCard();
                            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                                ChatSDKManager.hide();
                            }
                        }
                    });

                }
            }
        });
        //注册流程如果DJ601 activity 退出
        ProjectApp.getTempTaskStack().add(this);
        updateOkBackground(mId_register_next, true);
    }

    public void initSDKListener() {


        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);

        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_INPUTADDRESS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCurrentType == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.hide();
            }
        }
    }

    private void initAreas() {
        mAreaEntity = new AreaEntity();
        List<AreaEntity.AreaItem> areaItems = new ArrayList<>();
        areaItems.add(new AreaEntity.AreaItem(getString(R.string.P3_A1_8_7_3)));
        areaItems.add(new AreaEntity.AreaItem(getString(R.string.P3_A1_8_7_2)));
        areaItems.add(new AreaEntity.AreaItem(getString(R.string.P3_A1_8_7_1)));
        mAreaEntity.setAreaItems(areaItems);
    }


    private void addItemButtons() {
        int size = mAreaEntity.getAreaItems().size();
        int itemWidth = (ScreenUtils.getDisplayWidth() - AndroidUtils.dip2px(this, 60)) / 3;
        for (int i = 0; i < size; i++) {
            AreaEntity.AreaItem areaItem = mAreaEntity.getAreaItems().get(i);
            TextView btn = new TextView(this);
            btn.setGravity(Gravity.CENTER);
            if (i == 0) {
                btn.setBackgroundResource(R.drawable.bg_btn_area_page_disable);
                btn.setTextColor(getResources().getColor(R.color.white));
                btn.setSelected(false);
            } else {
                btn.setBackgroundResource(R.drawable.bg_btn_area_page_normal);
                btn.setTextColor(getResources().getColor(R.color.selection));
            }
            btn.setTag(i);
            btn.setTextSize(10);
            btn.setText(areaItem.getpName());
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(itemWidth, AndroidUtils.dip2px(this, 30));
            if (i != 0) {
                layoutParams.leftMargin = AndroidUtils.dip2px(this, 10);
            }
            btn.setLayoutParams(layoutParams);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCurrentSelPostion = (Integer) v.getTag();
                    updateItemBtnStatus(mCurrentSelPostion);
                }
            });
            mLinear_area.addView(btn);
        }
    }

    private void updateItemBtnStatus(int sel) {
        for (int i = 0; i < mLinear_area.getChildCount(); i++) {
            TextView btn = (TextView) mLinear_area.getChildAt(i);
            if (sel == i) {
                btn.setBackgroundResource(R.drawable.bg_btn_area_page_disable);
                btn.setSelected(true);
                btn.setTextColor(Color.WHITE);
            } else {
                btn.setBackgroundResource(R.drawable.bg_btn_area_page_normal);
                btn.setSelected(false);
                btn.setTextColor(getResources().getColor(R.color.selection));
            }
        }
    }


    private void sendRegisterInfo() {
        checkVailed();
        String area = mTv_region.getText().toString().trim();
        String streetName = mTv_street_name.getText().trim();
        String buildingName = mTv_buildingName.getText().toString().trim();
        String room = mTv_room_name.getText().toString().trim();
        String region = mAreaEntity.getAreaItems().get(mCurrentSelPostion).getpName();
        String email = mEtwd_email.getText().toString().trim();
        if (TextUtils.isEmpty(area) ||
                TextUtils.isEmpty(streetName) ||
                TextUtils.isEmpty(buildingName) ||
                TextUtils.isEmpty(room) ||
                TextUtils.isEmpty(email)) {
            showErrorMsgDialog(this, getString(R.string.IDV_15b_2));
            return;
        }

        if (AndroidUtils.isContainChinese(streetName)) {
            if (streetName.length() > 14) {
                showErrorMsgDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.street_error_show));
                return;
            }
        }

        if (AndroidUtils.isContainChinese(buildingName)) {
            if (buildingName.length() > 16) {
                showErrorMsgDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.build_error_show));
                return;
            }
        }

        if (AndroidUtils.isContainChinese(room)) {
            if (room.length() > 16) {
                showErrorMsgDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.room_error_show));
                return;
            }
        }

        if (!AndroidUtils.isEmail(email)) {
            showErrorMsgDialog(STwoRegisterIdAuthInfoActivity.this, getString(R.string.IDV_15b_6));
            return;
        }
        RegisterCustomerInfo registerCustomerInfo = new RegisterCustomerInfo();
        registerCustomerInfo.mArea = area;
        registerCustomerInfo.mEmail = email;
        registerCustomerInfo.mStreetName = streetName;
        registerCustomerInfo.mBuildingName = buildingName;
        registerCustomerInfo.mRoom = room;
        registerCustomerInfo.mRegion = region;


        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.REGISTER_CUSTOM_INFO, registerCustomerInfo);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mCurrentType);
        if (registerDetailsData != null) {
            mHashMaps.put(DATA_REGISTER_DETAIL_DATA, registerDetailsData);
        }
        mHashMaps.put(Constants.DATA_COUNTRY_NAME, getIntent().getExtras().getString(Constants.DATA_COUNTRY_NAME));
        mHashMaps.put(Constants.DATA_ID_CARD, getIntent().getExtras().getString(Constants.DATA_ID_CARD));
        mHashMaps.put(Constants.OCR_INFO, getIntent().getExtras().getSerializable(Constants.OCR_INFO));
        ActivitySkipUtil.startAnotherActivity(STwoRegisterIdAuthInfoActivity.this, RegisterTaxListActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ProjectApp.getTempTaskStack().remove(this);

    }

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_s2_register_idauthinfo;
    }

    @Override
    public void onBackPressed() {

    }


}
