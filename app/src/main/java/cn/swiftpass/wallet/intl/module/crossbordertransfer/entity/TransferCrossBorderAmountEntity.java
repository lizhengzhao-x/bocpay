package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;

import cn.swiftpass.wallet.intl.module.transfer.entity.PayeeEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/13 19:31
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderAmountEntity extends PayeeEntity {

    public String rmb;
    public String hkd;
    public String purpose;
    public String account;

    public TransferCrossBorderAmountEntity() {
        super();
    }

    public TransferCrossBorderAmountEntity(String rmb, String hkd, String purpose, String account) {
        this.rmb = rmb;
        this.hkd = hkd;
        this.purpose = purpose;
        this.account = account;
    }

    public TransferCrossBorderAmountEntity(String payeeName, String payeeHideName, String payeeCardId, String payeeBank, String rmb, String hkd, String purpose, String account) {
        super(payeeName, payeeHideName, payeeCardId, payeeBank);
        this.rmb = rmb;
        this.hkd = hkd;
        this.purpose = purpose;
        this.account = account;
    }

}
