package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * @author zxc
 * 左边文字 右边箭头布局封装 去除本身的padding 由父布局处理
 */

public class ImageArrowNoPaddingView extends RelativeLayout {

    private Context mContext;
    private String leftText = "";
    private String rightText = "";
    private boolean showRightArrow;
    private boolean showLine;

    public TextView getTvLeft() {
        return tvLeft;
    }

    private TextView tvLeft;

    public TextView getTvRight() {
        return tvRight;
    }

    private TextView tvRight;

    public ImageView getImageRight() {
        return imageRight;
    }

    private ImageView imageRight;

    public ImageArrowNoPaddingView(Context context) {
        super(context);
        this.mContext = context;
        initViews(null, 0);
    }

    public ImageArrowNoPaddingView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initViews(attrs, defStyle);
    }

    public ImageArrowNoPaddingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews(attrs, 0);
    }

    private void initViews(AttributeSet attrs, int defStyle) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_imageright_nopadding, null);
        addView(rootView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        final Resources.Theme theme = mContext.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs, R.styleable.ArrowRightView, defStyle, 0);

        leftText = a.getString(R.styleable.ArrowRightView_left_text);
        rightText = a.getString(R.styleable.ArrowRightView_ar_right_text);
        showRightArrow = a.getBoolean(R.styleable.ArrowRightView_show_right_arrow, true);
        showLine = a.getBoolean(R.styleable.ArrowRightView_show_bottom_line, false);
        int d = a.getResourceId(R.styleable.ArrowRightView_right_image, R.mipmap.icon_button_nextxhdpi);
        int rightColor = a.getColor(R.styleable.ArrowRightView_right_text_color, Color.WHITE);
        a.recycle();

        tvLeft = rootView.findViewById(R.id.id_tv_left);
        tvRight = rootView.findViewById(R.id.tv_right);
        tvRight.setTextColor(rightColor);
        View lineView = rootView.findViewById(R.id.id_line);

        imageRight = rootView.findViewById(R.id.id_image_right);
        imageRight.setImageResource(d);
        if (!TextUtils.isEmpty(leftText)) {
            tvLeft.setText(leftText);
        }
        if (!TextUtils.isEmpty(rightText)) {
            tvRight.setText(rightText);
        }
        imageRight.setVisibility(showRightArrow ? VISIBLE : INVISIBLE);
        lineView.setVisibility(showLine ? VISIBLE : INVISIBLE);
    }


    public void setRightImageShow(boolean b) {
        imageRight.setVisibility(b ? VISIBLE : INVISIBLE);
    }

    public void setRightImage(int resourceId) {
        imageRight.setImageResource(resourceId);
    }

    public void setLeftTextTitle(String title) {
        tvLeft.setText(title);
    }

    public void setRightText(String txt) {
        tvRight.setText(txt);
    }

    public String getRightText() {
        return tvRight.getText().toString();
    }
}
