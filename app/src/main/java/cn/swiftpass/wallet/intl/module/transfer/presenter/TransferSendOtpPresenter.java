package cn.swiftpass.wallet.intl.module.transfer.presenter;

import android.text.TextUtils;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.api.protocol.FirstSendOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FirstTransferVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferConfirmByBocProtocol;
import cn.swiftpass.wallet.intl.api.protocol.TransferConfirmProtocol;
import cn.swiftpass.wallet.intl.entity.TransferCompleteEntity;
import cn.swiftpass.wallet.intl.entity.TransferConfirmReq;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferFirstSendOtpContract;

public class TransferSendOtpPresenter extends BasePresenter<TransferFirstSendOtpContract.View> implements TransferFirstSendOtpContract.Presenter {
    @Override
    public void transferFirstSenOtp(String txnId, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FirstSendOtpProtocol(txnId, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstSenOtpError(errorCode, errorMsg, isTryAgain);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstSenOtpSuccess(response, isTryAgain);
            }
        }).execute();
    }

    @Override
    public void transferFirstVerifyOtp(String txnId, String verifyCode, final TransferPreCheckEntity mPreCheckEntity, final boolean isLiShi, final boolean isBocPay) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new FirstTransferVerifyOtpProtocol(txnId, verifyCode, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                }
                getView().transferFirstVerifyOtpError(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {
                //验证完otp 直接调用转账确认接口
                transferConfirm(mPreCheckEntity, isLiShi, isBocPay);
//                if (getView() != null) {
//                    getView().dismissDialog();
//                }
//                getView().transferFirstVerifyOtpSuccess(response);

            }
        }).execute();


    }

    @Override
    public void transferConfirm(TransferPreCheckEntity mPreCheckEntity, boolean isLiShi, boolean isBocPay) {
//        if (getView() != null) {
//            getView().showDialogNotCancel();
//        }

        if (isBocPay) {
            new TransferConfirmByBocProtocol(mPreCheckEntity.getScrRefNo(), isLiShi, new NetWorkCallbackListener<TransferCompleteEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().transferConfirmError(errorCode, errorMsg);
                    }
                }

                @Override
                public void onSuccess(TransferCompleteEntity response) {
                    if (getView() != null) {
                        getView().dismissDialog();
                        getView().transferConfrimSuccess(response);
                    }
                }
            }).execute();
        } else {

            final TransferConfirmReq req = new TransferConfirmReq();
            req.tansferToAccount = TextUtils.isEmpty(mPreCheckEntity.getAccountNo()) ? mPreCheckEntity.getPreCheckReq().tansferToAccount : mPreCheckEntity.getAccountNo();
            req.transferAmount = mPreCheckEntity.getDrAmt();
            req.bankType = mPreCheckEntity.getPreCheckReq().bankType;
            req.scrRefNo = mPreCheckEntity.getScrRefNo();
            req.transferType = mPreCheckEntity.getPreCheckReq().transferType;
            req.isLiShi = mPreCheckEntity.getPreCheckReq().isLiShi;

            new TransferConfirmProtocol(req, new NetWorkCallbackListener<TransferCompleteEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (getView() != null) {
                        getView().dismissDialog();
                    }
                    getView().transferConfirmError(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(TransferCompleteEntity response) {
                    getView().transferConfrimSuccess(response);
                    if (getView() != null) {
                        getView().dismissDialog();
                    }
                }
            }).execute();
        }

    }
}
