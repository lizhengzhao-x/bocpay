package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.MenuFilterEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.MenuFilterAdapter;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

public class RightDrawerlayoutView extends LinearLayout {

    private Context mContext;
    //右边侧滑 筛选列表
    private RecyclerViewForEmpty idRecyclerViewFilter;
    private TextView tvClearFilter;
    private TextView tvRegister;
    private List<MenuFilterEntity> menuFilterEntities;

    public RightDrawerlayoutView(Context context) {
        super(context);
        this.mContext = context;
        initViews();
    }

    public RightDrawerlayoutView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initViews();
    }

    private void initViews() {
        menuFilterEntities = new ArrayList<>();
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.layout_drawer_right, this);
        idRecyclerViewFilter = rootView.findViewById(R.id.id_recyclerView_filter);
        tvClearFilter = rootView.findViewById(R.id.tv_clear_filter);
        tvRegister = rootView.findViewById(R.id.tv_register);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerViewFilter.setLayoutManager(layoutManager);
        idRecyclerViewFilter.setAdapter(getAdapter(null));
        ((IAdapter<MenuFilterEntity>) idRecyclerViewFilter.getAdapter()).setData(menuFilterEntities);
        idRecyclerViewFilter.getAdapter().notifyDataSetChanged();
    }

    private CommonRcvAdapter<MenuFilterEntity> getAdapter(final List<MenuFilterEntity> data) {
        return new CommonRcvAdapter<MenuFilterEntity>(data) {

            @Override
            public Object getItemType(MenuFilterEntity demoModel) {
                return "";
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new MenuFilterAdapter(getContext(), new MenuFilterAdapter.OnItemSelClicklistener() {
                    @Override
                    public void onItemClick(int position) {

                    }
                });
            }
        };
    }

    public void initData(List<MenuFilterEntity> menuFilterEntitiesIn) {
        menuFilterEntities.clear();
        menuFilterEntities.addAll(menuFilterEntitiesIn);
        idRecyclerViewFilter.getAdapter().notifyDataSetChanged();
    }
}
