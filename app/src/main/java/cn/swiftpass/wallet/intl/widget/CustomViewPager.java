package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Scroller;

import androidx.viewpager.widget.ViewPager;

import java.lang.reflect.Field;

/**
 * 解决部分手机安卓系统BUG Java.lang.IllegalArgumentException: pointerIndex out of range
 * Created by yangyong on 2017/7/14.
 */

public class CustomViewPager extends ViewPager {

    private float x1 = 0;
    private float x2 = 0;
    private float y1 = 0;
    private float y2 = 0;

    private boolean touchException;
    private boolean isScroll = true;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * 让动画慢一点
     */
    public void setViewPagerScrollerTime(final boolean scrollSlowly) {
        try {
            Field scrollerField = ViewPager.class.getDeclaredField("mScroller");
            scrollerField.setAccessible(true);
            Field interpolator = ViewPager.class.getDeclaredField("sInterpolator");
            interpolator.setAccessible(true);
            Scroller scroller = new Scroller(getContext(), (android.view.animation.Interpolator) interpolator.get(null)) {
                @Override
                public void startScroll(int startX, int startY, int dx, int dy, int duration) {
                    int slow = scrollSlowly ? 4 : 1;
                    super.startScroll(startX, startY, dx, dy, duration * slow);// 这里是关键，将duration变长或变短
                }
            };
            scrollerField.set(this, scroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (isScroll) {
            try {
                return super.onTouchEvent(ev);
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            return true;
        }
    }

    public void setTouchException(boolean touchException) {
        this.touchException = touchException;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (isScroll) {
            try {
                if (touchException) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        x1 = event.getX();
                        y1 = event.getY();
                    } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        x2 = event.getX();
                        y2 = event.getY();
                        // 当 view.requestDisallowInterceptTouchEvent 参数为true的时候view 不会拦截其子控件的 触摸事件
                        if (Math.abs(y1 - y2) > 5 && Math.abs(x2 - x1) < 10) {
                            getParent().requestDisallowInterceptTouchEvent(false);
                        } else {
                            getParent().requestDisallowInterceptTouchEvent(true);
                        }
                    }
                }
                return super.onInterceptTouchEvent(event);
            } catch (Exception ex) {
                ex.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 是否可以左右滑动切换viewpager
     *
     * @param scroll
     */
    public void setCanScroll(boolean scroll) {
        isScroll = scroll;
    }

}

