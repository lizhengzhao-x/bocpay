package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class RedpacketPopResourceEntity extends BaseEntity {
    /**
     * bocpayRPImageUrl : https://ewasit3.ftcwifi.com/images/en_US/red_packet_pop_img.gif
     * bocpayRPMusicUrl : https://ewasit3.ftcwifi.com/json/lish.mp3
     * staffRPImageUrl : https://ewasit3.ftcwifi.com/images/en_US/red_packet_pop_img.gif
     * staffRPMusicUrl : https://ewasit3.ftcwifi.com/json/lish.mp3
     */
    private String bocpayRPImageUrl;
    private String bocpayRPMusicUrl;
    private String staffRPImageUrl;
    private String staffRPMusicUrl;

    public String getBocpayRPImageUrl() {
        return bocpayRPImageUrl;
    }

    public void setBocpayRPImageUrl(String bocpayRPImageUrl) {
        this.bocpayRPImageUrl = bocpayRPImageUrl;
    }

    public String getBocpayRPMusicUrl() {
        return bocpayRPMusicUrl;
    }

    public void setBocpayRPMusicUrl(String bocpayRPMusicUrl) {
        this.bocpayRPMusicUrl = bocpayRPMusicUrl;
    }

    public String getStaffRPImageUrl() {
        return staffRPImageUrl;
    }

    public void setStaffRPImageUrl(String staffRPImageUrl) {
        this.staffRPImageUrl = staffRPImageUrl;
    }

    public String getStaffRPMusicUrl() {
        return staffRPMusicUrl;
    }

    public void setStaffRPMusicUrl(String staffRPMusicUrl) {
        this.staffRPMusicUrl = staffRPMusicUrl;
    }
}
