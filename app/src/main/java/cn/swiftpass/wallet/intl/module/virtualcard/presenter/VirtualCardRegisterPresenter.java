package cn.swiftpass.wallet.intl.module.virtualcard.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.VirtualcardRegisterVerifyBocProtocol;
import cn.swiftpass.wallet.intl.module.virtualcard.contract.RegisterVitualCardVerifyIdCardContract;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VitualCardIsBocEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualCardRegisterPresenter extends BasePresenter<RegisterVitualCardVerifyIdCardContract.View> implements RegisterVitualCardVerifyIdCardContract.Presenter {
    @Override
    public void virtualCardJudgeIsBocCustomer(VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new VirtualcardRegisterVerifyBocProtocol(virtualcardRegisterVerifyEntity, new NetWorkCallbackListener<VitualCardIsBocEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardJudgeIsBocCustomerError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(VitualCardIsBocEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().virtualCardJudgeIsBocCustomerSuccess(response);
                }
            }
        }).execute();
    }
}
