package cn.swiftpass.wallet.intl.module.redpacket.view;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.jay.widget.StickyHeadersLinearLayoutManager;
import com.scwang.smart.refresh.layout.SmartRefreshLayout;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;

import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.BottomDividerItemDecoration;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.module.redpacket.contract.PaleyShiContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.module.redpacket.presenter.PaleyShiPresenter;
import cn.swiftpass.wallet.intl.module.redpacket.view.adapter.RedPacketHistoryAdapter;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ContactCompareUtils;

/**
 * 派利是
 */
public class PaleyShiFragment extends BaseFragment<PaleyShiContract.Presenter> implements PaleyShiContract.View {
    @BindView(R.id.main_refreshLayout)
    SmartRefreshLayout mainRefreshLayout;
    @BindView(R.id.rcv_lishi_history)
    RecyclerView rcvLishiHistory;


    RedPacketHistoryAdapter redPacketHistoryAdapter;

    PaleyShiHomeEntity mPaleyShiHomeEntity;

    //private View emptyView;
    private UpdateTvTabDot mUpdateTvTabDot;
    /**
     * 是否需要初始化的时候刷新数据
     */
    private boolean isNeedLoad = true;


    public static PaleyShiFragment newInstance(UpdateTvTabDot mUpdateTvTabDot) {
        return PaleyShiFragment.newInstance(mUpdateTvTabDot, true);
    }

    public static PaleyShiFragment newInstance(UpdateTvTabDot mUpdateTvTabDot, boolean isNeedLoad) {
        PaleyShiFragment paleyShiFragment = new PaleyShiFragment();
        paleyShiFragment.mUpdateTvTabDot = mUpdateTvTabDot;
        paleyShiFragment.isNeedLoad = isNeedLoad;
        return paleyShiFragment;
    }

    @Override
    protected boolean isResetTitleByInit() {
        return false;
    }

    @Override
    public void initTitle() {
        redPacketHistoryAdapter = new RedPacketHistoryAdapter(getActivity());
        rcvLishiHistory.setAdapter(redPacketHistoryAdapter);
    }


    @Override
    protected PaleyShiContract.Presenter loadPresenter() {
        return new PaleyShiPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_paleyshi;
    }

    @Override
    protected void initView(View v) {
        if (null == getActivity()) {
            return;
        }
        StickyHeadersLinearLayoutManager layoutManager = new StickyHeadersLinearLayoutManager<RedPacketHistoryAdapter>(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcvLishiHistory.setLayoutManager(layoutManager);
        BottomDividerItemDecoration itemDecoration = BottomDividerItemDecoration.createVertical(mContext.getColor(R.color.app_white), AndroidUtils.dip2px(mContext, 30f));
        rcvLishiHistory.addItemDecoration(itemDecoration);

        mainRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                //清除通讯录
                ContactCompareUtils.clearAllContactList();
                mPresenter.getPaleyShiHomeData();
            }
        });

        if (isNeedLoad) {
            autoRefreshList();
        }
    }


    public void autoRefreshList() {
        if (mainRefreshLayout == null) {
            return;
        }
        mainRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mPresenter.getPaleyShiHomeData();
            }
        });
    }

    private void stopRefreshList() {
        if (mainRefreshLayout == null) {
            return;
        }
        mainRefreshLayout.finishRefresh();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if (null == mPaleyShiHomeEntity) {
            return;
        }

        redPacketHistoryAdapter.setPaleyShiHome(mPaleyShiHomeEntity);


        if (null != mPaleyShiHomeEntity.getSendRecords()) {
            ContactCompareUtils.compareHisotryLocalRecord(getActivity(), mPaleyShiHomeEntity.getSendRecords(), new ContactCompareUtils.CompareRedPacketsListResult() {
                @Override
                public void getRedPackets(List<RedPacketBeanEntity> redPacketBeanEntities) {
                    if (null != redPacketHistoryAdapter) {
                        redPacketHistoryAdapter.setDataList(redPacketBeanEntities);
                        redPacketHistoryAdapter.notifyDataSetChanged();
                    }

                }
            });
        }


        if (null != mUpdateTvTabDot) {
            mUpdateTvTabDot.upDateTvTabDot(mPaleyShiHomeEntity.getReceivedRedPacketNum());
        }

    }


    /**
     * tab 在没有网络上点击会显示弹框
     */
    public void tabClickFresh() {
        if (null == mPaleyShiHomeEntity && null != mPresenter) {
            mPresenter.getPaleyShiHomeData();
        }
    }


    @Override
    public void getPaleyShiHomeDataSuccess(PaleyShiHomeEntity response) {
        stopRefreshList();
        mPaleyShiHomeEntity = response;
        initData();
        mainRefreshLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void getPaleyShiHomeDataError(String errorCode, String errorMsg) {
        stopRefreshList();
        showErrorMsgDialog(getContext(), errorMsg);
    }

}

