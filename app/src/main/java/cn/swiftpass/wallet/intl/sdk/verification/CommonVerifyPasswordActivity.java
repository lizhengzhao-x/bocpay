package cn.swiftpass.wallet.intl.sdk.verification;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.base.fio.FIODispatcher;
import cn.swiftpass.wallet.intl.base.fio.FIOResultView;
import cn.swiftpass.wallet.intl.dialog.CustomMsgDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentPasswordDialogWithFioFragment;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.FIOActivity;
import cn.swiftpass.wallet.intl.sdk.fingercore.MyFingerPrintManager;

/**
 * Created by ZhangXinchao on 2019/11/28.
 * 登录态的忘记密码 非登录态暂时用SelectPopupWindow 后期要优化
 * 验证支付密码 指纹模块/密码模块通用处理
 * 暂时用 static onPwdVerifyCallBack callback
 * setResult 有可能在fragment拿不到回调出问题
 */
public class CommonVerifyPasswordActivity extends BaseCompatActivity<CommonVerifyPasswordContract.Presenter> implements FIOResultView,CommonVerifyPasswordContract.View {


    public static VerifyPasswordCommonManager builder;
    private  OnPwdVerifyCallBack onPwdVerifyCallBack;
    private  OnPwdVerifyWithMajorScanCallBack onPwdVerifyWithMajorScanCallBack;
    private  boolean isSupportFingerPrint;

    private PaymentPasswordDialogWithFioFragment paymentPasswordDialogFragment;
    private  String mCurrentAction;
    private  boolean isDisMissForForgetPwd = false;



    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    private Handler mPwdHandler;



    @Override
    public void init() {
        if (builder!=null){
             mCurrentAction= builder.getAction();
            onPwdVerifyCallBack=builder.getOnPwdVerifyCallBack();
            isSupportFingerPrint=builder.isSupportFingerPrint();
            isDisMissForForgetPwd =builder.isDismissForForgetPwd();
            if (builder.getOnPwdVerifyWithMajorScanCallBack()!=null){
                onPwdVerifyWithMajorScanCallBack=builder.getOnPwdVerifyWithMajorScanCallBack();
            }
        }else {
            finish();
        }

        EventBus.getDefault().register(this);
        verifyPwdAction();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_fio_common;
    }

    private void verifyPwdAction() {
        if (MyFingerPrintManager.getInstance().isRegisterFIO(getActivity()) && !MyFingerPrintManager.getInstance().isChangeFIO(getActivity()) && isSupportFingerPrint && CacheManagerInstance.getInstance().isOpenBiometricAuth()) {
            FIOActivity.startAuthFingerPrint(getActivity());
        } else {
            initPwdWindow();
        }

    }

    private void initPwdWindow() {
        paymentPasswordDialogFragment = new PaymentPasswordDialogWithFioFragment();
        PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogWithFioFragment.OnPwdDialogClickListener() {
            @Override
            public void onPwdCompleteListener(String psw, boolean complete) {
                if (complete) {
                    mPresenter.getCheckPasswordWithAction(psw,mCurrentAction);
                }
            }

            @Override
            public void onPwdBackClickListener() {
                //内存释放问题
                CommonVerifyPasswordActivity.this.finish();
                mPwdHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (onPwdVerifyCallBack != null) {
                            onPwdVerifyCallBack.onVerifyCanceled();
                        }
                        onPwdVerifyCallBack = null;
                    }
                }, 300);

            }

            @Override
            public void onPwdFailed(String errorCode, String errorMsg) {
                showErrorDialogMsgDialog(errorMsg);
            }
        };
        paymentPasswordDialogFragment.initParams(onPwdDialogClickListener, isDisMissForForgetPwd);
        if (getActivity()!=null&&!getActivity().isFinishing()){
            paymentPasswordDialogFragment.show(getSupportFragmentManager(), "PaymentPasswordDialogWithFioFragment");
        }
    }

    private void showErrorDialogMsgDialog(String errorMsg) {
        try {
            CustomMsgDialogFragment dialogFragment = new CustomMsgDialogFragment();
            dialogFragment.setCanceledOnTouchOutside(false);
            dialogFragment.setMessage(errorMsg);
            dialogFragment.setCancelable(false);

            FragmentManager fm = getSupportFragmentManager();
            if (fm != null) {
                fm.executePendingTransactions();
                if (!dialogFragment.isAdded()) {
                    dialogFragment.show(fm, "CustomMsgDialogFragment");
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_CLOSE_VERIFY_PASSWORD) {
            finish();
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        FIODispatcher.requestCallBack(this, requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onFioSuccess(int requestCode, int resultCode, Intent data) {
        verifySuccessResult();
    }

    @Override
    public void onFioFail(int requestCode, int resultCode, Intent data) {
        initPwdWindow();
    }



    private void verifySuccessResult() {
        finish();
        if (onPwdVerifyCallBack != null) {
            onPwdVerifyCallBack.onVerifySuccess();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (onPwdVerifyWithMajorScanCallBack != null) {
            onPwdVerifyWithMajorScanCallBack.onActivityResume();
        }
    }

    @Override
    protected void onDestroy() {
        dismissDialog();
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.dismiss();
        }
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPwdHandler = new Handler();
    }



    @Override
    public void finish() {
        overridePendingTransition(0, 0);
        super.finish();
    }

    @Override
    protected CommonVerifyPasswordContract.Presenter createPresenter() {
        return new CommonVerifyPasswordPresenter();
    }


    @Override
    public void getCheckPasswordWithActionSuccess(BaseEntity response) {
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.clearPwd();
            if (getActivity()!=null&&!getActivity().isFinishing()) {
                paymentPasswordDialogFragment.dismiss();
            }
        }
        verifySuccessResult();
    }

    @Override
    public void getCheckPasswordWithActionError(String errorCode, String errorMsg) {
        showErrorDialogMsgDialog(errorMsg);
        if (paymentPasswordDialogFragment != null) {
            paymentPasswordDialogFragment.clearPwd();
        }
    }
}
