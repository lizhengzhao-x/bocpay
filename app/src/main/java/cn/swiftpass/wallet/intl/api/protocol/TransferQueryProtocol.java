package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/20
 * 收款转账查询接口
 */

public class TransferQueryProtocol extends BaseProtocol {
    public static final String TAG = TransferQueryProtocol.class.getSimpleName();
    //内部订单号
    private String scrRefNo;

    //交易唯一标识
    private String txnId;


    public TransferQueryProtocol(String scrRefNo, String txnId, NetWorkCallbackListener callback) {
        this.scrRefNo = scrRefNo;
        this.txnId = txnId;
        mDataCallback = callback;
        mUrl = "api/transfer/transferResultEnquiry";
    }

    @Override
    public void packData() {
        mBodyParams.put(RequestParams.TRANSFER_ORDER_NUM, scrRefNo);
        mBodyParams.put(RequestParams.TXNID, txnId);
    }
}
