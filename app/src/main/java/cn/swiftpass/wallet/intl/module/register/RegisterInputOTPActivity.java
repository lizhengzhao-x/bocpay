package cn.swiftpass.wallet.intl.module.register;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.CardsEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.CommonRequestUtils;
import cn.swiftpass.wallet.intl.app.constants.FioConst;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.dialog.PaymentPasswordDialogFragment;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.BankLoginResultEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ForgetpwdEntity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordsEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtpEntity;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.LoginSendOtpEntity;
import cn.swiftpass.wallet.intl.entity.NormalLoginSucEntity;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.RegSucEntity;
import cn.swiftpass.wallet.intl.entity.RegisterAccountEntity;
import cn.swiftpass.wallet.intl.entity.SendCreditOTPEntity;
import cn.swiftpass.wallet.intl.entity.VerifyOTPEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.LoginEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PWidEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.EditPwdDialogEvent;
import cn.swiftpass.wallet.intl.event.JumpCenterNotificationEvent;
import cn.swiftpass.wallet.intl.event.RegisterPopBannerDialogEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.manage.OtpCountDownManager;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.BindCardSuccessActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsBankRecordsActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsBindSucActivity;
import cn.swiftpass.wallet.intl.module.register.contract.OtpAction;
import cn.swiftpass.wallet.intl.module.register.contract.OtpContract;
import cn.swiftpass.wallet.intl.module.register.presenter.OtpPresenter;
import cn.swiftpass.wallet.intl.module.setting.paymentsetting.ResetPwdSuccessActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatManager;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisErrorEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisPageEntity;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisParams;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AdvancedCountdownTimer;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DataCollectManager;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;

public class RegisterInputOTPActivity extends BaseCompatActivity<OtpContract.Presenter> implements OtpContract.View {

    @BindView(R.id.tv_smsCode)
    TextView tvSmsCode;
    @BindView(R.id.etwd_otp_code)
    EditTextWithDel etwdOtpCode;
    @BindView(R.id.id_send_msg)
    TextView tvSendMsg;
    @BindView(R.id.id_sub_title)
    TextView idSubTitle;
    @BindView(R.id.tv_one_time_title)
    TextView mOneTimeTitle;

    /**
     * PWID_ERROR_CODE 智障人士错误码
     */
    public static final String PWID_ERROR_CODE = "EWA5628";

    /**
     * 跳转type 1.注册 2.绑卡 3.登录
     */
    private int mPageFlow;

    private String cardNo;
    private String phoneNo;
    private String walletId;
    private String expiryDate;
    private String cardID;
    private String mAccountType = Constants.ACCOUNT_TYPE_CREDIT;
    private String mAccount = "";
    private String smart_action;
    private BankLoginResultEntity smartInfo;
    /**
     * 添加FPS Bank时的参数
     */
    private FpsPreCheckEntity mFpsPreCheckAdd;
    private FpsBankRecordBean mFpsBankRecord;
    private PaymentPasswordDialogFragment mPaymentPasswordDialogFragment;


    VerifyOTPEntity mVerifyOTPEntity;

    private String mSendAgainTip;
    /**
     * 进入该页面的前一个页面
     */
    private String lastAddress;

    private String mCurrentCardId;

    public static final String LOGIN_OTHER_DEVICE_YES = "Y";

    public void setResetTime(boolean isReset, String phoneNo) {
        if (isReset) {
            tvSendMsg.setClickable(false);
            tvSendMsg.setEnabled(false);
            AdvancedCountdownTimer.getInstance().countDownEvent(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setText(String.format(getString(R.string.IDV_3a_20_4_a), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                    tvSendMsg.setClickable(false);
                    tvSendMsg.setEnabled(false);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(mSendAgainTip);
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        } else {
            AdvancedCountdownTimer.getInstance().startCountDown(phoneNo, OtpCountDownManager.getInstance().getOtpSendIntervalTime().intValue(), new AdvancedCountdownTimer.OnCountDownListener() {
                @Override
                public void onTick(int millisUntilFinished) {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setText(String.format(getString(R.string.IDV_3a_20_4_a), millisUntilFinished + ""));
                    tvSendMsg.setTextColor(getResources().getColor(R.color.home_title_gray_color));
                    tvSendMsg.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    if (tvSendMsg == null) return;
                    tvSendMsg.setClickable(true);
                    tvSendMsg.setEnabled(true);
                    tvSendMsg.setText(mSendAgainTip);
                    tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                }
            });
        }
    }

    private void sendErrorEvent(String errorCode, String errorMsg) {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            AnalysisErrorEntity analysisErrorEntity = new AnalysisErrorEntity(startTime, errorCode, errorMsg, PagerConstant.ONE_TIME_PASSCODE);
            sendAnalysisEvent(analysisErrorEntity);
        }
    }

    @Override
    public boolean isAnalysisPage() {
        if (mPageFlow == Constants.PAGE_FLOW_BIND_PA || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            return true;
        }
        return super.isAnalysisPage();
    }

    @Override
    public AnalysisPageEntity getAnalysisPageEntity() {
        AnalysisPageEntity analysisPageEntity = new AnalysisPageEntity();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            analysisPageEntity.last_address = PagerConstant.PAYMENT_PASSCODE_SETTING;
        } else {
            analysisPageEntity.last_address = lastAddress;
        }
        analysisPageEntity.current_address = PagerConstant.ONE_TIME_PASSCODE;
        return analysisPageEntity;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Override
    protected OtpContract.Presenter createPresenter() {
        return new OtpPresenter();
    }

    @Override
    public void init() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        mHandler = new Handler();
        mSendAgainTip = getString(R.string.IDV_3a_20_5);

        tvSendMsg.setVisibility(View.INVISIBLE);
        cardNo = getIntent().getExtras().getString(Constants.DATA_CARD_NUMBER);
        phoneNo = getIntent().getExtras().getString(Constants.DATA_PHONE_NUMBER);
        walletId = getIntent().getExtras().getString(Constants.WALLET_ID);
        expiryDate = getIntent().getExtras().getString(Constants.EXDATE);
        cardID = getIntent().getExtras().getString(Constants.CARDID);
        mAccountType = getIntent().getExtras().getString(Constants.ACCOUNT_TYPE);
        mAccount = getIntent().getExtras().getString(Constants.USER_ACCOUNT);

        tvSmsCode.setText(AndroidUtils.formatCenterPhoneNumberStr(phoneNo));
        etwdOtpCode.hideErrorView();
        etwdOtpCode.setEditTextGravity(Gravity.CENTER);
        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        lastAddress = getIntent().getExtras().getString(AnalysisParams.CURRENT_ADDRESS);
        //fps/邮箱P3_A1_22_2  其他都是新的词条IDV_3a_20_3
        if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
            setToolBarTitle(R.string.setting_payment_fgw);
            idSubTitle.setText(R.string.IDV_3a_20_3);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            setToolBarTitle(R.string.IDV_3_1a);
        } else if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
            setToolBarTitle(R.string.button_login);
            tvSmsCode.setVisibility(View.VISIBLE);
            idSubTitle.setText(R.string.IDV_3a_20_3);
        } else if (mPageFlow == Constants.DATA_UPDATE_DAILY_LIMIT) {
            setToolBarTitle(R.string.CR2101_16_1);
            tvSmsCode.setVisibility(View.VISIBLE);
            idSubTitle.setText(R.string.IDV_3a_20_3);
            smart_action = getIntent().getExtras().getString(Constants.DATA_SMART_ACTION);
        } else if (mPageFlow == Constants.DATA_TYPE_SET_FIO) {
            setToolBarTitle(R.string.IDV_32_1);
            tvSmsCode.setVisibility(View.VISIBLE);
            idSubTitle.setText(R.string.IDV_3a_20_3);
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_ADD) {
            initFpsAdd();
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_QUERY) {
            initFpsBankQuery();
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            setToolBarTitle(R.string.IDV_2_1);
            DataCollectManager.getInstance().sendOtp();
            idSubTitle.setText(R.string.IDV_3a_20_3);
            hideBackIcon();
            setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);

            getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AndroidUtils.showConfrimExitDialog(RegisterInputOTPActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskWithRegister();
                            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                                ChatSDKManager.hide();
                            }
                        }
                    });
                }
            });
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
            //s2用户充值绑定银行卡
            setToolBarTitle(R.string.IDV_3_1a);
            idSubTitle.setText(R.string.IDV_3a_20_3);
            hideBackIcon();
            setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);

            getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AndroidUtils.showConfrimExitDialog(RegisterInputOTPActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskWithPaBindCard();
                            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                                ChatSDKManager.hide();
                            }
                        }
                    });
                }
            });

        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            setToolBarTitle(R.string.setting_payment_fgw);
            idSubTitle.setText(R.string.IDV_3a_20_3);
            hideBackIcon();
            setToolBarRightViewToImage(R.mipmap.icon_nav_back_close);

            getToolBarRightView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AndroidUtils.showConfrimExitDialog(RegisterInputOTPActivity.this, getString(R.string.IDV_28_2), new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            if (CacheManagerInstance.getInstance().isLogin()) {
                                //登录态忘记密码
                                MyActivityManager.removeAllTaskForForgetPwdWithLoginStatusMainStack();
                            } else {
                                //登出态忘记密码
                                MyActivityManager.removeAllTaskWithForgetNoLoginStatusPwd();
                            }
                        }
                    });
                }
            });


        } else {
            setToolBarTitle(R.string.IDV_2_1);
            idSubTitle.setText(R.string.IDV_3a_20_3);
            //除了Pa 所有注册点击back 都返回登录界面
            setBackBtnListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    AndroidUtils.showConfrimExitDialog(RegisterInputOTPActivity.this, new AndroidUtils.ConfirmClickListener() {
                        @Override
                        public void onConfirmBtnClick() {
                            MyActivityManager.removeAllTaskExcludeMainStackForRegister();
//                            ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, PreLoginActivity.class);
                        }
                    });
                }
            });
        }

        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                initSDKListener();
            }
        }
        //监听验证码是否够6位 去校验otp接口
        etwdOtpCode.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (etwdOtpCode.getEditText().getText().toString().trim().length() == 8) {
                    //验证码够长度之后再接口未调用成功之后
                    etwdOtpCode.getmDelImageView().setEnabled(false);
                    setBackBtnEnable(false);
                    AndroidUtils.hideKeyboard(etwdOtpCode.getEditText());
                    //软键盘 dialog 位置晃动
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            verfiyOtpRequest();
                        }
                    }, 200);
                } else {
                    setBackBtnEnable(true);
                    etwdOtpCode.getmDelImageView().setEnabled(true);
                }
            }
        });


        tvSendMsg.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                sendOtpRequest(true);
            }
        });
        etwdOtpCode.getEditText().setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                /*判断是否是“GO”键*/
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    return true;
                } else if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /*判断是否是“DONE”键*/
                    if (etwdOtpCode.getEditText().getText().toString().trim().length() == 8) {
                        verfiyOtpRequest();
                    }
                    return true;
                }
                return false;
            }
        });
        sendOtpRequest(false);
    }


    public void initSDKListener() {
        EGXChatProtocol.AppWatcher appWatcher = OnLineChatManager.getInstance().getLoginAppWatcher(this);
        ChatSDKManager.setChatWatcher(appWatcher);
        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_REGISTER, OnLineChatConfig.PAGEID_REGISTER_SENDOTP);
        ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
    }


    @Override
    public void sendOtpSuccess(int action, ContentEntity entity, boolean isTryAgain) {
        switch (action) {
            case OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT:
            case OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER:
            case OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD:
                setResetTime(true, phoneNo);
                //如果是重发 弹窗提示
                if (isTryAgain) {
                    showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                        @Override
                        public void onBtnClickListener() {
                            clearOtpInput();
                        }
                    });
                } else {
                    clearOtpInput();
                }
                break;
            default:
                break;
        }


    }

    @Override
    public void sendOtpError(int action, String errorCode, String errorMsg, boolean isTryAgain) {
        switch (action) {
            case OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT:
            case OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER:
            case OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD:
                sendErrorEvent(errorCode, errorMsg);
                onOtpActionFailed(errorCode, errorMsg, true);
                break;
            default:
                break;
        }
    }


    @Override
    public void verifyOtpSuccess(int action, RegisterAccountEntity response) {
        switch (action) {
            case OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT:
                reloadBankcards();
                RegisterAccountEntity registerAccountEntity = new RegisterAccountEntity();
                registerAccountEntity.setEmail(response.getEmail());
                registerAccountEntity.setMobile(response.getMobile());
                registerAccountEntity.setIsAudit(response.getIsAudit());
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                mHashMaps.put(Constants.REGISTER_SUCCESS_DATA, registerAccountEntity);
                ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, STwoRegisterSuccessActiviy.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                break;
            case OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER:
                goS2RegisterSuccessPage(response);
                break;
            default:
                break;
        }
    }

    @Override
    public void verifyOtpError(int action, String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, false);
    }


    @Override
    public void verifyOtpWithPwdError(int action, String errorCode, String errorMsg) {
        switch (action) {
            case OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD:
                sendErrorEvent(errorCode, errorMsg);
                onOtpActionFailed(errorCode, errorMsg, false);
                break;
            default:
                break;
        }
    }

    @Override
    public void verifyOtpWithPwdSuccess(int action, ForgetpwdEntity response) {
        switch (action) {
            case OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD:
                if (CacheManagerInstance.getInstance().checkLoginStatus() && !TextUtils.isEmpty(response.getsId())) {
                    HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
                }
                goSetPwdPage();
                break;
            default:
                break;
        }
    }


    @Override
    public void sendCreditOtpSuccess(SendCreditOTPEntity response, boolean isTryAgain) {
        mAccount = response.getAccount();
        setResetTime(true, phoneNo);
        clearOtpInput();
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        }
    }

    @Override
    public void sendCreditOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, true);
    }


    /**
     * s2用户充值绑定银行卡
     */
    private void sTwoUserBindBankAccountSendOtp(final boolean isTryAgain) {
        mPresenter.sendOtp(OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT, isTryAgain);
    }

    /**
     * s2用户绑定银行卡
     */
    private void sTwoUserBindBankAccountVerifyOtp(String otpNo) {
        mPresenter.verifyOtp(OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT, otpNo, "2");
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.hide();
            }
        }
    }

    /**
     * s2用户注册设置密码之后发送otp
     */
    private void s2RegisterSendOtp(final boolean isTryAgain) {
        mPresenter.sendOtp(OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER, isTryAgain);
    }

    /**
     * s2用户注册设置密码之后校验otp
     */
    private void verifyOtpS2Register(String otpNo) {
        mPresenter.verifyOtp(OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER, otpNo, "2");
    }


    /**
     * s2用户忘记密码发送otp
     */
    private void s2ForgetPasswordSendOtp(final boolean isTryAgain) {
        mPresenter.sendOtp(OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD, isTryAgain);
    }

    /**
     * s2用户忘记密码
     */
    private void verifyForgetPwdRegister(String otpNo, String pwd) {
        mPresenter.verifyOtpWithPwd(OtpAction.OTP_SMART_ACCOUNT_TWO_FORGET_PASSWORD, otpNo, pwd);
    }

    private void goSetPwdPage() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        ActivitySkipUtil.startAnotherActivity(this, ResetPwdSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void initFpsAdd() {
        setToolBarTitle(R.string.TME1_2_1);
        tvSmsCode.setVisibility(View.VISIBLE);
        if (null != getIntent()) {
            mFpsPreCheckAdd = (FpsPreCheckEntity) getIntent().getSerializableExtra(FpsConst.FPS_PRE_CHECK_ENTITY_ADD);
            mFpsBankRecord = (FpsBankRecordBean) getIntent().getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
        }
        if (mFpsPreCheckAdd == null || null == mFpsBankRecord) {
            finish();
        } else {
            if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mFpsBankRecord.getAccountIdType())) {
                tvSmsCode.setText(AndroidUtils.fpsEmailFormat(mFpsBankRecord.getAccountId()));

                mOneTimeTitle.setText(getString(R.string.TME1_4_2));
                mSendAgainTip = getString(R.string.TME1_2_4);
                idSubTitle.setText(R.string.TME1_2_3);
            } else {
                tvSmsCode.setText(AndroidUtils.fpsPhoneFormat(mFpsBankRecord.getAccountId()));
                mSendAgainTip = getString(R.string.TME1_5_4);
                mOneTimeTitle.setText(getString(R.string.TME1_5_2));
                idSubTitle.setText(R.string.TME1_3_3);
            }
        }
    }

    private void initFpsBankQuery() {
        setToolBarTitle(R.string.TME1_5_1);
        tvSmsCode.setVisibility(View.VISIBLE);
        if (null != getIntent()) {
            mFpsBankRecord = (FpsBankRecordBean) getIntent().getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
        }
        if (null == mFpsBankRecord) {
            finish();
        } else {
            if (TextUtils.equals(mFpsBankRecord.getAccountIdType(), FpsConst.ACCOUNT_ID_TYPE_EMAIL)) {
                tvSmsCode.setText(AndroidUtils.fpsEmailFormat(mFpsBankRecord.getAccountId()));

                mOneTimeTitle.setText(getString(R.string.TME1_4_2));
                mSendAgainTip = getString(R.string.TME1_2_4);

                idSubTitle.setText(R.string.TME1_2_3);
            } else {
                tvSmsCode.setText(AndroidUtils.fpsPhoneFormat(mFpsBankRecord.getAccountId()));

                mOneTimeTitle.setText(getString(R.string.TME1_5_2));
                mSendAgainTip = getString(R.string.TME1_5_4);

                idSubTitle.setText(R.string.TME1_3_3);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        showKeyBoard();
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            if (BuildConfig.INCLUDE_ONLINE_CHAT) {
                ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 2);
            }
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }


    /**
     * 弹出软键盘
     */
    private void showKeyBoard() {
        if (mHandler == null) return;
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (null == etwdOtpCode) {
                    return;
                }
                etwdOtpCode.getEditText().setFocusable(true);
                etwdOtpCode.getEditText().requestFocus();
                AndroidUtils.showKeyboardView(etwdOtpCode.getEditText());
            }
        }, 1000);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_register_input_otp;
    }


    @OnClick({R.id.tv_smsCode, R.id.tv_OTP_nextPage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_smsCode:
                break;
            case R.id.tv_OTP_nextPage:
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
                mHashMaps.put(Constants.WALLET_ID, walletId);
                mHashMaps.put(Constants.CARDID, cardID);
                mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
                ActivitySkipUtil.startAnotherActivity(this, RegisterSetPaymentPswActivity.class, mHashMaps, null);
                break;
            default:
                break;
        }
    }

    /**
     * 如果短信发送成功 用户点击back退出 弹框提示确认
     */
    private void showConfrimExitDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(RegisterInputOTPActivity.this);
        builder.setTitle(getString(R.string.register_otp_title));
        builder.setMessage(getString(R.string.register_otp_disable_message));
        builder.setPositiveButton(getString(R.string.dialog_right_btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER || mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
                showConfrimExitDialog();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 发送otp
     */
    private void sendOtpRequest(boolean isTryAgain) {

        String typeFlow = AndroidUtils.getFlowFromType(mPageFlow);
        if (mPageFlow == Constants.DATA_TYPE_SET_FIO) {
            if (TextUtils.equals(Constants.ACCOUNT_TYPE_CREDIT, mAccountType)) {
                sendCreditOtp(typeFlow, isTryAgain);
            } else {
                sendFioOtp(isTryAgain);
            }
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_ADD) {
            sendFpsAddOtp(isTryAgain);
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_QUERY) {
            sendFpsQueryOtp(isTryAgain);
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            s2RegisterSendOtp(isTryAgain);
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            s2ForgetPasswordSendOtp(isTryAgain);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
            sTwoUserBindBankAccountSendOtp(isTryAgain);
        } else if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
            //登录
            sendLoginOtp(isTryAgain);
        } else {
            if (TextUtils.equals(Constants.ACCOUNT_TYPE_CREDIT, mAccountType)) {
                sendCreditOtp(typeFlow, isTryAgain);
            } else {
                sendSmartOtp(typeFlow, isTryAgain);
            }
        }
    }

    private void sendLoginOtp(boolean isTryAgain) {
        if (mPresenter != null) {
            mPresenter.loginSendOtp(phoneNo, isTryAgain);
        }
    }

    private void sendFpsAddOtp(boolean isTryAgain) {
        sendFpsOtp(mFpsBankRecord.getAccountId(), mFpsBankRecord.getAccountIdType(), FpsConst.FPS_ACTION_ADD, isTryAgain);
    }

    private void sendFpsQueryOtp(boolean isTryAgain) {
        sendFpsOtp(mFpsBankRecord.getAccountId(), mFpsBankRecord.getAccountIdType(), FpsConst.FPS_ACTION_QUERY, isTryAgain);
    }

    @Override
    public void sendFpsOtpSuccess(FpsOtpEntity response, String accountIdType, boolean isTryAgain) {
        if (null != response && FpsConst.FPS_OTP_DISPLAY_RESEND.equalsIgnoreCase(response.getCountDownFlag())) {
            tvSendMsg.setVisibility(View.VISIBLE);
            tvSendMsg.setClickable(true);
            tvSendMsg.setEnabled(true);
            tvSendMsg.setText(mSendAgainTip);
            tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
        } else {
            setResetTime(true, phoneNo);
        }
        //如果是重发 弹窗提示
        if (isTryAgain) {
            String title = "";
            if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(accountIdType)) {
                title = mContext.getString(R.string.TME1_4_5);
            } else {
                title = mContext.getString(R.string.TME1_5_5);
            }
            showErrorMsgDialog(mContext, title, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void sendFpsOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, true);
    }

    private void sendFpsOtp(String accountId, final String accountIdType, String action, final boolean isTryAgain) {
        String retry = isTryAgain ? FpsConst.FPS_SEND_OTP_AGAIN : FpsConst.FPS_SEND_OTP_DEF;
        mPresenter.sendFpsOtp(accountId, accountIdType, action, retry, isTryAgain);
    }

    @Override
    public void sendFioOtpSuccess(BaseEntity entity, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void sendFioOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        onOtpActionFailed(errorCode, errorMsg, true);
    }


    private void sendFioOtp(final boolean isTryAgain) {
        mPresenter.sendFioOtp(isTryAgain);
    }

    //信用卡账号发送OTP
    private void sendCreditOtp(String typeFlow, final boolean isTryAgain) {
        mPresenter.sendCreditOtp(mAccount, walletId, cardNo, expiryDate, typeFlow, isTryAgain);
    }


    @Override
    public void sendSmartAccountOtpSuccess(BaseEntity entity, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        clearOtpInput();
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        }
    }

    @Override
    public void sendSmartAccountOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, true);
    }


    //智能账号发送OTP
    private void sendSmartOtp(String typeFlow, boolean isTryAgain) {
        mPresenter.sendSmartAccountOtp(typeFlow, walletId, isTryAgain);
    }


    /**
     * 重新拉去一次卡列表
     */
    private void reloadBankcards() {
        CommonRequestUtils.getCardsList(this, HttpCoreKeyManager.getInstance().getUserId(), new NetWorkCallbackListener<CardsEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                sendErrorEvent(errorCode, errorMsg);
                //showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onSuccess(CardsEntity response) {
                ArrayList<BankCardEntity> rows = response.getRows();
                CacheManagerInstance.getInstance().setCardEntities(rows);
            }
        });
    }

    //登录校验密码，不需要指纹
    private void goSetPasscodeAfterOtp() {
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.WALLET_ID, walletId);
        mHashMaps.put(Constants.CARDID, cardID);
        mHashMaps.put(Constants.DATA_CARD_NUMBER, cardNo);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.ACCOUNT_TYPE, mAccountType);
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, RegisterSetPaymentPswActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        //不可退回 之前界面销毁
        finish();
        EventBus.getDefault().post(new RegisterEventEntity(RegisterEventEntity.EVENT_REGISTER, ""));
    }

    //登录校验密码，不需要指纹
    private void goLoginAfterOtp() {
        //验证码够长度之后再接口未调用成功之后
        etwdOtpCode.getmDelImageView().setEnabled(false);
        //延迟显示 部分手机软键盘退出比较慢 屏蔽快速点击事件

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showPasswordDialog();
                etwdOtpCode.getmDelImageView().setEnabled(true);
            }
        }, 200);
    }

    private void showPasswordDialog() {
        mPaymentPasswordDialogFragment = new PaymentPasswordDialogFragment();
        PaymentPasswordDialogFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogFragment.OnPwdDialogClickListener() {
            @Override
            public void onPwdCompleteListener(String psw, boolean complete) {
                if (complete) {
                    if (TextUtils.isEmpty(phoneNo)) {
                        return;
                    }
                    String regions[] = phoneNo.split("-");
                    if (null == regions) {
                        return;
                    }
                    final String region = regions[0];
                    boolean needVerify = false;
                    if (null != mVerifyOTPEntity && TextUtils.equals(mVerifyOTPEntity.getPwdVerifyState(), Constants.LOGIN_VERIFY_STATE_YES)) {
                        needVerify = true;
                    }
                    String actionFlow = AndroidUtils.getFlowFromType(mPageFlow);
                    checkLogin(psw, region, actionFlow, needVerify);
                }
            }

            @Override
            public void onPwdBackClickListener() {
                setBackBtnEnable(true);
            }
        };
        mPaymentPasswordDialogFragment.initParams(onPwdDialogClickListener, false);
        mPaymentPasswordDialogFragment.show(getSupportFragmentManager(), "PaymentPasswordDialogFragment");
    }


    private void clearOtpInput() {
        etwdOtpCode.getEditText().setText("");
        showKeyBoard();
    }


    /**
     * 校验otp
     */
    private void verfiyOtpRequest() {
        String otpNo = etwdOtpCode.getEditText().getText().toString().trim();
        String typeFlow = AndroidUtils.getFlowFromType(mPageFlow);
        if (mPageFlow == Constants.DATA_TYPE_SET_FIO) {
            if (mAccountType.equalsIgnoreCase(Constants.ACCOUNT_TYPE_CREDIT)) {
                verifyOtpCredit(typeFlow, otpNo);
            } else {
                verifyOtpFio(otpNo);
            }
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_ADD) {
            verifyOtpFpsAdd(otpNo);
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_QUERY) {
            verifyOtpFpsQuery(otpNo);
        } else if (mPageFlow == Constants.PAGE_FLOW_REGISTER_PA) {
            //s2用户注册
            verifyOtpS2Register(otpNo);
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD_PA) {
            //s2用户忘记密码
            String pwd = getIntent().getExtras().getString(Constants.ACCOUNT_PASSCODE);
            verifyForgetPwdRegister(otpNo, pwd);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_PA) {
            //s2用户绑卡
            sTwoUserBindBankAccountVerifyOtp(otpNo);
        } else if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
            //登录
            if (mPresenter != null) {
                mPresenter.loginVerifyOtp(phoneNo, "L", mCurrentCardId, false, otpNo);
            }
        } else {
            if (mAccountType.equalsIgnoreCase(Constants.ACCOUNT_TYPE_CREDIT)) {
                verifyOtpCredit(typeFlow, otpNo);
            } else {
                verifyOtpSmart(typeFlow, otpNo);
            }
        }
    }


    /**
     * 支付账户注册成功
     *
     * @param response
     */
    private void goS2RegisterSuccessPage(RegisterAccountEntity response) {
        CacheManagerInstance.getInstance().saveLoginStatus();
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        if (response != null && response.getCards() != null && !response.getCards().getRows().isEmpty()) {
            CacheManagerInstance.getInstance().setCardEntities(response.getCards().getRows());
        }
        HttpCoreKeyManager.getInstance().setUserId(walletId);
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_REGISTER_PA);
        mHashMaps.put(Constants.REGISTER_SUCCESS_DATA, response);
        mHashMaps.put(Constants.REWARDTIP, response.getRewardTip());
//        mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
//        mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
        boolean jumpMessageCenter = false;
        if (!SpUtils.getInstance().hasShowNotificationDialog()) {
            //首次安装，升级
            jumpMessageCenter = response.isJumpToMessageCenterWhenFirstInstallOrUpdate();
        } else {
            jumpMessageCenter = response.isJumpToMessageCenter();
        }
        addRegisterSuccessEvent(response.getNewUserPopUpBanner(), response.isNeedEditPwd(), jumpMessageCenter, getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
        mHashMaps.put(Constants.SHOWINVITEPAGE, response.showInvitePage());
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, STwoRegisterSuccessActiviy.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    //添加fps bank
    private void verifyOtpFpsAdd(String otpNo) {
        verifyOtpFps(otpNo, FpsConst.FPS_ACTION_ADD, mFpsBankRecord.getAccountId(), mFpsBankRecord.getAccountIdType());
    }

    //查询fps other bank
    private void verifyOtpFpsQuery(String otpNo) {
        verifyOtpFps(otpNo, FpsConst.FPS_ACTION_QUERY, mFpsBankRecord.getAccountId(), mFpsBankRecord.getAccountIdType());
    }

//    @Override
//    public void verifyFpsOtpSuccess(BaseEntity entity, String accountId, String accountIdType) {
//        onVerifyFpsOtpSuc(accountId, accountIdType);
//    }

    @Override
    public void verifyFpsOtpError(String errorCode, String errorMsg) {
        onOtpActionFailed(errorCode, errorMsg, false);
    }

    //fps otp 验证
    private void verifyOtpFps(String otpNo, String action, final String accountId, final String accountIdType) {
        mPresenter.verifyFpsOtp(otpNo, action, accountId, accountIdType, mPageFlow);
    }

//    //FPS验证成功
//    private void onVerifyFpsOtpSuc(String accountId, String accountIdType) {
//        mPresenter.fpsQueryBankOtherRecords(accountId, accountIdType);
//    }

    @Override
    public void fpsQueryBankOtherRecordsError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg);
    }

    @Override
    public void fpsQueryBankOtherRecordsSuccess(FpsOtherBankRecordsEntity response) {
        boolean flag = false;
        if (null != response) {
            if ((null != response.getBankChina() && !response.getBankChina().isEmpty()) || (null != response.getData() && !response.getData().isEmpty())) {
                flag = true;
            }
        }
        if (mPageFlow == Constants.DATA_TYPE_FPS_ADD) {
            if (flag) {
                FpsBankRecordsActivity.startActivityNew(getActivity(), mFpsPreCheckAdd, mFpsBankRecord, response);
                finish();
            }
        } else if (mPageFlow == Constants.DATA_TYPE_FPS_QUERY) {
            FpsBankRecordsActivity.startActivityQuery(getActivity(), mFpsBankRecord, response);
            finish();
        }
    }


    //确认FPS
    private void onFpsAddConfirm() {
        mPresenter.fpsConfirmAdd(mFpsBankRecord, mFpsPreCheckAdd.getFppRefNo(), true);
    }

    @Override
    public void fpsConfirmAddError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg);
    }

    @Override
    public void fpsConfirmAddSuccess(FpsConfirmEntity response) {
        FpsBindSucActivity.startActivity(getActivity(), mFpsBankRecord);
        finish();
    }

    @Override
    public void loginSendOtpError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, true);
    }

    @Override
    public void loginSendOtpSuccess(LoginSendOtpEntity response, boolean isTryAgain) {
        mCurrentCardId = response.getCardId();
        setResetTime(true, phoneNo);
        clearOtpInput();
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        }
    }

    /**
     * 验证  登录短信验证码  失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void loginVerifyError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        if (TextUtils.equals(errorCode, ErrorCode.LOGIN_NEW_DEVICE.code)) {
            //当前密码框不应该被输入 防止界面跳转 用户暴力输入
            String str_yes = getString(R.string.P3_A1_20_3);
            String str_no = getString(R.string.P3_A1_23_b_5);
            String str_msg = errorMsg;
            AndroidUtils.showTipDialog(RegisterInputOTPActivity.this, "  ", str_msg, str_yes, str_no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //登录时输完密码，在其他端登录时继续本次登录
                    String otpNo = etwdOtpCode.getEditText().getText().toString().trim();
                    mPresenter.loginVerifyOtp(phoneNo, "L", mCurrentCardId, true, otpNo);
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        } else if (TextUtils.equals(errorCode, "EWA5632") || TextUtils.equals(errorCode, "EWA5633") || TextUtils.equals(errorCode, "EWA5634")) {
            //EWA5632 5633 5634 操作逾时（10分钟）
            showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                    EventBus.getDefault().post(new LoginEventEntity(LoginEventEntity.EVENT_LOGIN_OVER_TIME_CODE, "over time"));
                    finish();
                }
            });
        } else {
            showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        }
    }

    @Override
    public void loginVerifySuccess(NormalLoginSucEntity response) {
        loginSuc(response);
    }

    @Override
    public FpsBankRecordBean getBankRecord() {
        return mFpsBankRecord;
    }

    @Override
    public String getFppRefNo() {
        if (mFpsPreCheckAdd == null) return "";
        return mFpsPreCheckAdd.getFppRefNo();
    }


    @Override
    public void verifyFioOtpSuccess(BaseEntity entity) {
        onVerifyFioOtpSuc();
    }

    @Override
    public void verifyFioOtpError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, false);
    }


    //FIO-otp校验
    private void verifyOtpFio(String otpNo) {
        mPresenter.verifyFioOtp(otpNo);
    }

    @Override
    public void verifySmartAccountOtpError(String errorCode, String errorMsg) {
        onOtpActionFailed(errorCode, errorMsg, false);
    }

    @Override
    public void verifySmartAccountOtpSuccess(BaseEntity response) {
        AndroidUtils.hideKeyboard(etwdOtpCode.getEditText());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                    //注册
                    goSmartRegistAfterOTP();
                } else if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
                    //登录
                    //登录成功才保存登录态 密码校验
                    goLoginAfterOtp();
                } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
                    bindCardSuccess(false);
                } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
                    //忘记支付密码
                    goSetPasscodeAfterOtp();
                }
            }
        }, 200);
    }


    //校验我的账户OTP
    private void verifyOtpSmart(String typeFlow, String otpNo) {
        //只要没有我的账户（1.注册BOCPAY流程，2.从bocpay绑定我的账户流程），都应该传这个值
        smartInfo = (BankLoginResultEntity) getIntent().getSerializableExtra(SmartAccountConst.DATA_SMART_ACCOUNT_INFO);
        mPresenter.verifySmartAccountOtp(walletId, typeFlow, otpNo, smartInfo);
    }

    @Override
    public void registerBySmartAccountError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        AndroidUtils.showTipDialog(RegisterInputOTPActivity.this, errorMsg);
    }

    @Override
    public void registerBySmartAccountSuccess(RegSucBySmartEntity response) {
        CacheManagerInstance.getInstance().saveLoginStatus();
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        CacheManagerInstance.getInstance().saveOpenBiometricAuth(response.isOpenBiometricAuth());
        HttpCoreKeyManager.getInstance().setUserId(walletId);
        showRegisterSmartAccountSuccessPage(response);
    }


    //otp成功后，进行注册
    private void goSmartRegistAfterOTP() {
        String passcode = getIntent().getStringExtra(Constants.ACCOUNT_PASSCODE);
        String typeFlow = AndroidUtils.getFlowFromType(mPageFlow);
        mPresenter.registerBySmartAccount(passcode, typeFlow);
    }

    /**
     * 信用卡用户注册成功
     *
     * @param response
     */
    private void showRegisterCreditAccountSuccessPage(RegSucEntity response) {
//        reloadBankcards();
        if (response != null && response.getCards() != null && !response.getCards().getRows().isEmpty()) {
            CacheManagerInstance.getInstance().setCardEntities(response.getCards().getRows());
        }
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.ACCOUNT_TYPE, mAccountType);
        mHashMaps.put(Constants.REWARDTIP, response.getRewardTip());
        mHashMaps.put(Constants.SHOWINVITEPAGE, response.showInvitePage());
//        mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
//        mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
        boolean jumpMessageCenter = false;
        if (!SpUtils.getInstance().hasShowNotificationDialog()) {
            //首次安装，升级
            jumpMessageCenter = response.isJumpToMessageCenterWhenFirstInstallOrUpdate();
        } else {
            jumpMessageCenter = response.isJumpToMessageCenter();
        }
        addRegisterSuccessEvent(response.getNewUserPopUpBanner(), response.isNeedEditPwd(), jumpMessageCenter, getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, RegisterCreditCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    /**
     * 智能账户注册成功
     *
     * @param response
     */
    private void showRegisterSmartAccountSuccessPage(RegSucBySmartEntity response) {
//        reloadBankcards();
        if (response != null && response.getCards() != null && !response.getCards().getRows().isEmpty()) {
            CacheManagerInstance.getInstance().setCardEntities(response.getCards().getRows());
        }

        HashMap<String, Object> mHashMaps = new HashMap<>();
        if (response != null) {
            //智能账户注册 要显示注册的账户
            mHashMaps.put(Constants.SMART_ACCOUNT, response.getSmartAccount());
            mHashMaps.put(RegisterSmartAccuntSuccessActivity.DATA_REGISTER_SUCCESS, response);
            mHashMaps.put(Constants.REWARDTIP, response.getRewardTip());
            mHashMaps.put(Constants.SHOWINVITEPAGE, response.showInvitePage());
            mHashMaps.put(Constants.HAVE_VC_CARD, "Y".equals(response.getHaveVCCard()));
        }
        mHashMaps.put(Constants.DATA_PHONE_NUMBER, phoneNo);
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
//        mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
//        mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
        boolean jumpMessageCenter = false;
        if (!SpUtils.getInstance().hasShowNotificationDialog()) {
            //首次安装，升级
            jumpMessageCenter = response.isJumpToMessageCenterWhenFirstInstallOrUpdate();
        } else {
            jumpMessageCenter = response.isJumpToMessageCenter();
        }
        addRegisterSuccessEvent(response.getNewUserPopUpBanner(), response.isNeedEditPwd(), jumpMessageCenter, getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, RegisterSmartAccuntSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }


    private void bindCardSuccess(boolean isCreditCard) {
        reloadBankcards();
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
        mHashMaps.put(Constants.IS_CREDIT_CARD, isCreditCard);
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, BindCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void verifyCreditOtpSuccess(VerifyOTPEntity response) {
        mVerifyOTPEntity = response;
        //验证码输入框删除按钮此时不可点击
        etwdOtpCode.getmDelImageView().setEnabled(false);
        AndroidUtils.hideKeyboard(etwdOtpCode.getEditText());
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                    //注册
                    goCreditRegisterAfterOTP();
                } else if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
                    //登录
                    //登录成功才保存登录态 密码校验
                    goLoginAfterOtp();
                } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
                    bindCardSuccess(true);
                } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
                    //忘记支付密码
                    goSetPasscodeAfterOtp();
                } else if (mPageFlow == Constants.DATA_TYPE_SET_FIO) {
                    onVerifyFioOtpSuc();
                }
                etwdOtpCode.getmDelImageView().setEnabled(true);
            }
        }, 200);
    }

    @Override
    public void verifyCreditOtpError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        onOtpActionFailed(errorCode, errorMsg, false);
    }

    private void verifyOtpCredit(String typeFlow, String otpNo) {
        mPresenter.verifyCreditOtp(mAccount, walletId, cardNo, expiryDate, typeFlow, otpNo);
    }


    private void onVerifyFioOtpSuc() {
        Intent intent = new Intent();
        intent.putExtra(FioConst.FIO_ACTION_RESULT_OTP, true);
        setResult(RESULT_OK, intent);
        finish();
    }


    @Override
    public void confirmRegisterError(String errorCode, String errorMsg) {
        sendErrorEvent(errorCode, errorMsg);
        AndroidUtils.showTipDialog(RegisterInputOTPActivity.this, errorMsg);
    }

    @Override
    public void confirmRegisterSuccess(RegSucEntity response) {
        CacheManagerInstance.getInstance().saveLoginStatus();
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        HttpCoreKeyManager.getInstance().setUserId(walletId);
        showRegisterCreditAccountSuccessPage(response);
    }


    private void goCreditRegisterAfterOTP() {
        String passcode = getIntent().getExtras().getString(Constants.ACCOUNT_PASSCODE);
        String typeFlow = AndroidUtils.getFlowFromType(mPageFlow);
        mPresenter.getConfirmRegister(passcode, walletId, cardID, typeFlow);
    }


    //登录时输完密码去校验是在其他端登录
    private void checkLogin(final String psw, final String region, final String actionFlow, final boolean needVerify) {
        setProgressDialogAlpha(80);
        mPresenter.getConfirmLogin(mAccount, walletId, psw, region, actionFlow, "", needVerify);
    }

    @Override
    public void confirmLoginError(String errorCode, String errorMsg, String isContinue, String passCode, String region, String action, boolean needVerify) {
        sendErrorEvent(errorCode, errorMsg);
        if (LOGIN_OTHER_DEVICE_YES.equals(isContinue)) {
            showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg);
            if (mPaymentPasswordDialogFragment != null && mPaymentPasswordDialogFragment.isVisible()) {
                mPaymentPasswordDialogFragment.clearPwd();
            }
        } else {
            if (mPaymentPasswordDialogFragment != null && mPaymentPasswordDialogFragment.isVisible()) {
                mPaymentPasswordDialogFragment.clearPwd();
            }
            if (TextUtils.equals(errorCode, ErrorCode.LOGIN_NEW_DEVICE.code)) {
                //当前密码框不应该被输入 防止界面跳转 用户暴力输入
                mPaymentPasswordDialogFragment.setItemClickEnabled(false);
                String str_yes = getString(R.string.P3_A1_20_3);
                String str_no = getString(R.string.P3_A1_23_b_5);
                String str_msg = errorMsg;
                AndroidUtils.showTipDialog(RegisterInputOTPActivity.this, "  ", str_msg, str_yes, str_no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //登录时输完密码，在其他端登录时继续本次登录
                        mPresenter.getConfirmLogin(mAccount, walletId, passCode, region, action, LOGIN_OTHER_DEVICE_YES, needVerify);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            } else {
                AndroidUtils.showTipDialog(RegisterInputOTPActivity.this, errorMsg);
            }
        }

    }

    @Override
    public void confirmLoginSuccess(NormalLoginSucEntity response) {
        loginSuc(response);
    }


    private void loginSuc(NormalLoginSucEntity response) {
        if (mPaymentPasswordDialogFragment != null && mPaymentPasswordDialogFragment.isVisible()) {
            mPaymentPasswordDialogFragment.clearPwd();
            mPaymentPasswordDialogFragment.dismiss();
        }
        HttpCoreKeyManager.getInstance().saveSessionId(response.getsId());
        HttpCoreKeyManager.getInstance().setUserId(response.getWalletId());
        CacheManagerInstance.getInstance().saveLoginStatus();
        ArrayList<BankCardEntity> rows = response.getCards().getRows();
        if (null != rows && 0 < rows.size()) {
            CacheManagerInstance.getInstance().setCardEntities(rows);
        }
        CacheManagerInstance.getInstance().saveOpenBiometricAuth(response.isOpenBiometricAuth());
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CURRENT_PAGE_FLOW, mPageFlow);
//        mHashMaps.put(Constants.IS_NEED_EDIT_PWD, response.isNeedEditPwd());
//        mHashMaps.put(Constants.IS_NEED_JUMP_NOTIFICATION, response.isJumpToMessageCenter());
        if (null != getIntent() && null != getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS)) {
            mHashMaps.put(RED_PACKET_RED_PACKET_PARAMS, getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
        }
//        mHashMaps.put(Constants.IS_NEED_SHOW_REGISTER_POPUP_BANNER, response.getNewUserPopUpBanner());
//        mHashMaps.put(AppCallAppLinkEntity.APP_LINK_URL, getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
        boolean jumpMessageCenter = false;
        if (!SpUtils.getInstance().hasShowNotificationDialog()) {
            //首次安装，升级
            jumpMessageCenter = response.isJumpToMessageCenterWhenFirstInstallOrUpdate();
        } else {
            jumpMessageCenter = response.isJumpToMessageCenter();
        }
        addRegisterSuccessEvent(response.getNewUserPopUpBanner(), response.isNeedEditPwd(), jumpMessageCenter, getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL));
        mHashMaps.put(Constants.IS_FROM_LOGOUT_DEEP_LINK, true);
        MyActivityManager.removeAllTaskForLogin();
        ActivitySkipUtil.startAnotherActivity(RegisterInputOTPActivity.this, MainHomeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    private void addRegisterSuccessEvent(String popUpBanner, boolean needPwd, boolean jumpMessageCenter, String appLink) {
        if (!TextUtils.isEmpty(popUpBanner)) {
            RegisterPopBannerDialogEvent registerPopBannerDialogEvent = new RegisterPopBannerDialogEvent();
            registerPopBannerDialogEvent.updateEventParams(RegisterPopBannerDialogEvent.POPUPBANNERURL, popUpBanner);
            UserLoginEventManager.getInstance().addUserLoginEvent(registerPopBannerDialogEvent);
        }
        if (needPwd) {
            EditPwdDialogEvent editPwdDialogEvent = new EditPwdDialogEvent();
            UserLoginEventManager.getInstance().addUserLoginEvent(editPwdDialogEvent);
        }
        if (jumpMessageCenter) {
            JumpCenterNotificationEvent jumpCenterNotificationEvent = new JumpCenterNotificationEvent();
            UserLoginEventManager.getInstance().addUserLoginEvent(jumpCenterNotificationEvent);
        }
        if (!TextUtils.isEmpty(appLink)) {
            AppCallAppEvent appCallAppEvent = new AppCallAppEvent();
            appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, popUpBanner);
            UserLoginEventManager.getInstance().addUserLoginEvent(appCallAppEvent);
        }

    }


    /**
     * DATA_CARD_TYPE_LOGIN  发送  登录短信验证码  失败
     *
     * @param errorCode
     * @param errorMsg
     * @param isSend
     */
    private void onOtpActionFailed(final String errorCode, String errorMsg, final boolean isSend) {
        if (TextUtils.equals(errorCode, PWID_ERROR_CODE) && (mPageFlow == Constants.PAGE_FLOW_REGISTER || mPageFlow == Constants.PAGE_FLOW_BIND_CARD)) {
            //智障人士错误码 - PAGE_FLOW_REGISTER : 智能账户注册，PAGE_FLOW_BIND_CARD : 智能账户绑卡
            showErrorMsgDialog(RegisterInputOTPActivity.this, "我们现时未能处理您的申请。如有查询，您可联络我们的在线客服 (目录 > 帮助) / 客户服务热线 (852) 3988 2388。", new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    if (MyActivityManager.containMainHomePage()) {
                        //已登陆，返回主页
                        EventBus.getDefault().post(new PWidEventEntity(PWidEventEntity.EVENT_HOME_PAGE, "pwid拒纳信息返回主页"));
                        MyActivityManager.removeAllTaskExcludeMainStack();
                    } else if (MyActivityManager.containPreloginPage()) {
                        //未登录，返回PreLogin
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    }
                }
            });
        } else {
            showErrorMsgDialog(RegisterInputOTPActivity.this, errorMsg, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                    if (isSend) {
                        tvSendMsg.setClickable(true);
                        tvSendMsg.setEnabled(true);
                        tvSendMsg.setVisibility(View.VISIBLE);
                        tvSendMsg.setText(mSendAgainTip);
                        tvSendMsg.setVisibility(View.VISIBLE);
                        tvSendMsg.setTextColor(getResources().getColor(R.color.color_green));
                    }
                    if (mPageFlow == Constants.DATA_CARD_TYPE_LOGIN) {
                        //登录验证码
                        if (TextUtils.equals(errorCode, "EWA5632") || TextUtils.equals(errorCode, "EWA5633") || TextUtils.equals(errorCode, "EWA5634")) {
                            //EWA5632 5633 5634 操作逾时（10分钟）
                            EventBus.getDefault().post(new LoginEventEntity(LoginEventEntity.EVENT_LOGIN_OVER_TIME_CODE, "over time"));
                            finish();
                        }
                    }
                }
            });
        }
    }
}
