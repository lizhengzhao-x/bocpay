package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.MediaPlayerUtils;
import cn.swiftpass.wallet.intl.utils.RedPacketDialogUtils;
import cn.swiftpass.wallet.intl.utils.RedPacketResourceUtil;
import me.jessyan.autosize.utils.AutoSizeUtils;

/**
 * 普通 派利是拆红包 不需要音乐！
 */
public class RedCommonPacketOpenDialog extends BaseRedPacketOpenDialog {
    public RedCommonPacketOpenDialog(Context context) {
        super(context);
    }

    public RedCommonPacketOpenDialog(Context context, int theme) {
        super(context, theme);
    }

    public static class Builder extends BaseRedPacketOpenDialog.Builder {

        public Builder(Context context) {
            super(context);
        }


        public Builder setGifType(String gifType) {
            this.gifType = gifType;
            return this;
        }

        public Builder setCloseClickListener(View.OnClickListener closeClickListener) {
            this.closeClickListener = closeClickListener;
            return this;
        }

        public Builder setNextRedPacketRefNo(String nextRedPacketRefNo) {
            this.nextRedPacketRefNo = nextRedPacketRefNo;
            return this;
        }

        public Builder setTransferType(String transferType) {
            this.transferType = transferType;
            return this;
        }

        public Builder setOpenType(String openType) {
            this.openType = openType;
            return this;
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setBlessingWords(String blessingWords) {
            this.blessingWords = blessingWords;
            return this;
        }
        public Builder setTitleFlag(String titleFlag) {
            this.titleFlag = titleFlag;
            return this;
        }


        public Builder setRedOpenNum(String redOpenNum) {
            this.redOpenNum = redOpenNum;
            return this;
        }
        public Builder setAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Builder setGifUri(Uri gifUri) {
            this.gifUri = gifUri;
            return this;
        }

        public Builder setMp3Uri(Uri mp3Uri) {
            this.mp3Uri = mp3Uri;
            return this;
        }


        public Builder setPositiveListener(ConFrimClick listener) {
            this.positiveButtonClickListener = listener;
            return this;
        }

        public RedCommonPacketOpenDialog create() {
            RedCommonPacketOpenDialog dialog = new RedCommonPacketOpenDialog(context, R.style.Dialog_No_KeyBoard);
            dialog.setContentView(getLayout());
            dialog.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            return dialog;
        }


        @Override
        protected void initDate() {
            super.initDate();

            if (null == tvDialogRedPacketTitle) {
                return;
            }


            if (!TextUtils.isEmpty(title)) {
                tvDialogRedPacketTitle.setText(title);
            }

            //如果没有祝福语，就让整个内容居中
            if (!TextUtils.isEmpty(blessingWords)) {
                tvDialogRedPacketTop.setVisibility(View.VISIBLE);
                tvDialogRedPacketBless.setVisibility(View.VISIBLE);
                tvDialogRedPacketBless.setText(blessingWords);
            }else {
                tvDialogRedPacketTop.setVisibility(View.GONE);
                tvDialogRedPacketBless.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(amount)) {
                llyAmountContain.setVisibility(View.VISIBLE);
                String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(amount), true);
                tvDialogRedPacketAmount.setText(price);
            } else {
                llyAmountContain.setVisibility(View.GONE);
            }

            tvDialogRedPacketTitleFlag.setText(titleFlag);
            if (TextUtils.equals(openType, Constants.RED_PACKET_ALL_TYPE)) {
                llyAmountContain.setVisibility(View.GONE);
                llyCommonContain.setVisibility(View.GONE);
                llyAllContain.setVisibility(View.VISIBLE);
                //如果拆开全部就只要填写 打开红包个数
                tvDialogRedPacketAllNum.setText(redOpenNum);
            }else {
                llyAmountContain.setVisibility(View.VISIBLE);
                llyCommonContain.setVisibility(View.VISIBLE);
                llyAllContain.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(nextRedPacketRefNo)
                    && TextUtils.equals(openType, Constants.RED_PACKET_COMON_TYPE)) {
                tvTransferBtnNext.setText(context.getResources().getString(R.string.RP2101_24_1));
                ivClose.setVisibility(View.VISIBLE);
            } else {
                tvTransferBtnNext.setText(context.getResources().getString(R.string.RP2101_25_1));
                ivClose.setVisibility(View.GONE);
            }


            /**
             * 如果是打开全部
             */
            if (TextUtils.equals(openType, Constants.RED_PACKET_ALL_TYPE)) {
                tvTransferBtnNext.setText(context.getResources().getString(R.string.RP2101_25_1));
            }

            //先关闭音乐
            MediaPlayerUtils.stopPlayMusic();
            if (null != mp3Uri) {
                //MediaPlayerUtils.startPlayMusic(context, mp3Uri);
            }

            String gifUrlStr = gifUri.toString();

            String defaultPath = RedPacketResourceUtil.getDefaultPath(context, gifType);

            //如果是默认图片可以直接加载 网络图片需要区分 gif和png
            if (TextUtils.equals(defaultPath, gifUri.toString())) {
                GlideApp.with(context).asGif().load(gifUri).override(childTotalWidth,childTotalHeight).into(new SimpleTarget<GifDrawable>() {
                    @Override
                    public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                        rlyRedPacketContain.setBackground(resource);
                        updateView();

                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                        updateView();
                    }
                });

            } else {
                File file = new File(gifUri.toString());
                //后台可能会返回 gif png这些格式 需要区分
                if (gifUrlStr.contains(".gif")) {
                    GlideApp.with(context).asGif().load(file).override(childTotalWidth,childTotalHeight).into(new SimpleTarget<GifDrawable>() {
                        @Override
                        public void onResourceReady(@NonNull GifDrawable resource, @Nullable Transition<? super GifDrawable> transition) {
                            rlyRedPacketContain.setBackground(resource);
                            updateView();
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                            updateView();
                        }
                    });
                } else {
                    GlideApp.with(context).load(file).override(childTotalWidth,childTotalHeight).into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            rlyRedPacketContain.setBackground(resource);
                            updateView();
                        }

                        @Override
                        public void onLoadFailed(@Nullable Drawable errorDrawable) {
                            super.onLoadFailed(errorDrawable);
                            rlyRedPacketContain.setBackgroundResource(RedPacketResourceUtil.getFailDefaultUrl(gifType));
                            updateView();
                        }
                    });
                }
            }

        }

        /**
         * 当图片 加载完成才显示所有控件
         */
        private void updateView(){
            if(null != llyRedPacketMainContain){
                llyRedPacketMainContain.setVisibility(View.VISIBLE);
            }

        }

        public void updateContain(String title, String blessingWords, String amount,
                                  Uri gifUri, Uri mp3Uri, String nextRedPacketRefNo, String openType,
                                  String transferType, String gifType,String titleFlag) {
            this.title = title;
            this.blessingWords = blessingWords;
            this.amount = amount;
            this.gifUri = gifUri;
            this.mp3Uri = mp3Uri;
            this.nextRedPacketRefNo = nextRedPacketRefNo;
            this.openType = openType;
            this.transferType = transferType;
            this.gifType = gifType;
            this.titleFlag = titleFlag;
            initDate();

        }

    }

}

