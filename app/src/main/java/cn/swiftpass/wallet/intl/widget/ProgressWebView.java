package cn.swiftpass.wallet.intl.widget;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.webkit.WebView;

import androidx.annotation.ColorInt;

/**
 * Created by junhua on 2019年7月24日20:28:54.
 */
public class ProgressWebView extends WebView {
    private static final int PROGRESS_HEIGHT = 6;

    private int mProgressWidth;
    private int mProgressHeight = PROGRESS_HEIGHT;
    private int mProgress;
    private boolean isShowProgress = true;
    private Paint mPaint;

    private int mProgressColor = Color.parseColor("#34C759");


    public ProgressWebView(Context context) {
        super(context);
        init();
    }

    public ProgressWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        //处理编译器预览效果
        if (isInEditMode()) {
            mProgress = 60;
        }
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setColor(mProgressColor);
        mPaint.setAntiAlias(true);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(dp2px(mProgressHeight));
        //初始化设置
        upDateProgress(0);
    }

    public void upDateProgress(int newProgress) {
        mProgress = newProgress;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        mProgressWidth = getMeasuredWidth();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (isShowProgress) {
            drawProgress(canvas);
        }
    }

    /**
     * 绘制进度条
     */
    private void drawProgress(Canvas canvas) {
        if (mProgress <= 0 || mProgress >= 99) {
            return;
        }
        int width = (int) (mProgressWidth * mProgress / 100.0f);
        canvas.save();
        canvas.drawLine(0, 0, width, 0, mPaint);
        canvas.restore();
    }

    /**
     * 设置进度条颜色，默认#00ff00
     *
     * @param color 颜色值
     */
    public void setProgressColor(@ColorInt int color) {
        mProgressColor = color;
        mPaint.setColor(color);
    }

    /**
     * 设置进度条的高度，默认3dp
     *
     * @param height dp值
     */
    public void setProgressHeight(int height) {
        mProgressHeight = height;
        mPaint.setStrokeWidth(dp2px(mProgressHeight));
    }

    public void setShowProgress(boolean showProgress) {
        isShowProgress = showProgress;
    }

    /**
     * dp转换成px
     *
     * @param dp dp
     * @return px值
     */
    private int dp2px(float dp) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }


}
