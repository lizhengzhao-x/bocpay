package cn.swiftpass.wallet.intl.module.redpacket.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.PaleyShiHomeProtocol;
import cn.swiftpass.wallet.intl.module.redpacket.contract.PaleyShiContract;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;

/**
 * 利是 历史记录
 */
public class PaleyShiPresenter extends BasePresenter<PaleyShiContract.View> implements PaleyShiContract.Presenter {

    @Override
    public void getPaleyShiHomeData() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new PaleyShiHomeProtocol(new NetWorkCallbackListener<PaleyShiHomeEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaleyShiHomeDataError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(PaleyShiHomeEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getPaleyShiHomeDataSuccess(response);
                }

            }
        }).execute();
    }

}
