package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author ramon
 * create date on  on 2018/8/27 15:07
 * 验证FIO的ID
 */

public class FioAuthIdProtocol extends BaseProtocol {
    String fioId;

    public FioAuthIdProtocol(String id, NetWorkCallbackListener dataCallback) {
        this.fioId = id;
        this.mDataCallback = dataCallback;
        mUrl = "api/fio/checkValidFIOId";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.FIO_ID, fioId);
    }
}
