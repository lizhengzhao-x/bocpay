package cn.swiftpass.wallet.intl.module.login.adapter;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import cn.swiftpass.wallet.intl.R;

public class PreLoginBannerHolder extends RecyclerView.ViewHolder {
    public ImageView preLoginBannerIv;

    public PreLoginBannerHolder(@NonNull View itemView) {
        super(itemView);
        preLoginBannerIv = itemView.findViewById(R.id.iv_pre_login_bottom_banner);
    }
}
