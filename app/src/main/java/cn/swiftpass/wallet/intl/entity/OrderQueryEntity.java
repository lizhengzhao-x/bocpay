package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @author Jamy
 * create date on  on 2018/7/26 11:08
 * 账单查询返回对象
 */

public class OrderQueryEntity extends BaseEntity {

    public static final String TYPE_TRANSFER = "TRANSFER";
    public static final String TYPE_FPS = "FPS";
    public static final String TYPE_TCOLLECTION = "COLLECTION";
    public static final String TYPE_REDPACKAGE = "REDPACKAGE";
    public static final String TYPE_BINDAWARD = "BINDAWARD";
    public static final String TYPE_TOPUP = "TOPUP";
    public static final String TYPE_GRADEREDUCE = "GRADEREDUCE";


    public String getOrderType() {
        return orderType;
    }

    /**
     * 交易类型 转账->TRANSFER
     * FPS消费->FPS
     * 收款->COLLECTION
     * 银联消费->UNIONPAY
     * 派利是->REDPACKAGE
     * 首绑奖赏->BINDAWARD
     * 增值->TOPUP
     * 积分抵消欠账->GRADEREDUCE
     */
    private String orderType;

    public String getOuttrfrefno() {
        return outtxrefno;
    }

    public void setOuttrfrefno(String outtxrefno) {
        this.outtxrefno = outtxrefno;
    }

    private String outtxrefno;


    /**
     *
     */
    public static final String PAYMENT_STATUS_PROCESS = "PROCESS";
    /**
     *
     */
    public static final String PAYMENT_STATUS_SUCCESS = "SUCCESS";
    /**
     *
     */
    public static final String PAYMENT_STATUS_FAIL = "FAIL";
    /**
     *
     */
    public static final String PAYMENT_STATUS_CANCEL = "CANCEL";
    /**
     *
     */
    public static final String PAYMENT_STATUS_REFUND = "REFUND";
    public static final String PAYMENT_STATUS_NONE = "PAYMENT_STATUS_NONE";


    public static final String CROSSBORDERTRANSFER_SUCCESS = "4";
    public static final String CROSSBORDERTRANSFER_SUCCESS_ = "1";
    public static final String CROSSBORDERTRANSFER_PROCESS = "2";
    public static final String CROSSBORDERTRANSFER_FAILED = "3";


    public static final String CROSSBORDERTRANSFER_SUCCESS_SMART = "1";
    public static final String CROSSBORDERTRANSFER_PROCESS_RED = "2";

    /**
     * acquirerIIN : 12345678
     * discount : 1
     * forwardingIIN : 1234567
     * merchantName : 商户名称
     * originalAmt : 101.10
     * paymentStatus : 2
     * qrCode : https://qr.95516.com/00010000/62422324494817353099926807422283
     * qrcVoucherNumber : voucheName
     * rejectionReason : rejreason
     * retrievalRefNum : RRN
     * transDate : 0813103233
     * trxAmt : 101.10
     * trxCurrency : 156
     * txnId : A5092018081310320443800000003
     */

    private String acquirerIIN;
    private String discount;
    private String forwardingIIN;
    private String merchantName;
    private String originalAmt;
    private String paymentStatus;
    private String qrCode;
    private String qrcVoucherNumber;
    private String rejectionReason;
    private String retrievalRefNum;
    private String transDate;
    private String trxAmt;
    private String trxCurrency;
    private String txnId;
    private String panFour;

    public String getCreditSign() {

//        return "#BF2F4F";
        return creditSign;
    }

    public void setCreditSign(String creditSign) {
        this.creditSign = creditSign;
    }

    /**
     * 是否有资格积分抵消签账，0：无资格、1：已抵消签账、2：有资格进行抵消签账
     */
    private String creditSign;

    public String getTxnLabel() {
        return txnLabel;
    }

    public void setTxnLabel(String txnLabel) {
        this.txnLabel = txnLabel;
    }

    private String txnLabel;

    public String getTxTitle() {
        return txTitle;
    }

    public void setTxTitle(String txTitle) {
        this.txTitle = txTitle;
    }

    /**
     * 用于详情页 增值及奖赏 类型的标题展示
     */
    private String txTitle;

    public String getGpName() {
        return gpName;
    }

    public void setGpName(String gpName) {
        this.gpName = gpName;
    }

    private String gpName;

    public String getAcMainNo() {
        return acMainNo;
    }

    public void setAcMainNo(String acMainNo) {
        this.acMainNo = acMainNo;
    }

    private String acMainNo;
    ;

    private String payWithPointFlag;

    public String getPayWithPointFlag() {
        return payWithPointFlag;
    }

    public void setPayWithPointFlag(String payWithPointFlag) {
        this.payWithPointFlag = payWithPointFlag;
    }

    public String getRedeemAmt() {
        return redeemAmt;
    }

    public void setRedeemAmt(String redeemAmt) {
        this.redeemAmt = redeemAmt;
    }

    public String getRedeemCount() {
        return redeemCount;
    }

    public void setRedeemCount(String redeemCount) {
        this.redeemCount = redeemCount;
    }

    public String getRedeemStatus() {
        return redeemStatus;
    }

    public void setRedeemStatus(String redeemStatus) {
        this.redeemStatus = redeemStatus;
    }

    public String getRedeemStatusMsg() {
        return redeemStatusMsg;
    }

    public void setRedeemStatusMsg(String redeemStatusMsg) {
        this.redeemStatusMsg = redeemStatusMsg;
    }

    public String getShowType() {
        return showType;
    }

    public void setShowType(String showType) {
        this.showType = showType;
    }

    private String redeemAmt;
    private String redeemCount;
    private String redeemStatus;
    private String redeemStatusMsg;
    private String showType;

    public String getRefundExplain() {
        return refundExplain;
    }

    public void setRefundExplain(String refundExplain) {
        this.refundExplain = refundExplain;
    }

    private String refundExplain;

    public String getRecordType() {
        return recordType;
    }

    /**
     * 跨境汇款
     *
     * @return
     */
    public boolean isCrossBorderTransfer() {

        if (TextUtils.isEmpty(recordType)) {
            return false;
        }
        return TextUtils.equals(recordType, "1");
    }


    public boolean isUplanInfo() {

        if (TextUtils.isEmpty(recordType)) {
            return false;
        }
        return TextUtils.equals(recordType, "6");
    }

    public boolean isAppCallApp() {
        if (TextUtils.isEmpty(recordType)) {
            return false;
        }
        return TextUtils.equals(recordType, "8") || TextUtils.equals(recordType, "9");
    }


    public void setrecordType(String recordType) {
        this.recordType = recordType;
    }

    //1:跨境汇款 2:银联交易 3:转账 4:新年开春利是   5:微信支付   6:银联uplan交易
    private String recordType;

    public String getGpDetail() {
        return gpDetail;
    }

    public boolean isGpDetail() {
        if (TextUtils.isEmpty(gpDetail)) {
            return false;
        }
        return TextUtils.equals(gpDetail, "1");
    }

    public void setGpDetail(String gpDetail) {
        this.gpDetail = gpDetail;
    }

    //App判断是否展示积分换领详情页 1:是 0：否
    private String gpDetail;


    /**
     * 我的账户级别 1：S1  2:S2  3:S3
     */
    private String smartAcLevel;

    private String postscript;

    private String statusDesc;

    private String paymentStatusDesc;

    private String rejectReason;

    private String cur;

    private String cardType;

    public String getOuttxtype() {
        return outtxtype;
    }

    public void setOuttxtype(String outtxtype) {
        this.outtxtype = outtxtype;
    }

    private String outtxtype;

    public String getOutrejtype() {
        return outrejtype;
    }

    public void setOutrejtype(String outrejtype) {
        this.outrejtype = outrejtype;
    }

    private String outrejtype;

    private String transactionNum;

    private String outamtsign;

    private String uiTitle;

    private String giftActivity;

    private String txnChannel;

    private String barcode;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    private String paymentType;

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    public String getSmartAcLevel() {
        return smartAcLevel;
    }

    public void setSmartAcLevel(String smartAcLevel) {
        this.smartAcLevel = smartAcLevel;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public String getPaymentStatusDesc() {
        return paymentStatusDesc;
    }

    public void setPaymentStatusDesc(String paymentStatusDesc) {
        this.paymentStatusDesc = paymentStatusDesc;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }


    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }


    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }


    public String getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(String transactionNum) {
        this.transactionNum = transactionNum;
    }


    public String getOutamtsign() {
        return outamtsign;
    }

    public void setOutamtsign(String outamtsign) {
        this.outamtsign = outamtsign;
    }


    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    public String getPanFour() {
        return panFour;
    }

    public String getUiType() {
        return uiType;
    }

    public void setUiType(String uiType) {
        this.uiType = uiType;
    }

    private String uiType;//ui界面使用区别是标题还是内容

    public String getUiTitle() {
        return uiTitle;
    }

    public void setUiTitle(String uiTitle) {
        this.uiTitle = uiTitle;
    }


    public String getGiftActivity() {
        return giftActivity;
    }

    public boolean isGiftActivity() {
        if (TextUtils.isEmpty(giftActivity)) {
            return false;
        }
        return TextUtils.equals(giftActivity, "1");
    }

    public void setGiftActivity(String giftActivity) {
        this.giftActivity = giftActivity;
    }


    public String getTxnChannel() {
        return txnChannel;
    }

    public boolean isWeChatPay() {
        if (TextUtils.isEmpty(txnChannel)) {
            return false;
        }
        return TextUtils.equals(txnChannel, "wxpay");
    }

    public void setTxnChannel(String txnChannel) {
        this.txnChannel = txnChannel;
    }


    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }


    public String getAcquirerIIN() {
        return acquirerIIN;
    }

    public void setAcquirerIIN(String acquirerIIN) {
        this.acquirerIIN = acquirerIIN;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getForwardingIIN() {
        return forwardingIIN;
    }

    public void setForwardingIIN(String forwardingIIN) {
        this.forwardingIIN = forwardingIIN;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getOriginalAmt() {
        return originalAmt;
    }

    public void setOriginalAmt(String originalAmt) {
        this.originalAmt = originalAmt;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    public String getQrcVoucherNumber() {
        return qrcVoucherNumber;
    }

    public void setQrcVoucherNumber(String qrcVoucherNumber) {
        this.qrcVoucherNumber = qrcVoucherNumber;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getRetrievalRefNum() {
        return retrievalRefNum;
    }

    public void setRetrievalRefNum(String retrievalRefNum) {
        this.retrievalRefNum = retrievalRefNum;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTrxAmt() {
        return trxAmt;
    }

    public void setTrxAmt(String trxAmt) {
        this.trxAmt = trxAmt;
    }

    public String getTrxCurrency() {
        return trxCurrency;
    }

    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public boolean canGradeReduce() {
//        return true;
        return (!TextUtils.isEmpty(creditSign) && (TextUtils.equals(creditSign, "1") || TextUtils.equals(creditSign, "2")));
    }

    public boolean alreadyGradeReduce() {
        return (!TextUtils.isEmpty(creditSign) && TextUtils.equals(creditSign, "1"));
    }
}
