package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseRecyclerAdapter;
import cn.swiftpass.wallet.intl.widget.recyclerview.BaseViewHolder;

public class LishiHistoryAdapter extends BaseRecyclerAdapter<RedPacketBeanEntity> {

    private String action;
    private Context mContext;

    public LishiHistoryAdapter(@Nullable List<RedPacketBeanEntity> data, String action, Context mContext) {
        super(R.layout.item_lishi_history, data);
        this.mContext = mContext;
        this.action = action;
    }

    @Override
    protected void bindData(@NonNull BaseViewHolder baseViewHolder, RedPacketBeanEntity redPacketBean, int position) {
        baseViewHolder.setText(R.id.tv_red_packet_history_title, redPacketBean.getName());

        String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(redPacketBean.getSendAmt()), true);

        //Constants.SEND  派利是
        if (TextUtils.equals(Constants.SEND, action)) {
            baseViewHolder.setText(R.id.tv_red_packet_history_money, mContext.getResources().getString(R.string.RP2101_10_9) + " " + redPacketBean.getCurrency() + " " + price);
        } else {
            baseViewHolder.setText(R.id.tv_red_packet_history_money, mContext.getResources().getString(R.string.RP2101_23_9) + " " + redPacketBean.getCurrency() + " " + price);
        }
        baseViewHolder.setText(R.id.tv_red_packet_history_content, redPacketBean.getBlessingWords());
        if (TextUtils.isEmpty(redPacketBean.getBlessingWords())) {
            baseViewHolder.getView(R.id.tv_red_packet_history_content).setVisibility(View.GONE);
        } else {
            baseViewHolder.getView(R.id.tv_red_packet_history_content).setVisibility(View.VISIBLE);
        }
        baseViewHolder.setText(R.id.tv_red_packet_history_time, redPacketBean.getTime());
    }
}
