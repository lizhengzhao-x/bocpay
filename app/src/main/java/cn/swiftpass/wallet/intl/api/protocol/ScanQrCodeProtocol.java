package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class ScanQrCodeProtocol extends BaseProtocol {
    public static final String TAG = LoginCheckProtocol.class.getSimpleName();
    String mpQrCode;
    String cardId;

    public ScanQrCodeProtocol(String mpQrCode, NetWorkCallbackListener dataCallback) {
        this.mpQrCode = mpQrCode;
        this.cardId = cardId;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/scanQrCode";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MPQRCODE, mpQrCode);
//        mBodyParams.put(RequestParams.CARDID,cardId);
    }
}
