package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonSyntaxException;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.EcoupEventEntity;
import cn.swiftpass.wallet.intl.entity.event.UplanEntity;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.SPFuncUtils;
import cn.swiftpass.wallet.intl.widget.CircleWebview;
import cn.swiftpass.wallet.intl.widget.dialog.UplanDialog;


public class UplanActivity extends BaseWebViewActivity {

    @BindView(R.id.webView)
    CircleWebview webView;
    public static final String URL_COUPONPICTURE = "couponPictureUrl";
    public static final String URL_REDIRECT = "redirectUrl";
    public static final String URL_MYCOUPON = "myCouponUrl";
    public static final String URL_INSTRUCTIONS = "instructionsUrl";
    public static final String IS_FROM_DEEP_LINK = "isFromDeepLink";
    private boolean isFromDeepLink = false;
    public String couponPictureUrl;
    public String redirectUrl;
    public String couponUrl;
    public String instructionsUrl;
    private UplanDialog uplanDialog;
    public static final String METHOD_UPLAN_PARAM = "CallUplanPay";
    public static final String action = "UPLAN";
    private String couponInfo;
    private boolean hasShowDialog;
    /**
     * 防止弹框提示逻辑出现两次
     */
    private boolean isFinishShowDialog;

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_uplan_webview;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            couponPictureUrl = intent.getStringExtra(URL_COUPONPICTURE);
            redirectUrl = intent.getStringExtra(URL_REDIRECT);
            couponUrl = intent.getStringExtra(URL_MYCOUPON);
            instructionsUrl = intent.getStringExtra(URL_INSTRUCTIONS);
            isFromDeepLink = intent.getBooleanExtra(IS_FROM_DEEP_LINK, false);
            initWebView(webView, false, redirectUrl);
        } else {
            finish();
        }
    }

    @Override
    public void init() {
        super.init();
        setToolBarTitle(R.string.P209_5_3);
        if (getIntent() != null) {
            couponPictureUrl = getIntent().getStringExtra(URL_COUPONPICTURE);
            redirectUrl = getIntent().getStringExtra(URL_REDIRECT);
            couponUrl = getIntent().getStringExtra(URL_MYCOUPON);
            instructionsUrl = getIntent().getStringExtra(URL_INSTRUCTIONS);
            isFromDeepLink = getIntent().getBooleanExtra(IS_FROM_DEEP_LINK, false);
        } else {
            finish();
        }
        hideBackIcon();
        ImageView faqIv = addToolBarRightViewToImage(R.mipmap.icon_uplan_usage_faq);
        faqIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_UPLAN_HELP, startTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_UPLAN, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                showUplanMsgDialog(true);

            }
        });
        getCloseView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showExitDialog();
            }
        });
        setWebViewClient(webView, true, true, new OnUrlVerifyCallBack() {
            @Override
            public void OnUrlVerifyFinish(String url, boolean isSuccess) {
                if (isFinishShowDialog) return;
                isFinishShowDialog = true;
                //因为涉及到证书校验 第一个url 会dialog显示 这样两个dialog都显示会出现闪动 这里做一个延迟
                boolean value = SPFuncUtils.getValue(Constants.DISPLAY_UPLAN_DIALOG, false);
                if (!value) {
                    showUplanMsgDialog(false);
                }
            }
        });
        initWebView(webView, false, redirectUrl);

        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.addJavascriptInterface(new UplanActivity.JsInterface(), METHOD_UPLAN_PARAM);


    }

    private void showExitDialog() {

        DialogUtils.showTipDialog(getActivity(), "  ", getString(R.string.P209_001_1), getString(R.string.P209_001_2), getString(R.string.P209_001_3), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        }, null);
    }


    private void showUplanMsgDialog(boolean isFinish) {
        if (hasShowDialog || mContext == null || getActivity().isFinishing()) return;
        hasShowDialog = true;
        uplanDialog = new UplanDialog(this);
        View contentView = LayoutInflater.from(this).inflate(R.layout.dialog_uplan_msg, null);
        ImageView uplanIv = contentView.findViewById(R.id.iv_uplan_msg);
        View linearCheckView = contentView.findViewById(R.id.id_linear_check);
        TextView confirmTv = contentView.findViewById(R.id.tv_confirm);
        TextView tvNotShow = contentView.findViewById(R.id.tv_not_show);
        CheckBox cbNotShow = contentView.findViewById(R.id.cb_not_show);
        tvNotShow.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG); //下划线
        tvNotShow.getPaint().setAntiAlias(true);//抗锯齿
        if (isFinish) {
            linearCheckView.setVisibility(View.GONE);
        } else {
            linearCheckView.setVisibility(View.VISIBLE);
        }
        RequestOptions options = new RequestOptions()
                .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL);
        if (!isFinishing() && mContext != null) {
            GlideApp.with(mContext)
                    .load(couponPictureUrl)
                    .placeholder(R.mipmap.img_promotion_skeleton_uplan_quit)
                    .apply(options)
                    .into(uplanIv);

        }


        confirmTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFinish) {

                } else {
                    if (cbNotShow.isChecked()) {
                        AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_UPLAN_NONEXTSHOW, startTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_UPLAN, System.currentTimeMillis() - startTime);
                        sendAnalysisEvent(analysisButtonEntity);
                        SPFuncUtils.put(Constants.DISPLAY_UPLAN_DIALOG, cbNotShow.isChecked());
                    }
                }
                uplanDialog.dismiss();
            }
        });


        uplanDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                hasShowDialog = false;
            }
        });

        uplanDialog.setContentView(contentView);
        if (uplanDialog.getWindow() != null) {
            uplanDialog.getWindow().setGravity(Gravity.CENTER);
            uplanDialog.setCanceledOnTouchOutside(false);
            uplanDialog.setCancelable(false);
            uplanDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface anInterface, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
                        return true;
                    }
                    return false;
                }
            });
            float padding = (AndroidUtils.getScreenWidth(this) * 1.0f / AndroidUtils.getScreenHeight(this) / 0.5f - 1) * 400 + 60;
            uplanDialog.getWindow().setLayout(AndroidUtils.getScreenWidth(this) - AndroidUtils.dip2px(this, padding), ViewGroup.LayoutParams.WRAP_CONTENT);
        }
        if (!isFinishing() && mContext != null) {
            uplanDialog.showWithBottomAnim();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EcoupEventEntity event) {
        if (event.getEventType() == EcoupEventEntity.EVENT_FINISH_VONVERT_ECOUP) {
            finish();
        }
    }


    class JsInterface {

        @JavascriptInterface
        public void postMessage(String msg) {
            try {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_UPLAN_USECODE, startTime, PagerConstant.ADDRESS_PAGE_FRONT, PagerConstant.ADDRESS_PAGE_UPLAN, System.currentTimeMillis() - startTime);
                    sendAnalysisEvent(analysisButtonEntity);

                    if (CacheManagerInstance.getInstance().isLogin()) {
                        couponInfo = msg;
                        verifyPwdAction();
                    } else {
                        ActivitySkipUtil.startAnotherActivity(getActivity(), LoginActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                        MyActivityManager.removeAllTaskExcludePreLoginAndLoginStack();
                    }
                }

            } catch (JsonSyntaxException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
    }


    public void verifyPwdAction() {

        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {

                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //防止黑屏问题 应该是透明activity造成的 要做延迟
                        EventBus.getDefault().post(new UplanEntity(UplanEntity.EVENT_UPLAN_BACK_SCAN, action, couponInfo));
                        if (isFromDeepLink) {
                            //从deepLink过来的
                            //使用场景：比如当前处于MGM二级界面，此时点击浏览器的deepLink跳转至UPlan使用优惠券，输入完密码后，应显示收款码界面，关闭MGM界面
                            MyActivityManager.getInstance().removeAllTaskWithMainActivity();
                        }
                        finish();
                    }
                }, 300);

                LogUtils.i("DATA_COUPON_INFO", couponInfo);
//                HashMap<String, Object> mHashMaps = new HashMap<>();
//                mHashMaps.put(ScanActivity.DATA_COUPON_INFO, couponInfo);
//                mHashMaps.put(ScanActivity.ACTION, action);
//                ActivitySkipUtil.startAnotherActivity(UplanActivity.this, ScanActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            showExitDialog();
        }
    }


}

