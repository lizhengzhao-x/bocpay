package cn.swiftpass.wallet.intl.module.home.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;

public class NewMsgContract {

    public interface View extends IView {
//        void getMsgListSuccess(PromotionEntity promotionEntity);
//
//        void getMsgListFail(String errorCode, String errorMsg);

        void getActListSuccess(NewMsgListEntity activityEntity);

        void getActListFail(String errorCode, String errorMsg);
    }

    public interface Presenter extends IPresenter<View> {
        void getMsgList();

//        void getActivityList(String url);
    }
}
