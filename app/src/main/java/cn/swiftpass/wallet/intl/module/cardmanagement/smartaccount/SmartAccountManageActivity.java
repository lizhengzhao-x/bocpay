package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount;

import android.content.DialogInterface;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.SmartAccountUpdateEntity;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.contract.SmartAccountManagerContract;
import cn.swiftpass.wallet.intl.module.cardmanagement.fps.FpsManageActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.presenter.SmartAccountManagerPresenter;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.update.UpdateSmartAccountInputOTPActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.widget.ImageArrowNoPaddingView;


public class SmartAccountManageActivity extends BaseCompatActivity<SmartAccountManagerContract.Presenter> implements SmartAccountManagerContract.View {


    @BindView(R.id.iav_primary_account)
    ImageArrowNoPaddingView mIavPrimaryAccount;
    @BindView(R.id.iav_limit_amount)
    ImageArrowNoPaddingView mIavLimitAmount;
    @BindView(R.id.iav_topup_setting)
    ImageArrowNoPaddingView mIavTopupSetting;

    @BindView(R.id.iav_suspend_smart)
    ImageArrowNoPaddingView mIavSuspendSmart;
    @BindView(R.id.iav_cancel_smart)
    ImageArrowNoPaddingView mIavCancelSmart;
    @BindView(R.id.ll_topup_account)
    LinearLayout mTopUpAccount;

    @BindView(R.id.tv_acvive_smartaccount)
    TextView mTvActiveSmartaccount;
    @BindView(R.id.tv_setting_topup_account)
    TextView tvSettingTopupAccount;
    @BindView(R.id.iv_image_right)
    ImageView ivImageRight;

    @BindView(R.id.tv_suspend_tip)
    TextView mTvSuspendTip;
    @BindView(R.id.tv_suspend_msg)
    TextView mTvSuspendMsg;


    @BindView(R.id.layout_suspend)
    View mSuspendLayout;


    @BindView(R.id.layout_normal)
    View mNormalLayout;

    //当前的事件 验证支付密码成功之后--
    private int currentEventType = 0;
    private static final int EVENT_SUSPEND = 1;
    private static final int EVENT_CANCEL = 2;

    private static final int EVENT_CHECK_EDDA_BY_VERIFY_PWD = 6;
    private static final int EVENT_CHECK_EDDA_BIND = 7;

    MySmartAccountEntity mSmartAccountInfo;
    /**
     * 后台需求otp 每次进到这个页面 要重新获取智能账户信息 这里排除校验密码操作
     */
    private boolean isResumePwd = false;

    @Override
    protected SmartAccountManagerContract.Presenter createPresenter() {
        return new SmartAccountManagerPresenter();
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.AC2101_3_1);

        EventBus.getDefault().register(this);
        if (getIntent() == null || getIntent().getExtras() == null) {

        } else {
            ImageView faqIv = addToolBarRightViewToImage(R.mipmap.icon_navibar_morewhite);
            faqIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkCardMoreInfo();
                }
            });
        }

        if (CacheManagerInstance.getInstance().getMySmartAccountEntity() != null) {
            mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();
            refreshView();
        } else {
            finish();
        }

    }

    /**
     * 卡片详情页
     */
    private void checkCardMoreInfo() {
        BankCardEntity bankCardEntity = (BankCardEntity) getIntent().getExtras().getSerializable(Constants.CURRENT_BANK_CARD);

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.CARD_ENTITY, bankCardEntity);
        if (bankCardEntity.isDefaultCard()) {
            //删除的是默认卡 就默认下一张为新的默认卡
            if (CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity() != null) {
                mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, CacheManagerInstance.getInstance().getOtherNewDefaultCardEntitity().getCardId());
            }
        } else {
            //删除的不是默认卡 要找一个剩下的默认卡id
            BankCardEntity entity = CacheManagerInstance.getInstance().getDefaultCardEntitity();
            String cardId = "";
            if (null != entity) {
                cardId = entity.getCardId();
            }
            mHashMaps.put(Constants.DATA_NEW_DEFAULT_PANID, cardId);
        }
        ActivitySkipUtil.startAnotherActivity(SmartAccountManageActivity.this, CardInfoActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_smartaccount_manage;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isResumePwd) {
            if (mPresenter!=null){
                mPresenter.getSmartAccountInfo();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick({R.id.iav_primary_account, R.id.iav_limit_amount, R.id.iav_topup_setting, R.id.iav_suspend_smart, R.id.iav_cancel_smart, R.id.tv_acvive_smartaccount, R.id.tv_setting_topup_account})
    public void onClick(View v) {
        if (ButtonUtils.isFastDoubleClick()) return;
        isResumePwd = false;
        switch (v.getId()) {
            case R.id.iav_primary_account:
                onClickPrimaryAccount();
                break;
            case R.id.iav_limit_amount:
                onClickDailyLimit();
                break;
            case R.id.iav_topup_setting:
                onClickTopUpSetting();
                break;
            case R.id.iav_suspend_smart:
                onClickSuspendSmart();
                break;
            case R.id.iav_cancel_smart:
                onClickCancelSmart();
                break;
            case R.id.tv_acvive_smartaccount:
                onClickActiveSmartAccount();
                break;
            case R.id.tv_setting_topup_account:
                List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
                if (bankCardsBeans != null && bankCardsBeans.size() > 0) {
                    currentEventType = EVENT_CHECK_EDDA_BY_VERIFY_PWD;
                    verifyPwdAction();
                } else {
                    //没有绑卡 setup状态 点击去绑定edda
                    currentEventType = EVENT_CHECK_EDDA_BIND;
                    mPresenter.getRegEddaInfo(EVENT_CHECK_EDDA_BIND);
                }
                break;
            default:
                break;
        }
    }


    //我的账户主账户
    private void onClickPrimaryAccount() {
        if (mSmartAccountInfo != null) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.CURRENT_SEL_ACCOUNT_NO, mIavPrimaryAccount.getRightText());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(SmartAccountManageActivity.this, SmartAccountListActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }


    private void refreshView() {
        if (null != mSmartAccountInfo) {

            String price = AndroidUtils.formatPrice(Double.parseDouble(mSmartAccountInfo.getPayLimit()), true);
            mIavLimitAmount.setRightText(mSmartAccountInfo.getCurrency() + " " + price);
            mIavPrimaryAccount.setRightText(AndroidUtils.getPrimaryAccountDisplay(mSmartAccountInfo.getAccType(), AndroidUtils.subPaymentMethod(mSmartAccountInfo.getRelevanceAccNo())));
            mIavTopupSetting.setRightText(AndroidUtils.getTopUpMethodValue(this, mSmartAccountInfo.getAddedmethod()));

            if (mSmartAccountInfo.getCardState().equals(MySmartAccountEntity.ACCOUNT_STATUS_NORMAL)) {
                //正常
                mSuspendLayout.setVisibility(View.GONE);
                mNormalLayout.setVisibility(View.VISIBLE);
            } else {
                mSuspendLayout.setVisibility(View.VISIBLE);
                mNormalLayout.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
                //s3账户不需要显示增值账户
                mTopUpAccount.setVisibility(View.GONE);
                mTvSuspendTip.setText(getString(R.string.AC2101_19_5));
                mTvSuspendMsg.setText(getString(R.string.AC2101_19_6));
            } else {
                //不是s3用户显示增值账户
                mTopUpAccount.setVisibility(View.VISIBLE);
                mTvSuspendTip.setText(getString(R.string.AC2101_19_2));
                mTvSuspendMsg.setText(getString(R.string.AC2101_19_3));
                mIavPrimaryAccount.setVisibility(View.GONE);
                mIavTopupSetting.getImageRight().setVisibility(View.INVISIBLE);
                mIavTopupSetting.setOnClickListener(null);
                //如果银行账户绑定银行卡 显示后后四位
                List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
                if (bankCardsBeans != null && bankCardsBeans.size() > 0) {
                    //已经绑定银行卡
                    tvSettingTopupAccount.setText(bankCardsBeans.get(0).getBankName() + "(" + AndroidUtils.getFormatBankCardNumber(bankCardsBeans.get(0).getBankNo()) + ")");
                    tvSettingTopupAccount.setTextColor(getResources().getColor(R.color.font_gray_three));
                    ivImageRight.setVisibility(View.VISIBLE);
                } else {
                    //未绑定卡
                    tvSettingTopupAccount.setTextColor(getColor(R.color.color_FF72016C));
                    tvSettingTopupAccount.setText(getString(R.string.AC2101_9_4));
                    ivImageRight.setVisibility(View.GONE);
                }
            }


        }
    }


    //激活我的账户
    private void onClickActiveSmartAccount() {
        if (mSmartAccountInfo != null) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.DATA_UPDATE_SUSPEND);
            mHashMapsLogin.put(Constants.DATA_SMART_ACTION, Constants.SMART_ACCOUNT_R);
            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(SmartAccountManageActivity.this, UpdateSmartAccountInputOTPActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }

    }

    //我的账户挂起
    private void onClickSuspendSmart() {
        String strTitle = getString(R.string.D2_1_1);
        String strMsg = "";
        if (!TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            strMsg = getString(R.string.MACC1_1_1);
        } else {
            strMsg = getString(R.string.MACC1_1_2);
        }

        String strPos = getString(R.string.dialog_right_btn_ok);
        String strNeg = getString(R.string.string_cancel);
        AndroidUtils.showTipDialog(this, strTitle, strMsg, strPos, strNeg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentEventType = EVENT_SUSPEND;
                verifyPwdAction();
            }
        });
    }


    //我的账户取消
    private void onClickCancelSmart() {
        String strTitle = getString(R.string.cancel_smart_account);
        String strMsg = getString(R.string.cancel_smart_account_msg);
        if (mSmartAccountInfo != null && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            strMsg = getString(R.string.cancel_smart_account_msg);
        } else if (mSmartAccountInfo != null && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_TWO)) {
            strMsg = getString(R.string.string_title_stwo_clear_account_waining);
        }

        String strPos = getString(R.string.title_confirm);
        String strNeg = getString(R.string.string_cancel);
        AndroidUtils.showTipDialog(this, strTitle, strMsg, strPos, strNeg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentEventType = EVENT_CANCEL;
                verifyPwdAction();
            }
        });

    }

    private void verifyPwdAction() {
        isResumePwd = true;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                verifyResultSuc();
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
            }
        });

    }


    private void verifyResultSuc() {
        if (currentEventType == EVENT_SUSPEND) {
            mPresenter.updateSmartAccountSuspend();
        } else if (currentEventType == EVENT_CANCEL) {
            mPresenter.updateSmartAccountCancel();
        } else if (currentEventType == EVENT_CHECK_EDDA_BY_VERIFY_PWD) {
            mPresenter.getRegEddaInfo(EVENT_CHECK_EDDA_BY_VERIFY_PWD);
        }

    }


    private void onClickTopUpSetting() {
        if (null != mSmartAccountInfo) {
            SmartAccountTopUpSettingActivity.startActivity(this, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
        }
    }

    private void onClickDailyLimit() {
        if (null != mSmartAccountInfo) {
            SmartAccountAdjustDailyLimitActivity.startActivity(this, mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
        }
    }

    /**
     * 更改top-up-setting之后需要刷新一下信息
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {
            mPresenter.getSmartAccountInfo();
        }
    }


    /**
     * 解绑成功退出这个页面
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_UNBIND_CARD_SUCCESS) {
            finish();
        }
    }


    @Override
    public void getRegEddaInfoError(int action, String errorCode, String errorMsg) {
        if (action == EVENT_CHECK_EDDA_BY_VERIFY_PWD) {
            showErrorMsgDialog(SmartAccountManageActivity.this, errorMsg);
        } else if (action == EVENT_CHECK_EDDA_BIND) {
            showErrorMsgDialog(SmartAccountManageActivity.this, errorMsg);
        }
    }

    @Override
    public void getRegEddaInfoSuccess(int action, GetRegEddaInfoEntity response) {
        if (action == EVENT_CHECK_EDDA_BY_VERIFY_PWD) {
            List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
            if (bankCardsBeans != null && bankCardsBeans.get(0) != null) {
                HashMap<String, Object> mHashMapsLogin = new HashMap<>();
                mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
                mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.CHECKEDDA);
                mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
                mHashMapsLogin.put(Constants.BANK_CODE, bankCardsBeans.get(0).getBankNo());
                mHashMapsLogin.put(Constants.BANK_NAME, bankCardsBeans.get(0).getBankName());
                mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
                mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
                ActivitySkipUtil.startAnotherActivity(SmartAccountManageActivity.this, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        } else if (action == EVENT_CHECK_EDDA_BIND) {
            HashMap<String, Object> mHashMapsLogin = new HashMap<>();
            mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
            mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
            mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
            mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
            ActivitySkipUtil.startAnotherActivity(SmartAccountManageActivity.this, TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
    }

    @Override
    public void checkSmartAccountBindError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            AndroidUtils.showBindSmartAccountDialog(getActivity());
        } else {

showErrorMsgDialog(getActivity(), errorMsg);
        }
    }

    @Override
    public void checkSmartAccountBindSuccess(ContentEntity response) {
        ActivitySkipUtil.startAnotherActivity(getActivity(), FpsManageActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountManageActivity.this, errorMsg);
    }

    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        if (response != null) {
            mSmartAccountInfo = response;
            refreshView();
        }
    }

    @Override
    public void updateSmartAccountSuspendError(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountManageActivity.this, errorMsg);
    }

    @Override
    public void updateSmartAccountSuspendSuccess(BaseEntity response) {
        String strMsg = "";
        if (!TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            strMsg = getString(R.string.MACC1_1_8);
        } else {
            strMsg = getString(R.string.MACC1_1_9);
        }
        showErrorMsgDialog(mContext, strMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                mSuspendLayout.setVisibility(View.VISIBLE);
                mNormalLayout.setVisibility(View.GONE);
                EventBus.getDefault().postSticky(new CardEventEntity(CardEventEntity.EVENT_REFRESH_SMART_INFO, ""));
            }
        });
    }

    @Override
    public void updateSmartAccountCancelSuccess(SmartAccountUpdateEntity response) {
        if (null == response) {
            return;
        }
        final boolean isLogout = TextUtils.equals(response.getLogout(), Constants.NO_CARD_EXIST);
        if (isLogout) {
            AndroidUtils.clearMemoryCacheOnly();
        }
        showErrorMsgDialog(mContext, getString(R.string.cancel_smart_suc_tip), new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                if (isLogout) {
                    AndroidUtils.clearMemoryCacheOnly();
                    AndroidUtils.goPreLoginPage();
                    MyActivityManager.removeAllTaskExcludePreLoginStack();
                } else {
                    finish();
                }
            }
        });

    }

    @Override
    public void updateSmartAccountCancelError(String errorCode, String errorMsg) {
        showErrorMsgDialog(SmartAccountManageActivity.this, errorMsg);
    }
}
