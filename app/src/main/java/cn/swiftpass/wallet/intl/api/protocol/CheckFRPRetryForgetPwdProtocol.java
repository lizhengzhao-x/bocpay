package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;


public class CheckFRPRetryForgetPwdProtocol extends BaseProtocol {

    public CheckFRPRetryForgetPwdProtocol(NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/smartForgot/checkFRPRetry";
    }
}
