package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.E2eeProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.utils.HashUtility;

public class LoginCheckNewProtocol extends E2eeProtocol {
    public static final String TAG = LoginCheckNewProtocol.class.getSimpleName();
    String mPin;
    String mMobile;

    public LoginCheckNewProtocol(String passcode, String mobile, NetWorkCallbackListener dataCallback) {
        this.mPasscode = passcode;
        this.mMobile = mobile;
        this.mDataCallback = dataCallback;
        mUrl = "api/login/checkLogin";
        mPin = HashUtility.getMD5(passcode);
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.MOBILE, mMobile);
        mBodyParams.put(RequestParams.ACTION, "L");
        mBodyParams.put(RequestParams.PASSCODE, mPasscode);
        mBodyParams.put(RequestParams.FIRST_PIN, mPin);
    }



}
