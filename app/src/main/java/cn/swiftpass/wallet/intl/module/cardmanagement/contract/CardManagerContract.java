package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.base.card.CardListQueryPresenter;
import cn.swiftpass.wallet.intl.base.card.CardListQueryView;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;

public class CardManagerContract  {
    public interface View extends CardListQueryView<CardManagerContract.Presenter> {


        void getRegEddaInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);

        void checkSmartAccountBindError(String errorCode, String errorMsg);

        void checkSmartAccountBindSuccess(ContentEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getSmartAccountInfoSuccess(MySmartAccountEntity response);
    }


    public interface Presenter extends CardListQueryPresenter<CardManagerContract.View> {


        void getRegEddaInfo();

        void checkSmartAccountBind(boolean isShowLoading);

        void getSmartAccountInfo(boolean iShowLoading);
    }
}
