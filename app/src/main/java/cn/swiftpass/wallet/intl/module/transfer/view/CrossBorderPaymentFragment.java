package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.protocol.ActivityNewPromotionProtocol;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.ActivityEntity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.PromotionNewEntity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.SpUtils;

/**
 * 跨境支付
 */
public class CrossBorderPaymentFragment extends BaseFragment {

    @BindView(R.id.id_top_banner)
    ImageView idTopBanner;
    @BindView(R.id.id_title)
    TextView idTitle;
    @BindView(R.id.id_subtitle)
    TextView idSubtitle;
    @BindView(R.id.id_content)
    TextView idContent;
    @BindView(R.id.id_tips)
    TextView idTips;
    @BindView(R.id.tv_open)
    TextView tvOpen;
    @BindView(R.id.id_discover_view)
    LinearLayout idDiscoverView;

    private PromotionNewEntity.DataBeanX mPromotionBean;
    private boolean isGetSuccess;

    private static final String STR_EMAIL_URL = "<EmailUrl>";
    private static final String STR_COOPERATION_URL = "<Cooperation>";
    private static final String STR_EMAIL_URL_LEFT = "</EmailUrl>";
    private static final String STR_COOPERATION_LEFT = "</Cooperation>";


    @Override
    public void initTitle() {
        mActivity.setToolBarTitle(R.string.home_discover);
    }

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    @Override
    protected IPresenter loadPresenter() {
        return null;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fmg_cross_border_payment;
    }

    @Override
    protected void initView(View v) {

        tvOpen.setText(getString(R.string.string_open_fee));
        int width = AndroidUtils.getScreenWidth(getContext());
        int height = width / 3;
        idTopBanner.setLayoutParams(new LinearLayout.LayoutParams(width, height));
        tvOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    String str_title = getString(R.string.great_bay_payment);
                    String str_msg = getString(R.string.P3_Cross_border_A1_01);
                    String str_ok = getActivity().getString(R.string.dialog_right_btn_ok);
                    String str_cancel = getActivity().getString(R.string.cancel);
                    AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_ok, str_cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mPromotionBean != null) {
                                openUrl(mPromotionBean.getCrossPaymentUrl());
                            }
                        }
                    });
                }
            }
        });

        idDiscoverView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isGetSuccess) {
                    getPromotionsRequest();
                }
            }
        });

        getPromotionsRequest();
    }

    private void openUrl(String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


    /**
     * 获取发现列表
     */
    private void getPromotionsRequest() {
        if (getActivity() == null) return;
        ApiProtocolImplManager.getInstance().showDialog(getActivity());
        ApiProtocolImplManager.getInstance().getActivitiesList(ApiConstant.CROSS_BORDER_PAYMENT_PLATFORM, getContext(), new NetWorkCallbackListener<ActivityEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onSuccess(ActivityEntity response) {
                getActiviesData(response.getValue());
            }
        });
    }


    /**
     * 获取活动列表json文件 url
     *
     * @param url
     */
    private void getActiviesData(String url) {
        NetWorkCallbackListener listener = new NetWorkCallbackListener<PromotionNewEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ApiProtocolImplManager.getInstance().dismissDialog();
            }

            @Override
            public void onSuccess(PromotionNewEntity response) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                if (null == response) {
                    return;
                }
                //广告列表 根据不同语言做出筛选
                String lan = SpUtils.getInstance(getContext()).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    if (response.getZh_HK() != null && response.getZh_CN().getData() != null) {
                        refreshView(response.getZh_CN().getData());
                    }
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    if (response.getZh_HK() != null && response.getZh_HK().getData() != null) {
                        refreshView(response.getZh_HK().getData());
                    }
                } else {
                    if (response.getEn_US() != null && response.getEn_US().getData() != null) {
                        refreshView(response.getEn_US().getData());
                    }
                }
            }
        };
        new ActivityNewPromotionProtocol(url, listener).execute();
    }

    private void refreshView(final PromotionNewEntity.DataBeanX promotionBean) {
        isGetSuccess = true;
        GlideApp.with(this).load(promotionBean.getBannerUrl()).into(idTopBanner);
        mPromotionBean = promotionBean;
        tvOpen.setVisibility(View.VISIBLE);
        idTitle.setText(promotionBean.getTitle());
        String subTitleStr = promotionBean.getSubTitle();
//        int length = "<Flow>".length() * 2 + "<Cooperation>".length() * 2 + 2;
        int firstSubStart = 0;
        int firstSubEnd = 0;
        if (subTitleStr.contains(STR_EMAIL_URL)) {
            firstSubStart = subTitleStr.indexOf(STR_EMAIL_URL);
            subTitleStr = subTitleStr.replace(STR_EMAIL_URL, "");
            firstSubEnd = subTitleStr.indexOf(STR_EMAIL_URL_LEFT);
            subTitleStr = subTitleStr.replace(STR_EMAIL_URL_LEFT, "");
        }
        SpannableString spannableStringSubTitle = new SpannableString(subTitleStr);
        spannableStringSubTitle.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                try {
                    Intent data = new Intent(Intent.ACTION_SENDTO);
                    data.setData(Uri.parse("mailto:" + promotionBean.getFlowUrl()));
                    startActivity(data);
                } catch (Exception e) {
                }
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, firstSubStart, firstSubStart + (firstSubEnd - firstSubStart), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        idSubtitle.setText(spannableStringSubTitle);
        idSubtitle.setMovementMethod(LinkMovementMethod.getInstance());
        String introduction = promotionBean.getIntroduction();
        int secondStart = 0;
        int secondEnd = 0;
        if (introduction.contains(STR_COOPERATION_URL)) {
            secondStart = introduction.indexOf(STR_COOPERATION_URL);
            introduction = introduction.replace(STR_COOPERATION_URL, "");
            secondEnd = introduction.indexOf(STR_COOPERATION_LEFT);
            introduction = introduction.replace(STR_COOPERATION_LEFT, "");
        }

        final String urlTitle = introduction.substring(secondStart, secondEnd);
        SpannableString spannableString = new SpannableString(introduction);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View view) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.DETAIL_URL, promotionBean.getCooperationUrl());
                mHashMaps.put(Constants.DETAIL_TITLE, urlTitle);
                mHashMaps.put(WebViewActivity.NEW_MSG, true);
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#136EF1"));
                ds.setUnderlineText(false);
            }
        }, secondStart, secondStart + (secondEnd - secondStart), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        idContent.setText(spannableString);
        idContent.setMovementMethod(LinkMovementMethod.getInstance());
        idTips.setText(promotionBean.getTips());
    }
}
