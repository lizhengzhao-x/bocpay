package cn.swiftpass.wallet.intl.module.ecoupon;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ZhangXinchao on 2019/8/30.
 */
public class GridLinearDivider extends RecyclerView.ItemDecoration {
    private int space;
    private int color;
    private Paint mPaint;

    /**
     * 默认的，垂直方向 横纵1px 的分割线 颜色透明
     */
    public GridLinearDivider() {
        this(1);
    }

    /**
     * 自定义宽度的透明分割线
     *
     * @param space 指定宽度
     */
    public GridLinearDivider(int space) {
        this(space, Color.TRANSPARENT);
    }

    /**
     * 自定义宽度，并指定颜色的分割线
     *
     * @param space 指定宽度
     * @param color 指定颜色
     */

    public GridLinearDivider(int space, int color) {
        this.space = space;
        this.color = color;
        initPaint();
    }


    private void initPaint() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(space);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //得到View的位置
        int childPosition = parent.getChildAdapterPosition(view);
        if (parent.getAdapter() != null && childPosition == parent.getAdapter().getItemCount() - 1){
            outRect.set(0, 0, 0, space);
        }else{
            outRect.set(0, 0, 0, 0);
        }

    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDraw(c, parent, state);
    }
}
