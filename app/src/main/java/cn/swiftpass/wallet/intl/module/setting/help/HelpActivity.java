package cn.swiftpass.wallet.intl.module.setting.help;


import android.Manifest;
import android.app.Activity;
import android.view.View;
import android.webkit.JavascriptInterface;

import com.egoo.chat.ChatSDKManager;
import com.egoo.chat.enity.Channel;
import com.egoo.chat.listener.EGXChatProtocol;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.entity.ChatEntity;
import cn.swiftpass.wallet.intl.entity.OCSLanguageEntity;
import cn.swiftpass.wallet.intl.entity.OnlineChatSdkPermission;
import cn.swiftpass.wallet.intl.entity.OnlineChatSdkResultPermission;
import cn.swiftpass.wallet.intl.entity.SmilingFaceEntity;
import cn.swiftpass.wallet.intl.module.setting.BaseWebViewActivity;
import cn.swiftpass.wallet.intl.sdk.onlinechat.OnLineChatConfig;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.CircleWebview;


public class HelpActivity extends BaseWebViewActivity {
    @BindView(R.id.webView)
    CircleWebview webView;
    //笑脸点击的js回调
    private final String METHOD_SMAIL_FACE_SUBMIT = "VoteInfo";



    @Override
    protected int getLayoutId() {
        return R.layout.act_help_webview;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        super.init();
        setToolBarTitle(R.string.M10_1_24);

        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            initSDKListener();
        }
        setWebViewClient(webView, true);
        String url = getUrlWithLanguage();
        initWebView(webView, url);
        webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        webView.addJavascriptInterface(new HelpActivity.JsInterface(), METHOD_SMAIL_FACE_SUBMIT);
    }


    private String getUrlWithLanguage() {
        //index.html?lang=zh_CN zh_HK en_US
        String url = BuildConfig.FAQ_URL + "creditcard/BOCPAY/faq2/index.html";
//        String url = AppTestManager.getInstance().getBaseUrl() + "index.html";
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            url = url + "?lang=zh_CN";
        } else if (AndroidUtils.isHKLanguage(lan)) {
            url = url + "?lang=zh_HK";
        } else {
            url = url + "?lang=en_US";
        }
        return url;
    }


    public void initSDKListener() {

        ChatSDKManager.setChatWatcher(new EGXChatProtocol.AppWatcher() {
            @Override
            public void getMbkLgnInfo(String s, final EGXChatProtocol.AppCallback appCallback) {
                ///api/onlineChat/getLgnInfo
                if (!NetworkUtil.isNetworkAvailable()) {
                    return;
                }
                showDialogNotCancel();
                ApiProtocolImplManager.getInstance().getMbkLgnInfo(s, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        if (!response.status.equals("login")) {
                            dismissDialog();
                        }
                        dismissDialog();
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });
            }

            @Override
            public void redirectToMbkFunc(String channel, String functionId, String javaData) {

            }

            @Override
            public void notifyLoginMbk(String s, String s1, String s2) {
                LogUtils.d("centerbank", s + "======" + s1 + "=====" + s2);
                dismissDialog();
            }

            @Override
            public void getMbaInfo(String s, final EGXChatProtocol.AppCallback appCallback) {
                OCSLanguageEntity ocsLanguageEntity = new OCSLanguageEntity();
//                C = trad. chinese  S= simp. chinese  E= English
                String lan = SpUtils.getInstance(HelpActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    ocsLanguageEntity.setLanguage("S");
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    ocsLanguageEntity.setLanguage("C");
                } else {
                    ocsLanguageEntity.setLanguage("E");
                }
                String jsonString = new Gson().toJson(ocsLanguageEntity);
                appCallback.returnValue(jsonString);

            }

            @Override
            public void notifyMbaAction(Activity activity, String channel, String actionId, String jsonData, EGXChatProtocol.AppCallback appCallback) {
//                攝像頭	camera	0x009001
//                麥克風	microphone	0x009002
//                iOS相冊	photo
//                Android 存儲讀	read_storage	0x009003
//                Android 存儲寫	write_storage	0x009004
//                懸浮窗	system_alert_window	0x009005
//                1、麦克风：microphone
//                2、摄像头：camera
//                3、相册：photo
                //权限管理
                if (actionId.equals("requestPermission")) {
                    OnlineChatSdkPermission onlineChatSdkPermission = new Gson().fromJson(jsonData, OnlineChatSdkPermission.class);
                    String permissionName = onlineChatSdkPermission.getPermission();
                    String requestCode = onlineChatSdkPermission.getRequestCode();
                    if (permissionName.equals("microphone")) {
                        checkPermission(activity, appCallback, requestCode, Manifest.permission.RECORD_AUDIO, permissionName);
                    } else if (permissionName.equals("camera")) {
                        checkPermission(activity, appCallback, requestCode, Manifest.permission.CAMERA, permissionName);
                    } else if (permissionName.equals("read_storage")) {
                        checkPermission(activity, appCallback, requestCode, Manifest.permission.READ_EXTERNAL_STORAGE, permissionName);
                    } else if (permissionName.equals("write_storage")) {
                        checkPermission(activity, appCallback, requestCode, Manifest.permission.WRITE_EXTERNAL_STORAGE, permissionName);
                    }
                } else if (actionId.equals("onRequestPermissionsResult")) {
                    appCallback.returnValue(jsonData);
                }
            }

            @Override
            public void getMbkAccessToken(String channel, String requesterAccessToken, final EGXChatProtocol.AppCallback appCallback) {
                ///api/onlineChat/getAccessToken
                showDialogNotCancel();
                ApiProtocolImplManager.getInstance().getMbkAccessToken(channel, requesterAccessToken, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });
            }

            @Override
            public void confirmMbkAccessToken(String ts, String channel, String nonceStr, String signature, final EGXChatProtocol.AppCallback appCallback) {
                ///api/onlineChat/confirmAccessToken
                ApiProtocolImplManager.getInstance().confirmMbkAccessToken(ts, channel, nonceStr, signature, new NetWorkCallbackListener<ChatEntity>() {
                    @Override
                    public void onFailed(String errorCode, String errorMsg) {
                        dismissDialog();
                    }

                    @Override
                    public void onSuccess(ChatEntity response) {
                        dismissDialog();
                        String jsonString = new Gson().toJson(response);
                        appCallback.returnValue(jsonString);
                    }
                });
            }

            @Override
            public void getCertFromMbaSrv(String s, String s1, EGXChatProtocol.AppCallback appCallback) {

            }
        });


        ChatSDKManager.mbaInitOcs(getActivity(), "012", Channel.CHANNEL_BOCPAY);
        ChatSDKManager.notifyMbaMbkpageId(System.currentTimeMillis(), Channel.CHANNEL_BOCPAY, OnLineChatConfig.FUNCTIONID_HELP, OnLineChatConfig.PAGEID_HELPPAGE_HELP);

    }

    private void checkPermission(Activity activity, EGXChatProtocol.AppCallback appCallback, String requestCode, String permission, String permissionAlias) {
        if (isGranted(permission)) {
            OnlineChatSdkResultPermission onlineChatSdkResultPermission = new OnlineChatSdkResultPermission();
            onlineChatSdkResultPermission.setResult("Y");
            onlineChatSdkResultPermission.setPermission(permissionAlias);
            appCallback.returnValue(new Gson().toJson(onlineChatSdkResultPermission));
        } else {
            activity.requestPermissions(new String[]{permission}, Integer.valueOf(requestCode));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            ChatSDKManager.show(AndroidUtils.getScreenHeight(this) / 3 * 2);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (BuildConfig.INCLUDE_ONLINE_CHAT) {
            ChatSDKManager.hide();
        }
    }


    /**
     * 笑脸/苦脸 反馈
     *
     * @param action
     * @param pageId
     * @param desc
     */
    private void submitFaceInfoRequest(String action, String pageId, String desc) {
        showErrorMsgDialog(this, getString(R.string.FAQ1_1_1));
        ApiProtocolImplManager.getInstance().smilingFaceSubmint(this, action, pageId, desc, new NetWorkCallbackListener<ContentEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                //showErrorMsgDialog(HelpActivity.this, errorMsg);
            }

            @Override
            public void onSuccess(ContentEntity response) {

            }
        });
    }

    class JsInterface {

        @JavascriptInterface
        public void postMessage(String msg) {
            try {
                LogUtils.i("VoteInfo", msg);
                SmilingFaceEntity smilingFaceEntity = new Gson().fromJson(msg, SmilingFaceEntity.class);
                submitFaceInfoRequest(smilingFaceEntity.getAction(), smilingFaceEntity.getPageId(), smilingFaceEntity.getDesc());
            } catch (JsonSyntaxException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}

