package cn.swiftpass.wallet.intl.module.transfer.view;

import android.content.DialogInterface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.base.BaseFragment;
import cn.swiftpass.wallet.intl.entity.AccountBalanceEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckReq;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PayEventEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountAdjustDailyLimitActivity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountTopUpActivity;
import cn.swiftpass.wallet.intl.module.topup.TopUpActivity;
import cn.swiftpass.wallet.intl.module.transfer.contract.TransferByFpsIDContract;
import cn.swiftpass.wallet.intl.module.transfer.presenter.TransferByFpsIDPresenter;
import cn.swiftpass.wallet.intl.sdk.sa.ActionIdConstant;
import cn.swiftpass.wallet.intl.sdk.sa.AnalysisButtonEntity;
import cn.swiftpass.wallet.intl.sdk.sa.PagerConstant;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.MoneyValueFilter;
import cn.swiftpass.wallet.intl.widget.EditTextWithDel;


public class TransferByFpsIDFragment extends BaseFragment<TransferByFpsIDContract.Presenter> implements TransferByFpsIDContract.View {

    @BindView(R.id.et_fps_number)
    EditTextWithDel mEtFpsNumber;
    @BindView(R.id.etwd_transfer_amount)
    EditTextWithDel mEtwdTransferAmount;

    @BindView(R.id.id_available_amount)
    TextView mIdAvailableAmount;
    @BindView(R.id.tv_amount_usable)
    TextView tvAmountUsable;
    @BindView(R.id.tv_amount)
    TextView tvAmount;

    @BindView(R.id.transfer_email_next)
    TextView mTransferEmailNext;

    @BindView(R.id.id_transfer_remark)
    EditTextWithDel mIdTransferRemark;
    @BindView(R.id.id_remark_size)
    TextView mIdRemarkSize;
    @BindView(R.id.ll_content)
    LinearLayout llContent;
    @BindView(R.id.rl_softkeyboard_view)
    SoftKeyboardTranslationView mSoftKeyboardTranslationView;
    @BindView(R.id.cb_toggle_see)
    CheckBox cbToggleSee;
    @BindView(R.id.tv_transfer_info_title)
    TextView tvTransferInfoTitle;
    @BindView(R.id.iv_set_daily_limit_balance)
    ImageView ivSetDailyLimitBalance;
    @BindView(R.id.iv_bg_wave)
    ImageView ivBgWave;
    @BindView(R.id.tv_balance_tody_title)
    TextView tvBalanceTodyTitle;

    AccountBalanceEntity accountBalanceEntity;

    MySmartAccountEntity mSmartAccountInfo;
    //需不要隐藏眼睛
    private boolean isHideEye = true;

    //当没有获取到数据时 点眼睛是关闭的 获取数据后眼睛打开
    private boolean isEyeDateNull = false;
    private ReferenNoSetDialog mFillReferenceNoDialog;

    private static final String ERROR_NO_REFERENCENO = "EWA3318";

    @Override
    protected boolean useBackgroundRadius() {
        return true;
    }

    private void bindViews() {
        AndroidUtils.setLimit(mIdTransferRemark.getEditText(), mIdRemarkSize);
//        AutoEnableTextWatcher.bind(mTransferEmailNext, 1, mEtFpsNumber.getEditText(), mEtwdTransferAmount.getEditText());
        //两位小数过滤
        mEtwdTransferAmount.getEditText().setFilters(new InputFilter[]{new MoneyValueFilter(), new InputFilter.LengthFilter(9)});
        mEtwdTransferAmount.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        mEtwdTransferAmount.hideDelView();
        mTransferEmailNext.setOnClickListener(onClickListener);

        //防止软键盘换车换行
        mIdTransferRemark.getEditText().setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (ButtonUtils.isFastDoubleClick()) return;
            preCheckEvent(null);
        }
    };

    private void preCheckEvent(String referenceNo) {
        final String fpsNumber = mEtFpsNumber.getText();
        String transferAmount = mEtwdTransferAmount.getText();
        final TransferPreCheckReq req = new TransferPreCheckReq();
        req.transferType = TransferConst.TRANS_FPS;
        req.tansferToAccount = fpsNumber;
        req.transferAmount = transferAmount;
        req.billReference = referenceNo;
        req.postscript = mIdTransferRemark.getText().toString().trim();
        req.isQrcode = false;
        mPresenter.transferPreCheck(req);
    }

    /**
     * 判断眼睛是否可以打开 如果没有获取到数据 需要获取到数据后才打开
     */
    private void setCbToggleSeeCheck() {
        if (null == mSmartAccountInfo) {
            isEyeDateNull = true;
            mPresenter.getSmartAccountInfo();
        } else {
            cbToggleSee.setChecked(true);
        }
    }


    /**
     * 根据输入金额参数控制next状态
     *
     * @param s
     */
    private void dealWithExceedMaxMoney(String s) {
        checkEditMoney(false);
        mIdAvailableAmount.setText("");
        if (TextUtils.isEmpty(s)) {
            if (accountBalanceEntity != null) {
            }
            updateOkBackground(mTransferEmailNext, false);
            return;
        }
        double currentV = 0;
        try {
            currentV = Double.valueOf(s);
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        if (currentV == 0) {
            updateOkBackground(mTransferEmailNext, false);
            return;
        }
        double maxValue = 0;
        if (accountBalanceEntity != null && !TextUtils.isEmpty(accountBalanceEntity.getBalance())) {
            try {
                maxValue = Double.valueOf(accountBalanceEntity.getBalance());
            } catch (NumberFormatException e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
            if (currentV > maxValue) {
                mIdAvailableAmount.setText(getResources().getString(R.string.TF2101_4_11));
                mIdAvailableAmount.setTextColor(ContextCompat.getColor(mContext, R.color.color_FFBF2F4F));
                checkEditMoney(true);
            }else{
                mIdAvailableAmount.setText("");
                mIdAvailableAmount.setTextColor(getResources().getColor(R.color.font_gray_four));
            }
            if (mEtFpsNumber.getText().length() >= 7) {
                updateOkBackground(mTransferEmailNext, currentV <= maxValue);
            } else {
                updateOkBackground(mTransferEmailNext, false);
            }
        } else {
            updateOkBackground(mTransferEmailNext, false);
        }
    }

    /**
     * 判断金额输入框 数字是否大于今日可用交易限额
     *
     * @param isRed
     */
    void checkEditMoney(boolean isRed) {
        if (null == mEtwdTransferAmount) {
            return;
        }

        if (isRed) {
            mEtwdTransferAmount.setLineRedVisible(true);
            mEtwdTransferAmount.setLineVisible(false);
        } else {
            mEtwdTransferAmount.setLineRedVisible(false);
            mEtwdTransferAmount.setLineVisible(true);
        }
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (null != cbToggleSee && hidden && isHideEye) {
            cbToggleSee.setChecked(false);
        } else {
            isHideEye = true;
        }
    }

    /**
     * 修改昵称的输入dialog
     */
    private void inputReferenceNo() {
        ReferenNoSetDialog.Builder builder = new ReferenNoSetDialog.Builder(getActivity());
        mFillReferenceNoDialog = builder.setPositiveListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dlg, int which) {
                String billreference = mFillReferenceNoDialog.getReferenceNoStr().trim();
                preCheckEvent(billreference);
            }
        }).create();
        mFillReferenceNoDialog.show();
        WindowManager.LayoutParams params = mFillReferenceNoDialog.getWindow().getAttributes();
        params.width = (int) (getActivity().getWindowManager().getDefaultDisplay().getWidth() * 0.8);
        mFillReferenceNoDialog.getWindow().setAttributes(params);
    }


    @Override
    protected TransferByFpsIDContract.Presenter loadPresenter() {
        return new TransferByFpsIDPresenter();
    }


    @Override
    protected void initView(View v) {
        bindViews();
        mEtwdTransferAmount.hideErrorView();
        mEtFpsNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                dealWithExceedMaxMoney(mEtwdTransferAmount.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateNextStatus();
            }
        });


        mEtwdTransferAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                dealWithExceedMaxMoney(mEtwdTransferAmount.getEditText().getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        EventBus.getDefault().register(this);

        cbToggleSee.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    openAccount();
                } else {
                    hideAccount();
                }
            }
        });


        getSmartAccountInfo();
        tvBalanceTodyTitle.setText(AndroidUtils.addStringColonSpace(getString(R.string.TF2101_3_2)));
    }

    @OnClick({R.id.tv_balance_title,
            R.id.tv_amount,
            R.id.tv_balance_tody_title,
            R.id.tv_amount_usable, R.id.tv_toggle_see, R.id.lly_set_daily_limit_balance, R.id.tv_transfer_top_up})
    public void onViewClicked(View view) {
        if (ButtonUtils.isFastDoubleClick(view.getId(),300)) return;
        switch (view.getId()) {
            case R.id.lly_set_daily_limit_balance:
                if (null != mSmartAccountInfo) {
                    try {

                        SmartAccountAdjustDailyLimitActivity.startActivity(getActivity(), mSmartAccountInfo, mSmartAccountInfo.getHideMobile());
                    } catch (Exception e) {

                    }
                }
                break;
            case R.id.tv_toggle_see:
            case R.id.tv_balance_title:
            case R.id.tv_amount:
            case R.id.tv_balance_tody_title:
            case R.id.tv_amount_usable:
                AnalysisButtonEntity analysisButtonEntity = new AnalysisButtonEntity(ActionIdConstant.BUT_TRANSFER_FPSID_BALANCEEYE, startTime, null, PagerConstant.ADDRESS_PAGE_TRANSFER_PAGE, System.currentTimeMillis() - startTime);
                sendAnalysisEvent(analysisButtonEntity);
                setEyeStatus();
                break;
            case R.id.tv_transfer_top_up:
                onClickTopUp();
                break;
        }
    }

    //充值
    private void onClickTopUp() {
        if (mSmartAccountInfo == null) return;
        if (TextUtils.isEmpty(mSmartAccountInfo.getSmartAcLevel())) return;
        if (!TextUtils.isEmpty(mSmartAccountInfo.getRelevanceAccNo()) && mSmartAccountInfo.getSmartAcLevel().equals(MySmartAccountEntity.SMART_LEVEL_THREE)) {
            HashMap<String, Object> mHashMaps = new HashMap<>();
            mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
            mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
            mHashMaps.put(Constants.ACCTYPE, mSmartAccountInfo.getAccType());
            mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
            mHashMaps.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
            ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            topUpAccountEvent();
        }

    }

    private void topUpAccountEvent() {
        List<BankCardEntity> bankCardsBeans = mSmartAccountInfo.getBankCards();
        if (bankCardsBeans != null && bankCardsBeans.size() > 0 && !TextUtils.isEmpty(bankCardsBeans.get(0).getStatus())) {
            //Edda状态，N：没有登记过，A：登记成功 R：登记失败 1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
            // E:删除失败 C:删除成功 4:删除申请失败 9删除申请处理中 3提交删除申请成功
            if (bankCardsBeans.get(0).getEddaStatus().equals("A") || bankCardsBeans.get(0).getEddaStatus().equals("E") || bankCardsBeans.get(0).getEddaStatus().equals("3")) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.PAYMENT_METHOD, mSmartAccountInfo.getRelevanceAccNo());
                mHashMaps.put(Constants.CURRENCY, mSmartAccountInfo.getCurrency());
                mHashMaps.put(Constants.ACCTYPE, bankCardsBeans.get(0).getBankName());
                mHashMaps.put(Constants.LAVEL, mSmartAccountInfo.getSmartAcLevel());
                ActivitySkipUtil.startAnotherActivity(getActivity(), SmartAccountTopUpActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("N")) {
                onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("R")) {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            } else if (bankCardsBeans.get(0).getEddaStatus().equals("0") || bankCardsBeans.get(0).getEddaStatus().equals("1")) {
                showErrorMsgDialog(getActivity(), getString(R.string.string_edda_loading));
            } else {
                onWainningTopUpWithBindAccount(getString(R.string.string_edda_fail));
            }
        } else {
            onWainningTopUpWithBindAccount(getString(R.string.P3_D1_1_2));
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (null != cbToggleSee) {
            if (isHideEye) {
                cbToggleSee.setChecked(false);
            } else {
                isHideEye = true;
            }

        }
    }

    /**
     * s2用户绑定银行卡
     */
    private void onWainningTopUpWithBindAccount(String message) {
        String str_title = getString(R.string.P3_D1_1_1);
        String str_msg = message;
        String str_pos = getString(R.string.P3_D1_1_3);
        String str_neg = getString(R.string.string_cancel);
        AndroidUtils.showTipDialog(getActivity(), str_title, str_msg, str_pos, str_neg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getRegEddaInfo();
            }
        });
    }

    private void getRegEddaInfo() {
        mPresenter.getRegEddaInfo();
    }

    @Override
    public void getRegEddaInfoSuccess(GetRegEddaInfoEntity response) {
        HashMap<String, Object> mHashMapsLogin = new HashMap<>();
        mHashMapsLogin.put(Constants.REG_EDDA_INFO, response);
        mHashMapsLogin.put(Constants.CURRENT_PAGE_FLOW, Constants.ADDEDDA);
        mHashMapsLogin.put(Constants.DATA_PHONE_NUMBER, mSmartAccountInfo.getHideMobile());
        mHashMapsLogin.put(Constants.SMART_ACCOUNT, mSmartAccountInfo);
        mHashMapsLogin.put(Constants.PAGE_FROM_REGISTER, false);
        ActivitySkipUtil.startAnotherActivity(getActivity(), TopUpActivity.class, mHashMapsLogin, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

    @Override
    public void getRegEddaInfoError(String errorCode, String errorMsg) {
        AndroidUtils.showTipDialog(getActivity(), errorMsg);
    }

    private void setEyeStatus() {
        if (null == cbToggleSee) {
            return;
        }

        // 如果眼睛是打开的 直接关闭
        if (cbToggleSee.isChecked()) {
            cbToggleSee.setChecked(false);
            return;
        }

        //眼睛是关闭的 不需要验密 眼睛直接打开
        if (SystemInitManager.getInstance().isValidShowAccount()) {
            setCbToggleSeeCheck();
            return;
        }

        //眼睛是关闭的 需要验密 需要交易密码
        verifyPwdAction();
    }

    /**
     * 因为 取消验密的逻辑与其他地方的验密逻辑不同，独立方法单独处理
     */
    public void verifyPwdAction() {
        isHideEye = false;
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (null != cbToggleSee) {
                    setCbToggleSeeCheck();
                }
                SystemInitManager.getInstance().setValidShowAccount(true);
                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_START_TIMER, ""));
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(mContext, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    @Override
    public void initTitle() {
        if (mActivity != null) {
            mActivity.setToolBarTitle(R.string.TF2101_25_1);
            mSoftKeyboardTranslationView.initPopView(mActivity, llContent);
        }
    }

    public void getSmartAccountInfo() {
        mPresenter.getSmartAccountInfo();
    }


    private void openAccount() {
        if (null == mSmartAccountInfo) {
            mPresenter.getSmartAccountInfo();
            return;
        }
        //每次从缓存读取
        mSmartAccountInfo = CacheManagerInstance.getInstance().getMySmartAccountEntity();

        try {
            double flag = Double.parseDouble(mSmartAccountInfo.getBalance());
            if (flag < 0) {
                hideAccount();
                return;
            }
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getBalance()), true);
            tvAmount.setText(price);

            String outBal = AndroidUtils.formatPriceWithPoint(Double.parseDouble(mSmartAccountInfo.getOutavailbal()), true);
            tvAmountUsable.setText(outBal);

            ivSetDailyLimitBalance.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            hideAccount();
        }
    }

    private void hideAccount() {
        tvAmount.setText("*****");
        tvAmountUsable.setText("*****");
        ivSetDailyLimitBalance.setVisibility(View.GONE);
    }

    void getRetainBalanceInfo() {
        if (null == accountBalanceEntity) {
            return;
        }
        String str = getString(R.string.TF2101_4_8);
        str = str.replace("(HKD)", "");
        tvTransferInfoTitle.setText(str + "(" + accountBalanceEntity.getCurrency() + ")");
    }


    private void updateNextStatus() {
        if (!TextUtils.isEmpty(mEtFpsNumber.getText()) && mEtFpsNumber.getText().length() >= 7 && !TextUtils.isEmpty(mEtwdTransferAmount.getText())) {
            dealWithExceedMaxMoney(mEtwdTransferAmount.getEditText().getText().toString());
            return;
        }
        mTransferEmailNext.setEnabled(false);
        // mTransferEmailNext.setBackgroundResource(R.drawable.bg_btn_next_page_disable);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PayEventEntity event) {
        //修改限额之后要更新
        if (event.getEventType() == PayEventEntity.EVENT_PAY_SUCCESS) {

            mPresenter.getSmartAccountInfo();
        }
    }

    /**
     * @param event
     */

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MainEventEntity event) {
        if (event.getEventType() == MainEventEntity.EVENT_KEY_END_TIMER) {
            if (null != cbToggleSee) {
                cbToggleSee.setChecked(false);
            }
        } else if (event.getEventType() == MainEventEntity.EVENT_KEY_REFRESH_ACCOUNT) {
            if (null != mPresenter) {
                mPresenter.getSmartAccountInfo();
            }
        }

    }


    @Override
    protected int getLayoutId() {
        return R.layout.fmg_transfer_by_fpsid;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void getSmartAccountInfoSuccess(MySmartAccountEntity response) {
        mSmartAccountInfo = response;
        CacheManagerInstance.getInstance().setMySmartAccountEntity(mSmartAccountInfo);

        //这里也重新设置AccountBalanceEntity
        accountBalanceEntity = new AccountBalanceEntity();
        accountBalanceEntity.setBalance(mSmartAccountInfo.getOutavailbal());
        accountBalanceEntity.setCurrency(mSmartAccountInfo.getCurrency());

        getRetainBalanceInfo();

        dealWithExceedMaxMoney(mEtwdTransferAmount.getEditText().getText().toString());
        if (null != cbToggleSee && cbToggleSee.isChecked()) {
            openAccount();
        } else {
            hideAccount();
        }

        if (isEyeDateNull) {
            cbToggleSee.setChecked(true);
            isEyeDateNull = false;
        }

        ivBgWave.setVisibility(View.VISIBLE);
        llContent.setVisibility(View.VISIBLE);

    }

    @Override
    public void getSmartAccountInfoError(String errorCode, String errorMsg) {
        showErrorMsgDialog(mActivity, errorMsg);
        mSoftKeyboardTranslationView.setVisibility(View.VISIBLE);

        ivBgWave.setVisibility(View.VISIBLE);
        llContent.setVisibility(View.VISIBLE);
    }

    @Override
    public void transferPreCheckSuccess(TransferPreCheckReq req, TransferPreCheckEntity response) {
        response.setPreCheckReq(req);
        response.setTransferType(TransferConst.TRANS_FPS);
        response.setAccountNo(req.tansferToAccount);
        TransferConfirmMoneyActivity.startActivity(mActivity, response);
    }

    @Override
    public void transferPreCheckError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, ERROR_NO_REFERENCENO)) {
            inputReferenceNo();
        } else {
            showErrorMsgDialog(mActivity, errorMsg);
        }
    }
}
