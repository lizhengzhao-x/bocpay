package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.register.adapter.holder.ConfirmBindNewCardHolder;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public class ConfirmBindeNewCardAdapter extends RecyclerView.Adapter {


    private final Context context;
    private final ArrayList<BindNewCardEntity> selectCardList;


    public ConfirmBindeNewCardAdapter(Context context, ArrayList<BindNewCardEntity> selectCardList) {
        this.context = context;
        this.selectCardList = selectCardList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.item_confirm_bind_new_card, parent, false);
        return new ConfirmBindNewCardHolder(context, inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ConfirmBindNewCardHolder confirmBindNewCardHolder = (ConfirmBindNewCardHolder) holder;
        confirmBindNewCardHolder.setData(selectCardList.get(position));
    }

    @Override
    public int getItemCount() {
        return selectCardList != null ? selectCardList.size() : 0;
    }
}
