package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class GetTransferByTnxIdWithSmartProtocol extends BaseProtocol {


    private final String outtrfrefno;
    private String txnType;

    public GetTransferByTnxIdWithSmartProtocol(String outtrfrefno, String txnTypeIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.txnType = txnTypeIn;
        this.outtrfrefno = outtrfrefno;
        mUrl = "api/crossTransfer/smaOrderInfo";

    }


    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.TXNTYPE, txnType);
        mBodyParams.put(RequestParams.OUTTRFREFNO, outtrfrefno);
    }

}
