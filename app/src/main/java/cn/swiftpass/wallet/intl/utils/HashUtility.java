package cn.swiftpass.wallet.intl.utils;


import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 从一期lib中独立出来 误删除 无改动
 */
public class HashUtility {
    public static String getMD5(String data) {
        try {
            String shaValue = null;
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            sha.reset();
            sha.update(data.getBytes("UTF-8"));
            byte[] pwhash = sha.digest();
            shaValue = BaseUtils.encodeBytes(pwhash);
            return shaValue;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }
}
