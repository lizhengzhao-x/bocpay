package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * @author Jamy
 * create date on  on 2018/7/30 13:50
 * 被扫二维码信息，返回对象为 QrIsSweptEntity
 */

public class ScavengingProtocol extends BaseProtocol {
    public static final String TAG = ScavengingProtocol.class.getSimpleName();
    /**
     * 交易银行卡id，只能是数字，长度不能超过32位
     */
    String mCardId;
    /**
     * 优惠券信息
     */
    String mCouponInfo;

    String action;

    public ScavengingProtocol(String cardId, String couponInfo, String action, NetWorkCallbackListener dataCallback) {
        this.mCardId = cardId;
        this.mCouponInfo = couponInfo;
        this.action = action;
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/getQrCode";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARD_ID, mCardId);
        mBodyParams.put(RequestParams.ACTION, action);
        if (!TextUtils.isEmpty(mCouponInfo)) {
            mBodyParams.put(RequestParams.COUPONINFO, mCouponInfo);
        }

    }
}
