package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.dialog.BasePopWindow;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 积分查询列表弹框 列表查询
 */
public class ShowMoreRecordPop extends BasePopWindow {

    private TextView mId_grade_title;
    private TextView id_grade_desc;
    private ImageView mId_close_dialog;
    private String title;
    private String name;
    private MyEVoucherEntity.EvoucherItem rowsBean;
    private androidx.recyclerview.widget.RecyclerView mId_recyclerView;
    private List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> eVoucherInfos;

    public ShowMoreRecordPop(Activity activity, MyEVoucherEntity.EvoucherItem rowsBeanIn, String titleIn) {
        super(activity);
        this.rowsBean = rowsBeanIn;
        this.title = titleIn;
        initView();
    }

    private void bindViews() {
        mId_grade_title = (TextView) mContentView.findViewById(R.id.id_grade_title);
        mId_close_dialog = (ImageView) mContentView.findViewById(R.id.id_close_dialog);
        id_grade_desc = (TextView) mContentView.findViewById(R.id.id_grade_desc);
        mId_recyclerView = (androidx.recyclerview.widget.RecyclerView) mContentView.findViewById(R.id.id_recyclerView);
    }


    @Override
    protected void initView() {
        setContentView(R.layout.pop_list_ecoupon_detail);
        bindViews();
        eVoucherInfos = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        mId_recyclerView.setLayoutManager(layoutManager);
        mId_recyclerView.setAdapter(getAdapter(null));
        eVoucherInfos.addAll(rowsBean.getEVoucherInfos());
        ((IAdapter<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean>) mId_recyclerView.getAdapter()).setData(eVoucherInfos);
        mId_recyclerView.getAdapter().notifyDataSetChanged();
        mId_grade_title.setText(title);
        setWidth((int) (AndroidUtils.getScreenWidth(mActivity) * 0.8));
        setHeight((int) (AndroidUtils.getScreenHeight(mActivity) * 0.6));
        mId_close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    private CommonRcvAdapter<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> getAdapter(final List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> data) {
        return new CommonRcvAdapter<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean>(data) {

            @Override
            public Object getItemType(MyEVoucherEntity.EvoucherItem.EVoucherInfosBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new EcouponListAdapter(mActivity);
            }
        };
    }


    public void show() {
        showAtLocation(mContentView, Gravity.CENTER, Gravity.CENTER, Gravity.CENTER);
    }
}
