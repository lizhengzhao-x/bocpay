package cn.swiftpass.wallet.intl.module.guide;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.AppCallAppLinkEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.home.PreLoginActivity;
import cn.swiftpass.wallet.intl.module.register.RegisterSelActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;

/**
 * 新引导
 */
public class GuideNewActivity extends BaseCompatActivity {

    @BindView(R.id.iv_indicator_first)
    ImageView mIndicatorFirst;
    @BindView(R.id.iv_indicator_second)
    ImageView mIndicatorSecond;
    @BindView(R.id.iv_indicator_third)
    ImageView mIndicatorThird;

    @BindView(R.id.tv_skip)
    TextView mSkipTV;

    private ViewPager mNewFuncVP;

    private List<View> mArrayPage = new ArrayList<>();

    private int[] newFuncSourceId = new int[3];
    private int[] newFuncSourceEN = new int[]{R.mipmap.guide_e_11, R.mipmap.guide_e_12, R.mipmap.guide_e_13};
    private int[] newFuncSourceTC = new int[]{R.mipmap.guide_tc_11, R.mipmap.guide_tc_12, R.mipmap.guide_tc_13};
    private int[] newFuncSourceSC = new int[]{R.mipmap.guide_sc_11, R.mipmap.guide_sc_12, R.mipmap.guide_sc_13};
    private boolean isNeedAutoLogin;
//    private String appLinkUrl;

    @Override
    protected boolean notUsedToolbar() {
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        if (getIntent() != null) {
            isNeedAutoLogin = getIntent().getBooleanExtra(MainHomeActivity.IS_AUTO_LOGIN, false);
//            appLinkUrl = getIntent().getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
        }
        String lan = SpUtils.getInstance(this).getAppLanguage();
        if (AndroidUtils.isZHLanguage(lan)) {
            //判断是否展示过引导页 如果已经展示过  那么APP版本号改变 则换引导页图片
            if (CacheManagerInstance.getInstance().isShowGuidePage()) {
                newFuncSourceSC[0] = R.mipmap.guide_update_sc_1;
                newFuncSourceSC[1] = R.mipmap.guide_update_sc_2;
                newFuncSourceSC[2] = R.mipmap.guide_update_sc_3;
            }
            newFuncSourceId = newFuncSourceSC;
        } else if (AndroidUtils.isHKLanguage(lan)) {
            if (CacheManagerInstance.getInstance().isShowGuidePage()) {
                newFuncSourceTC[0] = R.mipmap.guide_update_tc_1;
                newFuncSourceTC[1] = R.mipmap.guide_update_tc_2;
                newFuncSourceTC[2] = R.mipmap.guide_update_tc_3;
            }
            newFuncSourceId = newFuncSourceTC;
        } else {
            if (CacheManagerInstance.getInstance().isShowGuidePage()) {
                newFuncSourceEN[0] = R.mipmap.guide_update_e_1;
                newFuncSourceEN[1] = R.mipmap.guide_update_e_2;
                newFuncSourceEN[2] = R.mipmap.guide_update_e_3;
            }
            newFuncSourceId = newFuncSourceEN;
        }
        mNewFuncVP = (ViewPager) findViewById(R.id.vp_new_func);
        showNewFuncRemind();

        mSkipTV.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                jump();
            }
        });
    }


    @Override
    protected int getLayoutId() {
        return R.layout.new_func_layout;
    }


    private void initNewFuncImage() {
        for (int i = 0; i < newFuncSourceId.length; i++) {
            ImageView photo = new ImageView(this);
            photo.setScaleType(ImageView.ScaleType.FIT_XY);
            photo.setImageResource(newFuncSourceId[i]);
            mArrayPage.add(photo);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null) {
            isNeedAutoLogin = intent.getBooleanExtra(MainHomeActivity.IS_AUTO_LOGIN, false);
//            appLinkUrl = intent.getStringExtra(AppCallAppLinkEntity.APP_LINK_URL);
        }
    }

    private void jump() {
        CacheManagerInstance.getInstance().setShowGuidePage(true);
        if (CacheManagerInstance.getInstance().isLogin()) {
            HashMap<String, Object> param = new HashMap<>();
            param.put(MainHomeActivity.IS_AUTO_LOGIN, isNeedAutoLogin);
//            param.put(AppCallAppLinkEntity.APP_LINK_URL, appLinkUrl);
//            param.put(Constants.ISUPDATETOJANUARYVERSION, BocLaunchSDK.checkIsUpdateToJanuaryVersion(this));
//            if (null != getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS)) {
//                param.put(RED_PACKET_RED_PACKET_PARAMS, getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
//            }
            ActivitySkipUtil.startAnotherActivity(this, MainHomeActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        } else {
            HashMap<String, Object> param = new HashMap<>();
//            param.put(AppCallAppLinkEntity.APP_LINK_URL, appLinkUrl);
//            if (null != getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS)) {
//                param.put(RED_PACKET_RED_PACKET_PARAMS, getIntent().getSerializableExtra(RED_PACKET_RED_PACKET_PARAMS));
//            }
            ActivitySkipUtil.startAnotherActivity(this, PreLoginActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        }
        //引导页显示完成之后要更新本地状态
        SpUtils.getInstance().saveShowGuidePage(true);
        finish();
    }

    private void jumpToRegister() {
        CacheManagerInstance.getInstance().setShowGuidePage(true);
//        HashMap<String, Object> param = new HashMap<>();
//        param.put(AppCallAppLinkEntity.APP_LINK_URL, appLinkUrl);
//        ActivitySkipUtil.startAnotherActivity(this, PreLoginActivity.class, param, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

        ActivitySkipUtil.startAnotherActivity(this, RegisterSelActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
        //引导页显示完成之后要更新本地状态
        SpUtils.getInstance().saveShowGuidePage(true);
        finish();
    }

    private void showNewFuncRemind() {
        mArrayPage.clear();
        initNewFuncImage();
        mNewFuncVP.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return mArrayPage.size();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }

            @Override
            public void destroyItem(ViewGroup container, int position,
                                    Object object) {
                container.removeView(mArrayPage.get(position));
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                container.addView(mArrayPage.get(position));
                return mArrayPage.get(position);
            }
        });
        mNewFuncVP.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                updateIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mNewFuncVP.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getY() > AndroidUtils.getScreenHeight(GuideNewActivity.this) * 0.9) {
                        if (mNewFuncVP.getCurrentItem() == mArrayPage.size() - 1) {
                            //点击跳转不同
                            if (CacheManagerInstance.getInstance().isShowGuidePage()) {
                                //老用户
                                jump();
                            } else {
                                //新用户
                                jumpToRegister();
                            }
                        } else {
                            //下一步 下个界面
                            mNewFuncVP.setCurrentItem(mNewFuncVP.getCurrentItem() + 1);
                        }
                    }
                }
                return false;
            }
        });
    }

    private void updateIndicator(int postion) {
        if (postion == 0) {
            mIndicatorFirst.setImageResource(R.mipmap.img_guide_circle_now);
            mIndicatorSecond.setImageResource(R.mipmap.img_guide_circle_default);
            mIndicatorThird.setImageResource(R.mipmap.img_guide_circle_default);

        } else if (postion == 1) {
            mIndicatorFirst.setImageResource(R.mipmap.img_guide_circle_default);
            mIndicatorSecond.setImageResource(R.mipmap.img_guide_circle_now);
            mIndicatorThird.setImageResource(R.mipmap.img_guide_circle_default);
        } else if (postion == 2) {
            mIndicatorFirst.setImageResource(R.mipmap.img_guide_circle_default);
            mIndicatorSecond.setImageResource(R.mipmap.img_guide_circle_default);
            mIndicatorThird.setImageResource(R.mipmap.img_guide_circle_now);
        }
    }
}
