package cn.swiftpass.wallet.intl.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/5 15:11
 * @change
 * @chang time
 * @class describe
 */
public class ChatEntity extends BaseEntity {
    public String status;
    public String language;
    public String accessToken;
    public String resultFlag;
    public String returnCode;
}
