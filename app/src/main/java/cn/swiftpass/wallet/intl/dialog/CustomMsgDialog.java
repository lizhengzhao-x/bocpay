package cn.swiftpass.wallet.intl.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.LogUtils;


public class CustomMsgDialog extends Dialog {

    private String message;

    public CustomMsgDialog(Context context) {
        super(context);
    }

    public CustomMsgDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        Window window = getWindow();
        if (hasFocus && window != null) {
            View decorView = window.getDecorView();
            if (decorView.getHeight() == 0 || decorView.getWidth() == 0) {
                decorView.requestLayout();
                LogUtils.i("CustomMsgDialog", "ERROR onWindowFocusChanged");
            }
        }
    }

    public static class Builder {
        private Context context;
        private String message;
        private String title;
        private String positiveButtonText;
        private String negativeButtonText;
        private View contentView;
        private OnClickListener positiveButtonClickListener;
        private int leftColor;
        private int rightColor;
        private int textSize;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setMessageTextSize(int textSize) {
            this.textSize = textSize;
            return this;
        }


        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        public Builder setMessage(int message) {
            this.message = (String) context.getText(message);
            return this;
        }



        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        //gravity
        int gravity;

        public Builder setGravity(int gravity) {
            this.gravity = gravity;
            return this;
        }

        public Builder setContentView(View v) {
            this.contentView = v;
            return this;
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        public Builder setPositiveButton(int positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = (String) context.getText(positiveButtonText);
            this.positiveButtonClickListener = listener;
            return this;
        }

        public Builder setPositiveButton(String positiveButtonText, OnClickListener listener) {
            this.positiveButtonText = positiveButtonText;
            this.positiveButtonClickListener = listener;
            return this;
        }


        public CustomMsgDialog create() {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // instantiate the dialog with the custom Theme
            final CustomMsgDialog dialog = new CustomMsgDialog(context, R.style.Dialog_No_KeyBoard);
            View layout = inflater.inflate(R.layout.dialog_msg_layout, null);
            dialog.addContentView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            // set the dialog title
            // set the confirm button
            if (positiveButtonText != null) {
                ((Button) layout.findViewById(R.id.positive_dl)).setText(positiveButtonText);
                if (positiveButtonClickListener != null) {
                    layout.findViewById(R.id.positive_dl).setOnClickListener(new View.OnClickListener() {
                        public void onClick(View v) {
                            positiveButtonClickListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                        }
                    });
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById(R.id.positive_dl).setVisibility(View.GONE);
            }
            // set the content message
            if (message != null) {
                if (textSize != 0) {
                    ((TextView) layout.findViewById(R.id.message_dl)).setTextSize(textSize);
                }
                ((TextView) layout.findViewById(R.id.message_dl)).setText(message.trim());
                if (0 != gravity) {
                    ((TextView) layout.findViewById(R.id.message_dl)).setGravity(gravity);
                }
            }
            if (title != null) {
                ((TextView) layout.findViewById(R.id.id_title)).setText(title);
                ((LinearLayout) layout.findViewById(R.id.id_center_view)).setBackgroundResource(R.drawable.dialog_center_bg);
            } else {
                ((LinearLayout) layout.findViewById(R.id.id_title_layout)).setVisibility(View.GONE);
            }
            dialog.setMessage(message);
            dialog.setContentView(layout);
            return dialog;
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

