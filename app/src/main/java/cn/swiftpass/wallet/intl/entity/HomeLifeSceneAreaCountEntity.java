package cn.swiftpass.wallet.intl.entity;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.httpcore.entity.BaseEntity;


public class HomeLifeSceneAreaCountEntity extends BaseEntity {

    private ArrayList<HomeLifeMenuCountEntity>lifeSceneAreaCount=new ArrayList<>();
    private HashMap<String,Integer> lifeMenuMap=new HashMap<>();

    public ArrayList<HomeLifeMenuCountEntity> getLifeSceneAreaCount() {
        return lifeSceneAreaCount;
    }

    public void setLifeSceneAreaCount(ArrayList<HomeLifeMenuCountEntity> lifeSceneAreaCount) {
        this.lifeSceneAreaCount = lifeSceneAreaCount;
    }

    public HashMap<String, Integer> getLifeMenuMap() {
        if (lifeSceneAreaCount!=null&&lifeSceneAreaCount.size()>0){
            for (HomeLifeMenuCountEntity lifeMenuCountEntity : lifeSceneAreaCount) {
                if (!TextUtils.isEmpty(lifeMenuCountEntity.getMenuKey())){
                    lifeMenuMap.put(lifeMenuCountEntity.getMenuKey(), Math.max(lifeMenuCountEntity.getCount(), 0));
                }
            }
        }
        return lifeMenuMap;
    }

    public void setLifeMenuMap(HashMap<String, Integer> lifeMenuMap) {
        this.lifeMenuMap = lifeMenuMap;
    }
}
