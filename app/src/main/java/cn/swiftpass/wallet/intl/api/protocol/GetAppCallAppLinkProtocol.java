package cn.swiftpass.wallet.intl.api.protocol;


import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * app call app 获取跳转连接
 */
public class GetAppCallAppLinkProtocol extends BaseProtocol {
    String token;
    String checkSum;
    String digest;
    String url;

    public GetAppCallAppLinkProtocol(String token, String checkSum, String digest, String url, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.token = token;
        this.checkSum = checkSum;
        this.digest = digest;
        this.url = url;
        mUrl = "api/sysconfig/getAppCallAppLink";
        mEncryptFlag = false;

    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(token)) {
            mBodyParams.put(RequestParams.TOKEN, token);
        }
        if (!TextUtils.isEmpty(checkSum)) {
            mBodyParams.put(RequestParams.CHECKSUM, checkSum);
        }
        if (!TextUtils.isEmpty(digest)) {
            mBodyParams.put(RequestParams.DIGEST, digest);
        }
        if (!TextUtils.isEmpty(url)) {
            mBodyParams.put(RequestParams.URL, url);
        }

    }

}
