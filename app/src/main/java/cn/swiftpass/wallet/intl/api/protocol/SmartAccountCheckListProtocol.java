package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.entity.SmartAccountListEntity;

/**
 * @author create date on  on 2018/9/3 13:50
 * 获取我的账户主账号列表
 * e2ee
 */

public class SmartAccountCheckListProtocol extends BaseProtocol {
    public static final String TAG = SmartAccountCheckListProtocol.class.getSimpleName();


    public SmartAccountCheckListProtocol(NetWorkCallbackListener<SmartAccountListEntity> dataCallback) {
        mUrl = "api/smartAccountManager/getAccountNos";
        mDataCallback = dataCallback;
    }
}
