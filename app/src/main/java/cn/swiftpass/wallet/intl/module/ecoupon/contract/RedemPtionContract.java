package cn.swiftpass.wallet.intl.module.ecoupon.contract;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.CheckCcEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemResponseEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemableGiftListEntity;

/**
 * 换领电子券
 */
public class RedemPtionContract {

    public interface View extends IView {

        void checkCCardSuccess(CheckCcEntity response);

        void checkCCardError(String errorCode, String errorMsg);

        void showRedeemGiftListInfo(String errorCode, String errorMsg, RedeemableGiftListEntity o);
    }


    public interface Presenter extends IPresenter<RedemPtionContract.View> {

        void redeemGiftCheckStock(String giftTp, List<RedeemResponseEntity.ResultRedmBean> redmBeansIn);

        void getRedeemGiftList();
    }

}
