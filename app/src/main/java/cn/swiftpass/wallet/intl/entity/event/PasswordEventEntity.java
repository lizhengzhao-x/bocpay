package cn.swiftpass.wallet.intl.entity.event;


public class PasswordEventEntity extends BaseEventEntity{

    public static final int EVENT_FORGOT_PASSWORD = 1001;

    public static final int EVENT_RESET_PWD_SUCCESS = 1003;

    public static final int EVENT_CLOSE_VERIFY_PASSWORD = 91;


    public PasswordEventEntity(int eventType, String message) {
        super(eventType, message);
    }

    public PasswordEventEntity(int eventType, String message, String errorCodeIn) {
        super(eventType, message, errorCodeIn);
    }
}
