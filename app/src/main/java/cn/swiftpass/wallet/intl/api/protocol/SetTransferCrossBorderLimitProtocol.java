package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class SetTransferCrossBorderLimitProtocol extends BaseProtocol {

    private final int limit;

    public SetTransferCrossBorderLimitProtocol(int limit, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/crossTransfer/settingDailyLimit";
        this.limit = limit;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.DAILYLIMIT, limit);
    }


}
