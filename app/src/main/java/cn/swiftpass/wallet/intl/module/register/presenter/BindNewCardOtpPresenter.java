package cn.swiftpass.wallet.intl.module.register.presenter;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.api.protocol.BindNewCardListProtocol;
import cn.swiftpass.wallet.intl.api.protocol.BindNewCardVerifyOtpProtocol;
import cn.swiftpass.wallet.intl.api.protocol.BindeNewCardSendOtpProtocol;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardOtpContract;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;


public class BindNewCardOtpPresenter extends BasePresenter<BindNewCardOtpContract.View> implements BindNewCardOtpContract.Presenter {
    @Override
    public void bindNewCardSendOtp(ArrayList<BindNewCardEntity> newCardEntities, String action, final boolean isTryAgain) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BindeNewCardSendOtpProtocol(newCardEntities, action, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardSendOtpError(errorCode, errorMsg, isTryAgain);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardSendOtpSuccess(response, isTryAgain);
                }
            }
        }).execute();

    }

    @Override
    public void bindNewCardVerifyOtp(String action, String verifyCode) {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BindNewCardVerifyOtpProtocol(action, verifyCode, new NetWorkCallbackListener<BaseEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardVerifyOtpError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BaseEntity response) {
                if (getView() != null) {
                    //接下来会发起bindNewCardList 不需要隐藏loading
                    getView().bindNewCardVerifyOtpSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void bindNewCardList(ArrayList<BindNewCardEntity> selectCardList, String action) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new BindNewCardListProtocol(selectCardList, action, new NetWorkCallbackListener<BindNewCardResultEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardListError(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(BindNewCardResultEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().bindNewCardListSuccess(response);
                }
            }
        }).execute();

    }


}
