package cn.swiftpass.wallet.intl.module.login.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.CheckCreditCardProtocol;
import cn.swiftpass.wallet.intl.api.protocol.GetSmartLevelForBindProtocol;
import cn.swiftpass.wallet.intl.entity.RegSucBySmartEntity;
import cn.swiftpass.wallet.intl.entity.SmartLevelEntity;
import cn.swiftpass.wallet.intl.module.login.contract.SelRegisterTypeContract;

public class SelRegisterPresenter extends BasePresenter<SelRegisterTypeContract.View> implements SelRegisterTypeContract.Presenter {
    @Override
    public void getSmartLevelForBind() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetSmartLevelForBindProtocol(new NetWorkCallbackListener<SmartLevelEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartLevelForBindFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(SmartLevelEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getSmartLevelForBindSuccess(response);
                }
            }
        }).execute();
    }

    @Override
    public void checkCreditCardNum() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new CheckCreditCardProtocol(new NetWorkCallbackListener<RegSucBySmartEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkCreditCardNumFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(RegSucBySmartEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkCreditCardNumSuccess(response);
                }
            }
        }).execute();
    }
}
