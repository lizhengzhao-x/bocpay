package cn.swiftpass.wallet.intl.utils;


import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.SparseArray;

import androidx.room.util.StringUtil;

import com.github.promeg.pinyinhelper.Pinyin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.InviteContactEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.module.redpacket.entity.UnOpenLiShiListEntity;

/**
 * 对比本地通讯录得到有效电话
 */
public class ContactCompareUtils {
    private static List<InviteContactEntity> allContactList;


    //清空通讯录
    public static void clearAllContactList() {
        allContactList = null;
    }

    public static List<InviteContactEntity> getValidData(ArrayList<ContactEntity> contactEntities) {
        List<InviteContactEntity> allContactList = new ArrayList<>();
        //去除所有特殊符号与空格，只认可13位、11位、8位纯数字为香港\澳门\大陆电话号码；
        //② 如果为13位数字，必须为86开头，且第三位数字为1，否则认定为非电话号码；
        //③如果为11位数字，
        //        i. 若为1开头，则为大陆电话号码；
        //        ii. 若为852\853开头，检验第四位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
        //④ 如果为8位数字； 检验第1位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；

        for (ContactEntity contactEntity : contactEntities) {
            InviteContactEntity inviteContactEntity = new InviteContactEntity();
            String number = contactEntity.getNumber();
            //去除所有特殊符号与空格 先只处理-/+/(/)/空格
            number = number.
                    replace("-", "").
                    replace("+", "").
                    replace(" ", "").
                    replace("(", "").
                    replace(")", "");
            //如果是邮箱也要返回
            if (!TextUtils.isEmpty(number)) {
                if (number.contains("@")) {
                    //香港\澳门手机号码；
                    inviteContactEntity.setAccount(contactEntity.getNumber());
                    inviteContactEntity.setName(contactEntity.getUserName());
                    allContactList.add(inviteContactEntity);
                }
            }

            if (!TextUtils.isEmpty(number) && isNumeric(number)) {
                //只认可13位、11位、8位纯数字为香港\澳门\大陆电话号码；
                if (number.length() == 8 || number.length() == 11 || number.length() == 13) {
                    if (number.length() == 13) {
                        //如果为13位数字，必须为86开头，且第三位数字为1，否则认定为非电话号码；
                        if (number.startsWith("861")) {
                            isZhNumber(inviteContactEntity, contactEntity, allContactList);
                        } else {
                            LogUtils.i("DEAL:", "invailable " + number);
                        }
                    } else if (number.length() == 11) {
                        // ii. 若为852\853开头，检验第四位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
                        if (number.startsWith("852") || number.startsWith("853")) {
                            isHkPhoneNumber(number, inviteContactEntity, contactEntity, allContactList);
                        } else if (number.startsWith("1")) {
                            // i. 若为1开头，则为大陆电话号码；
                            isZhNumber(inviteContactEntity, contactEntity, allContactList);
                        } else {
                            LogUtils.i("DEAL:", "invailable " + number);
                        }
                    } else {
                        //如果为8位数字； 检验第1位数字是否为0、1、2、3，若非0\1\2\3，则可判定为香港\澳门手机号码；
                        isHkPhoneNumberFirst(number, inviteContactEntity, contactEntity, allContactList);
                    }
                } else {
                    LogUtils.i("DEAL:", "invailable " + number);
                }
            } else {
                LogUtils.i("DEAL:", "invailable " + number);
            }
        }
        return allContactList;
    }

    /**
     * 是否是纯数字
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    private static void isZhNumber(InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        inviteContactEntity.setAccount(contactEntity.getNumber());
        inviteContactEntity.setName(contactEntity.getUserName());
        allContactList.add(inviteContactEntity);
    }

    private static void isHkPhoneNumberFirst(String number, InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        if (TextUtils.equals(number.charAt(0) + "", "0") ||
                TextUtils.equals(number.charAt(0) + "", "1") ||
                TextUtils.equals(number.charAt(0) + "", "2") ||
                TextUtils.equals(number.charAt(0) + "", "3")) {
            //非法手机号
            LogUtils.i("DEAL:", "invailable " + number);
        } else {
            //香港\澳门手机号码；
            inviteContactEntity.setAccount(contactEntity.getNumber());
            inviteContactEntity.setName(contactEntity.getUserName());
            allContactList.add(inviteContactEntity);
        }
    }

    private static void isHkPhoneNumber(String number, InviteContactEntity inviteContactEntity, ContactEntity contactEntity, final List<InviteContactEntity> allContactList) {
        if (TextUtils.equals(number.charAt(3) + "", "0") ||
                TextUtils.equals(number.charAt(3) + "", "1") ||
                TextUtils.equals(number.charAt(3) + "", "2") ||
                TextUtils.equals(number.charAt(3) + "", "3")) {
            //非法手机号
            LogUtils.i("DEAL:", "invailable " + number);
        } else {
            //香港\澳门手机号码；
            inviteContactEntity.setAccount(contactEntity.getNumber());
            inviteContactEntity.setName(contactEntity.getUserName());
            allContactList.add(inviteContactEntity);
        }
    }

    /**
     * 手机号码如果在本地通讯录有 那么就用通讯录姓名
     *
     * @param redPacketBeanEntities
     * @return
     */
    public static void compareUnOpenListHisotryLocalRecord(Activity mActivity, List<UnOpenLiShiListEntity.RowsBean> rowsBeans, CompareRowsResult compareRowsResult) {
        if (null == mActivity) {
            return;
        }
        //单独线程来处理 防止阻塞 华为手机上5000条通讯录会有明显卡顿
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null == allContactList) {
                    allContactList = getValidData(ContactUtils.getInstance().getAllContacts(mActivity, true));
                }

                if (null == allContactList || allContactList.size() == 0) {
                    for (UnOpenLiShiListEntity.RowsBean rowsBean :
                            rowsBeans) {
                        rowsBean.setName(AndroidUtils.getHistoryRedNameEllipStr(rowsBean.getName()));
                    }
                    if(null != mActivity) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compareRowsResult.getRedPackets(rowsBeans);
                            }
                        });
                    }

                    return;
                }

                for (UnOpenLiShiListEntity.RowsBean rowsBean :
                        rowsBeans) {
                    String name = rowsBean.getName();
                    for (InviteContactEntity inviteContactEntity :
                            allContactList) {
                        //得到通讯录名称 如果通讯录里不存在 就返回后台name
                        String flag = compareHisotryLocalRecord(inviteContactEntity, rowsBean.getPhone(), rowsBean.getEmail(), rowsBean.getName());

                        //如果比对到了就退出循环
                        if (!TextUtils.equals(flag, name)) {
                            name = flag;
                            break;
                        }
                    }
                    //名称缩略
                    rowsBean.setName(AndroidUtils.getHistoryRedNameEllipStr(name));
                }
                if(null != mActivity) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            compareRowsResult.getRedPackets(rowsBeans);
                        }
                    });
                }
            }
        }).run();

    }

    /**
     * 手机号码如果在本地通讯录有 那么就用通讯录姓名
     *
     * @param redPacketBeanEntities
     * @return
     */
    public static void compareHisotryLocalRecord(Activity mActivity, List<RedPacketBeanEntity> redPacketBeanEntities, CompareRedPacketsListResult compareResult) {
        if (null == mActivity) {
            return;
        }
        //单独线程来处理 防止阻塞 华为手机上5000条通讯录会有明显卡顿
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null == allContactList) {
                    allContactList = getValidData(ContactUtils.getInstance().getAllContacts(mActivity, true));
                }

                if (null == allContactList || allContactList.size() == 0) {
                    for (RedPacketBeanEntity redPacketBeanEntity :
                            redPacketBeanEntities) {
                        redPacketBeanEntity.setName(AndroidUtils.getHistoryRedNameEllipStr(redPacketBeanEntity.getName()));
                    }
                    if(null != mActivity){
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compareResult.getRedPackets(redPacketBeanEntities);
                            }
                        });
                    }


                    return;
                }

                for (RedPacketBeanEntity redPacketBeanEntity :
                        redPacketBeanEntities) {
                    String name = redPacketBeanEntity.getName();
                    for (InviteContactEntity inviteContactEntity :
                            allContactList) {
                        //得到通讯录名称 如果通讯录里不存在 就返回后台name
                        String flag = compareHisotryLocalRecord(inviteContactEntity, redPacketBeanEntity.getPhone(), redPacketBeanEntity.getEmail(), redPacketBeanEntity.getName());

                        //如果比对到了就退出循环
                        if (!TextUtils.equals(flag, name)) {
                            name = flag;
                            break;
                        }
                    }
                    //名称缩略
                    redPacketBeanEntity.setName(AndroidUtils.getHistoryRedNameEllipStr(name));
                }
                if(null != mActivity) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            compareResult.getRedPackets(redPacketBeanEntities);
                        }
                    });
                }

            }
        }).run();

    }

    public static String compareHisotryLocalRecord(InviteContactEntity inviteContactEntity, String phone, String email, String name) {
        if (null == inviteContactEntity) {
            return name;
        }

        String localName = name;
        //取出区号 进行手机对比
        if (!TextUtils.isEmpty(phone)) {
            if (phone.contains("-")) {
                String[] strings = phone.split("-");
                if (TextUtils.equals(strings[1], inviteContactEntity.getAccount())) {
                    localName = inviteContactEntity.getName();
                }
            } else {
                if (TextUtils.equals(phone, inviteContactEntity.getAccount())) {
                    localName = inviteContactEntity.getName();
                }
            }
        }


        if (!TextUtils.isEmpty(email)) {
            if (TextUtils.equals(email, inviteContactEntity.getAccount())) {
                localName = inviteContactEntity.getName();
            }
        }
        return localName;

    }

    /**
     * 手机号码如果在本地通讯录有 那么就用通讯录姓名
     *
     * @param redPacketBeanEntities
     * @return
     */
    public static void compareHisotryLocalRecord(Activity mActivity, String phone, String email, String name, CompareSingleResult compareResult) {
        if (null == mActivity) {
            compareResult.getStringName(name);
            return;
        }

        //单独线程来处理 防止阻塞 华为手机上5000条通讯录会有明显卡顿
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null == allContactList) {
                    allContactList = getValidData(ContactUtils.getInstance().getAllContacts(mActivity, true));
                }

                if (null == allContactList || allContactList.size() == 0) {
                    if(null != mActivity) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                compareResult.getStringName(name);
                            }
                        });
                    }

                    return;
                }
                String localName = name;
                for (InviteContactEntity inviteContactEntity :
                        allContactList) {
                    String flag = compareHisotryLocalRecord(inviteContactEntity, phone, email, name);
                    //如果比对到了就退出循环
                    if (!TextUtils.equals(flag, localName)) {
                        localName = flag;
                        break;
                    }
                }
                String finalLocalName = localName;
                if(null != mActivity) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            compareResult.getStringName(finalLocalName);
                        }
                    });
                }
            }
        }).run();
    }


    public interface CompareRedPacketsListResult {
        void getRedPackets(List<RedPacketBeanEntity> redPacketBeanEntities);
    }

    public interface CompareSingleResult {
        void getStringName(String name);
    }

    public interface CompareRowsResult {
        void getRedPackets(List<UnOpenLiShiListEntity.RowsBean> rowsBeans);
    }
}
