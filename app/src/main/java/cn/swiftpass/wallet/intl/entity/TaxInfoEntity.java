package cn.swiftpass.wallet.intl.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

public class TaxInfoEntity extends BaseEntity {

    private TaxResidentEntity taxResidence;




    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public TaxInfoReason getReason() {
        return reason;
    }

    public void setReason(TaxInfoReason reason) {
        this.reason = reason;
    }

    public TaxResidentEntity getTaxResidence() {
        return taxResidence;
    }

    public void setTaxResidence(TaxResidentEntity taxResidence) {
        this.taxResidence = taxResidence;
    }


    private String taxNumber;
    private TaxInfoReason reason;

}
