package cn.swiftpass.wallet.intl.module.setting.about;


import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.api.protocol.ActivityPromotionProtocol;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.ActivityEntity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.PromotionEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.SmartAccountInfoSetActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.setting.help.HelpListAdapter;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.DialogUtils;
import cn.swiftpass.wallet.intl.utils.SpUtils;
import cn.swiftpass.wallet.intl.widget.RecyclerViewForEmpty;

import static cn.swiftpass.wallet.intl.entity.Constants.DETAIL_TITLE;

/**
 * Created by  on 2018/1/15.
 */

public class ServerAgreementActivity extends BaseCompatActivity {


    private static final int POSITION_CONTACT = 1;
    @BindView(R.id.id_recyclerview)
    RecyclerViewForEmpty idRecyclerview;
    @BindView(R.id.id_nodata_image)
    ImageView idNodataImage;
    private List<PromotionEntity.PromotionBean> promotionBeans;
    @BindView(R.id.id_empty_view)
    View id_empty_view;
    @BindView(R.id.id_empty_activity)
    TextView idEmptyActivity;
    @BindView(R.id.tv_agreement_remark)
    TextView tvAgreementRemark;


    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_server_agreement;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(mContext.getString(R.string.VC02_01a_5));
        if (getIntent() != null) {
            String title = getIntent().getStringExtra(DETAIL_TITLE);
            if (!TextUtils.isEmpty(title)) {
                setToolBarTitle(title);
            }
        }
        promotionBeans = new ArrayList<>();
        initView();
    }

    private void initView() {
        idNodataImage.setVisibility(View.INVISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        layoutManager.setRecycleChildrenOnDetach(true);
        idRecyclerview.setLayoutManager(layoutManager);
        idRecyclerview.setAdapter(getAdapter(null));
        promotionBeans = new ArrayList<>();
        idEmptyActivity.setText(" ");
        idRecyclerview.setEmptyView(id_empty_view);
        // 设置新的数据
        ((IAdapter<PromotionEntity.PromotionBean>) idRecyclerview.getAdapter()).setData(promotionBeans);
        idRecyclerview.getAdapter().notifyDataSetChanged(); // 通知数据刷新
        getHelpDataRequest();
        id_empty_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHelpDataRequest();
            }
        });
    }

    /**
     * 获取发现列表
     */
    private void getHelpDataRequest() {
        if (!NetworkUtil.isNetworkAvailable()) {
            showErrorMsgDialog(getActivity(), getString(R.string.string_network_available), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    finish();
                }
            });
        } else {
            ApiProtocolImplManager.getInstance().showDialog(getActivity());

            String url_params = ApiConstant.ABOUT_LIST;
            if (getIntent().getExtras() != null) {
                url_params = getIntent().getExtras().getString(Constants.DETAIL_URL_PARAMS, ApiConstant.ABOUT_LIST);
            }
            ApiProtocolImplManager.getInstance().getActivitiesList(url_params, mContext, new NetWorkCallbackListener<ActivityEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    ApiProtocolImplManager.getInstance().dismissDialog();
                    showErrorMsgDialog(ServerAgreementActivity.this, errorMsg);
                }

                @Override
                public void onSuccess(ActivityEntity response) {
                    getHelpList(response.getValue());

                    if (!TextUtils.isEmpty(response.getRemark())) {
                        try {
                            String temp = response.getRemark().replace("\\n", "\n");

                            tvAgreementRemark.setText(temp);
                        } catch (Exception e) {
                            tvAgreementRemark.setText(response.getRemark());
                        }

                    }
                }
            });
        }
    }

    /**
     * 获取活动列表json文件 url
     *
     * @param url
     */
    private void getHelpList(String url) {
        NetWorkCallbackListener listener = new NetWorkCallbackListener<PromotionEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                ApiProtocolImplManager.getInstance().dismissDialog();
            }

            @Override
            public void onSuccess(PromotionEntity response) {
                ApiProtocolImplManager.getInstance().dismissDialog();
                if (null == response) {
                    return;
                }
                //广告列表 根据不同语言做出筛选
                String lan = SpUtils.getInstance(ServerAgreementActivity.this).getAppLanguage();
                if (AndroidUtils.isZHLanguage(lan)) {
                    if (response.getZh_CN() != null && response.getZh_CN().getPromotion() != null) {
                        promotionBeans.addAll(response.getZh_CN().getPromotion());
                    }
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    if (response.getZh_HK() != null && response.getZh_HK().getPromotion() != null) {
                        promotionBeans.addAll(response.getZh_HK().getPromotion());
                    }
                } else {
                    if (response.getEn_US() != null && response.getEn_US().getPromotion() != null) {
                        promotionBeans.addAll(response.getEn_US().getPromotion());
                    }
                }


                idRecyclerview.getAdapter().notifyDataSetChanged();
            }
        };
        new ActivityPromotionProtocol(url, listener).execute();
    }


    private CommonRcvAdapter<PromotionEntity.PromotionBean> getAdapter(final List<PromotionEntity.PromotionBean> data) {
        return new CommonRcvAdapter<PromotionEntity.PromotionBean>(data) {

            @Override
            public Object getItemType(PromotionEntity.PromotionBean demoModel) {
                return demoModel.getType();
            }

            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new HelpListAdapter(ServerAgreementActivity.this, new HelpListAdapter.MessageItemCallback() {
                    @Override
                    public void onMessageItemCallback(final int position) {
                        super.onMessageItemCallback(position);
                        if (null != promotionBeans && null != promotionBeans.get(position)) {
                            if (TextUtils.equals(promotionBeans.get(position).getIsJsonConnect(), "Y")) {
                                HashMap<String, Object> mHashMaps = new HashMap<>();
                                mHashMaps.put(Constants.DETAIL_URL_PARAMS, ApiConstant.APP_VIRTUALCARD_URL);
                                mHashMaps.put(DETAIL_TITLE, promotionBeans.get(position).getTitle());
                                ActivitySkipUtil.startAnotherActivity(ServerAgreementActivity.this, ServerAgreementActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                            } else {
                                startToOtherActivity(promotionBeans.get(position).getUrl(), promotionBeans.get(position).getTitle());
                            }

                        }
                    }
                });
            }
        };
    }


    private void startToOtherActivity(String url, String title) {
        if (url.endsWith(".pdf")) {
            //解决pdf文件结尾 webview无法加载的问题
            AndroidUtils.openUrl(mContext, url);
            return;
        }

        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, url);
        mHashMaps.put(DETAIL_TITLE, title);
        mHashMaps.put(WebViewActivity.NEW_MSG, true);
        ActivitySkipUtil.startAnotherActivity(ServerAgreementActivity.this, WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

    }

}
