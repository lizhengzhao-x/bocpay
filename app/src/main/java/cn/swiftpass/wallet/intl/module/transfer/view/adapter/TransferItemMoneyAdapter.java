package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * 派利是 转账金额
 */
public class TransferItemMoneyAdapter implements AdapterItem<String> {

    private TextView id_sel_text_money;
    private Context mContext;
    private OnIemMoneyClickCallback onIemMoneyClickCallback;
    private int position;


    public TransferItemMoneyAdapter(Context contextIn, OnIemMoneyClickCallback onIemMoneyClickCallbackIn) {
        mContext = contextIn;
        onIemMoneyClickCallback = onIemMoneyClickCallbackIn;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.transfer_item_sel_money;
    }

    @Override
    public void bindViews(View root) {
        id_sel_text_money = root.findViewById(R.id.id_sel_text_money);
    }

    @Override
    public void setViews() {
        int itemMargin = AndroidUtils.dip2px(mContext, 20);
        int width = (AndroidUtils.getScreenWidth(mContext) - itemMargin * 5) / 5;
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) id_sel_text_money.getLayoutParams();
        lp.leftMargin = itemMargin;
        lp.width = width;
        lp.height = width * 43/64;
        id_sel_text_money.setLayoutParams(lp);
        id_sel_text_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onIemMoneyClickCallback.onItemClick(position);
            }
        });
    }

    @Override
    public void handleData(String itemMoney, int positionIn) {
        position = positionIn;
        id_sel_text_money.setText("$" + itemMoney);
    }

    public static class OnIemMoneyClickCallback {
        public void onItemClick(int position) {
            // do nothing
        }
    }
}
