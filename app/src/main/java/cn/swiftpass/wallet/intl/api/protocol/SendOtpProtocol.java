package cn.swiftpass.wallet.intl.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

import static cn.swiftpass.wallet.intl.entity.ApiConstant.FLOW_TRANSFER_CROSS_BORDER;

/**
 * @class name：LoginSendOtpProtocol
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/14 20:32
 * @change
 * @chang time
 * @class 登录OTP
 */
public class SendOtpProtocol extends BaseProtocol {


    /**
     * 用户id
     */
    String walletId;
    String mobile;
    /**
     * 标识取【L】：B：绑定检查；V：仅验证；F：忘记密码，L：登录；
     * M：修改限额，增值方式
     * S：暂停智能账户
     * R：重新激活
     * C：注销智能账户
     * LOGIN:登陆
     * V：注册
     * F：忘记密码
     * FIO：fio
     */
    String mAction;


    public SendOtpProtocol(String walletId, String mobile, String action, NetWorkCallbackListener dataCallback) {
        this.walletId = walletId;
        this.mobile = mobile;
        this.mAction = action;
        this.mDataCallback = dataCallback;
        if (FLOW_TRANSFER_CROSS_BORDER.equals(mAction)) {
            mUrl = "api/crossTransfer/sendOTP";
        } else {
            mUrl = "api/otp/send";
        }
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.WALLETID, walletId);
        mBodyParams.put(RequestParams.MOBILE, mobile);
        mBodyParams.put(RequestParams.ACTION, mAction);

    }
}
