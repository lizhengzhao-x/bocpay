package cn.swiftpass.wallet.intl.module.home.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.GetNewMsgDataProtocol;
import cn.swiftpass.wallet.intl.entity.NewMsgListEntity;
import cn.swiftpass.wallet.intl.module.home.contract.NewMsgContract;

public class NewMsgPresenter extends BasePresenter<NewMsgContract.View> implements NewMsgContract.Presenter {

//    @Override
//    public void getMsgList(String url) {
//        new ActivityPromotionProtocol(url, new NetWorkCallbackListener<PromotionEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView() != null) {
//                    getView().getActListFail(errorCode, errorMsg);
//                }
//
//
//            }
//
//            @Override
//            public void onSuccess(PromotionEntity promotionEntity) {
//                getView().getMsgListSuccess(promotionEntity);
//            }
//        }).execute();
//    }

    @Override
    public void getMsgList() {
        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new GetNewMsgDataProtocol(false, new NetWorkCallbackListener<NewMsgListEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().getActListFail(errorCode, errorMsg);
                    getView().dismissDialog();
                }
            }

            @Override
            public void onSuccess(NewMsgListEntity response) {
                if (getView() != null) {
                    getView().getActListSuccess(response);
                    getView().dismissDialog();
                }
            }
        }).execute();

//        new ActivityProtocol(url, new NetWorkCallbackListener<ActivityEntity>() {
//            @Override
//            public void onFailed(String errorCode, String errorMsg) {
//                if (getView() != null) {
//                    getView().getMsgListFail(errorCode, errorMsg);
//                }
//
//            }
//
//            @Override
//            public void onSuccess(ActivityEntity activityEntity) {
//                if (getView() != null) {
//                    getView().getActListSuccess(activityEntity);
//                }
//
//            }
//        }).execute();
    }

}
