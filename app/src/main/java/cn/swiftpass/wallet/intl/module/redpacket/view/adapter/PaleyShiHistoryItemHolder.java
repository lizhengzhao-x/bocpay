package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.RedPacketBeanEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

public class PaleyShiHistoryItemHolder extends RecyclerView.ViewHolder {
    private final Activity activity;
    @BindView(R.id.tv_red_packet_history_title)
    TextView tvRedPacketHistoryTitle;
    @BindView(R.id.tv_red_packet_history_money)
    TextView tvRedPacketHistoryMoney;
    @BindView(R.id.tv_red_packet_history_content)
    TextView tvRedPacketHistoryContent;
    @BindView(R.id.tv_red_packet_history_time)
    TextView tvRedPacketHistoryTime;

    public PaleyShiHistoryItemHolder(Activity activity, View titleHolder) {
        super(titleHolder);
        ButterKnife.bind(this, itemView);
        this.activity = activity;
    }

    public void setData(RedPacketBeanEntity redPacketBean) {
        if (redPacketBean != null) {
            tvRedPacketHistoryTitle.setText(redPacketBean.getName());
            String price = AndroidUtils.formatPriceWithPoint(Double.parseDouble(redPacketBean.getSendAmt()), true);
            tvRedPacketHistoryMoney.setText(activity.getString(R.string.RP2101_10_9) + " " + redPacketBean.getCurrency() + " " + price);
            tvRedPacketHistoryContent.setText(redPacketBean.getBlessingWords());
            if (TextUtils.isEmpty(redPacketBean.getBlessingWords())) {
                tvRedPacketHistoryContent.setVisibility(View.GONE);
            } else {
                tvRedPacketHistoryContent.setVisibility(View.VISIBLE);
            }

            tvRedPacketHistoryTime.setText(redPacketBean.getTime());

        }

    }
}
