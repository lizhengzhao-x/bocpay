package cn.swiftpass.wallet.intl.module.callapp;

import static cn.swiftpass.wallet.intl.entity.ExternalSysParameterEntity.QRCODETYPEUPI;
import static cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem.EVENT_APP_CALLAPP_BIND_CARD;
import static cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem.EVENT_APP_CALLAPP_SUCCESS;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.app.constants.SmartAccountConst;
import cn.swiftpass.wallet.intl.base.BaseAbstractActivity;
import cn.swiftpass.wallet.intl.dialog.OrderDetailGradeDialogFragment;
import cn.swiftpass.wallet.intl.dialog.PaymentListDialogFragment;
import cn.swiftpass.wallet.intl.entity.ActionTrxGpInfoEntity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.ExternalSysParameterEntity;
import cn.swiftpass.wallet.intl.entity.MainScanOrderRequestEntity;
import cn.swiftpass.wallet.intl.entity.PaymentEnquiryResult;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.module.login.LoginActivity;
import cn.swiftpass.wallet.intl.module.login.SelRegisterTypeActivity;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.transfer.view.TransferConfirmMoneyActivity;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

/**
 * app call app PFS
 * 应该要分两种长场景 1.intent调起来，bocpay通过setResult返回参数 这种方式未登录下不能finish AppCallAppPaymentActivity
 * 2.applink吊起来，想要返回商户app,需要调用商户的appLink
 * 1）未登录态 SplashActivity ---->PreLoginActivity---->Login---->MainHomeActivity(not need autologin )----> AppCallAppPaymentActivity
 * 2) 登录态  SplashActivity ---->MainHomeActivity(not need autologin )----> AppCallAppPaymentActivity
 * SplashActivity ---->MainHomeActivity( need autologin app进程杀死)----> AppCallAppPaymentActivity
 * SplashActivity ---->MainHomeActivity---->绑定PA/SA----> AppCallAppPaymentActivity
 */
public class AppCallAppPaymentActivity extends BaseAbstractActivity<AppCallAppPaymentContract.Presenter> implements AppCallAppPaymentContract.View {
    private static final String TAG = "AppCallAppPaymentActivity";
    public static final String PAYMENT_STATUS = "PAYMENT_STATUS";
    public static final String PAYMENT_STATUS_OK = "PAYMENT_STATUS_OK";
    public static final int REQUEST_CALLAPP_PAYMENT = 199;
    private static final String SUCCESS_CODE = "0";

    private ActionTrxGpInfoEntity mActionTrxGpInfoEntity;


    /**
     * 支付里欸包
     */
    private PaymentListDialogFragment mPaymentListDialog;


    private ActionTrxGpInfoEntity mActionUpiInfo;


    private String mCurrentAppLink;

    /**
     * 默认的cardId 当前支付的cardId
     */
    private String originalCardId;


    public static void startAppCallApp(Activity mActivity, String link, String merchantLink, String errorLink, boolean isH5Payment) {
        if (mActivity == null || TextUtils.isEmpty(link)) {
            LogUtils.e(TAG, "ERROR: link id null ............");
            return;
        }
        TempSaveHelper.setAppCallAppConfig(merchantLink, link, errorLink, isH5Payment);
        HashMap<String, Object> mHashMaps = new HashMap<>();
        mHashMaps.put(Constants.DETAIL_URL, link);
        ActivitySkipUtil.startAnotherActivity(mActivity, AppCallAppPaymentActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
    }

    /**
     * 收银台
     */
    private OrderDetailGradeDialogFragment mOrderDetailDialogFragment;

    protected void init() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            LogUtils.e(TAG, "ERROR getExtras NULL 1");
            finish();
            return;
        } else {
            EventBus.getDefault().register(this);
            String fpsUrlInfo = getIntent().getExtras().getString("url", null);
            mCurrentAppLink = getIntent().getExtras().getString(Constants.DETAIL_URL, null);

            AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
            if (appCallAppEvent == null) {
                appCallAppEvent = new AppCallAppEvent();
                UserLoginEventManager.getInstance().addUserLoginEvent(appCallAppEvent);
            }
            //添加appcallapp的事件
            appCallAppEvent.updateEventParams(AppCallAppEvent.APP_LINK_URL, fpsUrlInfo);
            if (!CacheManagerInstance.getInstance().isLogin()) {
                HashMap<String, Object> mHashMaps = new HashMap<>();
                mHashMaps.put(Constants.ISAPPCALLAPP, true);
                ActivitySkipUtil.startAnotherActivity(this, LoginActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                return;
            }

            if (!TextUtils.isEmpty(fpsUrlInfo) || AppCallAppUtils.isFromFpsUrl(mCurrentAppLink)) {
                //FPS支付要求参数
                mPresenter.verifyOCSPUrl(fpsUrlInfo);
            } else if (!TextUtils.isEmpty(mCurrentAppLink)) {
                //APPCALLAPP
                mPresenter.parserQrcode(mCurrentAppLink);
            } else {
                LogUtils.e(TAG, "ERROR getExtras NULL 2");
                finish();
                return;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //半透明样式
        this.getWindow().getDecorView().setBackgroundColor(Color.TRANSPARENT);
        init();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_test_appcallapp;
    }

    @Override
    protected AppCallAppPaymentContract.Presenter createPresenter() {
        return new AppCallPaymentPresenter();
    }

    @Override
    public void onBackPressed() {

    }

    /**
     * 1.绑卡成功
     * 2.用户未登录态到登录态
     * 3.
     *
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        mPresenter.parserQrcode(mCurrentAppLink);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CALLAPP_PAYMENT) {
            exitAppCallApp(resultCode);
        }
    }

    @Override
    public void getActionTrxGpInfoError(String errorCode, String errorMsg) {
        mActionTrxGpInfoEntity.setCardId(originalCardId);
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void getActionTrxGpInfoSuccess(ActionTrxGpInfoEntity parserQrcodeEntity) {
        showCheckStand(parserQrcodeEntity);
    }

    @Override
    public void getPaymentTypeListSuccess(ArrayList<BankCardEntity> bankCards) {
        showPaymentListDialog(bankCards);
    }

    @Override
    public void getPaymentTypeListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }

    @Override
    public void getPayMentResultSuccess(PaymentEnquiryResult response) {
        GradeReduceSuccessActivity.startActivityFromAppCallApp(getActivity(), response);
        finish();
    }

    /**
     * 订单结果查询失败，需要返回给商户link 为 networkErrorLink
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getPayMentResultError(String errorCode, String errorMsg) {
        showErrorMsgDialogWithButtomMsg(this, errorMsg, R.string.ACA2101_0_4, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this, TempSaveHelper.getNetworkErrorLink(), TempSaveHelper.isIsH5Pay());
                finish();
            }
        });
    }

    /**
     * fps解析二维码失败
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void parseTransferCodeError(String errorCode, String errorMsg) {
        if (TextUtils.equals(errorCode, SmartAccountConst.SMART_ACCOUNT_EXIST_NOT)) {
            AndroidUtils.showBindSmartAccount(this, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.ACA2101_0_4), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            verifyPwdToUpdateAccount();
                        }
                    }, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            AppCallAppUtils.goToMerchantAppForFps(AppCallAppPaymentActivity.this, false);
                            finish();
                        }
                    }
            );
        } else {
            showErrorMsgDialog(this, errorMsg, R.string.ACA2101_0_4, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    AppCallAppUtils.goToMerchantAppForFps(AppCallAppPaymentActivity.this, false);
                    finish();
                }
            });
        }
    }

    private void verifyPwdToUpdateAccount() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), false, new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                HashMap<String, Object> maps = new HashMap<>();
                maps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, maps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(AppCallAppPaymentActivity.this, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {
                AppCallAppUtils.exitMerchantApp(AppCallAppPaymentActivity.this,false,false);
            }
        });
    }


    @Override
    public void parseTransferCodeSuccess(TransferPreCheckEntity response) {
        TransferConfirmMoneyActivity.startActivityForResult(getActivity(), response);
    }

    /**
     * 下单接口直接报错，点击返回 应该返回的link为  networkErrorLink
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getMainScanOrderError(String errorCode, String errorMsg) {
        showErrorMsgDialogWithButtomMsg(this, errorMsg, R.string.ACA2101_0_4, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this, TempSaveHelper.getNetworkErrorLink(), TempSaveHelper.isIsH5Pay());
                finish();
            }
        });
    }


    /**
     * 业务逻辑异常，server会重新返回需要更新的link
     *
     * @param errorCode
     * @param errorMsg
     */
    @Override
    public void getMainScanOrderParamsError(String errorCode, String errorMsg) {
        showErrorMsgDialogWithButtomMsg(this, errorMsg, R.string.ACA2101_0_4, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this, TempSaveHelper.getMerchantLink(), TempSaveHelper.isIsH5Pay());
                finish();
            }
        });
    }

    @Override
    public void parserQrcodeError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg, R.string.ACA2101_0_4, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this, TempSaveHelper.getNetworkErrorLink(), TempSaveHelper.isIsH5Pay());
                finish();
            }
        });
    }

    /**
     * 二维码解析成功 分为 FPS 银联
     *
     * @param qrcodeEntity
     */
    @Override
    public void parserQrcodeSuccess(ExternalSysParameterEntity qrcodeEntity) {
        if (TextUtils.equals(qrcodeEntity.getResultCode(), SUCCESS_CODE)) {
            //解码成功 暂时屏蔽转账这种方式
//            if (TextUtils.equals(qrcodeEntity.getCodeType(), QRCODETYPEFPS)) {
//                if (qrcodeEntity.getFpsData() != null) {
//                    TransferPreCheckEntity transferPreCheckEntity = qrcodeEntity.getFpsData().parserData();
//                    TransferConfirmMoneyActivity.startActivityForResult(AppCallAppPaymentActivity.this, transferPreCheckEntity);
//                }
//            } else
            if (TextUtils.equals(qrcodeEntity.getCodeType(), QRCODETYPEUPI)) {
                if (qrcodeEntity.getUpiData() != null) {
                    mActionUpiInfo = qrcodeEntity.getUpiData();
                    showCheckStand(mActionUpiInfo);
                }
            }
        } else {
            showErrorMsgDialog(this, qrcodeEntity.getMessage(), R.string.ACA2101_0_4, new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this, qrcodeEntity.getAppLink(), TempSaveHelper.isIsH5Pay());
                    finish();
                }
            });
        }

 /*       else if (TextUtils.equals(qrcodeEntity.getResultCode(), SMART_ACCOUNT_EXIST_NOT)) {
            //如果是转账二维码 看是不是智能账户
            AndroidUtils.showBindSmartAccountAndCancelDialog(AppCallAppPaymentActivity.this, getString(R.string.SMB1_1_11), getString(R.string.SMB1_1_12), getString(R.string.SMB1_1_15), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    HashMap<String, Object> mHashMaps = new HashMap<>();
                    mHashMaps.put(Constants.CURRENT_PAGE_FLOW, Constants.PAGE_FLOW_BIND_CARD);
                    mHashMaps.put(Constants.ISAPPCALLAPP, true);
                    ActivitySkipUtil.startAnotherActivity(getActivity(), SelRegisterTypeActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                }
            });
        } else {
            //如果出现订单已经支付 弹框 点击确认退出
            AndroidUtils.showTipDialog(this, null, qrcodeEntity.getMessage(), getString(R.string.ACA2101_0_4), getString(R.string.ACA2101_0_2), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //返回商户
                    finish();
                    AppCallAppUtils.goToMerchantApp(AppCallAppPaymentActivity.this);
                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    TempSaveHelper.clearLink();
                }
            });

        }*/
    }

    /**
     * fps流程ocsp异常
     *
     * @param errorCode
     * @param errorMsg
     * @param callbackUrl
     */
    @Override
    public void verifyOCSPUrlError(String errorCode, String errorMsg, String callbackUrl) {
        showErrorMsgDialog(this, getString(R.string.string_network_available), R.string.ACA2101_0_4, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                AppCallAppUtils.goToMerchantAppForFps(AppCallAppPaymentActivity.this, false);
                finish();
            }
        });
    }


    /**
     * 吊起收银台 后期要全部换成这种方式
     */
    private void showCheckStand(final ActionTrxGpInfoEntity actionTrxGpInfoEntity) {
        this.mActionTrxGpInfoEntity = actionTrxGpInfoEntity;
        //成功之后 收银台当前选择的cardId要赋值
        originalCardId = actionTrxGpInfoEntity.getCardId();
        if (mOrderDetailDialogFragment == null) {
            mOrderDetailDialogFragment = new OrderDetailGradeDialogFragment();
            OrderDetailGradeDialogFragment.OnDialogClickListener onDialogClickListener = new OrderDetailGradeDialogFragment.OnDialogClickListener() {
                @Override
                public void onOkListener(boolean isPoint) {
                    initPwdDialog(isPoint);
                }

                @Override
                public void onSelCardListener() {
                    mPresenter.getPaymentTypeList();
                }

                @Override
                public void onDisMissListener() {
                    mOrderDetailDialogFragment = null;
                    AppCallAppUtils.exitMerchantApp(AppCallAppPaymentActivity.this, false, true);
                }
            };
            mOrderDetailDialogFragment.initParams(actionTrxGpInfoEntity, onDialogClickListener, false);
            mOrderDetailDialogFragment.show(getSupportFragmentManager(), "OrderDetailGradeDialogFragment");
        } else {
            mOrderDetailDialogFragment.updateParams(actionTrxGpInfoEntity);
        }
    }


    /**
     * 验密逻辑
     */
    public void initPwdDialog(boolean isPoint) {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                submitOrderInfoRequest(isPoint);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }

    /**
     * 提交订单信息
     */
    private void submitOrderInfoRequest(boolean isPoint) {
        MainScanOrderRequestEntity orderRequest = new MainScanOrderRequestEntity();
        orderRequest.setTrxAmt(mActionTrxGpInfoEntity.getTranAmt());
        orderRequest.setTxnCurr(mActionTrxGpInfoEntity.getTranCur());
        orderRequest.setTxnId(mActionTrxGpInfoEntity.getTranID());
        orderRequest.setCardId(mActionTrxGpInfoEntity.getCardId());
        orderRequest.setMpQrCode(mActionTrxGpInfoEntity.getQrCode());
        orderRequest.setTrxFeeAmt("0");
        orderRequest.setQrcType(mActionTrxGpInfoEntity.getQrTp());
        orderRequest.setMerchantName(mActionTrxGpInfoEntity.getMerchantName());
        if (mActionTrxGpInfoEntity.getRedeemFlag().equals("1") && isPoint) {
            orderRequest.setRedeemGpAmt(mActionTrxGpInfoEntity.getRedeemGpTnxAmt());
            orderRequest.setMerchantName(mActionTrxGpInfoEntity.getName());
            orderRequest.setGpRequired(mActionTrxGpInfoEntity.getGpRequired());
            orderRequest.setRedeemFlag(mActionTrxGpInfoEntity.getRedeemFlag());
            orderRequest.setGpCode(mActionTrxGpInfoEntity.getGpCode());
            orderRequest.setYearHolding(mActionTrxGpInfoEntity.getYearHolding());
            orderRequest.setPayAmt(mActionTrxGpInfoEntity.getPayAmt());
            orderRequest.setRedeemGpCount(mActionTrxGpInfoEntity.getRedeemGpCount());
            orderRequest.setAvaGpCount(mActionTrxGpInfoEntity.getAvaGpCount());
            mPresenter.actionMpqrPaymentWithPoint(orderRequest);
        } else {
            mPresenter.getMainScanOrder(orderRequest);
        }

    }


    /**
     * 支付列表
     *
     * @param bankCardEntities
     */
    private void showPaymentListDialog(final ArrayList<BankCardEntity> bankCardEntities) {
        mPaymentListDialog = new PaymentListDialogFragment();
        PaymentListDialogFragment.OnPaymentListClickListener onPaymentListClickListener = new PaymentListDialogFragment.OnPaymentListClickListener() {
            @Override
            public void onBackBtnClickListener() {

            }

            @Override
            public void onMoreBtnClickListener() {
            }

            @Override
            public void onCardSelListener(int positon) {
                //切换卡号
                mActionUpiInfo.setCardId(bankCardEntities.get(positon).getCardId());
                mPresenter.getActionTrxGpInfo(mActionUpiInfo);
            }
        };
        mPaymentListDialog.initParams(onPaymentListClickListener, bankCardEntities, mOrderDetailDialogFragment.getCurrentSelCardId(), mOrderDetailDialogFragment.isFpsPaymentType());
        mPaymentListDialog.show(getSupportFragmentManager(), "paymentListDialogFragment");
    }


    /**
     * 退出App流程调用
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(AppCallAppEventItem event) {
        if (event.getEventType() == EVENT_APP_CALLAPP_BIND_CARD) {
            //绑卡成功之后需要重新发起流程
            mPresenter.parserQrcode(mCurrentAppLink);
        } else {
            exitAppCallApp(event.getEventType());
        }
    }

    @Override
    protected boolean useScreenOrientation() {
        return true;
    }

    /**
     * 退出整个app call app流程
     */
    private void exitAppCallApp(int resultCode) {
        MyActivityManager.removeAllTaskExcludeAppCallAppStack();
        setResult(resultCode == EVENT_APP_CALLAPP_SUCCESS ? RESULT_OK : RESULT_CANCELED);
        finish();
//        AppCallAppUtils.exitMerchantApp(this, true, true);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();


//        if (mOrderDetailDialogFragment != null) {
//            mOrderDetailDialogFragment.dismissAllowingStateLoss();
//        }
//
//        if (mPaymentListDialog != null) {
//            mPaymentListDialog.dismissAllowingStateLoss();
//        }
        EventBus.getDefault().unregister(this);
        if (mPaymentListDialog != null) {
            mPaymentListDialog = null;
        }

        if (mOrderDetailDialogFragment != null) {
            mOrderDetailDialogFragment = null;
        }
    }
}
