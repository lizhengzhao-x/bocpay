package cn.swiftpass.wallet.intl.module.register.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;

public class TaxCountryHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tv_country_name)
    TextView tvCountryName;
    @BindView(R.id.iv_select_country)
    ImageView ivSelectCountry;

    private final Context context;

    public TaxCountryHolder(Context context, View view) {
        super(view);
        ButterKnife.bind(this, view);
        this.context = context;
    }
}
