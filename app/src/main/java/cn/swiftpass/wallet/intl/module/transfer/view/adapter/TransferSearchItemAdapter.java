package cn.swiftpass.wallet.intl.module.transfer.view.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

/**
 * Created by ZhangXinchao on 2019/10/31.
 */
public class TransferSearchItemAdapter implements AdapterItem<ContractListEntity.RecentlyBean> {


    private ImageView mId_contact_head_img;
    private TextView mId_contact_user_name;
    private TextView mId_contact_user_phone;
    private View mId_contact_parent_view;

    private View mContact_collect_imag_view;
    private ImageView mContact_collect_Image;

    private int position;
    private OnItemContactClickCallback onItemContactClickCallback;

    public TransferSearchItemAdapter(Context context, OnItemContactClickCallback onItemContactClickCallbackIn) {
        this.onItemContactClickCallback = onItemContactClickCallbackIn;
    }


    @Override
    public int getLayoutResId() {
        return R.layout.transfer_search_item_contact_view;
    }

    @Override
    public void bindViews(View root) {
        mId_contact_head_img = (ImageView) root.findViewById(R.id.id_contact_head_img);
        mId_contact_user_name = (TextView) root.findViewById(R.id.id_contact_user_name);
        mId_contact_user_phone = root.findViewById(R.id.id_contact_user_phone);
        mId_contact_parent_view = root.findViewById(R.id.id_contact_parent_view);
        mContact_collect_imag_view = root.findViewById(R.id.ll_contact_collect_imag_view);
        mContact_collect_Image = (ImageView) root.findViewById(R.id.id_contact_collect_imag);
    }

    @Override
    public void setViews() {
        mId_contact_parent_view.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (onItemContactClickCallback != null) {
                    onItemContactClickCallback.onItemClick(position);
                }
            }
        });

        mContact_collect_imag_view.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                if (onItemContactClickCallback != null) {
                    onItemContactClickCallback.onItemCollectClick(position);
                }
            }
        });

    }

    @Override
    public void handleData(ContractListEntity.RecentlyBean addressBookItemEntity, int positionIn) {
        position = positionIn;
        if (!TextUtils.isEmpty(addressBookItemEntity.getDisCustName())) {
            mId_contact_user_name.setText(addressBookItemEntity.getDisCustName());
        } else {
            mId_contact_user_name.setVisibility(View.GONE);
        }

        if (addressBookItemEntity.isEmail()) {
            mId_contact_user_phone.setText(addressBookItemEntity.getDisEmail());
        } else {
            mId_contact_user_phone.setText(addressBookItemEntity.getDisPhoneNo());
        }
        mId_contact_head_img.setImageResource(addressBookItemEntity.isEmail() ? R.mipmap.icon_transfer_profile_email : R.mipmap.icon_transfer_profile_mobile);
        if (TextUtils.equals(addressBookItemEntity.getUiShowType(), ContractListEntity.RecentlyBean.UI_FREQUENT)) {
            mContact_collect_imag_view.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals(addressBookItemEntity.getUiShowType(), ContractListEntity.RecentlyBean.UI_ADDRESSBOOK)) {
            mContact_collect_imag_view.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals(addressBookItemEntity.getUiShowType(), ContractListEntity.RecentlyBean.UI_ADDRESS_FREQUENT)) {
            mContact_collect_imag_view.setVisibility(View.VISIBLE);
        } else {
            mContact_collect_imag_view.setVisibility(View.GONE);
        }

        mContact_collect_Image.setImageResource(addressBookItemEntity.isCollect() ? R.mipmap.icon_contactlist_favorite : R.mipmap.icon_contactlist_favorite_empty);

    }

    public static class OnItemContactClickCallback {


        public void onItemClick(int position) {
            // do nothing
        }

        public void onItemCollectClick(int position) {
            // do nothing
        }

    }
}
