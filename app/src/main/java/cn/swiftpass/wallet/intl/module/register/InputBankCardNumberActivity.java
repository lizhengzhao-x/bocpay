package cn.swiftpass.wallet.intl.module.register;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.event.CardEventEntity;
import cn.swiftpass.wallet.intl.entity.event.ForgetPwdEventEntity;
import cn.swiftpass.wallet.intl.entity.event.PasswordEventEntity;
import cn.swiftpass.wallet.intl.entity.event.RegisterEventEntity;


public class InputBankCardNumberActivity extends BaseCompatActivity {
    @BindView(R.id.fl_transaction)
    FrameLayout flTransaction;
    private FragmentManager mFragmentManager;
    private InputBankCardNumberFragment mInputBankCardNumberFragment;
    private int mPageFlow;


    @Override
    public void init() {

        mPageFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
            //注册流程
            setToolBarTitle(R.string.IDV_2_1);
        } else if (mPageFlow == Constants.PAGE_FLOW_BIND_CARD) {
            //绑卡流程
            setToolBarTitle(R.string.bind_title);
        } else if (mPageFlow == Constants.PAGE_FLOW_FORGETPASSWORD) {
            //忘记支付密码
            setToolBarTitle(R.string.setting_payment_short);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            //虚拟卡注册
            setToolBarTitle(R.string.VC02_01a_1);
        } else if (mPageFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //虚拟卡忘记密码
            setToolBarTitle(R.string.setting_payment_short);
        }
        TempSaveHelper.setBankCardNumberType(mPageFlow);

        setBackBtnListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                    showConfirmExitDialog();
                } else {
                    finish();
                }
            }
        });

        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        mInputBankCardNumberFragment = new InputBankCardNumberFragment();
        mInputBankCardNumberFragment.setPageFlow(mPageFlow);
        fragmentTrasaction.add(R.id.fl_transaction, mInputBankCardNumberFragment);
        fragmentTrasaction.commitAllowingStateLoss();

    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(CardEventEntity event) {
        if (event.getEventType() == CardEventEntity.EVENT_BIND_CARD
        ) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(PasswordEventEntity event) {
        if (event.getEventType() == PasswordEventEntity.EVENT_FORGOT_PASSWORD) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ForgetPwdEventEntity event) {
        if (event.getEventType() == ForgetPwdEventEntity.EVENT_FORGET_PWD || event.getEventType() == ForgetPwdEventEntity.EVENT_FORGET_PWD_LOGIN
        ) {
            finish();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(RegisterEventEntity event) {
        if (event.getEventType() == RegisterEventEntity.EVENT_REGISTER) {
            finish();
        }
    }



    @Override
    protected int getLayoutId() {
        return R.layout.act_register_input_bankcard_number;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (mPageFlow == Constants.PAGE_FLOW_REGISTER) {
                showConfirmExitDialog();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showConfirmExitDialog() {
        CustomDialog.Builder builder = new CustomDialog.Builder(InputBankCardNumberActivity.this);
        builder.setTitle(getString(R.string.register_otp_title));
        builder.setMessage(getString(R.string.string_cancel_register_app));
        builder.setPositiveButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.string_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (mInputBankCardNumberFragment != null) {
                    mInputBankCardNumberFragment.showKeyBoard();
                }
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        Activity mActivity = (Activity) mContext;
        if (mActivity != null && !mActivity.isFinishing()) {
            CustomDialog mDealDialog = builder.create();
            mDealDialog.setCancelable(false);
            mDealDialog.show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
