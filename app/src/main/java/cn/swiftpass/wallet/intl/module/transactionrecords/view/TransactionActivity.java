package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;

/**
 * @author 2018.8.29
 * 账单
 */

public class TransactionActivity extends BaseCompatActivity {
    @BindView(R.id.id_boc_pay)
    TextView idBocPay;
    @BindView(R.id.id_smart_account)
    TextView idSmartAccount;
    @BindView(R.id.id_credit_card)
    TextView idCreditCard;
    private FragmentManager mFragmentManager;
    private Fragment mCurrentFragment;
    private TransacSmartAccountFragment transacSmartAccountFragment;
    private TransacCreditCardFragment transacCreditCardFragment;
    private BocPayTransacFragment bocPayTransacFragment;
    private int lastSelPosition;

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.home_transactions);
        bocPayTransacFragment = new BocPayTransacFragment();
        transacSmartAccountFragment = new TransacSmartAccountFragment();
        transacCreditCardFragment = new TransacCreditCardFragment();
        mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        mCurrentFragment = bocPayTransacFragment;
        fragmentTrasaction.add(R.id.fl_transaction, mCurrentFragment);
        fragmentTrasaction.commitAllowingStateLoss();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transaction;
    }


    @OnClick({R.id.id_boc_pay, R.id.id_smart_account, R.id.id_credit_card})
    public void onViewClicked(View view) {
        setDefaultSelect();
        switch (view.getId()) {
            case R.id.id_boc_pay:
                idBocPay.setBackgroundResource(R.drawable.left_gloable_sel_btn_bac);
                idBocPay.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(bocPayTransacFragment, R.id.fl_transaction, 0);
                break;
            case R.id.id_smart_account:
                idSmartAccount.setBackgroundResource(R.drawable.center_smart_btn_bac);
                idSmartAccount.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(transacSmartAccountFragment, R.id.fl_transaction, 1);
                break;
            case R.id.id_credit_card:
                idCreditCard.setBackgroundResource(R.drawable.right_china_btn_bac);
                idCreditCard.setTextColor(getResources().getColor(R.color.app_white));
                switchDiffFragmentContent(transacCreditCardFragment, R.id.fl_transaction, 2);
                break;
        }
    }

    private void setDefaultSelect() {
        idSmartAccount.setBackgroundResource(R.drawable.center_smart_btn_nosel_bac);
        idSmartAccount.setTextColor(getResources().getColor(R.color.font_gray_three));
        idBocPay.setBackgroundResource(R.drawable.left_gloable_btn_bac);
        idBocPay.setTextColor(getResources().getColor(R.color.font_gray_three));
        idCreditCard.setBackgroundResource(R.drawable.right_china_btn_nosel_bac);
        idCreditCard.setTextColor(getResources().getColor(R.color.font_gray_three));
    }

    protected void switchDiffFragmentContent(Fragment toFragment, int resId, int index) {
        if (null == mCurrentFragment || null == toFragment) {
            return;
        }
        FragmentTransaction fragmentTrasaction = mFragmentManager.beginTransaction();
        if (!toFragment.isAdded()) {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.add(resId, toFragment, String.valueOf(index));
            fragmentTrasaction.commitAllowingStateLoss();
        } else {
            fragmentTrasaction.hide(mCurrentFragment);
            fragmentTrasaction.show(toFragment);
            fragmentTrasaction.commitAllowingStateLoss();
        }
        mCurrentFragment = toFragment;
        lastSelPosition = index;
    }


}
