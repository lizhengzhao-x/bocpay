package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 20:20
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderUsedEntity extends BaseEntity {
    /**
     * value : 还款
     * code : 7
     */

    private String value;
    private String code;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
