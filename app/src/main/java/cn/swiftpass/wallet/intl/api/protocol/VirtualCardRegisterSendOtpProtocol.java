package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualCardListEntity;

/**
 * 虚拟卡注册发送otp
 */
public class VirtualCardRegisterSendOtpProtocol extends BaseProtocol {
    VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiy;

    public VirtualCardRegisterSendOtpProtocol(VirtualCardListEntity.VirtualCardListBean vitualCardSendOtpEntiyIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        vitualCardSendOtpEntiy = vitualCardSendOtpEntiyIn;
        mUrl = "api/virtual/reg/sendOtp";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.ACTION, "VIRREG");
        mBodyParams.put(RequestParams.VIRTUALCARDIND, "Y");
        mBodyParams.put(RequestParams.PAN_ID, vitualCardSendOtpEntiy.getPan());
        mBodyParams.put(RequestParams.EXPDATE, vitualCardSendOtpEntiy.getExpDate());
        mBodyParams.put(RequestParams.FACEID, vitualCardSendOtpEntiy.getCrdArtId());
        mBodyParams.put(RequestParams.EAIACTION, "B");
    }

}
