package cn.swiftpass.wallet.intl.test;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.ProjectApp;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.RedPackageEntity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.module.MainHomeActivity;
import cn.swiftpass.wallet.intl.module.guide.SplashActivity;

import static cn.swiftpass.wallet.intl.event.PushNotificationEvent.RED_PACKET_RED_PACKET_PARAMS;


public class TestNotifacation {
    private static int NOTIFICATION_ID = 100;
    private final int MSG_VIRTUALCARD_BIND = -1;

    public void testRed(Context mContext) {


        //用BoC Pay買滿$30即減$10，真係好抵！仲等？""快啲嗌野食啦！優惠期至5月31日。受條款及細則約束^[McDonald's 優惠]  。
        RedPackageEntity redPackageEntity = new RedPackageEntity();
        String messageContent = "用BoC Pay買滿$30即減$10，真係好抵！仲等？\"\"快啲嗌野食啦！優惠期至5月31日。受條款及細則約束^[McDonald's 優惠]  。";
        String items[] = messageContent.split("\\^");
        redPackageEntity.setTitle(items[1]);
        redPackageEntity.setSubTitle(items[0]);
        sendNotification(redPackageEntity, mContext.getApplicationContext(), mContext.getPackageName(), mContext.getPackageName() + "_Notification", 100);

        //点击广播监听
        //Intent intentClick = new Intent(mContext, NotificationBroadcastReceiver.class);
        //intentClick.setAction("notification_clicked");
        //intentClick.putExtra("MESSAGE","消息");
        //intentClick.setClass(mContext,NotificationBroadcastReceiver.class);
        //Intent intent = new Intent(mContext, MainHomeActivity.class);
//        Intent intent = new Intent(mContext, RedPacketPushReceiverActivity.class);
//        sendNotifacation(mContext, intent, redPackageEntity, mContext.getPackageName(), mContext.getPackageName() + "_Notification");
    }


    private void test(Context mContext) {
        //用BoC Pay買滿$30即減$10，真係好抵！仲等？""快啲嗌野食啦！優惠期至5月31日。受條款及細則約束^[McDonald's 優惠]  。
        RedPackageEntity redPackageEntity = new RedPackageEntity();
        String messageContent = "用BoC Pay買滿$30即減$10，真係好抵！仲等？\"\"快啲嗌野食啦！優惠期至5月31日。受條款及細則約束^[McDonald's 優惠]  。";
        String items[] = messageContent.split("\\^");
        redPackageEntity.setTitle(items[1]);
        redPackageEntity.setSubTitle(items[0]);
        sendNotification(redPackageEntity, mContext.getApplicationContext(), mContext.getPackageName(), mContext.getPackageName() + "_Notification", 100);
    }

    private void sendNotification(RedPackageEntity packageEntity, Context mContext, String id, String name, int msgType) {

        Intent intent = new Intent(mContext, MainHomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        if (msgType == MSG_VIRTUALCARD_BIND) {
            //虚拟卡推送类型
            intent.putExtra(Constants.MSGTYPE_VCNTF, true);
            //要判断app此时是否在前台 如果在前台需要发通知 弹框提示
            if (ProjectApp.getFrontActivityNumber() != 0) {
                //app在前台
                EventBus.getDefault().post(new MainEventEntity(MainEventEntity.EVENT_KEY_NOTIFIACTION_VIRTUALCARD_BIND, ""));
            } else {
                sendNotifacation(mContext, intent, packageEntity, id, name);
            }
        } else {

            //中转页面监听点击事件
            Intent intentClick = new Intent(mContext, SplashActivity.class);
            intentClick.putExtra(RED_PACKET_RED_PACKET_PARAMS, packageEntity);
            intentClick.putExtra("MESSAGE_TYPE",Constants.REDMG_STAFF_PARAM);
            //其他类型
            //intent.putExtra(Constants.AUTO_SHOW_PACKAGE, true);
            sendNotifacation(mContext, intentClick, packageEntity, id, name);
        }

    }


    private void sendNotifacation(Context mContext, Intent intent, RedPackageEntity packageEntity, String id, String name) {
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Bitmap iconBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);

        mBuilder.setSmallIcon(R.mipmap.ic_launcher).
                setContentText(packageEntity.getSubTitle()).
                setAutoCancel(true).
                setLargeIcon(iconBitmap).
                setSound(defaultSoundUri).
                setContentIntent(pendingIntent);
        //创建大文本样式
        NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
        bigTextStyle.setBigContentTitle(packageEntity.getTitle()).setSummaryText("").bigText(packageEntity.getSubTitle());
        mBuilder.setStyle(bigTextStyle);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            mBuilder.setChannelId(id);
            notificationManager.createNotificationChannel(channel);
        }
        notificationManager.notify(NOTIFICATION_ID++, mBuilder.build());
    }
}
