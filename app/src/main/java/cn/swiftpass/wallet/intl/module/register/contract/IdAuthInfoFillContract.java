package cn.swiftpass.wallet.intl.module.register.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.wallet.intl.base.fpr.FPRUsedCountPresenter;
import cn.swiftpass.wallet.intl.base.fpr.FPRUsedCountView;
import cn.swiftpass.wallet.intl.base.ocr.OCRPresenter;
import cn.swiftpass.wallet.intl.base.ocr.OCRView;
import cn.swiftpass.wallet.intl.entity.CheckHKCustEntity;
import cn.swiftpass.wallet.intl.entity.IdAuthIinfoEntity;
import cn.swiftpass.wallet.intl.entity.IdvImageEntity;
import cn.swiftpass.wallet.intl.entity.S2RegisterInfoEntity;

public class IdAuthInfoFillContract {
    public interface View extends IView, OCRView<Presenter>, FPRUsedCountView<Presenter> {


        void submitDataSuccess(ContentEntity response, String action);

        void submitDataFail(String action, String errorCode, String errorMsg);

        void forgetPwdCustomerInfoSuccess(IdAuthIinfoEntity response);

        void forgetPwdCustomerInfoFail(String errorCode, String errorMsg);

        void getCardImageSuccess(IdvImageEntity response);

        void getCardImageFail(String errorCode, String errorMsg);

        void checkHKCustSuccess(String action, CheckHKCustEntity entity);

        void checkHKCustFail(String action, String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<View>, OCRPresenter<View>, FPRUsedCountPresenter<View> {
        void submitRegisterData(String type, S2RegisterInfoEntity registerInfoEntity);

        void downloadCardImage(String mIdvSequnumber, String mIdvFid);

        void checkHKCust(String action);

        void forgetPwdCustomerInfo();
    }
}
