package cn.swiftpass.wallet.intl.module.cardmanagement.card;

import android.content.Context;
import android.util.AttributeSet;

import androidx.recyclerview.widget.LinearLayoutManager;

public class CardLinearLayoutManager extends LinearLayoutManager {

    private boolean isScrollEnabled=true;


    public CardLinearLayoutManager(Context context) {
        super(context);
    }

    public CardLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    public CardLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }



    public void setScrollEnabled(boolean flag) {
        this.isScrollEnabled = flag;
    }



    @Override
    public boolean canScrollVertically() {
        //Similarly you can customize "canScrollHorizontally()" for managing horizontal scroll
        return isScrollEnabled && super.canScrollVertically();
    }

    @Override
    public boolean canScrollHorizontally() {
        return isScrollEnabled &&super.canScrollHorizontally();
    }
}
