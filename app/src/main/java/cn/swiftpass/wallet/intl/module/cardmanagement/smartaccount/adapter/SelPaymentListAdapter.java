package cn.swiftpass.wallet.intl.module.cardmanagement.smartaccount.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;


public class SelPaymentListAdapter implements AdapterItem<BankCardEntity> {
    private Context context;
    private CardItemSelCallback cardItemSelCallback;

    public SelPaymentListAdapter(Context context, CardItemSelCallback cardItemSelCallback) {
        this.context = context;
        this.cardItemSelCallback = cardItemSelCallback;
    }

    private TextView mId_card_number;
    private TextView id_sub_title;
    private ImageView mId_card_sel;
    private ImageView id_image_view;
    private View id_parent_view;
    private int selPosition;

    @Override
    public int getLayoutResId() {
        return R.layout.item_sel_payment_item;
    }

    @Override
    public void bindViews(View root) {
        mId_card_number = root.findViewById(R.id.id_card_number);
        mId_card_sel = root.findViewById(R.id.id_card_sel);
        id_image_view = root.findViewById(R.id.id_image_view);
        id_sub_title = root.findViewById(R.id.id_sub_title);
        id_parent_view = root.findViewById(R.id.id_parent_view);
    }

    @Override
    public void setViews() {
        id_parent_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardItemSelCallback.onItemCardSelClick(selPosition);
            }
        });
    }

    @Override
    public void handleData(BankCardEntity cardEntity, int position) {
        selPosition = position;
        mId_card_number.setText(AndroidUtils.getSubMasCardNumberTitle(context, cardEntity.getPanShowNumber(), cardEntity.getCardType()));
        mId_card_sel.setVisibility(cardEntity.isSel() ? View.VISIBLE : View.INVISIBLE);
        id_image_view.setImageBitmap(null);
        if (cardEntity.getCardType().equals("1")) {
            //  //1:信用卡  2：智能账号
            GlideApp.with(context)
                    .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                    .into(id_image_view);
        } else {
            if (cardEntity.getIsFPS().equals("1")) {
                //fps类型
                id_sub_title.setVisibility(View.VISIBLE);
                id_image_view.setImageResource(R.mipmap.icon_button_fps_large);
            } else {
                GlideApp.with(context)
                        .load(AndroidUtils.getCardFaceUrl(cardEntity.getCardType()) + cardEntity.getCardFaceId() + ".png")
                        .into(id_image_view);
            }
        }
    }

    public static class CardItemSelCallback {
        public void onItemCardSelClick(int position) {
            // do nothing
        }
    }
}
