package cn.swiftpass.wallet.intl.manage;


import android.text.TextUtils;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.entity.InitBannerEntity;

/**
 * 所有发送otp 倒计时统计 首页进入 拉取配置参数成功保存到内存中
 */
public class OtpCountDownManager {

    private static final int DEFAULT_INTERVALTIME = 60;

    public InitBannerEntity getOtpCountDownInfo() {
        return mOtpCountDownInfo;
    }

    public Integer getOtpSendIntervalTime() {
        if (mOtpCountDownInfo == null) {
            return DEFAULT_INTERVALTIME;
        }
        if (TextUtils.isEmpty(mOtpCountDownInfo.getOtpSendIntervalTime())) {
            return DEFAULT_INTERVALTIME;
        }
        try {
            return Integer.valueOf(mOtpCountDownInfo.getOtpSendIntervalTime());
        } catch (NumberFormatException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
            return DEFAULT_INTERVALTIME;
        }
    }

    public void setmOtpCountDownInfo(InitBannerEntity mOtpCountDownInfo) {
        this.mOtpCountDownInfo = mOtpCountDownInfo;
    }

    private InitBannerEntity mOtpCountDownInfo;

    private static class ApiManagerHolder {
        private static OtpCountDownManager instance = new OtpCountDownManager();
    }

    public static OtpCountDownManager getInstance() {
        return OtpCountDownManager.ApiManagerHolder.instance;
    }
}
