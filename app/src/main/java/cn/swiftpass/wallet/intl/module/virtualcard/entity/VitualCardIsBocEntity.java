package cn.swiftpass.wallet.intl.module.virtualcard.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VitualCardIsBocEntity extends BaseEntity {


    /**
     * phoneNo : 86-17620398534
     * nid : C1743404
     * nidRgn : HK
     * boccus : 0
     * nidType : NM
     */

    private String phoneNo;
    private String nid;
    private String nidRgn;
    private String boccus;
    private String nidType;

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getNid() {
        return nid;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public String getNidRgn() {
        return nidRgn;
    }

    public void setNidRgn(String nidRgn) {
        this.nidRgn = nidRgn;
    }

    public String getBoccus() {
        return boccus;
    }

    public void setBoccus(String boccus) {
        this.boccus = boccus;
    }

    public String getNidType() {
        return nidType;
    }

    public void setNidType(String nidType) {
        this.nidType = nidType;
    }
}
