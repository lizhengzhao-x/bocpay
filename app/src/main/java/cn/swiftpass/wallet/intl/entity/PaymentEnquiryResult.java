/**
 * Copyright 2011 Bank of China Credit Card (International) Ltd. All Rights Reserved.
 * <p>
 * This software is the proprietary information of Bank of China Credit Card (International) Ltd.
 * Use is subject to license terms.
 **/
package cn.swiftpass.wallet.intl.entity;

import android.graphics.Color;
import android.text.TextUtils;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * Class Name: PaymentEnquiryResult<br/>
 * <p>
 * Class Description:
 *
 * @author 86755221
 * @version 1 Date: 2018-1-19
 */
public class PaymentEnquiryResult extends BaseEntity {
    /**
     *
     */
    public static final String PAYMENT_STATUS_PROCESS = "PROCESS";
    /**
     *
     */
    public static final String PAYMENT_STATUS_SUCCESS = "SUCCESS";
    public static final String PAYMENT_STATUS_NOTPAY = "NOTPAY";
    public static final String PAYMENT_STATUS_CLOSED = "CLOSED";
    public static final String PAYMENT_STATUS_REJECT = "REJECT";

    //    NOTPAY("1"),  待支付
//    SUCCESS("2"),  支付成功
//    CLOSED("3"),  关闭
//    REFUND("4"),  退款
//    CANCEL("5"), 撤销
//    REJECT("6")  拒绝
    public static final String PAYMENT_STATUS_SUCCESS_ = "2";
    /**
     *
     */
    public static final String PAYMENT_STATUS_FAIL = "FAIL";
    /**
     *
     */
    public static final String PAYMENT_STATUS_CANCEL = "CANCEL";
    /**
     *
     */
    public static final String PAYMENT_STATUS_REFUND = "REFUND";
    public static final String PAYMENT_STATUS_NONE = "PAYMENT_STATUS_NONE";
    /**
     * acquirerIIN :
     * forwardingIIN :
     * merchantName :
     * panFour : 0913
     * paymentStatus : PROCESS
     * qrcVoucherNumber :
     * rejectionReason :
     * retrievalRefNum :
     * sysTraceAuditNumber :
     * transDate :
     * trxAmt : null
     * trxCurrency :
     * txnId : A399900052018012220110900000609
     */

    private String acquirerIIN;
    private String forwardingIIN;
    //商户名称
    private String merchantName;
    //银行卡后四位
    private String panFour;
    //交易状态
    private String paymentStatus;
    private String qrcVoucherNumber;

    public void setColorSign(String colorSign) {
        this.colorSign = colorSign;
    }

    /**
     * 特殊颜色
     */
    private String colorSign;

    public String getAppLink() {
        return appLink;
    }

    public void setAppLink(String appLink) {
        this.appLink = appLink;
    }

    /**
     * app link 返回
     */
    private String appLink;
    //拒绝原因

    private String rejectionReason;

    public String getSmaType() {
        return smaType;
    }

    public void setSmaType(String smaType) {
        this.smaType = smaType;
    }

    //2：支付账户 3：智能账户
    private String smaType;
    //证件名称
    private String retrievalRefNum;
    private String sysTraceAuditNumber;
    //交易时间
    private String transDate;
    //交易金额
    private String trxAmt;
    //交易币种
    private String trxCurrency;
    //备注
    private String postscript;

    public String getPostscript() {
        return postscript;
    }

    public void setPostscript(String postscript) {
        this.postscript = postscript;
    }

    public boolean getGpFlag() {
        if (TextUtils.isEmpty(gpFlag)) {
            return false;
        }
//        gpFlag 1：符合  0：不符合
        return gpFlag.equals("1");
    }

    public void setGpFlag(String gpFlag) {
        this.gpFlag = gpFlag;
    }


    private String gpMsgUrl;
    private String gpFlag;
    //交易ID
    private String txnId;

    //积分code
    private String gpCode;
    //抵扣状态
    private String redeemStatus;

    //抵扣失败原因
    public String getGpCode() {
        return gpCode;
    }

    public void setGpCode(String gpCode) {
        this.gpCode = gpCode;
    }

    public String getRedeemStatus() {
        return redeemStatus;
    }

    public void setRedeemStatus(String redeemStatus) {
        this.redeemStatus = redeemStatus;
    }

    public String getRedeemStatusMsg() {
        return redeemStatusMsg;
    }

    public void setRedeemStatusMsg(String redeemStatusMsg) {
        this.redeemStatusMsg = redeemStatusMsg;
    }

    public String getRedeemCount() {
        return redeemCount;
    }

    public void setRedeemCount(String redeemCount) {
        this.redeemCount = redeemCount;
    }

    public String getRedeemGpTnxAmt() {
        return redeemGpTnxAmt;
    }

    public void setRedeemGpTnxAmt(String redeemGpTnxAmt) {
        this.redeemGpTnxAmt = redeemGpTnxAmt;
    }


    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    private String redeemStatusMsg;
    //积分抵扣数量
    private String redeemCount;
    //积分抵扣金额
    private String redeemGpTnxAmt;
    //积分抵扣后实际支付金额
    private String payAmt;

    private String smaUsedGp;

    public String getSmaUsedGp() {
        return smaUsedGp;
    }

    public void setSmaUsedGp(String smaUsedGp) {
        this.smaUsedGp = smaUsedGp;
    }

    public String getCcUsedGP() {
        return ccUsedGP;
    }

    public void setCcUsedGP(String ccUsedGP) {
        this.ccUsedGP = ccUsedGP;
    }

    private String ccUsedGP;


    public String getGpBalance() {
        return gpBalance;
    }

    public void setGpBalance(String gpBalance) {
        this.gpBalance = gpBalance;
    }

    private String gpBalance;

    public String getPayWithPointFlag() {
        return payWithPointFlag;
    }

    /**
     * 是否进行积分抵扣
     *
     * @return
     */
    public boolean isPayWithPoint() {
        return !TextUtils.isEmpty(getPayWithPointFlag()) && getPayWithPointFlag().equals("1");
    }


    public int getColorSign() {
        if (TextUtils.isEmpty(colorSign)) {
            return Color.BLACK;
        }
        return Color.parseColor(colorSign);
    }

    /**
     * 积分抵扣是否成功
     *
     * @return
     */
    public boolean isPayWithPointSuccess() {
        return !TextUtils.isEmpty(getRedeemStatus()) && !getRedeemStatus().equals("1");
    }


    public void setPayWithPointFlag(String payWithPointFlag) {
        this.payWithPointFlag = payWithPointFlag;
    }

    private String payWithPointFlag;

    public String getOriginalAmt() {
        return originalAmt;
    }

    public void setOriginalAmt(String originalAmt) {
        this.originalAmt = originalAmt;
    }

    private String originalAmt;

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    private String discount;

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    private String cardType;

    public String getUiType() {
        return uiType;
    }

    public void setUiType(String uiType) {
        this.uiType = uiType;
    }

    private String uiType;//ui界面使用区别是标题还是内容

    public String getUiTitle() {
        return uiTitle;
    }

    public void setUiTitle(String uiTitle) {
        this.uiTitle = uiTitle;
    }

    private String uiTitle;

    public String getAcquirerIIN() {
        return acquirerIIN;
    }

    public void setAcquirerIIN(String acquirerIIN) {
        this.acquirerIIN = acquirerIIN;
    }

    public String getForwardingIIN() {
        return forwardingIIN;
    }

    public void setForwardingIIN(String forwardingIIN) {
        this.forwardingIIN = forwardingIIN;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getQrcVoucherNumber() {
        return qrcVoucherNumber;
    }

    public void setQrcVoucherNumber(String qrcVoucherNumber) {
        this.qrcVoucherNumber = qrcVoucherNumber;
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }

    public String getRetrievalRefNum() {
        return retrievalRefNum;
    }

    public void setRetrievalRefNum(String retrievalRefNum) {
        this.retrievalRefNum = retrievalRefNum;
    }

    public String getSysTraceAuditNumber() {
        return sysTraceAuditNumber;
    }

    public void setSysTraceAuditNumber(String sysTraceAuditNumber) {
        this.sysTraceAuditNumber = sysTraceAuditNumber;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTrxAmt() {
        return trxAmt;
    }

    public void setTrxAmt(String trxAmt) {
        this.trxAmt = trxAmt;
    }

    public String getTrxCurrency() {
        return trxCurrency;
    }

    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getIsProm() {
        return isProm;
    }

    public void setIsProm(String isProm) {
        this.isProm = isProm;
    }

    /**
     * 是否是特别兑换率
     *
     * @return
     */
    public boolean isProm() {
        if (TextUtils.isEmpty(isProm)) {
            return false;
        } else {
            return isProm.equals("Y") || isProm.equals("A");
        }
    }

    private String isProm;//是否是特别兑换率
    /**
     * app call app 支持h5 H5 APP
     */
    private String fromType;

    public boolean isAppCallAppFromH5() {
        if (TextUtils.isEmpty(fromType)) {
            return false;
        }
        return TextUtils.equals(fromType, "H5");
    }
    public String getGpMsgUrl() {
        return gpMsgUrl;
    }

    public void setGpMsgUrl(String gpMsgUrl) {
        this.gpMsgUrl = gpMsgUrl;
    }
}
