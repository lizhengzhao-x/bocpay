package cn.swiftpass.wallet.intl.module.transactionrecords.view;

import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.ApiConstant;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.entity.SweptCodePointEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.view.CheckEcouponsDetailPop;
import cn.swiftpass.wallet.intl.module.scan.GradeReduceSuccessActivity;
import cn.swiftpass.wallet.intl.module.setting.WebViewActivity;
import cn.swiftpass.wallet.intl.module.transactionrecords.contract.GradeReduceContract;
import cn.swiftpass.wallet.intl.module.transactionrecords.presenter.GradeReducePresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.utils.BasicUtils;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.BigDecimalFormatUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.utils.SpUtils;

public class GradeReduceActivity extends BaseCompatActivity<GradeReduceContract.Presenter> implements GradeReduceContract.View {

    private static final String TAG = "GradeReduceActivity";
    @BindView(R.id.tv_gr_title_symbol)
    TextView tvSymbol;
    @BindView(R.id.tv_gr_title_money)
    TextView tvMoney;
    @BindView(R.id.tv_gr_merchant)
    TextView tvMerchant;
    @BindView(R.id.tv_gr_account)
    TextView tvAccount;
    @BindView(R.id.iv_gr_centerimage)
    ImageView ivCenterImage;
    @BindView(R.id.tv_gr_totalGrade)
    TextView tvTotalGrade;
    @BindView(R.id.tv_gr_reduceGrade)
    TextView tvReduceGrade;
    @BindView(R.id.tv_gr_retainGrade)
    TextView tvRetainGrade;
    @BindView(R.id.tv_gr_reduceMoney)
    TextView tvReduceMoney;
    @BindView(R.id.rl_gr_wainingInfo)
    RelativeLayout rlWainingInfo;
    @BindView(R.id.tv_gr_termRule)
    TextView tvTermRule;
    @BindView(R.id.tv_gr_confirm)
    TextView tvConfirm;

    @BindView(R.id.id_buttom_text)
    TextView idButtomText;

    private SweptCodePointEntity sweptCodePointEntity;
    public static final String GRADEREDUCEINFO = "GRADEREDUCEINFO";


    @Override
    protected void init() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            LogUtils.e(TAG, "error getIntent().getExtras() null");
            finish();
            return;
        }
        setToolBarTitle(R.string.CCP2105_3_1);
        sweptCodePointEntity = (SweptCodePointEntity) getIntent().getExtras().getSerializable(GRADEREDUCEINFO);
        tvMoney.setText(BigDecimalFormatUtils.forMatWithDigs(sweptCodePointEntity.getTrxAmt(), 2));
        //商户名称
        tvMerchant.setText(sweptCodePointEntity.getMerchantName());
        //支付的卡号后四位
        tvAccount.setText(AndroidUtils.getSubMasCardNumberTitle(mContext, sweptCodePointEntity.getPanFour(), sweptCodePointEntity.getCardType()));
        //支付的币种符号
        tvSymbol.setText(AndroidUtils.getTrxCurrency(sweptCodePointEntity.getTrxCurrency()));
        try {
            tvTotalGrade.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getAvaGpCount()), false));
            tvReduceGrade.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getRedeemGpCount()), false));
            tvRetainGrade.setText(AndroidUtils.formatPrice(Double.valueOf(sweptCodePointEntity.getGpBalance()), false));
            tvReduceMoney.setText(AndroidUtils.getTrxCurrency(sweptCodePointEntity.getTranCur()) + " " + sweptCodePointEntity.getRedeemGpTnxAmt());
        } catch (Exception e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        TextPaint tp = tvReduceMoney.getPaint();
        tp.setFakeBoldText(false);
        if (!TextUtils.isEmpty(sweptCodePointEntity.getGpDateCmt())) {
            idButtomText.setText(sweptCodePointEntity.getGpDateCmt());
        } else {
            idButtomText.setVisibility(View.GONE);
        }
        tvReduceMoney.setTextColor(sweptCodePointEntity.getColorSign());
        String totalStr = getResources().getString(R.string.CCP2105_3_11);
        BasicUtils.initSpannableStrWithTv(totalStr, "##", tvTermRule, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick()) return;
                String lan = SpUtils.getInstance(GradeReduceActivity.this).getAppLanguage();
                HashMap<String, Object> mHashMaps = new HashMap<>();
                if (AndroidUtils.isZHLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_SIMP_URL);
                } else if (AndroidUtils.isHKLanguage(lan)) {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_TRAD_URL);
                } else {
                    mHashMaps.put(Constants.DETAIL_URL, ApiConstant.WALLET_GRADE_ENGLISH_URL);
                }
                mHashMaps.put(Constants.DETAIL_TITLE, getString(R.string.CP1_4a_9));
                ActivitySkipUtil.startAnotherActivity(getActivity(), WebViewActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
        });

        rlWainingInfo.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                //被扫成功之后 使用积分抵扣选项
                String message = "";
                if (sweptCodePointEntity == null) return;
                if (!TextUtils.isEmpty(sweptCodePointEntity.getCardType()) && sweptCodePointEntity.getCardType().equals("1")) {
                    message = getString(R.string.EPTNC_CC_01);
                } else {
                    message = getString(R.string.EPTNC_AC_01);
                }
                showEcouponCountDetail(message);
            }
        });

        GlideApp.with(this).load(sweptCodePointEntity.getGpMsgUrl())
                .skipMemoryCache(true)
                .placeholder(R.mipmap.banner_bg_preview)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(ivCenterImage);

        tvConfirm.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                verifyPwdInfo();
            }
        });
    }

    private void verifyPwdInfo() {
        VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (mPresenter != null) {
                    mPresenter.gradeReduceInfo(sweptCodePointEntity.getTxnId());
                }
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(GradeReduceActivity.this, errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });
    }


    /**
     * 被扫成功之后 显示积分获取详情
     *
     * @param titleDetail
     */
    private void showEcouponCountDetail(String titleDetail) {
        CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), "", titleDetail);
        checkEcouponsDetailPop.show();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transaction_grade_reduce;
    }


    @Override
    protected GradeReduceContract.Presenter createPresenter() {
        return new GradeReducePresenter();
    }

    @Override
    public void gradeReduceInfoSuccess(SweptCodePointEntity sweptCodePointEntity) {
        //需要更新列表 刷新积分抵扣状态
        GradeReduceSuccessActivity.startActivityFromOrderDetail(GradeReduceActivity.this, sweptCodePointEntity);
        finish();
    }

    @Override
    public void gradeReduceInfoFailed(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);

    }

}
