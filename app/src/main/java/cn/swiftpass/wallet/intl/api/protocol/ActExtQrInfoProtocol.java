package cn.swiftpass.wallet.intl.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

public class ActExtQrInfoProtocol extends BaseProtocol {
    String cardId;
    String payOrderInfo;

    public ActExtQrInfoProtocol(String cardIdIn, String payOrderInfoIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/transaction/actExtQrInfo";
        this.cardId = cardIdIn;
        this.payOrderInfo = payOrderInfoIn;
    }

    @Override
    public void packData() {
        super.packData();
        if (!TextUtils.isEmpty(cardId)) {
            mBodyParams.put(RequestParams.CARDID, cardId);
        }
        if (!TextUtils.isEmpty(payOrderInfo)) {
            mBodyParams.put(RequestParams.PAYORDERINFO, payOrderInfo);
        }
    }
}
