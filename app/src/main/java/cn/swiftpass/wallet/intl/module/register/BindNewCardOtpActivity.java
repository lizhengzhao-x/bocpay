package cn.swiftpass.wallet.intl.module.register;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import cn.swiftpass.boc.commonui.base.mvp.OnMsgClickCallBack;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.contract.BindNewCardOtpContract;
import cn.swiftpass.wallet.intl.module.register.contract.OtpAction;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;
import cn.swiftpass.wallet.intl.module.register.presenter.BindNewCardOtpPresenter;
import cn.swiftpass.wallet.intl.module.virtualcard.view.AbstractSendOTPActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;

import static cn.swiftpass.wallet.intl.module.register.ConfirmBindNewCardActivity.DATA_OTP_MOBILE;
import static cn.swiftpass.wallet.intl.module.register.ConfirmBindNewCardActivity.DATA_SELECT_CARD_LIST;


public class BindNewCardOtpActivity extends AbstractSendOTPActivity implements BindNewCardOtpContract.View {
    private ArrayList<BindNewCardEntity> selectCardList;

    private BindNewCardOtpContract.Presenter mPresenterDetail;
    public static final String DATA_BIND_NEW_CARD_RESULT = "DATA_BIND_NEW_CARD_RESULT";
    private int flowType;

    @Override
    public void init() {
        if (getIntent() != null) {
            flowType = getIntent().getIntExtra(Constants.CURRENT_PAGE_FLOW, -1);
            Serializable serializableExtra = getIntent().getSerializableExtra(DATA_SELECT_CARD_LIST);
            if (serializableExtra != null) {
                selectCardList = (ArrayList<BindNewCardEntity>) serializableExtra;
            }
            phoneNo = getIntent().getStringExtra(DATA_OTP_MOBILE);
        }
        setToolBarTitle(R.string.KBCC2105_4_1);

        if (mPresenter instanceof BindNewCardOtpContract.Presenter) {
            mPresenterDetail = (BindNewCardOtpContract.Presenter) mPresenter;
        }
        super.init();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected BindNewCardOtpContract.Presenter createPresenter() {
        return new BindNewCardOtpPresenter();
    }

    @Override
    protected void verifyOtpAction() {
        mPresenterDetail.bindNewCardVerifyOtp(OtpAction.OTP_BIND_NEW_CARD, etwdOtpCode.getEditText().getText().toString().trim());
    }

    @Override
    protected void sendOtpAction(boolean isTryAgain) {
        mPresenterDetail.bindNewCardSendOtp(selectCardList, OtpAction.OTP_BIND_NEW_CARD, isTryAgain);
    }


    @Override
    public void bindNewCardSendOtpSuccess(BaseEntity response, boolean isTryAgain) {
        setResetTime(true, phoneNo);
        //如果是重发 弹窗提示
        if (isTryAgain) {
            showErrorMsgDialog(mContext, mContext.getString(R.string.IDV_3a_20_6), new OnMsgClickCallBack() {
                @Override
                public void onBtnClickListener() {
                    clearOtpInput();
                }
            });
        } else {
            clearOtpInput();
        }
    }

    @Override
    public void bindNewCardSendOtpError(String errorCode, String errorMsg, boolean isTryAgain) {
        showErrorMsgDialog(BindNewCardOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
        showRetrySendOtp();
    }

    @Override
    public void bindNewCardVerifyOtpSuccess(BaseEntity response) {
        mPresenterDetail.bindNewCardList(selectCardList, OtpAction.OTP_BIND_NEW_CARD);
    }

    @Override
    public void bindNewCardVerifyOtpError(String errorCode, String errorMsg) {
        showErrorMsgDialog(BindNewCardOtpActivity.this, errorMsg, new OnMsgClickCallBack() {
            @Override
            public void onBtnClickListener() {
                clearOtpInput();
            }
        });
    }

    @Override
    public void bindNewCardListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);

    }

    @Override
    public void bindNewCardListSuccess(BindNewCardResultEntity response) {
        if (response != null) {
            mHashMaps = new HashMap<>();
            if (!TextUtils.isEmpty(response.getErrorMsg())) {
                mHashMaps.put(DATA_BIND_NEW_CARD_RESULT, response);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, flowType);
                if (MyActivityManager.isAppOnForeground(this)) {
                    ActivitySkipUtil.startAnotherActivity(BindNewCardOtpActivity.this, BindNewCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    mHashMaps = null;
                    this.finish();
                }
            } else {
                mHashMaps.put(DATA_BIND_NEW_CARD_RESULT, response);
                mHashMaps.put(Constants.CURRENT_PAGE_FLOW, flowType);
                if (MyActivityManager.isAppOnForeground(this)) {
                    ActivitySkipUtil.startAnotherActivity(BindNewCardOtpActivity.this, BindNewCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                    mHashMaps = null;
                    this.finish();
                }
            }

        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mHashMaps != null) {
            BindNewCardResultEntity bindNewCardResultEntity = (BindNewCardResultEntity) mHashMaps.get(DATA_BIND_NEW_CARD_RESULT);
            if (!TextUtils.isEmpty(bindNewCardResultEntity.getErrorMsg())) {
                ActivitySkipUtil.startAnotherActivity(BindNewCardOtpActivity.this, BindNewCardFailActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            } else {
                ActivitySkipUtil.startAnotherActivity(BindNewCardOtpActivity.this, BindNewCardSuccessActivity.class, mHashMaps, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
            }
            mHashMaps = null;
            this.finish();
        }
    }
}
