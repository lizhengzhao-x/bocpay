package cn.swiftpass.wallet.intl.widget;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cn.swiftpass.wallet.intl.R;

/**
 * Created by admin on 2017/11/23.
 * 所有共同的titlebar控件
 */

public class TitleBarWidget extends RelativeLayout {
    private RelativeLayout mTitleLayout;
    private Context mContext;

    public TitleBarWidget(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public TitleBarWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public TitleBarWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        mTitleLayout = (RelativeLayout) inflater.inflate(R.layout.title_bar, null);
        addView(mTitleLayout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }

    public void setTitleBackgroundColor(int id) {
        mTitleLayout.setBackgroundColor(id);
    }

    public void setTitleBackground(int id) {
        mTitleLayout.setBackgroundResource(id);
    }

    public View getView(int id) {
        return mTitleLayout.findViewById(id);
    }

    /**
     * @param @param  activity
     * @param @return 设定文件
     * @return ImageButton    返回类型
     * @throws
     * @Title: showBackBtn
     * @Description: 显示默认的返回按钮
     */
    public ImageButton showBackBtn(final Activity activity) {
        LinearLayout llayout_back = (LinearLayout) getView(R.id.llayout_back);
        llayout_back.setVisibility(View.VISIBLE);
        ImageButton iBtn = (ImageButton) getView(R.id.ibtn_back);
        iBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                activity.onBackPressed();
            }
        });
        return iBtn;
    }

    /**
     * @return void    返回类型
     * @throws
     * @Title: hideBackBtn
     * @Description: 隐藏返回按钮
     */
    public void hideBackBtn() {
        LinearLayout llayout_back = (LinearLayout) getView(R.id.llayout_back);
        llayout_back.setVisibility(View.GONE);
    }

    private String getString(Object text) {
        String title = "";
        if (text instanceof Integer) {
            title = getResources().getString((Integer) text);
        } else if (text instanceof String) {
            title = (String) text;
        }
        return title;
    }

    /**
     * @param text
     * @return TextView
     * @throws
     * @Title: showTitleText
     * @Description: 显示文字标题，不带logo图片
     */
    public View showTitleText(Object text) {
        return showTitleText(getString(text), null);
    }

    /**
     * @param text
     * @param icoID 图片的资源id
     * @return View    返回类型
     * @throws TextView
     * @Title: showTitleText
     * @Description: 显示带有logo图片的文字标题
     */
    public View showTitleText(String text, Integer icoID) {
        TextView tv_title = (TextView) getView(R.id.tv_title_name);
        tv_title.setVisibility(VISIBLE);
        tv_title.setText(text);
        if (icoID != null)
            tv_title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(icoID), null, null, null);
        return tv_title;
    }

    /**
     * @param text 文字
     * @return TextView
     * @Title: ShowRightText
     * @Description: (显示右边的文字按钮)
     */
    public View showRightText(Object text) {
        return showRightText(getString(text), null);
    }

    /**
     * @param icoID 图片资源id
     * @return TextView
     * @Title: ShowRightImage
     * @Description: (显示右边的图片按钮)
     */
    public View showRightImage(int icoID) {
        return showRightText(null, icoID);
    }

    /**
     * @param text  文字
     * @param icoID 图片资源id
     * @return TextView
     * @Title: showRightText
     * @Description: (显示右边的图片和文字)
     */
    public View showRightText(String text, Integer icoID) {
        TextView tv_title = (TextView) getView(R.id.tv_right_name);
        tv_title.setVisibility(VISIBLE);
        if (!TextUtils.isEmpty(text))
            tv_title.setText(text);
        else
            tv_title.setText("");
        if (icoID != null)
            tv_title.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(icoID), null, null, null);
        return tv_title;
    }
}
