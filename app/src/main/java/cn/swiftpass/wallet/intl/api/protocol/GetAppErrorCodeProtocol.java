package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 动态拉取错误码
 */
public class GetAppErrorCodeProtocol extends BaseProtocol {
    String version;

    public GetAppErrorCodeProtocol(String version, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.version = version;
        mUrl = "api/sysconfig/getAppErrorCode";
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERSION, version);
    }


}
