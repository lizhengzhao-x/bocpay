package cn.swiftpass.wallet.intl.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;


/**
 * @name WalletBasicAppUIMergeVersion
 * @class name：cn.swiftpass.wallet.intl.widget
 * @class describe
 * @anthor zhangfan
 * @time 2019/9/4 14:40
 * @change
 * @chang time
 * @class describe
 */
public class TouchRecyclerView extends RecyclerView {


    private boolean hadMultiPoint;
    private float scrollX;
    private final int IntervalTime = 300;
    private long lastTime = 0;

    public TouchRecyclerView(@NonNull Context context) {
        super(context);
    }

    public TouchRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TouchRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
            if (!hadMultiPoint) {
                scrollX = event.getRawX();
            }
        }
        if (event.getActionMasked() == MotionEvent.ACTION_POINTER_DOWN) {
            hadMultiPoint = event.getPointerCount() > 1;
            return false;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_POINTER_UP) {
            if (event.getPointerCount() == 0) hadMultiPoint = false;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_UP) {
            hadMultiPoint = false;
            scrollX = 0f;
            lastTime = System.currentTimeMillis();
        }
        if (event.getActionMasked() == MotionEvent.ACTION_CANCEL) {
            hadMultiPoint = false;
            scrollX = 0f;
            lastTime = System.currentTimeMillis();
        }

        long nowTime = System.currentTimeMillis();
        if (event.getActionMasked() == MotionEvent.ACTION_MOVE && nowTime < lastTime + IntervalTime) {

            return false;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_MOVE && hadMultiPoint) {
            return false;
        }
        if (event.getActionMasked() == MotionEvent.ACTION_MOVE && Math.abs(event.getRawX() - scrollX) > AndroidUtils.getScreenWidth(getContext()) * 0.7) {
            return false;
        }

        if (event.getActionMasked() == MotionEvent.ACTION_MOVE && event.getPointerCount() > 1) {
            return false;
        } else {
            return super.dispatchTouchEvent(event);
        }

    }


}
