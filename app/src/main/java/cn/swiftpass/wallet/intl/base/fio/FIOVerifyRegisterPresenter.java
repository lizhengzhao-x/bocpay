package cn.swiftpass.wallet.intl.base.fio;


import cn.swiftpass.boc.commonui.base.mvp.IView;

/**
 * @name cn.swiftpass.bocbill.model.base.fio
 * @class name：FIOVerifyRegisterPresenter
 * @class describe
 * @anthor zhangfan
 * @time 2019/7/13 18:30
 * @change
 * @chang time
 * @class Fio登记注册是否成功服务器校验
 */
public interface FIOVerifyRegisterPresenter<V extends IView> {
    void onFioVerifyRegister(String walletId, String resp, String regType);    //检测FIO在EWA服务器是否注册
}
