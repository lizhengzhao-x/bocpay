package cn.swiftpass.wallet.intl.module.callapp;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import org.greenrobot.eventbus.EventBus;

import cn.swiftpass.wallet.intl.BuildConfig;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.TempSaveHelper;
import cn.swiftpass.wallet.intl.dialog.CustomDialog;
import cn.swiftpass.wallet.intl.entity.event.AppCallAppEventItem;
import cn.swiftpass.wallet.intl.event.AppCallAppEvent;
import cn.swiftpass.wallet.intl.event.UserLoginEventManager;
import cn.swiftpass.wallet.intl.utils.LogUtils;

public class AppCallAppUtils {

    private static final String FPS_TAG = "fps";
    public static final String FPS_FLOW_ACTION = "EXTFPSPAY";

    /**
     * app call app退出拦截弹框
     *
     * @param mActivity
     * @param isClearMerchant     点击取消，是否要清除link
     * @param isFinishCurrentPage 点击取消，是否要关闭当前页面
     */
    public static void exitMerchantApp(Activity mActivity, boolean isClearMerchant, boolean isFinishCurrentPage) {
        if (mActivity == null) return;


        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
        builder.setTitle("");
        builder.setMessage(mActivity.getString(R.string.ACA2101_0_1));
//        builder.setTextBold(true);
        builder.setTextSize(16);
        builder.setPositiveButton(mActivity.getString(R.string.ACA2101_0_4), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isBackToMerchantFps()) {
                    //FPS  appcallapp 退回商户方式不同
                    goToMerchantAppForFps(mActivity, false);
                    return;
                } else {
                    goToMerchantApp(mActivity);
                    if (isFinishCurrentPage) {
                        mActivity.finish();
                    }
                }
                UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
            }
        });
        builder.setNegativeButton(mActivity.getString(R.string.ACA2101_0_2), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (isBackToMerchantFps()) {

                } else {
                    if (isClearMerchant) {
                        TempSaveHelper.clearLink();
                    }
                    if (isFinishCurrentPage) {
                        mActivity.finish();
                    }
                }
            }
        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
        CustomDialog mDealDialog = builder.create();
        mDealDialog.setCancelable(false);
        mDealDialog.show();
    }


    /**
     * 当前返回商户是appcallapp 还是fps支付
     *
     * 退回商户 appcallapp是通过intent applink
     *         fps 是通过setResult
     *
     * @return
     */
    private static boolean isBackToMerchantFps() {
        AppCallAppEvent appCallAppEvent = UserLoginEventManager.getInstance().getEventDetail(AppCallAppEvent.class);
        return
                appCallAppEvent != null &&
                        appCallAppEvent.getCurrentEventHashParams() != null &&
                        appCallAppEvent.getCurrentEventHashParams().get(AppCallAppEvent.APP_LINK_URL) != null &&
                        AppCallAppUtils.isFromFpsUrl((String) appCallAppEvent.getCurrentEventHashParams().get(AppCallAppEvent.APP_LINK_URL));
    }

    public static void goToMerchantApp(Context mContext, String merchantLink, boolean isFromH5) {
        LogUtils.i("AppCallAppUtils", "merchantLink:" + merchantLink);
        if (!TextUtils.isEmpty(merchantLink)) {
            TempSaveHelper.clearLink();
            try {
                if (isFromH5) {
                    Intent intent = new Intent();
                    intent.setAction("android.intent.action.VIEW");
                    Uri content_url = Uri.parse(merchantLink);
                    intent.setData(content_url);
                    mContext.startActivity(intent);
                } else {
                    Uri uri = Uri.parse(merchantLink);
                    Intent it = new Intent(Intent.ACTION_VIEW, uri);
                    it.addFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME | Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(it);
                }
            } catch (Exception e) {
//                if (!Constants.BUILD_FLAVOR_PRD.equals(BuildConfig.FLAVOR)) {
//                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
//                }
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void goToMerchantApp(Context mContext) {
        UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
        goToMerchantApp(mContext, TempSaveHelper.getMerchantLink(), TempSaveHelper.isIsH5Pay());
    }

    public static void clearAppCallAppEvent() {
        UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
        TempSaveHelper.clearLink();
    }

//
//    public static void goToMerchantAppForFpsWithFailed(Activity mActivity) {
////        UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
////        goToMerchantApp(mContext, getFPSCallBackUrl(TempSaveHelper.getMerchantLink(), isSuccess), TempSaveHelper.isIsH5Pay());
//        if (mActivity == null) return;
//        CustomDialog.Builder builder = new CustomDialog.Builder(mActivity);
//        builder.setTitle("");
//        builder.setMessage(mActivity.getString(R.string.ACA2101_0_1));
////        builder.setTextBold(true);
//        builder.setTextSize(16);
//        builder.setPositiveButton(mActivity.getString(R.string.ACA2101_0_4), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                goToMerchantApp(mActivity, getFPSCallBackUrl(TempSaveHelper.getMerchantLink(), false), TempSaveHelper.isIsH5Pay());
//                UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
//                mActivity.finish();
//            }
//        });
//        builder.setNegativeButton(mActivity.getString(R.string.ACA2101_0_2), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        }).setLeftColor(R.color.color_green).setRightColor(R.color.color_green);
//        CustomDialog mDealDialog = builder.create();
//        mDealDialog.setCancelable(false);
//        mDealDialog.show();
//
//    }

    public static void goToMerchantAppForFps(Context mContext, boolean isSuccess) {
        UserLoginEventManager.getInstance().removeItemEvent(AppCallAppEvent.class);
        EventBus.getDefault().postSticky(new AppCallAppEventItem(isSuccess ? AppCallAppEventItem.EVENT_APP_CALLAPP_SUCCESS : AppCallAppEventItem.EVENT_APP_CALLAPP_CANCEL, ""));
    }

    public static String getFPSCallBackUrl(String callBackUrl, boolean isSuccess) {
        String finalUrl = "";
        if (isSuccess) {
            finalUrl = callBackUrl + "&" + "is_successful=" + 0;
        } else {
            finalUrl = callBackUrl + "&" + "is_successful=" + 1;
        }
        return finalUrl;
    }

    /**
     * 判断通过intent调过来的连接是否是fps支付 去掉前缀 然后看是否是fps开头
     *
     * @param url
     * @return
     */
    public static boolean isFromFpsUrl(String url) {
        String splitUrl = url.replace("https://", "").replace("http://", "").replace("HTTP://", "").replace("HTTPS://", "");
        return splitUrl.startsWith(FPS_TAG);
    }

}
