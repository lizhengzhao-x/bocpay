package cn.swiftpass.wallet.intl.module.setting.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.api.protocol.NotificationStatusProtocol;
import cn.swiftpass.wallet.intl.entity.NotificationStatusEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.SettingContract;

public class SettingPresenter extends BasePresenter<SettingContract.View> implements SettingContract.Presenter {
    @Override
    public void getNotificationStatus(boolean showDialog,String status, String action) {
        if (getView()!=null){
            getView().showDialogNotCancel();
        }
        new NotificationStatusProtocol(status, action, new NetWorkCallbackListener<NotificationStatusEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNotificationStatusFailed(errorCode, errorMsg);
                }
            }

            @Override
            public void onSuccess(NotificationStatusEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().getNotificationStatusSuccess(response);
                }
            }
        }).execute();
    }


}
