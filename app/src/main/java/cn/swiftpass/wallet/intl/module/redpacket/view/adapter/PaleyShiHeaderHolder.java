package cn.swiftpass.wallet.intl.module.redpacket.view.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.module.redpacket.entity.PaleyShiHomeEntity;
import cn.swiftpass.wallet.intl.module.redpacket.view.RedPacketAddressActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.ButtonUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;

public class PaleyShiHeaderHolder extends RecyclerView.ViewHolder {

    private final Activity activity;
    @BindView(R.id.iv_paley_shi_banner)
    ImageView ivPaleyShiBanner;
    @BindView(R.id.tv_paley_shi_title)
    TextView tvPaleyShiTitle;
    @BindView(R.id.tv_paley_shi_content)
    TextView tvPaleyShiContent;
    @BindView(R.id.tv_paleysi_confirm)
    TextView tvPaleysiConfirm;

    public PaleyShiHeaderHolder(Activity activity, View holder) {
        super(holder);
        ButterKnife.bind(this, itemView);
        this.activity = activity;
        //根据宽度 等比 算出高度
        int height = AndroidUtils.getScreenWidth(activity) * 188 / 374;

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ivPaleyShiBanner.getLayoutParams();
        layoutParams.height = height;
        ivPaleyShiBanner.setLayoutParams(layoutParams);
    }

    public void setData(PaleyShiHomeEntity paleyShiHomeEntity) {

        if (paleyShiHomeEntity != null) {
            GlideApp.with(activity).load(paleyShiHomeEntity.getRedPacketBanner()).placeholder(R.mipmap.img_banner_redpacket_skeleton)
                    .into(ivPaleyShiBanner);

            tvPaleyShiTitle.setText(paleyShiHomeEntity.getRedPacketIntroduceTitle());
            tvPaleyShiContent.setText(paleyShiHomeEntity.getRedPacketIntroduce());
        }
        tvPaleysiConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ButtonUtils.isFastDoubleClick(v.getId())) return;
                if (null == activity) {
                    return;
                }
                ActivitySkipUtil.startAnotherActivity(activity, RedPacketAddressActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);

            }
        });

    }
}
