package cn.swiftpass.wallet.intl.module.cardmanagement.contract;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.wallet.intl.entity.CreditCardGradeEntity;

/**
 * 信用卡总积分
 */
public class TotalCreditCardGradeContract {

    public interface View extends IView {

        void getTotalGradeInfoSuccess(CreditCardGradeEntity response);

        void getTotalGradeInfoError(String errorCode, String errorMsg);
    }


    public interface Presenter extends IPresenter<TotalCreditCardGradeContract.View> {
        void getTotalGradeInfo();
    }

}
