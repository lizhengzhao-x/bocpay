package cn.swiftpass.wallet.intl.module.register.adapter;

import java.util.ArrayList;

import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;

public interface OnConfirmCardListLinstener {
    void onConfirmList(ArrayList<BindNewCardEntity> selectData);
}
