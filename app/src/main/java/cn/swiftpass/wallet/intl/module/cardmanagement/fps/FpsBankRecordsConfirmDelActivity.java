package cn.swiftpass.wallet.intl.module.cardmanagement.fps;


import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.swiftpass.boc.commonui.base.adapter.CommonRcvAdapter;
import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.boc.commonui.base.adapter.util.IAdapter;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.api.ApiProtocolImplManager;
import cn.swiftpass.wallet.intl.app.constants.FpsConst;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.FpsBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsConfirmEntity;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordBean;
import cn.swiftpass.wallet.intl.entity.FpsOtherBankRecordDisplayBean;
import cn.swiftpass.wallet.intl.entity.FpsPreCheckEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

/**
 * @author Created by ramon on 2018/8/12.
 * 查询他进行删除时，确认删除记录信息
 */

public class FpsBankRecordsConfirmDelActivity extends BaseCompatActivity {
    //记录列表
    @BindView(R.id.rv_bank_list)
    RecyclerView mBankListRV;
    //手机或邮箱
    @BindView(R.id.tv_account_id)
    TextView mAccountIdTV;

    @BindView(R.id.tv_account_label)
    TextView mAccountLabelTV;

    @BindView(R.id.tv_fps_delete)
    TextView mFpsDeleteTV;


    CommonRcvAdapter<FpsOtherBankRecordDisplayBean> mAdapter;

    ArrayList<FpsOtherBankRecordBean> mSelectDeleteList;
    //添加时的中银银行记录
    FpsBankRecordBean mBankBean;
    FpsPreCheckEntity mFpsPreCheckEntityDel;

    protected IPresenter loadPresenter() {
        return null;
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_fps_bank_records_confirm_del;
    }


    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void init() {
        setToolBarTitle(R.string.C3_04_3_1);
        parseIntent();
        initData();
    }

    private void parseIntent() {
        Intent intent = getIntent();
        if (null != intent) {
            mBankBean = (FpsBankRecordBean) intent.getSerializableExtra(FpsConst.FPS_ADD_DATA_BANK);
            mFpsPreCheckEntityDel = (FpsPreCheckEntity) intent.getSerializableExtra(FpsConst.FPS_PRE_CHECK_ENTITY_DEL);
            mSelectDeleteList = (ArrayList<FpsOtherBankRecordBean>) intent.getSerializableExtra(FpsConst.FPS_DEL_RECORDS);
        }
    }

    private void initData() {
        if (null != mBankBean) {
            if (FpsConst.ACCOUNT_ID_TYPE_EMAIL.equalsIgnoreCase(mBankBean.getAccountIdType())) {
                mAccountLabelTV.setText(R.string.C3_01_2_2);
                mAccountIdTV.setText(AndroidUtils.fpsEmailFormat(mBankBean.getAccountId()));
            } else {
                mAccountLabelTV.setText(R.string.C3_01_2_1);
                mAccountIdTV.setText(AndroidUtils.fpsPhoneFormat(mBankBean.getAccountId()));
            }
            initListView();
        }
    }

    private void initListView() {
        if (null != mFpsPreCheckEntityDel && null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            ArrayList<FpsOtherBankRecordDisplayBean> beanList = new ArrayList<>();
            for (FpsOtherBankRecordBean recordBean : mSelectDeleteList) {
                FpsOtherBankRecordDisplayBean displayBean = new FpsOtherBankRecordDisplayBean();
                displayBean.setRecordBean(recordBean);
                displayBean.setHideCheck(true);
                beanList.add(displayBean);
            }
            if (!beanList.isEmpty()) {
                mBankListRV.setVisibility(View.VISIBLE);
                mAdapter = getAdapter(beanList);
                mBankListRV.setAdapter(mAdapter);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                layoutManager.setRecycleChildrenOnDetach(true);
                mBankListRV.setLayoutManager(layoutManager);
                ((IAdapter<FpsOtherBankRecordDisplayBean>) mBankListRV.getAdapter()).setData(beanList);
                mBankListRV.getAdapter().notifyDataSetChanged();
            } else {
                mBankListRV.setVisibility(View.GONE);
            }
        } else {
            mBankListRV.setVisibility(View.GONE);
        }
    }


    @OnClick({R.id.tv_fps_delete})
    public void onViewClicked(View view) {
        int viewId = view.getId();
        switch (viewId) {
            case R.id.tv_fps_delete:
                onClickDelete();
                break;
            default:
                break;
        }
    }


    private String getDeleteBankCodes() {
        String bankCode = "";
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            bankCode = mSelectDeleteList.get(0).getBankCode();
            if (mSelectDeleteList.size() > 1) {
                for (int i = 1; i < mSelectDeleteList.size(); i++) {
                    bankCode = bankCode + "||" + mSelectDeleteList.get(i).getBankCode();
                }
            }
        }
        return bankCode;
    }


    private void onClickDelete() {
        if (null != mSelectDeleteList && !mSelectDeleteList.isEmpty()) {
            onDeleteConfirm();
        }
    }

    //step1 确认删除
    private void onDeleteConfirm() {
        if (null != mFpsPreCheckEntityDel && !TextUtils.isEmpty(mFpsPreCheckEntityDel.getFppRefNo())) {
            ApiProtocolImplManager.getInstance().fpsConfirmDel(getActivity(), mBankBean, mFpsPreCheckEntityDel.getFppRefNo(), getDeleteBankCodes(), FpsConst.FPS_RECORD_DEL_TYPE_OTHER, new NetWorkCallbackListener<FpsConfirmEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    AndroidUtils.showTipDialog(getActivity(), errorMsg);
                }

                @Override
                public void onSuccess(FpsConfirmEntity response) {
                    onDeleteSuc();
                }
            });
        }
    }

    //step1 确认添加
    private void onDeleteSuc() {
        FpsBankRecordsDelSucActivity.startActivity(getActivity(), mBankBean, mSelectDeleteList);
        finish();
    }

    private CommonRcvAdapter<FpsOtherBankRecordDisplayBean> getAdapter(List<FpsOtherBankRecordDisplayBean> data) {
        return new CommonRcvAdapter<FpsOtherBankRecordDisplayBean>(data) {
            @NonNull
            @Override
            public AdapterItem createItem(Object type) {
                return new FpsBindBankListAdapter(mContext, null);
            }
        };
    }

    public static void startActivity(Activity activity, FpsBankRecordBean bean, FpsPreCheckEntity delEntity, ArrayList<FpsOtherBankRecordBean> delList) {
        Intent intent = new Intent(activity, FpsBankRecordsConfirmDelActivity.class);
        intent.putExtra(FpsConst.FPS_PRE_CHECK_ENTITY_DEL, delEntity);
        intent.putExtra(FpsConst.FPS_ADD_DATA_BANK, bean);
        intent.putExtra(FpsConst.FPS_DEL_RECORDS, delList);
        activity.startActivity(intent);
    }
}
