package cn.swiftpass.wallet.intl.utils;

import android.os.CountDownTimer;


public class AdvancedCountdownTimer {

    private static class AdvancedCountdownTimerHolder {
        private static AdvancedCountdownTimer instance = new AdvancedCountdownTimer();
    }

    public static AdvancedCountdownTimer getInstance() {
        return AdvancedCountdownTimer.AdvancedCountdownTimerHolder.instance;
    }

    private String lastMobilePhone = "";
    private int delayFinishTime;

    /**
     * 主要解决登录时输入一个手机号 发送验证码 这个时候推出 重新发送这个验证码 次倒计时为全局处理
     *
     * @param onCountDownListener
     * @param phoneNumber
     * @param totlaSecond
     */
    public void startCountDown(final String phoneNumber, int totlaSecond, final OnCountDownListener onCountDownListener) {
        if (phoneNumber.equals(lastMobilePhone)) {
            //倒计时跟之前登录的倒计时一样，接着上次的时间进行
            countDownEvent(phoneNumber, delayFinishTime, onCountDownListener);
        } else {
            countDownEvent(phoneNumber, totlaSecond, onCountDownListener);
        }
    }

    public void startCountDown(int totlaSecond, final OnCountDownListener onCountDownListener) {

        countDownEvent(totlaSecond, onCountDownListener);
    }

    /**
     * 正常倒计时
     *
     * @param totlaSecond
     * @param onCountDownListener
     */
    public void countDownEvent(int totlaSecond, final OnCountDownListener onCountDownListener) {
        new CountDownTimer(totlaSecond * 1000 + 70, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                int delaySecond = (int) (millisUntilFinished / 1000);
                if (delaySecond < totlaSecond) {
                    LogUtils.d("onTick", "====>" + millisUntilFinished + " == " + delaySecond);
                    delayFinishTime = delaySecond;
                    if (onCountDownListener == null) return;
                    onCountDownListener.onTick(delaySecond);
                }

            }

            @Override
            public void onFinish() {
                if (onCountDownListener == null) return;
                LogUtils.d("onTick", "====>finish");
                onCountDownListener.onFinish();
            }
        }.start();
    }


    /**
     * 正常倒计时
     *
     * @param totlaSecond
     * @param onCountDownListener
     */
    public void countDownEvent(final String phoneNumber, int totlaSecond, final OnCountDownListener onCountDownListener) {
        this.lastMobilePhone = phoneNumber;
        new CountDownTimer(totlaSecond * 1000 + 70, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                int delaySecond = (int) (millisUntilFinished / 1000);
                LogUtils.d("onTick", "====>" + millisUntilFinished + " == " + delaySecond);
                if (delaySecond < totlaSecond) {
                    delayFinishTime = delaySecond;
                    if (onCountDownListener == null) return;
                    onCountDownListener.onTick(delaySecond);
                }
            }

            @Override
            public void onFinish() {
                if (onCountDownListener ==null)return;
                LogUtils.d("onTick", "====>finish");
                onCountDownListener.onFinish();
            }
        }.start();
    }


    public interface OnCountDownListener {
        void onTick(int millisUntilFinished);

        void onFinish();
    }


}
