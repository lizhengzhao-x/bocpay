package cn.swiftpass.wallet.intl.module.virtualcard.view;

import android.content.Intent;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.register.adapter.AreaSelectionAdapter;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.AreaSelectionEntity;

import static cn.swiftpass.wallet.intl.module.register.IdAuthInfoFillActivity.CHOICE_AREA_CODE;


/**
 * 地区选中
 */
public class IDCardSelectionActivity extends BaseCompatActivity {

    private RecyclerView mRecyclerView;
    private List<AreaSelectionEntity> mList = new ArrayList<>();
    private AreaSelectionAdapter mAdapter;
    private int choice_area_position;
    private int mActionFlow;


    @Override
    public void init() {
        initData();
        mActionFlow = getIntent().getExtras().getInt(Constants.CURRENT_PAGE_FLOW);
        if (mActionFlow == Constants.PAGE_FLOW_VITUALCARD_FORGETPASSWORD) {
            //忘记密码
            setToolBarTitle(R.string.IDV_3a_19_1);
        } else if (mActionFlow == Constants.PAGE_FLOW_VITUALCARD_REGISTER) {
            setToolBarTitle(R.string.IDV_2_1);
        }
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_info);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new AreaSelectionAdapter(this, mList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(new AreaSelectionAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final int position, final String area) {
                for (int i = 0; i < mList.size(); i++) {
                    if (i == position) {
                        mList.get(i).setSelection(true);
                    } else {
                        mList.get(i).setSelection(false);
                    }
                }
                mAdapter.notifyDataSetChanged();
                Intent intent = new Intent();
                intent.putExtra(Constants.CHOICE_AREA, area);
                intent.putExtra(Constants.CHOICE_AREA_POSITION, position);
                setResult(CHOICE_AREA_CODE, intent);
                IDCardSelectionActivity.this.finish();
            }
        });
    }

    private void initData() {
        mList.add(new AreaSelectionEntity(getString(R.string.VC04_03_4a), false));
        mList.add(new AreaSelectionEntity(getString(R.string.VC04_03_4b), false));
        mList.add(new AreaSelectionEntity(getString(R.string.VC04_03_4c), false));
        choice_area_position = getIntent().getIntExtra(Constants.CHOICE_AREA_POSITION, -1);
        for (int i = 0; i < mList.size(); i++) {
            if (i == choice_area_position) {
                mList.get(i).setSelection(true);
            } else {
                mList.get(i).setSelection(false);
            }
        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.activity_idcard_selection;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

}
