package cn.swiftpass.wallet.intl.module.setting.presenter;

import android.content.Intent;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.api.protocol.CheckFioProtocol;
import cn.swiftpass.wallet.intl.api.protocol.CheckPasswordProtocol;
import cn.swiftpass.wallet.intl.api.protocol.DeleteFioProtocol;
import cn.swiftpass.wallet.intl.api.protocol.FioAuthIdProtocol;
import cn.swiftpass.wallet.intl.base.fio.FIODispatcher;
import cn.swiftpass.wallet.intl.entity.FioCheckEntity;
import cn.swiftpass.wallet.intl.entity.FioCheckValidEntity;
import cn.swiftpass.wallet.intl.entity.GetTnxIdEntity;
import cn.swiftpass.wallet.intl.module.setting.contract.TouchIDContract;


public class TouchIDPresenter implements TouchIDContract.Presenter {


    private TouchIDContract.View baseView;

    @Override
    public void onFioResult(int requestCode, int resultCode, Intent data) {
        if (baseView != null) {
            FIODispatcher.requestCallBack(baseView, requestCode, resultCode, data);
        }
    }


    @Override
    public void verifyPwd(String action, String payPwd) {
        if (baseView != null) {
            baseView.showDialogNotCancel();
            new CheckPasswordProtocol(payPwd, action, -1, new NetWorkCallbackListener<BaseEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    if (baseView != null) {
                        baseView.dismissDialog();
                        baseView.verifyPwdFailed(errorCode, errorMsg);
                    }
                }

                @Override
                public void onSuccess(BaseEntity response) {
                    if (baseView != null) {
                        baseView.dismissDialog();
                        baseView.verifyPwdSuccess(response);
                    }
                }
            }).execute();
        }

    }


    @Override
    public void onFIOCheck() {
        baseView.showDialogNotCancel();
        new CheckFioProtocol(new NetWorkCallbackListener<FioCheckEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                baseView.dismissDialog();
                baseView.onFioCheckFail(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioCheckEntity response) {
                baseView.dismissDialog();
                baseView.onFioCheckSuccess(response);
            }
        }).execute();
    }

    @Override
    public void onFioCheckValid(String fioId) {
        baseView.showDialogNotCancel();
        new FioAuthIdProtocol(fioId, new NetWorkCallbackListener<FioCheckValidEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                baseView.dismissDialog();
                baseView.onFioCheckValidFail(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(FioCheckValidEntity response) {
                baseView.dismissDialog();
                baseView.onFioCheckValidSuccess(response);
            }
        }).execute();
    }


    @Override
    public void onFioDelete(String fioId) {
        new DeleteFioProtocol(fioId, new NetWorkCallbackListener<GetTnxIdEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {

            }

            @Override
            public void onSuccess(GetTnxIdEntity response) {

            }
        }).execute();
    }

    @Override
    public void onAttachView(TouchIDContract.View view) {
        this.baseView = view;
    }

    @Override
    public void onDetachView() {
        this.baseView = null;
    }
}
