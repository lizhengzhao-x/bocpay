package cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.base.adapter.item.AdapterItem;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.entity.SmartAccountBillListEntity;
import cn.swiftpass.wallet.intl.module.cardmanagement.CardItemCallback;
import cn.swiftpass.wallet.intl.module.transactionrecords.view.BocPayTransUtils;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;

import static cn.swiftpass.wallet.intl.module.transactionrecords.view.adapter.BillListAdapter.SUCCESS_INFO_;


public class SmartAccountBillListAdapter implements AdapterItem<SmartAccountBillListEntity.OutgroupBean> {

    private ImageView mId_merchant_head;
    private TextView mId_merchant_name;
    private TextView mId_merchant_money;
    private TextView mId_bill_time;
    private TextView mId_pay_symbol;
    private TextView mId_transac_status;
    private View mId_root_view;

    private int position;
    private Context mContext;
    private static final String PAYMENT_SUCCESS = "SUCCESS";

    private CardItemCallback mCallback;

    public SmartAccountBillListAdapter(Context mContextIn, CardItemCallback callback) {
        mCallback = callback;
        mContext = mContextIn;
    }

    @Override
    public int getLayoutResId() {
        return R.layout.item_bill_smart_view;
    }

    @Override
    public void bindViews(View root) {
        mId_merchant_head = root.findViewById(R.id.id_merchant_head);
        mId_merchant_name = root.findViewById(R.id.id_merchant_name);
        mId_merchant_money = root.findViewById(R.id.id_merchant_money);
        mId_bill_time = root.findViewById(R.id.id_bill_time);
        mId_pay_symbol = root.findViewById(R.id.id_pay_symbol);
        mId_root_view = root.findViewById(R.id.id_root_view);
        mId_transac_status = root.findViewById(R.id.id_discount);
    }

    @Override
    public void setViews() {
        mId_root_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCallback != null) {
                    mCallback.onItemCardClick(position);
                }
            }
        });

    }


    @Override
    public void handleData(SmartAccountBillListEntity.OutgroupBean cardItemEntity, int position) {
        this.position = position;
        String merchantName = "";
        //新UI 银联消费 名字要换掉
        if (TextUtils.equals(cardItemEntity.getPaymentType(), "1")) {
            merchantName = cardItemEntity.getMerchantName();
        } else {
            merchantName = cardItemEntity.getOuttxtype();
        }

        mId_merchant_name.setText(merchantName);
        mId_bill_time.setText(cardItemEntity.getOuttxdate());
        mId_pay_symbol.setText(cardItemEntity.getShowType() + AndroidUtils.getTrxCurrency(cardItemEntity.getOuttxccy()));
        try {
            String balanceStr = AndroidUtils.formatPriceWithPoint(Double.parseDouble(cardItemEntity.getOuttxamt()), true);
            mId_merchant_money.setText(" " + balanceStr);
        } catch (NumberFormatException e) {
        }
        mId_merchant_head.setImageResource(R.mipmap.img_icon_store_list);
        mId_transac_status.setVisibility(View.INVISIBLE);

        mId_pay_symbol.setTextColor(BocPayTransUtils.getColor(mContext, cardItemEntity.getShowType(), false));
        mId_merchant_money.setTextColor(BocPayTransUtils.getColor(mContext, cardItemEntity.getShowType(), false));
        mId_merchant_head.setImageResource(BocPayTransUtils.getImageResourceForSmart(cardItemEntity));

        if (TextUtils.equals(cardItemEntity.getPaymentType(), "6") || TextUtils.equals(cardItemEntity.getPaymentType(), "8") || TextUtils.equals(cardItemEntity.getPaymentType(), "10")) {
            //增值 首榜奖赏的时候 针对新的ui 标题要单独处理
            mId_merchant_name.setText(cardItemEntity.getTxTitle());
        }

        if (cardItemEntity.getOuttxsts().equals(SUCCESS_INFO_)) {

        } else {
            mId_transac_status.setVisibility(View.VISIBLE);
            mId_transac_status.setText(mContext.getString(R.string.UI209_1_15));
            mId_transac_status.setTextColor(Color.WHITE);
            mId_transac_status.setBackgroundColor(Color.parseColor("#AEB2B8"));
        }

    }
}
