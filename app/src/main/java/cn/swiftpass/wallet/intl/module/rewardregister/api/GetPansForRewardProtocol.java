package cn.swiftpass.wallet.intl.module.rewardregister.api;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;

/**
 * 信用卡消费奖赏 查询列表
 */
public class GetPansForRewardProtocol extends BaseProtocol {

    private String bankCardType;

    public GetPansForRewardProtocol(String bankCardType, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        mUrl = "api/card/getPansByUser";
        this.bankCardType = bankCardType;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.CARDTYPE, bankCardType);
    }
}
