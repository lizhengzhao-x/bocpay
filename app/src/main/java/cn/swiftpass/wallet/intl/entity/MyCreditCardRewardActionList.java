package cn.swiftpass.wallet.intl.entity;

import java.util.List;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class MyCreditCardRewardActionList extends BaseEntity {

    public static final String NOT_HAS_NEXT_PAGE = "0";

    private String currentPage;
    private String hasNext;
    private List<CreditCardRewardEntity> rows;

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getHasNext() {
        return hasNext;
    }

    public void setHasNext(String hasNext) {
        this.hasNext = hasNext;
    }

    public List<CreditCardRewardEntity> getRows() {
        return rows;
    }

    public void setRows(List<CreditCardRewardEntity> rows) {
        this.rows = rows;
    }

}
