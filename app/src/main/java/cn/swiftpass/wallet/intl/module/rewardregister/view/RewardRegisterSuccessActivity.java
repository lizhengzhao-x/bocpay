package cn.swiftpass.wallet.intl.module.rewardregister.view;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.event.MainEventEntity;
import cn.swiftpass.wallet.intl.utils.MyActivityManager;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

import static cn.swiftpass.wallet.intl.module.rewardregister.view.RewardRegisterWebviewActivity.REMARK;

/**
 * 信用卡消费奖赏登记成功界面
 */
public class RewardRegisterSuccessActivity extends BaseCompatActivity {
    @BindView(R.id.id_top_success_img)
    ImageView idTopSuccessImg;
    @BindView(R.id.id_reward_status)
    TextView idRewardStatus;
    @BindView(R.id.id_reward_number_title)
    TextView idRewardNumberTitle;
    @BindView(R.id.id_reward_number)
    TextView idRewardNumber;
    @BindView(R.id.id_reward_waining_title)
    TextView idRewardWainingTitle;
    @BindView(R.id.id_reward_waining)
    TextView idRewardWaining;
    @BindView(R.id.id_reward_back)
    TextView idTvOk;
    public static final String REWARDNUMBERINFO = "REWARDNUMBERINFO";

    @Override
    public void init() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            finish();
            return;
        }
        hideBackIcon();
        setToolBarTitle(R.string.CR209_11_1);

        idTvOk.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {

                EventBus.getDefault().postSticky(new MainEventEntity(MainEventEntity.EVENT_KEY_HOME, ""));
                MyActivityManager.removeAllTaskExcludeMainStack();
            }
        });
        String ewardNumber = (String) getIntent().getExtras().get(REWARDNUMBERINFO);
        String remark = (String) getIntent().getExtras().get(REMARK);
        idRewardWaining.setText(remark);
        idRewardNumber.setText(ewardNumber);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_rewardregister_success;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
