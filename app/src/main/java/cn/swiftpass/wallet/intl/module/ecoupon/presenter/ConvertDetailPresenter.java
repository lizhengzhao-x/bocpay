package cn.swiftpass.wallet.intl.module.ecoupon.presenter;

import cn.swiftpass.boc.commonui.base.mvp.BasePresenter;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.wallet.intl.module.ecoupon.api.ConVertDetailProtocol;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.ConvertUseContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.RedeemDetailEntity;

public class ConvertDetailPresenter extends BasePresenter<ConvertUseContract.View> implements ConvertUseContract.Presenter {


    @Override
    public void checkEcouponInfo(String giftCode, String orderType, String referenceNo) {

        if (getView() != null) {
            getView().showDialogNotCancel();
        }
        new ConVertDetailProtocol(giftCode, orderType, referenceNo, new NetWorkCallbackListener<RedeemDetailEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkEcouponInfoError(errorCode, errorMsg);
                }

            }

            @Override
            public void onSuccess(RedeemDetailEntity response) {
                if (getView() != null) {
                    getView().dismissDialog();
                    getView().checkEcouponInfoSuccess(response);
                }
            }
        }).execute();
    }
}
