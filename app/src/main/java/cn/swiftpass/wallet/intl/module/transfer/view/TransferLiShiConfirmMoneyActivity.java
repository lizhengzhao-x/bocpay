package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import butterknife.BindView;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.app.constants.TransferConst;
import cn.swiftpass.wallet.intl.entity.TransferPreCheckEntity;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.TransferResourcesEntity;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.GlideApp;
import cn.swiftpass.wallet.intl.utils.ScreenUtils;

/**
 * Created by ZhangXinchao on 2019/11/7.
 * 派利是界面 ui细微差别
 */
public class TransferLiShiConfirmMoneyActivity extends TransferConfirmMoneyActivity {
    @BindView(R.id.id_total_bac_view)
    LinearLayout totalBacView;
    @BindView(R.id.id_buttom_image)
    ImageView idButtomImage;

    @BindView(R.id.id_header_img)
    ImageView mHeaderImg;
    @BindView(R.id.iv_red_packet_bg)
    ImageView ivRedPacketBg;

    @BindView(R.id.id_header_shaper_img)
    ImageView mHeaderBacShapeView;


    @Override
    public void init() {
        super.init();
        setToolBarTitle(R.string.RP2101_11_1);
        //转账附言标题 内容差异化
        transferNext.setText(getString(R.string.TF2101_31_8));
        TransferResourcesEntity.DefBean.BckImgsBean bckImgsBean = mPreCheckEntity.getBckImgsBean();
        String readPackageImageUrl = AppTestManager.getInstance().getBaseUrl() + bckImgsBean.getNewRedPackageImageUrl();

        ivRedPacketBg.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                int radius = ScreenUtils.dip2px(10);
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), radius);
            }
        });
        ivRedPacketBg.setClipToOutline(true);

        GlideApp.with(mContext)
                .load(readPackageImageUrl)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        mHeaderImg.setVisibility(View.VISIBLE);
                        ivRedPacketBg.setImageDrawable(resource);
                    }
                });

        FrameLayout.LayoutParams fl = (FrameLayout.LayoutParams) mHeaderBacShapeView.getLayoutParams();
        int imageWidth = AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 24) * 2;
        int imageHeight = (int) (imageWidth / 3.5);
        fl.width = imageWidth;
        fl.height = imageHeight;
        mHeaderBacShapeView.setLayoutParams(fl);

        LinearLayout.LayoutParams flButtom = (LinearLayout.LayoutParams) idButtomImage.getLayoutParams();
        imageWidth = AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(mContext, 24) * 2;
        imageHeight = (int) (imageWidth / 2.86);
        fl.width = imageWidth;
        fl.height = imageHeight;
        idButtomImage.setLayoutParams(flButtom);


        mHeaderBacShapeView.setVisibility(View.VISIBLE);

        GlideApp.with(mContext)
                .load(AppTestManager.getInstance().getBaseUrl() + bckImgsBean.getBottomBigSizeImageUrl())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new RoundedCorners(5))
                .into(idButtomImage);
        GlideApp.with(mContext)
                .load(AppTestManager.getInstance().getBaseUrl() + bckImgsBean.getRedPackageNewImageTopUrl())
                .transform(new RoundedCorners(5))
                .into(mHeaderBacShapeView);
    }


    public static void startActivity(Activity activity, TransferPreCheckEntity entity) {
        Intent intent = new Intent(activity, TransferLiShiConfirmMoneyActivity.class);
        intent.putExtra(TransferConst.TRANS_FPS_CHECK_DATA, entity);
        activity.startActivity(intent);
    }


    @Override
    protected int getLayoutId() {
        return R.layout.act_transfer_amount_lishi_confirm;
    }
}
