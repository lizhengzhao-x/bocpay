package cn.swiftpass.wallet.intl.api.protocol;


import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.api.protocol.E2eePreProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.manage.AppTestManager;
import cn.swiftpass.wallet.intl.module.virtualcard.entity.VirtualcardRegisterVerifyEntity;

/**
 * Created by ZhangXinchao on 2019/11/18.
 */
public class VirtualcardSmartAccountRegisterProtocol extends BaseProtocol {
    VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntity;
    private String mSid;
    private String mPin;

    public VirtualcardSmartAccountRegisterProtocol(VirtualcardRegisterVerifyEntity virtualcardRegisterVerifyEntityIn, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        virtualcardRegisterVerifyEntity = virtualcardRegisterVerifyEntityIn;
        mUrl = "api/virtual/reg/checkCCAndLogin";
    }

    @Override
    public void packData() {
        super.packData();
//        accNo	String	C	手机号
//        password	String	C	身份证号
//        loginType	String	C	1：网银 2：ATM 3：信用卡
//        verifyCode	String	C
//        pan	String	C
//        expiryDate	String	C
//        cvv	String	C
//        eaiAction	String	C	“B”
//        nid	String	C
//        nidType	String	C
//        nidRgn	String	C
//        phoneNo	String	C	852-11111111
//        checkAct	String	C	//R:register  F:forget
//        action	String	M	注册流程：‘VIRREG ‘ 忘记密码流程 ‘VIRPWDSET
//        virtualCardInd	String	C	是否是虚拟卡：Y/N

        putIngoreNullParams(RequestParams.ACCNO, virtualcardRegisterVerifyEntity.getAccNo());
        putIngoreNullParams(RequestParams.PASSWORD, mPin);
        putIngoreNullParams(RequestParams.LOGINTYPE, virtualcardRegisterVerifyEntity.getLoginType());
        putIngoreNullParams(RequestParams.VERIFYCODE, virtualcardRegisterVerifyEntity.getVerifyCode());
        putIngoreNullParams(RequestParams.PAN_ID, virtualcardRegisterVerifyEntity.getPan());
        putIngoreNullParams(RequestParams.EXPIRYDATE, virtualcardRegisterVerifyEntity.getExpiryDate());

        putIngoreNullParams(RequestParams.CVV, virtualcardRegisterVerifyEntity.getCvv());
        putIngoreNullParams(RequestParams.EAIACTION, virtualcardRegisterVerifyEntity.getEaiAction());
        putIngoreNullParams(RequestParams.NID, virtualcardRegisterVerifyEntity.getNid());
        putIngoreNullParams(RequestParams.NIDTYPE, virtualcardRegisterVerifyEntity.getNidType());
        putIngoreNullParams(RequestParams.NIDRGN, virtualcardRegisterVerifyEntity.getNidRgn());
        putIngoreNullParams(RequestParams.PHONENO, virtualcardRegisterVerifyEntity.getPhoneNo());

        putIngoreNullParams(RequestParams.CHECKACT, virtualcardRegisterVerifyEntity.getCheckAct());
        putIngoreNullParams(RequestParams.ACTION, virtualcardRegisterVerifyEntity.getAction());
        putIngoreNullParams(RequestParams.VIRTUALCARDIND, virtualcardRegisterVerifyEntity.getVirtualCardInd());
    }

    @Override
    public Void execute() {
        if (AppTestManager.getInstance().isE2ee()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    mPin = E2eeUtils.getLoginOrSetPin(mSid, virtualcardRegisterVerifyEntity.getPassword(), pubKey, random);
                    if (TextUtils.isEmpty(mPin)) {
                        mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                    } else {
                        VirtualcardSmartAccountRegisterProtocol.super.execute();
                    }
                }
            }).execute();
        } else {
            super.execute();
        }
        return null;
    }


}
