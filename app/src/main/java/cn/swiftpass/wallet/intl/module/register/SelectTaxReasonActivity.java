package cn.swiftpass.wallet.intl.module.register;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.TaxInfoReason;
import cn.swiftpass.wallet.intl.module.register.adapter.OnClickTaxReasonListener;
import cn.swiftpass.wallet.intl.module.register.adapter.TaxReasonAdapter;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;

public class SelectTaxReasonActivity extends BaseCompatActivity implements OnClickTaxReasonListener {


    @BindView(R.id.ry_tax_reason)
    RecyclerView ryTaxReason;
    private int index;
    //传递过来的，已经选中的税务原因，可以为空
    private TaxInfoReason currentReason;
    private ArrayList<TaxInfoReason> taxReasonList;
    //当前UI操作中，选中的税务原因，如果为空或者等于currentReasonData 时，不能点击完成按钮
    private TaxInfoReason selectReasonData;
    private TextView doneTv;

    @Override
    protected int getLayoutId() {
        return R.layout.act_select_tax_reason;
    }

    @Override
    protected IPresenter createPresenter() {
        return null;
    }

    @Override
    protected void init() {
        if (getIntent() != null) {
            if (getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_REASON) != null) {
                currentReason = (TaxInfoReason) getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_REASON);
            }
            index = getIntent().getIntExtra(RegisterTaxListActivity.DATA_TAX_INDEX, -1);
            if (getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_TAX_REASON_LIST) != null) {
                taxReasonList = (ArrayList<TaxInfoReason>) getIntent().getSerializableExtra(RegisterTaxListActivity.DATA_TAX_REASON_LIST);
            }
        }
        setToolBarTitle(R.string.PA2109_1_14);
        doneTv = setToolBarRightViewToText(R.string.btn_finish);
        getToolBarRightView().setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                Intent intent = new Intent();
                intent.putExtra(RegisterTaxListActivity.DATA_CURRENT_TAX_REASON, selectReasonData);
                intent.putExtra(RegisterTaxListActivity.DATA_TAX_INDEX, index);
                setResult(RESULT_OK, intent);
                finish();

            }
        });

        LinearLayoutManager layout = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        ryTaxReason.setLayoutManager(layout);
        TaxReasonAdapter adapter = new TaxReasonAdapter(this, taxReasonList, this);
        if (currentReason != null) {
            adapter.setCurrentReason(currentReason);
        }
        ryTaxReason.setAdapter(adapter);
        getToolBarRightView().setEnabled(false);
        doneTv.setTextColor(getColor(R.color.gray_two));
    }


    private void updateBtn(TaxInfoReason selectReason) {
        if (currentReason != null && currentReason.getNoResidentReasonCode().equals(selectReason.getNoResidentReasonCode())) {
            //选中了和进来时相同的条目
            getToolBarRightView().setEnabled(false);
            doneTv.setTextColor(getColor(R.color.gray_two));
        } else {
            getToolBarRightView().setEnabled(true);
            doneTv.setTextColor(getColor(R.color.app_white));
        }

    }


    @Override
    public void OnClickTaxReason(TaxInfoReason reasonInfo) {
        this.selectReasonData = reasonInfo;
        updateBtn(reasonInfo);
    }
}