package cn.swiftpass.wallet.intl.module.crossbordertransfer.entity;


import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * @name bocpay-aos
 * @class name：cn.swiftpass.wallet.intl.module.transfer.entity
 * @class describe
 * @anthor zhangfan
 * @time 2019/11/14 20:20
 * @change
 * @chang time
 * @class describe
 */
public class TransferCrossBorderCardEntity extends BaseEntity {
    public String cardNo;
    public String bankName;
    public String cardType;
}
