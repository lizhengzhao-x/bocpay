package cn.swiftpass.wallet.intl.api.protocol;

import com.alibaba.fastjson.JSONArray;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;


public class SendActionDataProtocol extends BaseProtocol {
    public static final String TAG = SendActionDataProtocol.class.getSimpleName();
    JSONArray model;

    @Override
    public boolean isNeedLogin() {
        return false;
    }

    public SendActionDataProtocol(JSONArray model, NetWorkCallbackListener<String> dataCallback) {
        this.model = model;
        this.mDataCallback = dataCallback;
        mUrl = "logTrace/actionLogUpload";
        mEncryptFlag = false;
        mRequestHeader = false;
        useCustomizeBodyKey = true;
    }

    @Override
    public boolean ignoreInterceptor() {
        return true;
    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.DATA, model);
    }
}
