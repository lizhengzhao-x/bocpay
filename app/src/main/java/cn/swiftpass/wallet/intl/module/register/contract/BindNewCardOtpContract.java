package cn.swiftpass.wallet.intl.module.register.contract;

import java.util.ArrayList;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.BaseEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardEntity;
import cn.swiftpass.wallet.intl.module.register.model.BindNewCardResultEntity;

/**
 * 发送验证码
 */
public class BindNewCardOtpContract {

    public interface View extends IView {

        void bindNewCardSendOtpSuccess(BaseEntity response, boolean isTryAgain);

        void bindNewCardSendOtpError(String errorCode, String errorMsg, boolean isTryAgain);

        void bindNewCardVerifyOtpSuccess(BaseEntity response);

        void bindNewCardVerifyOtpError(String errorCode, String errorMsg);

        void bindNewCardListError(String errorCode, String errorMsg);

        void bindNewCardListSuccess(BindNewCardResultEntity response);
    }


    public interface Presenter extends IPresenter<BindNewCardOtpContract.View> {

        void bindNewCardSendOtp(ArrayList<BindNewCardEntity> newCardEntities, String action, boolean isTryAgain);

        void bindNewCardVerifyOtp(String action, String verifyCode);

        void bindNewCardList(ArrayList<BindNewCardEntity> selectCardList, String otpBindNewCard);
    }

}
