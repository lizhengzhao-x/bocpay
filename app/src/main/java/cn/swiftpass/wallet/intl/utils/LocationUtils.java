package cn.swiftpass.wallet.intl.utils;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.provider.Settings;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import cn.swiftpass.wallet.intl.BuildConfig;

/**
 * 定位工具类
 * Created by xiaoyehai on 2018/8/22 0022.
 */

public class LocationUtils {


    //China/中国/中國
    public static final String COUNTRY_CHINA_EN = "China";
    public static final String COUNTRY_CHINA_CH = "中国";
    public static final String COUNTRY_CHINA_HK = "中國";
    public static final String TAG = "LocationUtils";
    private static OnLocationChangeListener mListener;

    private static MyLocationListener myLocationListener;

    private static LocationManager mLocationManager;

    private LocationUtils() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 判断Gps是否可用
     *
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public static boolean isGpsEnabled(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    /**
     * 判断定位是否可用
     *
     * @return {@code true}: 是<br>{@code false}: 否
     */
    public static boolean isLocationEnabled(Context context) {
        Object locationService = context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationService != null) {
            LocationManager lm = (LocationManager) locationService;
            return lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER) || lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        }
        return false;
    }

    /**
     * 打开Gps设置界面
     */
    public static void openGpsSettings(Context context) {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void registerLocationWifi(Context context, long minTime, long minDistance, OnLocationChangeListener listener) {
        if (listener == null) return;
        Object locationService = context.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        if (locationService != null) {
            mLocationManager = (LocationManager) locationService;
        }
        if (mLocationManager == null) return;
        mListener = listener;
        //判断当前GPS/network provider是否可用 如果都不可用需要引导用户打开定位权限
        if (isLocationEnabled(context)) {
            try {
                //先拿网络 再根据GPS
                String provider = null;
                LocationProvider locationProvider = mLocationManager.getProvider(LocationManager.NETWORK_PROVIDER);
                if (locationProvider == null) {
                    provider = mLocationManager.getProvider(LocationManager.GPS_PROVIDER).getName();
                } else {
                    provider = locationProvider.getName();
                }
                Location location = mLocationManager.getLastKnownLocation(provider);
                if (location != null) {
                    listener.getLastKnownLocation(location);
                }
                if (myLocationListener == null) {
                    myLocationListener = new MyLocationListener();
                }
                LogUtils.i(TAG, "start location....");
                if (mLocationManager != null) {
                    mLocationManager.requestLocationUpdates(provider, minTime, minDistance, myLocationListener);
                }
                return;
            } catch (Exception e) {
                if (BuildConfig.isLogDebug) {
                    e.printStackTrace();
                }
            }
        } else {
            return;
        }
    }


    /**
     * 注销
     */
    public static void unregister() {
        if (mLocationManager != null) {
            if (myLocationListener != null) {
                mLocationManager.removeUpdates(myLocationListener);
                myLocationListener = null;
            }
            mLocationManager = null;
        }
    }

    /**
     * 设置定位参数
     *
     * @return {@link Criteria}
     */
    private static Criteria getCriteria() {
        Criteria criteria = new Criteria();
        //设置定位精确度 Criteria.ACCURACY_COARSE比较粗略，Criteria.ACCURACY_FINE则比较精细
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        //设置是否要求速度
        criteria.setSpeedRequired(false);
        // 设置是否允许运营商收费
        criteria.setCostAllowed(false);
        //设置是否需要方位信息
        criteria.setBearingRequired(false);
        //设置是否需要海拔信息
        criteria.setAltitudeRequired(false);
        // 设置对电源的需求
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        return criteria;
    }

    /**
     * 根据经纬度获取地理位置
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return {@link Address}
     */
    public static Address getAddress(Context context, double latitude, double longitude) {


        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
//        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
//        Geocoder geocoder = new Geocoder(context, Locale.SIMPLIFIED_CHINESE);
//        Geocoder geocoder = new Geocoder(context, Locale.TRADITIONAL_CHINESE);
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0)
                return addresses.get(0);
        } catch (IOException e) {
            if (BuildConfig.isLogDebug) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * 根据经纬度获取所在国家 需要注意一下 这个需要外网才可以成功 China/中国/中國
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在国家
     */
    public static String getCountryName(Context context, double latitude, double longitude) {
        Address address = getAddress(context, latitude, longitude);
//        LogUtils.i(TAG,"address:"+address.toString());
        return address == null ? "" : address.getCountryName();
    }

    /**
     * 根据经纬度获取所在地
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在地
     */
    public static String getLocality(Context context, double latitude, double longitude) {
        Address address = getAddress(context, latitude, longitude);
        return address == null ? "unknown" : address.getLocality();
    }

    /**
     * 根据经纬度获取所在街道
     *
     * @param context   上下文
     * @param latitude  纬度
     * @param longitude 经度
     * @return 所在街道
     */
    public static String getStreet(Context context, double latitude, double longitude) {
        Address address = getAddress(context, latitude, longitude);
        return address == null ? "unknown" : address.getAddressLine(0);
    }

    private static class MyLocationListener implements LocationListener {
        /**
         * 当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
         *
         * @param location 坐标
         */
        @Override
        public void onLocationChanged(Location location) {
            LogUtils.i(TAG, "onLocationChanged+" + location);
            if (mListener != null) {
                mListener.onLocationChanged(location);
            }
        }

        /**
         * provider的在可用、暂时不可用和无服务三个状态直接切换时触发此函数
         *
         * @param provider 提供者
         * @param status   状态
         * @param extras   provider可选包
         */
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (mListener != null) {
                mListener.onStatusChanged(provider, status, extras);
            }
            switch (status) {
                case LocationProvider.AVAILABLE:
                    LogUtils.i(TAG, "当前GPS状态为可见状态");
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    LogUtils.i(TAG, "当前GPS状态为服务区外状态");
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    LogUtils.i(TAG, "当前GPS状态为暂停服务状态");
                    break;
            }
        }

        /**
         * provider被enable时触发此函数，比如GPS被打开
         */
        @Override
        public void onProviderEnabled(String provider) {
            LogUtils.i(TAG, "onProviderEnabled+" + provider);
        }

        /**
         * provider被disable时触发此函数，比如GPS被关闭
         */
        @Override
        public void onProviderDisabled(String provider) {
            if (mListener != null) {
                mListener.onLocationClose();
            }
            LogUtils.i(TAG, "onProviderDisabled+" + provider);

        }
    }

    public interface OnLocationChangeListener {

        /**
         * 获取最后一次保留的坐标
         *
         * @param location 坐标
         */
        void getLastKnownLocation(Location location);

        /**
         * 当坐标改变时触发此函数，如果Provider传进相同的坐标，它就不会被触发
         *
         * @param location 坐标
         */
        void onLocationChanged(Location location);

        void onLocationClose();

        /**
         * provider的在可用、暂时不可用和无服务三个状态直接切换时触发此函数
         *
         * @param provider 提供者
         * @param status   状态
         * @param extras   provider可选包
         */
        void onStatusChanged(String provider, int status, Bundle extras);//位置状态发生改变
    }


//    static class ParseLocationTask extends AsyncTask<Location, Void, String> {
//
//
//        private String mLatitude;
//        private String mLongitude;
//        public ParseLocationTask(double latitude, double longitude) {
//            this.mLatitude = String.valueOf(latitude);
//            this.mLongitude = String.valueOf(longitude);
//        }
//
//        //二维码解析回调
//        public interface OnParseResultListener {
//            void onResult(String res);
//        }
//
//        //回调
//        private OnParseResultListener mListener;
//
//
//
////        public ParseLocationTask(String path, OnParseResultListener listener) {
////            mPath = path;
////            mListener = listener;
////        }
//
//
//        @Override
//        protected String doInBackground(Location... params) {
//            //处理二维码 去谷歌的地理位置获取中去解析经纬度对应的地理位置
//            StringBuilder url = new StringBuilder();
//            url.append("http://maps.googleapis.com/maps/api/geocode/json?latlng=");
//            url.append(mLatitude).append(",");
//            url.append(mLongitude);
//            url.append("&sensor=false");
//            try {
//                OkHttpClient client = new OkHttpClient.Builder().build();
//                Request.Builder builder = new Request.Builder().get();
//                Request request = builder.get().url(url.toString()).build();
//                Response response = client.newCall(request).execute();
//                if (response.code() == 200) {
//                    JSONObject jsonObject = new JSONObject(response.body().string());
//                    JSONObject locationObject = jsonObject.getJSONArray("results").getJSONObject(0).getJSONObject("formatted_address");
//                    LogUtils.i(TAG,"locationObject:"+locationObject);
//                }
//            } catch (Exception e) {
//                if (BuildConfig.DEBUG) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        }
//
//        @Override
//        protected void onPostExecute(final String res) {
//            if (null != mListener) {
//                mListener.onResult(res);
//            }
//        }
//    }

}

