package cn.swiftpass.wallet.intl.module.transfer.view;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.RelativeLayout;

import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.LogUtils;
import cn.swiftpass.wallet.intl.widget.HeightProvider;

/**
 * scrollview 使用圆角背景图片 如果软键盘弹出
 * activity windowSoftInputMode adjustResize 圆角效果会被挤压
 * 需要设置成    adjustNothing 然后activity的父布局通过PopupWindow 来获取软键盘的高度 然后根据focus view的位置进行平移 平移内容为scrollview的根布局linearlayout 只能这样
 * 具体使用：
 * mSoftKeyboardTranslationView = (SoftKeyboardTranslationView) findViewById(R.id.id_softkeyboard_view);
 * mSoftKeyboardTranslationView.initPopView(this,findViewById(R.id.ll_content));
 */
public class SoftKeyboardTranslationView extends RelativeLayout implements ViewTreeObserver.OnGlobalFocusChangeListener {
    private static final String TAG = "ResizeFrameLayout";
    private ResizeFrameLayout.KeyboardListener mListener;
    private int minKeyboardHeight;
    private View mTranslationYView;
    private int screenHeight;
    private View currentFocusView;
    private int touchY, offetY, lastTranslationY, softKeyBoardHeight;
    private Activity mActivity;


    public interface KeyboardListener {
        void onKeyboardShown(int height);

        void onKeyboardHidden(int height);
    }

    public SoftKeyboardTranslationView(Context context) {
        super(context);
        init();
    }

    public SoftKeyboardTranslationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SoftKeyboardTranslationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        screenHeight = AndroidUtils.getScreenHeight(getContext());
        offetY = AndroidUtils.dip2px(getContext(), 60);
        //全局监听activity中焦点变化
        getViewTreeObserver().addOnGlobalFocusChangeListener(this);
    }

    public void setKeyboardListener(ResizeFrameLayout.KeyboardListener listener) {
        mListener = listener;
    }

    public void setTranslationYView(View view) {
        mTranslationYView = view;
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        LogUtils.i(TAG, "dispatchTouchEvent:" + ev.getAction());
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:

                //如果打开下面隐藏的注释，在mate30 手机，转账界面会导致点击x图标无法清除输入的内容
                //软键盘弹出 用户手势滑动的时候 隐藏软键盘
//                AndroidUtils.hideKeyboard(currentFocusView);
                break;

            default:
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    public void initPopView(final Activity activity, View translationYView) {
        if (activity == null || translationYView == null) return;
        mActivity = activity;
        mTranslationYView = translationYView;
        new HeightProvider(mActivity).init().setHeightListener(new HeightProvider.HeightListener() {
            @Override
            public void onHeightChanged(int height) {
                softKeyBoardHeight = height;
                if (height == 0) {
                    lastTranslationY = -height;
                } else {
                    //currentFocusView = mActivity.getWindow().getDecorView().findFocus();
                    reCalculateTranslationY();
                }
                translationYView();
            }
        });

    }

    private void reCalculateTranslationY() {
        if (currentFocusView == null) return;
        int[] location = new int[2];
        currentFocusView.getLocationOnScreen(location);
        touchY = screenHeight - location[1];
        if (touchY < (softKeyBoardHeight + offetY)) {
            lastTranslationY = -(softKeyBoardHeight + offetY - touchY - lastTranslationY);
        }
    }

    private void translationYView() {
        LogUtils.i(TAG, "lastTranslationY:" + lastTranslationY);
        mTranslationYView.setTranslationY(lastTranslationY);
    }

    @Override
    public void onGlobalFocusChanged(View oldFocus, View newFocus) {
        LogUtils.i(TAG, "oldFocus:" + oldFocus + " newFocus:" + newFocus);
        currentFocusView = newFocus;
        //当前edittext焦点 用户可能点击键盘的下一步 这个时候 软键盘依然在 需要重新平移
        if (newFocus instanceof EditText && lastTranslationY != 0) {
//            reCalculateTranslationY();
//            translationYView();
        }

    }
}
