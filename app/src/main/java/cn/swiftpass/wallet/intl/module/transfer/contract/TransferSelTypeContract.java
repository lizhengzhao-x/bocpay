package cn.swiftpass.wallet.intl.module.transfer.contract;

import java.util.List;

import cn.swiftpass.boc.commonui.base.mvp.IPresenter;
import cn.swiftpass.boc.commonui.base.mvp.IView;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.wallet.intl.entity.ContactEntity;
import cn.swiftpass.wallet.intl.entity.ContractListEntity;
import cn.swiftpass.wallet.intl.entity.GetRegEddaInfoEntity;

/**
 * 转账选择
 */
public class TransferSelTypeContract {

    public interface View extends IView {

        void getSmartAccountInfoSuccess(MySmartAccountEntity response);

        void getSmartAccountInfoError(String errorCode, String errorMsg);

        void getRegEddaInfoSuccess(GetRegEddaInfoEntity response);

        void getRegEddaInfoError(String errorCode, String errorMsg);

    }


    public interface Presenter extends IPresenter<TransferSelTypeContract.View> {


        void getSmartAccountInfo();

        void getRegEddaInfo();


        /**
         * 数据处理
         *
         * @param recentlyBeans
         * @param contactEntities
         * @param frequentCcontacts
         */
        List<ContractListEntity.RecentlyBean> dealWithAllList(List<ContractListEntity.RecentlyBean> recentlyBeans, List<ContactEntity> contactEntities, List<ContactEntity> frequentCcontacts);


    }

}
