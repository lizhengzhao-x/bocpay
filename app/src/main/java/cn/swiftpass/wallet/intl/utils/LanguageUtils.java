package cn.swiftpass.wallet.intl.utils;


import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;

import java.util.Locale;

import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;

public class LanguageUtils {

    public static Locale getCurrentLocale(Context mContext) {
        String currentLag = SpUtils.getInstance(mContext).getAppLanguage();
        if (!AndroidUtils.isEmptyOrNull(currentLag)) {
            HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, currentLag);
        }
        if (AndroidUtils.isHKLanguage(currentLag)) {
            // 繁体
            return Locale.TRADITIONAL_CHINESE;
        } else if (AndroidUtils.isEGLanguage(currentLag)) {
            return Locale.ENGLISH;
        } else if (AndroidUtils.isZHLanguage(currentLag)) {
            return Locale.SIMPLIFIED_CHINESE;
        } else {
            // 跟随系统//默认英文
            return Locale.getDefault();
        }


    }
    public static void updatelocalSpLanguage(Locale locale){
        String lan = locale.getLanguage() + "-" + locale.getCountry();
        if (AndroidUtils.isZHLanguage(lan)) {
        } else if (AndroidUtils.isHKLanguage(lan)) {
        } else {
            lan = HttpCoreConstants.LANG_CODE_EN_US;
        }
        HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, lan);
    }

    public static void setDefaultLanguage(Locale locale) {
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList.setDefault(new LocaleList(locale));
        }
    }

    public static Context wrapContextCompat(Context context, Locale locale) {
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            LocaleList localeList = new LocaleList(locale);
            configuration.setLocales(localeList);
        } else {
            configuration.setLocale(locale);
        }
        return context.createConfigurationContext(configuration);
    }
}

