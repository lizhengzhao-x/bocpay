package cn.swiftpass.wallet.intl.api.protocol;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.wallet.intl.app.constants.RequestParams;
import cn.swiftpass.wallet.intl.module.register.contract.OtpAction;

public class VerifyOtpBaseProtocol extends BaseProtocol {
    String otpNo;
    String mSmartAccLevel;

    public VerifyOtpBaseProtocol(int action, String otpNo, String smartAccLevel, NetWorkCallbackListener dataCallback) {
        this.mDataCallback = dataCallback;
        this.otpNo = otpNo;
        this.mSmartAccLevel = smartAccLevel;
        switch (action) {
            case OtpAction.OTP_CREDIT_BIND_SMART_ACCOUNT:
                mUrl = "api/smartBind/verifyOtp";
                break;
            case OtpAction.OTP_SMART_ACCOUNT_TWO_REGISTER:
                mUrl = "api/smartReg/verifyOtp";
                break;
            default:
                break;
        }

    }

    @Override
    public void packData() {
        super.packData();
        mBodyParams.put(RequestParams.VERIFY, otpNo);
        mBodyParams.put(RequestParams.SMARTACCLEVEL, mSmartAccLevel);
    }
}
