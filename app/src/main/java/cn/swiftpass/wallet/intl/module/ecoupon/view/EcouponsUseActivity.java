package cn.swiftpass.wallet.intl.module.ecoupon.view;


import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.wallet.intl.R;
import cn.swiftpass.wallet.intl.base.BaseCompatActivity;
import cn.swiftpass.wallet.intl.entity.Constants;
import cn.swiftpass.wallet.intl.module.ecoupon.contract.EcouponsUseContract;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EVoucherStatusEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.EcouponsDetailEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.entity.MyEVoucherEntity;
import cn.swiftpass.wallet.intl.module.ecoupon.presenter.UseEcouponsPresenter;
import cn.swiftpass.wallet.intl.module.ecoupon.view.adapter.EcouponUseAdapter;
import cn.swiftpass.wallet.intl.sdk.verification.OnPwdVerifyCallBack;
import cn.swiftpass.wallet.intl.sdk.verification.VerifyPasswordCommonActivity;
import cn.swiftpass.wallet.intl.utils.ActivitySkipUtil;
import cn.swiftpass.wallet.intl.utils.AndroidUtils;
import cn.swiftpass.wallet.intl.utils.OnProhibitFastClickListener;
import cn.swiftpass.wallet.intl.widget.ScalableCardHelper;
import cn.swiftpass.wallet.intl.widget.TouchRecyclerView;


/**
 * 使用电子券
 */
public class EcouponsUseActivity extends BaseCompatActivity<EcouponsUseContract.Presenter> implements EcouponsUseContract.View, ScalableCardHelper.OnPageChangeListener {
    private static final long CURRENT_DELAT_TIME = 5000;
    //    private static final long CURRENT_QEQUERY_QRCODE = 60000 * 5;
    private static final long CURRENT_QEQUERY_QRCODE = 55000;
    private static final int UPDATE_QERUERY_QRCODE = 100;
    @BindView(R.id.id_ecoupon_desc)
    TextView idEcouponDesc;
    @BindView(R.id.id_terms_condition)
    TextView idTermCondition;
    @BindView(R.id.id_viewpager)
    TouchRecyclerView mRecyclerView;
    @BindView(R.id.tv_payment)
    TextView tvPayment;
    @BindView(R.id.id_item_ecoupon_cotainer)
    LinearLayout idItemEcouponCotainer;
    @BindView(R.id.tv_ecoupon_use_flag)
    TextView tvEcouponUseFlag;
    private List<MyEVoucherEntity.EvoucherItem.EVoucherInfosBean> eVoucherInfosBeans;
    private int viewPagerWidth;
    /**
     * 顶部 不同金额的选择位置
     */
    private int currentPosition;
    /**
     * 给予当前所选金额 选择不同位置
     */
    private int currentChildPosition;
    private EcouponsDetailEntity currentEcouponsDetailEntity;
    /**
     * 轮训电子券 二维码状态
     */
    private List<String> ecouponReferenceNos;
    /**
     * 当前电子券类型
     */
    private String buNumber, currentMoney;
    private List<MyEVoucherEntity.EvoucherItem> mEvoucherItems;
    private EcouponUseAdapter ecouponUseAdapter;
    private Handler mQueryQrStatusHandler;
    private ScalableCardHelper cardHelper;

    @Override
    public void init() {
        setToolBarTitle(getString(R.string.EC13_1));
        ecouponReferenceNos = new ArrayList<>();
        eVoucherInfosBeans = new ArrayList<>();
        mQueryQrStatusHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == UPDATE_QERUERY_QRCODE) {
                    String referenceNo = (String) msg.obj;
                    //当前二维码5分钟之后更新二维码信息
                    mPresenter.geteVoucherDetails(referenceNo, true);
                } else {
                    //轮训插叙状态
                    mPresenter.redeemeVouchersStatus(ecouponReferenceNos);
                }
            }
        };
        if (getIntent() != null && getIntent().getExtras() != null) {
            buNumber = getIntent().getExtras().getString(Constants.BU_NUMBER);
            currentMoney = getIntent().getExtras().getString(Constants.CURRAMOUNT);
        }
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
        viewPagerWidth = (int) (((AndroidUtils.getScreenWidth(mContext) - AndroidUtils.dip2px(this, 20))));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        cardHelper = new ScalableCardHelper(this);
        cardHelper.attachToRecyclerView(mRecyclerView);
        currentChildPosition = 0;
        tvPayment.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                ActivitySkipUtil.startAnotherActivity(EcouponsUseActivity.this, EcoupUseSuccessActivity.class, ActivitySkipUtil.ANIM_TYPE.RIGHT_IN);
                finish();
            }
        });
        mPresenter.getMyEcouponsList(buNumber, "true");
        initTextMsg();
    }

    private void initTextMsg() {
        idTermCondition.getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
        idTermCondition.getPaint().setAntiAlias(true);
        idTermCondition.setOnClickListener(new OnProhibitFastClickListener() {
            @Override
            public void onFilterClick(View v) {
                showEcouponCountDetail();
            }
        });
    }

    private void showEcouponCountDetail() {
        if (this.currentEcouponsDetailEntity != null && !TextUtils.isEmpty(currentEcouponsDetailEntity.getItemTitle()) && !TextUtils.isEmpty(currentEcouponsDetailEntity.getItemDetail())) {
            CheckEcouponsDetailPop checkEcouponsDetailPop = new CheckEcouponsDetailPop(getActivity(), currentEcouponsDetailEntity.getItemTitle(), currentEcouponsDetailEntity.getItemDetail());
            checkEcouponsDetailPop.show();
        }
    }


    private void initChildItems() {
        idItemEcouponCotainer.removeAllViews();
        int totalSize = mEvoucherItems.size();
        //每个item的宽度在xml文件中写死了默认是85dp
        int defaultWidth = AndroidUtils.dip2px(this, 80);
        int itemMargin = AndroidUtils.dip2px(this, 5 * 2);
        int totalUseWidth = AndroidUtils.getScreenWidth(this) - AndroidUtils.dip2px(this, 10 * 2);
        int tempSize = totalSize;
        while ((defaultWidth * tempSize + itemMargin * (tempSize - 1) > totalUseWidth)) {
            tempSize--;
        }
        if (totalSize > tempSize) {
            //控件整体宽度超过可用宽度 整体横向需要支持滑动 通过改变间距来实现
            //大概最多可以显示多少个条目
            itemMargin = (totalUseWidth - (defaultWidth * (tempSize - 1)) - defaultWidth * 2 / 3) / (tempSize - 1);
        }
        for (int i = 0; i < mEvoucherItems.size(); i++) {
            VouchersChildView vouchersChildView = new VouchersChildView(this);
            vouchersChildView.initText(mEvoucherItems.get(i).getCurrency() + mEvoucherItems.get(i).getCurrAmount(), mEvoucherItems.get(i).getEVoucherInfos().size() + getString(R.string.EC05c_1));
            if (!TextUtils.isEmpty(currentMoney)) {
                boolean isSel = Double.valueOf(currentMoney).doubleValue() == Double.valueOf(mEvoucherItems.get(i).getCurrAmount()).doubleValue();
                if (isSel) {
                    currentPosition = i;
                }
                vouchersChildView.setSel(isSel);
            }
            vouchersChildView.setTag(i);
            vouchersChildView.setOnClickListener(new OnProhibitFastClickListener() {
                @Override
                public void onFilterClick(View v) {
                    int changePosition = (int) v.getTag();

                    //更新当前选中的金额
                    currentMoney = mEvoucherItems.get(changePosition).getCurrAmount();
                    //重新拉取二维码列表
                    mPresenter.getMyEcouponsList(buNumber, null);
                }
            });
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (i != 0) {
                params.leftMargin = itemMargin;
            }
            params.gravity = Gravity.CENTER_VERTICAL;
            idItemEcouponCotainer.addView(vouchersChildView, params);
        }
    }

    private void updateItemStatus(int position) {
        for (int i = 0; i < idItemEcouponCotainer.getChildCount(); i++) {
            VouchersChildView vouchersChildView = (VouchersChildView) idItemEcouponCotainer.getChildAt(i);
            vouchersChildView.setSel(position == i);
        }
    }

    private void initViewPager() {
        eVoucherInfosBeans.clear();
        eVoucherInfosBeans.addAll(mEvoucherItems.get(currentPosition).getEVoucherInfos());
        cardHelper.setCardCount(eVoucherInfosBeans.size());
        //首次显示
        cardHelper.setArrowShow(true);

        //如客戶只持有一張電子禮券，不顯示操作指示
        if (eVoucherInfosBeans.size() <= 1) {
            tvEcouponUseFlag.setVisibility(View.GONE);
        } else {
            tvEcouponUseFlag.setVisibility(View.VISIBLE);
        }

        ecouponUseAdapter = new EcouponUseAdapter(this, eVoucherInfosBeans, viewPagerWidth, new EcouponUseAdapter.OnQrCodeRetryListener() {
            /**
             * 二维码详情拉取失败 点击重试
             *
             * @param position
             */
            @Override
            public void OnQrCodeRetry(int position) {
                MyEVoucherEntity.EvoucherItem.EVoucherInfosBean evoucherItem = eVoucherInfosBeans.get(position);
                mPresenter.geteVoucherDetails(evoucherItem.getReferenceNo(), true);
            }
        }, new EcouponUseAdapter.OnArrowClickListener() {

            /**
             * 点击左箭头
             *
             * @param toLeftPosition
             */
            @Override
            public void onLeftArrowClick(int toLeftPosition) {
                mRecyclerView.smoothScrollToPosition(toLeftPosition);
            }

            /**
             * 点击右箭头
             *
             * @param toRightPosition
             */
            @Override
            public void onRightArrowClick(int toRightPosition) {
                mRecyclerView.smoothScrollToPosition(toRightPosition);
            }
        });
        mRecyclerView.setAdapter(ecouponUseAdapter);
    }


    /**
     * 左右滑动切换二维码 更改底部词条 更新二维码状态 轮训二维码状态
     *
     * @param position
     * @param refreshParentViewCount
     * @param isQueryDetail
     * @param showLoading            页面加载第一步 拉取二维码列表 然后自动拉取详情 这个只需要显示一次弹框
     */
    private void changeTitle(int position, boolean refreshParentViewCount, boolean isQueryDetail, boolean showLoading) {
        //左右切换二维码的时候 清除掉所有轮询
        mQueryQrStatusHandler.removeCallbacksAndMessages(null);
        if (refreshParentViewCount) {
            //金额类别切换 刷新底部的滚动个数 二维码详情重新获取 并滚动到第一个位置
            //eVoucherInfosBeans.clear();
            for (int i = 0; i < eVoucherInfosBeans.size(); i++) {
                eVoucherInfosBeans.get(i).setEcouponsDetail(null);
            }
            ecouponUseAdapter.notifyDataSetChanged();
            mRecyclerView.smoothScrollToPosition(0);
            currentChildPosition = 0;
        }
        if (isQueryDetail) {
            //不管当前二维码存在不存在 都需要拉去详情界面 如果存在只是更新
            if (eVoucherInfosBeans.size() > currentChildPosition && eVoucherInfosBeans.get(currentChildPosition).getEcouponsDetail() != null && eVoucherInfosBeans.get(currentChildPosition).getEcouponsDetail().isUsed()) {
            } else {
                mPresenter.geteVoucherDetails(eVoucherInfosBeans.get(currentChildPosition).getReferenceNo(), showLoading);
            }
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.act_ecoupon_use;
    }

    @Override
    protected EcouponsUseContract.Presenter createPresenter() {
        return new UseEcouponsPresenter();
    }


    /**
     * OnQrCodeRetry
     * 二维码详情拉取成功之后 1.判断当前referenceNo是否存在 如果存在 直接更新二维码状态 如果不存在添加集合
     * 2.每个二维码详情查询成功之后 根据status判断是否需要开始轮询插曲结果/是否需要5分钟之后更新
     *
     * @param response
     */
    @Override
    public void geteVoucherDetailsSuccess(EcouponsDetailEntity response) {
        //拉取二维码详情之后 判断当前详情列表list中是否存在 如果不存在 添加进去 并开始轮训二维码状态 否则 根据二维码状态判断是否需要轮训
        for (int i = 0; i < eVoucherInfosBeans.size(); i++) {
            if (eVoucherInfosBeans.get(i).getReferenceNo().equals(response.getReferenceNo())) {
                //更新二维码信息
                eVoucherInfosBeans.get(i).setEcouponsDetail(response);
                eVoucherInfosBeans.get(i).setGetDetailSuccess(true);
                break;
            }
        }
        this.currentEcouponsDetailEntity = response;
        if (currentEcouponsDetailEntity.getStatus().equals("32")) {
            //二维码状态未使用的时候 做更新
            ecouponReferenceNos.clear();
            ecouponReferenceNos.add(currentEcouponsDetailEntity.getReferenceNo());
            //轮训插叙状态
            mQueryQrStatusHandler.removeCallbacksAndMessages(null);
            mQueryQrStatusHandler.sendEmptyMessageDelayed(-1, CURRENT_DELAT_TIME);
            //五分钟之后更新二维码信息
            Message message = Message.obtain();
            message.what = UPDATE_QERUERY_QRCODE;
            message.obj = currentEcouponsDetailEntity.getReferenceNo();
            mQueryQrStatusHandler.sendMessageDelayed(message, CURRENT_QEQUERY_QRCODE);
        }
        //刷新二维码
        ecouponUseAdapter.notifyDataSetChanged();
    }

    /**
     * 二维码详情拉取失败之后 需要更新ui ui要展示点击重试
     *
     * @param errorCode
     * @param errorMsg
     * @param referenceNo
     */
    @Override
    public void geteVoucherDetailsError(String errorCode, String errorMsg, String referenceNo) {
        if (errorCode.equals(ErrorCode.SHOULE_VERIRY_PWD.code)) {
            //需要验证密码
            verifyPwdAction();
        } else {
            showErrorMsgDialog(this, errorMsg);
        }
        //二维码详情拉取失败的时候 更新状态 需要重试按钮
        for (int i = 0; i < eVoucherInfosBeans.size(); i++) {
            if (!TextUtils.isEmpty(eVoucherInfosBeans.get(i).getReferenceNo()) && eVoucherInfosBeans.get(i).getReferenceNo().equals(referenceNo)) {
                //更新二维码信息为拉取失败
                eVoucherInfosBeans.get(i).setEcouponsDetail(null);
                eVoucherInfosBeans.get(i).setGetDetailSuccess(false);
                break;
            }
        }
        mQueryQrStatusHandler.removeCallbacksAndMessages(null);
        //刷新二维码
        ecouponUseAdapter.notifyDataSetChanged();
    }

    @Override
    public void redeemeVouchersStatusSuccess(EVoucherStatusEntity response) {
        if (response.getRows() != null && response.getRows().size() > 0) {
            updateEvoucherItemStatus(response.getRows().get(0));
        } else {
            //轮训插叙状态
            mQueryQrStatusHandler.sendEmptyMessageDelayed(-1, CURRENT_DELAT_TIME);
        }
    }

    /**
     * 更新二维码状态
     *
     * @param rowsBean
     */
    private void updateEvoucherItemStatus(EVoucherStatusEntity.RowsBean rowsBean) {
        boolean isQueryStatusStop = false;
        String status = rowsBean.getStatus();
        String referenceNo = rowsBean.getReferenceNo();
        for (int i = 0; i < eVoucherInfosBeans.size(); i++) {
            if (eVoucherInfosBeans.get(i).getReferenceNo().equals(referenceNo)) {
                //更新二维码最新状态
                eVoucherInfosBeans.get(i).getEcouponsDetail().setStatus(status);
                if (eVoucherInfosBeans.get(i).getEcouponsDetail().isUsed()) {
                    //已经使用之后 不能轮训了
                    isQueryStatusStop = true;
                }
                break;
            }
        }
        ecouponUseAdapter.notifyDataSetChanged();
        if (!isQueryStatusStop) {
            //二维码状态未使用 接着轮训
            mQueryQrStatusHandler.sendEmptyMessageDelayed(-1, CURRENT_DELAT_TIME);
        }
    }

    @Override
    public void redeemeVouchersStatusError(String errorCode, String errorMsg) {
        //轮训插叙状态
        mQueryQrStatusHandler.sendEmptyMessageDelayed(-1, CURRENT_DELAT_TIME);
    }

    @Override
    public void getMyEcouponsListSuccess(MyEVoucherEntity response) {
        mEvoucherItems = response.getRows();
        //根据金额 分组 显示
        initChildItems();
        //默认选中第一个金额的 然后根据第一个金额的数组刷新底部二维码列表
        initViewPager();
        //默认拉取第一个底部二维码详情
        changeTitle(currentPosition, true, true, false);
        idTermCondition.setVisibility(View.VISIBLE);
        tvPayment.setVisibility(View.VISIBLE);
        //更新当前的选中位置
        updateItemStatus(currentPosition);
    }


    @Override
    public void getMyEcouponsListError(String errorCode, String errorMsg) {
        showErrorMsgDialog(this, errorMsg);
    }


    private void verifyPwdAction() {
        VerifyPasswordCommonActivity.startActivityForActionResult(getActivity(), "E", new OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                changeTitle(currentPosition, false, true, true);
            }

            @Override
            public void onVerifyFailed(String errorCode, String errorMsg) {
                showErrorMsgDialog(getActivity(), errorMsg);
            }

            @Override
            public void onVerifyCanceled() {

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mQueryQrStatusHandler.removeCallbacksAndMessages(null);
        // 移除所有消息
        setWindowBrightness(WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE);
    }

    @Override
    public void onPageSelected(int position) {
        if (currentChildPosition == position) return;
        currentChildPosition = position;
        changeTitle(currentPosition, false, true, true);
    }

}

