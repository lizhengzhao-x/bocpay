package cn.swiftpass.wallet.intl.module.rewardregister.entity;

import cn.swiftpass.httpcore.entity.BaseEntity;

/**
 * 信用卡奖赏登记返回
 */
public class RewardRegisterEntity extends BaseEntity {

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    private String refId;


}
