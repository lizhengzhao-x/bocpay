package cn.swiftpass.wallet.intl.module.ecoupon.entity;

import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.BaseEntity;

public class EcouponActivitityListEntity extends BaseEntity {

    private ArrayList<EcouponActivityEntity> activities;

    public ArrayList<EcouponActivityEntity> getActivities() {
        return activities;
    }

    public void setActivities(ArrayList<EcouponActivityEntity> activities) {
        this.activities = activities;
    }


}
