
#org.simpleframework
-dontwarn org.simpleframework.**
-keep class org.simpleframework.**{*;}


#bochksdk包
-dontwarn com.bochk.ewtqrclibrary.**
-keep class com.bochk.ewtqrclibrary.**{*;}

#bouncycastle包
-dontwarn org.bouncycastle.**
-keep class org.bouncycastle.**{*;}



#EventBus
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }

# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}

#Glide
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep class * extends com.bumptech.glide.module.AppGlideModule {
 <init>(...);
}
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
-keep class com.bumptech.glide.load.data.ParcelFileDescriptorRewinder$InternalRewinder {
  *** rewind();
}


#Glide



## butterknife[version 8.2.1]

-keep public class * implements butterknife.Unbinder { public <init>(**, android.view.View); }

# Prevent obfuscation of types which use ButterKnife annotations since the simple name
# is used to reflectively look up the generated ViewBinding.
-keep class butterknife.*
-keepclasseswithmembernames class * { @butterknife.* <methods>; }
-keepclasseswithmembernames class * { @butterknife.* <fields>; }
## butterknife




## gson[version 2.8.0]
#-keep class sun.misc.Unsafe { *; }
-dontwarn com.google.**
-keep class com.google.gson.** {*;}
-keep class com.google.gson.stream.** { *; }
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
## gson

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}


######################################webview-----start ###########################################################
#-keepclassmembers class fqcn.of.javascript.interface.for.Webview {
#   public *;
#}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String, android.graphics.Bitmap);
    public boolean *(android.webkit.WebView, java.lang.String);
}
-keepclassmembers class * extends android.webkit.WebViewClient {
    public void *(android.webkit.WebView, java.lang.String);
}

######################################webview-----end ###########################################################


###################################### Support包规则----start ###################################################
# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**        # 如果引用了v4或者v7包
-keep class android.support.** {*;} #保留support下的所有类及其内部类

# Understand the @Keep support annotation.
-keep class android.support.annotation.Keep
-keep @android.support.annotation.Keep class * {*;}

-keepclasseswithmembers class * {
    @android.support.annotation.Keep <methods>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <fields>;
}
-keepclasseswithmembers class * {
    @android.support.annotation.Keep <init>(...);
}
###################################### Support包规则-----end ###################################################



#---------------------------------1.实体类---------------------------------
-keep class cn.swiftpass.wallet.intl.entity.** { *;}
-keep class cn.swiftpass.wallet.intl.utils.** {*;}
-keep class * extends cn.swiftpass.httpcore.entity.BaseEntity{*;}
#-------------------------------------------------------------------------

#samsung
-keep class com.samsung.android.sdk.** { *; }
-dontwarn com.samsung.android.sdk.**


#butterknife
-keep class butterknife.** { *; }
-dontwarn butterknife.internal.**
-keep class **$$ViewBinder { *; }
-keepclasseswithmembernames class * {
    @butterknife.* <fields>;
}
-keepclasseswithmembernames class * {
    @butterknife.* <methods>;
}

#FIO
#fido
-dontwarn com.daon.**,org.bouncycastle.**, com.samsung.**
-keep public class com.daon.fido.client.sdk.core.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.exception.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.handler.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.model.* {
    public protected *;
    private <fields>;
}
-keep public class com.daon.fido.client.sdk.transaction.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.ui.* {
    public protected *;
}
-keep class com.daon.fido.client.sdk.init.InitialiseSdkActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.auth.UafClientAuthenticateActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.dereg.UafClientDeregisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.reg.UafClientRegisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.uaf.UafMessageUtils {
    public protected *;
}
-keep class com.daon.sdk.authenticator.capture.CaptureActivity {
    public protected *;
}
-keep class com.samsung.android.sdk.** {*;}

# Preserve all class for Tradelink
-keep public class com.tradelink.boc.authapp.exception.* {
    public protected *;
}
-keep public class com.tradelink.boc.authapp.task.* {
    public protected *;
}
-keep public class com.tradelink.boc.authapp.model.* {
    public protected *;
    private <fields>;
}
-keep public class com.tradelink.boc.authapp.ui.* {
    public protected *;
}

-keep public class com.tradelink.boc.sotp.task.* {
    public protected *;
}
-keep public class com.tradelink.boc.sotp.model.* {
    public protected *;
    private <fields>;
}
-keep public class com.tradelink.boc.sotp.ui.* {
    public protected *;
}
#FIO

#root
-keep class com.scottyab.rootbeer.RootBeer{
public <methods>;
}
#root

#启动检查sdk
-keepattributes Signature
-dontwarn com.bochklaunchflow.**
-keep class com.bochklaunchflow.base.** {*;}
-keep class com.bochklaunchflow.bean.** {*;}
-keep class com.bochklaunchflow.callback.** {*;}
-keep class com.bochklaunchflow.http.** {*;}
-keep class com.bochklaunchflow.okhttp.** {*;}
-keep class com.bochklaunchflow.string.** {*;}
-keep class com.bochklaunchflow.utils.** {
public <methods>;
}
-keep class com.bochklaunchflow.BOCHKLaunchFlow {
public <methods>;}
-keep class com.bochklaunchflow.OkHttpUtils {
public <methods>;
}
-keep class com.bochklaunchflow.DownloadCertManager {
public <methods>;
}
#启动检查sdk

##webview  https
-keep public class android.net.http.SslError

-dontwarn android.webkit.WebView
-dontwarn android.net.http.SslError
-dontwarn android.webkit.WebViewClient


#pinyin4j

-keep class net.sourceforge.** {*;}
-keep class com.hp.** {*;}
-dontwarn net.sourceforge.**
-dontwarn net.sourceforge.pinyin4j.**
-keep class net.sourceforge.pinyin4j.**{*;}
-keep class net.sourceforge.pinyin4j.format.**{*;}
-keep class net.sourceforge.pinyin4j.format.exception.**{*;}

-dontwarn demo.**
-keep class demo.** { *;}
#pinyin4j


##-keep class org.apache.commons.** { *; }

-dontwarn org.apache.http.**
-keep class org.apache.http.** { *;}

##自定义View
-keep class cn.swiftpass.wallet.intl.dialog.SelectPopupWindow{*;}

-dontwarn android.net.http.**

##fastjson
-dontwarn com.alibaba.fastjson.**
-keep class com.alibaba.fastjson.** { *; }
-keepattributes Signature
-keepattributes *Annotation*
##fastjson

#-keep class sun.misc.Unsafe{*;}
#-keep class fqcn.of.javascript.interface.for.Webview{*;}



-keep class com.fasterxml.jackson.annotation.** { *; }
-dontwarn com.fasterxml.jackson.databind.**


-keepattributes EnclosingMethod
-keepattributes InnerClasses

-keep class com.fasterxml.jackson.annotation.** { *; }
-dontwarn com.fasterxml.jackson.databind.**



-keep class pl.droidsonroids.gif.annotation.** { *; }
-dontwarn pl.droidsonroids.gif.**

-keep public class cn.cloudwalk.libproject.* {
    public protected *;
}
#IDV 混淆代码：

        -keep class com.idv.sdklibrary.bean.**{*;}
        -dontwarn com.abbyy.mobile.**
        -keep class com.abbyy.mobile.** { *; }

        #FRP混淆规则：
                -keep class cn.cloudwalk.** {*;}
                -keep class com.frp.libproject.bean.** {*;}



#kotlin
        -keep class kotlin.** { *; }
        -keep class kotlin.Metadata { *; }
        -dontwarn kotlin.**
        -keepclassmembers class **$WhenMappings {
               <fields>;
        }
        -keepclassmembers class kotlin.Metadata {
        public <methods>;
        }
        -assumenosideeffects class kotlin.jvm.internal.Intrinsics {
        static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
       }


    #===========commonlib============================
    -keep class com.lc.commonlib.**{*;}
    # FastJson 混淆代码
    -dontwarn com.alibaba.fastjson.**
    -keep class com.alibaba.fastjson.** { *; }
    -keepattributes Signature
    -keepattributes *Annotation*
    #==========webrtcsdk==============================
    -keep class com.lc.webrtcsdk.**{*;}
    #webrtc
    -dontwarn org.chromium.**
    -dontwarn org.webrtc.**
    -keep class org.chromium.**{*;}
    -keep class org.webrtc.**{*;}
    #==========xchatkit================================
    -keep class com.albert.xchatkit.**{*;}
    -keep class com.ws.WebSocketClient.**{*;}


#kotlin
    -keep class kotlin.** { *; }
    -keep class kotlin.Metadata { *; }
    -dontwarn kotlin.**
    -keepclassmembers class **$WhenMappings {
        <fields>;
    }
    -keepclassmembers class kotlin.Metadata {
        public <methods>;
    }
    -assumenosideeffects class kotlin.jvm.internal.Intrinsics {
        static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
    }








