package cn.swiftpass.httpcore.config;


public class HttpCoreConstants {


    //初始
    public static final String ORI_STR_SVR_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAkmgSG7esqkyhnV2O2kmaMjwl6P5fIjDFqcDzI3j87ygiRjRqgCZi/MVnTl4Jj8yj0cnT35HIZ9rwKLyOWfd/nOo92wgBNqGCZjBj3R1SIxP5EVbs3x09nWx2AEA5H+aOKKaTX2pXINuHLIv040nHpV9O0EAI81ePCPt1TWUGOrumZIw6kiAfzBnVdgVIr57Bwy85UfjVQ8ixmWdqD7ZcA0WrGpTC+JVCm1cvbeikwywJwHuWQce95vOE5BxuLjDJNVFTB0TtgKbNtVNRUW7kpthpXXRNx1DlXZd3wWEgLWwQeaxZ5xyeiiCVHhTo0AzgV68J0YhzNoVFUJqJ0QGbKQIDAQAB";


    public static final String INIT_FIXED_IV = "WT_INTIAL_IV";
    public static final String FIXED_IV_STR = "FIXED_IV_STR";

    public static final String APP_PRIVATEKEY = "privateKey";
    public static final String APP_PUBLICKEY = "publicKey";

    public static final String DATA_STR_UUID = "DATA_STR_UUID";

    public static final String App_RSA_Update = "App_RSA_Update";

    public static final String DATA_STR_UUID_NEW = "DATA_STR_UUID_NEW";
    public static final String DATA_STR_UUID_NEW_NOT_ENCRYPTION = "DATA_STR_UUID_NEW_NOT_ENCRYPTION";

    public static final String CACHE_IS_LOGIN = "CACHE_IS_LOGIN";
    public static final String CACHE_IS_HONGHK = "CACHE_IS_HONGHK";

    public static final String CACHE_FINGER_PRINT = "CACHE_FINGER_PRINT";

    public static final String CACHE_SHOW_GUIDE = "CACHE_SHOW_GUIDE";

    public static final String CACHE_LAST_LOGIN_TIME = "CACHE_LAST_LOGIN_TIME";

    /**
     * 繁体中文
     */
    public static final String LANG_CODE_ZH_TW = "zh_TW";

    public static final String LANG_CODE_ZH_MO = "zh_MO";

    public static final String LANG_CODE_ZH_HK = "zh_HK";
    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US = "en_US";
    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN = "zh_CN";


    public static final String LANG_CODE_ZH_CN_NEW = "zh_CN";


    /**
     * 英语
     */
    public static final String LANG_CODE_EN_US_NORMAL = "en-US";
    /**
     * 简体中文
     */
    public static final String LANG_CODE_ZH_CN_NORMAL = "zh-CN";

    public static final String LANG_CODE_ZH_TW_NORMAL = "zh-TW";

    public static final String LANG_CODE_ZH_HK_NORMAL = "zh-HK";

    public static final String LANG_CODE_ZH_MO_NEW_NORMAL = "zh-MO";

    public static final String LANG_CODE_ZH_HK_NEW = "zh_HK";
    public static final String APP_SETTING_LANGUAGE = "App_setting_language";

    public static final String LOCATION_TIP_ALREADY_SHOW = "LOCATION_TIP_ALREADY_SHOW";
}
