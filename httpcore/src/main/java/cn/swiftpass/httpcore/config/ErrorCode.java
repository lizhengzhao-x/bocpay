package cn.swiftpass.httpcore.config;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;

/**
 * Created by ramon on 2018/8/30.
 */

public enum ErrorCode {
    /**
     * 链接超时
     */

    CONTENT_TIME_OUT("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    /**
     * 后台返回response为""的异常
     */
    NULL_RESPONSE("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    /**
     * Gson解析CommonEntity异常
     */
    GSON_COMMON_ERROR("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    /**
     * Gson解析CommonEntity后commonEntity为空的异常
     */
    NULL_COMMON_ENTITY("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    /**
     * Gson解析ContentEntity异常
     */
    GSON_CONTENT_ERROR("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    /**
     * Gson解析ContentEntity后ContentEntity为空的异常
     */
    NULL_CONTENT_ENTITY("1001", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    NETWORK_ERROR("1001", HttpCoreKeyManager.getInstance().getNetWorkErrorStr()),

    SERVER_INTER_ERROR("1002", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    SERVER_INTER_FAILED("1003", HttpCoreKeyManager.getInstance().getTimeOutStr()),


    //encrypt
    SERVER_KEY_FAIL("A3001", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    //encrypt
    SHOULE_VERIRY_PWD("EWA5524", -1),

    DECRYPT_KEY_FAIL("A3002", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    DECRYPT_DATA_FAIL("A3003", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    VERIFY_SIGN_FAIL("A3004", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    VERIFY_SERVER_TIME_TAMP_FAIL("A3005", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    //訂單重複提交 超時問題
    TASNSFER_ORDER_ERROR("EWA3036", HttpCoreKeyManager.getInstance().getTimeOutStr()),

    //server public 出错
    SERVER_PUBKEY_ERROR("EWA9023", HttpCoreKeyManager.getInstance().getTimeOutStr()),
    LOGIN_NEW_DEVICE_FORGETPWD("EWA5341", 0),

    //Server
    //未登录，请重新登录
    UNLOGIN("EWA1002", 0), //在其他设备登录过，在新设备登录时，新设备提示
    LOGIN_NEW_DEVICE("EWA4011", 0),
    //在其他设备登录过，在新设备登录时，旧设备提示

    LOGIN_OLD_DEVICE("EWA1060", 0),
    RELOGIN("EWA5010", 0), //长时间未操作
    FIO_REG_FAILED("FIO2000", HttpCoreKeyManager.getInstance().getFioRegFailedResId()), FIO_AUTH_FAILED("FIO3000", HttpCoreKeyManager.getInstance().getFioRegFailedResId()), FIO_DEREG_FAILED("FIO4000", HttpCoreKeyManager.getInstance().getFioAuthFailedResId()),

    //卡信息已注册
    CARD_INFO_HAD_REG("EWA1013", 0),

    SERVER_PUBLIC_KEY_INVALID("EWA1004", 0),

    SERVER_PUBLIC_KEY_AGAIN("EWA1006", 0),

    //"當前登錄賬戶擁有人與BoC Pay註冊客戶並非同一人
    SERVER_ACCOUNT_NOT_SAME("EWA1036", 0), //电话号码已经变化,是否继续操作
    CARD_PHONE_CHANGE_REG("EWA1048", 0), CARD_PHONE_CHANGE_BIND("EWA1049", 0), MODIFY_PASSCODE_QUIT("EWA1059", 0),

    SERVER_DEVICE_OTHER_LOGIN("EWA7015", 0),

    //上一步操作未完成
    LAST_STEP_UNCOMPLETE("EWA5011", 0), //两分钟内不能重复发短信
    REPEAT_SEND_OTP_FAIL("EWA1022", 0),
    REGISTER_TIME_OUT("EWA5575", 0),//pa注册 流程控制接口超时
    PA_FORGETPWD_TIME_OUT("EWA5576", 0),//pa忘记密码流程控制接口超时
    PA_BIND_TIME_OUT("EWA5577", 0),//pa绑卡流程控制接口超时
    SA_BIND_NUM_OVER_FIVE("EWA5705", 0),//一键绑卡流程超过5次
    SA_BIND_NUM_OVER_FLOW("EWA9031", 0),//一键绑卡流程结束


    //手机号码有变更，注销账号
    PHONE_CHANGE_UNREG_ACCOUNT("EWA1063", 0),
    OTP_FAILED_FIVE("EWA5600", 0),//otp错误5次

    //登录时密码输入出错3次以上
    OTP_FAIL_LOGIN_ONE("EWA1053", 0), OTP_FAIL_LOGIN_TWO("EWA1054", 0), OTP_FAIL_REG_ONE("EWA1055", 0), OTP_FAIL_REG_TWO("EWA1056", 0), OTP_FAIL_BIND_ONE("EWA1057", 0), OTP_FAIL_BIND_TWO("EWA1058", 0),

    //我的账户相关操作OTP时密码输入出错3次以上
    OTP_FAIL_SMART_ACCOUNT_TWO("EWA1064", 0), OTP_FAIL_SMART_ACCOUNT_ONE("EWA1065", 0), //开启FIO操作OTP时密码输入出错3次以上
    OTP_FAIL_FIO_TWO("EWA1066", 0), OTP_FAIL_FIO_ONE("EWA1067", 0), //忘记密码
    OTP_FAIL_FORGET_ONE("EWA1023", 0), OTP_FAIL_FORGET_TWO("EWA1043", 0),
//    1、登录，如果出现这两个（EWA1053(发送otp)，EWA1054(验证OTP)）错误码，需要返回登录页
//    2、注册，如果出现这两个（EWA1055(发送otp)，EWA1056(验证OTP)）错误码，需要结束注册流程，回到登录页
//    3、绑定，如果出现这两个（EWA1057(发送otp)，EWA1058(验证OTP)）错误码，需要结束绑定流程，回到卡列表

    SERVER_VERUFT_PWD_AGAIN("EWA4003", 0), //atm注册信息错误,网银5次三次退出到主页面
    BANK_VERIFY_ONLINE_INFO_FAIL("EWA5069", 0), BANK_VERIFY_OFFLINE_INFO_FAIL("EWA5001", 0),

    //有信用卡，提示去绑定我的账户，跳到登陆界面(已注册电子钱包，请绑定我的账户)
    REG_FAIL_TO_BIND("EWA5071", 0),
    //注销用户信息，提示重新注册 调到登陆界面(我的账户信息发生变更，已解绑我的账户,请退出重新注册)
    SMARTACCOUNT_CHANGE_TO_REG("EWA5072", 0),

    VERIFY_PASSWORD_ERROR("EWA4010", 0),


    //EWA9002   客户端fioid与服务端不匹配，或者客户端设备id 与 服务端设备id不匹配
    // EWA9022  未经过短信验证

    FIO_SERVER_VERIFY_ERROR("EWA9002", 0),


    FIO_SERVER_REG_ERROR("EWA9022", 0),

    FPS_ADD_CONFIRM_ERROR("PJ775", 0),

    FPS_OTP_FAIL_SEND("EWA5123", 0), FPS_OTP_FAIL_VERIFY("EWA5119", 0),

    FPS_EMAIL_OTP_FAIL_SEND("EWA5125", 0),


    /**
     * session id 过期，需要登出，重新登录 没有sessionid
     */
    FPS_SESSION_INVALID("EWA4008", 0),

    //相同資料已經註冊,請直接登錄”，客戶點擊確認後直接進入登錄頁面
    SERVER_STWO_REGISTER("EWA5150", 0),

    //相同資料已經註冊，請使用已註冊手機號登錄” ，客戶點擊確認後直接進入登錄頁面
    SERVER_PUBKEY_ERROR_DATA("EWA5133", 0),

    //彈出對話窗“相同手機號已經註冊，請直接登錄” ，客戶點擊確認後直接進入登錄頁面
    SERVER_PUBKEY_ERROR_PHONE("EWA5151", 0),
    ACTIVITY_FINISH("EWA3105", 0),
    ACTIVITY_FINISH_OTHER("EWA3109", 0),
    CHECK_ID_RETRY("DJ321", 0),
    CHECK_ID_ZHONGYIN_CURTOMER("EWA5347", 0),
    BOUNCE_BACK("EWA1097", 0),
    CHECK_ID_ZHONGYIN_CUS("EWA5346", 0),
    TRANSFER_NO_ACCOUNT("EWA3037", 0),
    CHECK_IDV_FAILED("DJ998", 0),
    DEFAULT_ERROR("DEFAULT_ERROR", 0),
    VIRTUAL_LIST_FAILED("EWA5573", 0),
    ACCOUNT_ACTIVED("EWA5067", 0),

    //红包重复拆 弹出不一样的对话框
    RED_OPEN_REPEAT("EWA3317", 0)
    //5182 5183  5279  5280  中黑名单检测

    ;


    public final String code;
    public final int msgId;

    private ErrorCode(String code, int msgResId) {
        this.code = code;
        this.msgId = msgResId;
    }
}
