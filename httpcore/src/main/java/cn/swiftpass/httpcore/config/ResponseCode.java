package cn.swiftpass.httpcore.config;

/**
 * http请求所有responseCode
 */

public interface ResponseCode {
    String RESPONSE_SUCCESS = "0";
    /**
     * 自定义本地错误code
     */
    String SERVER_INTER_ERROR = "1002";
    /**
     * 链接超时
     */
    String CONTENT_TIME_OUT = "1001";
    /**
     * 客户端私钥解密Key值失败
     */
    String DECRYPT_KEY_FAIL = "1003";
    /**
     * 客户端AES解密data数据失败
     */
    String DECRYPT_DATA_FAIL = "1004";
    /**
     * 验签失败
     */
    String VERRIFY_KEY_FAIL = "1005";
    /**
     * 无网络
     */
    String CONTENT_NO_INTERNET = "400";


    /**
     * json解析失败
     */
    String RESPONSE_PARSER_ERROR = "150";


    /******************服务端返回errorcode****************************/
    /**
     * 服务端参数解析出错
     */
    String ERROR_ANALISIS_PARAMETER = "1000001";
    /**
     * 未登录，请重新登录
     */
    String RE_LOGIN = "1000002";
    /**
     * session过期，请重新登录
     */
    String SESSION_LOSE_RE_LOGIN = "1000003";
    /**
     * 服务端参数解密出错
     */
    String ERROR_DECRYPT_PARAMETER = "1000004";
    /**
     * 服务端参数验签失败
     */
    String ERROR_SIGN_PARAMETER_FAIL = "1000005";
    /**
     * 服务端参数校验未通过
     */
    String ERROR_SIGN_FAIL = "1010001";


}
