package cn.swiftpass.httpcore.config;

public class HttpCoreRequestParams {

    //--------获取服务端公钥接口--------------//
    //公钥
    public static String APP_PUBKEY = "appPubKey";


    /**
     * 用户会话
     */
    public static String S_ID = "s_id";

    /**
     * 请求时间戳
     */
    public static String TIMETAMP = "timestamp";

    /**
     * 语言标识
     */
    public static String LANG = "lang";
    /**
     * 设备标志
     */
    public static String DEVICE_MODEL = "device_mode";
    /**
     * 设备品牌
     */
    public static String DEVICE_BRAND = "device_brand";
    /**
     * APP版本
     */
    public static String APPVERSION = "app_version";
    /**
     * 系统版本
     */
    public static String SYSVERSION = "sys_version";

    /**
     * API版本
     */
    public static String APIVERSION = "api_version";
    /**
     * 网络类型
     */
    public static String NET_TYPE = "network_type";

    /**
     * 设备唯一ID
     */
    public static String DEVICE_ID = "device_id";

    /**
     * 用户ID
     */
    public static String USER_ID = "user_id";
    /**
     * 签名
     */
    public static String SIGN = "sign";
    public static String DEVICE_TOKEN = "device_token";


}
