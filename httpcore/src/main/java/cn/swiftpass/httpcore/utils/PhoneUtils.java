package cn.swiftpass.httpcore.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.AndroidKeyStoreUtils;


/**
 * Created by Jamy on 2018/7/19.
 * 获取手机的基础参数
 */

public class PhoneUtils {
    private static final String TAG = "AndroidUtils";

    /**
     * 获取手机App语言
     *
     * @return
     */
    public static String getLan() {

        String lan = HttpCoreSpUtils.getAppLanguageForServer();
        return lan;
    }

    /**
     * 设备名称
     *
     * @return
     */
    public static String getPhoneModel() {
        return Build.MODEL;
    }

    /**
     * 设备厂商
     *
     * @return
     */
    public static String getDeviceBrand() {
        //return Build.BOARD + "  " + Build.MANUFACTURER;
        return Build.MANUFACTURER;
    }

    /*   *//**
     * 获取当前本地apk的版本
     *
     * @return
     *//*
    public static String getAppVersionName() {
        String versionName = BuildConfig.VERSION_NAME;
        return versionName;
    }*/

    /**
     * 获取当前手机系统版本号
     *
     * @return 系统版本号
     */
    public static String getSystemVersion() {
        return Build.VERSION.RELEASE;
    }

    /**
     * 1.app首次安装 生成UUID 并通过加密存放到 SP
     * 2.app非首次安装 先从SP中拿到解密之后的生成UUID 然后通过解密 并同时通过static 放到内存中，防止多次加解密耗时
     *
     * @return
     */
    public static String getDeviceId() {
        //先从内存中获取deviceID 如果存在直接返回 理论上只要从本地获取一次之后都会直接用内存中的值
        String udid = HttpCoreKeyManager.getInstance().getCurrentDeviceId();
        if (!TextUtils.isEmpty(udid)) {
            return udid;
        }
        try {
            //线上发现个别机型异常 用此字段表示异常机型
            boolean isSpecialMachine = (boolean) HttpCoreSpUtils.get(HttpCoreConstants.DATA_STR_UUID_NEW_NOT_ENCRYPTION, false);
            //通过Sp里头获取加密之后的udid
            udid = (String) HttpCoreSpUtils.get(HttpCoreConstants.DATA_STR_UUID_NEW, "");
            if (!isSpecialMachine) {
                if (TextUtils.isEmpty(udid)) {
                    //如果app是首次安装 需要先生成 然后通过AES加密之后存放到SP
                    udid = java.util.UUID.randomUUID().toString();
                    String encryptedText = AndroidKeyStoreUtils.getInstance().encryptData(udid, false);
                    HttpCoreSpUtils.put(HttpCoreConstants.DATA_STR_UUID_NEW, encryptedText);
                    HttpLogUtils.i(TAG, "uuid create uuid:" + udid);
                } else {
                    //app并非第一次打开，直接AES解密 然后存放到内存中 之后再使用
                    udid = AndroidKeyStoreUtils.getInstance().decryptData(udid, false);
                    HttpLogUtils.i(TAG, "uuid use old :" + udid);
                }
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        if (TextUtils.isEmpty(udid)) {
            HttpLogUtils.e(TAG, "ERROR-------:" + udid);
            //一堆操作之后，发现uuid还是加解密不成功，因为涉及AES加密try catch 线上用户个别手机出现问题，具体不知道如何操作，此问题纠结很久，现在解决方案是如果发现出现这种
            //问题，就直接明文存储到SP里头，保证没有UUID直接无法使用Bocpay功能
            udid = java.util.UUID.randomUUID().toString();
            HttpCoreSpUtils.put(HttpCoreConstants.DATA_STR_UUID_NEW, udid);
            HttpCoreSpUtils.put(HttpCoreConstants.DATA_STR_UUID_NEW_NOT_ENCRYPTION, true);
        }
        HttpCoreKeyManager.getInstance().setCurrentDeviceId(udid);
        return udid;
    }


    /**
     * 获取设备时间
     *
     * @return
     */
    public static String getTimeStem() {
        return System.currentTimeMillis() + "";
    }

    //没有网络连接
    public static final int NETWORN_NONE = 0;
    //wifi连接
    public static final int NETWORN_WIFI = 1;
    //手机网络数据连接类型
    public static final int NETWORN_2G = 2;
    public static final int NETWORN_3G = 3;
    public static final int NETWORN_4G = 4;
    public static final int NETWORN_MOBILE = 5;


    public static String getNetworkType() {
        int networkstatus = getNetworkState();
        if (NETWORN_NONE == networkstatus) {
            return "";
        } else if (NETWORN_WIFI == networkstatus) {
            return "wifi";
        } else if (NETWORN_2G == networkstatus) {
            return "2G";
        } else if (NETWORN_3G == networkstatus) {
            return "3G";
        } else if (NETWORN_4G == networkstatus) {
            return "4G";
        }
        return "";
    }

    /**
     * 获取当前网络连接状态
     *
     * @return
     */
    private static int getNetworkState() {
        //获取系统的网络服务
        ConnectivityManager connManager = (ConnectivityManager) HttpCoreKeyManager.getInstance().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        //如果当前没有网络
        if (null == connManager) {
            return NETWORN_NONE;
        }
        //获取当前网络类型，如果为空，返回无网络
        NetworkInfo activeNetInfo = connManager.getActiveNetworkInfo();
        if (activeNetInfo == null || !activeNetInfo.isAvailable()) {
            return NETWORN_NONE;
        }
        // 判断是不是连接的是不是wifi
        NetworkInfo wifiInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (null != wifiInfo) {
            NetworkInfo.State state = wifiInfo.getState();
            if (null != state) {
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    return NETWORN_WIFI;
                }
            }
        }
        // 如果不是wifi，则判断当前连接的是运营商的哪种网络2g、3g、4g等
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (null != networkInfo) {
            NetworkInfo.State state = networkInfo.getState();
            String strSubTypeName = networkInfo.getSubtypeName();
            if (null != state) {
                if (state == NetworkInfo.State.CONNECTED || state == NetworkInfo.State.CONNECTING) {
                    switch (activeNetInfo.getSubtype()) {
                        //如果是2g类型
                        case TelephonyManager.NETWORK_TYPE_GPRS:
                        case TelephonyManager.NETWORK_TYPE_CDMA:
                        case TelephonyManager.NETWORK_TYPE_EDGE:
                        case TelephonyManager.NETWORK_TYPE_1xRTT:
                        case TelephonyManager.NETWORK_TYPE_IDEN:
                            return NETWORN_2G;
                        //如果是3g类型
                        case TelephonyManager.NETWORK_TYPE_EVDO_A:
                        case TelephonyManager.NETWORK_TYPE_UMTS:
                        case TelephonyManager.NETWORK_TYPE_EVDO_0:
                        case TelephonyManager.NETWORK_TYPE_HSDPA:
                        case TelephonyManager.NETWORK_TYPE_HSUPA:
                        case TelephonyManager.NETWORK_TYPE_HSPA:
                        case TelephonyManager.NETWORK_TYPE_EVDO_B:
                        case TelephonyManager.NETWORK_TYPE_EHRPD:
                        case TelephonyManager.NETWORK_TYPE_HSPAP:
                            return NETWORN_3G;
                        //如果是4g类型
                        case TelephonyManager.NETWORK_TYPE_LTE:
                            return NETWORN_4G;
                        default:
                            //中国移动 联通 电信 三种3G制式
                            if (strSubTypeName.equalsIgnoreCase("TD-SCDMA") || strSubTypeName.equalsIgnoreCase("WCDMA") || strSubTypeName.equalsIgnoreCase("CDMA2000")) {
                                return NETWORN_3G;
                            } else {
                                return NETWORN_MOBILE;
                            }
                    }
                }
            }
        }
        return NETWORN_NONE;
    }


    public static boolean isHKLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_TW) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_HK) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_TW_NORMAL) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO_NEW_NORMAL) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_HK_NORMAL) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_MO_NEW_NORMAL));
    }


    public static boolean isEGLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US) || lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_EN_US_NORMAL));
    }

    public static boolean isZHLanguage(String lan) {
        if (TextUtils.isEmpty(lan)) {
            return false;
        }
        return (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_CN) || (lan.equalsIgnoreCase(HttpCoreConstants.LANG_CODE_ZH_CN_NORMAL)));
    }


}
