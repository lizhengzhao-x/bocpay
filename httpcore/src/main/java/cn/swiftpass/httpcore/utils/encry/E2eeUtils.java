package cn.swiftpass.httpcore.utils.encry;

import android.util.Log;

import com.isprint.e2eea.client.AxMxE2EEClient;

import cn.swiftpass.httpcore.utils.HttpLogUtils;


/**
 * Created by ramon on 2018/8/7.
 * E2EE加密
 */

public class E2eeUtils {
    private static final String TAG = E2eeUtils.class.getSimpleName();

    /*E2EE 登录或者设置密码加密
     *返回sid,pin
     */
    public static String getLoginOrSetPin(String e2eeSid, String pin, String publicKey, String serverRandom) {
        String result = "";
        try {
            byte hashAlgorithmID = AxMxE2EEClient.ALGO_ID_3DES_SHA1;
            AxMxE2EEClient e2ee = new AxMxE2EEClient();
            String RPIN = e2ee.encryptForLogin(hashAlgorithmID, e2eeSid, pin, publicKey, serverRandom);
            int retCode = e2ee.getRetCode();
            if (retCode == 0) {
                result = RPIN;

            } else {
                if (e2ee.isInvalidPin(retCode)) {
                    //invalid pin
                    HttpLogUtils.e(TAG, "error=" + retCode);
                } else {
                    if (e2ee.isError(retCode)) {
                        //is error
                        HttpLogUtils.e(TAG, "error=" + retCode);
                    }
                }
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return result;
    }

    /*E2EE 修改密码加密
     *Default value - DefaultClient.ALGO_ID_3DES_SHA1
     *
     */
    public static String getChangePin(String e2eeSid, String oldPin, String newPin, String publicKey, String serverRandom) {
        String result = "";
        AxMxE2EEClient e2ee = new AxMxE2EEClient();
        try {
            byte hashAlgorithmID = AxMxE2EEClient.ALGO_ID_3DES_SHA1;
            String RPIN = e2ee.encryptForChangePin(hashAlgorithmID, e2eeSid, oldPin, newPin, publicKey, serverRandom);
            int retCode = e2ee.getRetCode();
            if (retCode == 0) {
                //successful
                result = RPIN;
            } else {
                if (e2ee.isInvalidPin(retCode)) {
                    //invalid pin
                    HttpLogUtils.e(TAG, "error=" + retCode);
                } else {
                    if (e2ee.isError(retCode)) {
                        // is error
                        HttpLogUtils.e(TAG, "error=" + retCode);
                    }
                }
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return result;
    }
}
