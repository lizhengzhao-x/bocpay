package cn.swiftpass.httpcore.utils;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;

public class HeaderUtils {

    /**
     * 自定义header
     *
     * @return
     */
    public static JSONObject getHeaders() {
        JSONObject headers = new JSONObject();
        //头部参数
        try {
            headers.put(HttpCoreRequestParams.LANG, PhoneUtils.getLan());
            headers.put(HttpCoreRequestParams.DEVICE_MODEL, PhoneUtils.getDeviceBrand() + PhoneUtils.getPhoneModel());
            headers.put(HttpCoreRequestParams.DEVICE_BRAND, "AOS");
            headers.put(HttpCoreRequestParams.APPVERSION, HttpCoreKeyManager.getInstance().getAppVersion());
            headers.put(HttpCoreRequestParams.APIVERSION, "1.3.5");
            headers.put(HttpCoreRequestParams.NET_TYPE, PhoneUtils.getNetworkType());
            headers.put(HttpCoreRequestParams.TIMETAMP, PhoneUtils.getTimeStem());
            headers.put(HttpCoreRequestParams.DEVICE_ID, PhoneUtils.getDeviceId() + "_" + HttpCoreKeyManager.getInstance().getPackageId());
            headers.put(HttpCoreRequestParams.S_ID, HttpCoreKeyManager.getInstance().getSessionId());
            headers.put(HttpCoreRequestParams.SYSVERSION, PhoneUtils.getSystemVersion());
            if (!TextUtils.isEmpty(HttpCoreKeyManager.getInstance().getDeviceToken())) {
                headers.put(HttpCoreRequestParams.DEVICE_TOKEN, HttpCoreKeyManager.getInstance().getDeviceToken());
            }
        } catch (Exception e) {

        }
        return headers;
    }
}
