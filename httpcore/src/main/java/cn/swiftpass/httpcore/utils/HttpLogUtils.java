package cn.swiftpass.httpcore.utils;

import android.util.Log;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;

public class HttpLogUtils {

    private HttpLogUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * TODO FIX 要动态配置
     */
    public static final boolean isLogDebug = HttpCoreKeyManager.getInstance().isDebug();
    public static final boolean isWriteLog = true;

    public static void d(String tag, String msg) {
        if (isLogDebug) {
            Log.d(tag, msg + "");
        }
    }

    public static void d(Object object, String msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void d(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void i(String tag, String msg) {
        if (isLogDebug) {
            Log.i(tag, msg + "");
        }
    }

    public static void i(Object object, String msg) {
        if (isLogDebug) {
            Log.i(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void i(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void w(String tag, String msg) {
        if (isLogDebug) {
            Log.w(tag, msg + "");
        }
    }

    public static void w(Object object, String msg) {
        if (isLogDebug) {
            Log.w(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void w(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void e(String tag, String msg) {
        if (isLogDebug) {
            Log.e(tag, msg + "");
        }
    }

    public static void e(Object object, String msg) {
        if (isLogDebug) {
            Log.e(object.getClass().getSimpleName(), msg + "");
        }
    }
}
