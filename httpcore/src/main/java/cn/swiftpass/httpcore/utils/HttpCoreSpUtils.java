package cn.swiftpass.httpcore.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import cn.swiftpass.httpcore.config.HttpCoreConstants;


/**
 * SharedPreferences
 *
 * @author gc
 * @since 1.1
 */
public class HttpCoreSpUtils {

    private static final Object TAG = "HttpCoreSpUtils";

    private HttpCoreSpUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    /**
     * 保存文件名
     */
    private static final String FILE_NAME = "wallet_config";

    public static void initContext(Context mContext) {
        context = mContext;
    }

    private static Context context;

    /**
     * 保存数据的方法，根据类型调用不同的保存方法
     *
     * @param key
     * @param object
     */
    public static void put(String key, Object object) {
        if (context == null) return;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        if (object instanceof String) {
            editor.putString(key, (String) object);
        } else if (object instanceof Integer) {
            editor.putInt(key, (Integer) object);
        } else if (object instanceof Boolean) {
            editor.putBoolean(key, (Boolean) object);
        } else if (object instanceof Float) {
            editor.putFloat(key, (Float) object);
        } else if (object instanceof Long) {
            editor.putLong(key, (Long) object);
        } else {
            editor.putString(key, object.toString());
        }
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 获取数据的方法，根据默认值得到数据的类型，然后调用对应方法获取值
     *
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object get(String key, Object defaultObject) {
        if (context == null) return null;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        if (defaultObject instanceof String) {
            return sp.getString(key, (String) defaultObject);
        } else if (defaultObject instanceof Integer) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if (defaultObject instanceof Boolean) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if (defaultObject instanceof Float) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if (defaultObject instanceof Long) {
            return sp.getLong(key, (Long) defaultObject);
        }
        return null;
    }


    /**
     * 保存Object到SharedPreferences
     *
     * @param key
     * @param object
     */
    public static void putObject(String key, Object object) {
        if (context == null) return;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(baos);
            out.writeObject(object);
            String objectVal = new String(Base64.encode(baos.toByteArray(), Base64.DEFAULT));
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(key, objectVal);
            SharedPreferencesCompat.apply(editor);
        } catch (IOException e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        } finally {
            try {
                if (baos != null) {
                    baos.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
        }
    }


    /**
     * 移除某个key对应的值
     *
     * @param key
     */
    public static void remove(String key) {
        if (context == null) return;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        SharedPreferencesCompat.apply(editor);
    }

    /**
     * 清除所有数据
     */
    public static void clear() {
        if (context == null) return;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        //清除SharedPreferences 数据时候不允许删除 UUID 本地存储的数据
        String uuidEntryStr = (String) get(HttpCoreConstants.DATA_STR_UUID_NEW, "");
        editor.clear();
        SharedPreferencesCompat.apply(editor);
        put(HttpCoreConstants.DATA_STR_UUID_NEW, uuidEntryStr);
    }

    /**
     * 查询某个key是否已经存在
     *
     * @param key
     * @return
     */
    public static boolean contains(String key) {
        if (context == null) return false;
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        return sp.contains(key);
    }

//    /**
////     * 返回所有的键值对
////     *
////     * @return
////     */
////    public static Map<String, ?> getAll() {
////
////        SharedPreferences sp = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
////        return sp.getAll();
////    }

    /**
     * 创建一个解决SharedPreferencesCompat.apply方法的一个兼容类
     */
    private static class SharedPreferencesCompat {
        private static final Method sApplyMethod = findApplyMethod();

        /**
         * 反射查找apply的方法
         *
         * @return
         */
        @SuppressWarnings({"unchecked", "rawtypes"})
        private static Method findApplyMethod() {
            try {
                Class clz = SharedPreferences.Editor.class;
                return clz.getMethod("apply");
            } catch (NoSuchMethodException e) {
            }
            return null;
        }

        /**
         * 如果找到则使用apply执行，否则使用commit
         *
         * @param editor
         */
        public static void apply(SharedPreferences.Editor editor) {
            try {
                if (sApplyMethod != null) {
                    sApplyMethod.invoke(editor);
                    return;
                }
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            } catch (InvocationTargetException e) {
            }
            editor.commit();
        }
    }

    /**
     * @return
     * @throws
     * @Title:
     * @Description: 是否完成RSA2048升级
     */
    public static void setRSAUpdateFinish(int isDone) {
        put(HttpCoreConstants.App_RSA_Update, isDone);
    }

    /**
     * @return
     * @throws
     * @Title:
     * @Description: 是否完成RSA2048升级
     */
    public static int getRSAUpdateFinish() {
        return (int) get(HttpCoreConstants.App_RSA_Update, 0);
    }


    public static String getAppLanguageForServer() {
        String language = (String) get("App_setting_language", "");
        if (PhoneUtils.isHKLanguage(language)) {
            language = HttpCoreConstants.LANG_CODE_ZH_HK;
        } else if (PhoneUtils.isEGLanguage(language)) {
            language = HttpCoreConstants.LANG_CODE_EN_US;
        } else {
            language = HttpCoreConstants.LANG_CODE_ZH_CN;
        }
        return language;
    }


}