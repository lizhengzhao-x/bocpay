package cn.swiftpass.httpcore.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.BuildConfig;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;


/**
 * Created by ramon on 2018/9/18.
 */

public class HttpsUtils {

    public static final String TAG = HttpsUtils.class.getSimpleName();
    public static final String SSL_KEY_ALIAS = "BOC";


    /**
     * 加载本地的证书列表
     *
     * @return
     */
    public static List<Certificate> getLocalCertificates() {
        String certPath = HttpCoreKeyManager.getInstance().getHttpCertsName();
        List<Certificate> currentCertificates = new ArrayList<>();
        CertificateFactory certificateFactory = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
        } catch (Exception e1) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e1));
        }
        if (null == certificateFactory) {
            return null;
        }

        AssetManager assetManager = HttpCoreKeyManager.getInstance().getContext().getAssets();
        try {
            String[] filenameList = assetManager.list(certPath);
            for (int i = 0; i < filenameList.length; i++) {
                String filename = filenameList[i];
                HttpLogUtils.i(TAG, "filename: " + filename);
                if (!filename.endsWith(".crt") && !filename.endsWith(".cer")) {
                    continue;
                }
                try {
                    InputStream is = assetManager.open(certPath + File.separator + filename);
                    try {
                        Certificate cert = certificateFactory.generateCertificate(is);
                        currentCertificates.add(cert);
                    } catch (Exception e) {
                        HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                    } finally {
                        try {
                            if (is != null) {
                                is.close();
                            }
                        } catch (IOException e) {
                            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                } catch (IOException e) {
                    HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                }
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return currentCertificates;
    }

    //读取Assets目录文件夹下的ca证书
    public static KeyStore readKeyStore(String certPath) {
        if (TextUtils.isEmpty(certPath)) {
            return null;
        }
        KeyStore keyStore = null;
        CertificateFactory certificateFactory = null;
        try {
            certificateFactory = CertificateFactory.getInstance("X.509");
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
        } catch (Exception e1) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e1));
        }

        if (null == keyStore || null == certificateFactory) {
            return null;
        }


        AssetManager assetManager = HttpCoreKeyManager.getInstance().getContext().getAssets();
        try {
            String[] filenameList = assetManager.list(certPath);
            for (int i = 0; i < filenameList.length; i++) {
                String filename = filenameList[i];

                HttpLogUtils.i(TAG, "filename: " + filename);
                if (!filename.endsWith(".crt") && !filename.endsWith(".cer")) {
                    continue;
                }

                try {
                    InputStream is = assetManager.open(certPath + File.separator + filename);

                    try {
                        Certificate cert = certificateFactory.generateCertificate(is);
                        keyStore.setCertificateEntry(SSL_KEY_ALIAS + i, cert);
                    } catch (Exception e) {
                        HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                    } finally {
                        try {
                            if (is != null) {
                                is.close();
                            }
                        } catch (IOException e) {
                            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                        }
                    }
                } catch (IOException e) {
                    HttpLogUtils.e(TAG, Log.getStackTraceString(e));
                }
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return keyStore;
    }


    /**
     * CONTENT_TIME_OUT("1001", R.string.connect_time_out),
     * NETWORK_ERROR("400", R.string.string_network_available),
     * SERVER_INTER_ERROR("1002", R.string.connect_time_out),
     * SERVER_INTER_FAILED("1003", R.string.connect_time_out),
     * //encrypt
     * SERVER_KEY_FAIL("A3001", R.string.connect_time_out),
     * //encrypt
     * DECRYPT_KEY_FAIL("A3002", R.string.connect_time_out),
     * DECRYPT_DATA_FAIL("A3003", R.string.connect_time_out),
     * VERIFY_SIGN_FAIL("A3004", R.string.connect_time_out),
     * VERIFY_SERVER_TIME_TAMP_FAIL("A3005", R.string.connect_time_out),
     * //TODO 等待后台给出新的提示和错误编码
     * //protocol
     * PROTOCOL_PARSE_FAIL_ONE("A2001", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_TWO("A2002", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_THREE("A2003", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_FOUR("A2004", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_FIVE("A2005", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_SIX("A2006", R.string.connect_time_out),
     * PROTOCOL_PARSE_FAIL_SEVEN("A2007", R.string.connect_time_out),
     *
     * @param errorCode
     * @return
     */
    public static String getErrorString(ErrorCode errorCode) {
        Context activity = HttpCoreKeyManager.getInstance().getContext();
        if (null != errorCode && null != activity) {
            if (BuildConfig.FLAVOR.toLowerCase().contains("prd")) {
                return activity.getString(errorCode.msgId);
            } else {
                String errorCodeList[] = {ErrorCode.CONTENT_TIME_OUT.code, ErrorCode.NETWORK_ERROR.code, ErrorCode.SERVER_INTER_ERROR.code, ErrorCode.SERVER_INTER_FAILED.code, ErrorCode.DECRYPT_KEY_FAIL.code, ErrorCode.SERVER_KEY_FAIL.code,
                        ErrorCode.DECRYPT_DATA_FAIL.code, ErrorCode.VERIFY_SIGN_FAIL.code, ErrorCode.VERIFY_SERVER_TIME_TAMP_FAIL.code,
                        ErrorCode.NULL_RESPONSE.code, ErrorCode.GSON_COMMON_ERROR.code, ErrorCode.GSON_CONTENT_ERROR.code, ErrorCode.NULL_COMMON_ENTITY.code, ErrorCode.NULL_CONTENT_ENTITY.code};

                boolean isExitCustomCode = false;
                for (int i = 0; i < errorCodeList.length; i++) {
                    if (TextUtils.equals(errorCodeList[i], errorCode.code)) {
                        isExitCustomCode = true;
                        break;
                    }
                }
                if (isExitCustomCode) {
                    return activity.getString(errorCode.msgId);
                } else {
                    return activity.getString(errorCode.msgId) + "(" + errorCode.code + ")";
                }

            }
        }
        //TODO 确认是否有可能为空
        return "";

//        else {
//            return HttpCoreKeyManager.getInstance().getContext().getString(cn.swiftpass.wallet.intl.R.string.connect_time_out);
//        }
    }


}
