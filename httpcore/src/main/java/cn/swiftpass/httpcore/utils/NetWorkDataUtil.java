package cn.swiftpass.httpcore.utils;

import java.io.IOException;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * response.body().string()只能读取一次问题处理
 */
public class NetWorkDataUtil {

    /**
     * responseBody转String
     *
     * @param responseBody
     * @return
     * @throws IOException
     */
    public static String cloneResponseBody(ResponseBody responseBody) throws IOException {
        BufferedSource source = responseBody.source();
        // 获取全部body的数据
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();
        // 在读取缓存去之前clone数据，解决response.body().string()只能读取一次的问题
        return buffer.clone().readUtf8();
    }

    /**
     * okhttp requestBody转string方法
     *
     * @param requestBody
     * @return
     * @throws IOException
     */
    public static String requestBodyToStr(RequestBody requestBody) throws IOException {
        Buffer buffer = new Buffer();
        requestBody.writeTo(buffer);
        return buffer.readUtf8();
    }
}
