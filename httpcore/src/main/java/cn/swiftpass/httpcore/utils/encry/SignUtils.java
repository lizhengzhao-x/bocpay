package cn.swiftpass.httpcore.utils.encry;

import android.util.Log;

import java.security.MessageDigest;
import java.security.Signature;


/**
 * 验签工具 服务器用RSA私钥加签   客户端用服务器RSA公钥验签
 */

public class SignUtils {

    public static boolean verify(String sign, String data, String pubKey) {
        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(RSAHelper.getPublicKey(pubKey));
            //摘要
            MessageDigest sha256Digest = DigestUtils.getSha256Digest();
            sha256Digest.update(data.getBytes());
            signature.update(sha256Digest.digest());
            return signature.verify(Base64.decode(sign));
        } catch (Exception e) {
            Log.e("SignUtils", Log.getStackTraceString(e));
        }
        return false;

    }
}
