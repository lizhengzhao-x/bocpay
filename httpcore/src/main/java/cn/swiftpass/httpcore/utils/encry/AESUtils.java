package cn.swiftpass.httpcore.utils.encry;


import android.util.Log;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import cn.swiftpass.httpcore.utils.HttpLogUtils;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.ORI_STR_SVR_KEY;


/**
 * app->server 之间报文AES加解密
 */
public class AESUtils {

    public static final String TAG = AESUtils.class.getSimpleName();
    private static final String KEY_ALGORITHM = "AES";
    private static final String DEFAULT_CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final String CHAT_SET = "utf-8";
    private static final String VALUE_IV = ORI_STR_SVR_KEY.substring(0, 16);
    ;

    public static String AESEncrypt(String sSrc, String sKey) throws Exception {
        if (sKey == null) {
            Log.e(TAG, "Key为空null");
            return null;
        }
        // 判断Key是否为16位
        if (sKey.length() != 16 && sKey.length() != 32) {
            Log.e(TAG, "Key长度不是16位或32位");
            return null;
        }
        byte[] raw = sKey.getBytes(CHAT_SET);
        SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_ALGORITHM);
        //"算法/模式/补码方式"
        Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
        IvParameterSpec iv = new IvParameterSpec(VALUE_IV.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        byte[] encrypted = cipher.doFinal(sSrc.getBytes(CHAT_SET));
        //此处使用BASE64做转码功能，同时能起到2次加密的作用。
        return Base64.encodeBytes(encrypted);
    }

    public static String AESDecrypt(String sSrc, String sKey) {
        try {
            // 判断Key是否正确
            if (sKey == null) {
                Log.e(TAG, "Key为空null");
                return null;
            }
            // 判断Key是否为16位32位
            if (sKey.length() != 16 && sKey.length() != 32) {
                Log.e(TAG, "Key长度不是16位或32位");
                return null;
            }
            byte[] raw = sKey.getBytes(CHAT_SET);
            SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_ALGORITHM);
            Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);
            IvParameterSpec iv = new IvParameterSpec(VALUE_IV.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
            //先用base64解密
            byte[] encrypted1 = Base64.decode(sSrc);
            try {
                byte[] original = cipher.doFinal(encrypted1);
                String originalString = new String(original, CHAT_SET);
                return originalString;
            } catch (Exception e) {
                HttpLogUtils.e("AESUtils", Log.getStackTraceString(e));
                return null;
            }
        } catch (Exception ex) {
            HttpLogUtils.e("AESUtils", Log.getStackTraceString(ex));
            return null;
        }
    }
}