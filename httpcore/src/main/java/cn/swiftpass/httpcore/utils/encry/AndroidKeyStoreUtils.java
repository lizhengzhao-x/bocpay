package cn.swiftpass.httpcore.utils.encry;

import android.content.Context;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.GCMParameterSpec;

import cn.swiftpass.httpcore.BuildConfig;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.FIXED_IV_STR;
import static cn.swiftpass.httpcore.config.HttpCoreConstants.INIT_FIXED_IV;
import static cn.swiftpass.httpcore.utils.encry.AppInfoUtils.SHA1;


public class AndroidKeyStoreUtils {

    private static final String AES_MODE = "AES/GCM/NoPadding";
    private static final String AndroidKeyStore = "AndroidKeyStore";
    private static String FIXED_IV;
    private static final String KEY_ALIAS = "Wallet";
    private static final String TAG = "AndroidKeyStoreUtils";
    private String md5StrInfo;


    static AndroidKeyStoreUtils encryUtilsInstance;
    private KeyStore keyStore;
    private Context context;

    public static AndroidKeyStoreUtils getInstance() {
        synchronized (AndroidKeyStoreUtils.class) {
            if (null == encryUtilsInstance) {
                encryUtilsInstance = new AndroidKeyStoreUtils();
            }
        }
        return encryUtilsInstance;
    }

    public AndroidKeyStoreUtils() {
        try {
            FIXED_IV = (String) HttpCoreSpUtils.get(FIXED_IV_STR, INIT_FIXED_IV);
        } catch (Exception e) {
            if (HttpCoreKeyManager.getInstance().isDebug()) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
        }
        initKeyStore();
    }

    public void init(Context context) {
        this.context = context;
        initKeyStore();
    }

    private void initKeyStore() {
        try {
            keyStore = KeyStore.getInstance(AndroidKeyStore);
            keyStore.load(null);
            if (!keyStore.containsAlias(KEY_ALIAS)) {
                KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore);
                keyGenerator.init(new KeyGenParameterSpec.Builder(KEY_ALIAS, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT).
                        setBlockModes(KeyProperties.BLOCK_MODE_GCM).
                        setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE).
                        setRandomizedEncryptionRequired(false).
                        setKeySize(256).build());
                keyGenerator.generateKey();
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }

    private java.security.Key getSecretKey() throws Exception {
        return keyStore.getKey(KEY_ALIAS, null);
    }


    private String createSecretKeyFromSHA() {
        if (TextUtils.isEmpty(md5StrInfo)) {
            //防止多次加载 耗时
            String sha1Info = AppInfoUtils.getSingInfo(context, context.getPackageName(), SHA1);
            HttpLogUtils.i(TAG, "sha1Info: " + sha1Info);
            md5StrInfo = md5(sha1Info);
        }
        HttpLogUtils.i(TAG, "md5StrInfo: " + md5StrInfo);
        return md5StrInfo;
    }

    private String md5(String plainText) {
        String reMd5 = "";
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuilder buf = new StringBuilder("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0) {
                    i += 256;
                }
                if (i < 16) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(i));
            }

            reMd5 = buf.toString();

        } catch (Exception e) {
            if (BuildConfig.DEBUG){
                e.printStackTrace();
            }
        }
        return reMd5;
    }

    public String encryptData(String oringinalStr, boolean useOldSecretKeyType) {
        String encryptedBase64Encoded = null;
        if (useOldSecretKeyType) {
            try {
                Cipher c = Cipher.getInstance(AES_MODE);
                c.init(Cipher.ENCRYPT_MODE, getSecretKey(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
                byte[] encodedBytes = c.doFinal(oringinalStr.getBytes());
                encryptedBase64Encoded = Base64.encodeToString(encodedBytes, Base64.DEFAULT);
            } catch (Exception e) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
        } else {
            try {
                encryptedBase64Encoded = AESUtils.AESEncrypt(oringinalStr, createSecretKeyFromSHA());
            } catch (Exception e) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
        }
        return encryptedBase64Encoded;
    }


    public String decryptData(String oringinalStr, boolean useOldSecretKeyType) {
        if (useOldSecretKeyType) {
            byte[] decodedBytes = new byte[0];
            try {
                Cipher c = Cipher.getInstance(AES_MODE);
                c.init(Cipher.DECRYPT_MODE, getSecretKey(), new GCMParameterSpec(128, FIXED_IV.getBytes()));
                decodedBytes = c.doFinal(Base64.decode(oringinalStr.getBytes(), Base64.DEFAULT));
            } catch (Exception e) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
            return new String(decodedBytes, StandardCharsets.UTF_8);
        } else {
            String originStr = "";
            try {
                originStr = AESUtils.AESDecrypt(oringinalStr, createSecretKeyFromSHA());
            } catch (Exception e) {
                HttpLogUtils.e(TAG, Log.getStackTraceString(e));
            }
            return originStr;
        }

    }

    public static String getLocalSvrPubkey() {
        String pubKey = null;
        try {
            String encryptedKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.SVR_PUB_KEY_NEW, "");
            if (!TextUtils.isEmpty(encryptedKey)) {
                pubKey = AndroidKeyStoreUtils.getInstance().decryptData(encryptedKey, false);
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return pubKey;
    }

    public static void saveSvrPubKey(String pubKey) {
        String encryptedKey = AndroidKeyStoreUtils.getInstance().encryptData(pubKey, false);
        if (!TextUtils.isEmpty(encryptedKey)) {
            HttpCoreSpUtils.put(HttpCoreKeyManager.SVR_PUB_KEY_NEW, encryptedKey);
        }
    }

    public static String getLocalAppPrikey() {
        String priKey = null;
        try {
            String encryptedKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.APP_PRI_KEY_NEW, "");
            if (!TextUtils.isEmpty(encryptedKey)) {
                priKey = AndroidKeyStoreUtils.getInstance().decryptData(encryptedKey, false);
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return priKey;
    }

    public static void saveAppPriKey(String priKey) {
        String encryptedKey = AndroidKeyStoreUtils.getInstance().encryptData(priKey, false);
        HttpCoreSpUtils.put(HttpCoreKeyManager.APP_PRI_KEY_NEW, encryptedKey);
    }

    public static void clearLocalKey() {
        HttpCoreKeyManager.getInstance().setServerPubKey("");
        HttpCoreKeyManager.getInstance().setAppPriKey("");
        HttpCoreSpUtils.remove(HttpCoreKeyManager.SVR_PUB_KEY_NEW);
        HttpCoreSpUtils.remove(HttpCoreKeyManager.APP_PRI_KEY_NEW);
    }
}
