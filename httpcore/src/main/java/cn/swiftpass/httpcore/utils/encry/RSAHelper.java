package cn.swiftpass.httpcore.utils.encry;

import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;

import cn.swiftpass.httpcore.utils.HttpLogUtils;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.APP_PRIVATEKEY;
import static cn.swiftpass.httpcore.config.HttpCoreConstants.APP_PUBLICKEY;


public class RSAHelper {
    /**
     * 指定加密算法为RSA
     */
    private static final String RSA = "RSA";

    private static final String ALGORITHM = "RSA/None/PKCS1Padding";
    /**
     * 密钥长度，用来初始化
     */
    private static final int KEYSIZE = 2048;

    public static final String SIGNATURE_ALGORITHM = "SHA256withRSA";
    public static final String TAG = RSAHelper.class.getSimpleName();


    /**
     * 生成密钥对
     *
     * @throws Exception
     */
    public static Map<String, String> generateKeyPair() {
        Map<String, String> map = new HashMap<>();
        // /** RSA算法要求有一个可信任的随机数源 */
        // SecureRandom secureRandom = new SecureRandom();
        /** 为RSA算法创建一个KeyPairGenerator对象 */
        KeyPairGenerator keyPairGenerator = null;
        try {
            keyPairGenerator = KeyPairGenerator.getInstance(RSA);
            /** 利用上面的随机数据源初始化这个KeyPairGenerator对象 */
            // keyPairGenerator.initialize(KEYSIZE, secureRandom);
            keyPairGenerator.initialize(KEYSIZE);
            /** 生成密匙对 */
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            /** 得到公钥 */
            Key publicKey = keyPair.getPublic();
            /** 得到私钥 */
            Key privateKey = keyPair.getPrivate();
            map.put(APP_PUBLICKEY, Base64.encodeBytes(publicKey.getEncoded()));
            map.put(APP_PRIVATEKEY, Base64.encodeBytes(privateKey.getEncoded()));
        } catch (NoSuchAlgorithmException e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return map;
    }

    /**
     * 公钥加密方法
     *
     * @param source 源数据
     * @return
     */
    public static String encrypt(String source, String strPublicKey) throws Exception {
        if (TextUtils.isEmpty(strPublicKey)) {
            return null;
        }
        Key publicKey = getPublicKey(strPublicKey);
        /** 得到Cipher对象来实现对源数据的RSA加密 */
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] b = source.getBytes();
        /** 执行加密操作 */
        byte[] b1 = cipher.doFinal(b);
        return Base64.encodeBytes(b1);
    }

    /**
     * 公钥分段加密方法
     *
     * @param source 源数据
     * @return
     */
    public static String encryptPart(String source, String strPublicKey) throws Exception {
        if (TextUtils.isEmpty(strPublicKey)) {
            return null;
        }

        byte[] data = source.getBytes();
        /** 执行加密操作 */

        Key publicKey = getPublicKey(strPublicKey);
        /** 得到Cipher对象来实现对源数据的RSA加密 */
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        int inputLen = data.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;

        for (int i = 0; inputLen - offSet > 0; offSet = i * 244) {
            byte[] cache;
            if (inputLen - offSet > 244) {
                cache = cipher.doFinal(data, offSet, 244);
            } else {
                cache = cipher.doFinal(data, offSet, inputLen - offSet);
            }

            out.write(cache, 0, cache.length);
            ++i;
        }

        byte[] encryptedData = out.toByteArray();
        out.close();
        return Base64.encodeBytes(encryptedData);

    }

    /**
     * 转换公钥
     * <p>
     * String to PublicKey
     *
     * @throws Exception
     */
    public static PublicKey getPublicKey(String key) throws Exception {
        byte[] keyBytes = Base64.decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PublicKey publicKey = keyFactory.generatePublic(keySpec);
        return publicKey;
    }

    /**
     * 转换私钥
     * *            String to PrivateKey
     *
     * @throws Exception
     */
    public static PrivateKey getPrivateKey(String key) throws Exception {
        byte[] keyBytes;
        keyBytes = Base64.decode(key);
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }


    /**
     * 私钥解密算法
     *
     * @param cryptograph 密文
     * @return
     * @throws Exception
     */
    public static String decrypt(String cryptograph, String strPrivateKey) {
        if (TextUtils.isEmpty(cryptograph)) {
            return null;
        }
        Key privateKey;
        try {
            privateKey = getPrivateKey(strPrivateKey);
            /** 得到Cipher对象对已用公钥加密的数据进行RSA解密 */
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] b1 = Base64.decode(cryptograph);
            /** 执行解密操作 */
            byte[] b = cipher.doFinal(b1);
            return new String(b, "utf-8");
        } catch (Exception e) {
            HttpLogUtils.i(TAG, "RSAHelper:" + e.getLocalizedMessage());
            return null;
        }
    }


}
