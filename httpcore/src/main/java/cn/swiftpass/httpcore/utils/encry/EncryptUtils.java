package cn.swiftpass.httpcore.utils.encry;

import android.util.Log;

import java.security.MessageDigest;
import java.security.Signature;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.RandomUtils;


/**
 * Created by jamy on 2018/7/18.
 * 加密工具类主要处理数据的加密算法
 */

public class EncryptUtils {

    public static final String TAG = EncryptUtils.class.getSimpleName();

    /**
     * 入参 加签  客户端用RSA私钥加签   服务器用客户端RSA公钥验签
     *
     * @param data
     * @return
     */
    public static String getDigestSign(String data) {
        try {
            MessageDigest sha256Digest = DigestUtils.getSha256Digest();
            sha256Digest.update(data.getBytes());
            // 完成哈希计算，得到摘要
            byte[] shaEncoded = sha256Digest.digest();
            //使用摘要，使用SHA256withRSA加签 完成签名，返回byte数组使用base64编码
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(RSAHelper.getPrivateKey(HttpCoreKeyManager.getInstance().getAppPriKey()));
            signature.update(shaEncoded);
            String sign = Base64.encodeBytes(signature.sign());
            return sign;
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    /**
     * 获得加密 加密处理
     *
     * @return
     */
    public static String getEncryptionToken(String token, String pubKey) {
        HttpLogUtils.i(TAG, "pubKey: " + pubKey);
        try {
            return RSAHelper.encryptPart(token, pubKey);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    public static String getAESKey() {
        return RandomUtils.getNumSmallLetter(16);
    }

}
