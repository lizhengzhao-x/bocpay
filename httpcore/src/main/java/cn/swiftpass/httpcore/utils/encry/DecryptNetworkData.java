package cn.swiftpass.httpcore.utils.encry;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import cn.swiftpass.httpcore.api.protocol.BaseProtocol;
import cn.swiftpass.httpcore.api.protocol.IProtocol;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.HttpsUtils;

/**
 * 从第二层开始解析数据 contentEntity  第一层数据在拦截器解析 CommonEntity
 */
public class DecryptNetworkData {

    public static final String TAG = BaseProtocol.class.getSimpleName();

    private Gson mGson;

    /**
     * 本地生成上传给服务器的时间戳
     */
    private String timetamp;

    public DecryptNetworkData() {
        this.mGson = new Gson();
    }

    public void setTimetamp(String timetamp) {
        this.timetamp = timetamp;
    }

    /**
     * @param response
     * @param protocol
     * @param type     代表class   Class实现了Type接口
     */
    public boolean decryptResponse(String response, IProtocol protocol, Type type) {
        try {
            ContentEntity contentEntity = mGson.fromJson(response, ContentEntity.class);
            if (ResponseCode.RESPONSE_SUCCESS.equals(contentEntity.getResult_code())) {
                onBusinessSuc(contentEntity, protocol, type);
                return true;
            } else {
                //后台Bug  可能存在返回第二层无内容情况  即contentEntity为Null 本地
                protocol.onFailCallback(contentEntity.getResult_code(), contentEntity.getResult_msg());
                return false;
            }
        } catch (Exception e) {
            //TODO 这里需要自定义一个解析code 以便快速定位问题
            Log.e(TAG,"ERROR:"+e.getLocalizedMessage());
            e.printStackTrace();
            protocol.onFailCallback(ErrorCode.NULL_RESPONSE.code, getErrorString(ErrorCode.NULL_RESPONSE));
            return false;
        }
    }

    private String getErrorString(ErrorCode errorCode) {
        return HttpsUtils.getErrorString(errorCode);
    }

    private void onBusinessSuc(ContentEntity contentEntity, IProtocol protocol, Type type) {
        if (!protocol.isEncrypt()) {
            Object o = null;
            //如果解析data为空则返回contentEntity
            if (!TextUtils.isEmpty(contentEntity.getData())) {
                try {
                    o = mGson.fromJson(contentEntity.getData(), type);
                } catch (Exception e) {
                    protocol.onFailCallback(ErrorCode.NULL_CONTENT_ENTITY.code, getErrorString(ErrorCode.NULL_CONTENT_ENTITY));
                    return;
                }
            } else {
                o = contentEntity;
            }
            protocol.onSuccessCallback(o);
            return;
        } else {
            String sign = contentEntity.getSign();
            String token = contentEntity.getToken();
            String serverTimestamp = contentEntity.getTimestamp();
            //获取到的服务器加密data解析
            String data = contentEntity.getData();
            if (!TextUtils.isEmpty(data)) {
                //本地生成RSA公私钥 请求服务器公钥接口时将客户端公钥上传到服务器 服务器用该公钥加密解析数据用的服务器AES私钥即token返回给客户端
                //客户端用本地RSA私钥解密该token拿到服务器AES秘钥 解密服务器返回的数据
//                HttpLogUtils.i(TAG, "getAppPriKey: " + HttpCoreKeyManager.getInstance().getAppPriKey());
                String token1 = RSAHelper.decrypt(token, HttpCoreKeyManager.getInstance().getAppPriKey());
                if (TextUtils.isEmpty(token1)) {
                    HttpLogUtils.i(TAG, "data1:error 01 " + "token1:" + token1);
                    //本地私钥解密服务器私钥解密失败,返回数据请求失败
                    protocol.onFailCallback(ErrorCode.DECRYPT_KEY_FAIL.code, getErrorString(ErrorCode.DECRYPT_KEY_FAIL));
                    return;
                }
                //使用服务端返回的AES秘钥解密数据  判断本地上传的时间戳和解密服务器返回时间戳是否一致
                String data1 = null;
                try {
                    if (!TextUtils.isEmpty(timetamp) && timetamp.equals(AESUtils.AESDecrypt(serverTimestamp, token1))) {
                        data1 = AESUtils.AESDecrypt(data, token1);
                    } else {

                        protocol.onFailCallback(ErrorCode.VERIFY_SERVER_TIME_TAMP_FAIL.code, getErrorString(ErrorCode.VERIFY_SERVER_TIME_TAMP_FAIL));
                        String s = AESUtils.AESDecrypt(serverTimestamp, token1);
                        HttpLogUtils.i(TAG, "data1:error 02 ");
                        return;
                    }

                } catch (Exception e) {
                    HttpLogUtils.d(TAG, e.getMessage());
                }
                HttpLogUtils.i(TAG, "data1: " + data1);
                if (TextUtils.isEmpty(data1)) {
                    //私钥解密失败,返回数据请求失败
                    HttpLogUtils.i(TAG, "data1:error 03 ");
                    protocol.onFailCallback(ErrorCode.DECRYPT_DATA_FAIL.code, getErrorString(ErrorCode.DECRYPT_DATA_FAIL));
                    return;
                }
                boolean isverifySuccess = protocol.verifySign(sign, data1);
                //验签
                HttpLogUtils.i(TAG, "验签结果:" + isverifySuccess);
                if (!isverifySuccess) {
                    //验签失败
                    protocol.onFailCallback(ErrorCode.VERIFY_SIGN_FAIL.code, getErrorString(ErrorCode.VERIFY_SIGN_FAIL));
                    return;
                }

                Object result = null;
                Object tmp = mGson.fromJson(data1, type);
                if (tmp instanceof ContentEntity) {
                    result = contentEntity;
                } else {
                    result = tmp;
                }
                protocol.onSuccessCallback(result);

            } else {
                try {
                    Object o = contentEntity;
                    protocol.onSuccessCallback(o);
                } catch (Exception e) {
                    HttpLogUtils.d(TAG, e.getMessage());
                }
                return;
            }
        }
    }
}
