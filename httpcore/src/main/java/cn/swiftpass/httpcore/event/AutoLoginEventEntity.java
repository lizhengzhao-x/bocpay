package cn.swiftpass.httpcore.event;


import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.entity.BaseEntity;

public class AutoLoginEventEntity extends BaseEntity {

    public static final int EVENT_AUTO_LOGIN_SUCCESS = 1001;
    public static final int EVENT_AUTO_LOGIN_FAIL = 1002;
    private int eventType;
    private String eventMessage;
    private String errorCode;
    private AutoLoginSucEntity autoLoginSucEntity;

    public AutoLoginEventEntity(int eventType, String message) {

        this.eventType = eventType;
        this.eventMessage = message;
    }

    public AutoLoginEventEntity(int eventType, String message, String errorCodeIn) {

        this.eventType = eventType;
        this.eventMessage = message;
        this.errorCode = errorCodeIn;
    }

    public AutoLoginEventEntity(int eventType, String message, AutoLoginSucEntity autoLoginSucEntity) {
        this.eventType = eventType;
        this.eventMessage = message;
        this.autoLoginSucEntity = autoLoginSucEntity;
    }

    public int getEventType() {
        return eventType;
    }

    public void setEventType(int eventType) {
        this.eventType = eventType;
    }

    public String getEventMessage() {
        return eventMessage;
    }

    public void setEventMessage(String eventMessage) {
        this.eventMessage = eventMessage;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public AutoLoginSucEntity getAutoLoginSucEntity() {
        return autoLoginSucEntity;
    }

    public void setAutoLoginSucEntity(AutoLoginSucEntity autoLoginSucEntity) {
        this.autoLoginSucEntity = autoLoginSucEntity;
    }
}
