package cn.swiftpass.httpcore.manager;

import android.text.TextUtils;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.entity.AccountPointDataEntity;
import cn.swiftpass.httpcore.entity.BankCardEntity;
import cn.swiftpass.httpcore.entity.MySmartAccountEntity;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;

/**
 * 所有内存缓存处理
 */

public class CacheManagerInstance {


    public static final String ACCOUNT_TYPE_CREDIT = "1";
    public static final String ACCOUNT_TYPE_SMART_ACCOUNT = "2";


//    /**
//     * 注册成功是否需要显示迎新banner
//     */
//    private String registerPopupBannerUrl;
//
//    public boolean isShowNotificationCenter() {
//        return showNotificationCenter;
//    }
//
//    public void setShowNotificationCenter(boolean showNotificationCenter) {
//        this.showNotificationCenter = showNotificationCenter;
//    }

//    /**
//     * 新注册显示消息弹框 主要解决一键绑定流程太长 没有通过intent携带
//     */
//    private boolean showNotificationCenter;

    /**
     * 卡列表
     */
    private List<BankCardEntity> cardEntities = new ArrayList<>();
    //包括空卡面
    private ArrayList<BankCardEntity> cardAllEntities = new ArrayList<>();
    @Nullable
    private AccountPointDataEntity accountPointData;
    @Nullable
    private MySmartAccountEntity mySmartAccountEntity;

    /**
     * 是否开启生物认证
     *
     * @return
     */
    public boolean isOpenBiometricAuth() {
        return openBiometricAuth;
    }

    /**
     * 生物认证开关
     */
    private boolean openBiometricAuth = true;

    public List<BankCardEntity> getCardEntities() {
        if (cardEntities == null) {
            cardEntities = new ArrayList<>();
        }
        return cardEntities;
    }

    private boolean isSmartAccount = false;


    public MySmartAccountEntity getMySmartAccountEntity() {
        return mySmartAccountEntity;
    }

    public void setMySmartAccountEntity(MySmartAccountEntity mySmartAccountEntity) {
        this.mySmartAccountEntity = mySmartAccountEntity;
    }

    public boolean isSmartAccount() {
        return isSmartAccount;
    }

    public void setSmartAccount(boolean smartAccount) {
        isSmartAccount = smartAccount;
    }

    public void setAccountPointData(AccountPointDataEntity accountData) {
        this.accountPointData = accountData;
    }

    /**
     * 使用单例的原因，要求切换语言时，不能出现界面刷新，mainActivty销毁后，数据需要保留
     *
     * @return
     */
    public AccountPointDataEntity getAccountPointData() {
        return accountPointData;
    }

    /**
     * 获取默认卡
     *
     * @return
     */
    public BankCardEntity getDefaultCardEntitity() {
        if (cardEntities == null || cardEntities.size() == 0) {
            return null;
        }
        for (int i = 0; i < cardEntities.size(); i++) {
            if (TextUtils.equals("1", cardEntities.get(i).getIsDefault())) {
                return cardEntities.get(i);
            }
        }

        return null;
    }

    public BankCardEntity getOtherNewDefaultCardEntitity() {
        if (cardEntities == null || cardEntities.size() == 0) {
            return null;
        }

        for (int i = 0; i < cardEntities.size(); i++) {
            if (!TextUtils.equals("1", cardEntities.get(i).getIsDefault())) {
                return cardEntities.get(i);
            }
        }
        return null;
    }

    public void setCardEntities(List<BankCardEntity> cardEntitiesIn) {
        if (cardEntitiesIn == null || cardEntitiesIn.size() == 0) {
            this.cardEntities = cardEntitiesIn;
        } else {
            if (this.cardEntities == null) {
                this.cardEntities = new ArrayList<>();
            } else {
                this.cardEntities.clear();
            }
            for (BankCardEntity cardEntity : cardEntitiesIn) {
                //非空信用卡或非空智能账户
                if (cardEntity.getCardFlag() != 1 && cardEntity.getCardFlag() != 2) {
                    this.cardEntities.add(cardEntity);
                }
            }
        }

        isSmartAccount = false;
        if (this.cardEntities != null) {
            for (int i = 0; i < this.cardEntities.size(); i++) {
                BankCardEntity cardEntity = this.cardEntities.get(i);
                if (TextUtils.equals(cardEntity.getCardType(), ACCOUNT_TYPE_SMART_ACCOUNT)) {
                    isSmartAccount = true;
                    return;
                }
            }
        }

    }

    public void setCardAllEntities(ArrayList<BankCardEntity> cardEntitiesIn) {
        if (cardEntitiesIn == null || cardEntitiesIn.size() == 0) {
            this.cardAllEntities = cardEntitiesIn;
        } else {
            if (this.cardAllEntities == null) {
                this.cardAllEntities = new ArrayList<>();
            } else {
                this.cardAllEntities.clear();
            }
            this.cardAllEntities.addAll(cardEntitiesIn);
        }

    }

    public ArrayList<BankCardEntity> getCardAllEntities() {
        return cardAllEntities;
    }

    public void saveOpenBiometricAuth(boolean openBiometricAuth) {
        this.openBiometricAuth = openBiometricAuth;
    }


    private static class ApiManagerHolder {
        private static CacheManagerInstance instance = new CacheManagerInstance();
    }

    public static CacheManagerInstance getInstance() {
        return ApiManagerHolder.instance;
    }


    //    /**
//     * 用户是否登录过
//     *
//     * @param context
//     * @return
//     */
    public boolean isLogin() {
        return (boolean) HttpCoreSpUtils.get(HttpCoreConstants.CACHE_IS_LOGIN, false);
    }

    /**
     * 判断登录态通用方法
     *
     * @return
     */
    public boolean isLoginStatus() {
        return isLogin() && isHasSessionId();
    }


    /**
     * 用户是否登录
     *
     * @return
     */
    public boolean isHasSessionId() {
        return !TextUtils.isEmpty(HttpCoreKeyManager.getInstance().
                getSessionId());
    }


    /**
     * 保存用户登录状态
     */
    public void saveLoginStatus() {
        HttpCoreSpUtils.put(HttpCoreConstants.CACHE_IS_LOGIN, true);
    }

    /**
     * 保存用户登录状态
     */
    public void saveLocationInHongKong(boolean isIn) {
        HttpCoreSpUtils.put(HttpCoreConstants.CACHE_IS_HONGHK, isIn);
    }

    public boolean currentLocationInHongKong() {
        return (boolean) HttpCoreSpUtils.get(HttpCoreConstants.CACHE_IS_HONGHK, true);
    }

    /**
     * 保存用户登出状态
     */
    public void saveLogoutStatus() {
        HttpCoreKeyManager.getInstance().saveSessionId("");
        HttpCoreSpUtils.put(HttpCoreConstants.CACHE_IS_LOGIN, false);
    }

    public boolean checkLoginStatus() {
        boolean isLogin = CacheManagerInstance.getInstance().isLogin() && CacheManagerInstance.getInstance().isHasSessionId();
        return isLogin;
    }


    /**
     * 是否开启指纹
     *
     * @return
     */
    public boolean isOpenFingerPrint() {
        return (boolean) HttpCoreSpUtils.get(HttpCoreConstants.CACHE_FINGER_PRINT, false);
    }

    /**
     * 是否已经显示过引导页
     *
     * @return
     */
    public boolean isShowGuidePage() {
        return (boolean) HttpCoreSpUtils.get(HttpCoreConstants.CACHE_SHOW_GUIDE, false);
    }

    public void setShowGuidePage(boolean isOpen) {
        HttpCoreSpUtils.put(HttpCoreConstants.CACHE_SHOW_GUIDE, isOpen);
    }

//    public String registetPopupBannerUrl() {
//        return registerPopupBannerUrl;
//    }
//
//    public void setRegisterPopupBannerUrl(String registerPopupBannerUrl) {
//        this.registerPopupBannerUrl = registerPopupBannerUrl;
//    }
//
//    public void clearDialogEvent() {
//        CacheManagerInstance.getInstance().setRegisterPopupBannerUrl(null);
//        CacheManagerInstance.getInstance().setShowNotificationCenter(false);
//    }


}
