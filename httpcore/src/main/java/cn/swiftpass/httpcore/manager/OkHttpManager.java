package cn.swiftpass.httpcore.manager;

import android.util.Log;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import cn.swiftpass.httpcore.api.interceptor.AutoLoginRequestInterceptor;
import cn.swiftpass.httpcore.api.interceptor.DecryptResponseInterceptor;
import cn.swiftpass.httpcore.api.interceptor.EncryptRequestInterceptor;
import cn.swiftpass.httpcore.api.interceptor.PublicKeyInterceptor;
import cn.swiftpass.httpcore.api.interceptor.RsaUpdateRequestInterceptor;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.Platform;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class OkHttpManager {
    public static final int CONNECT_TIME_OUT = 75;
    public static final int READ_TIME_OUT = 75;
    public static final int WRITE_TIME_OUT = 75;

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

    private OkHttpClient mOkHttpClient;

    public OkHttpClient getOkHttpClientNoInterceptor() {
        return mOkHttpClientNoInterceptor;
    }

    private OkHttpClient mOkHttpClientNoInterceptor;
    private static final String TAG = "OkHttpManager";
    public static final MediaType JSON = MediaType.parse("application/json;charset=utf-8");

    /**
     * 回调的handler
     */
    private Platform mDelivery;

    enum HttpMethodType {
        GET, POST
    }

    private static OkHttpManager okHttpUtilsInstance;

    /**
     * 创建 单例模式（OkHttp官方建议如此操作）
     */
    public static OkHttpManager getInstance() {
        if (okHttpUtilsInstance == null) {
            okHttpUtilsInstance = new OkHttpManager();
        }
        return okHttpUtilsInstance;
    }


    private OkHttpManager() {
        try {
            mOkHttpClient = getBuilder(true).connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS).readTimeout(READ_TIME_OUT, TimeUnit.SECONDS).writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS).build();
            mOkHttpClientNoInterceptor = getBuilder(false).
                    connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS).
                    readTimeout(READ_TIME_OUT, TimeUnit.SECONDS).
                    writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS).
                    build();
            mDelivery = Platform.get();
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }

    }

    /**
     * 添加自定义拦截器  拦截器处理request顺序按依赖关系排序
     *
     * @return
     */
    public OkHttpClient.Builder getBuilder(boolean isAddInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (isAddInterceptor) {
            builder.addInterceptor(new AutoLoginRequestInterceptor());
            builder.addInterceptor(new RsaUpdateRequestInterceptor());
            builder.addInterceptor(new PublicKeyInterceptor());
            builder.addInterceptor(new EncryptRequestInterceptor());
            builder.addInterceptor(new DecryptResponseInterceptor());
        }
        try {
            SSLContext sslContext = SSLContext.getInstance("TLS");
            final TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(HttpsUtils.readKeyStore(HttpCoreKeyManager.getInstance().getHttpCertsName()));
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            builder.sslSocketFactory(sslContext.getSocketFactory());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
        } catch (Exception e) {
            HttpLogUtils.d(TAG, e.getMessage());
        }
        return builder;
    }


    private void onSuccessCallback(final String object, final OkHttpCallback callback, boolean isAsyn) {
        if (isAsyn) {
            mDelivery.post(new Runnable() {
                @Override
                public void run() {
                    if (callback != null) {
                        callback.onSuccess(object);
                    }
                }
            });
        } else {
            if (callback != null) {
                callback.onSuccess(object);
            }
        }
    }


    private void onFailCallback(final ErrorCode errorCode, final OkHttpCallback callback, boolean isAsyn) {
        if (isAsyn) {
            mDelivery.post(new Runnable() {
                @Override
                public void run() {
                    if (callback != null) {
                        callback.onFailure(errorCode.code, HttpsUtils.getErrorString(errorCode));
                    }
                }
            });
        } else {
            if (callback != null) {
                callback.onFailure(errorCode.code, HttpsUtils.getErrorString(errorCode));
            }
        }

    }


    /**
     * @param url      请求url
     * @param callback 回调
     */
    public void doPostAsyn(final String url, final String postData, final MediaType mediaType, final OkHttpCallback callback) {
        Request request = makePostRequest(url, postData, mediaType);
        doRequest(request, callback);

    }

    public void doPostNoAsyn(final String url, final String postData, final MediaType mediaType, final OkHttpCallback callback) {
        Request request = makePostRequest(url, postData, mediaType);
        doRequestNotMainThread(request, callback);
    }

    /**
     * 同步post请求
     *
     * @param url
     * @param headMap
     * @param postData
     * @return
     */
    public NetWorkEntity doPostSyn(String url, HashMap<String, Object> headMap, String postData) {
        Request request = makePostRequest(url, headMap, postData);
        return doRequestSyn(request);
    }

    /**
     * 异步post请求
     *
     * @param url
     * @param headMap
     * @param postData
     * @param callback
     */
    public void doPostAsyn(final String url, HashMap<String, Object> headMap, final String postData, final OkHttpCallback callback) {
        Request request = makePostRequest(url, headMap, postData);
        doRequest(request, callback);
    }

    /**
     * 异步get请求
     *
     * @param url
     * @param headMap
     * @param callback
     */
    public void doGetAsyn(final String url, HashMap<String, Object> headMap, final OkHttpCallback callback) {
        Request request = getHttpBuilder(headMap).get().url(url).build();
        doRequest(request, callback);
    }

    /**
     * 创建request
     *
     * @param url
     * @param headMap
     * @param postData
     * @return
     */
    public Request makePostRequest(String url, HashMap<String, Object> headMap, String postData) {
        RequestBody requestBody = RequestBody.create(OkHttpManager.JSON, postData);
        Request request = getHttpBuilder(headMap).post(requestBody).url(url).build();
        return request;
    }

    /**
     * 创建builder 添加header内容
     *
     * @param headMap
     * @return
     */
    private Request.Builder getHttpBuilder(HashMap<String, Object> headMap) {
        Request.Builder builder = new Request.Builder()
                .addHeader("Connection", "keep-alive");
        if (headMap != null && headMap.size() > 0) {
            for (Map.Entry<String, Object> set : headMap.entrySet()) {
                builder.addHeader(set.getKey(), set.getValue() != null ? set.getValue().toString() : "");
            }
        }
        return builder;
    }

    /**
     * 同步请求获取response
     *
     * @param request
     * @return
     */
    public NetWorkEntity doRequestSyn(Request request) {
        try {
            Response response = mOkHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                if (response.body() != null) {
                    return new NetWorkEntity(true, response);
                }
            } else {
                return new NetWorkEntity(false, response);
            }
        } catch (IOException e) {
            HttpLogUtils.d(TAG, e.getMessage());
        }
        return new NetWorkEntity(false, null);
    }

    private Request makePostRequest(String url, String jsonContent, MediaType mediaType) {
        RequestBody body = RequestBody.create(mediaType, jsonContent);
        Request request = addHttpHeaders().url(url).post(body).build();
        return request;
    }

    public void doGetAsyn(final String url, final MediaType mediaType, final OkHttpCallback callback) {
        Request request = addHttpHeaders().get().url(url).build();
        doRequest(request, callback);
    }

    private Request.Builder addHttpHeaders() {
        Request.Builder builder = new Request.Builder()
                .addHeader("Connection", "keep-alive");
        return builder;
    }

    private void doRequestNotMainThread(Request request, final OkHttpCallback callback) {
        try {
            Response response = mOkHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                try {
                    final String string = response.body().string();
                    onSuccessCallback(string, callback, false);
                } catch (Exception e) {
                    // TODO 需协商异常code
                    onFailCallback(ErrorCode.SERVER_INTER_ERROR, callback, false);
                }
            } else {
                onFailCallback(ErrorCode.CONTENT_TIME_OUT, callback, false);
            }

        } catch (IOException e) {
            HttpLogUtils.d(TAG, e.getMessage());
        }

    }

    /**
     * 开始加密数据请求
     *
     * @param
     * @param callback
     */
    private void doRequest(Request request, final OkHttpCallback callback) {
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call request, IOException e) {
                HttpLogUtils.i(TAG, "response IOException" + e);
                if (e instanceof SocketTimeoutException) {
                    onFailCallback(ErrorCode.CONTENT_TIME_OUT, callback, true);
                } else if (e instanceof ConnectException) {
                    onFailCallback(ErrorCode.NETWORK_ERROR, callback, true);
                } else {
                    onFailCallback(ErrorCode.SERVER_INTER_FAILED, callback, true);
                }
            }

            @Override
            public void onResponse(Call call, Response response) {
                try {
                    final String string = response.body().string();
                    onSuccessCallback(string, callback, true);
                } catch (Exception e) {
                    // TODO 需协商异常code
                    onFailCallback(ErrorCode.SERVER_INTER_ERROR, callback, true);
                }
            }
        });
    }


    public interface OkHttpCallback {
        /**
         * 响应成功
         */
        void onSuccess(String response);

        /**
         * 响应失败
         */
        void onFailure(String errorCode, String errorMsg);
    }

}
