package cn.swiftpass.httpcore.manager;


import cn.swiftpass.httpcore.api.protocol.RSAUpdateProtocol;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.utils.HttpLogUtils;

/**
 * @author zhangfan
 * 单例保证 多线程下 rsa升级
 */

public class RsaUpdateManager {

    private static final String TAG = "RsaUpdateManagerInstance";
    private static RsaUpdateManager instance;
    private static final Object INSTANCE_LOCK = new Object();


    private RsaUpdateManager() {

    }

    public static RsaUpdateManager getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new RsaUpdateManager();
                }
            }
        }
        return instance;
    }


    /**
     * 释放内存
     */
    public static void clear() {
        instance = null;
    }


    public synchronized NetWorkEntity getUpdateRsaFinish() {

        if (SystemInitManager.getInstance().getUpdateRsaFinish() == SystemInitManager.RSA_UPDATE_DONE) {
            HttpLogUtils.i(TAG, "getUpdateRsaFinish----> rsa update done return ");
            return new NetWorkEntity(true, null);
        }

        return new RSAUpdateProtocol().execute();
    }


}
