package cn.swiftpass.httpcore.manager;

/**
 * 权限特殊处理 很多场景都要求自定义弹框一次
 */
public class PermissionAskManager {

    private static class PermissionAskManagerHolder {
        private static PermissionAskManager instance = new PermissionAskManager();
    }

    public static PermissionAskManager getInstance() {
        return PermissionAskManager.PermissionAskManagerHolder.instance;
    }


    public boolean ismCameraPermissionAskClicked() {
        return mCameraPermissionAskClicked;
    }

    public void setmCameraPermissionAskClicked(boolean mCameraPermissionAskClicked) {
        this.mCameraPermissionAskClicked = mCameraPermissionAskClicked;
    }

    private boolean mCameraPermissionAskClicked;


}
