package cn.swiftpass.httpcore.manager;


import cn.swiftpass.httpcore.entity.NetWorkEntity;

/**
 * 自动登录结果监听接口
 */
public abstract class AutoLoginListener {

    NetWorkEntity result=null;

   public abstract NetWorkEntity onSuccess(NetWorkEntity netWorkEntity);

    public abstract  NetWorkEntity onFail(NetWorkEntity netWorkEntity);
}
