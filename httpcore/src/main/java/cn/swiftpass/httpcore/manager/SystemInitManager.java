package cn.swiftpass.httpcore.manager;


import java.util.ArrayList;

import cn.swiftpass.httpcore.entity.GreetingEntity;

public class SystemInitManager {

    private static SystemInitManager sInstance;
    private static final Object INSTANCE_LOCK = new Object();

    public static int RSA_UPDATE_DONE = 1;
    public static int RSA_UPDATE_NOT_FINISHED = 0;

    //积分显示无需验密有效期中
    private boolean validShowPoint;
    //账户显示无需验密有效期中
    private boolean validShowAccount;

    private long serverHideTime;
    private long nowHideTime;

    private SystemInitManager() {
        init();
    }

    private void init() {

    }

    public static SystemInitManager getInstance() {
        if (sInstance == null) {
            synchronized (INSTANCE_LOCK) {
                if (sInstance == null) {
                    sInstance = new SystemInitManager();
                }
            }
        }
        return sInstance;
    }


    private int updateRsaFinish = RSA_UPDATE_NOT_FINISHED;


    public boolean isValidShowAccount() {
        return validShowAccount;
    }

    public void setValidShowAccount(boolean validShowAccount) {
        this.validShowAccount = validShowAccount;
    }

    public int getUpdateRsaFinish() {
        return updateRsaFinish;
    }

    public void setUpdateRsaFinish(int updateRsaFinish) {
        this.updateRsaFinish = updateRsaFinish;
    }

    private ArrayList<GreetingEntity> greetings;


    public ArrayList<GreetingEntity> getGreetings() {
        return greetings;
    }

    public void setGreetings(ArrayList<GreetingEntity> greetings) {
        this.greetings = greetings;
    }

    private String  postLoginBgImageVer;
    private String  preLoginBgImageVer;

    public String getPostLoginBgImageVer() {
        return postLoginBgImageVer;
    }

    public void setPostLoginBgImageVer(String postLoginBgImageVer) {
        this.postLoginBgImageVer = postLoginBgImageVer;
    }

    public String getPreLoginBgImageVer() {
        return preLoginBgImageVer;
    }

    public void setPreLoginBgImageVer(String preLoginBgImageVer) {
        this.preLoginBgImageVer = preLoginBgImageVer;
    }



    public long getServerHideTime() {
        return serverHideTime;
    }

    public void setServerHideTime(long serverHideTime) {
        this.serverHideTime = serverHideTime;
    }

    public long getNowHideTime() {
        return nowHideTime;
    }

    public void setNowHideTime(long nowHideTime) {
        this.nowHideTime = nowHideTime;
    }


}
