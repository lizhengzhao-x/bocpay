package cn.swiftpass.httpcore.manager;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.util.Map;

import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.encry.AndroidKeyStoreUtils;
import cn.swiftpass.httpcore.utils.encry.RSAHelper;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.FIXED_IV_STR;
import static cn.swiftpass.httpcore.config.HttpCoreConstants.INIT_FIXED_IV;

/**
 * 核心库 内存中管理公私钥 登录态
 */
public class HttpCoreKeyManager {


    private static final String TAG = "HttpCoreKeyManager";
    public static String SVR_PUB_KEY;

    public static String APP_PRI_KEY;

    public static String SVR_PUB_KEY_NEW;

    public static String APP_PRI_KEY_NEW;


    /**
     * log 日志
     */
    private boolean isDebug = true;

    /**
     * 是否支持E2EE
     */
    private boolean e2ee = true;
    /**
     * http baseurl
     */
    private String baseUrl = "";
    /**
     *
     */
    private String httpCertsName = "";

    private static HttpCoreKeyManager instance;

    private String packageId;


//    private String deviceToken;


    private String currentDeviceId;

    /**
     * 客户端sessionid
     */
    private String sessionId = "";


    /**
     * 客户端UserID
     */
    private String mUserId = "";


    /**
     * 客户端私钥
     */
    private String appPriKey = "";


    /**
     * 客户端公钥
     */
    private String appPubKey = "";
    /**
     * 服务端公钥
     */
    private String serverPubKey = "";


    private String mGcmDeviceToken = "";



    /**
     * app当前的版本号
     */
    private String mAppVersion ;


    private int mTimeOutStr;
    private int mNetWorkErrorStr;
    private int mFioRegFailedResId;

    private int mFioAuthFailedResId;
    private int mFioDeregFailResId;

    public String getAppVersion() {
        return mAppVersion;
    }

    public void setmContext(Context mContext) {
        this.mContext = mContext;
    }

    private Context mContext;

    private HttpRequestCallBack httpRequestCallBack;

    private static final Object INSTANCE_LOCK = new Object();

    private HttpCoreKeyManager() {

    }

    /**
     * App 初始化传参 需要提前初始化
     *
     * @param mContext
     * @param isDebug
     * @param baseUrlIn
     * @param e2ee
     * @param certName
     * @param packageIdIn
     */
    public void initGlobalContext(Context mContext, boolean isDebug, String baseUrlIn, boolean e2ee, String certName, String packageIdIn,String appVersion) {
        this.packageId = packageIdIn;
        this.isDebug = isDebug;
        this.baseUrl = baseUrlIn;
        this.e2ee = e2ee;
        this.httpCertsName = certName;
        this.mContext = mContext;
        this.mAppVersion = appVersion;
        SVR_PUB_KEY = "svr_pub_key" + baseUrl.hashCode();
        APP_PRI_KEY = "app_pri_key" + baseUrl.hashCode();
        SVR_PUB_KEY_NEW = "svr_pub_key_new" + baseUrl.hashCode();
        APP_PRI_KEY_NEW = "app_pri_key_new" + baseUrl.hashCode();
        AndroidKeyStoreUtils.getInstance().init(mContext);
        HttpCoreSpUtils.initContext(mContext);
        changeNewSecretKey();
        updateRsaStatus();
        try {
            //AndroidKeyStoreUtils google play 扫描此字符串不能硬编码 临时方案规避
            Object fIXED_IV = HttpCoreSpUtils.get(FIXED_IV_STR, null);
            if (fIXED_IV == null) {
                HttpCoreSpUtils.put(FIXED_IV_STR, INIT_FIXED_IV);
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
    }


    /**
     * http 通用错误码的顶层拦截
     *
     * @param mContextIn
     * @param baseUrl             http请求的baseURL
     * @param e2ee                验密接口是否E2EE
     * @param certName            https 证书校验
     * @param packageIdIn
     * @param isDebug             log 日志是否打印
     * @param timeOutResId        网络超时 提示string
     * @param netWorkErrorResId   网络异常 提示string
     * @param fioRegFailedResId   fio认证失败提示
     * @param fioAuthFailedResId  fio 认证失败提示
     * @param fioDeregFailResId
     * @param httpRequestCallBack http请求通用code返回提示
     */
    public void initHttpCallBack(Context mContextIn, int timeOutResId, int netWorkErrorResId, int fioRegFailedResId, int fioAuthFailedResId, int fioDeregFailResId, HttpRequestCallBack httpRequestCallBack) {
        this.mContext = mContextIn;
        this.mTimeOutStr = timeOutResId;
        this.mNetWorkErrorStr = netWorkErrorResId;
        this.mFioRegFailedResId = fioRegFailedResId;
        this.mFioAuthFailedResId = fioAuthFailedResId;
        this.mFioDeregFailResId = fioDeregFailResId;
        this.httpRequestCallBack = httpRequestCallBack;
    }


    public static HttpCoreKeyManager getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new HttpCoreKeyManager();
                }
            }
        }
        return instance;
    }

    public String getDeviceToken() {
        return mGcmDeviceToken;
    }


    /**
     * 加这个方法主要处理app升级情况的兼容问题，因为如果新安装这个版本的app 就无需考虑兼容。
     */
    public void updateRsaStatus() {
        //TODO 看一下这里改如何处理
        //获取当前SP里头存储的步进值
        int rsaUpdateFinish = HttpCoreSpUtils.getRSAUpdateFinish();
        String serverPubKey = (String) HttpCoreSpUtils.get(SVR_PUB_KEY_NEW, "");
        if (TextUtils.isEmpty(serverPubKey)) {
            //如果公钥为空不需要走RSA升级，直接使用2048 一般情况为新版本卸载安装/或者是旧版本根本没有经过秘钥交换接口升级到了新版本
            SystemInitManager.getInstance().setUpdateRsaFinish(SystemInitManager.RSA_UPDATE_DONE);
            HttpCoreSpUtils.setRSAUpdateFinish(SystemInitManager.RSA_UPDATE_DONE);
        } else {
            SystemInitManager.getInstance().setUpdateRsaFinish(rsaUpdateFinish);
        }
    }


    /**
     * 该方法用于更换新旧加密方式，添加时间为2020/06/05，
     * 多期以后，旧版本用户不在使用后，可以移除这块逻辑
     * 目前app只存储了三个参数 deviceId/app private key/ server public key
     */
    private void changeNewSecretKey() {
        String uuid = (String) HttpCoreSpUtils.get(HttpCoreConstants.DATA_STR_UUID, "");
        if (!TextUtils.isEmpty(uuid)) {
            changeNewSecretKey(HttpCoreConstants.DATA_STR_UUID, HttpCoreConstants.DATA_STR_UUID_NEW);
        }
        String serverPubKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.SVR_PUB_KEY, "");
        if (!TextUtils.isEmpty(serverPubKey)) {
            changeNewSecretKey(HttpCoreKeyManager.SVR_PUB_KEY, HttpCoreKeyManager.SVR_PUB_KEY_NEW);
        }
        String appPriKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.APP_PRI_KEY, "");
        if (!TextUtils.isEmpty(appPriKey)) {
            changeNewSecretKey(HttpCoreKeyManager.APP_PRI_KEY, HttpCoreKeyManager.APP_PRI_KEY_NEW);
        }
    }

    /**
     * AndroidKeyStore 加密方式导致异常 2020/07月版本使用keystore sha替代方案
     *
     * @param oldKey
     * @param newKey
     */
    public static void changeNewSecretKey(String oldKey, String newKey) {
        String oldKeyValue = null;
        String newKeyValue = null;
        try {
            oldKeyValue = (String) HttpCoreSpUtils.get(oldKey, "");
            newKeyValue = (String) HttpCoreSpUtils.get(newKey, "");
            if (!TextUtils.isEmpty(oldKeyValue) && TextUtils.isEmpty(newKeyValue)) {
                //旧版有值，新版为空 就是使用AndroidKeyStore的旧版本APP升级成新版本 需要先用老的方案解密 然后通过新的方案加密
                oldKeyValue = AndroidKeyStoreUtils.getInstance().decryptData(oldKeyValue, true);
                newKeyValue = AndroidKeyStoreUtils.getInstance().encryptData(oldKeyValue, false);
                HttpCoreSpUtils.remove(oldKey);
                HttpCoreSpUtils.put(newKey, newKeyValue);
            }
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        HttpLogUtils.i(TAG, "newKey:" + newKey + " newKeyValue " + newKeyValue);
    }


    public boolean isE2ee() {
        return e2ee;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getHttpCertsName() {
        return httpCertsName;
    }

    public boolean isDebug() {
        return isDebug;
    }

    public String getPackageId() {
        return packageId;
    }

    public HttpRequestCallBack getHttpRequestCallBack() {
        return httpRequestCallBack;
    }

    public String getmGcmDeviceToken() {
        return mGcmDeviceToken;
    }

    public void setmGcmDeviceToken(String mGcmDeviceTokenIn) {
        mGcmDeviceToken = mGcmDeviceTokenIn;
    }

    public int getFioRegFailedResId() {
        return mFioRegFailedResId;
    }

    public int getFioAuthFailedResId() {
        return mFioAuthFailedResId;
    }

    public int getFioDeregFailResId() {
        return mFioDeregFailResId;
    }

    public Context getContext() {
        return mContext;
    }

    public int getTimeOutStr() {
        return mTimeOutStr;
    }

    public int getNetWorkErrorStr() {
        return mNetWorkErrorStr;
    }


    public String getAppPriKey() {
        return appPriKey;
    }

    public void setAppPriKey(String aPriKey) {
        this.appPriKey = aPriKey;
    }


    public void setAppPubKey(String aPubKey) {
        this.appPubKey = aPubKey;
    }

    public String getAppPubKey() {
        return appPubKey;
    }

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String sPubKey) {
        this.serverPubKey = sPubKey;
    }

    /**
     * 初始化客户端公私钥
     */
    public void initAppKeypair() {
        Map<String, String> map = RSAHelper.generateKeyPair();
        if (null != map) {
            setAppPriKey(map.get(HttpCoreConstants.APP_PRIVATEKEY));
            setAppPubKey(map.get(HttpCoreConstants.APP_PUBLICKEY));
        }
    }

    public void saveServerPublicKey() {
        AndroidKeyStoreUtils.saveSvrPubKey(serverPubKey);
    }

    public void saveServerPublicKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
        AndroidKeyStoreUtils.saveSvrPubKey(serverPubKey);
    }

    public void saveAppPriKey() {
        AndroidKeyStoreUtils.saveAppPriKey(appPriKey);
    }


    public void saveSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionId() {
        return sessionId;
    }


    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getUserId() {
        return mUserId;
    }


    public void setCurrentDeviceId(String currentDeviceIdIn) {
        currentDeviceId = currentDeviceIdIn;
    }

    public String getCurrentDeviceId() {

        return currentDeviceId;
    }

    /**
     * 注意一下 用户登出不能清除的SP
     */
    public void clearMemoryCache() {
        String svrPubKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.SVR_PUB_KEY_NEW, "");
        String appPriKey = (String) HttpCoreSpUtils.get(HttpCoreKeyManager.APP_PRI_KEY_NEW, "");
        String language = (String) HttpCoreSpUtils.get(HttpCoreConstants.APP_SETTING_LANGUAGE, "");
        boolean isLocationDialog = (boolean) HttpCoreSpUtils.get(HttpCoreConstants.LOCATION_TIP_ALREADY_SHOW, false);

        //用户登出 定位不要区分
        boolean location = (boolean) HttpCoreSpUtils.get(HttpCoreConstants.CACHE_IS_HONGHK, true);
        //是否是uuid生成过程中aes异常的特殊机型
        boolean isSpecialMachine = (boolean) HttpCoreSpUtils.get(HttpCoreConstants.DATA_STR_UUID_NEW_NOT_ENCRYPTION, false);
        Integer rsaUpdateDone = (Integer) HttpCoreSpUtils.get(HttpCoreConstants.App_RSA_Update, SystemInitManager.RSA_UPDATE_DONE);
        HttpCoreSpUtils.clear();
        //这两个字段不需要清除
        HttpCoreSpUtils.put(HttpCoreKeyManager.SVR_PUB_KEY_NEW, svrPubKey);
        HttpCoreSpUtils.put(HttpCoreKeyManager.APP_PRI_KEY_NEW, appPriKey);
        HttpCoreSpUtils.put(HttpCoreConstants.App_RSA_Update, rsaUpdateDone);
        HttpCoreSpUtils.put(HttpCoreConstants.APP_SETTING_LANGUAGE, language);
        HttpCoreSpUtils.put(HttpCoreConstants.CACHE_IS_HONGHK, location);
        HttpCoreSpUtils.put(HttpCoreConstants.LOCATION_TIP_ALREADY_SHOW, isLocationDialog);
        HttpCoreSpUtils.put(HttpCoreConstants.DATA_STR_UUID_NEW_NOT_ENCRYPTION, isSpecialMachine);

    }


    public interface HttpRequestCallBack {
        boolean onRequestCallBack(String errorCode, String errorMsg);

        void startLoginPage(String errorMsg);

        void goMainPage(String errorMsg);

        void startOnlyMainPage(String errorMsg);

        void startMainPage(String errorMsg);

        void goLoginPage(String errorMsg);
    }
}
