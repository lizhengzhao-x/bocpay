package cn.swiftpass.httpcore.manager;

import java.util.Iterator;
import java.util.Vector;

import cn.swiftpass.httpcore.api.protocol.AutoLoginSynProtocol;
import cn.swiftpass.httpcore.entity.NetWorkEntity;


/**
 * @author 单例保证 多线程下 获取publickey 防止用例多次被执行
 */

public class AutoLoginManager {

    private static final String TAG = AutoLoginManager.class.getSimpleName();
    private static AutoLoginManager instance;

    private static volatile Object autoLock = new Object();

    private Vector<AutoLoginListener> listenerList = new Vector<>();
    private volatile Iterator<AutoLoginListener> iterator;

    private static class SingletonHolder {
        public static AutoLoginManager instance = new AutoLoginManager();

    }

    private AutoLoginManager() {

    }

    public static AutoLoginManager getInstance() {
        return SingletonHolder.instance;
    }

    /**
     * 释放内存
     */
    public static void clear() {
        instance = null;
    }

//    /**
//     * @return
//     */
//    public synchronized NetWorkEntity doAutoLogin(){
//        if(CacheManagerInstance.getInstance().isHasSessionId()){
//            return new NetWorkEntity(true,null);
//        }
//
//        NetWorkEntity netWorkEntity = new AutoLoginSynProtocol().execute();
//        return netWorkEntity;
//    }

    /**
     * 自动登录  多个线程请求  给每一个注册成功失败监听 只要有一个成功则发通知给其他线程
     * 管理这些监听用线程安全的集合类
     *
     * @return
     */
    public NetWorkEntity doAutoLoginSyn() {
        AutoLoginListener autoLoginListener = new AutoLoginListener() {
            @Override
            public NetWorkEntity onSuccess(NetWorkEntity netWorkEntity) {
                result=netWorkEntity;
                return netWorkEntity;
            }

            @Override
            public NetWorkEntity onFail(NetWorkEntity netWorkEntity) {
                result=netWorkEntity;
                return netWorkEntity;
            }
        };
        listenerList.add(autoLoginListener);

        if (autoLoginListener.result!=null){
            return autoLoginListener.result;
        }
        synchronized (autoLock) {
            if (autoLoginListener.result!=null){
                return autoLoginListener.result;
            }else {
                NetWorkEntity netWorkEntity = new AutoLoginSynProtocol().execute();
                iterator = listenerList.iterator();
                autoLoginListener.result=netWorkEntity;
                while (iterator.hasNext()) {
                    AutoLoginListener loginListener = iterator.next();
                    if (loginListener != null && loginListener != autoLoginListener) {
                        if (netWorkEntity.success) {
                            loginListener.onSuccess(netWorkEntity);
                            iterator.remove();
                        } else {
                            loginListener.onFail(netWorkEntity);
                            iterator.remove();
                        }
                    } else {
                        iterator.remove();
                    }
                }
                return netWorkEntity;
            }

        }
    }


}
