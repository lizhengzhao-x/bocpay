package cn.swiftpass.httpcore.manager;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.protocol.ServerPublicProtocol;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.utils.HttpLogUtils;


/**
 * 单例保证 多线程下 获取publickey 防止用例多次被执行
 */
public class ServerPublicKeyInstance {

    private static final String TAG = "ServerPublicKeyManagerInstance";
    private static ServerPublicKeyInstance instance;

    private static final Object INSTANCE_LOCK = new Object();

    private static class SingletonHolder {
        public static ServerPublicKeyInstance instance = new ServerPublicKeyInstance();
    }

    private ServerPublicKeyInstance() {

    }


    public static ServerPublicKeyInstance getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new ServerPublicKeyInstance();
                }
            }
        }
        return instance;
    }

//    public static ServerPublicKeyInstance getInstance() {
//        return ServerPublicKeyInstance.SingletonHolder.instance;
//    }

    /**
     * 释放内存
     */
    public static void clear() {
        instance = null;
    }


    /**
     * 同步方法  获取服务器公钥
     *
     * @return
     */
    public synchronized NetWorkEntity getServerPublicKeyEvent() {

        if (!TextUtils.isEmpty(HttpCoreKeyManager.getInstance().getServerPubKey())) {
            HttpLogUtils.i(TAG, "getServerPublicKeyEvent----> key already exits should return ");
            return new NetWorkEntity(true, null);
        }
        HttpLogUtils.i(TAG, "getServerPublicKeyEvent---->key not exits request");

        NetWorkEntity netWorkEntity = new ServerPublicProtocol().execute();
        return netWorkEntity;
    }
}
