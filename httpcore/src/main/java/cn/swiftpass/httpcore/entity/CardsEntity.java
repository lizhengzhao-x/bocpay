package cn.swiftpass.httpcore.entity;

import java.util.ArrayList;

/**
 * @author Jamy
 * 银行卡列表返回信息
 */

public class CardsEntity extends BaseEntity {

    /**
     * pages : 1
     * rows : [{"cardFaceId":"888888","cardId":345,"cardStatus":"2","cardType":1,"checked":false,"createTime":1533621512000,"expireDate":"1223","isDefault":1,"pageNumber":1,"pageSize":10,"pan":"4274257537557853","panFour":"853","phone":"86-15701927495","physicFlag":1,"total":0,"updateTime":1533621512000,"userId":334}]
     * total : 1
     */

    private int pages;
    private int total;
    private ArrayList<BankCardEntity> rows;

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<BankCardEntity> getRows() {
        return rows;
    }

    public void setRows(ArrayList<BankCardEntity> rows) {
        this.rows = rows;


    }

}
