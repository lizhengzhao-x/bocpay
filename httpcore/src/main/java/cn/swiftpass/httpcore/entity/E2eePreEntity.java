package cn.swiftpass.httpcore.entity;


/**
 * Created by ramon on 2018/8/7.
 */

public class E2eePreEntity extends BaseEntity {
    private String e2eeSid;
    private String publicKey;
    private String random;


    public String getE2eeSid() {
        return e2eeSid;
    }

    public void setE2eeSid(String e2eeSid) {
        this.e2eeSid = e2eeSid;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getRandom() {
        return random;
    }

    public void setRandom(String random) {
        this.random = random;
    }
}
