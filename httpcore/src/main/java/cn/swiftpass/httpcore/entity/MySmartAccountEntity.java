package cn.swiftpass.httpcore.entity;

import java.util.List;

/**
 * @author Jamy
 * create date on  on 2018/7/24 16:35
 * 我的账户注册，绑定，忘记密码返回的对象   在首页和转账 哪里用这个账户详情
 */
public class MySmartAccountEntity extends BaseEntity {

    //s1 s2 s3
    public static final String SMART_LEVEL_ONE = "1";
    public static final String SMART_LEVEL_TWO = "2";
    public static final String SMART_LEVEL_THREE = "3";

    //账户状态
    public static final String ACCOUNT_STATUS_NORMAL = "2";
    public static final String ACCOUNT_STATUS_ERROR = "3";
    //智能账号
    private String smartNo;
    //智能账户绑定的卡
    private BankCardEntity bindCard;
    //智能账户等级  等级3为智能smart账户 等级2为支付账户
    private String smartAcLevel;
    //电话号码
    private String mobile;
    //电邮
    private String email;
    //主账号
    private String relevanceAccNo;
    //每日限额 用于设置交易限额的时候
    private String payLimit;
    //最小限额
    private String minPay;
    //最大限额
    private String maxPay;
    //充值方式
    private String addedmethod;
    //Auto方式时 的限额
    private String autotopupamt;
    //余额
    private String balance;
    //币种
    private String currency;

    //脱敏后的手机号
    private String hideMobile;
    //自动充值时的限额
    private String methodMaxPay;
    //自动充值时的限额
    private String methodMinPay;
    //账户类型 已实现国际化 例如"储蓄账户"
    private String accType;
    //今日交易限额 用于展示， 每次转账这个值会发送变动
    private String outavailbal;
    //账户的状态 只有2是正常
    private String cardState;
    //edda已经绑定了的银行卡 不是用户现在有的所有银行卡 注意区分卡列表中的银行卡
    private List<BankCardEntity> bankCards;


    public BankCardEntity getBindCard() {
        return bindCard;
    }

    public void setBindCard(BankCardEntity bindCard) {
        this.bindCard = bindCard;
    }

    public String getSmartAcLevel() {
        return smartAcLevel;
    }

    public void setSmartAcLevel(String smartAcLevel) {
        this.smartAcLevel = smartAcLevel;
    }


    public String getHideMobile() {
        return hideMobile;
    }

    public void setHideMobile(String hideMobiel) {
        this.hideMobile = hideMobiel;
    }


    public List<BankCardEntity> getBankCards() {
        return bankCards;
    }

    public void setBankCards(List<BankCardEntity> bankCards) {
        this.bankCards = bankCards;
    }


    public String getOutavailbal() {
        return outavailbal;
    }

    public void setOutavailbal(String outavailbal) {
        this.outavailbal = outavailbal;
    }


    public String getAccType() {
        return (null == accType ? "" : accType);
    }

    public void setAccType(String accType) {
        this.accType = accType;
    }

    public String getMethodMaxPay() {
        return methodMaxPay;
    }

    public void setMethodMaxPay(String methodMaxPay) {
        this.methodMaxPay = methodMaxPay;
    }

    public String getMethodMinPay() {
        return methodMinPay;
    }

    public void setMethodMinPay(String methodMinPay) {
        this.methodMinPay = methodMinPay;
    }


    public String getCardState() {
        return cardState;
    }

    public void setCardState(String cardState) {
        this.cardState = cardState;
    }


    public String getSmartNo() {
        return smartNo;
    }

    public void setSmartNo(String smartNo) {
        this.smartNo = smartNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRelevanceAccNo() {
        return relevanceAccNo;
    }

    public void setRelevanceAccNo(String relevanceAccNo) {
        this.relevanceAccNo = relevanceAccNo;
    }

    public String getPayLimit() {
        return payLimit;
    }

    public void setPayLimit(String payLimit) {
        this.payLimit = payLimit;
    }

    public String getMinPay() {
        return minPay;
    }

    public void setMinPay(String minPay) {
        this.minPay = minPay;
    }

    public String getMaxPay() {
        return maxPay;
    }

    public void setMaxPay(String maxPay) {
        this.maxPay = maxPay;
    }

    public String getAddedmethod() {
        return addedmethod;
    }

    public void setAddedmethod(String addedmethod) {
        this.addedmethod = addedmethod;
    }

    public String getAutotopupamt() {
        return autotopupamt;
    }

    public void setAutotopupamt(String autotopupamt) {
        this.autotopupamt = autotopupamt;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
