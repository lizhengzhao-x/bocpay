package cn.swiftpass.httpcore.entity;

import android.text.TextUtils;

public class BankCardEntity extends BaseEntity {
    /**
     * bankCode : 014
     * bankName : CHAN TAI MAN
     * bankNo : 234234234
     * checked : false
     * createTime : 1546410932197
     * currency : HKD
     * eddaStatus : 1
     * id : 18
     * pageNumber : 1
     * pageSize : 10
     * physicFlag : 1
     * status : 1
     * total : 0
     * updateTime : 1546410932197
     * userId : 1278
     */
    //银行卡id
    private String cardId;
    //用户id
    private String userId;
    //银行卡号
    private String pan;

    private String virCardType;

    //1:信用卡  2：智能账号
    private String cardType;
    //当为空的卡列表时（1为空的虚拟卡数据 2为空的智能账户的卡数据）
    private int cardFlag;

    private String panFour;

    //卡面ID
    private String cardFaceId;
    //脱敏卡号
    private String panShowNumber;
    //银行卡cvv
    private String cvv;

    //电话
    private String phone;

    //是否默认卡 1：默认卡
    private String isDefault;

    private String bankCode;
    private String bankName;
    private String bankNo;


    private String currency;
    private String eddaStatus;
    //卡内余额
    private String  balance;
    //每日交易限额 余额
    private String  dailyLimitBalance;

    //Edda状态，N：没有登记过，A：登记成功 R：登记失败 1:未激活（登记edda，未充值过） 2：正常（充值过），3：没提现过，4：已经提现
    // E:删除失败 C:删除成功 4:删除申请失败 9删除申请处理中 3提交删除申请成功
    private String status;

    private String isFPS;

    private String smartAccLevel;

    private boolean isSel;
    //卡的确定类型 （1智能账户 2支付账户 3信用卡 4虚拟卡）
    private String cardDefineType;


    public int getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(int cardFlag) {
        this.cardFlag = cardFlag;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getDailyLimitBalance() {
        return dailyLimitBalance;
    }

    public void setDailyLimitBalance(String dailyLimitBalance) {
        this.dailyLimitBalance = dailyLimitBalance;
    }

    public boolean isVirtualCard() {
        if (TextUtils.isEmpty(virCardType)) {
            return false;
        }
        return virCardType.equals("1");
    }


    public String getPanFour() {
        return panFour;
    }

    public void setPanFour(String panFour) {
        this.panFour = panFour;
    }

    public String getVirCardType() {
        return virCardType;
    }

    public void setVirCardType(String virCardType) {
        this.virCardType = virCardType;
    }


    public String getIsFPS() {
        return isFPS;
    }

    public void setIsFPS(String isFPS) {
        this.isFPS = isFPS;
    }


    public String getSmartAccLevel() {
        return smartAccLevel;
    }

    public void setSmartAccLevel(String smartAccLevel) {
        this.smartAccLevel = smartAccLevel;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardFaceId() {
        return cardFaceId;
    }

    public void setCardFaceId(String cardFaceId) {
        this.cardFaceId = cardFaceId;
    }

    public String getPanShowNumber() {
        return panShowNumber;
    }

    public void setPanShowNumber(String panShowNumber) {
        this.panShowNumber = panShowNumber;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }


    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    public String getIsDefault() {
        return isDefault;
    }

    public boolean isDefaultCard() {
        if (TextUtils.isEmpty(isDefault)) {
            return false;
        }

        return TextUtils.equals(isDefault, "1");
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }


    //*********************ui添加字段*********************************//

    public boolean isSel() {
        return isSel;
    }

    public void setSel(boolean sel) {
        isSel = sel;
    }


    //*********************ui添加字段*********************************//


    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankNo() {
        return bankNo;
    }

    public void setBankNo(String bankNo) {
        this.bankNo = bankNo;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getEddaStatus() {
        return eddaStatus;
    }

    public void setEddaStatus(String eddaStatus) {
        this.eddaStatus = eddaStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getCardDefineType() {
        return cardDefineType;
    }

    public void setCardDefineType(String cardDefineType) {
        this.cardDefineType = cardDefineType;
    }

    public interface CardType{
        //卡的确定类型 （1智能账户 2支付账户 3信用卡 4虚拟卡）
        String TYPE_SMART_ACCOUNT="1";
        String TYPE_PAYMENT_ACCOUNT="2";
        String TYPE_CREDIT_CARD="3";
        String TYPE_VIRTUAL_CARD="4";
    }

    /**
     * 这个状态已经和后端同步过
     * Edda状态，
     *  N：没有登记过，
     *  A：登记成功
     *  R：登记失败
     *  1:未激活（登记edda，未充值过）
     *  2：正常（充值过），
     *  3：没提现过，
     *  4：已经提现
     *  E:删除失败
     *  C:删除成功
     *  4:删除申请失败
     *  9删除申请处理中
     *  3提交删除申请成功
     */
    public  interface CardEddaStatus{
        //Edda状态，
        // N：没有登记过
        String NEVER_REGISTER="N";
        // A：登记成功
        String REGISTER_SUCCESS="A";
        // R：登记失败
        String REGISTER_FAIL="R";
        //0:提交申请处理中
        String APPLY_PROCESSING="0";
        // 1: 提交申请成功但是未激活（登记edda，未充值过）
        String APPLY_NO_ACTIVE_WHEN_SUCCESS="1";
        // 2： 提交申请失败，
        String APPLY_FAIL="2";
        // E:删除失败
        String DELETE_FAIL="E";
        // C:删除成功
        String DELETE_SUCCESS="C";
        // 4:删除申请失败
        String DELETE_APPLY_FAIL="4";
        // 9删除申请处理中
        String DELETE_APPLY_PROCESSING="9";
        // 3提交删除申请成功
        String DELETE_APPLY_SUCCESS="3";
    }

    public interface CardNormalStatus{

    }
}
