package cn.swiftpass.httpcore.entity;

import okhttp3.Response;

/**
 * 自定义okhttp请求状态status 响应response实体类
 */
public class NetWorkEntity extends BaseEntity {

    public boolean success;
    public Response response;

    public NetWorkEntity(boolean success, Response response) {
        this.success = success;
        this.response = response;
    }
}
