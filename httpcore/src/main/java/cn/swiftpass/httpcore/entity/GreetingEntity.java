package cn.swiftpass.httpcore.entity;


/**
问候语
 */

public class GreetingEntity extends BaseEntity {
    String greeting;
    String startTime;
    String endTime;
    String dateFormat;


    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public GreetingEntity(String greeting, String startTime, String endTime, String dateFormat) {
        this.greeting = greeting;
        this.startTime = startTime;
        this.endTime = endTime;
        this.dateFormat = dateFormat;
    }
}
