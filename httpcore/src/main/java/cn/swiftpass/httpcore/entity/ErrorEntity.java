package cn.swiftpass.httpcore.entity;


/**
 * @author Jamy
 * create date on  on 2018/7/24 16:35
 * 请求返回错误信息对象
 */

public class ErrorEntity extends BaseEntity {
    String errormsg;
    String errorcode;

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public String getErrorcode() {
        return errorcode;
    }
}
