package cn.swiftpass.httpcore.entity;

import android.text.TextUtils;

/**
 * @author Jamy
 * create date on  on 2018/7/23 10:58
 * 卡列表返回对象
 */

public class AutoLoginSucEntity extends BaseEntity {

    /**
     * cards : {"pages":1,"rows":[{"cardFaceId":"888888","cardId":345,"cardStatus":"2","cardType":1,"checked":false,"createTime":1533621512000,"expireDate":"1223","isDefault":1,"pageNumber":1,"pageSize":10,"pan":"4274257537557853","panFour":"853","phone":"86-15701927495","physicFlag":1,"total":0,"updateTime":1533621512000,"userId":334}],"total":1}
     * sId : f196d9f9-81c5-43ab-bf35-76116e371327
     */
    /**
     * 注册迎新popup banner
     */
    private String newUserPopUpBanner;
    private CardsEntity cards;
    private String sId;
    private String walletId;

    public boolean isOpenBiometricAuth() {
        if (TextUtils.isEmpty(openBiometricAuth)) {
            return true;
        }
        return TextUtils.equals(openBiometricAuth, "Y");
    }

    public void setOpenBiometricAuth(String openBiometricAuth) {
        this.openBiometricAuth = openBiometricAuth;
    }

    /**
     * 是否开启生物认证 Y开启 N 不开启
     */
    private String openBiometricAuth;
    /**
     * 通讯中心  (Y：跳 N：不跳)
     */
    private String jumpToMessageCenter;

    /**
     * Y 的时候是打开， N的时候是关闭
     */
    private String messageCenterIsOpen;

    /**
     * 当app为升级的时候，要判断 messageCenterIsOpen = N 的时候才显示推送通知弹框
     *
     * @return
     */
    public boolean isJumpToMessageCenterWhenFirstInstallOrUpdate() {
        if (TextUtils.isEmpty(messageCenterIsOpen)) {
            return false;
        } else {
            return TextUtils.equals(messageCenterIsOpen, "N");
        }
    }

    /**
     * 手动登录 确认是新设备 跳转到通知中心
     *
     * @return
     */
    public boolean isJumpToMessageCenter() {
        if (TextUtils.isEmpty(jumpToMessageCenter)) {
            return false;
        } else {
            return TextUtils.equals(jumpToMessageCenter, "Y");
        }
    }

    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getWalletId() {
        return walletId;
    }

    public CardsEntity getCards() {
        return cards;
    }

    public void setCards(CardsEntity cards) {
        this.cards = cards;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public boolean isNeedEditPwd() {
        if (TextUtils.isEmpty(needEditPwd)) {
            return false;
        } else {
            return TextUtils.equals(needEditPwd, "Y");
        }
    }

    /**
     * 是否需要修改密码 Y/N
     */
    private String needEditPwd;

    public String getNewUserPopUpBanner() {
        return newUserPopUpBanner;
    }

    public void setNewUserPopUpBanner(String newUserPopUpBanner) {
        this.newUserPopUpBanner = newUserPopUpBanner;
    }


    public String getMessageCenterIsOpen() {
        return messageCenterIsOpen;
    }

    public void setMessageCenterIsOpen(String messageCenterIsOpen) {
        this.messageCenterIsOpen = messageCenterIsOpen;
    }

    public String getJumpToMessageCenter() {
        return jumpToMessageCenter;
    }

    public void setJumpToMessageCenter(String jumpToMessageCenter) {
        this.jumpToMessageCenter = jumpToMessageCenter;
    }
}
