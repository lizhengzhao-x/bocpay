package cn.swiftpass.httpcore.entity;

/**
 * Created by admin on 2018/7/19.
 * 服务端私钥对象
 */

public class ServerPubKeyEntity extends BaseEntity {
    private String serverPubKey;

    public String getServerPubKey() {
        return serverPubKey;
    }

    public void setServerPubKey(String serverPubKey) {
        this.serverPubKey = serverPubKey;
    }
}
