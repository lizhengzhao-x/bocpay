package cn.swiftpass.httpcore.entity;


public class AccountPointDataEntity extends BaseEntity {


    /**
     * sumGp :
     * flagSumGp :
     * sumGpMsg :
     * balance :
     * flagBalance :
     * dailyLimitBalance :
     * balanceMsg :
     */
    public static final String NORMAL="0";
    public static final String INVALID="1";
    public static final String ERROR="2";
    public static final String ACCOUNT_PA_SA="0";
    public static final String ACCOUNT_CCA="1";

    private String sumGp;
    private String flagSumGp;
    private String sumGpMsg;
    private String balance;
    private String flagBalance;//余额查询标志，0智能账户,1信用卡，，2查询接口异常
    private String dailyLimitBalance;
    private String balanceMsg;

    public String getSumGp() {
        return sumGp;
    }

    public void setSumGp(String sumGp) {
        this.sumGp = sumGp;
    }

    public String getFlagSumGp() {
        return flagSumGp;
    }

    public void setFlagSumGp(String flagSumGp) {
        this.flagSumGp = flagSumGp;
    }

    public String getSumGpMsg() {
        return sumGpMsg;
    }

    public void setSumGpMsg(String sumGpMsg) {
        this.sumGpMsg = sumGpMsg;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getFlagBalance() {
        return flagBalance;
    }

    public void setFlagBalance(String flagBalance) {
        this.flagBalance = flagBalance;
    }

    public String getDailyLimitBalance() {
        return dailyLimitBalance;
    }

    public void setDailyLimitBalance(String dailyLimitBalance) {
        this.dailyLimitBalance = dailyLimitBalance;
    }

    public String getBalanceMsg() {
        return balanceMsg;
    }

    public void setBalanceMsg(String balanceMsg) {
        this.balanceMsg = balanceMsg;
    }
}
