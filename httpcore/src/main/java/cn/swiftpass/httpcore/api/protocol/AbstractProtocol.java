package cn.swiftpass.httpcore.api.protocol;

import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;


/**
 * 抽象类处理特殊错误码异常跳转
 */
public abstract class AbstractProtocol<T> implements IProtocol<T> {

    public boolean checkCustomErrorCode(final String errorCode, final String errorMsg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            return HttpCoreKeyManager.getInstance().getHttpRequestCallBack().onRequestCallBack(errorCode, errorMsg);
        }
        return false;
    }

    public void goLoginPage(final String errorMsg) {
        if (this instanceof ServerPublicProtocol || this instanceof AutoLoginSynProtocol) {
            if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
                HttpCoreKeyManager.getInstance().getHttpRequestCallBack().goLoginPage(errorMsg);
            }

        } else {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startLoginPage(errorMsg);
        }
    }

    public void goMainPage(final String msg) {
        if (this instanceof ServerPublicProtocol || this instanceof AutoLoginSynProtocol) {
            if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
                HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(msg);
            }
        } else {
            if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
                HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(msg);
            }
        }
    }


    /**
     * 继承该类都默认走拦截器
     */
    @Override
    public boolean ignoreInterceptor() {
        return false;
    }

}
