package cn.swiftpass.httpcore.api.protocol;

/**
 * Created by ramon on 2018/7/21.
 */

public interface IProtocol<T> {

    T execute();

    /**
     * 是否依赖登录态
     */
    boolean isNeedLogin();

    /**
     * 是否需要加密
     */
    boolean isEncrypt();

    /**
     * 是否忽略拦截器 默认接口都会走拦截器  部分接口不需要  例如okhttpGlide上传图片等
     */
    boolean ignoreInterceptor();

    void onSuccessCallback(Object o);

    void onFailCallback(String errorCode, String errorMsg);

    /**
     * 数据验签
     */
    boolean verifySign(String sign, String data);
}
