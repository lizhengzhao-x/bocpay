package cn.swiftpass.httpcore.api.protocol;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import java.util.HashMap;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.entity.ServerPubKeyEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.HeaderUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.encry.AndroidKeyStoreUtils;
import cn.swiftpass.httpcore.utils.encry.DecryptNetworkData;
import cn.swiftpass.httpcore.utils.encry.SignUtils;

/**
 * 获取服务器公钥请求 本地生成Rsa公私钥 将公钥上传到服务器 服务器用该公钥加密服务器本地AES秘钥 返回给客户端  客户端用本地RSA私钥解密服务器AES秘钥来做AES解密服务器返回数据
 */
public class ServerPublicProtocol extends AbstractProtocol<NetWorkEntity> {

    public static final String TAG = ServerPublicProtocol.class.getSimpleName();
    private String mAppPubKey;

    private String mUrl;
    private JSONObject mBodyParams;
    private Gson mGson;

    public ServerPublicProtocol() {
        mUrl = "api/security/deviceRegistration";
    }

    @Override
    public NetWorkEntity execute() {
        String pubKey = AndroidKeyStoreUtils.getLocalSvrPubkey();
        String appPrikey = AndroidKeyStoreUtils.getLocalAppPrikey();
        if (!TextUtils.isEmpty(pubKey) && !TextUtils.isEmpty(appPrikey)) {
            HttpCoreKeyManager.getInstance().setServerPubKey(pubKey);
            HttpCoreKeyManager.getInstance().setAppPriKey(appPrikey);
            return new NetWorkEntity(true, null);
        } else {
            HttpCoreKeyManager.getInstance().initAppKeypair();
            mAppPubKey = HttpCoreKeyManager.getInstance().getAppPubKey();
            mGson = new Gson();
            mBodyParams = new JSONObject();
            packData();
            String url = getUrl();

            //同步请求
            JSONObject postData = makePostData(mBodyParams);
            NetWorkEntity netWorkEntity = doPostSyn(url, postData.toString());

            //本地时间戳
            String timetamp = "";
            JSONObject header = postData.getJSONObject("header");
            if (header != null) {
                timetamp = header.getString(HttpCoreRequestParams.TIMETAMP);
            }
            final DecryptNetworkData decryptNetworkData = new DecryptNetworkData();
            decryptNetworkData.setTimetamp(timetamp);

            //如果请求成功 则解析数据
            if (netWorkEntity != null && netWorkEntity.success) {
                try {
                    //解析成功与失败回调 实现父类接口抽象方法
                    if (netWorkEntity.response.body() != null) {
                        //注意这里是string()不是toString() 且只能读取一次
                        decryptNetworkData.decryptResponse(netWorkEntity.response.body().string(), ServerPublicProtocol.this, ServerPubKeyEntity.class);
                    } else {
                        return new NetWorkEntity(false, null);
                    }
                } catch (Exception e) {
                    return new NetWorkEntity(false, null);
                }
            } else {
                return new NetWorkEntity(false, null);
            }
            return netWorkEntity;
        }
    }

    private JSONObject makePostData(JSONObject mBodyParams) {
        JSONObject jsonObject = new JSONObject();
        try {
            //摘要，签名处理  body加密放在加密拦截器处理
            jsonObject.put("header", HeaderUtils.getHeaders());
            jsonObject.put("body", mBodyParams);

            HttpLogUtils.i(TAG, "requestparams: " + jsonObject.toString());
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return jsonObject;
    }

    private NetWorkEntity doPostSyn(String url, String postData) {
        HashMap<String, Object> headerMap = new HashMap<>();
        headerMap.put(NetworkConstants.NEED_ENCRYPT, NetworkConstants.STATUS_TRUE);
        return OkHttpManager.getInstance().doPostSyn(url, headerMap, postData);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        try {
            ServerPubKeyEntity serverPubKeyEntity = mGson.fromJson(data, ServerPubKeyEntity.class);
            return SignUtils.verify(sign, data, serverPubKeyEntity.getServerPubKey());
        } catch (Exception e) {
            return false;
        }
    }

    public void packData() {
        mBodyParams.put(HttpCoreRequestParams.APP_PUBKEY, mAppPubKey);
    }

    public String getUrl() {
        return (HttpCoreKeyManager.getInstance().getBaseUrl() + mUrl);
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }

    @Override
    public boolean isEncrypt() {
        return true;
    }

    @Override
    public void onSuccessCallback(Object o) {
        //成功后将服务器公钥保存在本地
        if (o instanceof ServerPubKeyEntity) {
            ServerPubKeyEntity serverPubKeyEntity = (ServerPubKeyEntity) o;
            boolean flag = !TextUtils.isEmpty(serverPubKeyEntity.getServerPubKey());
            if (flag) {
                HttpCoreKeyManager.getInstance().setServerPubKey(serverPubKeyEntity.getServerPubKey());
                HttpCoreKeyManager.getInstance().saveAppPriKey();
                HttpCoreKeyManager.getInstance().saveServerPublicKey();
            }
        }
    }

    @Override
    public void onFailCallback(String errorCode, String errorMsg) {
        checkCustomErrorCode(errorCode, errorMsg);
    }

    @Override
    public void goMainPage(final String msg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(msg);
        }

    }

    @Override
    public void goLoginPage(final String errorMsg) {

        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(errorMsg);
        }


    }
}
