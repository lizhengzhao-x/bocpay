package cn.swiftpass.httpcore.api.interceptor;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.NetWorkDataUtil;
import cn.swiftpass.httpcore.utils.encry.AESUtils;
import cn.swiftpass.httpcore.utils.encry.EncryptUtils;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static cn.swiftpass.httpcore.config.HttpCoreConstants.ORI_STR_SVR_KEY;


/**
 * 请求加密拦截器 所有post请求都需要处理
 */
public class EncryptRequestInterceptor implements Interceptor {

    private String TAG = "BaseProtocol";
    private static final String METHOD_POST = "POST";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Headers headers = request.headers();

        if (NetworkConstants.STATUS_TRUE.equals(headers.get(NetworkConstants.IGNORE_INTERCEPTOR))) {
            return chain.proceed(request);
        }

        if (METHOD_POST.equals(request.method())) {
            String content = "";
            RequestBody requestBody = request.body();
            if (requestBody != null) {
                content = NetWorkDataUtil.requestBodyToStr(requestBody);
            }

            JSONObject obj = JSONObject.parseObject(content);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("header", obj.getJSONObject("header"));

            if (NetworkConstants.STATUS_TRUE.equals(headers.get(NetworkConstants.NEED_ENCRYPT))) {
                JSONObject bodyJson = obj.getJSONObject("body");
                String mAESKey = EncryptUtils.getAESKey();
                jsonObject.put("sign", getSign(bodyJson));
                //本地生成Aes秘钥加密Body
                jsonObject.put("body", getBody(bodyJson, mAESKey));
                //服务器Rsa公钥加密本地Aes私钥  上传到服务器
                jsonObject.put("token", getToken(mAESKey));
                HttpLogUtils.i(TAG, "encrypt request Url: " + request.url().toString());
                HttpLogUtils.i(TAG, "requestParams: " + jsonObject.toString() + "\n " + bodyJson);
            } else {
                JSONObject bodyObj = obj.getJSONObject("body");
                jsonObject.put("body", bodyObj);
                jsonObject.put("sign", "");
                jsonObject.put("token", "");
                HttpLogUtils.i(TAG, "encrypt request Url: " + request.url().toString());
                HttpLogUtils.i(TAG, "requestParams: " + jsonObject.toString());
            }



            RequestBody reqBody = RequestBody.create(OkHttpManager.JSON, jsonObject.toString());
            Request newRequst = request.newBuilder()
                    .headers(request.headers())
                    .post(reqBody)
                    .build();

            return chain.proceed(newRequst);
        }

        return chain.proceed(request);
    }

    public String getRawBody(JSONObject mBodyParams) {
        return mBodyParams.toString();
    }

    /**
     * 自定义body信息
     */
    public String getBody(JSONObject mBodyParams, String mAESKey) {
        String body = getRawBody(mBodyParams);
        try {
            body = AESUtils.AESEncrypt(body, mAESKey);
        } catch (Exception e) {
        }
        return body;
    }


    /**
     * 参数加签
     *
     * @param mBodyParams
     * @return
     */
    public String getSign(JSONObject mBodyParams) {
        //客户端用客户端Rsa私钥加签  服务器用客户端公钥验签
        return EncryptUtils.getDigestSign(getRawBody(mBodyParams));
    }

    public String getToken(String mAESKey) {
        //首次加密用本地写死的服务器公钥

        if (TextUtils.isEmpty(HttpCoreKeyManager.getInstance().getServerPubKey())) {
            return EncryptUtils.getEncryptionToken(mAESKey, ORI_STR_SVR_KEY);
        }
        return EncryptUtils.getEncryptionToken(mAESKey, HttpCoreKeyManager.getInstance().getServerPubKey());
    }
}
