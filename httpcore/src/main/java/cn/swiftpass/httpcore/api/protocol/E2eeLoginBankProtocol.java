package cn.swiftpass.httpcore.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;

/**
 * @author Created by ramon on 2018/8/7.
 * 进行E2EE登录网银账户、银行卡
 */

public class E2eeLoginBankProtocol extends E2eeProtocol {

    /**
     * 网银账号或者卡号
     */
    private String mAccount;

    /**
     * 1：网银+密码  2：银行卡+卡密码
     */
    private String mLoginType;

    /**
     * V：注册流程  B：绑定流程  F：忘记密码流程   接口输入：V  （大写）
     */
    private String mAction;

    private String mVerifyCode = "";

    public E2eeLoginBankProtocol(String account, String passcode, String action, String type, String verifyCode, NetWorkCallbackListener dataCallback) {
        mDataCallback = dataCallback;
        mUrl = "api/smartAccOp/loginNetBank";
        mAccount = account;
        mPasscode = passcode;
        mLoginType = type;
        mAction = action;
        mVerifyCode = verifyCode;
    }

    @Override
    public Void execute() {
        super.execute();
        return null;
    }

    @Override
    public void packData() {
        mBodyParams.put("accNo", mAccount);
        mBodyParams.put("password", mPasscode);
        mBodyParams.put("loginType", mLoginType);
        mBodyParams.put("action", mAction);
        mBodyParams.put("verifyCode", mVerifyCode);
    }
}

