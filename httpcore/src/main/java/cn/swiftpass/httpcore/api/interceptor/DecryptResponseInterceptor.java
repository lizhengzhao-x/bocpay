package cn.swiftpass.httpcore.api.interceptor;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import java.io.IOException;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.ResponseCode;
import cn.swiftpass.httpcore.entity.CommonEntity;
import cn.swiftpass.httpcore.entity.ContentEntity;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.NetWorkDataUtil;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * 解析第一层CommonEntity响应拦截器
 */
public class DecryptResponseInterceptor implements Interceptor {
    private String TAG = "BaseProtocol";
    private static final String METHOD_POST = "POST";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String url = request.url().toString();
        Headers headers = request.headers();
        Response response = chain.proceed(request);
        if (response == null || response.body() == null) {
            return response;
        }

        if (NetworkConstants.STATUS_TRUE.equals(headers.get(NetworkConstants.IGNORE_INTERCEPTOR))) {
            return response;
        }
        String responseBody = NetWorkDataUtil.cloneResponseBody(response.body());
        HttpLogUtils.d(TAG, url + "\n==responseBody: " + responseBody);
        if (!METHOD_POST.equals(request.method())) {
            return response;
        }

        //自定义Response.Builder根据返回结果来定义response
        Response.Builder builder = new Response.Builder()
                .protocol(Protocol.HTTP_2).request(request);

        if (TextUtils.isEmpty(responseBody)) {
            return builder.code(404).message(ErrorCode.NULL_RESPONSE.code)
                    .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
        }

        //请求成功部分
        CommonEntity common = null;
        Gson mGson = new Gson();
        try {
            common = mGson.fromJson(responseBody, CommonEntity.class);
        } catch (Exception e) {
            HttpLogUtils.i(TAG, "intercept" + e.getLocalizedMessage());
            //TODO  这里也需要自定义code来区分异常的场景
            return builder.code(404).message(ErrorCode.GSON_COMMON_ERROR.code)
                    .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
        }
        if (null == common) {
            //请求失败

            return builder.code(404).message(ErrorCode.NULL_COMMON_ENTITY.code)
                    .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
        }
        if (ResponseCode.RESPONSE_SUCCESS.equals(common.getReturn_code())) {
            //第一层code 代表通信成功
            ContentEntity contentEntity = null;
            try {
                contentEntity = mGson.fromJson(common.getContent(), ContentEntity.class);
            } catch (Exception e) {
                //TODO  这里也需要自定义code来区分异常的场景
                return builder.code(404).message(ErrorCode.GSON_CONTENT_ERROR.code)
                        .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
            }
            if (contentEntity == null) {
                //请求失败
                return builder.code(404).message(ErrorCode.NULL_CONTENT_ENTITY.code)
                        .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
            }

            String content = JSONObject.toJSONString(contentEntity);
            return builder.code(200).message(ResponseCode.RESPONSE_SUCCESS)
                    .body(ResponseBody.create(OkHttpManager.JSON, content)).build();
        } else {
            //第一层code 代表通信失败
            //后台bug，存在把错误码放第一层的情形
            ContentEntity contentEntity = new ContentEntity();
            contentEntity.setResult_code(common.getReturn_code());
            contentEntity.setResult_msg(common.getReturn_msg());
            String content = JSONObject.toJSONString(contentEntity);
            HttpLogUtils.d(TAG, "decryResponseBody: " + content);
            return builder.code(200).message(ResponseCode.RESPONSE_SUCCESS)
                    .body(ResponseBody.create(OkHttpManager.JSON, content)).build();
        }
    }
}
