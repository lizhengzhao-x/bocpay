package cn.swiftpass.httpcore.api.interceptor;

import android.text.TextUtils;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.manager.AutoLoginManager;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.NetWorkDataUtil;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;

/**
 * 自动登录请求拦截器  主要判断是否需要登录态  依赖获取公钥拦截器
 * chain是拦截器的节点
 */
public class AutoLoginRequestInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Headers headers = request.headers();
        if (NetworkConstants.STATUS_FALSE.equals(headers.get(NetworkConstants.NEED_LOGIN))
                || NetworkConstants.STATUS_TRUE.equals(headers.get(NetworkConstants.IGNORE_INTERCEPTOR))
                || request.url().toString().contains(NetworkConstants.URL_AUTO_LOGIN)
                || request.url().toString().contains(NetworkConstants.URL_RSA_UPDATE)
                || request.url().toString().contains(NetworkConstants.URL_ENCRYPT)) {
            //如果请求不依赖登录态 或者该请求为获取服务器公钥或自动登录请求 则默认处理
            return chain.proceed(request);
        } else {
            NetWorkEntity netWorkEntity = AutoLoginManager.getInstance().doAutoLoginSyn();
            if (netWorkEntity.success) {
                //自动登录成功后  重新自定义header 修改sid值
                Request newRequest = addParams(request);
                return chain.proceed(newRequest);
            }else {
                if (netWorkEntity.response!=null){
                    return netWorkEntity.response;
                }else {
                    //自定义Response.Builder根据返回结果来定义response
                    Response.Builder builder = new Response.Builder()
                            .protocol(Protocol.HTTP_2).request(request);
                    return builder.code(404).message(ErrorCode.NULL_RESPONSE.code)
                            .body(ResponseBody.create(OkHttpManager.JSON, "")).build();
                }
            }
        }
    }

    /**
     * 自动登录成功后 要修改requestBody中的header字段里面的sid值
     *
     * @param oldRequest
     * @return
     */
    private Request addParams(Request oldRequest) {
        Request request = oldRequest.newBuilder().url(oldRequest.url())
                .method(oldRequest.method(), changeHeaders(oldRequest.body()))
                .build();
        return request;
    }

    /**
     * 解析requestBody  修改sid值
     *
     * @return
     */
    private RequestBody changeHeaders(RequestBody body) {
        String bodyString = "";
        try {
            bodyString = NetWorkDataUtil.requestBodyToStr(body);
            JSONObject bodyJson = JSONObject.parseObject(bodyString);
            JSONObject header = bodyJson.getJSONObject("header");
            header.remove(HttpCoreRequestParams.S_ID);
            header.put(HttpCoreRequestParams.S_ID, HttpCoreKeyManager.getInstance().getSessionId());
            bodyJson.remove("header");
            bodyJson.put("header", header);
            return RequestBody.create(OkHttpManager.JSON, bodyJson.toString());
        } catch (Exception e) {
            return body;
        }
    }
}
