package cn.swiftpass.httpcore.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;

/**
 * @author Created by ramon on 2018/8/7.
 * 进行E2EE登录网银账户、银行卡
 */

public class E2eeProtocol extends BaseProtocol {
    public String mSid;
    public String mPasscodeOld;
    public String mPasscode;
    public boolean isModify = false;

    @Override
    public Void execute() {
        if (HttpCoreKeyManager.getInstance().isE2ee()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    if (isModify) {
                        mPasscodeOld = E2eeUtils.getChangePin(mSid, mPasscodeOld, mPasscode, pubKey, random);
                    }
                    mPasscode = E2eeUtils.getLoginOrSetPin(mSid, mPasscode, pubKey, random);
                    E2eeProtocol.super.execute();
                }
            }).execute();
        } else {
            super.execute();
        }

        return null;
    }
}

