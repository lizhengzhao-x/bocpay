package cn.swiftpass.httpcore.api.interceptor;

import java.io.IOException;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.manager.RsaUpdateManager;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @name bocbill-aos
 * @class name：cn.swiftpass.bocbill.support.network
 * @class describe
 * @anthor zhangfan
 * @time 2020/4/10 10:56
 * @change
 * @chang time
 * @class describe
 **/

public class RsaUpdateRequestInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Headers headers = request.headers();
        String url = request.url().toString();
        if (NetworkConstants.STATUS_FALSE.equals(headers.get(NetworkConstants.NEED_ENCRYPT)) ||
                url.contains(NetworkConstants.URL_RSA_UPDATE) ||
                url.contains(NetworkConstants.URL_ENCRYPT)) {
            return chain.proceed(request);
        } else {
            if (SystemInitManager.getInstance().getUpdateRsaFinish() != SystemInitManager.RSA_UPDATE_DONE) {
                NetWorkEntity networkBean = RsaUpdateManager.getInstance().getUpdateRsaFinish();
                if (networkBean.success) {
                    return chain.proceed(request);
                }
            }
            return chain.proceed(request);
        }
    }


}
