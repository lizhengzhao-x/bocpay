package cn.swiftpass.httpcore.api.protocol;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.HttpCoreConstants;
import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.entity.ServerPubKeyEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.httpcore.utils.HeaderUtils;
import cn.swiftpass.httpcore.utils.HttpCoreSpUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.encry.AndroidKeyStoreUtils;
import cn.swiftpass.httpcore.utils.encry.DecryptNetworkData;
import cn.swiftpass.httpcore.utils.encry.RSAHelper;
import cn.swiftpass.httpcore.utils.encry.SignUtils;


/**
 * @author jamy on 2018/7/23 11:27
 * rsa 升级接口
 */

public class RSAUpdateProtocol extends AbstractProtocol<NetWorkEntity> {
    public static final String TAG = RSAUpdateProtocol.class.getSimpleName();
    String mAppPubKey;
    private String mUrl;
    public JSONObject mBodyParams;
    private String mAppPriKey;

    public RSAUpdateProtocol() {
        mUrl = "api/security/forceUpdateKey";
    }


    @Override
    public void onSuccessCallback(Object obj) {
        if (obj instanceof ServerPubKeyEntity) {
            ServerPubKeyEntity networkBean = (ServerPubKeyEntity) obj;
            boolean flag = !TextUtils.isEmpty(networkBean.getServerPubKey());
            if (flag) {
                HttpCoreKeyManager.getInstance().setAppPriKey(mAppPriKey);
                HttpCoreKeyManager.getInstance().saveAppPriKey();
                SystemInitManager.getInstance().setUpdateRsaFinish(SystemInitManager.RSA_UPDATE_DONE);
                HttpCoreSpUtils.setRSAUpdateFinish(SystemInitManager.RSA_UPDATE_DONE);
                HttpCoreKeyManager.getInstance().saveServerPublicKey(networkBean.getServerPubKey());
                HttpLogUtils.i(TAG, "mAppPriKey: " + mAppPriKey + " ServerPubKey " + networkBean.getServerPubKey());
            }
        }

    }


    @Override
    public boolean isNeedLogin() {
        return false;
    }

    @Override
    public boolean isEncrypt() {
        return true;
    }

    @Override
    public void onFailCallback(String errorCode, String errorMsg) {
        //处理状态
        boolean result = checkCustomErrorCode(errorCode, errorMsg);
        if (result && !CacheManagerInstance.getInstance().checkLoginStatus()) {
            AndroidKeyStoreUtils.clearLocalKey();
            SystemInitManager.getInstance().setUpdateRsaFinish(SystemInitManager.RSA_UPDATE_DONE);
            HttpCoreSpUtils.setRSAUpdateFinish(SystemInitManager.RSA_UPDATE_DONE);
        }
    }

    private JSONObject makePostData(JSONObject mBodyParams) {
        JSONObject jsonObject = new JSONObject();
        try {
            //摘要，签名处理  body加密放在加密拦截器处理
            jsonObject.put("header", HeaderUtils.getHeaders());
            jsonObject.put("body", mBodyParams);

            HttpLogUtils.i(TAG, "requestparams: " + jsonObject.toString());
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return jsonObject;
    }

    private NetWorkEntity doPostSyn(String url, String postData) {
        HashMap<String, Object> headerMap = new HashMap<>();
        headerMap.put(NetworkConstants.NEED_ENCRYPT, NetworkConstants.STATUS_TRUE);
        return OkHttpManager.getInstance().doPostSyn(url, headerMap, postData);
    }

    public void packData() {
        mBodyParams.put(HttpCoreRequestParams.APP_PUBKEY, mAppPubKey);
    }

    public String getUrl() {
        return (HttpCoreKeyManager.getInstance().getBaseUrl() + mUrl);
    }


    @Override
    public void goMainPage(final String msg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(msg);
        }

    }

    @Override
    public void goLoginPage(final String errorMsg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().goLoginPage(errorMsg);
        }

    }

    /**
     * 自定义header
     *
     * @return
     */
    @Override
    public NetWorkEntity execute() {
        String pubKey = AndroidKeyStoreUtils.getLocalSvrPubkey();
        String appPrikey = AndroidKeyStoreUtils.getLocalAppPrikey();
        if (TextUtils.isEmpty(pubKey) || TextUtils.isEmpty(appPrikey)) {
            AndroidKeyStoreUtils.clearLocalKey();
            SystemInitManager.getInstance().setUpdateRsaFinish(SystemInitManager.RSA_UPDATE_DONE);
            HttpCoreSpUtils.setRSAUpdateFinish(SystemInitManager.RSA_UPDATE_DONE);
            return new NetWorkEntity(true, null);
        } else {
            Map<String, String> map = RSAHelper.generateKeyPair();
            mAppPriKey = map.get(HttpCoreConstants.APP_PRIVATEKEY);
            mAppPubKey = map.get(HttpCoreConstants.APP_PUBLICKEY);

            HttpLogUtils.i(TAG, "生成 mAppPriKey: " + mAppPriKey + " ServerPubKey " + mAppPubKey);


            mBodyParams = new JSONObject();
            packData();
            String url = getUrl();
            HttpLogUtils.i(TAG, url);
            JSONObject postData = makePostData(mBodyParams);
            NetWorkEntity networkBean = doPostSyn(url, postData.toString());
            //本地时间戳
            String timetamp = "";
            JSONObject header = postData.getJSONObject("header");
            if (header != null) {
                timetamp = header.getString(HttpCoreRequestParams.TIMETAMP);
            }
            final DecryptNetworkData decryptNetworkData = new DecryptNetworkData();
            decryptNetworkData.setTimetamp(timetamp);
            if (networkBean != null && networkBean.success) {
                try {
                    if (networkBean.response.body() != null) {
                        decryptNetworkData.decryptResponse(networkBean.response.body().string(), RSAUpdateProtocol.this, ServerPubKeyEntity.class);
                    } else {
                        return new NetWorkEntity(false, null);
                    }
                } catch (IOException e) {
                    return new NetWorkEntity(false, null);
                }
            } else {
                return new NetWorkEntity(false, null);
            }

            return networkBean;
        }
    }


    @Override
    public boolean verifySign(String sign, String data) {

        return SignUtils.verify(sign, data, HttpCoreKeyManager.getInstance().getServerPubKey());
    }

}
