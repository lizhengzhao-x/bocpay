package cn.swiftpass.httpcore.api;

public class NetworkConstants {

    private NetworkConstants() {
    }

    /**
     * 请求是否需要加密
     */
    public static final String NEED_ENCRYPT = "need_encrypt";
    /**
     * 获取公钥接口
     */
    public static final String URL_ENCRYPT = "security/deviceRegistration";
    /**
     * rsa接口
     */
    public static final String URL_RSA_UPDATE = "security/forceUpdateKey";
    /**
     * 请求是否依赖登录态
     */
    public static final String NEED_LOGIN = "need_login";
    /**
     * 自动登录接口
     */
    public static final String URL_AUTO_LOGIN = "login/auto";
    /**
     * 该请求是否忽略自定义拦截器
     */
    public static final String IGNORE_INTERCEPTOR = "ignore_interceptor";

    /**
     * 状态码
     */
    public static final String STATUS_TRUE = "1";
    public static final String STATUS_FALSE = "0";


}
