package cn.swiftpass.httpcore.api.protocol;


import cn.swiftpass.httpcore.api.NetWorkCallbackListener;

/**
 * @author Created by ramon on 2018/8/7.
 * 获取e2ee前置信息 E2eePreEntity
 */

public class E2eePreProtocol extends BaseProtocol {

    public E2eePreProtocol(NetWorkCallbackListener dataCallback) {
        mUrl = "api/security/getEncryptParam";
        mDataCallback = dataCallback;
    }

    @Override
    public Void execute() {
        super.execute();
        //mDataCallback.onSuccess(getTestData());
        return null;
    }


}
