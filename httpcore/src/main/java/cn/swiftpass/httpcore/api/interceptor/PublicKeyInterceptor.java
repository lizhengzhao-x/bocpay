package cn.swiftpass.httpcore.api.interceptor;

import android.text.TextUtils;

import java.io.IOException;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.ServerPublicKeyInstance;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 获取服务器公钥拦截器
 */
public class PublicKeyInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Headers headers = request.headers();
        String url = request.url().toString();

        if (NetworkConstants.STATUS_FALSE.equals(headers.get(NetworkConstants.NEED_ENCRYPT)) ||
                url.contains(NetworkConstants.URL_ENCRYPT)) {
            //请求无需加密 或者该请求为获取公钥请求
            return chain.proceed(request);
        } else {
            //需要加密
            if (TextUtils.isEmpty(HttpCoreKeyManager.getInstance().getServerPubKey())) {
                NetWorkEntity netWorkEntity = ServerPublicKeyInstance.getInstance().getServerPublicKeyEvent();
                if (netWorkEntity.success) {
                    return chain.proceed(request);
                }
            }
            return chain.proceed(request);
        }
    }
}
