package cn.swiftpass.httpcore.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;


/**
 * @author Created by ramon on 2018/8/7.
 * 进行E2EE登录，弃用
 */

public class E2eeLoginProtocol extends BaseProtocol {
    private String mPwd;
    private String mPin;
    private String mSid;
    private String mAccount;
    private String mWalletId;

    public E2eeLoginProtocol(String account, String walletId, String pwd, NetWorkCallbackListener dataCallback) {
        mAccount = account;
        mWalletId = walletId;
        mDataCallback = dataCallback;
        mUrl = "api/security/encryptPin";
        mPwd = pwd;
    }

    @Override
    public Void execute() {
        if (HttpCoreKeyManager.getInstance().isDebug()) {
            new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
                @Override
                public void onFailed(String errorCode, String errorMsg) {
                    mDataCallback.onFailed(errorCode, errorMsg);
                }

                @Override
                public void onSuccess(E2eePreEntity response) {
                    mSid = response.getE2eeSid();
                    String random = response.getRandom();
                    String pubKey = response.getPublicKey();
                    mPin = E2eeUtils.getLoginOrSetPin(mSid, mPwd, pubKey, random);
                    if (TextUtils.isEmpty(mPin)) {
                        mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                    } else {
                        E2eeLoginProtocol.super.execute();
                    }
                }
            }).execute();
        } else {
            super.execute();
        }
        return null;
    }

    @Override
    public void packData() {
        mBodyParams.put("walletId", mWalletId);
        mBodyParams.put("password", mPin);
        mBodyParams.put("e2eeSid", mSid);
        mBodyParams.put("account", mAccount);
    }
}

