package cn.swiftpass.httpcore.api.protocol;

import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;

import java.util.HashMap;

import cn.swiftpass.httpcore.BuildConfig;
import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.entity.ErrorEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.utils.HeaderUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.NetworkUtil;
import cn.swiftpass.httpcore.utils.encry.DecryptNetworkData;
import cn.swiftpass.httpcore.utils.encry.EncryptUtils;
import cn.swiftpass.httpcore.utils.encry.SignUtils;

public class BaseProtocol extends AbstractProtocol<Void> {
    public static final String TAG = BaseProtocol.class.getSimpleName();
    public String mUrl = "";
    public JSONObject mBodyParams;
    //是否协议加密
    public boolean mEncryptFlag = true;

    /**
     * 默认都需要上传header  部分接口不需要  例如埋点
     */
    public boolean mRequestHeader = true;


    public NetWorkCallbackListener mDataCallback;
    public String mAESKey = "";
    public Gson mGson;
    public boolean mPostFlag = true;
    public boolean useCustomizeBodyKey = false;


    @Override
    public Void execute() {
        if (!NetworkUtil.isNetworkAvailable()) {
            ErrorEntity errorEntity = new ErrorEntity();
            errorEntity.setErrorcode(ErrorCode.CONTENT_TIME_OUT.code);
//            try {
//                Activity activity = MyActivityManager.getInstance().getCurrentActivity();
//                if (activity != null) {
//                } else {
//                    errorEntity.setErrormsg(ProjectApp.getContext().getString(R.string.string_network_available));
//                }
//            } catch (Exception e) {
//                errorEntity.setErrormsg(errorMsg);
//            }
            if (HttpCoreKeyManager.getInstance().getContext() != null) {
                //多进程 埋点进程有问题
                try {
                    String errorMsg = HttpCoreKeyManager.getInstance().getContext().getString(HttpCoreKeyManager.getInstance().getNetWorkErrorStr());
                    errorEntity.setErrormsg(errorMsg);
                } catch (Exception e) {
                    if (BuildConfig.DEBUG) {
                        e.printStackTrace();
                    }

                }
            }


            if (mDataCallback == null) {
                return null;
            }
            mDataCallback.onFailed(errorEntity.getErrorcode(), errorEntity.getErrormsg());
            return null;
        }
        mAESKey = EncryptUtils.getAESKey();
        mGson = new Gson();
        mBodyParams = new JSONObject();
        final String requestUrl = getUrl();

        packData();
        sendRequest(requestUrl, mBodyParams);

        return null;
    }

    /**
     * 默认所有接口都需要依赖登录态   部分不需要依赖登录态接口单独重写该方法
     */
    @Override
    public boolean isNeedLogin() {
        return true;
    }

    @Override
    public boolean isEncrypt() {
        return mEncryptFlag;
    }

    protected void sendRequest(final String url, final JSONObject bodyParams) {
        JSONObject postData = makePostData(bodyParams);
        //自定义头部字段
        HashMap<String, Object> headMap = new HashMap<>();


        if (mEncryptFlag) {
            headMap.put(NetworkConstants.NEED_ENCRYPT, NetworkConstants.STATUS_TRUE);
        } else {
            headMap.put(NetworkConstants.NEED_ENCRYPT, NetworkConstants.STATUS_FALSE);
        }
        if (ignoreInterceptor()) {
            headMap.put(NetworkConstants.IGNORE_INTERCEPTOR, NetworkConstants.STATUS_TRUE);
        } else {
            headMap.put(NetworkConstants.IGNORE_INTERCEPTOR, NetworkConstants.STATUS_FALSE);
        }
        if (isNeedLogin() && CacheManagerInstance.getInstance().isLogin() && !CacheManagerInstance.getInstance().isHasSessionId()) {
            headMap.put(NetworkConstants.NEED_LOGIN, NetworkConstants.STATUS_TRUE);
        } else {
            headMap.put(NetworkConstants.NEED_LOGIN, NetworkConstants.STATUS_FALSE);
        }
        String timetamp = "";
        if (postData != null) {
            JSONObject header = postData.getJSONObject("header");
            if (header != null) {
                timetamp = header.getString(HttpCoreRequestParams.TIMETAMP);
            }
        }
        final DecryptNetworkData decryptNetworkData = new DecryptNetworkData();
        decryptNetworkData.setTimetamp(timetamp);

        OkHttpManager.OkHttpCallback okHttpCallback = new OkHttpManager.OkHttpCallback() {
            @Override
            public void onSuccess(String response) {
                HttpLogUtils.i(TAG, "response: " + response);
                if (mDataCallback == null) {
                    return;
                }
                if (mDataCallback.mType == String.class) {
                    onSuccessCallback(response);
                } else {
                    decryptNetworkData.decryptResponse(response, BaseProtocol.this, mDataCallback.mType);
                }
            }

            @Override
            public void onFailure(String errorCode, String errorMsg) {
                onFailCallback(errorCode, errorMsg);
            }
        };

        if (mPostFlag) {
            if (postData != null) {
                OkHttpManager.getInstance().doPostAsyn(url, headMap, postData.toString(), okHttpCallback);
            }
        } else {
            OkHttpManager.getInstance().doGetAsyn(url, headMap, okHttpCallback);
        }
    }

    protected void putIngoreNullParams(String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            mBodyParams.put(key, value);
        }
    }

//    public void sendAnalysisEvent(AnalysisBaseEntity analysisBaseEntity) {
//        if (analysisBaseEntity == null) {
//            return;
//        }
//        JSONObject jsonObject = analysisBaseEntity.getJsonObject();
//        AnalysisManager.getInstance().sendAnalysisEvent(jsonObject);
//    }

    @Override
    public void onSuccessCallback(Object o) {
        if (null != mDataCallback) {
            mDataCallback.onSuccess(o);
        }
    }

    @Override
    public void onFailCallback(String errorCode, String errorMsg) {
        if (checkCustomErrorCode(errorCode, errorMsg)) {
            return;
        }
        if (null != mDataCallback) {
            if (TextUtils.isEmpty(errorMsg)) {
                errorMsg = errorCode;
            }
            mDataCallback.onFailed(errorCode, errorMsg);
        }
    }

    /**
     * 参数拼接
     *
     * @param mBodyParams
     * @return
     */
    public JSONObject makePostData(JSONObject mBodyParams) {
        JSONObject jsonObject = new JSONObject();
        try {
            //不需要传header等参数
            if (!mRequestHeader) {
                //使用自定义body字段
                if (useCustomizeBodyKey) {
                    HttpLogUtils.i(TAG, "requestparams: " + mBodyParams.toJSONString());
                    return mBodyParams;
                } else {
                    jsonObject.put("body", mBodyParams);
                }
            } else {
                jsonObject.put("header", HeaderUtils.getHeaders());
                jsonObject.put("body", mBodyParams);
            }

        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return jsonObject;
    }


    public void packData() {

    }

    public String getUrl() {
        return (HttpCoreKeyManager.getInstance().getBaseUrl() + mUrl);
    }

    @Override
    public boolean verifySign(String sign, String data) {
        //服务器用服务器Rsa私钥加签  客户端用服务器公钥验签
        return SignUtils.verify(sign, data, HttpCoreKeyManager.getInstance().getServerPubKey());
    }
}
