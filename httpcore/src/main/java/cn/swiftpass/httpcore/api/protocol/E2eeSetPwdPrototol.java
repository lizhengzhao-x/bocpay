package cn.swiftpass.httpcore.api.protocol;

import android.text.TextUtils;

import cn.swiftpass.httpcore.api.NetWorkCallbackListener;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.entity.E2eePreEntity;
import cn.swiftpass.httpcore.utils.HttpsUtils;
import cn.swiftpass.httpcore.utils.encry.E2eeUtils;


/**
 * @author Created by ramon on 2018/8/7.
 * 设置和重置密码 弃用
 */

public class E2eeSetPwdPrototol extends BaseProtocol {
    private String mPwd;
    private String mPin;
    private String mSid;

    public E2eeSetPwdPrototol(String pwd, NetWorkCallbackListener dataCallback) {
        mDataCallback = dataCallback;
        mUrl = "XXXSetPwdUrl";
        mPwd = pwd;
    }

    @Override
    public Void execute() {
        new E2eePreProtocol(new NetWorkCallbackListener<E2eePreEntity>() {
            @Override
            public void onFailed(String errorCode, String errorMsg) {
                mDataCallback.onFailed(errorCode, errorMsg);
            }

            @Override
            public void onSuccess(E2eePreEntity response) {
                mSid = response.getE2eeSid();
                String random = response.getRandom();
                String pubKey = response.getPublicKey();
                mPin = E2eeUtils.getLoginOrSetPin(mSid, mPwd, pubKey, random);
                if (TextUtils.isEmpty(mPin)) {
                    mDataCallback.onFailed(ErrorCode.CONTENT_TIME_OUT.code, HttpsUtils.getErrorString(ErrorCode.CONTENT_TIME_OUT));
                } else {
                    E2eeSetPwdPrototol.super.execute();
                }
            }
        }).execute();
        return null;
    }

    @Override
    public void packData() {
        mBodyParams.put("sid", mSid);
        mBodyParams.put("pin", mPin);
    }
}
