package cn.swiftpass.httpcore.api.protocol;

import android.util.Log;

import com.alibaba.fastjson.JSONObject;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.swiftpass.httpcore.api.NetworkConstants;
import cn.swiftpass.httpcore.config.ErrorCode;
import cn.swiftpass.httpcore.config.HttpCoreRequestParams;
import cn.swiftpass.httpcore.entity.AutoLoginSucEntity;
import cn.swiftpass.httpcore.entity.NetWorkEntity;
import cn.swiftpass.httpcore.event.AutoLoginEventEntity;
import cn.swiftpass.httpcore.manager.CacheManagerInstance;
import cn.swiftpass.httpcore.manager.HttpCoreKeyManager;
import cn.swiftpass.httpcore.manager.OkHttpManager;
import cn.swiftpass.httpcore.manager.SystemInitManager;
import cn.swiftpass.httpcore.utils.HeaderUtils;
import cn.swiftpass.httpcore.utils.HttpLogUtils;
import cn.swiftpass.httpcore.utils.encry.DecryptNetworkData;
import cn.swiftpass.httpcore.utils.encry.SignUtils;
import okhttp3.ResponseBody;

/**
 * 自动登录  同步请求  用来处理防止多线程多次调用自动登录接口
 */
public class AutoLoginSynProtocol extends AbstractProtocol<NetWorkEntity> {

    public static final String TAG = AutoLoginSynProtocol.class.getSimpleName();

    private String mUrl;
    private JSONObject mBodyParams;

    public AutoLoginSynProtocol() {
        mUrl = "api/login/auto";
    }

    private NetWorkEntity doSynPost(String url, String postData) {
        HashMap<String, Object> headMap = new HashMap<>();
        headMap.put(NetworkConstants.NEED_ENCRYPT, NetworkConstants.STATUS_TRUE);
        return OkHttpManager.getInstance().doPostSyn(url, headMap, postData);
    }

    @Override
    public NetWorkEntity execute() {
        mBodyParams = new JSONObject();

        JSONObject postData = makePostData(mBodyParams);
        NetWorkEntity netWorkEntity = doSynPost(getUrl(), postData.toString());

        String timetamp = "";
        JSONObject header = postData.getJSONObject("header");
        if (header != null) {
            timetamp = header.getString(HttpCoreRequestParams.TIMETAMP);
        }
        final DecryptNetworkData decryptNetworkData = new DecryptNetworkData();
        decryptNetworkData.setTimetamp(timetamp);

        try {
            if (netWorkEntity.success) {
                ResponseBody responseBody = netWorkEntity.response.body();
                if (responseBody != null) {
                   boolean successBusiness= decryptNetworkData.decryptResponse(responseBody.string(), AutoLoginSynProtocol.this, AutoLoginSucEntity.class);
                   //只有业务逻辑也是自动登录成功的时候才认为是自动登录成功，否则返回失败，让自动登录继续调用
                   if (successBusiness){
                       return netWorkEntity;
                   }
                }
            }
        } catch (Exception e) {
            return new NetWorkEntity(false, netWorkEntity.response);
        }
        return new NetWorkEntity(false, netWorkEntity.response);
    }

    public String getUrl() {
        return (HttpCoreKeyManager.getInstance().getBaseUrl() + mUrl);
    }

    /**
     * 需要上传的header和body参数字段
     *
     * @param mBodyParams
     * @return
     */
    private JSONObject makePostData(JSONObject mBodyParams) {
        JSONObject jsonObject = new JSONObject();
        try {
            //摘要，签名处理  body加密放在加密拦截器处理
            jsonObject.put("header", HeaderUtils.getHeaders());
            jsonObject.put("body", mBodyParams);

            HttpLogUtils.i(TAG, "requestparams: " + jsonObject.toString());
        } catch (Exception e) {
            HttpLogUtils.e(TAG, Log.getStackTraceString(e));
        }
        return jsonObject;
    }

    @Override
    public boolean isNeedLogin() {
        return false;
    }

    @Override
    public boolean isEncrypt() {
        return true;
    }

    @Override
    public void onSuccessCallback(Object o) {
        if (o instanceof AutoLoginSucEntity) {
            AutoLoginSucEntity autoLoginSucEntity = (AutoLoginSucEntity) o;
            HttpCoreKeyManager.getInstance().saveSessionId(autoLoginSucEntity.getsId());
            HttpCoreKeyManager.getInstance().setUserId(autoLoginSucEntity.getWalletId());
            //自动登录成功保存卡列表
            CacheManagerInstance.getInstance().setCardEntities(autoLoginSucEntity.getCards().getRows());
            CacheManagerInstance.getInstance().saveOpenBiometricAuth(autoLoginSucEntity.isOpenBiometricAuth());
            CacheManagerInstance.getInstance().saveLoginStatus();
            EventBus.getDefault().postSticky(new AutoLoginEventEntity(AutoLoginEventEntity.EVENT_AUTO_LOGIN_SUCCESS, "",autoLoginSucEntity));
        }
    }

    @Override
    public void onFailCallback(String errorCode, String errorMsg) {
        if (checkCustomErrorCode(errorCode, errorMsg)) {
            return;
        }

        //todo fix 是否这里的清除也需要清卡数据
        if (ErrorCode.FPS_SESSION_INVALID.code.equalsIgnoreCase(errorCode)) {
            HttpCoreKeyManager.getInstance().saveSessionId("");
            HttpCoreKeyManager.getInstance().setUserId("");
            CacheManagerInstance.getInstance().saveLogoutStatus();
            SystemInitManager.getInstance().setValidShowAccount(false);
        }
        EventBus.getDefault().postSticky(new AutoLoginEventEntity(AutoLoginEventEntity.EVENT_AUTO_LOGIN_FAIL, errorCode));

    }

    @Override
    public boolean verifySign(String sign, String data) {

        return SignUtils.verify(sign, data, HttpCoreKeyManager.getInstance().getServerPubKey());
    }

    @Override
    public void goMainPage(final String msg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startMainPage(msg);
        }
    }

    @Override
    public void goLoginPage(final String errorMsg) {
        if (HttpCoreKeyManager.getInstance().getHttpRequestCallBack() != null) {
            HttpCoreKeyManager.getInstance().getHttpRequestCallBack().startLoginPage(errorMsg);
        }
    }
}
