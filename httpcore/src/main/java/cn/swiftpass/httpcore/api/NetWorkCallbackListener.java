package cn.swiftpass.httpcore.api;

import android.util.Log;

import com.google.gson.internal.$Gson$Types;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import cn.swiftpass.httpcore.utils.HttpLogUtils;


public abstract class NetWorkCallbackListener<T> {
    public NetWorkCallbackListener() {
        mType = getSuperclassTypeParameter(getClass());
    }

    static Type getSuperclassTypeParameter(Class<?> subclass) {
        Type superclass = subclass.getGenericSuperclass();
        if (superclass instanceof Class) {
        }
        ParameterizedType parameterized = null;
        try {
            parameterized = (ParameterizedType) superclass;
        } catch (Exception e) {
            HttpLogUtils.e("NetWorkCallbackListener", Log.getStackTraceString(e));
        }
        if (parameterized == null) {
            return null;
        }

        return $Gson$Types.canonicalize(parameterized.getActualTypeArguments()[0]);
    }

    public Type mType;

    /**
     * 执行前调用的方法
     */

//    public abstract void onPreExecute(Request request);

    /**
     * 执行失败调用的方法
     *
     * @param errorCode
     * @param errorMsg
     */

    public abstract void onFailed(String errorCode, String errorMsg);


    /**
     * 执行成功调用的方法
     *
     * @param response
     */
    public abstract void onSuccess(T response);


}
