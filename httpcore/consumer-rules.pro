# OkHttp
-dontwarn com.squareup.okhttp.**
-keep class com.squareup.okhttp.** {*;}
-keep interface com.squareup.okhttp.** {*;}
-dontwarn okio.**
## okhttp3[version_logging-interceptor 3.3.1]
-dontwarn com.squareup.okhttp3.**
-keep class com.squareup.okhttp3.** { *;}
-keep interface com.squareup.okhttp3.** {*;}
-dontwarn okio.**

#okhttp
-dontwarn okhttp3.**
-keep class okhttp3.**{*;}

#okio
-dontwarn okio.**
-keep class okio.**{*;}


#---------------------------------1.实体类---------------------------------
-keep class cn.swiftpass.httpcore.entity.** { *;}
-keep class ccn.swiftpass.httpcore.utils.** {*;}
#-------------------------------------------------------------------------
