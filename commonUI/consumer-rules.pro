#zxingcore
-dontwarn com.google.zxing.**
-keep class com.google.zxing.** {*;}
    -keepclassmembers class * extends android.app.Activity{
        public void *(android.view.View);
    }

-keep class cn.swiftpass.boc.commonui.base.utils.** {*;}