package cn.swiftpass.boc.commonui.base.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.swiftpass.boc.commonui.R;
import cn.swiftpass.boc.commonui.base.utils.ImageUtil;

public class CircleProgressGroup extends FrameLayout {
    //默认值
    private final int defaultFinishedColor = Color.rgb(216, 28, 80);
    private final int defaultUnfinishedColor = Color.rgb(216, 216, 216);
    private final int defaultTextColor = Color.rgb(0, 0, 0);
    private final int defaultTextTipColor = Color.rgb(164, 165, 165);
    private final int defaultInnerBackgroundColor = Color.TRANSPARENT;
    private final int defaultMax = 100;
    private final int defaultStartingDegree = 0;
    private float defaultStrokeWidth = 0;


    private CircleProgressView circleProgressView;
    private LinearLayout contentLayout;
    private float innerCircleWidth;
    private TextView circleProgressTv;
    private TextView textTipTv;

    private int finishedStrokeColor;
    private int unfinishedStrokeColor;
    private int maxProgress;
    private float progress;
    private float finishedStrokeWidth;
    private float outCircleSize = 0;
    private float unfinishedStrokeWidth;
    private int innerBackgroundColor;
    private int startingDegree;

    private String textProgress;
    private int textColor;
    private int textTipColor;
    private float textSize;
    private String textTip;
    private float textTipSize;


    private float defaultTextSize;
    private float defaultTextTipSize;

    public CircleProgressGroup(Context context) {
        this(context, null);
    }


    public CircleProgressGroup(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleProgressGroup(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDefault();
        final TypedArray attributes = context.getTheme()
                .obtainStyledAttributes(attrs, R.styleable.CircleProgress, defStyleAttr, 0);
        initByAttributes(attributes);
        attributes.recycle();
        initView();
    }


    private void initDefault() {
        defaultStrokeWidth = ImageUtil.dp2px(getResources(), 10);
        defaultTextSize = ImageUtil.sp2px(getResources(), 15);
        defaultTextTipSize = ImageUtil.sp2px(getResources(), 9);
    }


    protected void initByAttributes(TypedArray attributes) {
        finishedStrokeColor = attributes
                .getColor(R.styleable.CircleProgress_circle_finished_color, defaultFinishedColor);
        unfinishedStrokeColor = attributes
                .getColor(R.styleable.CircleProgress_circle_unfinished_color, defaultUnfinishedColor);

        maxProgress = attributes.getInt(R.styleable.CircleProgress_progress_max, defaultMax);
        progress = attributes.getFloat(R.styleable.CircleProgress_circle_progress, 0);
        finishedStrokeWidth = attributes
                .getDimension(R.styleable.CircleProgress_circle_finished_stroke_width, defaultStrokeWidth);
        unfinishedStrokeWidth = attributes
                .getDimension(R.styleable.CircleProgress_circle_unfinished_stroke_width, defaultStrokeWidth);
        outCircleSize = attributes
                .getDimension(R.styleable.CircleProgress_circle_out_circle_size, 0);

        if (attributes.getString(R.styleable.CircleProgress_circle_text) != null) {
            textProgress = attributes.getString(R.styleable.CircleProgress_circle_text);
        }
        if (attributes.getString(R.styleable.CircleProgress_circle_text_tip) != null) {
            textTip = attributes.getString(R.styleable.CircleProgress_circle_text_tip);
        }

        textColor = attributes
                .getColor(R.styleable.CircleProgress_circle_text_color, defaultTextColor);
        textTipColor = attributes
                .getColor(R.styleable.CircleProgress_circle_text_tip_color, defaultTextTipColor);
        textSize = attributes
                .getDimension(R.styleable.CircleProgress_circle_text_size, defaultTextSize);
        textTipSize = attributes
                .getDimension(R.styleable.CircleProgress_circle_text_tip_size, defaultTextTipSize);


        startingDegree = attributes
                .getInt(R.styleable.CircleProgress_circle_starting_degree, defaultStartingDegree);
        innerBackgroundColor = attributes
                .getColor(R.styleable.CircleProgress_circle_background_color, defaultInnerBackgroundColor);


    }


    private void initView() {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.include_circle_progress, this);
        circleProgressView = rootView.findViewById(R.id.view_circle_progress);
        contentLayout = rootView.findViewById(R.id.ll_content);
        circleProgressTv = rootView.findViewById(R.id.tv_circle_progress);
        textTipTv = rootView.findViewById(R.id.tv_text_tip);
        initCircleProgress();
        initText();
    }

    private void initCircleProgress() {
     /*   private int finishedStrokeColor;
        private int unfinishedStrokeColor;
        private int maxProgress;
        private float progress;
        private float finishedStrokeWidth;
        private float outCircleSize = 0;
        private float unfinishedStrokeWidth;
        private int innerBackgroundColor;
        private int startingDegree;*/
        circleProgressView.setFinishedStrokeColor(finishedStrokeColor);
        circleProgressView.setUnfinishedStrokeColor(unfinishedStrokeColor);
        circleProgressView.setMax(maxProgress);
        circleProgressView.setProgress(progress);
        circleProgressView.setFinishedStrokeWidth(finishedStrokeWidth);
        circleProgressView.setOutCircleSize(outCircleSize);
        circleProgressView.setUnfinishedStrokeWidth(unfinishedStrokeWidth);
        circleProgressView.setInnerBackgroundColor(innerBackgroundColor);
        circleProgressView.setStartingDegree(startingDegree);
        circleProgressView.initPainters();
    }

    private void initText() {
        circleProgressTv.setTextColor(textColor);
        textTipTv.setTextColor(textTipColor);

        if (!TextUtils.isEmpty(textProgress)) {
            circleProgressTv.setText(textProgress);
        } else if (circleProgressView.getProgress() >= 0) {
            circleProgressTv.setText("" + ((int) circleProgressView.getProgress()) + "%");
        }
        if (!TextUtils.isEmpty(textTip)) {
            textTipTv.setText(textTip);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        innerCircleWidth = circleProgressView.getWidth() - 2 * circleProgressView.getDelta();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int width = (int) (innerCircleWidth * 0.65);
        int height = (int) (innerCircleWidth);
        ViewGroup.LayoutParams layoutParams = contentLayout.getLayoutParams();
        layoutParams.width = width;
        layoutParams.height = height;
        contentLayout.setLayoutParams(layoutParams);
        invalidate();
    }

    public void setProgress(float progress) {
        circleProgressTv.setText("" + (int) (progress * 100) + "%");
        circleProgressView.setProgress(progress * 100);
        invalidate();
    }
}
