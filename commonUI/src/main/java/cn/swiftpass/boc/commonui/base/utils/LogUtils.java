package cn.swiftpass.boc.commonui.base.utils;

import android.util.Log;

import cn.swiftpass.boc.commonui.BuildConfig;


/**
 * Log
 *
 * @author gc
 * @since 1.1
 */
public class LogUtils {

    private LogUtils() {
        throw new UnsupportedOperationException("cannot be instantiated");
    }

    public static final boolean isLogDebug = BuildConfig.DEBUG;
    public static final boolean isWriteLog = true;

    public static void d(String tag, String msg) {
        if (isLogDebug) {
            Log.d(tag, msg + "");
        }
    }

    public static void d(Object object, String msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void d(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void i(String tag, String msg) {
        if (isLogDebug) {
            Log.i(tag, msg + "");
        }
    }

    public static void i(Object object, String msg) {
        if (isLogDebug) {
            Log.i(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void i(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void w(String tag, String msg) {
        if (isLogDebug) {
            Log.w(tag, msg + "");
        }
    }

    public static void w(Object object, String msg) {
        if (isLogDebug) {
            Log.w(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void w(Object object, Object msg) {
        if (isLogDebug) {
            Log.d(object.getClass().getSimpleName(), msg + "");
        }
    }

    public static void e(String tag, String msg) {
        if (isLogDebug) {
            Log.e(tag, msg + "");
        }
    }

    public static void e(Object object, String msg) {
        if (isLogDebug) {
            Log.e(object.getClass().getSimpleName(), msg + "");
        }
    }


}
