package cn.swiftpass.boc.commonui.base.mvp;

import java.lang.ref.WeakReference;


public class BasePresenter<V extends IView> implements IPresenter<V> {
    private WeakReference<V> viewRef;

    protected V getView() {
        if (viewRef == null) return null;
        return viewRef.get();
    }

    protected boolean isViewAttached() {
        return viewRef != null && viewRef.get() != null;
    }

    @Override
    public void onAttachView(V view) {
        viewRef = new WeakReference<V>(view);
    }

    /**
     * V P层的解绑
     */
    @Override
    public void onDetachView() {
        if (viewRef != null) {
            viewRef.clear();
            viewRef = null;
        }
    }
}
