package cn.swiftpass.boc.commonui.base.mvp;


public interface IPresenter<V extends IView> {
    void onAttachView(V view);

    void onDetachView();
}
