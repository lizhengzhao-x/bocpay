package cn.swiftpass.boc.commonui.base;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;

import com.google.zxing.Result;

import cn.swiftpass.boc.commonui.base.widget.ViewfinderView;
import cn.swiftpass.boc.commonui.base.zxing.camera.CameraManager;


public interface QrcodeScanListener {

    void scanResult(boolean isOk, String result);

    void submitData(String result, boolean status);

    void drawViewfinder();

    void handleDecode(Result obj, Bitmap barcode);

    void setResult(int resultOk, Intent obj);

    ViewfinderView getViewfinderView();

    Handler getHandler();

    CameraManager getCameraManager();

    Context getContext();
}
