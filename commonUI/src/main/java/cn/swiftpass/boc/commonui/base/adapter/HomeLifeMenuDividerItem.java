package cn.swiftpass.boc.commonui.base.adapter;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by ZhangXinchao on 2019/8/30.
 */
public class HomeLifeMenuDividerItem extends RecyclerView.ItemDecoration {
    private  int mFirstOffset;
    private int mSpace;
    private int color;
    private Paint mPaint;



    /**
     * 自定义宽度的透明分割线
     *
     * @param space 指定宽度
     */
    public HomeLifeMenuDividerItem(int space) {
        this(space,space, Color.TRANSPARENT);
    }

    public HomeLifeMenuDividerItem(int space, int firstOffset) {
        this(space,firstOffset, Color.TRANSPARENT);
    }

    /**
     * 自定义宽度，并指定颜色的分割线
     *
     * @param space 指定宽度
     * @param color 指定颜色
     */

    public HomeLifeMenuDividerItem(int space, int firstOffset,int color) {
        this.mSpace = space;
        this.color = color;
        this.mFirstOffset=firstOffset;
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        //得到View的位置
        if (parent.getAdapter() != null && parent.getChildAdapterPosition(view) == 0) {
            outRect.set(mFirstOffset, 0, 0, 0);
        } else {
            outRect.set(mSpace, 0, 0, 0);
        }

    }

}
