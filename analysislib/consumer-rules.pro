-keep class  * implements java.io.Serializable {
        public <methods>;
    }
-keepnames class * implements java.io.Serializable
-keepclassmembers class * implements java.io.Serializable {
        static final long serialVersionUID;
        private static final java.io.ObjectStreamField[] serialPersistentFields;
        private void writeObject(java.io.ObjectOutputStream);
        private void readObject(java.io.ObjectInputStream);
        java.lang.Object writeReplace();
        java.lang.Object readResolve();
    }
-keep class com.bocpay.analysislib {*;}
-keep public class * extends android.app.Activity