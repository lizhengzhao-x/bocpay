package com.bocpay.analysis;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.concurrent.LinkedBlockingQueue;

public class AnalysisService extends Service implements AnalysisResultListener {

    private static final String TAG = "AnalysisService";
    /**
     * 阻塞队列 解决读取问题
     */
    private LinkedBlockingQueue<JSONObject> eventList = new LinkedBlockingQueue<>();
    /**
     * 发送给服务器的埋点事件json数组
     */
    private JSONArray jsonCacheList = new JSONArray();
    private long lastUploadTime = 0L;
    private SendTask task;
    private boolean mForceExit;
    /**
     * 进程之间数据接收
     */
    private MessengerHandler mHandler = new MessengerHandler();

    /**
     * 进程之间通讯
     */
    private Messenger mMessenger = new Messenger(mHandler);

    @Override
    public void fail() {
        Log.d(TAG, "AnalysisService ->upload data failed");
    }

    @Override
    public void success() {
        //重置上传时间
        lastUploadTime = System.currentTimeMillis();
        jsonCacheList.clear();
        Log.d(TAG, "AnalysisService ->upload data success");
    }

    private class MessengerHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //取出客户端的消息内容
            Log.d(TAG, "receive msg ");
            Bundle bundle = msg.getData();
            if (bundle == null) {
                return;
            }
            String messageType = bundle.getString(AnalysisManager.MESSAGE_TYPE);
            if (AnalysisManager.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                String eventJson = bundle.getString(AnalysisManager.MESSAGE_KEY);
                if (!TextUtils.isEmpty(eventJson)) {
                    JSONObject jsonObject = JSONObject.parseObject(eventJson);
                    try {
                        eventList.put(jsonObject);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (AnalysisManager.MESSAGE_TYPE_FORCE.equals(messageType)) {
                sendActionData();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "AnalysisService ->onCreate");
        lastUploadTime = System.currentTimeMillis();
        task = new SendTask();
        mForceExit = false;
        task.start();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mForceExit = true;
        if (task != null) {
            task.interrupt();
        }
        Log.d(TAG, "AnalysisService ->onDestroy");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    private synchronized void sendActionData() {
        Log.d(TAG, "sendActionData ->called:");
        //重置上传时间
        lastUploadTime = System.currentTimeMillis();
        Log.d(TAG, "sendActionData ->jsonList size:" + eventList.size());
        for (int i = 0; i < AnalysisManager.UPLOAD_EVERY_COUNT; i++) {
            if (eventList.peek() != null) {
                jsonCacheList.add(eventList.poll());
            } else {
                break;
            }
        }
        Log.d(TAG, "sendActionData ->jsonCacheList size:" + jsonCacheList.size());
        if (jsonCacheList.size() > 0) {
            Log.d(TAG, "AnalysisService ->upload data");
            /**
             * {"current_address":"BOC_register_two_page","app_name":"bocpay","record_id":"8cfd6c8a0cec4132a043ad5850b716fc","app_version":"1.0.19",
             * "device_id":"d12f5c7c-be7e-40af-8e81-a80809dd5499_1.0.19","date_time":"2020-03-18 02:39:57",
             * "dataType":"PA_REG_FLOW_POINT","app_system":"AOS","time_duration":"00:02","last_address":"BOC_register_first_page"}
             */
            Log.d(TAG, "AnalysisService ->" + jsonCacheList.getString(0));
            AnalysisManager.getInstance().send(jsonCacheList, this);
        } else {
            // mReentrantLock.unlock();
        }
    }

    /**
     * while 循环必须放在单独线程中 否则会影响service启动
     */
    private class SendTask extends Thread {
        @Override
        public void run() {
            while (!mForceExit) {

                if ((System.currentTimeMillis() - lastUploadTime) > (AnalysisManager.UPLOAD_EVERY_TIME * 1000)) {
                    //时间达到
                    Log.d(TAG, "AnalysisService ->sendActionData time");
                    sendActionData();
                } else if (eventList.size() > AnalysisManager.UPLOAD_EVERY_COUNT) {
                    //条目达到
                    Log.d(TAG, "AnalysisService ->sendActionData size");
                    sendActionData();
                }
            }
        }
    }
}
