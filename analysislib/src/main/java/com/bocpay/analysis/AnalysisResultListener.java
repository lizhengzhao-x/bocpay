package com.bocpay.analysis;

/**
 * 发送埋点结果接口
 */
public interface AnalysisResultListener {

    void fail();

    void success();
}
