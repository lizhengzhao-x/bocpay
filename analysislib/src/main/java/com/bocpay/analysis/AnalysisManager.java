package com.bocpay.analysis;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import static android.content.Context.BIND_AUTO_CREATE;

/**
 * app 埋点功能 单独进程
 */
public class AnalysisManager {

    private String TAG = "AnalysisService";

    public static final String MESSAGE_KEY = "MESSAGE_KEY";
    public static final String MESSAGE_TYPE = "MESSAGE_TYPE";
    public static final int UPLOAD_EVERY_TIME = 5 * 60;
    public static final int UPLOAD_EVERY_COUNT = 30;

    public static final String MESSAGE_TYPE_MESSAGE = "MESSAGE_TYPE_MESSAGE";
    public static final String MESSAGE_TYPE_FORCE = "MESSAGE_TYPE_FORCE";

    private AnalysisEventListener analysisEventListener;
    private Context mContext;

    public void setAnalysisEventListener(AnalysisEventListener analysisEventListener) {
        this.analysisEventListener = analysisEventListener;
    }

    public void send(JSONArray jsonArray, AnalysisResultListener eventResultListener) {
        if (analysisEventListener != null) {
            analysisEventListener.sendEvent(jsonArray, eventResultListener);
        }
    }

    /**
     * 埋点统计service
     */
    private Messenger mStatisticalService;

    private static class AnalysisHolder {
        private static AnalysisManager instance = new AnalysisManager();
    }

    public static AnalysisManager getInstance() {
        return AnalysisManager.AnalysisHolder.instance;
    }

    public void sendAnalysisEvent(JSONObject jsonObject) {
        Log.i(TAG, "sendAnalysisEvent");
        if (mStatisticalService == null || jsonObject == null) {
            return;
        }
        try {
            Message messageItem = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_TYPE, MESSAGE_TYPE_MESSAGE);
            bundle.putString(MESSAGE_KEY, jsonObject.toJSONString());
            messageItem.setData(bundle);
            if (mStatisticalService != null && mStatisticalService.getBinder().isBinderAlive()) {
                mStatisticalService.send(messageItem);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * sdk 初始化 这个参数只需要传一次
     * 客户端deviceID + app versionNo 组成
     */
    public void init(Context context) {
        mContext = context;
        startAnalysisService(context);
    }

    /**
     * 启动埋点上传服务
     */
    private void startAnalysisService(Context mContext) {
        Log.i(TAG, "startAnalysisService--->");
        Intent intent = new Intent(mContext, AnalysisService.class);
        mContext.bindService(intent, mConnection, BIND_AUTO_CREATE);
    }

    /**
     * 停止埋点上传服务 app推出时候需要调用
     * 登录/非登录态
     */
    public void stopAnalysisService() {
        Log.i(TAG, "stopAnalysisService--->");
        if (mConnection == null || mContext == null) return;
        try {
            mContext.unbindService(mConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 1.定时 2.定量 3.app退到后台直接上传
     */
    public void uploadDataWhenAppGoBackGround() {
        if (mStatisticalService == null) {
            return;
        }
        try {
            Message messageItem = Message.obtain();
            Bundle bundle = new Bundle();
            bundle.putString(MESSAGE_TYPE, MESSAGE_TYPE_FORCE);
            messageItem.setData(bundle);

            if (mStatisticalService != null && mStatisticalService.getBinder().isBinderAlive()) {
                mStatisticalService.send(messageItem);
            }

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private IBinder.DeathRecipient mDeathRecipient = new IBinder.DeathRecipient() {

        @Override
        public void binderDied() {
            // 当绑定的service异常断开连接后，自动执行此方法

            if (mStatisticalService != null) {
                // 当前绑定由于异常断开时，将当前死亡代理进行解绑        mIMyAidlInterface.asBinder().unlinkToDeath(mDeathRecipient, 0);
                //  重新绑定服务端的service
                Intent intent = new Intent(mContext, AnalysisService.class);
                mContext.bindService(intent, mConnection, BIND_AUTO_CREATE);
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mStatisticalService = new Messenger(service);
            //绑定成功回调
            //获取服务端提供的接口
            try {
                // 注册死亡代理
                if (mStatisticalService != null) {
                    service.linkToDeath(mDeathRecipient, 0);
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };

    public interface AnalysisEventListener {
        void sendEvent(JSONArray jsonArray, AnalysisResultListener eventResultListener);
    }
}
