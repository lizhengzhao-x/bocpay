package com.bocpay.analysis;

public interface AnalysisInterface {

    /**
     * 发送埋点事件
     */
    void sendAnalysisEvent();

}
