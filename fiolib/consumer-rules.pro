# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\JavaTools\Android\android-sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontwarn com.daon.**,org.bouncycastle.**, com.samsung.**
-keepparameternames
-renamesourcefileattribute SourceFile
-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,*Annotation*,EnclosingMethod

-dontoptimize

# Preserve all SDK public classes, and their public and protected fields and methods.
# Preserve private field names of model classes to get JSON libraries to work
# Preserve activities to allow Android OS to create them using reflection

-keep public class com.daon.fido.client.sdk.core.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.exception.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.handler.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.model.* {
    public protected *;
    private <fields>;
}
-keep public class com.daon.fido.client.sdk.transaction.* {
    public protected *;
}
-keep public class com.daon.fido.client.sdk.ui.* {
    public protected *;
}
-keep class com.daon.fido.client.sdk.init.InitialiseSdkActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.auth.UafClientAuthenticateActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.dereg.UafClientDeregisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.reg.UafClientRegisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.auth.UafAsmAuthenticateActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.dereg.UafAsmDeregisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.reg.UafAsmRegisterActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.auth.UafAsmGetRegistrationsActivity {
    public protected *;
}
-keep class com.daon.fido.client.sdk.uaf.UafMessageUtils {
    public protected *;
}

-keep class com.daon.sdk.authenticator.capture.CaptureActivity {
    public protected *;
}

# Preserve all class for Tradelink
-keep public class com.tradelink.boc.authapp.exception.* {
    public protected *;
}
-keep public class com.tradelink.boc.authapp.task.* {
    public protected *;
}
-keep public class com.tradelink.boc.authapp.model.* {
    public protected *;
    private <fields>;
}
-keep public class com.tradelink.boc.authapp.ui.* {
    public protected *;
}


-keep class com.tradelink.boc.sotp.model.** {*;}




