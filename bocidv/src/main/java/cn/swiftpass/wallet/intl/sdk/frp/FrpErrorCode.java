package cn.swiftpass.wallet.intl.sdk.frp;

/**
 * @name bocbill-aos
 * @class name：com.idvlib
 * @class describe
 * @anthor zhangfan
 * @time 2020/4/24 13:47
 * @change
 * @chang time
 * @class describe
 */
public class FrpErrorCode {
    //FRP重试错误三次 要退出整个流程
    public static final String FRP_FAILED_TRY_MORE = "PJ008";
    public static final String FRP_FAILED_QUIT = "PJ009";

}
