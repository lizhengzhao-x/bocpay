package cn.swiftpass.wallet.intl.sdk.idv;

/**
 * @name bocbill-aos
 * @class name：com.idvlib
 * @class describe
 * @anthor zhangfan
 * @time 2020/4/24 13:47
 * @change
 * @chang time
 * @class describe
 */
public class IdvConstants {
    public static final String TRANSACTIONUNIQUEID = "transactionUniqueID";
    public static final String TRANSACTIONTYPE = "transactionType";
    public static final String IDVFID = "IdvFID4";
    public static final String IDVSEQNUM = "IdvSeqNum4";
    public static final String ERROR_MESSAGE = "ERROR_MESSAGE";
    public static final String ERROR_CODE = "ERROR_CODE";
    //    public static final String IDV_TRANSACTION_UNIQUE_ID = "transactionUniqueID";
//    public static final String IDV_TRANSACTION_TYPE = "transactionType";
    public static final String IDV_LANGUAGE = "language";
    public static final String IDV_DEVICE_ID = "deviceId";
    public static final String IDV_REGISTER_RECORD_ID = "registerRecordId";
    //    public static final String IDV_FID = "idvFID";
//    public static final String IDV_SEQ_NUM = "idvSeqNum";
    public static final String IDV_ERROR_CODE = "idvErrorCode";
    public static final String IDV_ERROR_MSG = "idvErrorMsg";
    public static final String APP_VERSION = "appVersion";

    public static final String IDV_BASE_MAINBACKGROUND_URI = "baseMainBackgroundUri";
    public static final String IDV_BASE_MAINBACKGROUND_RESID = "baseMainBackgroundResID";

}
