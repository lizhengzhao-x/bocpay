package cn.swiftpass.wallet.intl.sdk.frp;

/**
 * @name bocbill-aos
 * @class name：com.idvlib
 * @class describe
 * @anthor zhangfan
 * @time 2020/4/24 13:47
 * @change
 * @chang time
 * @class describe
 */
public class FrpConstants {
    public static final String FRP_TRANSACTION_UNIQUE_ID = "transactionUniqueID";
    public static final String FRP_TRANSACTION_TYPE = "transactionType";
    public static final String FRP_MIDV_SEQ_NUMBER = "MIDVSEQNUMBER";
    public static final String FRP_MIDV_SEQ_FID = "MIDVSEQFID";
    public static final String FRP_LANGUAGE = "language";
    public static final String FRP_DEVICE_ID = "deviceId";
    public static final String FRP_REGISTER_RECORD_ID = "registerRecordId";
    public static final String FRP_VOICE_ENABLE = "voiceEnable";
    public static final String APP_VERSION = "appVersion";
    public static final String FRP_ERROR_CODE = "frpErrorCode";
    public static final String FRP_ERROR_MSG = "frpErrorMsg";
}
