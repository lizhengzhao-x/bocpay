package cn.swiftpass.wallet.intl.sdk.idv;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.bocidv.R;
import com.idv.sdklibrary.api.IntegrateIdvApi;
import com.idv.sdklibrary.bean.UIImage;
import com.idv.sdklibrary.util.IdvConstant;

import java.security.cert.Certificate;

import cn.swiftpass.wallet.intl.sdk.utils.IdvSdkManager;


/**
 * 调用 idvsdk 过度activity 证书 transid等 都在此封装处理
 */
public class IdvTransitionActivity extends AppCompatActivity {

    private IntegrateIdvApi mIntegrateIdvApi;
    private String mTransactionUniqueID;
    private String mIdvFID4, mIdvSeqNum4, deviceId, recordRegisterdId, appVersion;
    private String language = "";
    private String TAG = "IdvTransitionActivity";
    private String transactionType;
    public static final String TRANSACTION_TYPE_REGISTER = "1";
    public static final String TRANSACTION_TYPE_RESET = "4";
    private String baseMainBackgroundUri;
    private String baseMainBackgroundResID;


    /**
     * @param mActivity
     * @param mTransactionUniqueID    idv 交易id 每次吊起不同
     * @param transactionType         交易类型 区分注册 忘记密码
     * @param idvLanguage             语言国际化
     * @param deviceId
     * @param registerRecordId
     * @param appVersion
     * @param requestIdvSdk
     * @param baseMainBackgroundUri   背景图的uri
     * @param baseMainBackgroundResID 背景图的resource id
     */
    public static void startActivityForResult(Activity mActivity, String mTransactionUniqueID, String transactionType, String idvLanguage, String deviceId, String registerRecordId, String appVersion, int requestIdvSdk,
                                              String baseMainBackgroundUri, String baseMainBackgroundResID) {

        Intent intent = new Intent(mActivity, IdvTransitionActivity.class);
        intent.putExtra(IdvConstants.TRANSACTIONUNIQUEID, mTransactionUniqueID);
        intent.putExtra(IdvConstants.TRANSACTIONTYPE, transactionType);
        intent.putExtra(IdvConstants.IDV_LANGUAGE, idvLanguage);
        intent.putExtra(IdvConstants.IDV_DEVICE_ID, deviceId);
        intent.putExtra(IdvConstants.IDV_REGISTER_RECORD_ID, registerRecordId);
        intent.putExtra(IdvConstants.APP_VERSION, appVersion);
        intent.putExtra(IdvConstants.IDV_BASE_MAINBACKGROUND_URI, baseMainBackgroundUri);
        intent.putExtra(IdvConstants.IDV_BASE_MAINBACKGROUND_RESID, baseMainBackgroundResID);
        mActivity.startActivityForResult(intent, requestIdvSdk);
    }


    /**
     * 防止app横竖屏切换 Android 8.0要求透明样式的activity不能再Manifest中指定横竖屏
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_idv_transition);
        mTransactionUniqueID = getIntent().getExtras().getString(IdvConstants.TRANSACTIONUNIQUEID);
        transactionType = getIntent().getExtras().getString(IdvConstants.TRANSACTIONTYPE);
        language = getIntent().getStringExtra(IdvConstants.IDV_LANGUAGE);
        deviceId = getIntent().getStringExtra(IdvConstants.IDV_DEVICE_ID);
        recordRegisterdId = getIntent().getStringExtra(IdvConstants.IDV_REGISTER_RECORD_ID);
        appVersion = getIntent().getStringExtra(IdvConstants.APP_VERSION);
        baseMainBackgroundUri = getIntent().getStringExtra(IdvConstants.IDV_BASE_MAINBACKGROUND_URI);
        baseMainBackgroundResID = getIntent().getStringExtra(IdvConstants.IDV_BASE_MAINBACKGROUND_RESID);
        goIdvAuthInfo();
    }


    private void goIdvAuthInfo() {
        Certificate[] certificates = IdvSdkManager.getInstance().getCertificates();
        mIntegrateIdvApi = new IntegrateIdvApi();
        IdvSdkManager.getInstance().setmIntegrateIdvApi(mIntegrateIdvApi);
        //每做完流程，第二个参数需要更换新的随机数
        mIntegrateIdvApi.configCertsArray("123", mTransactionUniqueID, "EWA-HK", "A", certificates, IdvTransitionActivity.this);
    }


    public void onErrorResult(String errorMsg, String errorCode) {
        Intent in = new Intent();
        in.putExtra(IdvConstants.IDVFID, "xxxxxxxxx");
        in.putExtra(IdvConstants.IDVSEQNUM, "xxxxxxxxx");
        in.putExtra(IdvConstants.ERROR_MESSAGE, errorMsg);
        in.putExtra(IdvConstants.ERROR_CODE, errorCode);
        setResult(RESULT_CANCELED, in);
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == IdvConstant.UNKNOW_ERROR_CODE) {
            String errorCode = data.getStringExtra("idvErrorCode");
            String errorMsg = data.getStringExtra("idvErrorMsg");
            Log.e(TAG, "未知错误！！");
            onErrorResult(errorMsg, errorCode);
            return;
        }
        if (requestCode == IdvConstant.CERT_REQUEST_CODE) {
            if (resultCode == IdvConstant.CERT_RESULT_SUCCESS) {
                //证书传入成功
                UIImage uiImage = null;
                if (TextUtils.isEmpty(baseMainBackgroundUri)) {
                    uiImage = new UIImage(null, baseMainBackgroundResID);
                } else {
                    uiImage = new UIImage(Uri.parse(baseMainBackgroundUri), baseMainBackgroundResID);
                }
                mIntegrateIdvApi.captureIDImage(getPackageName(), mTransactionUniqueID, transactionType, "EWA-HK", "A", language, "NM", "12313131313", appVersion, deviceId, recordRegisterdId, uiImage, IdvTransitionActivity.this);
            }

        } else if (requestCode == IdvConstant.CAPTURE_IDIMAGE_REQ_CODE) {

            Log.e(TAG, "IDVSDK:" + data.toString());
//            ActivitySkipUtil.startAnotherActivity(this, TransferByBankAccountActivity.class);
            if (resultCode == IdvConstant.CAPTURE_RESULT_OK) {
                mIdvFID4 = data.getStringExtra("idvFID");
                mIdvSeqNum4 = data.getStringExtra("idvSeqNum");
                Log.e(TAG, "IDVSDK:" + "mIdvFID4:" + mIdvFID4 + " mIdvSeqNum4:" + mIdvSeqNum4);
                Intent in = new Intent();
                in.putExtra(IdvConstants.IDVFID, mIdvFID4);
                in.putExtra(IdvConstants.IDVSEQNUM, mIdvSeqNum4);
                setResult(RESULT_OK, in);
                finish();
            } else if (resultCode == IdvConstant.CAPTURE_RESULT_FAIL) {
                String errorCode = data.getStringExtra("idvErrorCode");
                String errorMsg = data.getStringExtra("idvErrorMsg");
                onErrorResult(errorMsg, errorCode);
                //身份证扫描失败 or server请求失败
                Log.e(TAG, "IDVSDK:" + errorCode + "，errorMsg:" + errorMsg);
            }
        }
    }
}
