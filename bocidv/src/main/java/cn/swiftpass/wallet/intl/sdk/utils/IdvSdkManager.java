package cn.swiftpass.wallet.intl.sdk.utils;

import com.idv.sdklibrary.api.IntegrateIdvApi;

import java.security.cert.Certificate;

public class IdvSdkManager {

    private static IdvSdkManager instance;
    private static final Object INSTANCE_LOCK = new Object();

    public static IdvSdkManager getInstance() {
        if (instance == null) {
            synchronized (INSTANCE_LOCK) {
                if (instance == null) {
                    instance = new IdvSdkManager();
                }
            }
        }
        return instance;
    }


    public Certificate[] getCertificates() {
        return mCertificates;
    }

    private Certificate[] mCertificates;

    /**
     * 配置吊起idv证书配置
     *
     * @param mCertificatesIn
     */
    public void init(Certificate[] mCertificatesIn) {
        mCertificates = mCertificatesIn;
    }

    public IntegrateIdvApi getIntegrateIdvApi() {
        return mIntegrateIdvApi;
    }

    public void setmIntegrateIdvApi(IntegrateIdvApi mIntegrateIdvApiIn) {
        mIntegrateIdvApi = mIntegrateIdvApiIn;
    }

    /**
     * idv 流程 sdk 和 app 公用 对象 不能随便new 要公用
     */
    private IntegrateIdvApi mIntegrateIdvApi;
}
