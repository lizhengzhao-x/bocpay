package cn.swiftpass.wallet.intl.sdk.frp;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.bocidv.R;

import java.security.cert.Certificate;

import cn.cloudwalk.libproject.api.LiveBuilder;
import cn.cloudwalk.libproject.livelib.util.ActionContants;
import cn.swiftpass.wallet.intl.sdk.utils.IdvSdkManager;


/**
 * frp调用封装
 */
public class FrpTransitionActivity extends AppCompatActivity {

    private LiveBuilder mLiveBuilder;
    private String mTransactionUniqueID;

    private String mIdVSeqNumber;
    private String mIdVSeqFid;
    private String language = "";
    private String transactionType;
    private String deviceId;
    private String recordRegisterdId;
    private String appVersion;
    private boolean voiceEnable;


    public static void startActivityForResult(Activity mActivity, String transactionType, String mTransactionUniqueID, String idvLanguage, String deviceId, String registerRecordId, String appVersion, String seqNumber, String seqFid, boolean voiceEnable, int requestIdvSdk) {

        Intent intent = new Intent(mActivity, FrpTransitionActivity.class);
        intent.putExtra(FrpConstants.FRP_TRANSACTION_UNIQUE_ID, mTransactionUniqueID);
        intent.putExtra(FrpConstants.FRP_TRANSACTION_TYPE, transactionType);
        intent.putExtra(FrpConstants.FRP_LANGUAGE, idvLanguage);
        intent.putExtra(FrpConstants.FRP_DEVICE_ID, deviceId);
        intent.putExtra(FrpConstants.FRP_REGISTER_RECORD_ID, registerRecordId);
        intent.putExtra(FrpConstants.APP_VERSION, appVersion);

        intent.putExtra(FrpConstants.FRP_MIDV_SEQ_NUMBER, seqNumber);
        intent.putExtra(FrpConstants.FRP_VOICE_ENABLE, voiceEnable);
        intent.putExtra(FrpConstants.FRP_MIDV_SEQ_FID, seqFid);
        mActivity.startActivityForResult(intent, requestIdvSdk);
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_idv_transition);
        mTransactionUniqueID = getIntent().getExtras().getString(FrpConstants.FRP_TRANSACTION_UNIQUE_ID);
        mIdVSeqNumber = getIntent().getExtras().getString(FrpConstants.FRP_MIDV_SEQ_NUMBER);
        mIdVSeqFid = getIntent().getExtras().getString(FrpConstants.FRP_MIDV_SEQ_FID);
        transactionType = getIntent().getExtras().getString(FrpConstants.FRP_TRANSACTION_TYPE);
        language = getIntent().getStringExtra(FrpConstants.FRP_LANGUAGE);
        deviceId = getIntent().getStringExtra(FrpConstants.FRP_DEVICE_ID);
        voiceEnable = getIntent().getBooleanExtra(FrpConstants.FRP_VOICE_ENABLE, false);
        recordRegisterdId = getIntent().getStringExtra(FrpConstants.FRP_REGISTER_RECORD_ID);
        appVersion = getIntent().getStringExtra(FrpConstants.APP_VERSION);

        if (TextUtils.isEmpty(mTransactionUniqueID)) {
            finish();
        }
        goFPRSdkAuth();

    }

    /**
     * 防止app横竖屏切换 Android 8.0要求透明样式的activity不能再Manifest中指定横竖屏
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
    }


    private void goFPRSdkAuth() {
        //本地证书调试
        Certificate[] certificates = IdvSdkManager.getInstance().getCertificates();
        mLiveBuilder = new LiveBuilder();
        mLiveBuilder.configCertsArray(null, mTransactionUniqueID, "EWA-HK", "A", certificates, FrpTransitionActivity.this);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActionContants.CERT_REQUEST_CODE && resultCode == ActionContants.CERT_RESULT_SUCCESS) {
            mLiveBuilder.startLiveForResult(getPackageName(), mTransactionUniqueID, transactionType, "EWA-HK", "A", mIdVSeqNumber, mIdVSeqFid, language, "NM", "12313131313", appVersion, deviceId, recordRegisterdId, voiceEnable, this);
        } else if (requestCode == ActionContants.LIVE_REQUEST_CODE && resultCode == ActionContants.LIVE_RESULT_SUCCESS) {
            Intent in = new Intent();
            setResult(RESULT_OK, in);
            finish();
        } else {
            String code = data.getStringExtra(FrpConstants.FRP_ERROR_CODE);
            String msg = data.getStringExtra(FrpConstants.FRP_ERROR_MSG);
            Intent in = new Intent();
            in.putExtra(FrpConstants.FRP_ERROR_CODE, code);
            in.putExtra(FrpConstants.FRP_ERROR_MSG, msg);
            setResult(RESULT_CANCELED, in);
            finish();
        }
    }

}
