#IDV 混淆代码：

        -keep class com.idv.sdklibrary.bean.**{*;}
        -dontwarn com.abbyy.mobile.**
        -keep class com.abbyy.mobile.** { *; }

#kotlin
        -keep class kotlin.** { *; }
        -keep class kotlin.Metadata { *; }
        -dontwarn kotlin.**
        -keepclassmembers class **$WhenMappings {
               <fields>;
        }
        -keepclassmembers class kotlin.Metadata {
        public <methods>;
        }
        -assumenosideeffects class kotlin.jvm.internal.Intrinsics {
        static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
       }