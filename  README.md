1.实体类javaBean对象 请继承BaseEntity 内部已经实现了序列化接口 无需每个bean再序列化 最重要的是混淆是根据 extends BaseEntity

2.Activity 请继承 BaseActivity BaseActivity 已经存在setToolbarTitle setToolbarTitleColor setToolbarRightTitleColor setToolbarBackGroundColor showRightImage
                                            等等 自己直接使用 可以基本满足导航栏标题 颜色 文字等属性的更改
                  BaseActivity 支持MVP形式 MVP配置 具体使用可以参考TransferByStaticCodeActivity.class

                  BaseActivity 中 configBackGround 现在整个activity/fragment 背景是一张不规则的图片 为了拉伸问题 现在toolbar/底部view 是将这张背景图片切割成两部分来完成，
                               所以哪些界面不需要这种背景图 需要单独处理


3.Fragment 请继承 BaseFragment 同样里头BaseFrament/BaseActivity 都已经实现showErrorMsgDialog showDialogNotCancel 等等弹出消息框控件 （存在优化空间 可以都同步到工具类中进行封装）


4.utils: Activity 跳转 统一用ActivitySkipUtil 方便跳转动画整体修改
         AdvancedCountdownTimer 倒计时封装
         FileUtils 在app data/data中读写文件
         LogUtils 所有的log显示 统一用次工具类 杜绝Log.e Log.i("xxx",xxx)
         SPFuncUtils SharedPreferences 统一使用工具类
         ToastUtils toast封装
         UnsafeOkHttpClient 不加https证书验证的 okhttpclient
         UnsafeOkHttpGlideModule不加https证书验证的 glide控件 okhttpclient
         对于证书校验 1）项目中ewa我们自己的serverhttps证书校验是在OkHttpManager getBuilder方法中配置 ewa的证书路径 需要配置在UrlConst.CER_NAME 文件夹下 注意新旧证书要同时并存，防止新证书未启用ewa.bochk.com/www.bochk.com
                    2）通用功能sdk的证书要放到certs 文件夹下 域名对应的是mba.bochk
                    3)idv frp的证书要放到 idv_cert_prd/frp_cert_prd相应文件夹 注意vss证书应该两边都要用到FRPLoadUtils工具类中 主要读取idv/frp证书列表传给sdk
                    4)fio的证书目前放在fio_certs_prd文件夹

5.protocol:所有协议层 具体使用参考已有代码

6.baseAdapter:所有使用adapter的都直接使用已经存在的CommonAdapter 具体使用方式参考TransacFragment 多typeui使用


7.widget:BottomButtonView 底部tab栏封装 控件
         CustomProgressDialog 加载框dialog
         CustomPswView 自定义密码 控件
         CustomTvEditText 上边标题 下边Edittext 底部线条 Edittext 支持增加按钮 控件
         EditTextWithDel 左边文字 右边箭头 控件
         ImageArrowView 左边文字 右边箭头 底部线条 控件
         LeftImageArrowView 左边图标文字 右边箭头 底部线条 控件
         PasswordInputView ui数据密码原点 控件
         RecyclerViewForEmpty 支持emptyview的recycleView 控件


8.适配方案  AutoSizeConfig.getInstance().setDesignWidthInDp(400).setDesignHeightInDp(853).setExcludeFontScale(true);字体放大缩小/屏幕分辨率切换

9.消息通知：目前通过EventBus具体使用可参考代码

10.语言适配：values/values-en/values-zh-rCN/values-ZH-rHK/values-ZH-rTW  文件夹都不可删除 values-ZH-rHK/values-ZH-rTW string文件一模一样
             不同迭代开发周期的string命名不一致，之前发过生产版本的string文件非必须禁止更改命名规则可以是string_p8 string_p7

11.popwindow(DialogFragment):
//指纹密码通用使用模块
VerifyPasswordCommonActivity.startActivityForResult(getActivity(), new VerifyPasswordCommonActivity.OnPwdVerifyCallBack() {
            @Override
            public void onVerifySuccess() {
                if (currentAction == 0) {
                    startBackScan();
                } else {
                    startAnotherActivity(TransferByStaticCodeActivity.class);
                }
            }

            @Override
            public void onVeiryFailed(String errorCode, String errorMsg) {
                showErrorMessageDialog(getActivity(), errorMsg);
            }
        });
          1)密码认证模块弹出框使用
           private void showPwdVerifyDialog() {
               PaymentPasswordDialogFragment paymentPasswordDialogFragment = new PaymentPasswordDialogFragment();
               PaymentPasswordDialogFragment.OnPwdDialogClickListener onPwdDialogClickListener = new PaymentPasswordDialogFragment.OnPwdDialogClickListener() {
                   @Override
                   public void onPwdCompleteListener(String psw, boolean complete) {
                       if (complete) {
                           verifyPwdEvent(psw);
                       }
                   }

                   @Override
                   public void onPwdBackClickListener() {

                   }
               };
               paymentPasswordDialogFragment.initParams(onPwdDialogClickListener);
               paymentPasswordDialogFragment.show(getActivity().getSupportFragmentManager(), "PaymentPasswordDialogFragment");
           }
           2)卡列表选择弹出框使用
            public void initCardListPopWindow() {
                   SelCardListPopWindow  mSelCardListPopWindow = new SelCardListPopWindow(getActivity(), this, cardEntities);
                    Rect rect = new Rect();
                    getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
                    int winHeight = getActivity().getWindow().getDecorView().getHeight();
                    mSelCardListPopWindow.showAtLocation(getActivity().getWindow().getDecorView(), Gravity.BOTTOM, 0, winHeight - rect.bottom);
                }
            3)报错信息BaseActivity BaseFragment中都有
                public void showErrorMessageDialog(Context mContext, String msg) {
                    if (TextUtils.isEmpty(msg)) {
                        msg = "";
                    }
                    CustomMsgDialog.Builder builder = new CustomMsgDialog.Builder(mContext);
                    builder.setMessage(msg);
                    builder.setPositiveButton(mContext.getString(R.string.POPUP1_1), new android.content.DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    Activity mActivity = (Activity) mContext;
                    if (mActivity != null && !mActivity.isFinishing()) {
                        CustomMsgDialog mDealDialog = builder.create();
                        mDealDialog.show();
                    }
                }

12.代码混淆配置：具体可查看proguard-rules.pro 包含了idv/frp等等的配置

13.FRP IDV SDK使用调用 可以参考IdvTransitionActivity FrpTransitionActivity

14.sit/uat/prd 可以通过Build Variants切换环境 google-services.json文件会手动切换

15.google 推送 参考MyFirebaseMessagingService

16.词条生成工具有统一的jar 执行 导出excell文件到代码中

17.权限的使用 可以通过
                  new RxPermissions(TransferByStaticCodeActivity.this).request(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE).subscribe(new io.reactivex.functions.Consumer<Boolean>() {
                       @Override
                       public void accept(Boolean aBoolean) throws Exception {
                           if (aBoolean) {
                               shareImage(currentQrcode);
                           } else {
                               showLackOfPermissionDialog();
                           }
                       }
                   });
18.







