    #===========sdk====================================
    -keep class com.egoo.sdk.**{*;}
    -keep class tv.danmaku.ijk.media.player.**{*;}
    #=================chat===================================================
    -keep class com.egoo.**{*;}
    #tbs
    -keep class com.tencent.**{*;}
    #record
    -keep class com.buihha.audiorecorder.**{*;}
    -keep class com.czt.**{*;}
    #glide
    -keep public class * implements com.bumptech.glide.module.GlideModule
    -keep class com.bumptech.glide.load.resource.gif.GifFrameLoader {*;}
    -keep public class * extends com.bumptech.glide.module.AppGlideModule
    -keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
      **[] $VALUES;
      public *;
    }